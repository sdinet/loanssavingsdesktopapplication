﻿namespace SPARC
{
    partial class frmUPFAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSAcc = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblCurrBalValue = new System.Windows.Forms.Label();
            this.lblCurrBal = new System.Windows.Forms.Label();
            this.lblAccOpenDate = new System.Windows.Forms.Label();
            this.lblAccOpenlbl = new System.Windows.Forms.Label();
            this.lblStatusVal = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.dtpTransDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblMemberValue = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.dgUPFAccount = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayOutEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.UPFTransID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMessages = new System.Windows.Forms.Label();
            this.pnlSAcc.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSAcc
            // 
            this.pnlSAcc.Controls.Add(this.groupBox1);
            this.pnlSAcc.Controls.Add(this.lblInfo);
            this.pnlSAcc.Controls.Add(this.dgUPFAccount);
            this.pnlSAcc.Controls.Add(this.lblMessages);
            this.pnlSAcc.Location = new System.Drawing.Point(12, 22);
            this.pnlSAcc.Name = "pnlSAcc";
            this.pnlSAcc.Size = new System.Drawing.Size(797, 635);
            this.pnlSAcc.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lblCurrBalValue);
            this.groupBox1.Controls.Add(this.lblCurrBal);
            this.groupBox1.Controls.Add(this.lblAccOpenDate);
            this.groupBox1.Controls.Add(this.lblAccOpenlbl);
            this.groupBox1.Controls.Add(this.lblStatusVal);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.dtpTransDate);
            this.groupBox1.Controls.Add(this.lblTransactionDate);
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.cmbAgentName);
            this.groupBox1.Controls.Add(this.lblAgentName);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.lblAccValue);
            this.groupBox1.Controls.Add(this.lblAccountID);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.lblRemarks);
            this.groupBox1.Controls.Add(this.lblMemberValue);
            this.groupBox1.Controls.Add(this.lblMemberID);
            this.groupBox1.Location = new System.Drawing.Point(250, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 350);
            this.groupBox1.TabIndex = 109;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(159, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 109;
            this.button1.Text = "New";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblCurrBalValue
            // 
            this.lblCurrBalValue.AutoSize = true;
            this.lblCurrBalValue.Location = new System.Drawing.Point(156, 116);
            this.lblCurrBalValue.Name = "lblCurrBalValue";
            this.lblCurrBalValue.Size = new System.Drawing.Size(0, 13);
            this.lblCurrBalValue.TabIndex = 108;
            // 
            // lblCurrBal
            // 
            this.lblCurrBal.AutoSize = true;
            this.lblCurrBal.Location = new System.Drawing.Point(48, 116);
            this.lblCurrBal.Name = "lblCurrBal";
            this.lblCurrBal.Size = new System.Drawing.Size(83, 13);
            this.lblCurrBal.TabIndex = 107;
            this.lblCurrBal.Text = "Current Balance";
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.AutoSize = true;
            this.lblAccOpenDate.Location = new System.Drawing.Point(156, 85);
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Size = new System.Drawing.Size(0, 13);
            this.lblAccOpenDate.TabIndex = 106;
            // 
            // lblAccOpenlbl
            // 
            this.lblAccOpenlbl.AutoSize = true;
            this.lblAccOpenlbl.Location = new System.Drawing.Point(48, 85);
            this.lblAccOpenlbl.Name = "lblAccOpenlbl";
            this.lblAccOpenlbl.Size = new System.Drawing.Size(99, 13);
            this.lblAccOpenlbl.TabIndex = 105;
            this.lblAccOpenlbl.Text = "Account OpenDate";
            // 
            // lblStatusVal
            // 
            this.lblStatusVal.AutoSize = true;
            this.lblStatusVal.Location = new System.Drawing.Point(332, 116);
            this.lblStatusVal.Name = "lblStatusVal";
            this.lblStatusVal.Size = new System.Drawing.Size(0, 13);
            this.lblStatusVal.TabIndex = 104;
            this.lblStatusVal.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(289, 116);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 103;
            this.lblStatus.Text = "Status";
            this.lblStatus.Visible = false;
            // 
            // dtpTransDate
            // 
            this.dtpTransDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransDate.Location = new System.Drawing.Point(159, 172);
            this.dtpTransDate.Name = "dtpTransDate";
            this.dtpTransDate.Size = new System.Drawing.Size(122, 20);
            this.dtpTransDate.TabIndex = 2;
            this.dtpTransDate.ValueChanged += new System.EventHandler(this.dtpTransDate_ValueChanged);
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(48, 178);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(69, 13);
            this.lblTransactionDate.TabIndex = 89;
            this.lblTransactionDate.Text = "Deposit Date";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(156, 144);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(125, 20);
            this.txtAmount.TabIndex = 1;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(268, 311);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(51, 311);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(155, 204);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(126, 21);
            this.cmbAgentName.TabIndex = 3;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(48, 209);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(78, 13);
            this.lblAgentName.TabIndex = 77;
            this.lblAgentName.Text = "Leader Name *";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(48, 147);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(89, 13);
            this.lblAmount.TabIndex = 75;
            this.lblAmount.Text = "Deposit Amount *";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(156, 54);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(0, 13);
            this.lblAccValue.TabIndex = 74;
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(48, 54);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 73;
            this.lblAccountID.Text = "Account ID";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(155, 238);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(195, 59);
            this.txtRemarks.TabIndex = 4;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(48, 240);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 4;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblMemberValue
            // 
            this.lblMemberValue.AutoSize = true;
            this.lblMemberValue.Location = new System.Drawing.Point(156, 23);
            this.lblMemberValue.Name = "lblMemberValue";
            this.lblMemberValue.Size = new System.Drawing.Size(35, 13);
            this.lblMemberValue.TabIndex = 1;
            this.lblMemberValue.Text = "label1";
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(48, 23);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(59, 13);
            this.lblMemberID.TabIndex = 0;
            this.lblMemberID.Text = "Member ID";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(3, 370);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(139, 13);
            this.lblInfo.TabIndex = 102;
            this.lblInfo.Text = "Double click on a row to edit";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgUPFAccount
            // 
            this.dgUPFAccount.AllowUserToAddRows = false;
            this.dgUPFAccount.AllowUserToDeleteRows = false;
            this.dgUPFAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUPFAccount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUPFAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUPFAccount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.AccountNumber,
            this.AgentID,
            this.TransactionDate,
            this.TransType,
            this.AgentName,
            this.Amount,
            this.Status,
            this.PayOutEndDate,
            this.Remarks,
            this.btnDelete,
            this.UPFTransID,
            this.CurrentBalance});
            this.dgUPFAccount.Location = new System.Drawing.Point(6, 387);
            this.dgUPFAccount.Margin = new System.Windows.Forms.Padding(0);
            this.dgUPFAccount.Name = "dgUPFAccount";
            this.dgUPFAccount.ReadOnly = true;
            this.dgUPFAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUPFAccount.Size = new System.Drawing.Size(785, 238);
            this.dgUPFAccount.TabIndex = 101;
            this.dgUPFAccount.TabStop = false;
            this.dgUPFAccount.VirtualMode = true;
            this.dgUPFAccount.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellClick);
            this.dgUPFAccount.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellDoubleClick);
            this.dgUPFAccount.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgUPFAccount_DataBindingComplete);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // AccountNumber
            // 
            this.AccountNumber.DataPropertyName = "AccountNumber";
            this.AccountNumber.HeaderText = "Account ID";
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.ReadOnly = true;
            this.AccountNumber.Visible = false;
            // 
            // AgentID
            // 
            this.AgentID.DataPropertyName = "fk_AgentId";
            this.AgentID.HeaderText = "AgentID";
            this.AgentID.Name = "AgentID";
            this.AgentID.ReadOnly = true;
            this.AgentID.Visible = false;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            dataGridViewCellStyle1.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.TransactionDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionDate.HeaderText = "Transaction Date";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            // 
            // TransType
            // 
            this.TransType.DataPropertyName = "TranType";
            this.TransType.HeaderText = "TransType";
            this.TransType.Name = "TransType";
            this.TransType.ReadOnly = true;
            // 
            // AgentName
            // 
            this.AgentName.DataPropertyName = "AgentName";
            dataGridViewCellStyle2.Format = "dd/month/yyyy";
            this.AgentName.DefaultCellStyle = dataGridViewCellStyle2;
            this.AgentName.HeaderText = "Leader Name";
            this.AgentName.Name = "AgentName";
            this.AgentName.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // PayOutEndDate
            // 
            this.PayOutEndDate.DataPropertyName = "PayOutEndDate";
            this.PayOutEndDate.HeaderText = "Interest Pay Date";
            this.PayOutEndDate.Name = "PayOutEndDate";
            this.PayOutEndDate.ReadOnly = true;
            this.PayOutEndDate.Visible = false;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "TransRemarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            this.btnDelete.Width = 5;
            // 
            // UPFTransID
            // 
            this.UPFTransID.DataPropertyName = "UPFTrans";
            this.UPFTransID.HeaderText = "UPFTransID";
            this.UPFTransID.Name = "UPFTransID";
            this.UPFTransID.ReadOnly = true;
            this.UPFTransID.Visible = false;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentBalance";
            this.CurrentBalance.HeaderText = "CurrentBalance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            this.CurrentBalance.Visible = false;
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(151, 369);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 87;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmUPFAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(834, 644);
            this.Controls.Add(this.pnlSAcc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUPFAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.UPFAccount_Load);
            this.pnlSAcc.ResumeLayout(false);
            this.pnlSAcc.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSAcc;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label lblMemberValue;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.DateTimePicker dtpTransDate;
        private System.Windows.Forms.Label lblTransactionDate;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label lblInfo;
        public System.Windows.Forms.DataGridView dgUPFAccount;
        private System.Windows.Forms.Label lblStatusVal;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblAccOpenlbl;
        private System.Windows.Forms.Label lblAccOpenDate;
        private System.Windows.Forms.Label lblCurrBalValue;
        private System.Windows.Forms.Label lblCurrBal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransType;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayOutEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn UPFTransID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.Button button1;
    }
}