﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;

namespace SPARC
{
    public partial class EnlargeImage1 : Form
    {
        public Int16 intMemberOperationType;
        public EnlargeImage1()
        {
            InitializeComponent();
        }

        Image picQr;
        public EnlargeImage1(Image qrCodeimage)
        {

            InitializeComponent();
            picQr = qrCodeimage;
        }
        OpenFileDialog openFileDialog1 = new OpenFileDialog();
        private void EnlargeImage1_Load(object sender, EventArgs e)
        {
            
           pictureBox1.Image = picQr;

           //Bitmap bt = new Bitmap(picQr);
           //pictureBox1.Image = bt;
           pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
          trackBar1_Scroll(sender, e);
          
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;  
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

            try
            {
                if (trackBar1.Value > 0)
                {

                    // Image orgimg = pictureBox1.Image;
                    pictureBox1.Image = Zoom(picQr, new Size(trackBar1.Value, trackBar1.Value));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        Image Zoom(Image img, Size size)
        {
            Bitmap bmp = new Bitmap(img, img.Width + (img.Width * size.Width / 100), img.Height + (img.Height * size.Height / 100));
            Graphics g = Graphics.FromImage(bmp);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            return bmp;
        }
         









        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();     
        }
    }
}
