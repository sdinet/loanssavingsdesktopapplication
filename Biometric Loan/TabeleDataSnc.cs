﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace SPARC
{
    public partial class TabeleDataSnc : Form
    {
        public TabeleDataSnc()
        {
            InitializeComponent();
        }


        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["connstringindia"].Value;

  //    private static string ConnectionStringSync = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnSync"].Value;

        private void button1_Click(object sender, EventArgs e)
        {
           
                label2.Visible = false;
                label3.Visible = true;
                SqlConnection conn = new SqlConnection(ConnectionString);
                conn.Open();

                int i = 0;
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();
                DataSet ds4 = new DataSet();
                DataSet ds5 = new DataSet();
                DataSet ds6 = new DataSet();
                DataSet ds7 = new DataSet();
                DataSet ds8 = new DataSet();
                try
                {
           
                // codevalue table  query
                SqlDataAdapter da = new SqlDataAdapter("Select Id, Name, fk_CodeTypeId, ISNull(fk_ParentId, 0) as 'fk_ParentId',Isnull(DisplayOrder, 0) as 'DisplayOrder', Isnull(Abbreviation,0) as 'Abbreviation', Isdeleted, Createdby, CreatedDate, Updatedby, UpdatedDate, Isnull(Sync_Type,'N') as 'Sync_Type', Isnull(Server_Id, 0) as 'Server_Id' from CodeValue;", conn);

                da.Fill(ds);
                // CodeType  table  query
                SqlDataAdapter da1 = new SqlDataAdapter("Select Id, Code, Isdeleted, Createdby, CreatedDate, Updatedby, UpdatedDate, ISNULL(Sync_Type, 0) as 'Sync_Type', ISNULL(Server_Id, 0) as 'Server_Id' from codetype", conn);
                da1.Fill(ds1);
                //// dailytransactions table  Stored Procedure
                //SqlDataAdapter da2 = new SqlDataAdapter("GetDailyTotalGraphData", conn);
                //da2.Fill(ds2);
                

                //new code--start
                SqlDataAdapter da2 = new SqlDataAdapter();
                SqlCommand cmd2 = new SqlCommand("GetDailyTotalGraphData", conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandTimeout = 100;
                da2.SelectCommand = cmd2;
                da2.Fill(ds2);
                //new code--end

                //cancelled members table  query
                SqlDataAdapter da3 = new SqlDataAdapter("SELECT { fn MONTHNAME(s.UpdatedDate) } AS MonthName, YEAR(s.UpdatedDate) AS Year, COUNT(s.Id) AS Count,m.fk_slumid, c1.fk_ParentId as fk_cityId, c2.fk_ParentId as fk_stateId FROM savingaccount s, members m, codevalue c1, codevalue c2 where s.fk_memberId=m.Id and m.fk_slumId=c1.Id and c1.fk_parentId=c2.Id and s.status='Closed' GROUP BY { fn MONTHNAME(s.UpdatedDate) }, MONTH(s.UpdatedDate), YEAR(s.UpdatedDate),fk_slumId,c1.fk_ParentId,c2.fk_ParentId order by Year(s.UpdatedDate),month(s.UpdatedDate),fk_slumId,c1.fk_ParentId,c2.fk_ParentId", conn);
                da3.Fill(ds3);
                
               // weekly transactions table  Stored Procedure                
               // SqlDataAdapter da4 = new SqlDataAdapter("GetLineGraphDataToExport", conn);
               //da4.Fill(ds4);

                //new code--start
                SqlDataAdapter da4 = new SqlDataAdapter();
                SqlCommand cmd4 = new SqlCommand("GetLineGraphDataToExport", conn);
                cmd4.CommandType = CommandType.StoredProcedure;
                cmd4.CommandTimeout = 60;
                da4.SelectCommand = cmd4;
                da4.Fill(ds4);
                //new code--end
                
                // loan type  table  query
                SqlDataAdapter da5 = new SqlDataAdapter("Select c.name as LoanType,count(LoanType) as count,m.fk_slumId, c1.fk_ParentId as fk_cityId, c2.fk_ParentId as fk_stateId from loanaccount s, codevalue c, codevalue c1, codevalue c2, members m where s.LoanType=c.id and s.fk_memberId=m.Id and m.fk_slumId=c1.Id and c1.fk_parentId=c2.Id group by LoanType,c.Name,m.fk_slumId,c1.fk_ParentId,c2.fk_ParentId", conn);
                da5.Fill(ds5);
                // savingtype  table  query
                SqlDataAdapter da6 = new SqlDataAdapter("Select c.name as savingtype,count(fk_savingtype) as count,m.fk_slumId, c1.fk_ParentId as fk_cityId, c2.fk_ParentId as fk_stateId from savingaccount s, codevalue c, codevalue c1, codevalue c2, members m where s.fk_savingtype=c.id and s.fk_memberId=m.Id and m.fk_slumId=c1.Id and c1.fk_parentId=c2.Id  group by fk_savingtype,c.Name,m.fk_slumId,c1.fk_ParentId,c2.fk_ParentId", conn);
                da6.Fill(ds6);
                // new members table  query
                SqlDataAdapter da7 = new SqlDataAdapter("SELECT { fn MONTHNAME(m.CreatedDate) } AS MonthName, YEAR(m.CreatedDate) AS Year, COUNT(m.Id) AS Count,fk_slumid, c1.fk_ParentId as fk_cityId, c2.fk_ParentId as fk_stateId FROM members m, codevalue c1, codevalue c2 where m.fk_slumId=c1.Id and c1.fk_parentId=c2.Id GROUP BY { fn MONTHNAME(m.CreatedDate) }, MONTH(m.CreatedDate), YEAR(m.CreatedDate),fk_slumId,c1.fk_ParentId,c2.fk_ParentId order by Year(m.CreatedDate),month(m.CreatedDate),fk_slumId,c1.fk_ParentId,c2.fk_ParentId;", conn);
                da7.Fill(ds7);
                // loan status table query
                SqlDataAdapter da8 = new SqlDataAdapter("Select 'Loan Paid' as status, count(*) as total from loanaccount where status='Closed' Union select 'Pending Payment' as status, count(*) as total from loanaccount where emiplannedend<updatedDate and Status<>'Closed' and balanceprinciple>0", conn);
                da8.Fill(ds8);

                conn.Close();


            }
            catch (Exception ex)
            {
                //conn.Close();
                throw ex;
                
            }
                

                
            


            if (ds.Tables.Count <= 0 && ds1.Tables.Count <= 0 && ds2.Tables.Count <= 0 && ds3.Tables.Count <= 0 && ds4.Tables.Count <= 0 && ds5.Tables.Count <= 0 && ds6.Tables.Count <= 0 && ds7.Tables.Count <= 0 && ds8.Tables.Count < 0)
            {
                MessageBox.Show("NO Data Present To Sync");
                label2.Visible = true;
                label2.Text = "Sync Failed Since No Data Present To Sync";
            }
            else
            {
             
                NpgsqlConnection connsync = new NpgsqlConnection("Server=localhost;Port=5432;User id=postgres;Password=admin@123;Protocol=3;SSL=false;Pooling=false;MinPoolSize=1;MaxPoolSize=200;Timeout=60;SslMode=Disable;Database=wtiDataView");
                //NpgsqlConnection connsync = new NpgsqlConnection(ConnectionString1);
                
                connsync.Open();

                label3.Visible = true;
                using (IDbTransaction tran = connsync.BeginTransaction())
                {
                    try
                    {
                        //try
                        //{

                        NpgsqlCommand command1 = new NpgsqlCommand("Truncate table codevalue restart identity;", connsync);
                        command1.CommandType = CommandType.Text;
                        int DSS1 = command1.ExecuteNonQuery();

                        NpgsqlCommand command2 = new NpgsqlCommand("Truncate table codetype restart identity;", connsync);
                        command2.CommandType = CommandType.Text;
                        int DSS2 = command2.ExecuteNonQuery();

                        NpgsqlCommand command3 = new NpgsqlCommand("Truncate table dailytransactions restart identity;", connsync);
                        command3.CommandType = CommandType.Text;
                        int DSS3 = command3.ExecuteNonQuery();

                        NpgsqlCommand command4 = new NpgsqlCommand("Truncate table cancelledmembers restart identity;", connsync);
                        command4.CommandType = CommandType.Text;
                        int DSS4 = command4.ExecuteNonQuery();

                        NpgsqlCommand command5 = new NpgsqlCommand("Truncate table weeklytransactions restart identity;", connsync);
                        command5.CommandType = CommandType.Text;
                        int DSS5 = command5.ExecuteNonQuery();

                        NpgsqlCommand command6 = new NpgsqlCommand("Truncate table loantype restart identity;", connsync);
                        command6.CommandType = CommandType.Text;
                        int DSS6 = command6.ExecuteNonQuery();

                        NpgsqlCommand command7 = new NpgsqlCommand("Truncate table savingtype restart identity;", connsync);
                        command7.CommandType = CommandType.Text;
                        int DSS7 = command7.ExecuteNonQuery();

                        NpgsqlCommand command8 = new NpgsqlCommand("Truncate table newmembers restart identity;", connsync);
                        command8.CommandType = CommandType.Text;
                        int DSS8 = command8.ExecuteNonQuery();

                        NpgsqlCommand command9 = new NpgsqlCommand("Truncate table loanstatus restart identity;", connsync);
                        command9.CommandType = CommandType.Text;
                        int DSS9 = command9.ExecuteNonQuery();
                        //}
                        //catch (Exception)
                        //{
                        //    MessageBox.Show("Unable To Delete Data From The Postgres");
                        //    connsync.Close();
                        //}


                        NpgsqlCommand command11 = new NpgsqlCommand("select * from codevalue;", connsync);
                        command11.CommandType = CommandType.Text;
                        int count11 = command11.ExecuteNonQuery();

                        NpgsqlCommand command12 = new NpgsqlCommand("select * from cancelledmembers;", connsync);
                        command12.CommandType = CommandType.Text;
                        int count12 = command12.ExecuteNonQuery();

                        NpgsqlCommand command13 = new NpgsqlCommand("select * from codetype;", connsync);
                        command13.CommandType = CommandType.Text;
                        int count13 = command13.ExecuteNonQuery();

                        NpgsqlCommand command14 = new NpgsqlCommand("select * from dailytransactions;", connsync);
                        command14.CommandType = CommandType.Text;
                        int count14 = command14.ExecuteNonQuery();

                        NpgsqlCommand command15 = new NpgsqlCommand("select * from loantype;", connsync);
                        command15.CommandType = CommandType.Text;
                        int count15 = command15.ExecuteNonQuery();

                        NpgsqlCommand command16 = new NpgsqlCommand("select * from newmembers;", connsync);
                        command16.CommandType = CommandType.Text;
                        int count16 = command16.ExecuteNonQuery();

                        NpgsqlCommand command17 = new NpgsqlCommand("select * from savingtype;", connsync);
                        command17.CommandType = CommandType.Text;
                        int count17 = command17.ExecuteNonQuery();

                        NpgsqlCommand command18 = new NpgsqlCommand("select * from weeklytransactions;", connsync);
                        command18.CommandType = CommandType.Text;
                        int count18 = command18.ExecuteNonQuery();

                        NpgsqlCommand command19 = new NpgsqlCommand("select * from loanstatus;", connsync);
                        command19.CommandType = CommandType.Text;
                        int count19 = command19.ExecuteNonQuery();

                        //try
                        //{
                        //Code Value
                        String NAME;
                        Int64 fk_CodeTypeId;
                        Int64 fk_ParentId;
                        Int64 DisplayOrder;
                        String Abbreviation;
                        Boolean Isdeleted;
                        Int64 Createdby;
                        DateTime CreatedDate;
                        Int64 Updatedby;
                        DateTime UpdatedDate;
                        Char Sync_Type;
                        Int64 Server_Id;
                        if (count11 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from codevalue Table");
                            connsync.Close();
                        }
                        else
                        {
                            //try
                            //{
                            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                // code value

                                NAME = ds.Tables[0].Rows[i]["Name"].ToString();
                                fk_CodeTypeId = Convert.ToInt64(ds.Tables[0].Rows[i]["fk_CodeTypeId"].ToString());
                                fk_ParentId = Convert.ToInt64(ds.Tables[0].Rows[i]["fk_ParentId"].ToString());
                                DisplayOrder = Convert.ToInt64(ds.Tables[0].Rows[i]["DisplayOrder"].ToString());
                                Abbreviation = ds.Tables[0].Rows[i]["Abbreviation"].ToString();
                                Isdeleted = Convert.ToBoolean(ds.Tables[0].Rows[i]["Isdeleted"].ToString());
                                Createdby = Convert.ToInt64(ds.Tables[0].Rows[i]["Createdby"].ToString());
                                CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["CreatedDate"].ToString());
                                Updatedby = Convert.ToInt64(ds.Tables[0].Rows[i]["Updatedby"].ToString());
                                UpdatedDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["UpdatedDate"].ToString());
                                Sync_Type = Convert.ToChar(ds.Tables[0].Rows[i]["Sync_Type"].ToString());
                                Server_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Server_Id"].ToString());

                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command21 = new NpgsqlCommand("fn_codevalueinsert", connsync);
                                command21.CommandType = System.Data.CommandType.StoredProcedure;

                                command21.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                command21.Parameters[0].Value = NAME;

                                command21.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[1].Value = fk_CodeTypeId;

                                command21.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[2].Value = fk_ParentId;

                                command21.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[3].Value = DisplayOrder;

                                command21.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Text));
                                command21.Parameters[4].Value = Abbreviation;

                                command21.Parameters.Add(new NpgsqlParameter("@id6", NpgsqlTypes.NpgsqlDbType.Boolean));
                                command21.Parameters[5].Value = Isdeleted;

                                command21.Parameters.Add(new NpgsqlParameter("@id7", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[6].Value = Createdby;

                                command21.Parameters.Add(new NpgsqlParameter("@id8", NpgsqlTypes.NpgsqlDbType.TimestampTZ));
                                command21.Parameters[7].Value = CreatedDate;

                                command21.Parameters.Add(new NpgsqlParameter("@id9", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[8].Value = Updatedby;

                                command21.Parameters.Add(new NpgsqlParameter("@id10", NpgsqlTypes.NpgsqlDbType.TimestampTZ));
                                command21.Parameters[9].Value = UpdatedDate;

                                command21.Parameters.Add(new NpgsqlParameter("@id11", NpgsqlTypes.NpgsqlDbType.Char));
                                command21.Parameters[10].Value = Sync_Type;

                                command21.Parameters.Add(new NpgsqlParameter("@id12", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command21.Parameters[11].Value = Server_Id;


                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command21);
                                sa.Fill(updatechild1);


                            }

                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show(ex.Message);
                            //    connsync.Close();
                            //}
                        }

                        //cancelled members
                        String MONTHNAME1;
                        Int32 YEAR;
                        Int32 COUNT;
                        Int32 SLUMID1;
                        Int32 CITYID1;
                        Int32 STATEID1;
                        if (count12 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from cancelledmembers Table");
                            connsync.Close();
                        }
                        else
                        {
                            //try
                            //{

                            for (i = 0; i < ds3.Tables[0].Rows.Count; i++)
                            {
                                //cancelled members                           

                                MONTHNAME1 = ds3.Tables[0].Rows[i]["MonthName"].ToString();
                                YEAR = Convert.ToInt32(ds3.Tables[0].Rows[i]["Year"].ToString());
                                COUNT = Convert.ToInt32(ds3.Tables[0].Rows[i]["Count"].ToString());
                                SLUMID1 = Convert.ToInt32(ds3.Tables[0].Rows[i]["fk_slumid"].ToString());
                                CITYID1 = Convert.ToInt32(ds3.Tables[0].Rows[i]["fk_cityid"].ToString());
                                STATEID1 = Convert.ToInt32(ds3.Tables[0].Rows[i]["fk_stateid"].ToString());

                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command24 = new NpgsqlCommand("fn_cancelledmembersinsert", connsync);
                                command24.CommandType = System.Data.CommandType.StoredProcedure;

                                command24.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                command24.Parameters[0].Value = MONTHNAME1;

                                command24.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                command24.Parameters[1].Value = YEAR;

                                command24.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Integer));
                                command24.Parameters[2].Value = COUNT;

                                command24.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Integer));
                                command24.Parameters[3].Value = SLUMID1;

                                command24.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                command24.Parameters[4].Value = CITYID1;

                                command24.Parameters.Add(new NpgsqlParameter("@id6", NpgsqlTypes.NpgsqlDbType.Integer));
                                command24.Parameters[5].Value = STATEID1;

                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command24);
                                sa.Fill(updatechild1);

                            }
                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show(ex.Message);
                            //    connsync.Close();
                            //}

                        }

                        // code type
                        Int64 ID;
                        String CODE;
                        Boolean ISDELETED;
                        Int64 CREATEDBY;
                        DateTime CREATEDDATE;
                        Int64 UPDATEDBY;
                        DateTime UPDATEDDATE;
                        Char SYNC_TYPE;
                        Int64 SERVER_ID;

                        if (count13 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from codetype Table");
                            connsync.Close();
                        }
                        else
                        {
                            //try
                            //{
                            for (i = 0; i < ds1.Tables[0].Rows.Count; i++)
                            {
                                //  Code Type                        

                                ID = Convert.ToInt64(ds1.Tables[0].Rows[i]["Id"].ToString());
                                CODE = ds1.Tables[0].Rows[i]["Code"].ToString();
                                ISDELETED = Convert.ToBoolean(ds1.Tables[0].Rows[i]["Isdeleted"].ToString());
                                CREATEDBY = Convert.ToInt64(ds1.Tables[0].Rows[i]["Createdby"].ToString());
                                CREATEDDATE = Convert.ToDateTime(ds1.Tables[0].Rows[i]["CreatedDate"].ToString());
                                UPDATEDBY = Convert.ToInt64(ds1.Tables[0].Rows[i]["Updatedby"].ToString());
                                UPDATEDDATE = Convert.ToDateTime(ds1.Tables[0].Rows[i]["UpdatedDate"].ToString());
                                SYNC_TYPE = Convert.ToChar(ds1.Tables[0].Rows[i]["Sync_Type"].ToString());
                                SERVER_ID = Convert.ToInt64(ds1.Tables[0].Rows[i]["Server_Id"].ToString());

                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command22 = new NpgsqlCommand("fn_codetypeinsert", connsync);
                                command22.CommandType = System.Data.CommandType.StoredProcedure;

                                command22.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command22.Parameters[0].Value = ID;

                                command22.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Text));
                                command22.Parameters[1].Value = CODE;

                                command22.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Boolean));
                                command22.Parameters[2].Value = ISDELETED;

                                command22.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command22.Parameters[3].Value = CREATEDBY;

                                command22.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.TimestampTZ));
                                command22.Parameters[4].Value = CREATEDDATE;

                                command22.Parameters.Add(new NpgsqlParameter("@id6", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command22.Parameters[5].Value = UPDATEDBY;

                                command22.Parameters.Add(new NpgsqlParameter("@id7", NpgsqlTypes.NpgsqlDbType.TimestampTZ));
                                command22.Parameters[6].Value = UPDATEDDATE;

                                command22.Parameters.Add(new NpgsqlParameter("@id8", NpgsqlTypes.NpgsqlDbType.Char));
                                command22.Parameters[7].Value = SYNC_TYPE;

                                command22.Parameters.Add(new NpgsqlParameter("@id9", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command22.Parameters[8].Value = SERVER_ID;

                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command22);
                                sa.Fill(updatechild1);


                            }
                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show(ex.Message);
                            //    connsync.Close();
                            //}
                        }

                        //dailytransactions
                        String txntype6;
                        DateTime txndate6;
                        Double amount6;
                        Int32 slumid6;
                        Int32 cityid6;
                        Int32 stateid6;
                        if (count14 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from dailytransactions Table");
                            connsync.Close();
                        }
                        else
                        {
                            try
                            {
                            for (i = 0; i < ds2.Tables[0].Rows.Count; i++)
                            {
                                //dailytransactions

                                txntype6 = ds2.Tables[0].Rows[i]["txnType"].ToString();
                                txndate6 = Convert.ToDateTime(ds2.Tables[0].Rows[i]["txnDate"].ToString());
                                amount6 = Convert.ToDouble(ds2.Tables[0].Rows[i]["amount"].ToString());
                                slumid6 = Convert.ToInt32(ds2.Tables[0].Rows[i]["slumId"].ToString());
                                cityid6 = Convert.ToInt32(ds2.Tables[0].Rows[i]["cityId"].ToString());
                                stateid6 = Convert.ToInt32(ds2.Tables[0].Rows[i]["stateId"].ToString());

                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command23 = new NpgsqlCommand("fn_dailytransactionsinsert", connsync);
                                command23.CommandType = System.Data.CommandType.StoredProcedure;

                                command23.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Char));
                                command23.Parameters[0].Value = txntype6;

                                command23.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                command23.Parameters[1].Value = txndate6;

                                command23.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Double));
                                command23.Parameters[2].Value = amount6;

                                command23.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command23.Parameters[3].Value = slumid6;

                                command23.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command23.Parameters[4].Value = cityid6;

                                command23.Parameters.Add(new NpgsqlParameter("@id6", NpgsqlTypes.NpgsqlDbType.Bigint));
                                command23.Parameters[5].Value = stateid6;

                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command23);
                                sa.Fill(updatechild1);

                            }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                                connsync.Close();
                            }
                        }

                        // loan type
                        String LOANTYPE3;
                        Int32 COUNT3;
                        Int32 SLUMID3;
                        Int32 CITYID3;
                        Int32 STATEID3;
                        if (count15 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from loantype Table");
                            connsync.Close();
                        }
                        else
                        {
                            //try
                            //{

                            for (i = 0; i < ds5.Tables[0].Rows.Count; i++)
                            {
                                //loan type                            


                                LOANTYPE3 = ds5.Tables[0].Rows[i]["LoanType"].ToString();
                                COUNT3 = Convert.ToInt32(ds5.Tables[0].Rows[i]["count"].ToString());
                                SLUMID3 = Convert.ToInt32(ds5.Tables[0].Rows[i]["fk_slumId"].ToString());
                                CITYID3 = Convert.ToInt32(ds5.Tables[0].Rows[i]["fk_cityId"].ToString());
                                STATEID3 = Convert.ToInt32(ds5.Tables[0].Rows[i]["fk_stateId"].ToString());


                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command26 = new NpgsqlCommand("fn_loantypeinsert", connsync);
                                command26.CommandType = System.Data.CommandType.StoredProcedure;

                                command26.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                command26.Parameters[0].Value = LOANTYPE3;

                                command26.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                command26.Parameters[1].Value = COUNT3;

                                command26.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Integer));
                                command26.Parameters[2].Value = SLUMID3;

                                command26.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Integer));
                                command26.Parameters[3].Value = CITYID3;

                                command26.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                command26.Parameters[4].Value = STATEID3;


                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command26);
                                sa.Fill(updatechild1);

                            }
                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show(ex.Message);
                            //    connsync.Close();
                            //}
                        }

                        // newmembers
                        String MONTHNAME5;
                        Int32 YEAR5;
                        Int32 COUNT5;
                        Int32 SLUMID5;
                        Int32 CITYID5;
                        Int32 STATEID5;
                        if (count16 > 0)
                        {
                            MessageBox.Show("Data Not Deleted from newmembers Table");
                            connsync.Close();
                        }
                        else
                        {
                            //try
                            //{
                            for (i = 0; i < ds7.Tables[0].Rows.Count; i++)
                            {
                                // newmembers

                                MONTHNAME5 = ds7.Tables[0].Rows[i]["MonthName"].ToString();
                                YEAR5 = Convert.ToInt32(ds7.Tables[0].Rows[i]["Year"].ToString());
                                COUNT5 = Convert.ToInt32(ds7.Tables[0].Rows[i]["Count"].ToString());
                                SLUMID5 = Convert.ToInt32(ds7.Tables[0].Rows[i]["fk_slumid"].ToString());
                                CITYID5 = Convert.ToInt32(ds7.Tables[0].Rows[i]["fk_cityId"].ToString());
                                STATEID5 = Convert.ToInt32(ds7.Tables[0].Rows[i]["fk_stateId"].ToString());

                                DataSet updatechild1 = new DataSet();

                                NpgsqlCommand command28 = new NpgsqlCommand("fn_newmembersinsert", connsync);
                                command28.CommandType = System.Data.CommandType.StoredProcedure;

                                command28.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                command28.Parameters[0].Value = MONTHNAME5;

                                command28.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                command28.Parameters[1].Value = YEAR5;

                                command28.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Integer));
                                command28.Parameters[2].Value = COUNT5;

                                command28.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Integer));
                                command28.Parameters[3].Value = SLUMID5;

                                command28.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                command28.Parameters[4].Value = CITYID5;

                                command28.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                command28.Parameters[5].Value = STATEID5;


                                NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command28);
                                sa.Fill(updatechild1);

                                //    }
                                //}
                                //catch (Exception ex)
                                //{
                                //    MessageBox.Show(ex.Message);
                                //    connsync.Close();
                                //}
                            }

                            // savingtype
                            String SAVINGTYPE4;
                            Int32 COUNT4;
                            Int32 SLUMID4;
                            Int32 CITYID4;
                            Int32 STATEID4;
                            if (count17 > 0)
                            {
                                MessageBox.Show("Data Not Deleted from savingtype Table");
                                connsync.Close();
                            }
                            else
                            {
                                //try
                                //{
                                for (i = 0; i < ds6.Tables[0].Rows.Count; i++)
                                {
                                    // savingtype                          


                                    SAVINGTYPE4 = ds6.Tables[0].Rows[i]["savingtype"].ToString();
                                    COUNT4 = Convert.ToInt32(ds6.Tables[0].Rows[i]["count"].ToString());
                                    SLUMID4 = Convert.ToInt32(ds6.Tables[0].Rows[i]["fk_slumId"].ToString());
                                    CITYID4 = Convert.ToInt32(ds6.Tables[0].Rows[i]["fk_cityId"].ToString());
                                    STATEID4 = Convert.ToInt32(ds6.Tables[0].Rows[i]["fk_stateId"].ToString());


                                    DataSet updatechild1 = new DataSet();

                                    NpgsqlCommand command27 = new NpgsqlCommand("fn_savingtypeinsert", connsync);
                                    command27.CommandType = System.Data.CommandType.StoredProcedure;

                                    command27.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                    command27.Parameters[0].Value = SAVINGTYPE4;

                                    command27.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command27.Parameters[1].Value = COUNT4;

                                    command27.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command27.Parameters[2].Value = SLUMID4;

                                    command27.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command27.Parameters[3].Value = CITYID4;

                                    command27.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command27.Parameters[4].Value = STATEID4;


                                    NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command27);
                                    sa.Fill(updatechild1);

                                }
                                //}
                                //catch (Exception ex)
                                //{
                                //    MessageBox.Show(ex.Message);
                                //    connsync.Close();
                                //}
                            }

                            //weekly transactions
                            Char TXNTYPE2;
                            Int32 TXNYEAR2;
                            Int32 TXNWEEK2;
                            Double AMOUNT2;
                            Int32 SLUMID2;
                            Int32 CITYID2;
                            Int32 STATEID2;
                            if (count18 > 0)
                            {
                                MessageBox.Show("Data Not Deleted from weeklytransactions Table");
                                connsync.Close();
                            }
                            else
                            {
                                //try
                                //{

                                for (i = 0; i < ds4.Tables[0].Rows.Count; i++)
                                {

                                    //weekly transactions


                                    TXNTYPE2 = Convert.ToChar(ds4.Tables[0].Rows[i]["txnType"].ToString());
                                    TXNYEAR2 = Convert.ToInt32(ds4.Tables[0].Rows[i]["txnYear"].ToString());
                                    TXNWEEK2 = Convert.ToInt32(ds4.Tables[0].Rows[i]["txnWeek"].ToString());
                                    AMOUNT2 = Convert.ToDouble(ds4.Tables[0].Rows[i]["amount"].ToString());
                                    SLUMID2 = Convert.ToInt32(ds4.Tables[0].Rows[i]["slumId"].ToString());
                                    CITYID2 = Convert.ToInt32(ds4.Tables[0].Rows[i]["cityId"].ToString());
                                    STATEID2 = Convert.ToInt32(ds4.Tables[0].Rows[i]["stateId"].ToString());

                                    DataSet updatechild1 = new DataSet();

                                    NpgsqlCommand command25 = new NpgsqlCommand("fn_weeklytransactionsinsert", connsync);
                                    command25.CommandType = System.Data.CommandType.StoredProcedure;

                                    command25.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Char));
                                    command25.Parameters[0].Value = TXNTYPE2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command25.Parameters[1].Value = TXNYEAR2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id3", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command25.Parameters[2].Value = TXNWEEK2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id4", NpgsqlTypes.NpgsqlDbType.Double));
                                    command25.Parameters[3].Value = AMOUNT2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id5", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command25.Parameters[4].Value = SLUMID2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id6", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command25.Parameters[5].Value = CITYID2;

                                    command25.Parameters.Add(new NpgsqlParameter("@id7", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command25.Parameters[6].Value = STATEID2;

                                    NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command25);
                                    sa.Fill(updatechild1);

                                }
                                //}
                                //catch (Exception ex)
                                //{
                                //    MessageBox.Show(ex.Message);
                                //    connsync.Close();
                                //}

                            }
                            //Loan status
                            String STATUS5;
                            Int32 TOTAL5;

                            if (count19 > 0)
                            {
                                MessageBox.Show("Data Not Deleted from loanstatus Table");
                                connsync.Close();
                            }
                            else
                            {
                                //try
                                //{

                                for (i = 0; i < ds8.Tables[0].Rows.Count; i++)
                                {

                                    //Loan status 

                                    STATUS5 = ds8.Tables[0].Rows[i]["status"].ToString();
                                    TOTAL5 = Convert.ToInt32(ds8.Tables[0].Rows[i]["total"].ToString());


                                    DataSet updatechild1 = new DataSet();

                                    NpgsqlCommand command26 = new NpgsqlCommand("fn_loanstatusinsert", connsync);
                                    command26.CommandType = System.Data.CommandType.StoredProcedure;

                                    command26.Parameters.Add(new NpgsqlParameter("@id1", NpgsqlTypes.NpgsqlDbType.Text));
                                    command26.Parameters[0].Value = STATUS5;

                                    command26.Parameters.Add(new NpgsqlParameter("@id2", NpgsqlTypes.NpgsqlDbType.Integer));
                                    command26.Parameters[1].Value = TOTAL5;



                                    NpgsqlDataAdapter sa = new NpgsqlDataAdapter(command26);
                                    sa.Fill(updatechild1);

                                }
                                //}
                                //catch (Exception ex)
                                //{
                                //    MessageBox.Show(ex.Message);
                                //    connsync.Close();
                                //}

                            }
                            tran.Commit();
                            tran.Dispose();
                            label3.Visible = false;
                            label2.Visible = true;
                            label2.Text = "Data Sync Completed";
                            MessageBox.Show("Data Synced Successfully.");
                            connsync.Close();

                        }
                    }
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //    connsync.Close();
                    //}
                    //}

                    catch (Exception exRollback)
                    {
                        tran.Rollback();
                        tran.Dispose();
                        label2.Visible = true;
                        label2.Text = "Sync Failed";

                        //string input = exRollback.ToString();
                        //string sub = input.Substring(0, input.Length -1);
                        label3.Visible = false;
                        MessageBox.Show("Sync Failed Since " + (exRollback.Message));                        
                        //MessageBox.Show("Sync Failed Since: " +sub);
                    }

                }
                // label2.Text = "Data Sync Completed";
            }

        }     
                           
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TabeleDataSnc_Load(object sender, EventArgs e)
        {
            //label2.Text = "Click On Move Data Button To Sink Data";
            label2.Visible = true;
            label3.Visible = false;

        }
        
    }
}



