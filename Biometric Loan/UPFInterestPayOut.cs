﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;
using System.Configuration;
namespace SPARC
{
    public partial class frmUPFInterestPayOut : Form
    {
        private Int32 iUPFAccID = 0;

        public frmUPFInterestPayOut()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblIntDueVal.Text.Trim().Length > 0)
                {
                    if (Convert.ToDateTime(lblIntDueVal.Text + " 00:00:00") > DateTime.Today)
                    {
                        DialogResult dsConfirmation = MessageBox.Show("You are paying before the due date. Are you sure you want to continue to pay?", "SPARC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dsConfirmation == DialogResult.No)
                            return;
                    }
                }
                
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;
                    UPFAccount objUPFAcc = new UPFAccount();
                    objUPFAcc.Id = iUPFAccID;
                    objUPFAcc.fk_MemberId = GlobalValues.Member_PkId;
                    objUPFAcc.Status = SPARC.Status.ACTIVE;
                    objUPFAcc.Tran_type = Constants.DEBITTRANSABBREV;
                    objUPFAcc.AccId = GlobalValues.UPFAccId;

                    if (radIntPayout.Checked == true)
                        objUPFAcc.Amount = Convert.ToDecimal(txtIntPayout.Text);

                    if (radPricipalAmount.Checked == true)
                        objUPFAcc.Amount = Convert.ToDecimal(txtPrincipalWithdraw.Text);

                    objUPFAcc.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                    objUPFAcc.Opendate = dtpTransDate.Value;
                    objUPFAcc.Remarks = txtRemarks.Text;
                    objUPFAcc.PayOutEndDate = Convert.ToDateTime(lblIntDueVal.Text);
                    objUPFAcc.Createdby = GlobalValues.User_PkId;
                    objUPFAcc.Updatedby = GlobalValues.User_PkId;
                    objUPFAcc.Pay_Mode = cmbPayMode.Text;

                    DataSet dsUPFAcc = null;
                  dsUPFAcc= objUPFAcc.UpdateUPFAccount(1);
                  //  int saveresult = objUPFAcc.UpdateUPFAccount(1);

                    if (dsUPFAcc != null && dsUPFAcc.Tables.Count > 0 && dsUPFAcc.Tables[0].Rows.Count > 0)
                    {
                        lblMessages.Text = Messages.SAVED_SUCCESS;
                        lblMessages.ForeColor = Color.Green;

                       frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();
                    }
                    else
                    {
                        lblMessages.Text = Messages.ERROR_PROCESSING;
                        lblMessages.ForeColor = Color.Red;
                    }
                    LoadInterestPayOut();
                   // LoadAllUPFInterestPendings();
                    ClearControls();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure,ex);
            }
        }

        private void LoadInterestPayOut()
        {
            //Loads the grid
            SPARC.UPFAccount objUPFAccounts = new SPARC.UPFAccount();
            objUPFAccounts.fk_MemberId = GlobalValues.Member_PkId;

            DataSet dsUPF = objUPFAccounts.LoadInterestPayOut();
            lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["opendate"]).ToString("dd-MMM-yyyy");
            lblDepAmtVal.Text = dsUPF.Tables[1].Rows[0]["CurrentBalance"].ToString();
            lblStatusVal.Text = dsUPF.Tables[1].Rows[0]["Status"].ToString();
            lblIntDueVal.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["PayOutEndDate"]).ToString("dd-MMM-yyyy");
            if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[1].Rows.Count > 0)
            {
                iUPFAccID = Convert.ToInt32(dsUPF.Tables[1].Rows[0]["Id"]);
                
               // lblIntToBePaid.Text = dsUPF.Tables[0].Rows[0]["InterestAmount"].ToString();
              //  lblCheck.Text = dsUPF.Tables[0].Rows[0]["InterestAsOnDate"].ToString();
                dgUPFAccount.AutoGenerateColumns = true;
                dgUPFAccount.DataSource = dsUPF.Tables[0];
                dgUPFAccount.Columns["Id1"].Visible = false;
                for (int i = 0; i < dgUPFAccount.Rows.Count; i++)
                {
                    if (dgUPFAccount.Rows[i].Cells["Status"].Value.ToString().ToLower().Equals("active"))
                    {
                        dgUPFAccount.Rows[i].Cells["btnDelete"].Value = string.Empty;
                    }
                }
            }
            else
            {
                lblMessages.Text = Messages.NOPENDINGPAYOUTS;
                lblMessages.ForeColor = Color.Green;
                            
                dgUPFAccount.DataSource = null;            
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbPayMode.Text == "Select")
            {
                ErrorMsg = Messages.PAYMODE + Messages.COMMAWITHSPACE;
            }
            if (cmbPayMode.Text == PayMode.CASH && cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.PAYTHRUAGENTNAME + Messages.COMMAWITHSPACE;

            if (radIntPayout.Checked == true || radPricipalAmount.Checked == true)
            {

            }
            else
            {
                ErrorMsg += Messages.SELECTRADIOBUTTON + Messages.COMMAWITHSPACE;
            }

            if (radIntPayout.Checked == true)
            {
                if ((txtIntPayout.Text.Trim().Length == 0) ||  Convert.ToDouble(txtIntPayout.Text) != Convert.ToDouble(lblIntAsOnDate.Text))
                    ErrorMsg += Messages.INTERESTAMOUNT + Messages.COMMAWITHSPACE;
            }
            if (radPricipalAmount.Checked == true)
            {
                if (Convert.ToDouble(txtPrincipalWithdraw.Text) > Convert.ToDouble(lblDepAmtVal.Text))
                    ErrorMsg += Messages.PRINCIPALAMOUNTTXT + Messages.COMMAWITHSPACE;
            }

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }
         
        private void UPFInterestPayOut_Load(object sender, EventArgs e)
        {
            try
            {
                dtpTransDate.MaxDate = DateTime.Now;
                FillAgents(true);
                txtRemarks.Enabled = false;
                txtIntPayout.Enabled = false;
                txtPrincipalWithdraw.Enabled = false;
                ClearControls();
                LoadInterestPayOut();

                decimal Amount;
                int Days;
                decimal InterestRate = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["UPF_Interest"]);
                decimal simpleInterest = 0;
                SPARC.UPFAccount objUPFTrans = new SPARC.UPFAccount();
                objUPFTrans.fk_MemberId = GlobalValues.Member_PkId;
                DataSet dsUPF = objUPFTrans.LoadAllUPFTrans();

                foreach (DataRow dr in dsUPF.Tables[0].Rows)
                {
                    if (Convert.ToDateTime(dr[3]) > Convert.ToDateTime(dr[7]))
                    {
                        // Calculate interest when no initial interest is done
                        if (dr[9].ToString() == "CRE")
                        {
                            Amount = Convert.ToDecimal(dr[6]);
                            Days = Convert.ToInt32((Convert.ToDateTime(dr[3]) - Convert.ToDateTime(dr[7])).TotalDays);
                            simpleInterest = Math.Round(simpleInterest + CalculateInterest(Amount, Days, InterestRate), 2);
                        }

                        if (dr[9].ToString() == "DEB" && dr[10].ToString() == "PRINCIPAL PAYOUT")
                        {
                            Amount = Convert.ToDecimal(dr[6]);
                            Days = Convert.ToInt32((Convert.ToDateTime(dr[3]) - Convert.ToDateTime(dr[7])).TotalDays);
                            simpleInterest = Math.Round(simpleInterest - CalculateInterest(Amount, Days, InterestRate), 2);
                        }

                        if (dr[9].ToString() == "DEB" && dr[10].ToString() == "INT PAYOUT")
                        {
                            Amount = Convert.ToDecimal(dr[6]);
                            simpleInterest = Math.Round((simpleInterest - Amount), 2);
                        }
                    }
                }
                lblIntToBePaid.Text = simpleInterest.ToString();
               // LoadAllUPFInterestPendings();

                this.Text += Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                
            }
            catch (Exception ex)
            {
               // Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        //private void LoadAllUPFInterestPendings()
        //{
        //    //Loads the grid
        //    SPARC.UPFAccount objUPFAccounts = new SPARC.UPFAccount();
        //    objUPFAccounts.fk_MemberId = GlobalValues.Member_PkId;
            
        //    DataSet dsUPF = objUPFAccounts.LoadAllUPFAccsTrans();
        //    if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[0].Rows.Count > 0)
        //    {
        //        iUPFAccID = Convert.ToInt32(dsUPF.Tables[0].Rows[0]["Id"]);
        //        lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[0].Rows[0]["opendate"]).ToString("dd-MMM-yyyy");
        //        lblDepAmtVal.Text = dsUPF.Tables[0].Rows[0]["CurrentBalance"].ToString();
        //        lblStatusVal.Text = dsUPF.Tables[0].Rows[0]["Status"].ToString();
        //        lblIntDueVal.Text = Convert.ToDateTime(dsUPF.Tables[0].Rows[0]["duedate"]).ToString("dd-MMM-yyyy");
        //        lblIntToBePaid.Text = dsUPF.Tables[0].Rows[0]["InterestAmount"].ToString();
        //        lblCheck.Text = dsUPF.Tables[0].Rows[0]["InterestAsOnDate"].ToString();                
        //        dgUPFAccount.AutoGenerateColumns = false;
        //        dgUPFAccount.DataSource = dsUPF.Tables[0];

        //        for (int i = 0; i < dgUPFAccount.Rows.Count; i++)
        //        {
        //            if (dgUPFAccount.Rows[i].Cells["Status"].Value.ToString().ToLower().Equals("active"))
        //            {
        //                dgUPFAccount.Rows[i].Cells["btnDelete"].Value = string.Empty;
        //            }
        //        }
        //    }
        //    else
        //    { 
        //        lblMessages.Text = Messages.NOPENDINGPAYOUTS;
        //        lblMessages.ForeColor = Color.Green;
        //    }
        //}

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        //private void dgUPFAccount_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        ClearControls();
        //        if (e.RowIndex >= 0)
        //        {
        //            lblMemberValue.Text = GlobalValues.Member_ID;
        //            lblDepAmtVal.Text = dgUPFAccount.Rows[e.RowIndex].Cells["ContributionAmount"].Value.ToString();
        //            txtRemarks.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
        //            lblIntDueVal.Text = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["duedate"].Value).ToShortDateString();
        //            lblAccValue.Text = dgUPFAccount.Rows[e.RowIndex].Cells["AccountNumber"].Value.ToString();
        //            lblIntToBePaid.Text = dgUPFAccount.Rows[e.RowIndex].Cells["InterestAmount"].Value.ToString();//(Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["Amount"].Value) * (Convert.ToDouble(ConfigurationSettings.AppSettings["UPF_Interest"].ToString()) / 100.00)).ToString();
        //            iUPFAccID = Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["Id"].Value);
        //            lblStatusVal.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Status"].Value.ToString();
        //            lblIntAsOnDate.Text = dgUPFAccount.Rows[e.RowIndex].Cells["InterestAsOnDate"].Value.ToString();
        //            if (lblStatusVal.Text == SPARC.Status.CLOSED)
        //            {
        //                btnPay.Enabled = false;

        //                dtpTransDate.Value = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["DuePaidDate"].Value.ToString());
        //                if (dgUPFAccount.Rows[e.RowIndex].Cells["AgentPaidDue"].Value != DBNull.Value)
        //                {
        //                    cmbAgentName.SelectedValue = dgUPFAccount.Rows[e.RowIndex].Cells["AgentPaidDue"].Value.ToString();
        //                }
        //                else
        //                {
        //                    cmbAgentName.SelectedIndex = 0;
        //                }
        //                cmbPayMode.SelectedIndex = cmbPayMode.FindString(dgUPFAccount.Rows[e.RowIndex].Cells["PayMode"].Value.ToString());
        //            }
        //            else { btnPay.Enabled = true; }

        //            dtpTransDate.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
        //    }
        //}

        private void ClearControls()
        {
            lblMemberValue.Text = GlobalValues.Member_ID;
           lblAccValue.Text = GlobalValues.UPF_AccNumber;
          ////   lblDepAmtVal.Text = string.Empty;
          //  txtRemarks.Text = string.Empty;
            //  lblIntDueVal.Text = string.Empty;
            //lblIntAsOnDate.Text = string.Empty;
            //lblAccValue.Text = string.Empty;
            //lblIntToBePaid.Text = string.Empty;
            txtPrincipalWithdraw.Text = string.Empty;
            txtIntPayout.Text = string.Empty;
            lblStatusVal.Text = "";
            cmbPayMode.SelectedIndex = 0;
            cmbAgentName.SelectedIndex = 0;
            dtpTransDate.Value = DateTime.Today;

            //btnPay.Enabled = false;
            cmbAgentName.Enabled = true;

            lblMessages.Text = string.Empty;
            lblMessages.ForeColor = Color.Black;
        }

        private void cmbPayMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbPayMode.Text == PayMode.BANK)
                {
                    cmbAgentName.SelectedIndex = 0;
                    cmbAgentName.Enabled = false;
                }
                else
                {
                    cmbAgentName.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgUPFAccount_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgUPFAccount.Columns[e.ColumnIndex].Name == "btnDelete")
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                               Int32 dataid = Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[1].Value);
                            Int32 transid = Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[10].Value);
                           // DeleteData(Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[1].Value));
                            DeleteData(dataid, transid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId,Int32 transid)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            UPFAccount objUPFAccount = new UPFAccount();
            objUPFAccount.Id = DataId;

            dsDeleteStatus = objUPFAccount.ReOpenUPFAccount(GlobalValues.User_PkId,transid);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadInterestPayOut();
                    //LoadAllUPFInterestPendings();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;

                    frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    objBioMetric.FetchMemberDetailsByID();
                    objBioMetric.InsertMemberDetailsToGrid();
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgUPFAccount_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            //if (e.RowIndex > 0)
            //{
            //    if (dgUPFAccount.Rows[e.RowIndex].Cells["Status"].Value.ToString().ToLower().Equals("active"))
            //    {
            //        dgUPFAccount.Rows[e.RowIndex].Cells["btnDelete"].Value = string.Empty;
            //        dgUPFAccount.Rows[e.RowIndex].Cells["btnDelete"].ReadOnly = true;
            //    }
            //}
        }

        private void dgUPFAccount_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
            {
                dgUPFAccount.Columns["btnDelete"].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            decimal Amount;
            int Days;
            decimal InterestRate = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["UPF_Interest"]);
            decimal simpleInterest = 0;
            SPARC.UPFAccount objUPFTrans = new SPARC.UPFAccount();
            objUPFTrans.fk_MemberId = GlobalValues.Member_PkId;
            DataSet dsUPF = objUPFTrans.LoadAllUPFTrans();
            
            foreach ( DataRow dr in dsUPF.Tables[0].Rows )
            {
                if (DateTime.Now > Convert.ToDateTime(dr[7]))
                {
                    // Calculate interest when no initial interest is done
                    if (dr[9].ToString() == "CRE")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        Days = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(dr[7])).TotalDays);
                        simpleInterest = Math.Round(simpleInterest + CalculateInterest(Amount, Days, InterestRate), 2);
                    }

                    if (dr[9].ToString() == "DEB" && dr[10].ToString() == "PRINCIPAL PAYOUT")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        Days = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(dr[7])).TotalDays);
                        simpleInterest = Math.Round(simpleInterest - CalculateInterest(Amount, Days, InterestRate), 2);
                    }

                    if (dr[9].ToString() == "DEB" && dr[10].ToString() == "INT PAYOUT")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        simpleInterest = Math.Round((simpleInterest - Amount), 2);
                    }
                }
            }
            lblIntAsOnDate.Text = simpleInterest.ToString();

        }

        private decimal CalculateInterest(decimal Amount, int Days, decimal InterestRate)
        {
            decimal interestAmount;
            interestAmount = (Amount*(1+(InterestRate/100)*(Days)/365))-Amount;
            return interestAmount;
        }

        private void radIntPayout_CheckedChanged(object sender, EventArgs e)
        {
            txtIntPayout.Enabled = false;
            txtIntPayout.Text = lblIntAsOnDate.Text;
            txtRemarks.Text = "INT PAYOUT";
            //txtRemarks.Enabled = false;
        }

        private void radPricipalAmount_CheckedChanged(object sender, EventArgs e)
        {
            txtIntPayout.Text = "";
            txtRemarks.Text = "PRINCIPAL PAYOUT";
            txtPrincipalWithdraw.Enabled = true;
        }

        private void txtIntPayout_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtPrincipalWithdraw_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        //private void dgUPFAccount_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        //{
        //    if (GlobalValues.User_Role.ToUpper() == UserRoles.ADMIN.ToUpper())
        //    {
        //        if (e.ColumnIndex == dgUPFAccount.Rows[e.RowIndex].Cells["btnDelete"].ColumnIndex && dgUPFAccount.Rows[e.RowIndex].Cells["Status"].Value.ToString().ToLower() == "active")
        //        {
        //            dgUPFAccount.Rows[e.RowIndex].Cells[0].ReadOnly = true;
        //        }
        //    }
        //}
    }
}