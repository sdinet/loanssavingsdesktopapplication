﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Web;
using System.IO;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace SPARC
{
    public partial class NotificationForm1 : Form
    {
        public NotificationForm1()
        {
            InitializeComponent();
        }
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);

        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public string sUserID = "";
        public string sApikey = "";
        public string sNumber = "";
        public string sSenderid = "";
        public string sMessage = "";
        public string sType = "";

        // Code to send the sms to entered phone number
        public static String GetResponse(String sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sResponse = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
                return sResponse;
            }
            catch (Exception)
            {
                return "";
            }


            finally
            {
                request.Abort();
                request = null;
            }


        }

        private void NotificationForm1_Load(object sender, EventArgs e)
        {
            txtphn1.MaxLength = 10;
        }

       // calling the sms sending method and specifying the sms horizon api key details for sending the sms
        private void btnSend_Click(object sender, EventArgs e)
        {
            sUserID = "abacusindia";
            sApikey = "WkVWpJ38KXypKeDD5YxB";
            sNumber = txtphn1.Text;

            sSenderid = "ABACUS";
            sMessage = textBox1.Text;
            sType = "txt";


            string sURL = "http://smshorizon.co.in/api/sendsms.php?user=" + sUserID + "&apikey=" + sApikey + "&mobile=" + sNumber + "&senderid=" + sSenderid + "&message=" + sMessage + "&type=" + sType + " ";

            if (txtphn1.Text == "" && txtphn1.Text.Trim().Length < 10 && textBox1.Text == "")
            {
                MessageBox.Show("Please Enter The Required Fields : Mobile Number, Transaction Details");
            }
            else if (txtphn1.Text == "")
            {
                MessageBox.Show("Please Enter The Mobile Number To Send SMS");
            }
            else if (txtphn1.Text.Trim().Length < 10)
            {
                MessageBox.Show("The Entered Mobile Number Should Be 10 In Number");
                txtphn1.Clear();

            }
            else if (textBox1.Text == "")
            {
                MessageBox.Show("Please Generate Transaction Details To Send SMS");
            }

            else
            {
                GetResponse(sURL);
                MessageBox.Show("Message sent successfully to number " + sNumber + " ");
                ClearScreen();
            }
        }

        private void ClearScreen()
        {
            txtphn1.Text = String.Empty;
            textBox1.Text = String.Empty;
            dtpTransDate.ResetText();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_GenerateDetails_Click(object sender, EventArgs e)
        {
            // code to format the date value
            DateTime dt = this.dtpTransDate.Value.Date;
            string str = string.Format("dd-MM-yyyy");
            string ab = dt.ToString(str);

            // Query to fetch the totaltransaction amount of savings, withdrawal, repayment and new loan from DailySavingsRepayment and DailyLoanWithdrawal table
            //using (SqlConnection con = new SqlConnection("Data Source=3J9-PC;User Id=sa; Password = admin@123;Initial Catalog=Loans_and_savings_sajjanRao2;Integrated Security=True"))
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                using (SqlCommand cmd1 = new SqlCommand("select case when SUM(Amount) is null then 0 else SUM(Amount) end as 'TotalSavings' from [dbo].[DailySavingsRepayment] where Type = 'Savings' and  TransactionDate = '" + dt + "' ; ", con))
                {
                    SqlDataReader dr = cmd1.ExecuteReader();
                    if (dr.Read())
                    {
                        textBox1.Text = "Transaction Date: " + ab + ", Transaction Details: " + " Savings: " + dr["TotalSavings"].ToString();

                    }
                    dr.Close();
                }

                //using (SqlCommand cmd2 = new SqlCommand("select SUM(Amount) as 'Totalrepayment' from [dbo].[DailySavingsRepayment] where Type = 'Repayment' and TransactionDate = '" + dtpTransDate.Value.ToShortDateString() + "' ;", con))
                using (SqlCommand cmd2 = new SqlCommand("select case when SUM(Amount) is null then 0 else SUM(Amount) end as 'Totalrepayment'  from [dbo].[DailySavingsRepayment] where Type = 'Repayment' and TransactionDate = '" + dt + "' ;", con))
                {
                    SqlDataReader dr = cmd2.ExecuteReader();
                    if (dr.Read())
                    {
                        textBox1.Text += ", Repayment: " + dr["Totalrepayment"].ToString();

                    }
                    dr.Close();
                }
                //using (SqlCommand cmd3 = new SqlCommand("select SUM(Amount) as 'TotalWithdrawal' from [dbo].[DailyLoanWithdrawal] where Type = 'Withdrawal' and TransactionDate = '" + dtpTransDate.Value.Date.ToString("yyyy-MM-dd") + "' ; ", con))
                using (SqlCommand cmd3 = new SqlCommand("select case when SUM(Amount) is null then 0 else SUM(Amount) end as 'TotalWithdrawal' from [dbo].[DailyLoanWithdrawal] where Type = 'Withdrawal' and TransactionDate = '" + dt + "' ; ", con))
                {
                    SqlDataReader dr = cmd3.ExecuteReader();
                    if (dr.Read())
                    {
                        textBox1.Text += ", Withdrawal: " + dr["TotalWithdrawal"].ToString();

                    }
                    dr.Close();
                }
                using (SqlCommand cmd4 = new SqlCommand("select case when SUM(Amount) is null then 0 else SUM(Amount) end as 'Totalnewloan' from [dbo].[DailyLoanWithdrawal] where Type = 'New Loan' and TransactionDate  = '" + dt + "';  ", con))
                {
                    SqlDataReader dr = cmd4.ExecuteReader();
                    if (dr.Read())
                    {
                        textBox1.Text += ", New Loan: " + dr["Totalnewloan"].ToString() + ".";

                    }
                    dr.Close();
                }
                con.Close();

            }
        }
        
        // validation code for the textbox
        private void txtphn1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;

                }
            }
            catch (Exception ex)
            {
                //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            ClearScreen();
        }
    }
}
