﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;

namespace SPARC.Master_Pages
{
    public partial class frmAgentManager : Form
    {
        Agent globjAgent = new Agent();
        public frmAgentManager()
        {
            InitializeComponent();
        }

        private void frmAgentManager_Load(object sender, EventArgs e)
        {
            try
            {
                ResetFormHeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Messages.ERRORMESSAGE);
            }
        }

        private void btnAddNewAgent_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                btnUpdate.Text = Constants.SAVE;
                ResetFormHeight();
                globjAgent = new Agent();

                txtAgentName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Messages.ERRORMESSAGE);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                bool bNew = false;
                if (globjAgent.AgentID == 0)
                {
                    bNew = true;
                    globjAgent.Name = txtAgentName.Text.Trim();
                    globjAgent.ContactNumber = txtContactNumber.Text.Trim();
                }
                int AgentID = globjAgent.CreateAgent(globjAgent);
                    
                if (AgentID > 0)
                {
                    if (bNew)
                        MessageBox.Show(Messages.AGENTCREATED);
                    else
                        MessageBox.Show(Messages.SAVED_SUCCESS);
                }
                else
                {
                    MessageBox.Show(Messages.ERRAGENTSAVE);
                }

                ResetFormHeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Messages.ERRORMESSAGE);
            }
        }

        private void ResetFormHeight()
        {

            if (this.Height == 400)
                this.Size = new Size(540, 534);
            else
                this.Size = new Size(540, 400);
                
        }

        private void ClearControls()
        {
            txtAgentName.Text = String.Empty;
            txtContactNumber.Text = String.Empty;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
            btnUpdate.Text = Constants.UPDATE;
            ResetFormHeight();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }
    }
}
