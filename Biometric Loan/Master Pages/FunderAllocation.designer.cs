﻿namespace SPARC.Master_Pages
{
    partial class FunderAllocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgFundsRecieved = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbSelectFunder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.Center = new System.Windows.Forms.Label();
            this.CenterCombo = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Dlbl = new System.Windows.Forms.Label();
            this.Albl = new System.Windows.Forms.Label();
            this.Ntlbl = new System.Windows.Forms.Label();
            this.comboBoxcurrency = new System.Windows.Forms.ComboBox();
            this.Amttxt = new System.Windows.Forms.TextBox();
            this.Notestxt = new System.Windows.Forms.TextBox();
            this.txtTransactionId = new System.Windows.Forms.TextBox();
            this.txtActualAmt = new System.Windows.Forms.TextBox();
            this.txtAllocatedAmt = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btnclose2 = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.txtfundername = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgFundsRecieved)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dgFundsRecieved
            // 
            this.dgFundsRecieved.AllowUserToAddRows = false;
            this.dgFundsRecieved.AllowUserToDeleteRows = false;
            this.dgFundsRecieved.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFundsRecieved.Location = new System.Drawing.Point(12, 53);
            this.dgFundsRecieved.Name = "dgFundsRecieved";
            this.dgFundsRecieved.ReadOnly = true;
            this.dgFundsRecieved.Size = new System.Drawing.Size(737, 206);
            this.dgFundsRecieved.TabIndex = 16;
            this.dgFundsRecieved.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFundsRecieved_CellContentClick);
            this.dgFundsRecieved.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgFundsRecieved_RowHeaderMouseClick);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cmbSelectFunder);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(12, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 35);
            this.panel3.TabIndex = 20;
            // 
            // cmbSelectFunder
            // 
            this.cmbSelectFunder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelectFunder.FormattingEnabled = true;
            this.cmbSelectFunder.Location = new System.Drawing.Point(296, 7);
            this.cmbSelectFunder.Name = "cmbSelectFunder";
            this.cmbSelectFunder.Size = new System.Drawing.Size(121, 21);
            this.cmbSelectFunder.TabIndex = 1;
            this.cmbSelectFunder.SelectedIndexChanged += new System.EventHandler(this.cmbSelectFunder_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Funder";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(211, 434);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(324, 434);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Center
            // 
            this.Center.AutoSize = true;
            this.Center.Location = new System.Drawing.Point(72, 287);
            this.Center.Name = "Center";
            this.Center.Size = new System.Drawing.Size(38, 13);
            this.Center.TabIndex = 35;
            this.Center.Text = "Center";
            // 
            // CenterCombo
            // 
            this.CenterCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CenterCombo.FormattingEnabled = true;
            this.CenterCombo.Location = new System.Drawing.Point(144, 284);
            this.CenterCombo.Name = "CenterCombo";
            this.CenterCombo.Size = new System.Drawing.Size(121, 21);
            this.CenterCombo.TabIndex = 2;
            this.CenterCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(513, 287);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // Dlbl
            // 
            this.Dlbl.AutoSize = true;
            this.Dlbl.Location = new System.Drawing.Point(457, 292);
            this.Dlbl.Name = "Dlbl";
            this.Dlbl.Size = new System.Drawing.Size(30, 13);
            this.Dlbl.TabIndex = 38;
            this.Dlbl.Text = "Date";
            // 
            // Albl
            // 
            this.Albl.AutoSize = true;
            this.Albl.Location = new System.Drawing.Point(72, 345);
            this.Albl.Name = "Albl";
            this.Albl.Size = new System.Drawing.Size(92, 13);
            this.Albl.TabIndex = 39;
            this.Albl.Text = "Allocation Amount";
            // 
            // Ntlbl
            // 
            this.Ntlbl.AutoSize = true;
            this.Ntlbl.Location = new System.Drawing.Point(457, 345);
            this.Ntlbl.Name = "Ntlbl";
            this.Ntlbl.Size = new System.Drawing.Size(35, 13);
            this.Ntlbl.TabIndex = 40;
            this.Ntlbl.Text = "Notes";
            // 
            // comboBoxcurrency
            // 
            this.comboBoxcurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxcurrency.FormattingEnabled = true;
            this.comboBoxcurrency.Location = new System.Drawing.Point(170, 342);
            this.comboBoxcurrency.Name = "comboBoxcurrency";
            this.comboBoxcurrency.Size = new System.Drawing.Size(74, 21);
            this.comboBoxcurrency.TabIndex = 4;
            // 
            // Amttxt
            // 
            this.Amttxt.Location = new System.Drawing.Point(268, 344);
            this.Amttxt.Name = "Amttxt";
            this.Amttxt.ShortcutsEnabled = false;
            this.Amttxt.Size = new System.Drawing.Size(100, 20);
            this.Amttxt.TabIndex = 5;
            this.Amttxt.TextChanged += new System.EventHandler(this.Amttxt_TextChanged);
            this.Amttxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Amttxt_KeyPress);
            // 
            // Notestxt
            // 
            this.Notestxt.Location = new System.Drawing.Point(513, 338);
            this.Notestxt.Name = "Notestxt";
            this.Notestxt.Size = new System.Drawing.Size(100, 20);
            this.Notestxt.TabIndex = 6;
            this.Notestxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Notestxt_KeyPress);
            // 
            // txtTransactionId
            // 
            this.txtTransactionId.Location = new System.Drawing.Point(2, 390);
            this.txtTransactionId.Multiline = true;
            this.txtTransactionId.Name = "txtTransactionId";
            this.txtTransactionId.ReadOnly = true;
            this.txtTransactionId.Size = new System.Drawing.Size(10, 10);
            this.txtTransactionId.TabIndex = 44;
            // 
            // txtActualAmt
            // 
            this.txtActualAmt.Location = new System.Drawing.Point(38, 408);
            this.txtActualAmt.Name = "txtActualAmt";
            this.txtActualAmt.ReadOnly = true;
            this.txtActualAmt.Size = new System.Drawing.Size(29, 20);
            this.txtActualAmt.TabIndex = 45;
            // 
            // txtAllocatedAmt
            // 
            this.txtAllocatedAmt.Location = new System.Drawing.Point(38, 434);
            this.txtAllocatedAmt.Name = "txtAllocatedAmt";
            this.txtAllocatedAmt.ReadOnly = true;
            this.txtAllocatedAmt.Size = new System.Drawing.Size(29, 20);
            this.txtAllocatedAmt.TabIndex = 46;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(2, 467);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(947, 199);
            this.dataGridView2.TabIndex = 47;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridView2.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_RowHeaderMouseClick);
            // 
            // btnclose2
            // 
            this.btnclose2.Location = new System.Drawing.Point(387, 672);
            this.btnclose2.Name = "btnclose2";
            this.btnclose2.Size = new System.Drawing.Size(75, 23);
            this.btnclose2.TabIndex = 8;
            this.btnclose2.Text = "Close";
            this.btnclose2.UseVisualStyleBackColor = true;
            this.btnclose2.Click += new System.EventHandler(this.btnclose2_Click);
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(513, 672);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(75, 23);
            this.btnclear.TabIndex = 9;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // txtfundername
            // 
            this.txtfundername.Location = new System.Drawing.Point(73, 408);
            this.txtfundername.Name = "txtfundername";
            this.txtfundername.ReadOnly = true;
            this.txtfundername.Size = new System.Drawing.Size(35, 20);
            this.txtfundername.TabIndex = 51;
            // 
            // FunderAllocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 695);
            this.ControlBox = false;
            this.Controls.Add(this.txtfundername);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnclose2);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.txtAllocatedAmt);
            this.Controls.Add(this.txtActualAmt);
            this.Controls.Add(this.txtTransactionId);
            this.Controls.Add(this.Notestxt);
            this.Controls.Add(this.Amttxt);
            this.Controls.Add(this.comboBoxcurrency);
            this.Controls.Add(this.Ntlbl);
            this.Controls.Add(this.Albl);
            this.Controls.Add(this.Dlbl);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.CenterCombo);
            this.Controls.Add(this.Center);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.dgFundsRecieved);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FunderAllocation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FunderAllocation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgFundsRecieved)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgFundsRecieved;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbSelectFunder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label Center;
        private System.Windows.Forms.ComboBox CenterCombo;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label Dlbl;
        private System.Windows.Forms.Label Albl;
        private System.Windows.Forms.Label Ntlbl;
        private System.Windows.Forms.ComboBox comboBoxcurrency;
        private System.Windows.Forms.TextBox Amttxt;
        private System.Windows.Forms.TextBox Notestxt;
        private System.Windows.Forms.TextBox txtTransactionId;
        private System.Windows.Forms.TextBox txtActualAmt;
        private System.Windows.Forms.TextBox txtAllocatedAmt;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnclose2;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.TextBox txtfundername;

    }
}