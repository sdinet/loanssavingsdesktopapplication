﻿namespace SPARC.Master_Pages
{
    partial class MasterDataManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSAcc = new System.Windows.Forms.Panel();
            this.cmbParentFilter = new System.Windows.Forms.ComboBox();
            this.lblSearchParent = new System.Windows.Forms.Label();
            this.txtAbbreviation = new System.Windows.Forms.TextBox();
            this.lblAbbreviation = new System.Windows.Forms.Label();
            this.txtDisplayOrder = new System.Windows.Forms.TextBox();
            this.lblDisplayOrder = new System.Windows.Forms.Label();
            this.cmbParent = new System.Windows.Forms.ComboBox();
            this.lblParent = new System.Windows.Forms.Label();
            this.btnNewData = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.dgCodeValue = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fk_ParentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Abbreviation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.txtCodeValueVal = new System.Windows.Forms.TextBox();
            this.lblMessages = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCodeType = new System.Windows.Forms.ComboBox();
            this.lblCodeType = new System.Windows.Forms.Label();
            this.lblCodeValue = new System.Windows.Forms.Label();
            this.pnlSAcc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCodeValue)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSAcc
            // 
            this.pnlSAcc.Controls.Add(this.cmbParentFilter);
            this.pnlSAcc.Controls.Add(this.lblSearchParent);
            this.pnlSAcc.Controls.Add(this.txtAbbreviation);
            this.pnlSAcc.Controls.Add(this.lblAbbreviation);
            this.pnlSAcc.Controls.Add(this.txtDisplayOrder);
            this.pnlSAcc.Controls.Add(this.lblDisplayOrder);
            this.pnlSAcc.Controls.Add(this.cmbParent);
            this.pnlSAcc.Controls.Add(this.lblParent);
            this.pnlSAcc.Controls.Add(this.btnNewData);
            this.pnlSAcc.Controls.Add(this.lblInfo);
            this.pnlSAcc.Controls.Add(this.dgCodeValue);
            this.pnlSAcc.Controls.Add(this.txtCodeValueVal);
            this.pnlSAcc.Controls.Add(this.lblMessages);
            this.pnlSAcc.Controls.Add(this.btnClose);
            this.pnlSAcc.Controls.Add(this.btnSave);
            this.pnlSAcc.Controls.Add(this.cmbCodeType);
            this.pnlSAcc.Controls.Add(this.lblCodeType);
            this.pnlSAcc.Controls.Add(this.lblCodeValue);
            this.pnlSAcc.Location = new System.Drawing.Point(8, 11);
            this.pnlSAcc.Name = "pnlSAcc";
            this.pnlSAcc.Size = new System.Drawing.Size(645, 566);
            this.pnlSAcc.TabIndex = 2;
            // 
            // cmbParentFilter
            // 
            this.cmbParentFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParentFilter.FormattingEnabled = true;
            this.cmbParentFilter.Location = new System.Drawing.Point(103, 278);
            this.cmbParentFilter.Name = "cmbParentFilter";
            this.cmbParentFilter.Size = new System.Drawing.Size(195, 21);
            this.cmbParentFilter.TabIndex = 8;
            this.cmbParentFilter.SelectedIndexChanged += new System.EventHandler(this.cmbParentFilter_SelectedIndexChanged);
            // 
            // lblSearchParent
            // 
            this.lblSearchParent.AutoSize = true;
            this.lblSearchParent.Location = new System.Drawing.Point(12, 281);
            this.lblSearchParent.Name = "lblSearchParent";
            this.lblSearchParent.Size = new System.Drawing.Size(77, 13);
            this.lblSearchParent.TabIndex = 112;
            this.lblSearchParent.Text = "Filter by Parent";
            // 
            // txtAbbreviation
            // 
            this.txtAbbreviation.Location = new System.Drawing.Point(101, 119);
            this.txtAbbreviation.MaxLength = 2;
            this.txtAbbreviation.Name = "txtAbbreviation";
            this.txtAbbreviation.Size = new System.Drawing.Size(68, 20);
            this.txtAbbreviation.TabIndex = 3;
            // 
            // lblAbbreviation
            // 
            this.lblAbbreviation.AutoSize = true;
            this.lblAbbreviation.Location = new System.Drawing.Point(11, 123);
            this.lblAbbreviation.Name = "lblAbbreviation";
            this.lblAbbreviation.Size = new System.Drawing.Size(66, 13);
            this.lblAbbreviation.TabIndex = 110;
            this.lblAbbreviation.Text = "Abbreviation";
            // 
            // txtDisplayOrder
            // 
            this.txtDisplayOrder.Location = new System.Drawing.Point(102, 146);
            this.txtDisplayOrder.MaxLength = 4;
            this.txtDisplayOrder.Name = "txtDisplayOrder";
            this.txtDisplayOrder.ShortcutsEnabled = false;
            this.txtDisplayOrder.Size = new System.Drawing.Size(68, 20);
            this.txtDisplayOrder.TabIndex = 4;
            this.txtDisplayOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDisplayOrder_KeyPress);
            // 
            // lblDisplayOrder
            // 
            this.lblDisplayOrder.AutoSize = true;
            this.lblDisplayOrder.Location = new System.Drawing.Point(12, 150);
            this.lblDisplayOrder.Name = "lblDisplayOrder";
            this.lblDisplayOrder.Size = new System.Drawing.Size(70, 13);
            this.lblDisplayOrder.TabIndex = 108;
            this.lblDisplayOrder.Text = "Display Order";
            // 
            // cmbParent
            // 
            this.cmbParent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParent.FormattingEnabled = true;
            this.cmbParent.Location = new System.Drawing.Point(101, 92);
            this.cmbParent.Name = "cmbParent";
            this.cmbParent.Size = new System.Drawing.Size(195, 21);
            this.cmbParent.TabIndex = 2;
            // 
            // lblParent
            // 
            this.lblParent.AutoSize = true;
            this.lblParent.Location = new System.Drawing.Point(10, 95);
            this.lblParent.Name = "lblParent";
            this.lblParent.Size = new System.Drawing.Size(38, 13);
            this.lblParent.TabIndex = 106;
            this.lblParent.Text = "Parent";
            // 
            // btnNewData
            // 
            this.btnNewData.Location = new System.Drawing.Point(204, 193);
            this.btnNewData.Name = "btnNewData";
            this.btnNewData.Size = new System.Drawing.Size(82, 23);
            this.btnNewData.TabIndex = 6;
            this.btnNewData.Text = "&New";
            this.btnNewData.UseVisualStyleBackColor = true;
            this.btnNewData.Click += new System.EventHandler(this.btnNewData_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(11, 313);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(139, 13);
            this.lblInfo.TabIndex = 102;
            this.lblInfo.Text = "Double click on a row to edit";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgCodeValue
            // 
            this.dgCodeValue.AllowUserToAddRows = false;
            this.dgCodeValue.AllowUserToDeleteRows = false;
            this.dgCodeValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCodeValue.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgCodeValue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCodeValue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.CodeName,
            this.fk_ParentId,
            this.ParentName,
            this.Abbreviation,
            this.DisplayOrder,
            this.btnDelete});
            this.dgCodeValue.Location = new System.Drawing.Point(7, 328);
            this.dgCodeValue.Margin = new System.Windows.Forms.Padding(0);
            this.dgCodeValue.Name = "dgCodeValue";
            this.dgCodeValue.ReadOnly = true;
            this.dgCodeValue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCodeValue.Size = new System.Drawing.Size(627, 223);
            this.dgCodeValue.TabIndex = 101;
            this.dgCodeValue.TabStop = false;
            this.dgCodeValue.VirtualMode = true;
            this.dgCodeValue.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCodeValue_CellClick);
            this.dgCodeValue.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCodeValue_CellContentDoubleClick);
            this.dgCodeValue.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCodeValue_CellDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // CodeName
            // 
            this.CodeName.DataPropertyName = "Name";
            this.CodeName.HeaderText = "Value";
            this.CodeName.Name = "CodeName";
            this.CodeName.ReadOnly = true;
            // 
            // fk_ParentId
            // 
            this.fk_ParentId.DataPropertyName = "fk_ParentId";
            this.fk_ParentId.HeaderText = "ParentID";
            this.fk_ParentId.Name = "fk_ParentId";
            this.fk_ParentId.ReadOnly = true;
            this.fk_ParentId.Visible = false;
            // 
            // ParentName
            // 
            this.ParentName.DataPropertyName = "ParentName";
            dataGridViewCellStyle1.NullValue = null;
            this.ParentName.DefaultCellStyle = dataGridViewCellStyle1;
            this.ParentName.HeaderText = "Parent";
            this.ParentName.Name = "ParentName";
            this.ParentName.ReadOnly = true;
            // 
            // Abbreviation
            // 
            this.Abbreviation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Abbreviation.DataPropertyName = "Abbreviation";
            this.Abbreviation.HeaderText = "Abbreviation";
            this.Abbreviation.Name = "Abbreviation";
            this.Abbreviation.ReadOnly = true;
            this.Abbreviation.Width = 91;
            // 
            // DisplayOrder
            // 
            this.DisplayOrder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DisplayOrder.DataPropertyName = "DisplayOrder";
            this.DisplayOrder.HeaderText = "Display Order";
            this.DisplayOrder.Name = "DisplayOrder";
            this.DisplayOrder.ReadOnly = true;
            this.DisplayOrder.Width = 95;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            this.btnDelete.Width = 5;
            // 
            // txtCodeValueVal
            // 
            this.txtCodeValueVal.Location = new System.Drawing.Point(101, 66);
            this.txtCodeValueVal.MaxLength = 50;
            this.txtCodeValueVal.Name = "txtCodeValueVal";
            this.txtCodeValueVal.Size = new System.Drawing.Size(195, 20);
            this.txtCodeValueVal.TabIndex = 1;
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(8, 235);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 87;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(303, 193);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(101, 193);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbCodeType
            // 
            this.cmbCodeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodeType.FormattingEnabled = true;
            this.cmbCodeType.Location = new System.Drawing.Point(222, 18);
            this.cmbCodeType.Name = "cmbCodeType";
            this.cmbCodeType.Size = new System.Drawing.Size(195, 21);
            this.cmbCodeType.TabIndex = 0;
            this.cmbCodeType.SelectedIndexChanged += new System.EventHandler(this.cmbCodeType_SelectedIndexChanged);
            // 
            // lblCodeType
            // 
            this.lblCodeType.AutoSize = true;
            this.lblCodeType.Location = new System.Drawing.Point(11, 18);
            this.lblCodeType.Name = "lblCodeType";
            this.lblCodeType.Size = new System.Drawing.Size(178, 13);
            this.lblCodeType.TabIndex = 77;
            this.lblCodeType.Text = "Select the master data to add or edit";
            // 
            // lblCodeValue
            // 
            this.lblCodeValue.AutoSize = true;
            this.lblCodeValue.Location = new System.Drawing.Point(11, 70);
            this.lblCodeValue.Name = "lblCodeValue";
            this.lblCodeValue.Size = new System.Drawing.Size(34, 13);
            this.lblCodeValue.TabIndex = 75;
            this.lblCodeValue.Text = "Value";
            // 
            // MasterDataManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 589);
            this.ControlBox = false;
            this.Controls.Add(this.pnlSAcc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MasterDataManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.MasterDataManager_Load);
            this.pnlSAcc.ResumeLayout(false);
            this.pnlSAcc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCodeValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSAcc;
        private System.Windows.Forms.Button btnNewData;
        private System.Windows.Forms.Label lblInfo;
        public System.Windows.Forms.DataGridView dgCodeValue;
        private System.Windows.Forms.TextBox txtCodeValueVal;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbCodeType;
        private System.Windows.Forms.Label lblCodeType;
        private System.Windows.Forms.Label lblCodeValue;
        private System.Windows.Forms.ComboBox cmbParent;
        private System.Windows.Forms.Label lblParent;
        private System.Windows.Forms.TextBox txtAbbreviation;
        private System.Windows.Forms.Label lblAbbreviation;
        private System.Windows.Forms.TextBox txtDisplayOrder;
        private System.Windows.Forms.Label lblDisplayOrder;
        private System.Windows.Forms.ComboBox cmbParentFilter;
        private System.Windows.Forms.Label lblSearchParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fk_ParentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abbreviation;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayOrder;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
    }
}