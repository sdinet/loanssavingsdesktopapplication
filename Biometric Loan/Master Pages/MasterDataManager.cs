﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;
using System.Configuration;
using System.Data.SqlClient;

namespace SPARC.Master_Pages
{
    public partial class MasterDataManager : Form
    {
        int i;
        int j;
        DataSet ds = new DataSet();
        DataSet dj = new DataSet();
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private static string ConnectionStringSync = ConfigurationManager.AppSettings["ConnSync"];
        string[] selectQueries = new string[2];
        string[] insertUpdateQueries = new string[2];
        string[] syncdataTable = new string[2];
        private Int32 iSavDepID = 0;

        public MasterDataManager()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbObject, bool bAddNewRow)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(true);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select All--";
                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbObject.DisplayMember = "Name";
                cmbObject.ValueMember = "Id";
                cmbObject.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void FillCodeTypes(bool bAddNewRow)
        {
            CodeType objCodeType = new CodeType();
            objCodeType.Id = null;

            DataSet dsCodeTypes = objCodeType.FetchCodeType();
            if (dsCodeTypes != null && dsCodeTypes.Tables.Count > 0 && dsCodeTypes.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsCodeTypes.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsCodeTypes.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbCodeType.DisplayMember = "Code";
                cmbCodeType.ValueMember = "Id";
                cmbCodeType.DataSource = dsCodeTypes.Tables[0];
            }
        }

        private void FillCodeValues(int CodeTypeId, Int32 ParentId)
        {
            CodeValue objCodeValue = new CodeValue();
            objCodeValue.fk_CodeTypeId = CodeTypeId;
            if (ParentId == 0)
                objCodeValue.fk_ParentId = null;
            else
                objCodeValue.fk_ParentId = ParentId;

            DataSet dsCodeTypes = objCodeValue.FetchCodeValue();
            if (dsCodeTypes != null && dsCodeTypes.Tables.Count > 0 && dsCodeTypes.Tables[0].Rows.Count > 0)
            {
                dgCodeValue.AutoGenerateColumns = false;
                dgCodeValue.DataSource = dsCodeTypes.Tables[0];
            }
            else
            {
                dgCodeValue.DataSource = null;
            }
        }

        private void MasterDataManager_Load(object sender, EventArgs e)
        {
            try
            {                               
               FillCodeTypes(false);
               this.Text = Constants.MASTERDATAMANAGER;
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnNewData_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void ClearControls()
        {
            iSavDepID = 0;

            txtAbbreviation.Text = string.Empty;
            txtCodeValueVal.Text = string.Empty;
            txtDisplayOrder.Text = string.Empty;
            cmbParent.SelectedIndex = -1;            
            lblMessages.Text = string.Empty;
            lblMessages.ForeColor = Color.Black;

            btnSave.Enabled = true;
            btnSave.Text = "Save";

            if (!(Convert.ToInt32(cmbCodeType.SelectedValue) > 1 && Convert.ToInt32(cmbCodeType.SelectedValue) < 6))
            {
                cmbParent.Enabled = false;
                cmbParentFilter.Enabled = false;
            }
            else
            {
                cmbParentFilter.Enabled = true;
                cmbParent.Enabled = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {

                    string strMessage = DoValidations();
                    if (strMessage.Length > 0)
                    {
                        lblMessages.Text = strMessage;
                    }
                    else
                    {
                        lblMessages.Text = string.Empty;

                        CodeValue objCodeValue = new CodeValue();
                        objCodeValue.Id = iSavDepID;
                        objCodeValue.Abbreviation = txtAbbreviation.Text;
                        objCodeValue.DisplayOrder = Convert.ToInt16(txtDisplayOrder.Text);
                        objCodeValue.fk_CodeTypeId = Convert.ToInt16(cmbCodeType.SelectedValue);
                        if (cmbParent.Enabled)
                        {
                            objCodeValue.fk_ParentId = Convert.ToInt32(cmbParent.SelectedValue);
                        }
                        else
                        {
                            objCodeValue.fk_ParentId = 0;
                        }
                        objCodeValue.Name = txtCodeValueVal.Text;
                        objCodeValue.Createdby = GlobalValues.User_PkId;
                        objCodeValue.Updatedby = GlobalValues.User_PkId;

                        DataSet dsCodeValue = null;
                        if (iSavDepID == 0)
                            dsCodeValue = objCodeValue.SaveCodeValue();
                        else
                            dsCodeValue = objCodeValue.UpdateCodeValue();

                        try
                        {
                            // edited by archana--start
                            DBSyncfull();
                            //edited by archana--end
                        }
                        catch (Exception ex)
                        {
                        }
                        if (dsCodeValue != null && dsCodeValue.Tables.Count > 0 && dsCodeValue.Tables[0].Rows.Count > 0)
                        {
                            if (iSavDepID == 0)
                            {
                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;

                                iSavDepID = Convert.ToInt32(dsCodeValue.Tables[0].Rows[0]["Id"]);
                                btnSave.Text = Constants.UPDATE;
                            }
                            else
                            {
                                lblMessages.Text = Messages.UPDATED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                        }
                        else
                        {
                            Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                        }

                        FillCodeValues(Convert.ToInt32(cmbCodeType.SelectedValue), Convert.ToInt32(cmbParentFilter.SelectedValue));
                    }
                }
                else
                {
                    MessageBox.Show("Please check your Internet connection.This data will be saved and synced automatically.Hence Internet connection is mandatory");
                }
            }
           
            
            catch (SqlException exsql)
            {
                Messages.ShowMessage(Messages.Dataexist, Messages.MsgType.failure, exsql);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;
            lblMessages.Text = string.Empty;
            CodeValue objCodeValue = new CodeValue();
            objCodeValue.Id = DataId;

            dsDeleteStatus = objCodeValue.DeleteCodeValue(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    FillCodeValues(Convert.ToInt32(cmbCodeType.SelectedValue), Convert.ToInt32(cmbParentFilter.SelectedValue));
                    ClearControls();
                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;          
                CodeValue ObjAbcodevalue = new CodeValue();
                ObjAbcodevalue.fk_CodeTypeId = Convert.ToInt32(cmbCodeType.SelectedValue);
                ObjAbcodevalue.Abbreviation = txtAbbreviation.Text;
                ObjAbcodevalue.Id = iSavDepID;
                DataSet dsAbCheck = new DataSet();
                dsAbCheck = ObjAbcodevalue.FetchAbbreviation();

                if (dsAbCheck.Tables.Count > 0 && dsAbCheck.Tables[0].Rows.Count > 0)
                {
                    ErrorMsg = Messages.ABBREVATIONEXIST + Messages.COMMAWITHSPACE;
                }            
            if (txtCodeValueVal.Text.Trim() == string.Empty)
            {
                ErrorMsg += Messages.CODEVALUE + Messages.COMMAWITHSPACE;
            }
            if (cmbParent.Enabled && cmbParent.Text == string.Empty)
            {
                ErrorMsg += Messages.PARENT + Messages.COMMAWITHSPACE;
            }
            if (Convert.ToInt16(cmbCodeType.SelectedValue)<=5 && txtAbbreviation.Text.Trim() == string.Empty)
            {
                ErrorMsg += Messages.ABBREVIATION + Messages.COMMAWITHSPACE;
            }
            if (txtDisplayOrder.Text.Trim() == string.Empty)
            {
                ErrorMsg += Messages.DISPLAYORDER + Messages.COMMAWITHSPACE;
            }
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void txtDisplayOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar)) // && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgCodeValue_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                GridEventHandler(e);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbParentFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                FillCodeValues(Convert.ToInt32(cmbCodeType.SelectedValue), Convert.ToInt32(cmbParentFilter.SelectedValue));
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbCodeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControls();

            FillCodeValues(Convert.ToInt32(cmbCodeType.SelectedValue), 0);
            FillCombos(Convert.ToInt32(cmbCodeType.SelectedValue)-1, 0, cmbParent,false);
            FillCombos(Convert.ToInt32(cmbCodeType.SelectedValue) - 1, 0, cmbParentFilter,true);

            if (!(Convert.ToInt32(cmbCodeType.SelectedValue) > 1 && Convert.ToInt32(cmbCodeType.SelectedValue) < 6))
            {
                cmbParent.Enabled = false;
                cmbParentFilter.Enabled = false;

                cmbParent.DataSource = null;
                cmbParentFilter.DataSource = null;
            }
            else
            {
                cmbParentFilter.Enabled = true;
                cmbParent.Enabled = true;
            }
        }

        private void dgCodeValue_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                GridEventHandler(e);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void GridEventHandler(DataGridViewCellEventArgs e)
        {
            try
            {
                ClearControls();

                if (e.RowIndex >= 0)
                {
                    cmbParent.Text = dgCodeValue.Rows[e.RowIndex].Cells["ParentName"].Value.ToString();
                    txtCodeValueVal.Text = dgCodeValue.Rows[e.RowIndex].Cells["CodeName"].Value.ToString();
                    txtAbbreviation.Text = dgCodeValue.Rows[e.RowIndex].Cells["Abbreviation"].Value.ToString();
                    txtDisplayOrder.Text = dgCodeValue.Rows[e.RowIndex].Cells["DisplayOrder"].Value.ToString();

                    btnSave.Text = Constants.UPDATE;
                    iSavDepID = Convert.ToInt32(dgCodeValue.Rows[e.RowIndex].Cells["Id"].Value);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgCodeValue_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgCodeValue.Columns[e.ColumnIndex].Index == 6)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgCodeValue.CurrentRow.Cells[0].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        // edited by archana--start
        private void DBSyncfull()
        {


            SqlDataAdapter da;
            //create a array of sync data
            CreateSyncData();

            SqlConnection conn = new SqlConnection(ConnectionString);
            conn.Open();
            SqlConnection _connsync = new SqlConnection(ConnectionStringSync);
            _connsync.Open();

            try
            {
                for (i = 0; i < selectQueries.Length; i++)
                {
                    da = new SqlDataAdapter(selectQueries[i], conn);
                    cleanDataset();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DBSync(i, _connsync);

                    }


                }
            }
            catch (Exception ex)
            {
            }
        }
        private void updateLocalTable(int loc, int ser, string table)
        {
            //code to update local table with server id

            SqlConnection Con = new SqlConnection(ConnectionString);
            Con.Open();
            string sql = "Update  " + table + " set Server_Id=" + ser + ",Sync_type=NULL where id=" + loc;
            SqlCommand cmd = new SqlCommand(sql, Con);

            cmd.ExecuteNonQuery();
            Con.Close();
        }
        private void cleanDataset()
        {
            // removing items from a collection
            while (ds.Tables.Count > 0)
            {
                DataTable table = ds.Tables[0];
                if (ds.Tables.CanRemove(table))
                {
                    ds.Tables.Remove(table);
                }
            }

        }
        public void CreateSyncData()
        {

            selectQueries[0] = "Select  a.Id as localId,a.Name, a.fk_CodeTypeId, b.Server_Id as fk_ParentId, a.DisplayOrder, a.Abbreviation, a.Createdby,a. Updatedby from CodeValue a left join CodeValue b on a.fk_ParentId =b.Id where a.Sync_Type='N'";
            selectQueries[1] = "Select  a.Id as localId,a.Server_Id as ID,a.Name, a.fk_CodeTypeId,b.Server_Id as  fk_ParentId, a.DisplayOrder, a.Abbreviation,  a.Updatedby from CodeValue a  left join CodeValue b on a.fk_ParentId =b.Id  where a.Sync_Type='U'";

            insertUpdateQueries[0] = "usp_CodeValueInsert";
            insertUpdateQueries[1] = "usp_CodeValueUpdate";

            syncdataTable[0] = "CodeValue";
            syncdataTable[1] = "CodeValue";
        }
        private void DBSync(int i, SqlConnection connsync)
        {
            SqlCommand ccmd = new SqlCommand(insertUpdateQueries[i], connsync);
            SqlCommand ccmd1 = new SqlCommand(insertUpdateQueries[i], connsync);

            ccmd.CommandType = CommandType.StoredProcedure;
            ccmd1.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(ccmd1);
            Int32 serverId;
            Int32 localId;

            for (j = 0; j < ds.Tables[0].Rows.Count; j++)
            {

                SqlTransaction servertxn = connsync.BeginTransaction();


                ccmd.Parameters.Clear();
                //capture the local id
                localId = Convert.ToInt32(ds.Tables[0].Rows[j][0]);
                //loop through the columns of each row and insert the values into db
                for (int kk = 0; kk < ds.Tables[0].Columns.Count - 1; kk++)
                {
                    SqlParameter prm = new SqlParameter(ccmd1.Parameters[kk + 1].ToString(), ccmd1.Parameters[kk + 1].SqlDbType);
                    prm.Value = ds.Tables[0].Rows[j][kk + 1];
                    ccmd.Parameters.Add(prm);



                }
                ccmd.Transaction = servertxn;
                //capture the generated identity in the server
                serverId = Convert.ToInt32(ccmd.ExecuteScalar());

                //update the local table with the serverId which will be used for updates
                updateLocalTable(localId, serverId, syncdataTable[i]);
                servertxn.Commit();
                servertxn.Dispose();


                MessageBox.Show("CodeValue Record is Synced successfully!");
            }
            //  this.Close();
        }

       
        //edited by archana--end
    }
}
