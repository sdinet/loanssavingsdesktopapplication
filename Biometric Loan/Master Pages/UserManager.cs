﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;

namespace SPARC.Master_Pages
{
    public partial class frmUserManager : Form
    {
        private Int64 iUserID = 0;

        public frmUserManager()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void frmUserManager_Load(object sender, EventArgs e)
        {
            this.Text = Constants.USERMANAGER;
            cmbHub.Visible = false;
            cmbCountry.Visible = false;
            cmbState.Visible = false;
            cmbDistrict.Visible = false;
            FillCombos(Convert.ToInt32(LocationCode.UserRole), 0, cmbUserRole); //Fill UserRole
            FillCombos(Convert.ToInt32(LocationCode.UserRole), 0, cmbSearchRole); //Fill SearchRole
            cmbSearchRole.SelectedIndex = 0;
            cmbUserRole.SelectedIndex = 0;

            ClearControls();
            LoadUsers();
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadUsers();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            Users ObjUnUsers = new Users();
            ObjUnUsers.UserName = txtUserName.Text.Trim();
            ObjUnUsers.UserID = iUserID;
            DataSet dsUnCheck = new DataSet();
            dsUnCheck = ObjUnUsers.FetchUsers();
            if (dsUnCheck.Tables.Count > 0 && dsUnCheck.Tables[0].Rows.Count > 0)
            {
                ErrorMsg = Messages.MULTIPLEUSERNAME + Messages.COMMAWITHSPACE;
            }
            if (txtName.Text.Trim().Length == 0)
                ErrorMsg = Messages.FULLNAME + Messages.COMMAWITHSPACE;
            if (txtContactNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.CONTACTNO + Messages.COMMAWITHSPACE;
            if (txtUserName.Text.Trim().Length == 0)
                ErrorMsg += Messages.USERNAME + Messages.COMMAWITHSPACE;
            if (txtPassword.Text.Trim().Length == 0)
                ErrorMsg += Messages.PASSWORD + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;

            txtName.Text = String.Empty;
            txtContactNo.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            chkAgent.Checked = false;

            iUserID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
        }

        private void LoadUsers()
        {
            //Loads the grid
            Users objUsers = new Users();
            objUsers.Role =  cmbSearchRole.SelectedIndex == 0? null : cmbSearchRole.Text;
            objUsers.FullName = txtSearchName.Text.Trim().Length == 0 ? null : txtSearchName.Text.Trim();

            DataSet dsUsers = objUsers.LoadUsers();
            if (dsUsers != null && dsUsers.Tables.Count > 0 && dsUsers.Tables[0].Rows.Count > 0)
            {
                dgUsers.AutoGenerateColumns = false;
                dgUsers.DataSource = dsUsers.Tables[0];
            }
            else
            {
                dgUsers.DataSource = null;
                //added by archana--start
                MessageBox.Show("User does not exist!");
                //added by archana--end
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    Users objUsers = new Users();

                    objUsers.UserID = iUserID;
                    objUsers.FullName = txtName.Text.Trim();
                    objUsers.ContactNo = txtContactNo.Text.Trim();
                    objUsers.UserName = txtUserName.Text.Trim();
                    objUsers.Password = txtPassword.Text.Trim();
                    objUsers.Role = cmbUserRole.Text;
                    objUsers.Createdby = GlobalValues.User_PkId;
                    objUsers.Updatedby = GlobalValues.User_PkId;
                    objUsers.IsAgent = chkAgent.Checked?true:false;
                    
                    if (cmbUserRole.Text == UserRoles.SUPERADMIN)                       
                    {
                        objUsers.CenterID = null;
                    }

                    if (cmbUserRole.Text == UserRoles.ADMINLEVEL1)
                    {
                        objUsers.CenterID = Convert.ToInt32(cmbHub.SelectedValue);
                    }

                    if (cmbUserRole.Text == UserRoles.ADMINLEVEL2)
                    {
                        objUsers.CenterID = Convert.ToInt32(cmbCountry.SelectedValue);
                    }

                    if (cmbUserRole.Text == UserRoles.ADMINLEVEL3)
                    {
                        objUsers.CenterID = Convert.ToInt32(cmbState.SelectedValue);
                    }

                    if (cmbUserRole.Text == UserRoles.ADMINLEVEL4)
                    {
                        objUsers.CenterID = Convert.ToInt32(cmbDistrict.SelectedValue);
                    }

                    DataSet  dsUsers = null;
                    if (iUserID == 0)
                        dsUsers = objUsers.SaveUsers();
                    else
                        dsUsers = objUsers.UpdateUsers();

                    if (dsUsers != null && dsUsers.Tables.Count > 0 && dsUsers.Tables[0].Rows.Count > 0)
                    {
                        if (objUsers.UserID == 0)
                        {
                            btnSave.Text = Constants.UPDATE;

                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;

                            iUserID = Convert.ToInt32(dsUsers.Tables[0].Rows[0]["Id"]);
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                        }
                        LoadUsers();
                    }
                    else
                    {
                        lblMessages.Text = Messages.ERROR_PROCESSING;
                        lblMessages.ForeColor = Color.Red;
                    }

                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnNewData_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    cmbUserRole.Text = dgUsers.Rows[e.RowIndex].Cells["Role"].Value.ToString();
                    txtContactNo.Text = dgUsers.Rows[e.RowIndex].Cells["ContactNumber"].Value.ToString();
                    txtName.Text = dgUsers.Rows[e.RowIndex].Cells["Fullname"].Value.ToString();
                    txtUserName.Text = dgUsers.Rows[e.RowIndex].Cells["Username"].Value.ToString();
                    txtPassword.Text = dgUsers.Rows[e.RowIndex].Cells["Password"].Value.ToString();
                    chkAgent.Checked = Convert.ToBoolean(dgUsers.Rows[e.RowIndex].Cells["Agent"].Value);

                    btnSave.Text = Constants.UPDATE;
                    iUserID = Convert.ToInt32(dgUsers.Rows[e.RowIndex].Cells["Id"].Value);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgUsers.Columns[e.ColumnIndex].Index == 8)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgUsers.CurrentRow.Cells[0].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 UserId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;
            lblMessages.Text = string.Empty;

            Users objUsers = new Users();
            objUsers.UserID = UserId;
            dsDeleteStatus = objUsers.DeleteUsers(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadUsers();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                   lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void cmbUserRole_SelectedIndexChanged(object sender, EventArgs e)        
        {            
            int ParentId = 0;
            if (cmbUserRole.Text != UserRoles.SUPERADMIN && cmbUserRole.Text != "--Select--")
            {
                cmbHub.Visible = true;
                lblHub.Visible = true;
                lblCountry.Visible = false;
                cmbCountry.Visible = false;
                lblState.Visible = false;
                cmbState.Visible = false;
                lblDistrict.Visible = false;
                cmbDistrict.Visible = false;
                FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
            }
            else
            {
                cmbHub.DataSource = null;
                cmbHub.Visible = false;
                lblHub.Visible = false;
                lblCountry.Visible = false;
                cmbCountry.Visible = false;
                lblState.Visible = false;
                cmbState.Visible = false;
                lblDistrict.Visible = false;
                cmbDistrict.Visible = false;
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUserRole.Text != UserRoles.ADMINLEVEL1)
            {
                cmbCountry.Visible = true;
                lblCountry.Visible = true;
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            }            
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUserRole.Text != UserRoles.ADMINLEVEL2)
            {
                cmbState.Visible = true;
                lblState.Visible = true;
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            }
         }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUserRole.Text != UserRoles.ADMINLEVEL3)
            {
                cmbDistrict.Visible = true;
                lblDistrict.Visible = true;
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            }
        }

        private void txtContactNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }                
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
    }
}
