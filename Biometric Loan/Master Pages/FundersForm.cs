﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;

namespace SPARC.Master_Pages
{
    public partial class FundersForm : Form
    {
        private Int64 iUserID = 0;
        public FundersForm()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void FundersForm_Load(object sender, EventArgs e)
        {
            ClearControls();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
            }
            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbCity);
                    cmbCity.SelectedValue = Id;
                    cmbCity.Enabled = false;
                }
            }
            LoadFunders();
        }

        private void LoadFunders()
        {
            Funders objFunders = new Funders();
            objFunders.FundersName = txtSearchName.Text.Trim().Length == 0 ? null : txtSearchName.Text.Trim();

            DataSet dsFunder = objFunders.LoadFunders();
            if (dsFunder != null && dsFunder.Tables.Count > 0 && dsFunder.Tables[0].Rows.Count > 0)
            {
                dgfunders.AutoGenerateColumns = false;
                dgfunders.DataSource = dsFunder.Tables[0];
            }
            else
            {
                dgfunders.DataSource = null;
                MessageBox.Show("Funder doesn't exist!");
            }
        }
        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            txtFunderName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txtContactNumber.Text = string.Empty;
            txtContactPerson.Text = string.Empty;
            txtZip.Text = string.Empty;
            iUserID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Fetch District
                if (cmbState.SelectedIndex > 0)
                    FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbCity);
                else
                    cmbCity.DataSource = null;
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Fetch State
                if (cmbCountry.SelectedIndex > 0)
                    FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
                else
                    cmbState.DataSource = null;
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            //  Regex RX = new Regex("^[a-zA-Z0-9]{1,20}@[a-zA-Z0-9]{1,20}.[a-zA-Z]{2,3}$");

            if (txtFunderName.Text.Trim().Length == 0)
                ErrorMsg = Messages.FUNDERNAME + Messages.COMMAWITHSPACE;
            if (txtAddress1.Text.Trim().Length == 0)
                ErrorMsg += Messages.ADDRESS1 + Messages.COMMAWITHSPACE;
            if (txtAddress2.Text.Trim().Length == 0)
                ErrorMsg += Messages.ADDRESS2 + Messages.COMMAWITHSPACE;
            if (txtZip.Text.Trim().Length == 0)
                ErrorMsg += Messages.ZIP + Messages.COMMAWITHSPACE;
            if (txtEmailID.Text.Trim().Length == 0)
                ErrorMsg += Messages.EMAIL + Messages.COMMAWITHSPACE;
            //if (txtEmailID.Text.Trim().Length > 0 && !RX.IsMatch(txtEmailID.Text))
            //    ErrorMsg += Messages.INVALIDEMAIL + Messages.COMMAWITHSPACE;
            if (txtTelephone.Text.Trim().Length == 0)
                ErrorMsg += Messages.TELEPHONE + Messages.COMMAWITHSPACE;
            //if (txtFax.Text.Trim().Length == 0)
            //    ErrorMsg += Messages.FAX + Messages.COMMAWITHSPACE;
            if (txtContactNumber.Text.Trim().Length == 0)
                ErrorMsg += Messages.CONTACTNO + Messages.COMMAWITHSPACE;
            if (txtContactPerson.Text.Trim().Length == 0)
                ErrorMsg += Messages.CONTACTPERSON + Messages.COMMAWITHSPACE;
            if (cmbCity.SelectedIndex <= 0)
                ErrorMsg += Messages.City + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;
                    lblfun.Text = string.Empty;
                    Funders objFunder = new Funders();
                    objFunder.FundersID = iUserID;
                    objFunder.FundersName = txtFunderName.Text.Trim();
                    objFunder.Address1 = txtAddress1.Text.Trim();
                    objFunder.Address2 = txtAddress2.Text.Trim();
                    objFunder.Country = Convert.ToInt32(cmbCountry.SelectedValue);
                    objFunder.State = Convert.ToInt32(cmbState.SelectedValue);
                    objFunder.City = cmbCity.SelectedValue.ToString();
                    //added by archana--start
                    objFunder.Hub = Convert.ToInt32(cmbHub.SelectedValue.ToString());
                    //added by archana--end
                    objFunder.Zip = txtZip.Text.Trim();
                    objFunder.Telephone = txtTelephone.Text.Trim();
                    objFunder.Fax = txtFax.Text.Trim();
                    objFunder.ContactNumber = txtContactNumber.Text.Trim();
                    objFunder.ContactPerson = txtContactPerson.Text.Trim();
                    objFunder.Createdby = GlobalValues.User_PkId;
                    objFunder.Email = txtEmailID.Text.Trim();
                    objFunder.Updatedby = GlobalValues.User_PkId;

                    DataSet dsFunder = null;
                    if (iUserID == 0)
                    {
                        SqlCommand cmd = new SqlCommand("select id from Funders where FunderName=@FunderName", con);  //added by Rounaq Start { code to not allow same funder names
                        cmd.Parameters.AddWithValue("@FunderName", this.txtFunderName.Text);
                        con.Open();

                        var nId = cmd.ExecuteScalar();

                        if (nId != null)
                        {

                            lblfun.Text = "Funder with this name already exists";
                            lblfun.ForeColor = Color.Red;
                        }
                        else
                        {
                            dsFunder = objFunder.SaveFunders();
                            btnSave.Text = Constants.UPDATE;

                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;

                        }

                        con.Close();
                    }
                    else
                    {
                        dsFunder = objFunder.UpdateFunders();
                        if (dsFunder != null && dsFunder.Tables.Count > 0 && dsFunder.Tables[0].Rows.Count > 0)
                        {
                            if (objFunder.FundersID == 0)
                            {
                                btnSave.Text = Constants.UPDATE;

                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;

                                iUserID = Convert.ToInt32(dsFunder.Tables[0].Rows[0]["Id"]);
                            }
                            //  }//
                            else
                            {
                                lblMessages.Text = Messages.UPDATED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            LoadFunders();
                        }
                        else
                        {
                            lblMessages.Text = Messages.ERROR_PROCESSING;
                            lblMessages.ForeColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            LoadFunders();
        }

        private void dgfunders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //added by Archana--start
                    cmbHub.SelectedValue = Convert.ToInt64(dgfunders.Rows[e.RowIndex].Cells["Hub"].Value);
                    //added bY Archana--end
                    cmbCountry.SelectedValue = Convert.ToInt64(dgfunders.Rows[e.RowIndex].Cells["Country"].Value.ToString());
                    txtFunderName.Text = dgfunders.Rows[e.RowIndex].Cells["FunderName"].Value.ToString();
                    txtTelephone.Text = dgfunders.Rows[e.RowIndex].Cells["Telephone"].Value.ToString();
                    txtEmailID.Text = dgfunders.Rows[e.RowIndex].Cells["EmailAddress"].Value.ToString();
                    txtFax.Text = dgfunders.Rows[e.RowIndex].Cells["Fax"].Value.ToString();
                    txtZip.Text = dgfunders.Rows[e.RowIndex].Cells["Zip"].Value.ToString();
                    txtAddress1.Text = dgfunders.Rows[e.RowIndex].Cells["Address1"].Value.ToString();
                    txtAddress2.Text = dgfunders.Rows[e.RowIndex].Cells["Address2"].Value.ToString();
                    txtContactNumber.Text = dgfunders.Rows[e.RowIndex].Cells["ContactNumber"].Value.ToString();
                    txtContactPerson.Text = dgfunders.Rows[e.RowIndex].Cells["ContactPerson"].Value.ToString();
                    cmbState.SelectedValue = Convert.ToInt64(dgfunders.Rows[e.RowIndex].Cells["State"].Value.ToString());
                    cmbCity.SelectedValue = dgfunders.Rows[e.RowIndex].Cells["City"].Value.ToString();

                    btnSave.Text = Constants.UPDATE;
                    iUserID = Convert.ToInt32(dgfunders.Rows[e.RowIndex].Cells["Id"].Value);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgfunders_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
            }
            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }
                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbCity);
                    cmbCity.SelectedValue = Id;
                    cmbCity.Enabled = false;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadFunders();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void txtTelephone_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtContactNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

    }
}
