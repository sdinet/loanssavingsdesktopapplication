﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Configuration;

namespace SPARC.Master_Pages
{
    public partial class FunderAllocation : Form
    {
        SqlCommand cmd;
        SqlDataAdapter adapt;
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public FunderAllocation()
        {

            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        public int i = 0;
        public ArrayList textboxes = new ArrayList();
        public ArrayList textboxes1 = new ArrayList();
        public ArrayList textboxes2 = new ArrayList();
        public ArrayList textboxes3 = new ArrayList();
        public ArrayList textboxes4 = new ArrayList();
        public ArrayList textboxes6 = new ArrayList();
        public ArrayList textboxes7 = new ArrayList();
        public ArrayList textboxes8 = new ArrayList();
        public ArrayList textboxes9 = new ArrayList();
        public ArrayList textboxes10 = new ArrayList();
        public ArrayList textboxes12 = new ArrayList();
        public ArrayList textboxes13 = new ArrayList();
        public int index = 0;

        public int count = 0;
        private void FunderAllocation_Load(object sender, EventArgs e)
        {
            FillFunderAllocationCombo();
            FillFundDisplayGrid(0);
            FillCombosCenters(Convert.ToInt32(LocationCode.State), 0, CenterCombo);
            FillCombos(Convert.ToInt32(LocationCode.CurrencyType), 0, comboBoxcurrency);
            dataGridView2.Visible = false;
            btnclose2.Visible = false;
            btnclear.Visible = false;
            dateTimePicker1.Visible = false;
            Dlbl.Visible = false;
            txtTransactionId.Visible = false;
            txtfundername.Visible = false;

            i++;
            //this.reportViewer1.RefreshReport();
            //this.reportViewer1.RefreshReport();
        }

        private void FillFunderAllocationCombo()
        {
            FundAlloc objFundLoad = new FundAlloc();
            DataSet dsLoadFund = objFundLoad.LoadFundersForAlloc();
            if (dsLoadFund != null && dsLoadFund.Tables.Count > 0 && dsLoadFund.Tables[0].Rows.Count > 0)
            {
                cmbSelectFunder.DisplayMember = "Fundername";
                cmbSelectFunder.ValueMember = "Id";
                cmbSelectFunder.DataSource = dsLoadFund.Tables[0];
            }
        }

        private void FillFundDisplayGrid(int funderId)
        {
            FundAlloc objFundDisplay = new FundAlloc();
            DataSet dsLoadGrid = objFundDisplay.LoadFundDisplayinGrid(funderId);
            if (dsLoadGrid != null && dsLoadGrid.Tables.Count > 0 && dsLoadGrid.Tables[0].Rows.Count > 0)
            {
                dgFundsRecieved.DataSource = dsLoadGrid.Tables[0];
                this.dgFundsRecieved.Columns["Id"].Visible = false;
                this.dgFundsRecieved.Columns["Actual Amount"].DefaultCellStyle.Format = "N2";
                this.dgFundsRecieved.Columns["Allocated Amount"].DefaultCellStyle.Format = "N2";
                //dgFundsRecieved.RowsHeaderVisible = false;
                //this.dgFundsRecieved.AutoSizeColumnsMode = true;
                dgFundsRecieved.BackgroundColor = System.Drawing.SystemColors.Control;
                dgFundsRecieved.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgFundsRecieved.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                txtTransactionId.Visible = false;
                txtActualAmt.Visible = false;
                txtAllocatedAmt.Visible = false;
                txtTransactionId.Visible = false;
                txtfundername.Visible = false;

            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgFundsRecieved_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void clearscreen()
        {
            cmbSelectFunder.SelectedIndex = 0;
            CenterCombo.SelectedIndex = 0;
            comboBoxcurrency.SelectedIndex = 0;
            Amttxt.Text = "";
            Notestxt.Text = "";
            txtTransactionId.Text = "";
        }
        private void clearscreen1()
        {
            //cmbSelectFunder.SelectedIndex = 0;
            CenterCombo.SelectedIndex = 0;
            comboBoxcurrency.SelectedIndex = 0;
            Amttxt.Text = "";
            Notestxt.Text = "";
            txtTransactionId.Text = "";
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }
        private void LoadGridFundDetails()
        {
            //try
            //{
            //    FundAlloc objFundetails = new FundAlloc();                
            //    DataSet dsUsers = objFundetails.LoadFundAllocationForGrid();
            //    if (dsUsers != null && dsUsers.Tables.Count > 0 && dsUsers.Tables[0].Rows.Count > 0)
            //    {
            //        dgFundsRecieved.AutoGenerateColumns = false;
            //        dgFundsRecieved.DataSource = dsUsers.Tables[0];
            //    }
            //    else
            //    {
            //        dgFundsRecieved.DataSource = null;
            //    }

            //}
            //catch (Exception ex)
            //{

            //}
        }

        private void cmbSelectFunder_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSelectFunder.SelectedIndex > 0)
            {
                FillFundDisplayGrid(Convert.ToInt16(cmbSelectFunder.SelectedValue));
                clearscreen1();
            }
            if (cmbSelectFunder.SelectedIndex == 0)
            {
                FillFundDisplayGrid(0);
            }

        }


        private string DoValidations1()
        {
            string ErrorMsg = string.Empty;

            if (Convert.ToInt16(cmbSelectFunder.SelectedValue) <= 0)
                ErrorMsg = Messages.FUNDERNAME + Messages.COMMAWITHSPACE;

            if (txtTransactionId.Text.Trim().Length == 0)
                ErrorMsg += Messages.lblid11 + Messages.COMMAWITHSPACE;

            if (Convert.ToInt16(CenterCombo.SelectedValue) <= 0)
                ErrorMsg += Messages.CENTERNAME + Messages.COMMAWITHSPACE;

            if ((Amttxt.Text.Trim().Length == 0))
            {
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            }
            else if ((Amttxt.Text.Trim().Length) > (txtActualAmt.Text.Trim().Length))
            {
                ErrorMsg += Messages.AMOUNT2 + Messages.COMMAWITHSPACE;

            }
            else if (Convert.ToDouble(Amttxt.Text.Trim()) <= 0)
            {
                ErrorMsg += Messages.AMOUNT2 + Messages.COMMAWITHSPACE;
            }

            else if (Convert.ToInt16(cmbSelectFunder.SelectedValue) == 0)
            {
                ErrorMsg = Messages.FUNDERNAME + Messages.COMMAWITHSPACE;
            }
            else if (txtTransactionId.Text == "")
            {
                ErrorMsg = Messages.lblid11 + Messages.COMMAWITHSPACE;
            }

            else
            {
                if ((Convert.ToDouble(Amttxt.Text)) > ((Convert.ToDouble(txtActualAmt.Text)) - (Convert.ToDouble(txtAllocatedAmt.Text))))
                    ErrorMsg += Messages.AMOUNT1 + Messages.COMMAWITHSPACE;
            }

            if (Convert.ToInt16(comboBoxcurrency.SelectedValue) <= 0)
                ErrorMsg += Messages.CURRENCYTYPE + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
            }
            return ErrorMsg;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{
            string strMessage = DoValidations1();
            if (strMessage.Length > 0)
            {
                MessageBox.Show(strMessage);
            }
            else
            {
                FundAlloc objTransactionAll = new FundAlloc();
                objTransactionAll.FunderTransactionID = Convert.ToInt64(txtTransactionId.Text);
                objTransactionAll.TransactionCenter = Convert.ToInt64((CenterCombo).SelectedValue.ToString());
                double amount = Convert.ToDouble(Amttxt.Text);
                objTransactionAll.TransactionAmounts = amount;
                objTransactionAll.TransactionCurrency = Convert.ToInt64((comboBoxcurrency).SelectedValue.ToString());
                objTransactionAll.TransactionDate = dateTimePicker1.Value;
                objTransactionAll.TransactionNotes = Convert.ToString(Notestxt.Text);
                if ((Convert.ToInt32(dgFundsRecieved.SelectedRows[0].Cells["Allocated Amount"].Value) == 0))
                {
                    objTransactionAll.Remainingamount = Convert.ToDouble((Convert.ToInt32(dgFundsRecieved.SelectedRows[0].Cells["Actual Amount"].Value)) - (Convert.ToInt32(Amttxt.Text)));
                }
                else
                {
                    objTransactionAll.Remainingamount = Convert.ToDouble((Convert.ToDouble(dgFundsRecieved.SelectedRows[0].Cells["Actual Amount"].Value)) - ((Convert.ToDouble(dgFundsRecieved.SelectedRows[0].Cells["Allocated Amount"].Value)) + (Convert.ToDouble(Amttxt.Text))));
                }

                objTransactionAll.TransactionCreated = GlobalValues.User_PkId;
                objTransactionAll.TransactionUpdated = GlobalValues.User_PkId;
                objTransactionAll.transactionproject = Convert.ToString(dgFundsRecieved.SelectedRows[0].Cells["Project"].Value);
                objTransactionAll.TransactionAmountINSERTION();
                MessageBox.Show("The Transaction has been Completed Succesfully");
                FillFundDisplayGrid(0);
                clearscreen();
                txtTransactionId.Visible = false;
                txtfundername.Visible = false;
                //dataGridView2.Visible = false;
                //btnclose2.Visible = false;
                //btnclear.Visible = false;
            }
        }


        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void FillCombosCenters(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValuesForAllocation(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (cmbSelectFunder.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select Funder to Allocate");
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btn_Click_Click(object sender, EventArgs e)
        {

        }

        private void FillCurrencyCombo(int CodeTypeID, int ParentID, ComboBox cmbCurrencyType)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;
            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                cmbCurrencyType.DisplayMember = "Name";
                cmbCurrencyType.ValueMember = "Id";
                cmbCurrencyType.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            FillFunderAllocationCombo();
            FillFundDisplayGrid(0);
            //lblMessage.Text = "";
            //lblFunderName.Text = "";
            //lblAmountSponsered.Text = "";
            //lblDateRecieved.Text = "";
            //lblCurrency1.Text = "";
            //lblActualAmount.Text = "";
            //lblCurrency2.Text = "";
            //lblAmountAllocated.Text = "";
            //lblCurrency.Text = "";
            //lblUnallocatedAmnt.Text = "";
            //lblAnotherMessage.Text = "";

            for (int j = count; j > -1; j--)
            {
                ((ComboBox)this.textboxes10[j]).Dispose();
                ((DateTimePicker)this.textboxes12[j]).Dispose();
                ((ComboBox)this.textboxes1[j]).Dispose();
                ((TextBox)this.textboxes3[j]).Dispose();
                ((TextBox)this.textboxes4[j]).Dispose();
                ((Button)this.textboxes13[j]).Dispose();

                textboxes10.RemoveAt(j);
                textboxes12.RemoveAt(j);
                textboxes1.RemoveAt(j);
                textboxes3.RemoveAt(j);
                textboxes4.RemoveAt(j);
                textboxes13.RemoveAt(j);
            }
            count = -1;
            i = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {



        }

        private void amountToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void centrerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Amttxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void Amttxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }

        }

        private void txtfundername_ReadOnlyChanged(object sender, EventArgs e)
        {

        }

        private void Notestxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //char ch = e.KeyChar;
            //if (!char.IsLetter(ch) && ch != 8)
            //{
            //    e.Handled = true;
            //}

        }

        private void dgFundsRecieved_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtTransactionId.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Id"].Value.ToString();
            txtTransactionId.Visible = true;
            txtActualAmt.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Actual Amount"].Value.ToString();
            //txtActualAmt.Visible = false;
            txtAllocatedAmt.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Allocated Amount"].Value.ToString();
            //txtAllocatedAmt.Visible = false;
            txtfundername.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Funder Name"].Value.ToString();
            //txtfundername.Visible = true;
            //txtfundername.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Funder Name"].Value.ToString();
            //txtTransactionId.Text = dgFundsRecieved.Rows[e.RowIndex].Cells["Id"].Value.ToString();
            //txtTransactionId.Visible = true;
            SqlConnection con = new SqlConnection(ConnectionString);
            con.Open();
            dataGridView2.Visible = true;
            btnclose2.Visible = true;
            btnclear.Visible = true;
            Int64 txnid = 0;
            String fdname = "";
            //dataGridView2.DataBind();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            if (txtTransactionId.Text == "")
            {
                txnid = 0;
            }
            else
            {
                txnid = Convert.ToInt64(txtTransactionId.Text);
            }
            //edited by Rounaq
            double i = txnid;
            //edited by rounaq--end
            //if (txtfundername.Text == "")
            //{
            //    fdname = String.Empty;
            //}
            //else
            //{
            //    fdname = txtfundername.Text.ToString();
            //}

            //cmd.CommandText = "select fa.DateOfTransaction as 'Date Of Transaction', fa.Amount as 'Amount', Remaining_Amount_For_Loan_Allocation as 'Remaining Amount',c.name as 'center name', fa.Project as 'Project', fa.Notes as 'Notes'  from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id Left join CodeValue c on c.Id = fa.fk_CenterId where  fk_FunderTransactionId = '" + txnid + "'  order by fa.DateOfTransaction desc";
            ////cmd.CommandText = "select fa.Id, fa.DateOfTransaction as 'Date Of Transaction', fa.Amount as 'Amount', Remaining_Amount_For_Loan_Allocation as 'Remaining Amount',c.name as 'center name', fa.Project as 'Project', fa.Notes as 'Notes'  from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id Left join CodeValue c on c.Id = fa.fk_CenterId where  fk_FunderTransactionId = '" + txnid + "'  order by fa.Id desc";

            cmd.CommandText = "select  fa.Id, f.fundername as 'Funder Name', fa.DateOfTransaction as 'Date Of Transaction', fa.Amount as 'Amount', Remaining_Amount_For_Loan_Allocation as 'Remaining Amount',c.name as 'center name', fa.Project as 'Project', fa.Notes as 'Notes'  from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id LEFT JOIN funders f on f.Id= fd.fk_FunderId Left join CodeValue c on c.Id = fa.fk_CenterId where  fk_FunderTransactionId = '" + txnid + "'  order by fa.Id desc";

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView2.DataSource = dt;
            dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.Columns["Id"].Visible = false;
            this.dataGridView2.Columns["Amount"].DefaultCellStyle.Format = "N2";
            this.dataGridView2.Columns["Remaining Amount"].DefaultCellStyle.Format = "N2";
            con.Close();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //    dataGridView2.Visible = true;
            //    btnclose2.Visible = true;
            //    btnclear.Visible = true;
            //SqlConnection con = new SqlConnection(ConnectionString);
            //con.Open();
            //SqlCommand cmd = con.CreateCommand();
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandText = "select * from fundsallocation";
            //cmd.ExecuteNonQuery();
            //DataTable dt = new DataTable();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(dt);
            //dataGridView2.DataSource = dt;
            //this.dataGridView2.Columns["Id"].Visible = false;
            //this.dataGridView2.Columns["fk_FunderTransactionId"].Visible = false;
            //this.dataGridView2.Columns["fk_CenterId"].Visible = false;
            //this.dataGridView2.Columns["Currency"].Visible = false;
            //this.dataGridView2.Columns["Sync_Type"].Visible = false;
            //dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            //dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //dataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //this.dataGridView2.Columns["Remaining_Amount_For_Loan_Allocation"].DefaultCellStyle.Format = "N2";
            //this.dataGridView2.Columns["Amount"].DefaultCellStyle.Format = "N2";
            //con.Close();
            // dataGridView2.Rows.RemoveAt(0);  This code will Clears the 1st row in the grid and preserves the columns.
            //dataGridView12.Rows.Clear();  This code will clear the entire datagrid.

        }

        private void btnclose2_Click(object sender, EventArgs e)
        {
            dataGridView2.Visible = false;
            btnclose2.Visible = false;
            btnclear.Visible = false;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = "";
            dataGridView2.DataSource = null;

        }

        private void dataGridView2_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
    }
}
