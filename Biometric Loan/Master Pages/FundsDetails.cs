﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC.Master_Pages
{
    public partial class FundsDetails : Form
    {
        private Int64 iUserID = 0;
        public FundsDetails()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void FundsDetails_Load(object sender, EventArgs e)
        {
            dtDateTime.MaxDate = DateTime.Today;
            fillFundersCombo();            
            FillCurrencyCombo(Convert.ToInt32(LocationCode.CurrencyType), 0, cmbCurrencyType);
            LoadGridFundDetails();
            FillFundingTypeCombo1(Convert.ToInt32(LocationCode.FundersType), 0, cmbFundingType);           
        }

        private void fillFundersCombo()
        {
            DataTable table = new DataTable();
            FundDetailsClass objfundetails = new FundDetailsClass();
            DataSet FundersDataDS = objfundetails.LoadFundersName();
            if (FundersDataDS != null && FundersDataDS.Tables.Count > 0 && FundersDataDS.Tables[0].Rows.Count > 0)
            {
                cmbFunders.DisplayMember = "Fundername";
                cmbFunders.ValueMember = "Id";               
                cmbFunders.DataSource = FundersDataDS.Tables[0];                
            }
        }

        private void FillFundingTypeCombo1(int CodeTypeID, int ParentID, ComboBox cmbFundingType)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                cmbFundingType.DisplayMember = "Name";
                cmbFundingType.ValueMember = "Id";
                cmbFundingType.DataSource = dsCodeValues.Tables[0];
            }
        }        

        private void FillCurrencyCombo(int CodeTypeID, int ParentID, ComboBox cmbCurrencyType)
        {        
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                cmbCurrencyType.DisplayMember = "Name";
                cmbCurrencyType.ValueMember = "Id";
                cmbCurrencyType.DataSource = dsCodeValues.Tables[0];
            }           
        }
        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            if (Convert.ToInt16(cmbFunders.SelectedValue) <= 0)
                ErrorMsg = Messages.FUNDERNAME + Messages.COMMAWITHSPACE;
            if (Convert.ToInt16(cmbFundingType.SelectedValue) <= 0)
                ErrorMsg += Messages.FUNDTYPE + Messages.COMMAWITHSPACE;
            if (Convert.ToInt16(cmbCurrencyType.SelectedValue) <= 0)
                ErrorMsg += Messages.CURRENCYTYPE + Messages.COMMAWITHSPACE;
            if (txtAmount.Text.Trim().Length == 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;           
                                  
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

         private void button1_Click(object sender, EventArgs e)
         {
              try
              {                              
                 string strMessage = DoValidations();
                 if (strMessage.Length > 0)
                 {
                     lblMessages.Text = strMessage;
                 }
                 else
                 {
                         lblMessages.Text = string.Empty;                
                         FundDetailsClass objFundetails = new FundDetailsClass();
                         objFundetails.Id = iUserID;
                         objFundetails.Fk_FunderId = Convert.ToInt64(cmbFunders.SelectedValue);
                         objFundetails.Createdby = GlobalValues.User_PkId;
                         objFundetails.Updatedby = GlobalValues.User_PkId;
                         objFundetails.Amount = Convert.ToDouble(txtAmount.Text);
                         objFundetails.Currency = Convert.ToInt64(cmbCurrencyType.SelectedValue);
                         objFundetails.Notes = txtNotes.Text;
                         objFundetails.Sponser_Type = Convert.ToInt64(cmbFundingType.SelectedValue);
                         objFundetails.DateOfTransaction = dtDateTime.Value;
                     //added By Pawan--start
                         objFundetails.Project = txtProject.Text;
                     // added by pawan--end
                         DataSet dsFundetails = null;
                         if (iUserID == 0)
                             dsFundetails = objFundetails.SaveFundDetails();
                         else
                             dsFundetails = objFundetails.UpdateFundDetails();

                         if (dsFundetails != null && dsFundetails.Tables.Count > 0 && dsFundetails.Tables[0].Rows.Count > 0)
                         {
                             if (objFundetails.Id == 0)
                             {
                                 button1.Text = Constants.UPDATE;

                                 lblMessages.Text = Messages.SAVED_SUCCESS;
                                 lblMessages.ForeColor = Color.Green;

                                 iUserID = Convert.ToInt32(dsFundetails.Tables[0].Rows[0]["ID"]);
                             }
                             else
                             {
                                 lblMessages.Text = Messages.UPDATED_SUCCESS;
                                 lblMessages.ForeColor = Color.Green;
                             }                             
                             LoadGridFundDetails();                             
                         }
                         else
                         {
                             lblMessages.Text = Messages.ERROR_PROCESSING;
                             lblMessages.ForeColor = Color.Red;
                         }
                     }
              }
                 catch (Exception ex)
                 {
                     Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                 }
             }
         
         private void LoadGridFundDetails()
         {
             try
             {
                 FundDetailsClass objFundetails = new FundDetailsClass();               
                 DataSet dsUsers = objFundetails.LoadGridFundDetails();
                 if (dsUsers != null && dsUsers.Tables.Count > 0 && dsUsers.Tables[0].Rows.Count > 0)
                 {                     
                     gdFundsAllocation.AutoGenerateColumns = false;
                     gdFundsAllocation.DataSource = dsUsers.Tables[0];                    
                 }
                 else
                 {
                     gdFundsAllocation.DataSource = null;
                 }                 
             }
             catch (Exception ex)
             {
                 
             }
         }

         private void button2_Click(object sender, EventArgs e)
         {
             fillFundersCombo();            
             FillFundingTypeCombo1(Convert.ToInt32(LocationCode.FundersType), 0, cmbFundingType);
             FillCurrencyCombo(Convert.ToInt32(LocationCode.CurrencyType), 0, cmbCurrencyType);
             LoadGridFundDetails();
             cmbFunders.Enabled = true;
             txtAmount.Text = "";
             txtNotes.Text = "";
             txtProject.Text = "";
             lblMessages.Text = "";
             iUserID = 0;
             button1.Text = "Save";
         }

         private void button3_Click(object sender, EventArgs e)
         {
             try
             {
                 this.Close();
             }
             catch (Exception ex)
             {
             }
         }

         private void gdFundsAllocation_CellContentClick(object sender, DataGridViewCellEventArgs e)
         {

         }

         private void gdFundsAllocation_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
         {

         }

         private void gdFundsAllocation_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
         {
             try
             {
                 if (e.RowIndex >= 0)
                 {
                     cmbFunders.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["fundername"].Value.ToString();
                     cmbFundingType.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["FundingType"].Value.ToString();
                     cmbCurrencyType.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["currency"].Value.ToString();
                     txtAmount.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["amount"].Value.ToString();
                     dtDateTime.Value = Convert.ToDateTime(gdFundsAllocation.Rows[e.RowIndex].Cells["Date"].Value);
                     txtNotes.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["Notes"].Value.ToString();
                     //added by pawan--start
                     txtProject.Text = gdFundsAllocation.Rows[e.RowIndex].Cells["Project"].Value.ToString();
                     //added by pawan--end
                     button1.Text = Constants.UPDATE;
                     iUserID = Convert.ToInt32(gdFundsAllocation.Rows[e.RowIndex].Cells["Id"].Value);
                     cmbFunders.Enabled = false;
                 }
             }
             catch (Exception ex)
             {
                 Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
             }
         }

         private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
         {
             try
             {
                 if (!char.IsControl(e.KeyChar)
                         && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                 {
                     e.Handled = true;
                 }

                 // only allow one decimal point
                 if (e.KeyChar == '.'
                     && (sender as TextBox).Text.IndexOf('.') > -1)
                 {
                     e.Handled = true;
                 }
             }
             catch (Exception ex)
             {
                 Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
             }
         }
        
    }
}
