﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;


namespace SPARC
{
    public partial class SavingAccountTransaction : Form
    {
        public SavingAccountTransaction()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SavingAccountTransactions_Load(object sender, System.EventArgs e)
        {
            this.reportViewer1.Refresh();
            //FillCombos(Convert.ToInt32(LocationCode.Country), 0, cmbCountry);
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();

            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }

            }
        }
         
        private void FillCombos1(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            //SqlCommand cmd = new SqlCommand("usp_GetCodeValues1", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            //SqlDataAdapter SQLAdapter = new SqlDataAdapter(cmd);
            //SqlParameter[] param = new SqlParameter[1];
            //param[0] = new SqlParameter("@CodeTypeID", SqlDbType.Int);
            //param[0].Value = CodeTypeID;
            //DataSet dataset = new DataSet();

            //SQLAdapter.Fill(dataset, "usp_GetCodeValues1");

            //DataTable dt1 = dataset.Tables["usp_GetCodeValues1"];
            //lblMessage.Hide();
           

            CodeValues1 objCodeValues1 = new CodeValues1();
            objCodeValues1.CodeTypeID = CodeTypeID;
            objCodeValues1.ParentID = ParentID;

            DataSet dsCodeValues1 = objCodeValues1.GetCodeValues1(false);
            if (dsCodeValues1 != null && dsCodeValues1.Tables.Count > 0 && dsCodeValues1.Tables[0].Rows.Count > 0)
            {
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues1.Tables[0];
            }
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void cmbCountry_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch Taluk
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

       

        private void cmbSlum_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                //Fetch Slum
                dateTimePicker1.Value = System.DateTime.Now;
               // cmbTransType.ResetText();
                int value = Convert.ToInt32(cmbSlum.SelectedValue);


                if (cmbSlum.SelectedIndex > 0)

                    FillCombos1(Convert.ToInt32(LocationCode.Member), Convert.ToInt32(cmbSlum.SelectedValue), cmbMember);// .SelectedValue), cmbSlum);

                else
                    cmbSlum.DataSource = null;
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            
        }

        private void cmbMember_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dateTimePicker1.Value = System.DateTime.Now;
           // cmbTransType.ResetText();
        }
        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            if (cmbSlum.SelectedIndex <= 0)
                ErrorMsg += Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessage.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }
       
            public void getCreditDetails()
              {
            string theDate = dateTimePicker1.Value.ToString("MM/dd/yyyy");
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("SavingsAccountTransaction");
                SqlCommand cmd;
                SqlDataReader dr;
                if (checkBox1.Checked == false)
                {
                     //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                    string cmdText = @"   select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                          t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId  
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='CRE' order by t.transactiondate desc;";

                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                else {
                    //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                    string cmdText = @"   select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,u.Fullname,s.accountnumber,t.transactiondate,
                                          t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId  
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='CRE' order by t.transactiondate desc;";
                    
                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                 //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        public void getCreditDetails1()
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("SavingsAccountTransaction");
                SqlCommand cmd;
                SqlDataReader dr;
                if (checkBox1.Checked == false)
                {
                    //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                    string cmdText = @"   select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                          t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId 
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='CRE'and t.TransactionDate>=@theDate order by t.transactiondate desc;";

                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                else {
                    string cmdText = @"   select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                          u.Fullname,t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId 
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='CRE'and t.TransactionDate>=@theDate order by t.transactiondate desc;";

                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public void getDebitDetails()
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("SavingsAccountTransaction");
                SqlCommand cmd;
                SqlDataReader dr;
                if (checkBox1.Checked == true)
                {
                    //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                    string cmdText = @"  select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                          u.Fullname,t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId 
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='DEB'  order by t.transactiondate desc;";
                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                else {
                    string cmdText = @"  select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                          t.Tran_type,t.Amount,t.CurrentBal, 
                                          (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                          from CodeValue C
                                          inner join Members M on c.Id=m.fk_SlumId 
                                          Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                          inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                          inner join dbo.Users u 
                                          on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='DEB'  order by t.transactiondate desc;";
                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public void getDebitDetails1()
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("SavingsAccountTransaction");
                SqlCommand cmd;
                SqlDataReader dr;
                if (checkBox1.Checked == false)
                {
                    //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                    string cmdText = @"  select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                         t.Tran_type,t.Amount,t.CurrentBal, 
                                         (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                         from CodeValue C
                                         inner join Members M on c.Id=m.fk_SlumId 
                                         Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                         inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                         inner join dbo.Users u 
                                         on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='DEB' and t.TransactionDate>=@theDate  order by t.transactiondate desc;";

                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                    // dateTimePicker1.Value.ToShortDateString();
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                else {
                    string cmdText = @"  select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                         u.Fullname,t.Tran_type,t.Amount,t.CurrentBal, 
                                         (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                         from CodeValue C
                                         inner join Members M on c.Id=m.fk_SlumId 
                                         Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                         inner join dbo.SavingAccountTransactions t on s.fk_MemberId=t.fk_MemberId 
                                         inner join dbo.Users u 
                                         on t.fk_AgentId=u.Id and m.Id=@value and t.Tran_type='DEB' and t.TransactionDate>=@theDate  order by t.transactiondate desc;";

                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                    cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                    // dateTimePicker1.Value.ToShortDateString();
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                }
                //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";



                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));



                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public void getBothDetails()
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("SavingsAccountTransaction");
                SqlCommand cmd;
                SqlDataReader dr;
                String today = DateTime.Now.ToString("MM/dd/yyyy");
                string theDate = dateTimePicker1.Value.ToString("MM/dd/yyyy");
                if (theDate == today)
                {

                    if (checkBox1.Checked == false)
                    {
                        //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                        string cmdText = @" select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                            t.Tran_type,t.Amount,t.CurrentBal,t.fk_Accountid, 
                                            (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                            from CodeValue C
                                            inner join Members M on c.Id=m.fk_SlumId 
                                            Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                            inner join dbo.SavingAccountTransactions t on s.Id=t.fk_Accountid
                                            inner join dbo.Users u on t.fk_AgentId=u.Id and m.Id=@Value;";

                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                        //cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                        // dateTimePicker1.Value.ToShortDateString();
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                    }
                    else
                    {
                        string cmdText = @"  select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                            u.Fullname,t.Tran_type,t.Amount,t.CurrentBal, 
                                            (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                            from CodeValue C
                                            inner join Members M on c.Id=m.fk_SlumId  
                                            Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                           inner join dbo.SavingAccountTransactions t on s.Id=t.fk_Accountid
                                            inner join dbo.Users u 
                                            on t.fk_AgentId=u.Id and m.Id=@value;";

                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                        //cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                        // dateTimePicker1.Value.ToShortDateString();
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                    }
                    //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";



                    this.reportViewer1.LocalReport.EnableHyperlinks = true;
                    this.reportViewer1.LocalReport.DataSources.Add(
                       new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));



                    this.reportViewer1.RefreshReport();
                }
                else{
                    if (checkBox1.Checked == false)
                    {
                        //string cmdText = " select c.name,m.name,m.PassbookNumber,m.MemberId,s.currentbalance,s.Opendate from CodeValue C inner join Members M on c.Id=m.fk_SlumId and c.Id=@value inner join SavingAccount s on m.Id=s.fk_MemberId;";
                        string cmdText = @" select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                            t.Tran_type,t.Amount,t.CurrentBal,t.fk_Accountid, 
                                            (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                            from CodeValue C
                                            inner join Members M on c.Id=m.fk_SlumId
                                            Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                            inner join dbo.SavingAccountTransactions t on s.Id=t.fk_Accountid 
                                            inner join dbo.Users u 
                                            on t.fk_AgentId=u.Id and m.Id=@value and t.TransactionDate>=@theDate ;";

                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                        cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                        // dateTimePicker1.Value.ToShortDateString();
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                    }
                    else
                    {
                        string cmdText = @" select c.name,m.Name,m.id,m.Address1,m.Address2,m.photo,m.PassbookNumber,m.MemberId,s.CurrentBalance,s.accountnumber,t.transactiondate,
                                             u.Fullname,t.Tran_type,t.Amount,t.CurrentBal, 
                                             (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName                                
                                             from CodeValue C
                                             inner join Members M on c.Id=m.fk_SlumId 
                                             Inner join dbo.SavingAccount s on m.Id=s.fk_MemberId 
                                             inner join dbo.SavingAccountTransactions t on s.Id=t.fk_Accountid 
                                             inner join dbo.Users u 
                                             on t.fk_AgentId=u.Id and m.Id=@value and t.TransactionDate>=@theDate;";

                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@value", Convert.ToInt32(cmbMember.SelectedValue));
                        cmd.Parameters.Add("@theDate", dateTimePicker1.Value.ToShortDateString());
                        // dateTimePicker1.Value.ToShortDateString();
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                    }
                    //this.reportViewer1.LocalReport.ReportEmbeddedResource = "Biometric.Reports.MemberDetails2.rdlc";
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.SavingAccTransactionReport.rdlc";



                    this.reportViewer1.LocalReport.EnableHyperlinks = true;
                    this.reportViewer1.LocalReport.DataSources.Add(
                       new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));



                    this.reportViewer1.RefreshReport();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }




        private void btnSubmit_Click(object sender, System.EventArgs e)
        {
            
            reportViewer1.Reset();
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessage.Text = strMessage;
                }
                else
                {
                    lblMessage.Text = string.Empty;
                    // Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                    string theDate = dateTimePicker1.Value.ToString("MM/dd/yyyy");
                    string memberName = Convert.ToString(cmbMember.Text);
                    string transType = Convert.ToString(cmbTransType.Text);
                    String today = DateTime.Now.ToString("MM/dd/yyyy");
                    if (checkBox1.Checked == true && cmbCountry.SelectedIndex>0 && cmbDistrict.SelectedIndex>0 && cmbState.SelectedIndex>0 && cmbHub.SelectedIndex>0)
                    {
                        DateTime dt1 = this.dateTimePicker1.Value.Date;
                        DateTime dt2 = DateTime.Now;
                        int result = DateTime.Compare(dt1, dt2);
                        if (result <= 0)
                        {
                            if (theDate == today)
                            {
                                if (memberName != "--Select--")
                                {
                                    if (transType == "DEB")
                                    {

                                        getDebitDetails();
                                    }
                                    else if (transType == "CRE")
                                    {

                                        getCreditDetails();
                                    }
                                    else if (transType == "BOTH")
                                    {
                                        getBothDetails();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please select Transaction Type", "Trans_Type is must",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Please select Member", "Member is must",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                if (memberName != "--Select--")
                                {
                                    if (transType == "DEB")
                                    {

                                        getDebitDetails1();
                                    }
                                    else if (transType == "CRE")
                                    {

                                        getCreditDetails1();
                                    }
                                    else if (transType == "BOTH")
                                    {
                                        getBothDetails();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please select Transaction Type", "Trans_Type is must",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Please select Member", "Member is must",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Not a valid Date", "Date",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (cmbCountry.SelectedIndex > 0 && cmbDistrict.SelectedIndex > 0 && cmbState.SelectedIndex > 0 && cmbHub.SelectedIndex > 0)
                    {
                        DateTime dt1 = this.dateTimePicker1.Value.Date;
                        DateTime dt2 = DateTime.Now;
                        int result = DateTime.Compare(dt1, dt2);
                        if (result <= 0)
                        {
                            if (theDate == today)
                            {
                                if (memberName != "--Select--")
                                {
                                    if (transType == "DEB")
                                    {

                                        getDebitDetails();
                                    }
                                    else if (transType == "CRE")
                                    {

                                        getCreditDetails();
                                    }
                                    else if (transType == "BOTH")
                                    {
                                        getBothDetails();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please select Transaction Type", "Trans_Type is must",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Please select Member", "Member is must",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                if (memberName != "--Select--")
                                {
                                    if (transType == "DEB")
                                    {

                                        getDebitDetails1();   
                                    }
                                    else if (transType == "CRE")
                                    {

                                        getCreditDetails1();
                                    }
                                    else if (transType == "BOTH")
                                    {
                                        getBothDetails();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please select Transaction Type", "Trans_Type is must",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Please select Member", "Member is must",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }


                        }
                        else
                        {
                            MessageBox.Show("Not a valid Date", "Date",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            
        }

        private void cmbTransType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dateTimePicker1.Value = System.DateTime.Now;
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }
    }
}
