﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class SearchMemberFamily : Form
    {
        public SearchMemberFamily()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        private void SearchMemberFamily_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'memberFamilyDataset.MemberFamily' table. You can move, or remove it, as needed.
            //this.memberFamilyTableAdapter.Fill(this.memberFamilyDataset.MemberFamily);

            try
            {

                // ClearControls();
                int CodeType = 0;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {

                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);

                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();

                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }

                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                        cmbcountry.SelectedValue = Id;
                        cmbcountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                        cmbstate.SelectedValue = Id;
                        cmbstate.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                        cmbdistrict.SelectedValue = Id;
                        cmbdistrict.Enabled = false;
                    }

                }

            }
            catch
            {


            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }

        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Taluk
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void lblSearch_Click(object sender, EventArgs e)
        {

            //private bool isMemberSearched = true;
            MemberFamily objMemberfamily = new MemberFamily();
            objMemberfamily.FamilyheadName = txtName.Text.Trim();
            objMemberfamily.fk_HubId = cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (int?)null;
            objMemberfamily.fk_CountryId = cmbcountry.SelectedIndex > 0 ? Convert.ToInt16(cmbcountry.SelectedValue.ToString()) : (int?)null;
            objMemberfamily.fk_StateId = cmbstate.SelectedIndex > 0 ? Convert.ToInt32(cmbstate.SelectedValue.ToString()) : (int?)null;
            objMemberfamily.fk_DistricId = cmbdistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbdistrict.SelectedValue.ToString()) : (int?)null;
            objMemberfamily.fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?)null;
         

            DataSet dsMembers = objMemberfamily.SearchMemberFamily();
            if (dsMembers.Tables[0].Rows.Count > 0)
            {
                //dsMembers.Tables[0].Rows[0]["Name"] = (cmbslum.SelectedText).ToString();
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = dsMembers.Tables[0];
            }
            if (dsMembers.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Member family Does not Exist!");
            }
        }

        private void dgMembers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private bool OpenFormsFound()
        {
            bool bOpenForms = false;
            //if (this.scMain.Panel1.Controls.Count > 0)
            //{
            //    bOpenForms = true;
            //}
            return bOpenForms;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CreateMemberFamily frmCreateMemberFamily = new CreateMemberFamily();
            MemberFamily objMemberFetch = new MemberFamily();
            if (e.RowIndex >= 0)
            {
                objMemberFetch.MemberFamilyId = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                GlobalValues.FamilyMember_PkId = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            }

            frmCreateMemberFamily.intMemberOperationType = 2;

            frmCreateMemberFamily.InitializePage();

            frmCreateMemberFamily.LoadControls(objMemberFetch);
            frmCreateMemberFamily.MdiParent = this.ParentForm;
              frmCreateMemberFamily.Parent = this.Parent;
              frmCreateMemberFamily.WindowState = FormWindowState.Maximized;
            frmCreateMemberFamily.StartPosition = FormStartPosition.Manual;
           
            frmCreateMemberFamily.Show();
            this.Close();



        }



    }
}
