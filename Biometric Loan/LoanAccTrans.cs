﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using SPARC;
using System.Windows.Forms;

namespace SPARC
{
    public partial class frmLoanAccTrans : Form
    {
        
        private Int32 iSavDepID = 0;
        private Int32 iSavDepIDSaving = 0;
        public string TransType = string.Empty;
        private double OldAmountForUpdate = 0;
        private double totalAmount_Scheduled;
        private double totalEMI_Paid;
        private double CurrentBalance = 0;
        public frmLoanAccTrans()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void RetrieveLoanName()
        {
            CodeValues objLoanName = new CodeValues();
            objLoanName.fk_memberId = GlobalValues.Member_PkId;
            DataSet dsLoangname = objLoanName.GetLoanName();
            if (dsLoangname != null && dsLoangname.Tables.Count > 0 && dsLoangname.Tables[0].Rows.Count > 0)
            {
                cmbLoanType.DisplayMember = "Name";
                cmbLoanType.ValueMember = "Id";
                cmbLoanType.DataSource = dsLoangname.Tables[0];
            }
        }

        private void LoanAccTrans_Load(object sender, EventArgs e)
        {
            try
            {
               // FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbLoanType); //Fill Loantype     
                RetrieveLoanName();
                //dtpOpenDate.MaxDate = DateTime.Now;

                //  edit by pawan
                dtpOpenDate.MaxDate = DateTime.Today.AddDays(1);

                //  edit by pawan end

                if (TransType == Constants.CREDITTRANSABBREV)//EMI payment
                {
                    this.Text = Constants.EMIPAYLOANTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                    //lblPendingCharge.Visible = true;
                    //lblPendingChargeVal.Visible = true;
                    //lblPendingPrincipal.Visible = true;
                    //lblPendingPrincipalVal.Visible = true;
                    lblRemainingPrincipal.Visible = true;
                    lblRemainingPrincipalVal.Visible = true;
                    lblEMIDue.Visible = true;
                    lblEMIDueVal.Visible = true;
                    chkPayFromSaving.Checked = false;
                }
                else//dispersal
                {
                    dgLoanDeposit.Columns[8].Visible = false;
                    chkPayFromSaving.Visible = false;
                    this.Text = Constants.LOANDISPERSALTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                    //lblPendingCharge.Visible = false;
                    //lblPendingChargeVal.Visible = false;
                    //lblPendingPrincipal.Visible = false;
                    //lblPendingPrincipalVal.Visible = false;
                    lblRemainingPrincipal.Visible = false;
                    lblRemainingPrincipalVal.Visible = false;
                    lblEMIDue.Visible = false;
                    lblEMIDueVal.Visible = false;
                    lblLastPaidDate.Visible = false;
                    lblLastPaidDateVal.Visible = false;
                    lblChargesEMIPaid.Visible = false;
                    lblChargesEMIPaidVal.Visible = false;
                    lblPrincipalEMIPaid.Visible = false;
                    lblPrincipalEMIPaidVal.Visible = false;
                    lblPendingCharge.Visible = false;
                    lblPendingChargeVal.Visible = false;
                    lblPendingPrincipal.Visible = false;
                    lblPendingPrincipalVal.Visible = false;
                    grpdivision.Visible = false;
                    radioChargesFull.Visible = false;
                    radioChargesPartwise.Visible = false;
                }

               // lblAccValue.Text = GlobalValues.Loan_AccNumber;
                FillAgents(true);

                ClearControls();
             //   LoadLoanAccTrans();

                if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && TransType == Constants.CREDITTRANSABBREV)
                {
                    txtPrincipalAmt.Enabled = true;
                    txtChargeAmt.Enabled = true;
                }
                else
                {
                    txtChargeAmt.Enabled = false;
                    txtPrincipalAmt.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private string DoVal()
        {
            string ErrorMsg = string.Empty;
            if (chkPayFromSaving.Checked == true)
            {
                if (Convert.ToDouble(txtAmount.Text) > Convert.ToDouble(CurrentBalance))
                    ErrorMsg += Messages.AMOUNTTHRUSAVINGS + Messages.COMMAWITHSPACE;

            }
            return ErrorMsg;
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            //CR-13 Start
            LoanAccountTransactions objLoanTrans1 = new LoanAccountTransactions();
            objLoanTrans1.fk_Accountid = GlobalValues.Loan_PkId;
            objLoanTrans1.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans1.GetLoanTransactions();

            //CR-13 End

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;
            if ((txtAmount.Text.Trim().Length == 0) || Convert.ToDouble(txtAmount.Text) <= 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;            
            else if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && TransType == Constants.CREDITTRANSABBREV)
            {
                if (((txtPrincipalAmt.Text.Trim().Length == 0 || Convert.ToDouble(txtPrincipalAmt.Text) <= 0)) && (((txtChargeAmt.Text.Trim().Length == 0) || Convert.ToDouble(txtChargeAmt.Text) <= 0)))
                    ErrorMsg += Messages.PRINCIPALORCHARGEAMOUNT + Messages.COMMAWITHSPACE;
            }
            
            //CR-13 Start
             if (radioChargesFull.Checked == true)
            {
                if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count == 0)
                {
                    if (Convert.ToDouble(txtAmount.Text) < Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["totalcharges"]))
                    {
                        ErrorMsg += Messages.AMOUNTCHARGES + Messages.COMMAWITHSPACE;
                        //MessageBox.Show("Amount is less than Charges to be paid, Please enter correct Amount", "Important Message");
                    }
                }

            }
             //CR-13 End

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                if (TransType == Constants.CREDITTRANSABBREV)//EMI PAYMENT
                {
                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && (Convert.ToDouble(txtPrincipalAmt.Text) + Convert.ToDouble(txtChargeAmt.Text) != Convert.ToDouble(txtAmount.Text)))
                    {
                        ErrorMsg = Messages.AMTUNEQUAL;
                        lblMessages.ForeColor = Color.Red;
                    }

                    if (Convert.ToDouble(lblDispersalBalVal.Text) > 0)
                    {
                        ErrorMsg = Messages.DISPERSALPENDING;
                        lblMessages.ForeColor = Color.Red;
                    }
                    //check if amount is exceeding the pending needs to be paid
                    if (iSavDepID == 0 && (Convert.ToDouble(txtAmount.Text) > (Convert.ToDouble(lblRemainingPrincipalVal.Text) + Convert.ToDouble(lblPendingChargeVal.Text))))//if(Convert.ToDouble(txtAmount.Text)>(totalAmount_Scheduled-totalEMI_Paid))
                    {
                        
                        ErrorMsg = Messages.EMIEXCEEDSLIMIT;
                        lblMessages.ForeColor = Color.Red;
                    }
                    else if ((iSavDepID > 0) && (Convert.ToDouble(txtAmount.Text) > (OldAmountForUpdate + Convert.ToDouble(lblPendingChargeVal.Text) + Convert.ToDouble(lblRemainingPrincipalVal.Text))))
                    {
                        ErrorMsg = Messages.EMIEXCEEDSLIMIT;
                        lblMessages.ForeColor = Color.Red;
                    }
                }

                if (TransType != Constants.CREDITTRANSABBREV)//DISPERSAL
                {
                    //Checks disperse limit during insert
                    if (iSavDepID == 0 && Convert.ToDouble(txtAmount.Text) > Convert.ToDouble(lblDispersalBalVal.Text))
                    {
                        ErrorMsg = Messages.DISPERSLIMITCHK;
                        lblMessages.ForeColor = Color.Red;
                    }
                    //Checks disperse limit during update
                    //else if (iUPFAccID > 0 && ((Convert.ToDouble(lblDispersalBalVal.Text) - Convert.ToDouble(txtAmount.Text)) + OldAmountForUpdate) > Convert.ToDouble(lblDispersalBalVal.Text))
                    else if (iSavDepID > 0 && (Convert.ToDouble(lblDispersalBalVal.Text) + OldAmountForUpdate) < Convert.ToDouble(txtAmount.Text))
                    {
                        ErrorMsg = Messages.DISPERSLIMITCHK;
                        lblMessages.ForeColor = Color.Red;
                    }
                }
            }
            return ErrorMsg;
        }
        /// <summary>
        /// Loads transaction history grid and shows loan key information on the top
        /// </summary>
        private void LoadLoanAccTrans()
        {
            //Loads the grid
            LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
            objLoanTrans.fk_Accountid = GlobalValues.Loan_PkId;
            objLoanTrans.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();
            if (dsDepositTrans != null)
            {
                if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count > 0)
                {
                    dgLoanDeposit.AutoGenerateColumns = false;
                    dgLoanDeposit.DataSource = dsDepositTrans.Tables[0];
                }
                else
                {
                    dgLoanDeposit.DataSource = null;                    
                }
                //if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count == 0)
                //{
                //    lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
                //}
                //else lblPendingChargeVal.Text = "0";

                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {
                    lblDispersalBalVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingDispersal"].ToString();
                    lblLoanAmtVal.Text = dsDepositTrans.Tables[1].Rows[0]["LoanAmount"].ToString();

                    lblRemainingPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["RemainingPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["RemainingPrincipal"].ToString();
                    lblPendingPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingPrincipal"].ToString();//to be checked and included

                    //CR-13 Start
                    if (radioChargesPartwise.Checked == true)
                    {
                        lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString();
                    }

                    if (radioChargesFull.Checked == true)
                    {
                        if (dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString())
                        {
                            lblPendingChargeVal.Text = "0";
                        }

                        if (Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"]) > 0)
                        {
                            lblPendingChargeVal.Text = "0";
                        }
                        else
                            lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
                    }

                    //CR-13 END
                    lblPrincipalEMIPaidVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPrincipalPaid"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIPrincipalPaid"].ToString();
                    lblChargesEMIPaidVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString();
                    totalEMI_Paid = (dsDepositTrans.Tables[1].Rows[0]["EMITotalPaid"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[1].Rows[0]["EMITotalPaid"].ToString()));

                    lblEMIDueVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIDueDate"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIDueDate"]).ToString("dd-MMM-yyyy") : "NA";//4

                    lblEMIPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIPrincipal"].ToString();
                    //lblEMIChargesVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMICharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMICharges"].ToString();
                    lblEMIStartDateVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPlannedStart"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIPlannedStart"]).ToString("dd-MMM-yyyy") : "NA"; ;
                    lblEMIEndDateVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPlannedEnd"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIPlannedEnd"]).ToString("dd-MMM-yyyy") : "NA";
                    dtpOpenDate.MinDate = Convert.ToDateTime(lblEMIStartDateVal.Text);
                }
                else
                {
                    MessageBox.Show("Loan Account does not exist for this type");
                }
               
                if (dsDepositTrans.Tables.Count > 2 && dsDepositTrans.Tables[2].Rows.Count > 0)
                {
                    lblLastPaidDateVal.Text = dsDepositTrans.Tables[2].Rows[0]["LastPaidDate"]!=DBNull.Value ?Convert.ToDateTime(dsDepositTrans.Tables[2].Rows[0]["LastPaidDate"]).ToString("dd-MMM-yyyy"):"NA";
                }
                //if (dsDepositTrans.Tables.Count > 4 && dsDepositTrans.Tables[1].Rows.Count > 0)
                //{
                //}

                //if (dsDepositTrans.Tables.Count > 5 && dsDepositTrans.Tables[5].Rows.Count > 0)
                //{
                //}
                if (TransType == Constants.CREDITTRANSABBREV)//EMI PAYMENT
                {
                    if (dsDepositTrans.Tables.Count > 5 && dsDepositTrans.Tables[5].Rows.Count > 0)
                    {
                        //totalAmount_Scheduled = (dsDepositTrans.Tables[5].Rows[0]["EMITotal"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[5].Rows[0]["EMITotal"].ToString())) * (dsDepositTrans.Tables[5].Rows[0]["Tenure"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[5].Rows[0]["Tenure"].ToString()));
                    }
                }
            }
            
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            if (cmbAgentName.Items.Count > 0) cmbAgentName.SelectedIndex = 0;
            txtRemarks.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtPrincipalAmt.Text = string.Empty;
            txtChargeAmt.Text = string.Empty;
            dtpOpenDate.Value = DateTime.Now.Date;

            iSavDepID = 0; //Create
            iSavDepIDSaving = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
            txtRemarks.Enabled = true;
            chkPayFromSaving.Checked = false;
        }

        private void SaveTrans()
        {            
            SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();

                        objSavingsDepTrans.Id = iSavDepIDSaving;
                        objSavingsDepTrans.fk_MemberId = GlobalValues.Member_PkId;
                        objSavingsDepTrans.Tran_type = "DEB";
                        objSavingsDepTrans.fk_Accountid = GlobalValues.Savings_PkId;
                        objSavingsDepTrans.Amount = Convert.ToDouble(txtAmount.Text);
                        objSavingsDepTrans.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);

                        //DateTime now = dtpOpenDate.Value;
                        //dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                        objSavingsDepTrans.TransactionDate = dtpOpenDate.Value;
                        //objSavingsDepTrans.TransactionDate = dtpOpenDate.Value;
                        objSavingsDepTrans.Remarks = "Paid Towards Loan";
                        objSavingsDepTrans.Createdby = GlobalValues.User_PkId;
                        objSavingsDepTrans.Updatedby = GlobalValues.User_PkId;

                        int iResultSav = 0;
                        if (iSavDepIDSaving == 0)
                        {
                            if (chkPayFromSaving.Checked == true)
                            {
                                if (Convert.ToDouble(txtAmount.Text) > Convert.ToDouble(CurrentBalance))
                                {
                                    MessageBox.Show("Amount Exceeds More Than Current Balance or check loan and saving type whether  they are same type", "SPARC", MessageBoxButtons.OK, MessageBoxIcon.Question);
                                    return;
                                }
                            }                                                                                
                                iResultSav = objSavingsDepTrans.SaveSavingsDepositTransactions();                                                       
                        }
                        else
                            iResultSav = objSavingsDepTrans.UpdateSavingsDepositTransactions();

                        LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();

                        objLoanTrans.Id = iSavDepID;
                        objLoanTrans.fk_MemberId = GlobalValues.Member_PkId;
                        objLoanTrans.Tran_type = TransType;
                        objLoanTrans.fk_Accountid = GlobalValues.Loan_PkId;
                        objLoanTrans.Amount = Convert.ToDouble(txtAmount.Text);
                        objLoanTrans.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                        objLoanTrans.TransactionDate = dtpOpenDate.Value;

                        //DateTime now1 = dtpOpenDate.Value;
                        //dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now1.Hour,now1.Minute,now1.Second);
                        //objLoanTrans.TransactionDate = dtpOpenDate.Value;
                                   
                        objLoanTrans.PrinciplePart = txtPrincipalAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtPrincipalAmt.Text) : 0;
                        objLoanTrans.InterestPart = txtChargeAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtChargeAmt.Text) : 0;
                        objLoanTrans.Remarks = iResultSav.ToString();
                        objLoanTrans.Createdby = GlobalValues.User_PkId;
                        objLoanTrans.Updatedby = GlobalValues.User_PkId;
                        objLoanTrans.PaidFromSavings = true;

                        int iResult = 0;
                        if (iSavDepID == 0)
                            iResult = objLoanTrans.SaveLoanAccTransactions();
                        else
                            iResult = objLoanTrans.UpdateLoanTransactions();

                        if (iResult > 0)
                        {
                            if (objLoanTrans.Id == 0)
                            {
                                //btnSave.Text = Constants.UPDATE;
                                //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);
                                //iUPFAccID = iResult;
                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            else
                            {
                                //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                                lblMessages.Text = Messages.UPDATED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            LoadLoanAccTrans();
                            ClearControls();
                        }
                        else
                        {
                            lblMessages.Text = Messages.ERROR_PROCESSING;
                            lblMessages.ForeColor = Color.Red;
                            //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                        }
        }
    

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //LoanAccountTransactions objLoanTrans1 = new LoanAccountTransactions();
                //objLoanTrans1.fk_Accountid = GlobalValues.Loan_PkId;
                //objLoanTrans1.Tran_type = TransType;
                //DataSet dsDepositTrans = objLoanTrans1.GetLoanTransactions();
                //if (radioChargesFull.Checked == true)
                //{
                //    if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count == 0)
                //    {
                //        if (Convert.ToDouble(txtAmount.Text) < Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["totalcharges"]))
                //        {
                //            MessageBox.Show("Amount is less than Charges to be paid, Please enter correct Amount", "Important Message");
                //        }
                //    }

                //}

                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    if (chkPayFromSaving.Checked == true)
                    {
                        try
                        {
                            SaveTrans();
                        }
                        catch (Exception ex)
                        {
                            Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                        }                        
                    }
                    else
                    {
                        LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();

                        objLoanTrans.Id = iSavDepID;
                        objLoanTrans.fk_MemberId = GlobalValues.Member_PkId;
                        objLoanTrans.Tran_type = TransType;
                        objLoanTrans.fk_Accountid = GlobalValues.Loan_PkId;
                        objLoanTrans.Amount = Convert.ToDouble(txtAmount.Text);
                        objLoanTrans.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                        
                        //  edit by pawan
                        DateTime now = DateTime.Now;
                        dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                        objLoanTrans.TransactionDate = dtpOpenDate.Value;

                        //  edit by pawan  end
                        objLoanTrans.TransactionDate = dtpOpenDate.Value;
                        objLoanTrans.PrinciplePart = txtPrincipalAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtPrincipalAmt.Text) : 0;
                        objLoanTrans.InterestPart = txtChargeAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtChargeAmt.Text) : 0;
                        objLoanTrans.Remarks = txtRemarks.Text;
                        objLoanTrans.Createdby = GlobalValues.User_PkId;
                        objLoanTrans.Updatedby = GlobalValues.User_PkId;
                        objLoanTrans.PaidFromSavings = false;

                        int iResult = 0;
                        if (iSavDepID == 0)
                            iResult = objLoanTrans.SaveLoanAccTransactions();
                        else
                            iResult = objLoanTrans.UpdateLoanTransactions();

                        if (iResult > 0)
                        {
                            if (objLoanTrans.Id == 0)
                            {
                                //btnSave.Text = Constants.UPDATE;
                                //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);
                                //iUPFAccID = iResult;
                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            else
                            {
                                //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                                lblMessages.Text = Messages.UPDATED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            LoadLoanAccTrans();
                            ClearControls();
                            frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                            objBioMetric.InsertMemberDetailsToGrid();
                        }
                        else
                        {
                            lblMessages.Text = Messages.ERROR_PROCESSING;
                            lblMessages.ForeColor = Color.Red;
                            //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
               // Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                MessageBox.Show("Please enter data to save");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                GlobalValues.Loan_PkId = 0;
                GlobalValues.Loan_AccNumber = "";
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgLoanDeposit_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtRemarks.Enabled = true;
                chkPayFromSaving.Checked = false;
                iSavDepIDSaving = 0;
                cmbAgentName.SelectedValue = dgLoanDeposit.Rows[e.RowIndex].Cells["AgentID"].Value;
                dtpOpenDate.Value = Convert.ToDateTime(dgLoanDeposit.Rows[e.RowIndex].Cells["TransDate"].Value);
                txtAmount.Text = dgLoanDeposit.Rows[e.RowIndex].Cells["Amount"].Value.ToString();

                if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && TransType == Constants.CREDITTRANSABBREV)
                {
                    txtPrincipalAmt.Text = dgLoanDeposit.Rows[e.RowIndex].Cells["PrinciplePart"].Value.ToString();
                    txtChargeAmt.Text = dgLoanDeposit.Rows[e.RowIndex].Cells["InterestPart"].Value.ToString();
                }
                txtRemarks.Text = dgLoanDeposit.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();

                if (dgLoanDeposit.Rows[e.RowIndex].Cells["Remarks"].Value.ToString() == "Taken From Savings")
                {
                    chkPayFromSaving.Checked = true;
                }
                string ab;
                btnSave.Text = Constants.UPDATE;                
                iSavDepID = Convert.ToInt32(dgLoanDeposit.Rows[e.RowIndex].Cells["Id"].Value);
                ab = dgLoanDeposit.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                OldAmountForUpdate = Convert.ToDouble(dgLoanDeposit.Rows[e.RowIndex].Cells["Amount"].Value);
                lblMessages.Text = string.Empty;
                string al;
                al = dgLoanDeposit.Rows[e.RowIndex].Cells["PaidFromSavings"].Value.ToString();
                if (al == "True")
                {
                    iSavDepIDSaving = Convert.ToInt32(ab);
                    txtRemarks.Enabled = false;
                    chkPayFromSaving.Checked = true;
                }
            }
            catch (Exception ex)
            {
              //  Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            //return;
            try
            {
                if (txtAmount.Text.Trim().Length > 0)
                {
                    //txtChargeAmt.Text = (Convert.ToDouble(lblPendingChargeVal.Text) > 0) ? Convert.ToDouble(lblPendingChargeVal.Text) > Convert.ToDouble(txtAmount.Text) ? txtAmount.Text : lblPendingChargeVal.Text : "0";
                    //txtPrincipalAmt.Text = (Convert.ToDouble(lblPendingChargeVal.Text) > 0) ? Convert.ToDouble(lblPendingChargeVal.Text) > Convert.ToDouble(txtAmount.Text) ? "0" : (Convert.ToDouble(txtAmount.Text) - Convert.ToDouble(lblPendingChargeVal.Text)).ToString() : txtAmount.Text;
                    if (lblPendingChargeVal.Text.Trim() != "0")//to be checked
                    {
                        if (Convert.ToDouble(lblPendingChargeVal.Text) >= Convert.ToDouble(txtAmount.Text))
                        {
                            txtChargeAmt.Text = txtAmount.Text;
                            txtPrincipalAmt.Text = "0";
                        }
                        else
                        {
                            txtChargeAmt.Text = lblPendingChargeVal.Text;
                            txtPrincipalAmt.Text = Convert.ToString(Convert.ToDouble(txtAmount.Text) - Convert.ToDouble(lblPendingChargeVal.Text));
                        }
                    }
                    else
                    {
                        txtPrincipalAmt.Text = txtAmount.Text;
                        txtChargeAmt.Text = "0";
                    }
                }
                else
                {
                    txtPrincipalAmt.Text = "";
                    txtChargeAmt.Text = "";
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void DeleteData(Int32 DataId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            LoanAccountTransactions objLoanAccTrans = new LoanAccountTransactions();
            objLoanAccTrans.Id = DataId;

            dsDeleteStatus = objLoanAccTrans.DeleteLoanAccTrans(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadLoanAccTrans();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgLoanDeposit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgLoanDeposit.Columns[e.ColumnIndex].Index == 9)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgLoanDeposit.CurrentRow.Cells[0].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgLoanDeposit_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
            {
                dgLoanDeposit.Columns["btnDelete"].Visible = false;
            }
        }

//CR-13 Start
        private void radioChargesFull_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
                objLoanTrans.fk_Accountid = GlobalValues.Loan_PkId;
                objLoanTrans.Tran_type = TransType;
                DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();

                if (dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString())
                {
                    lblPendingChargeVal.Text = "0";
                }

                if (Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"]) > 0)
                {
                    lblPendingChargeVal.Text = "0";
                }
                else
                    lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();

                // lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
            }
            catch (IndexOutOfRangeException)
            {

            }
        }

        private void radioChargesPartwise_CheckedChanged(object sender, EventArgs e)
        {
            LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
            objLoanTrans.fk_Accountid = GlobalValues.Loan_PkId;
            objLoanTrans.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();
            if (dsDepositTrans != null)
            {                
                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {

                    lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString();
                }
            }
        }
        //CR-13 END
        private void chkPayFromSaving_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPayFromSaving.Checked == true)
            {
                LoanAccountTransactions objLoanAccTra = new LoanAccountTransactions();
                objLoanAccTra.fk_MemberId = GlobalValues.Member_PkId;
                objLoanAccTra.SavingType = Convert.ToInt64(cmbLoanType.SelectedValue);
                DataSet dsLoanTrns = objLoanAccTra.GetSavingsCurrBalance();
                if (dsLoanTrns.Tables.Count > 0 && dsLoanTrns.Tables[0].Rows.Count > 0)
                {
                    CurrentBalance = Convert.ToDouble(dsLoanTrns.Tables[0].Rows[0][0].ToString());
                }
                else if (dsLoanTrns.Tables.Count <= 0)
                {
                    MessageBox.Show("Saving Account of this type does not exist");
                }
                txtRemarks.Enabled = false;
                //txtRemarks.Text = "Taken From Savings";
            }

            if (chkPayFromSaving.Checked == false)
            {
                txtRemarks.Enabled = true;
                txtRemarks.Text = string.Empty;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtPrincipalAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtChargeAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbLoanType_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (Convert.ToInt64(cmbLoanType.SelectedValue) > 0)
            {
                ClearControls();
                lblAccValue.Text = "<>";
                lblLoanAmtVal.Text = "0";
                lblEMIStartDateVal.Text = "0";
                lblEMIPrincipalVal.Text = "0";
                lblDispersalBalVal.Text = "0";
                lblEMIEndDateVal.Text = "0";

                lblPrincipalEMIPaidVal.Text = "0";
                lblPendingPrincipalVal.Text = "0";
                lblRemainingPrincipalVal.Text = "0";
                lblChargesEMIPaidVal.Text = "0";
                lblPendingChargeVal.Text = "0";
                lblLastPaidDateVal.Text = "NA";
                lblEMIDueVal.Text = "NA";

                GlobalValues.Loan_PkId = 0;
                GlobalValues.Loan_AccNumber = "";

                LoanIdRetrieve();
                LoadLoanAccTrans();
                //objSavingsAccFetch.SavingType = Convert.ToInt64(cmbLoanType.SelectedValue);
                //LoadControls(objSavingsAccFetch);
            }
        }

        private void LoanIdRetrieve()
        {
            LoanAccountTransactions objloanIdRetrieve = new LoanAccountTransactions();
            objloanIdRetrieve.fk_MemberId = GlobalValues.Member_PkId;
            objloanIdRetrieve.LoanType = Convert.ToInt64(cmbLoanType.SelectedValue);
            DataSet dsLoanId = objloanIdRetrieve.GetLoanID();
            if (dsLoanId.Tables.Count > 0 && dsLoanId.Tables[0].Rows.Count > 0)
            {
                GlobalValues.Loan_PkId = Convert.ToInt64(dsLoanId.Tables[0].Rows[0]["Id"]);
                GlobalValues.Loan_AccNumber = dsLoanId.Tables[0].Rows[0]["AccountNumber"].ToString();
                lblAccValue.Text = GlobalValues.Loan_AccNumber;
            }
                
        }

        
    }
}
