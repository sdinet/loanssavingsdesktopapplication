﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;


namespace SPARC
{
    public partial class GroupInfo : Form
    {
        // SqlConnection con = new SqlConnection("Data Source=AIT5-PC\\SQLEXPRESS;Initial Catalog=Loans_and_savings_sajjanRao;User ID=sa;Password=123456");
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        SqlCommand cmd;
        SqlDataAdapter adapt;
        public Int32 rowindex;
           
        public GroupInfo()
        {
            InitializeComponent();

        }
        public Int16 intMemberOperationType;
        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (m.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = m.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }
            base.WndProc(ref m);
        }
        private void Newform_Load(object sender, EventArgs e)
        {
            ClearControls();
            lblMessage.Visible = false;
            
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            MaximizeBox = false;
            MaximizeBox = false;

          
            try
            {
                lblMessage.Visible = true;
                FillCombos(Convert.ToInt32(LocationCode.WithdrawalReasons), 0, cmb_wr);
                int CodeType = 0;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {
                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                   
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }
            
                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }

                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }

                FillCombos(Convert.ToInt32(LocationCode.WithdrawalReasons), 0, cmb_wr);

                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {
                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbohub);
                }
                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {

                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }


                Users objjUsers = new Users();

                DataSet dssvalues = objjUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);

                foreach (DataRow dr in dssvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }
                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbohub);
                        cmbohub.SelectedValue = Id;
                        cmbohub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbocountry);
                        cmbocountry.SelectedValue = Id;
                        cmbocountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbostate);
                        cmbostate.SelectedValue = Id;
                        cmbostate.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbodis);
                        cmbodis.SelectedValue = Id;
                        cmbodis.Enabled = false;
                    }
                   
                        
                }
            }

            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }

        }
      


        public void LoadControls(Group objGroupFetch)
        {
            try
            {
                DataSet dsGroupFetch = objGroupFetch.GetGroupDetails();

                if (dsGroupFetch != null && dsGroupFetch.Tables.Count > 0 && dsGroupFetch.Tables[0].Rows.Count > 0)
                {
                    GlobalValues.GroupId = Convert.ToInt32(dsGroupFetch.Tables[0].Rows[0]["Id"]);
                    txt_gname.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Group_Name"].ToString();


                    txt_eyear.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Est_year"].ToString();
                    txt_male.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Male"].ToString();
                    txt_female.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Female"].ToString();
                    txt_ts.SelectedText = dsGroupFetch.Tables[0].Rows[0]["tot_sav"].ToString();
                    txt_land.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Land"].ToString();
                    txt_LFS.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Ln_frm_sav"].ToString();
                    txt_LFSR.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Ln_frm_sav_rep"].ToString();
                    txt_Int.SelectedText = dsGroupFetch.Tables[0].Rows[0]["interest"].ToString();
                    txt_cash.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Cash"].ToString();
                    txt_short.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Short"].ToString();
                    txt_bank.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Bank"].ToString();
                    txt_bnkc.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Bnk_chrg"].ToString();
                    txt_bt.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Busi_type"].ToString();
                    txt_bto.SelectedText = dsGroupFetch.Tables[0].Rows[0]["Busi_typeother"].ToString();

                    cmbHub.SelectedValue = Convert.ToInt16(dsGroupFetch.Tables[0].Rows[0]["fk_hubid"]);
                    cmbCountry.SelectedValue = dsGroupFetch.Tables[0].Rows[0]["fk_countryid"];
                    cmbState.SelectedValue = Convert.ToInt16(dsGroupFetch.Tables[0].Rows[0]["fk_stateid"]);
                    cmbDistrict.SelectedValue = Convert.ToInt16(dsGroupFetch.Tables[0].Rows[0]["fk_districtid"]);
                    cmbSlum.SelectedValue = Convert.ToInt16(dsGroupFetch.Tables[0].Rows[0]["fk_SlumId"]);
                    cmb_wr.SelectedValue = Convert.ToInt16(dsGroupFetch.Tables[0].Rows[0]["wd_reasons"]);
                }
            }
            catch (Exception e)
            {
            }
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }

        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void cmbHub_SelectedIndexChanged_1(object sender, EventArgs e)
        {
           
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbSlum_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




        private void btn_update_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbSlum.SelectedIndex > 0)
            {
                Group ObjPassCheck = new Group();
                ObjPassCheck.GroupName = txt_gname.Text.Trim();
                ObjPassCheck.fk_SlumId = cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (int?)null;
                ObjPassCheck.OperationType = intMemberOperationType;
                ObjPassCheck.GroupId = GlobalValues.GroupId;
                DataSet dsUnGroup = new DataSet();
                dsUnGroup = ObjPassCheck.FetchGroup();
                if (dsUnGroup.Tables.Count > 0 && dsUnGroup.Tables[0].Rows.Count > 0)
                {
                    ErrorMsg = Messages.MULTIPLEGROUPNAMES;
                }

            }
           // Group objgp = new Group();
            
            if (txt_gname.Text.Trim().Length == 0)
                ErrorMsg += Messages.Groupname + Messages.COMMAWITHSPACE;
            if (cmbSlum.SelectedIndex <= 0)
                    ErrorMsg += Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
              

                if (ErrorMsg.Trim().Length > 0)
                {
                    ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                    ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                    lblMessage.ForeColor = Color.Red;
                }

            
            return ErrorMsg;
        }



        private void txt_eyear_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txt_male_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_male_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txt_female_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txt_ts_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txt_LFS_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }

        }

        private void txt_land_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_Int_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_cash_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_short_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_short_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_bank_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txt_bt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void txt_bnkc_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_gname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void ClearControls()
        {
 
            txt_gname.Text = string.Empty;
            cmbHub.Text = string.Empty;
            cmbCountry.Text = string.Empty;
            cmbState.Text = string.Empty;
            cmbDistrict.Text = string.Empty;
            cmbSlum.Text = string.Empty;
            txt_eyear.Text = string.Empty;
            txt_male.Text = string.Empty;
            txt_female.Text = string.Empty;
            txt_ts.Text = string.Empty;
            txt_land.Text = string.Empty;
            txt_LFS.Text = string.Empty;
            txt_LFSR.Text = string.Empty;
            txt_Int.Text = string.Empty;
            txt_cash.Text = string.Empty;
            txt_short.Text = string.Empty;
            txt_bank.Text = string.Empty;
            txt_bnkc.Text = string.Empty;
            txt_bt.Text = string.Empty;
            txt_bto.Text = string.Empty;
            cmb_wr.Text = string.Empty;
            lblMessages.Text = string.Empty;
            lblMessage.Text = string.Empty;
            GlobalValues.GroupId = 0;

        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                int CodeType = 0;
                FillCombos(Convert.ToInt32(LocationCode.WithdrawalReasons), 0, cmb_wr);
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {

                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }
                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            Group objgroupfetch = new Group();
            if (e.RowIndex >= 0)
            {
                objgroupfetch.GroupId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());

            }

            try
            {
                ClearControls();
                LoadControls(objgroupfetch);

                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y);

                MaximizeBox = false;
                MaximizeBox = false;
                dataGridView1.MultiSelect = false;
                dataGridView1.ReadOnly = true;


            }

            catch (Exception ex)
            {
              
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbohub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbohub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbohub.SelectedValue), cmbocountry);
            else
                cmbocountry.DataSource = null;
        }

        private void cmbocountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbocountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbocountry.SelectedValue), cmbostate);
            else
                cmbostate.DataSource = null;
        }

        private void cmbodis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbodis.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbodis.SelectedValue), cmboslum);
            else
                cmboslum.DataSource = null;

        }
        private void cmbostate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbostate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbostate.SelectedValue), cmbodis);
            else
                cmbodis.DataSource = null;
        }
        public void Intializesearchform()
        {

        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            Group objgrp = new Group();
            objgrp.GroupName = txt_grpname.Text.Trim();
            objgrp.fk_HubId = cmbohub.SelectedIndex > 0 ? Convert.ToInt32(cmbohub.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_CountryId = cmbocountry.SelectedIndex > 0 ? Convert.ToInt16(cmbocountry.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_StateId = cmbostate.SelectedIndex > 0 ? Convert.ToInt32(cmbostate.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_DistricId = cmbodis.SelectedIndex > 0 ? Convert.ToInt32(cmbodis.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_SlumId = cmboslum.SelectedIndex > 0 ? Convert.ToInt32(cmboslum.SelectedValue.ToString()) : (int?)null;
            DataSet dsgroup = objgrp.SearchGroup();
            if (dsgroup.Tables[0].Rows.Count > 0)
            {
                //dsMembers.Tables[0].Rows[0]["Group_Name"] = (txt_grpname.SelectedText).ToString();
                 dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = dsgroup.Tables[0];
                //MessageBox.Show("Group searched Successfully!");
                lblMessages.Text = Messages.SEARCHEDGROUP;
                lblMessages.ForeColor = Color.Green;
                
            }
            if(dsgroup.Tables[0].Rows.Count <= 0)
            {
                //dataGridView1.Visible = false;
               // dataGridView1.Rows[0].Visible = false;
                lblMessages.Text = Messages.NOGROUPSEARCH;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            //Group objgroupfetch = new Group();
            //if (e.RowIndex >= 0)
            //{
            //    objgroupfetch.GroupId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());

            //}

        }



      

        private void cmbState_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        private void txt_eyear_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            txt_eyear.MaxLength = 4;
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;

                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }


        private void txt_land_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txt_LFS_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }



        private void txt_Int_KeyPress_1(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txt_LFSR_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_cash_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_short_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_bank_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void txt_bnkc_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_bt_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void txt_bto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void dataGridView1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
           
            Group objgroupfetch = new Group();
            if (e.RowIndex >= 0)
            {
                objgroupfetch.GroupId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());

            }

            //try
            //{
                ClearControls();
                LoadControls(objgroupfetch);
                
                    this.StartPosition = FormStartPosition.Manual;
                    this.Location = new Point(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y);

                    MaximizeBox = false;
                    MaximizeBox = false;
                    dataGridView1.MultiSelect = false;
                    dataGridView1.ReadOnly = true;


            //}

            //catch (Exception ex)
            //{
            //    Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            //}


           
        }

        private void Delete_Click(object sender, EventArgs e)
        {
           
            //DisplayData();
            //int Result;
            ////if ( Result == 1)
            ////{
            //    SqlConnection con = new SqlConnection(ConnectionString);
            //    con.Open();
            //    cmd = new SqlCommand("usp_DeleteGroup", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.AddWithValue("@GroupId", GlobalValues.GroupId);
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //    MessageBox.Show("Record Deleted Successfully");
            //}
            ////else
            ////{
            ////    lblMessage.Text = Messages.ERROR_PROCESSING;
            ////    lblMessage.ForeColor = Color.Red;
            ////}
            }

        private void btn_save_Click_1(object sender, EventArgs e)
        {
try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessage.Text = strMessage;
                }
                else
                {
                    lblMessage.Text = string.Empty;

                    Group objgroup = new Group();
                    if (intMemberOperationType == 1)
                        objgroup.GroupId = 0;
                    else
                        objgroup.GroupId = GlobalValues.GroupId;
                    objgroup.GroupId = Convert.ToInt32(GlobalValues.GroupId);
                    objgroup.GroupName = txt_gname.Text.Trim();
                    objgroup.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                    objgroup.fk_CountryId = Convert.ToInt32(cmbCountry.SelectedValue);
                    objgroup.fk_StateId = Convert.ToInt32(cmbState.SelectedValue);
                    objgroup.fk_DistricId = Convert.ToInt32(cmbDistrict.SelectedValue);
                    objgroup.fk_SlumId = Convert.ToInt32(cmbSlum.SelectedValue);

                    if (txt_eyear.Text =="")
                    {
                        objgroup.Estyear = Convert.ToInt32(null);
                    }
                    else
                        objgroup.Estyear = Convert.ToInt32(txt_eyear.Text);
                   // objgroup.Estyear = txt_eyear.Text.Trim();
                    //if (txt_male.Text == "")
                    //{
                    //    objgroup.Male = Convert.ToDouble(null);
                    //}
                    //else
                    //    objgroup.Male = Convert.ToDouble(txt_male.Text);
                    objgroup.Male = txt_male.Text.Trim().Length > 0 ? Convert.ToInt32(txt_male.Text) : 0;
                    objgroup.Female = txt_female.Text.Trim().Length > 0 ?Convert.ToInt32(txt_female.Text): 0;
                    objgroup.totsav = txt_ts.Text.Trim().Length > 0 ? Convert.ToDouble(txt_ts.Text) : 0;
                    objgroup.Land = txt_land.Text.Trim().Length > 0 ? Convert.ToDouble(txt_land.Text) : 0;
                    objgroup.LFS = txt_LFS.Text.Trim().Length > 0 ? Convert.ToDouble(txt_LFS.Text) : 0;
                    //objgroup.Land = Convert.ToDouble(txt_land.Text.Trim());
                    //objgroup.LFS = Convert.ToDouble(txt_LFS.Text.Trim());
                    objgroup.LFSR = txt_LFSR.Text.Trim().Length > 0 ? Convert.ToDouble(txt_LFSR.Text) : 0;
                    objgroup.Interest = txt_Int.Text.Trim().Length > 0 ? Convert.ToDouble(txt_Int.Text) : 0;
                    objgroup.Cash = txt_cash.Text.Trim().Length > 0 ? Convert.ToDouble(txt_cash.Text) : 0;
                    objgroup.Short = txt_short.Text.Trim().Length > 0 ? Convert.ToDouble(txt_short.Text) : 0;
                    //objgroup.Interest = Convert.ToDouble(txt_Int.Text.Trim());
                    //objgroup.Cash = Convert.ToDouble(txt_cash.Text.Trim());
                    //objgroup.Short = Convert.ToDouble(txt_short.Text.Trim());
                    objgroup.Bankchrg = txt_bnkc.Text.Trim().Length > 0 ? Convert.ToDouble(txt_bnkc.Text) : 0;
                   
                    objgroup.Bank = txt_bank.Text.Trim();
                  //  objgroup.Bankchrg = Convert.ToDouble(txt_bnkc.Text.Trim());
                    objgroup.Busitype = txt_bt.Text.Trim();
                    objgroup.Busitypeother = txt_bto.Text.Trim();
                    objgroup.Wdreasons = Convert.ToInt32(cmb_wr.SelectedValue);
                    objgroup.Createdby = GlobalValues.User_PkId;
                    objgroup.Updatedby = GlobalValues.User_PkId;
                    objgroup.UpdatedDate = DateTime.Today;
                    objgroup.CreatedDate = DateTime.Today;
                    //if (Convert.ToInt32(txt_eyear.Text) <= 0)
                    //{
                    //    objgroup.Estyear = null;
                    //}
                    //else
                    //    objgroup.Estyear =txt_eyear.Text;
                    //if (Convert.ToDouble(txt_male.Text) <= 0)
                    //{
                    //    objgroup.Male = null;
                    //}
                    //else
                    //    objgroup.Male = Convert.ToDouble(txt_male.Text);


                    int saveResult = objgroup.SaveGroup();
                    if (objgroup.GroupId == 0)
                    {
                        lblMessage.Text = Messages.SAVEGROUP;
                        lblMessage.ForeColor = Color.Green;
                        //intMemberOperationType = 2;
                        Group objgrp = new Group();
            objgrp.GroupName = txt_grpname.Text.Trim();
            objgrp.fk_HubId = cmbohub.SelectedIndex > 0 ? Convert.ToInt32(cmbohub.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_CountryId = cmbocountry.SelectedIndex > 0 ? Convert.ToInt16(cmbocountry.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_StateId = cmbostate.SelectedIndex > 0 ? Convert.ToInt32(cmbostate.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_DistricId = cmbodis.SelectedIndex > 0 ? Convert.ToInt32(cmbodis.SelectedValue.ToString()) : (int?)null;
            objgrp.fk_SlumId = cmboslum.SelectedIndex > 0 ? Convert.ToInt32(cmboslum.SelectedValue.ToString()) : (int?)null;
            DataSet dsgroup = objgrp.SearchGroup();
            if (dsgroup.Tables[0].Rows.Count > 0)
            {
                //dsMembers.Tables[0].Rows[0]["Group_Name"] = (txt_grpname.SelectedText).ToString();
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = dsgroup.Tables[0];
            }
                    }
                    else
                    {
                        lblMessage.Text = Messages.UPDATEGROUP;
                        lblMessage.ForeColor = Color.Green;
                        
                    }

                }
            }

            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        
        }

        private void btn_clear_Click_1(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                int CodeType = 0;
                FillCombos(Convert.ToInt32(LocationCode.WithdrawalReasons), 0, cmb_wr);
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {

                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }
                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dataGridView1.Columns[e.ColumnIndex].Index == 3)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32((dataGridView1.CurrentRow.Cells[2].Value)));
                            dataGridView1.Refresh();
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;
            lblMessages.Text = string.Empty;
           Group objgrp = new Group();
           objgrp.GroupId = DataId;

           dsDeleteStatus = objgrp.DeleteGroup(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    //FillCodeValues(Convert.ToInt32(cmbCodeType.SelectedValue), Convert.ToInt32(cmbParentFilter.SelectedValue));
                    ClearControls();
                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                    LoadGroup();
                    
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void LoadGroup()
        {  // private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
          

            Group objgrp = new Group();
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                using (SqlDataAdapter ad = new SqlDataAdapter("select G.Group_Name,G.Id,C.Name as SlumName from GroupInfo G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId", ConnectionString))
                {
                    DataTable dt = new DataTable();
                    ad.Fill(dt);
                    dataGridView1.DataSource = dt;
                }
            }
        }
       
        private void txt_eyear_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_male_KeyPress_1(object sender, KeyPressEventArgs e)
        {

        }

        private void txt_female_KeyPress_1(object sender, KeyPressEventArgs e)
        {

        }

        private void txt_ts_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }
                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_land_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_Int_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_bnkc_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txt_bank_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || (Keys)e.KeyChar == Keys.Space);
        }

        private void txt_female_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_gname_KeyPress_1(object sender, KeyPressEventArgs e)
        {

            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cmboslum_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txt_bt_TextChanged(object sender, EventArgs e)
        {

        }

      

       
      
       
       
            }
    }