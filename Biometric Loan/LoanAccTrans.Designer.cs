﻿namespace SPARC
{
    partial class frmLoanAccTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblInfo = new System.Windows.Forms.Label();
            this.dgLoanDeposit = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrinciplePart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InterestPart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PaidFromSavings = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbLoanType = new System.Windows.Forms.ComboBox();
            this.lblLoanType = new System.Windows.Forms.Label();
            this.chkPayFromSaving = new System.Windows.Forms.CheckBox();
            this.radioChargesPartwise = new System.Windows.Forms.RadioButton();
            this.radioChargesFull = new System.Windows.Forms.RadioButton();
            this.lblEMIChargesVal = new System.Windows.Forms.Label();
            this.grpdivision = new System.Windows.Forms.GroupBox();
            this.lblCloseBrace = new System.Windows.Forms.Label();
            this.lblOpenBrace = new System.Windows.Forms.Label();
            this.txtChargeAmt = new System.Windows.Forms.TextBox();
            this.lblChargeAmt = new System.Windows.Forms.Label();
            this.txtPrincipalAmt = new System.Windows.Forms.TextBox();
            this.lblPrincipalAmt = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grpEMISchedule = new System.Windows.Forms.GroupBox();
            this.lblEMIPrincipalVal = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblEMIEndDateVal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEMIStartDateVal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLoanAmt = new System.Windows.Forms.Label();
            this.lblLoanAmtVal = new System.Windows.Forms.Label();
            this.lblDispersalBal = new System.Windows.Forms.Label();
            this.lblDispersalBalVal = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblMessages = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.dtpOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransDate = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblPendingChargeVal = new System.Windows.Forms.Label();
            this.lblPendingCharge = new System.Windows.Forms.Label();
            this.lblPendingPrincipalVal = new System.Windows.Forms.Label();
            this.lblPendingPrincipal = new System.Windows.Forms.Label();
            this.lblChargesEMIPaidVal = new System.Windows.Forms.Label();
            this.lblChargesEMIPaid = new System.Windows.Forms.Label();
            this.lblPrincipalEMIPaidVal = new System.Windows.Forms.Label();
            this.lblPrincipalEMIPaid = new System.Windows.Forms.Label();
            this.lblEMIDueVal = new System.Windows.Forms.Label();
            this.lblEMIDue = new System.Windows.Forms.Label();
            this.lblRemainingPrincipalVal = new System.Windows.Forms.Label();
            this.lblRemainingPrincipal = new System.Windows.Forms.Label();
            this.lblLastPaidDateVal = new System.Windows.Forms.Label();
            this.lblLastPaidDate = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgLoanDeposit)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpdivision.SuspendLayout();
            this.grpEMISchedule.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(4, 417);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(142, 13);
            this.lblInfo.TabIndex = 103;
            this.lblInfo.Text = "Double click on a row to edit";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgLoanDeposit
            // 
            this.dgLoanDeposit.AllowUserToAddRows = false;
            this.dgLoanDeposit.AllowUserToDeleteRows = false;
            this.dgLoanDeposit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgLoanDeposit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgLoanDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLoanDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.PrinciplePart,
            this.InterestPart,
            this.AgentID,
            this.TransDate,
            this.AgentName,
            this.Amount,
            this.Remarks,
            this.PaidFromSavings,
            this.btnDelete});
            this.dgLoanDeposit.Location = new System.Drawing.Point(7, 434);
            this.dgLoanDeposit.Margin = new System.Windows.Forms.Padding(0);
            this.dgLoanDeposit.Name = "dgLoanDeposit";
            this.dgLoanDeposit.ReadOnly = true;
            this.dgLoanDeposit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgLoanDeposit.Size = new System.Drawing.Size(814, 201);
            this.dgLoanDeposit.TabIndex = 102;
            this.dgLoanDeposit.TabStop = false;
            this.dgLoanDeposit.VirtualMode = true;
            this.dgLoanDeposit.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgLoanDeposit_CellContentClick);
            this.dgLoanDeposit.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgLoanDeposit_CellDoubleClick);
            this.dgLoanDeposit.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgLoanDeposit_DataBindingComplete);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "ID";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // PrinciplePart
            // 
            this.PrinciplePart.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PrinciplePart.DataPropertyName = "PrinciplePart";
            this.PrinciplePart.HeaderText = "PrincipalPart";
            this.PrinciplePart.Name = "PrinciplePart";
            this.PrinciplePart.ReadOnly = true;
            this.PrinciplePart.Visible = false;
            // 
            // InterestPart
            // 
            this.InterestPart.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InterestPart.DataPropertyName = "InterestPart";
            this.InterestPart.HeaderText = "InterestPart";
            this.InterestPart.Name = "InterestPart";
            this.InterestPart.ReadOnly = true;
            this.InterestPart.Visible = false;
            // 
            // AgentID
            // 
            this.AgentID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AgentID.DataPropertyName = "fk_AgentId";
            this.AgentID.HeaderText = "AgentID";
            this.AgentID.Name = "AgentID";
            this.AgentID.ReadOnly = true;
            this.AgentID.Visible = false;
            // 
            // TransDate
            // 
            this.TransDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TransDate.DataPropertyName = "TransactionDate";
            dataGridViewCellStyle1.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.TransDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.TransDate.HeaderText = "Transaction Date";
            this.TransDate.Name = "TransDate";
            this.TransDate.ReadOnly = true;
            // 
            // AgentName
            // 
            this.AgentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AgentName.DataPropertyName = "AgentName";
            dataGridViewCellStyle2.Format = "dd/month/yyyy";
            this.AgentName.DefaultCellStyle = dataGridViewCellStyle2;
            this.AgentName.HeaderText = "Leader Name";
            this.AgentName.Name = "AgentName";
            this.AgentName.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            this.Remarks.Visible = false;
            // 
            // PaidFromSavings
            // 
            this.PaidFromSavings.DataPropertyName = "PaidFromSavings";
            this.PaidFromSavings.HeaderText = "PaidFromSavings";
            this.PaidFromSavings.Name = "PaidFromSavings";
            this.PaidFromSavings.ReadOnly = true;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbLoanType);
            this.panel1.Controls.Add(this.lblLoanType);
            this.panel1.Controls.Add(this.chkPayFromSaving);
            this.panel1.Controls.Add(this.radioChargesPartwise);
            this.panel1.Controls.Add(this.radioChargesFull);
            this.panel1.Controls.Add(this.lblEMIChargesVal);
            this.panel1.Controls.Add(this.grpdivision);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.grpEMISchedule);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.txtAmount);
            this.panel1.Controls.Add(this.lblMessages);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.txtRemarks);
            this.panel1.Controls.Add(this.lblRemarks);
            this.panel1.Controls.Add(this.dtpOpenDate);
            this.panel1.Controls.Add(this.lblTransDate);
            this.panel1.Controls.Add(this.cmbAgentName);
            this.panel1.Controls.Add(this.lblAgentName);
            this.panel1.Controls.Add(this.lblAccValue);
            this.panel1.Controls.Add(this.lblAccountID);
            this.panel1.Controls.Add(this.lblAmount);
            this.panel1.Location = new System.Drawing.Point(7, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 408);
            this.panel1.TabIndex = 101;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // cmbLoanType
            // 
            this.cmbLoanType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoanType.FormattingEnabled = true;
            this.cmbLoanType.Location = new System.Drawing.Point(155, 7);
            this.cmbLoanType.Name = "cmbLoanType";
            this.cmbLoanType.Size = new System.Drawing.Size(121, 21);
            this.cmbLoanType.TabIndex = 1;
            this.cmbLoanType.SelectedIndexChanged += new System.EventHandler(this.cmbLoanType_SelectedIndexChanged);
            // 
            // lblLoanType
            // 
            this.lblLoanType.AutoSize = true;
            this.lblLoanType.Location = new System.Drawing.Point(42, 12);
            this.lblLoanType.Name = "lblLoanType";
            this.lblLoanType.Size = new System.Drawing.Size(58, 13);
            this.lblLoanType.TabIndex = 172;
            this.lblLoanType.Text = "Loan Type";
            // 
            // chkPayFromSaving
            // 
            this.chkPayFromSaving.AutoSize = true;
            this.chkPayFromSaving.Location = new System.Drawing.Point(359, 300);
            this.chkPayFromSaving.Name = "chkPayFromSaving";
            this.chkPayFromSaving.Size = new System.Drawing.Size(111, 17);
            this.chkPayFromSaving.TabIndex = 8;
            this.chkPayFromSaving.Text = "Pay From Savings";
            this.chkPayFromSaving.UseVisualStyleBackColor = true;
            this.chkPayFromSaving.CheckedChanged += new System.EventHandler(this.chkPayFromSaving_CheckedChanged);
            // 
            // radioChargesPartwise
            // 
            this.radioChargesPartwise.AutoSize = true;
            this.radioChargesPartwise.Location = new System.Drawing.Point(584, 198);
            this.radioChargesPartwise.Name = "radioChargesPartwise";
            this.radioChargesPartwise.Size = new System.Drawing.Size(150, 17);
            this.radioChargesPartwise.TabIndex = 4;
            this.radioChargesPartwise.Text = "Pay Charges In Installment";
            this.radioChargesPartwise.UseVisualStyleBackColor = true;
            this.radioChargesPartwise.CheckedChanged += new System.EventHandler(this.radioChargesPartwise_CheckedChanged);
            // 
            // radioChargesFull
            // 
            this.radioChargesFull.AutoSize = true;
            this.radioChargesFull.Checked = true;
            this.radioChargesFull.Location = new System.Drawing.Point(374, 196);
            this.radioChargesFull.Name = "radioChargesFull";
            this.radioChargesFull.Size = new System.Drawing.Size(134, 17);
            this.radioChargesFull.TabIndex = 3;
            this.radioChargesFull.TabStop = true;
            this.radioChargesFull.Text = "Pay Charges One Time";
            this.radioChargesFull.UseVisualStyleBackColor = true;
            this.radioChargesFull.CheckedChanged += new System.EventHandler(this.radioChargesFull_CheckedChanged);
            // 
            // lblEMIChargesVal
            // 
            this.lblEMIChargesVal.AutoSize = true;
            this.lblEMIChargesVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIChargesVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIChargesVal.Location = new System.Drawing.Point(701, 308);
            this.lblEMIChargesVal.Name = "lblEMIChargesVal";
            this.lblEMIChargesVal.Size = new System.Drawing.Size(15, 15);
            this.lblEMIChargesVal.TabIndex = 154;
            this.lblEMIChargesVal.Text = "0";
            this.lblEMIChargesVal.Visible = false;
            // 
            // grpdivision
            // 
            this.grpdivision.Controls.Add(this.lblCloseBrace);
            this.grpdivision.Controls.Add(this.lblOpenBrace);
            this.grpdivision.Controls.Add(this.txtChargeAmt);
            this.grpdivision.Controls.Add(this.lblChargeAmt);
            this.grpdivision.Controls.Add(this.txtPrincipalAmt);
            this.grpdivision.Controls.Add(this.lblPrincipalAmt);
            this.grpdivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpdivision.Location = new System.Drawing.Point(264, 248);
            this.grpdivision.Name = "grpdivision";
            this.grpdivision.Size = new System.Drawing.Size(392, 38);
            this.grpdivision.TabIndex = 164;
            this.grpdivision.TabStop = false;
            // 
            // lblCloseBrace
            // 
            this.lblCloseBrace.AutoSize = true;
            this.lblCloseBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloseBrace.Location = new System.Drawing.Point(376, 12);
            this.lblCloseBrace.Name = "lblCloseBrace";
            this.lblCloseBrace.Size = new System.Drawing.Size(11, 15);
            this.lblCloseBrace.TabIndex = 164;
            this.lblCloseBrace.Text = ")";
            // 
            // lblOpenBrace
            // 
            this.lblOpenBrace.AutoSize = true;
            this.lblOpenBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpenBrace.Location = new System.Drawing.Point(6, 12);
            this.lblOpenBrace.Name = "lblOpenBrace";
            this.lblOpenBrace.Size = new System.Drawing.Size(11, 15);
            this.lblOpenBrace.TabIndex = 163;
            this.lblOpenBrace.Text = "(";
            // 
            // txtChargeAmt
            // 
            this.txtChargeAmt.Location = new System.Drawing.Point(272, 12);
            this.txtChargeAmt.MaxLength = 10;
            this.txtChargeAmt.Name = "txtChargeAmt";
            this.txtChargeAmt.Size = new System.Drawing.Size(100, 20);
            this.txtChargeAmt.TabIndex = 160;
            this.txtChargeAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtChargeAmt_KeyPress);
            // 
            // lblChargeAmt
            // 
            this.lblChargeAmt.AutoSize = true;
            this.lblChargeAmt.Location = new System.Drawing.Point(204, 16);
            this.lblChargeAmt.Name = "lblChargeAmt";
            this.lblChargeAmt.Size = new System.Drawing.Size(62, 13);
            this.lblChargeAmt.TabIndex = 162;
            this.lblChargeAmt.Text = "Charge Amt";
            // 
            // txtPrincipalAmt
            // 
            this.txtPrincipalAmt.Location = new System.Drawing.Point(101, 12);
            this.txtPrincipalAmt.MaxLength = 10;
            this.txtPrincipalAmt.Name = "txtPrincipalAmt";
            this.txtPrincipalAmt.Size = new System.Drawing.Size(100, 20);
            this.txtPrincipalAmt.TabIndex = 159;
            this.txtPrincipalAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrincipalAmt_KeyPress);
            // 
            // lblPrincipalAmt
            // 
            this.lblPrincipalAmt.AutoSize = true;
            this.lblPrincipalAmt.Location = new System.Drawing.Point(18, 16);
            this.lblPrincipalAmt.Name = "lblPrincipalAmt";
            this.lblPrincipalAmt.Size = new System.Drawing.Size(68, 13);
            this.lblPrincipalAmt.TabIndex = 161;
            this.lblPrincipalAmt.Text = "Principal Amt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(588, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 153;
            this.label5.Text = "EMI Charges";
            this.label5.Visible = false;
            // 
            // grpEMISchedule
            // 
            this.grpEMISchedule.Controls.Add(this.lblEMIPrincipalVal);
            this.grpEMISchedule.Controls.Add(this.label7);
            this.grpEMISchedule.Controls.Add(this.lblEMIEndDateVal);
            this.grpEMISchedule.Controls.Add(this.label3);
            this.grpEMISchedule.Controls.Add(this.lblEMIStartDateVal);
            this.grpEMISchedule.Controls.Add(this.label2);
            this.grpEMISchedule.Controls.Add(this.lblLoanAmt);
            this.grpEMISchedule.Controls.Add(this.lblLoanAmtVal);
            this.grpEMISchedule.Controls.Add(this.lblDispersalBal);
            this.grpEMISchedule.Controls.Add(this.lblDispersalBalVal);
            this.grpEMISchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpEMISchedule.Location = new System.Drawing.Point(2, 25);
            this.grpEMISchedule.Name = "grpEMISchedule";
            this.grpEMISchedule.Size = new System.Drawing.Size(788, 60);
            this.grpEMISchedule.TabIndex = 163;
            this.grpEMISchedule.TabStop = false;
            // 
            // lblEMIPrincipalVal
            // 
            this.lblEMIPrincipalVal.AutoSize = true;
            this.lblEMIPrincipalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIPrincipalVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIPrincipalVal.Location = new System.Drawing.Point(699, 11);
            this.lblEMIPrincipalVal.Name = "lblEMIPrincipalVal";
            this.lblEMIPrincipalVal.Size = new System.Drawing.Size(15, 15);
            this.lblEMIPrincipalVal.TabIndex = 152;
            this.lblEMIPrincipalVal.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(580, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 151;
            this.label7.Text = "EMI Principal";
            // 
            // lblEMIEndDateVal
            // 
            this.lblEMIEndDateVal.AutoSize = true;
            this.lblEMIEndDateVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIEndDateVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIEndDateVal.Location = new System.Drawing.Point(469, 37);
            this.lblEMIEndDateVal.Name = "lblEMIEndDateVal";
            this.lblEMIEndDateVal.Size = new System.Drawing.Size(15, 15);
            this.lblEMIEndDateVal.TabIndex = 150;
            this.lblEMIEndDateVal.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(312, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 149;
            this.label3.Text = "Installment Planned End";
            // 
            // lblEMIStartDateVal
            // 
            this.lblEMIStartDateVal.AutoSize = true;
            this.lblEMIStartDateVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIStartDateVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIStartDateVal.Location = new System.Drawing.Point(469, 11);
            this.lblEMIStartDateVal.Name = "lblEMIStartDateVal";
            this.lblEMIStartDateVal.Size = new System.Drawing.Size(15, 15);
            this.lblEMIStartDateVal.TabIndex = 148;
            this.lblEMIStartDateVal.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(312, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 147;
            this.label2.Text = "Installment Planned Start";
            // 
            // lblLoanAmt
            // 
            this.lblLoanAmt.AutoSize = true;
            this.lblLoanAmt.Location = new System.Drawing.Point(40, 11);
            this.lblLoanAmt.Name = "lblLoanAmt";
            this.lblLoanAmt.Size = new System.Drawing.Size(70, 13);
            this.lblLoanAmt.TabIndex = 145;
            this.lblLoanAmt.Text = "Loan Amount";
            // 
            // lblLoanAmtVal
            // 
            this.lblLoanAmtVal.AutoSize = true;
            this.lblLoanAmtVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAmtVal.ForeColor = System.Drawing.Color.Black;
            this.lblLoanAmtVal.Location = new System.Drawing.Point(177, 11);
            this.lblLoanAmtVal.Name = "lblLoanAmtVal";
            this.lblLoanAmtVal.Size = new System.Drawing.Size(15, 15);
            this.lblLoanAmtVal.TabIndex = 146;
            this.lblLoanAmtVal.Text = "0";
            // 
            // lblDispersalBal
            // 
            this.lblDispersalBal.AutoSize = true;
            this.lblDispersalBal.Location = new System.Drawing.Point(40, 37);
            this.lblDispersalBal.Name = "lblDispersalBal";
            this.lblDispersalBal.Size = new System.Drawing.Size(92, 13);
            this.lblDispersalBal.TabIndex = 100;
            this.lblDispersalBal.Text = "Pending Dispersal";
            // 
            // lblDispersalBalVal
            // 
            this.lblDispersalBalVal.AutoSize = true;
            this.lblDispersalBalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispersalBalVal.ForeColor = System.Drawing.Color.Black;
            this.lblDispersalBalVal.Location = new System.Drawing.Point(177, 37);
            this.lblDispersalBalVal.Name = "lblDispersalBalVal";
            this.lblDispersalBalVal.Size = new System.Drawing.Size(15, 15);
            this.lblDispersalBalVal.TabIndex = 101;
            this.lblDispersalBalVal.Text = "0";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(342, 358);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 10;
            this.btnNew.Text = "&New Entry";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(155, 261);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ShortcutsEnabled = false;
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 6;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(42, 386);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(55, 13);
            this.lblMessages.TabIndex = 97;
            this.lblMessages.Text = "Messages";
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(419, 358);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(264, 358);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(155, 287);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(195, 59);
            this.txtRemarks.TabIndex = 7;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(42, 308);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 93;
            this.lblRemarks.Text = "Remarks";
            // 
            // dtpOpenDate
            // 
            this.dtpOpenDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenDate.Location = new System.Drawing.Point(155, 198);
            this.dtpOpenDate.Name = "dtpOpenDate";
            this.dtpOpenDate.Size = new System.Drawing.Size(105, 20);
            this.dtpOpenDate.TabIndex = 2;
            // 
            // lblTransDate
            // 
            this.lblTransDate.AutoSize = true;
            this.lblTransDate.Location = new System.Drawing.Point(42, 198);
            this.lblTransDate.Name = "lblTransDate";
            this.lblTransDate.Size = new System.Drawing.Size(89, 13);
            this.lblTransDate.TabIndex = 90;
            this.lblTransDate.Text = "Transaction Date";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(155, 223);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(195, 21);
            this.cmbAgentName.TabIndex = 5;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(42, 226);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(78, 13);
            this.lblAgentName.TabIndex = 88;
            this.lblAgentName.Text = "Leader Name *";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(397, 9);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(19, 13);
            this.lblAccValue.TabIndex = 87;
            this.lblAccValue.Text = "<>";
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(314, 9);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 86;
            this.lblAccountID.Text = "Account ID";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(42, 264);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(65, 13);
            this.lblAmount.TabIndex = 84;
            this.lblAmount.Text = "Amount (Rs)";
            // 
            // lblPendingChargeVal
            // 
            this.lblPendingChargeVal.AutoSize = true;
            this.lblPendingChargeVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPendingChargeVal.ForeColor = System.Drawing.Color.Black;
            this.lblPendingChargeVal.Location = new System.Drawing.Point(464, 53);
            this.lblPendingChargeVal.Name = "lblPendingChargeVal";
            this.lblPendingChargeVal.Size = new System.Drawing.Size(15, 15);
            this.lblPendingChargeVal.TabIndex = 168;
            this.lblPendingChargeVal.Text = "0";
            // 
            // lblPendingCharge
            // 
            this.lblPendingCharge.AutoSize = true;
            this.lblPendingCharge.Location = new System.Drawing.Point(306, 53);
            this.lblPendingCharge.Name = "lblPendingCharge";
            this.lblPendingCharge.Size = new System.Drawing.Size(141, 13);
            this.lblPendingCharge.TabIndex = 167;
            this.lblPendingCharge.Text = "Pending Installment Charges";
            // 
            // lblPendingPrincipalVal
            // 
            this.lblPendingPrincipalVal.AutoSize = true;
            this.lblPendingPrincipalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPendingPrincipalVal.ForeColor = System.Drawing.Color.Black;
            this.lblPendingPrincipalVal.Location = new System.Drawing.Point(464, 29);
            this.lblPendingPrincipalVal.Name = "lblPendingPrincipalVal";
            this.lblPendingPrincipalVal.Size = new System.Drawing.Size(15, 15);
            this.lblPendingPrincipalVal.TabIndex = 166;
            this.lblPendingPrincipalVal.Text = "0";
            // 
            // lblPendingPrincipal
            // 
            this.lblPendingPrincipal.AutoSize = true;
            this.lblPendingPrincipal.Location = new System.Drawing.Point(306, 29);
            this.lblPendingPrincipal.Name = "lblPendingPrincipal";
            this.lblPendingPrincipal.Size = new System.Drawing.Size(142, 13);
            this.lblPendingPrincipal.TabIndex = 165;
            this.lblPendingPrincipal.Text = "Pending Installment Principal";
            // 
            // lblChargesEMIPaidVal
            // 
            this.lblChargesEMIPaidVal.AutoSize = true;
            this.lblChargesEMIPaidVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChargesEMIPaidVal.ForeColor = System.Drawing.Color.Black;
            this.lblChargesEMIPaidVal.Location = new System.Drawing.Point(172, 51);
            this.lblChargesEMIPaidVal.Name = "lblChargesEMIPaidVal";
            this.lblChargesEMIPaidVal.Size = new System.Drawing.Size(15, 15);
            this.lblChargesEMIPaidVal.TabIndex = 162;
            this.lblChargesEMIPaidVal.Text = "0";
            // 
            // lblChargesEMIPaid
            // 
            this.lblChargesEMIPaid.AutoSize = true;
            this.lblChargesEMIPaid.Location = new System.Drawing.Point(34, 51);
            this.lblChargesEMIPaid.Name = "lblChargesEMIPaid";
            this.lblChargesEMIPaid.Size = new System.Drawing.Size(123, 13);
            this.lblChargesEMIPaid.TabIndex = 161;
            this.lblChargesEMIPaid.Text = "Installment Charges Paid";
            // 
            // lblPrincipalEMIPaidVal
            // 
            this.lblPrincipalEMIPaidVal.AutoSize = true;
            this.lblPrincipalEMIPaidVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrincipalEMIPaidVal.ForeColor = System.Drawing.Color.Black;
            this.lblPrincipalEMIPaidVal.Location = new System.Drawing.Point(172, 27);
            this.lblPrincipalEMIPaidVal.Name = "lblPrincipalEMIPaidVal";
            this.lblPrincipalEMIPaidVal.Size = new System.Drawing.Size(15, 15);
            this.lblPrincipalEMIPaidVal.TabIndex = 160;
            this.lblPrincipalEMIPaidVal.Text = "0";
            // 
            // lblPrincipalEMIPaid
            // 
            this.lblPrincipalEMIPaid.AutoSize = true;
            this.lblPrincipalEMIPaid.Location = new System.Drawing.Point(34, 27);
            this.lblPrincipalEMIPaid.Name = "lblPrincipalEMIPaid";
            this.lblPrincipalEMIPaid.Size = new System.Drawing.Size(124, 13);
            this.lblPrincipalEMIPaid.TabIndex = 159;
            this.lblPrincipalEMIPaid.Text = "Installment Principal Paid";
            // 
            // lblEMIDueVal
            // 
            this.lblEMIDueVal.AutoSize = true;
            this.lblEMIDueVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIDueVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIDueVal.Location = new System.Drawing.Point(462, 73);
            this.lblEMIDueVal.Name = "lblEMIDueVal";
            this.lblEMIDueVal.Size = new System.Drawing.Size(25, 15);
            this.lblEMIDueVal.TabIndex = 156;
            this.lblEMIDueVal.Text = "NA";
            // 
            // lblEMIDue
            // 
            this.lblEMIDue.AutoSize = true;
            this.lblEMIDue.Location = new System.Drawing.Point(306, 75);
            this.lblEMIDue.Name = "lblEMIDue";
            this.lblEMIDue.Size = new System.Drawing.Size(106, 13);
            this.lblEMIDue.TabIndex = 155;
            this.lblEMIDue.Text = "Installment Due Date";
            // 
            // lblRemainingPrincipalVal
            // 
            this.lblRemainingPrincipalVal.AutoSize = true;
            this.lblRemainingPrincipalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainingPrincipalVal.ForeColor = System.Drawing.Color.Black;
            this.lblRemainingPrincipalVal.Location = new System.Drawing.Point(693, 27);
            this.lblRemainingPrincipalVal.Name = "lblRemainingPrincipalVal";
            this.lblRemainingPrincipalVal.Size = new System.Drawing.Size(15, 15);
            this.lblRemainingPrincipalVal.TabIndex = 154;
            this.lblRemainingPrincipalVal.Text = "0";
            // 
            // lblRemainingPrincipal
            // 
            this.lblRemainingPrincipal.AutoSize = true;
            this.lblRemainingPrincipal.Location = new System.Drawing.Point(574, 27);
            this.lblRemainingPrincipal.Name = "lblRemainingPrincipal";
            this.lblRemainingPrincipal.Size = new System.Drawing.Size(100, 13);
            this.lblRemainingPrincipal.TabIndex = 153;
            this.lblRemainingPrincipal.Text = "Remaining Principal";
            // 
            // lblLastPaidDateVal
            // 
            this.lblLastPaidDateVal.AutoSize = true;
            this.lblLastPaidDateVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastPaidDateVal.ForeColor = System.Drawing.Color.Black;
            this.lblLastPaidDateVal.Location = new System.Drawing.Point(172, 73);
            this.lblLastPaidDateVal.Name = "lblLastPaidDateVal";
            this.lblLastPaidDateVal.Size = new System.Drawing.Size(25, 15);
            this.lblLastPaidDateVal.TabIndex = 150;
            this.lblLastPaidDateVal.Text = "NA";
            // 
            // lblLastPaidDate
            // 
            this.lblLastPaidDate.AutoSize = true;
            this.lblLastPaidDate.Location = new System.Drawing.Point(34, 73);
            this.lblLastPaidDate.Name = "lblLastPaidDate";
            this.lblLastPaidDate.Size = new System.Drawing.Size(77, 13);
            this.lblLastPaidDate.TabIndex = 149;
            this.lblLastPaidDate.Text = "Last Paid Date";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblEMIDue);
            this.groupBox1.Controls.Add(this.lblLastPaidDate);
            this.groupBox1.Controls.Add(this.lblLastPaidDateVal);
            this.groupBox1.Controls.Add(this.lblRemainingPrincipal);
            this.groupBox1.Controls.Add(this.lblPendingChargeVal);
            this.groupBox1.Controls.Add(this.lblRemainingPrincipalVal);
            this.groupBox1.Controls.Add(this.lblPendingCharge);
            this.groupBox1.Controls.Add(this.lblEMIDueVal);
            this.groupBox1.Controls.Add(this.lblPendingPrincipalVal);
            this.groupBox1.Controls.Add(this.lblPrincipalEMIPaid);
            this.groupBox1.Controls.Add(this.lblPendingPrincipal);
            this.groupBox1.Controls.Add(this.lblPrincipalEMIPaidVal);
            this.groupBox1.Controls.Add(this.lblChargesEMIPaid);
            this.groupBox1.Controls.Add(this.lblChargesEMIPaidVal);
            this.groupBox1.Location = new System.Drawing.Point(11, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(785, 102);
            this.groupBox1.TabIndex = 165;
            this.groupBox1.TabStop = false;
            // 
            // frmLoanAccTrans
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(830, 646);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.dgLoanDeposit);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmLoanAccTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.LoanAccTrans_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgLoanDeposit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpdivision.ResumeLayout(false);
            this.grpdivision.PerformLayout();
            this.grpEMISchedule.ResumeLayout(false);
            this.grpEMISchedule.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInfo;
        public System.Windows.Forms.DataGridView dgLoanDeposit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.DateTimePicker dtpOpenDate;
        private System.Windows.Forms.Label lblTransDate;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblDispersalBalVal;
        private System.Windows.Forms.Label lblDispersalBal;
        private System.Windows.Forms.Label lblLoanAmtVal;
        private System.Windows.Forms.Label lblLoanAmt;
        private System.Windows.Forms.Label lblLastPaidDateVal;
        private System.Windows.Forms.Label lblLastPaidDate;
        private System.Windows.Forms.Label lblRemainingPrincipalVal;
        private System.Windows.Forms.Label lblRemainingPrincipal;
        private System.Windows.Forms.Label lblEMIDueVal;
        private System.Windows.Forms.Label lblEMIDue;
        private System.Windows.Forms.Label lblPrincipalEMIPaidVal;
        private System.Windows.Forms.Label lblPrincipalEMIPaid;
        private System.Windows.Forms.Label lblChargesEMIPaidVal;
        private System.Windows.Forms.Label lblChargesEMIPaid;
        private System.Windows.Forms.GroupBox grpEMISchedule;
        private System.Windows.Forms.Label lblEMIStartDateVal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEMIEndDateVal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEMIChargesVal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblEMIPrincipalVal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox grpdivision;
        private System.Windows.Forms.Label lblCloseBrace;
        private System.Windows.Forms.Label lblOpenBrace;
        private System.Windows.Forms.TextBox txtChargeAmt;
        private System.Windows.Forms.Label lblChargeAmt;
        private System.Windows.Forms.TextBox txtPrincipalAmt;
        private System.Windows.Forms.Label lblPrincipalAmt;
        private System.Windows.Forms.Label lblPendingChargeVal;
        private System.Windows.Forms.Label lblPendingCharge;
        private System.Windows.Forms.Label lblPendingPrincipalVal;
        private System.Windows.Forms.Label lblPendingPrincipal;
        private System.Windows.Forms.RadioButton radioChargesPartwise;
        private System.Windows.Forms.RadioButton radioChargesFull;
        private System.Windows.Forms.CheckBox chkPayFromSaving;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrinciplePart;
        private System.Windows.Forms.DataGridViewTextBoxColumn InterestPart;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PaidFromSavings;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblLoanType;
        private System.Windows.Forms.ComboBox cmbLoanType;

    }
}