﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Timers;
using System.Windows.Threading;

namespace SPARC
{
    public partial class DailyAccountsEntry : Form
    {
        public DailyAccountsEntry()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private void DailyAccountsEntry_Load(object sender, EventArgs e)
        {
            int CodeType = 0;            
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHubLoan);
            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the state type code and populate state 
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                    cmbcountry.SelectedValue = Id;
                    cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the District type code and populate district
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                    cmbstate.SelectedValue = Id;
                    cmbstate.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the Slum type code and populate Slum
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                    cmbdistrict.SelectedValue = Id;
                    cmbdistrict.Enabled = false;
                }
            }

            dtpAccountDate.MaxDate = DateTime.Now;
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {

            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);


                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
           
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }

        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private string DoValidationsSavRep()
        {
            string ErrorMsg = string.Empty;

            if (cmbslum.SelectedIndex <= 0)
                ErrorMsg = Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            if (cmbTypeSavRe.SelectedIndex < 0)
                ErrorMsg += Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;
            if (txtSavReTotal.Text.Trim().Length == 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                lblMessages.ForeColor = Color.Red;
            }

            return ErrorMsg;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string strMessage = DoValidationsSavRep();
            if (strMessage.Length > 0)
            {
                lblMessages.Text = strMessage;
            }

            else
            {
                lblMessages.Text = string.Empty;
                dgvSavRe.Rows.Add();

                int Row = 0;
                Row = dgvSavRe.Rows.Count - 1;

                dgvSavRe[0, Row].Value = dtpAccountDate.Text;
                dgvSavRe[1, Row].Value = cmbslum.Text;
                dgvSavRe[2, Row].Value = cmbslum.SelectedValue;
                dgvSavRe[3, Row].Value = cmbTypeSavRe.Text;
                dgvSavRe[4, Row].Value = txtSavReTotal.Text;

                dgvSavRe.Refresh();
                txtSavReTotal.Text = string.Empty;
                cmbTypeSavRe.SelectedIndex = -1;
               
            }
        }

        DateTime SavReDate;
        Int64 SlumId;
        string SavReType;
        double SavReAmount;

        private void btnSavReSave_Click(object sender, EventArgs e)
        {
            if (dgvSavRe.Rows.Count > 0)
            {
                for (int i = 0; i < dgvSavRe.Rows.Count; i++)
                {
                    SavReDate = Convert.ToDateTime(dgvSavRe.Rows[i].Cells["Date"].Value);
                    SlumId = Convert.ToInt64(dgvSavRe.Rows[i].Cells["Settlment_id"].Value);
                    SavReType = Convert.ToString(dgvSavRe.Rows[i].Cells["Type"].Value);
                    SavReAmount = Convert.ToDouble(dgvSavRe.Rows[i].Cells["Amount"].Value);

                    SqlParameter[] sqlParam = new SqlParameter[6];
                    int iCounter = 0;

                    sqlParam[iCounter] = new SqlParameter("@TransDate", SqlDbType.DateTime);
                    sqlParam[iCounter++].Value = SavReDate;

                    sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = SlumId;

                    sqlParam[iCounter] = new SqlParameter("@Type", SqlDbType.NVarChar, 20);
                    sqlParam[iCounter++].Value = SavReType;

                    sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
                    sqlParam[iCounter++].Value = SavReAmount;

                    sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = GlobalValues.User_PkId; ;

                    sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = GlobalValues.User_PkId; ;

                    SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_InsertTotalSaving_Repayment", sqlParam);
                }
                // dgvSavRe.DataSource = null;
                dgvSavRe.Rows.Clear();
            }
            else
            {
                MessageBox.Show("Enter data to save");
            }
        }

        private void SaveSavReAmount()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSavReTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgvSavRe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvSavRe.Columns["Delete"].Index) //To check that we are in the right column
            {
                foreach (DataGridViewCell oneCell in dgvSavRe.SelectedCells)
                {
                    if (oneCell.Selected)
                        dgvSavRe.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
        }

        private void cmbHubLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHubLoan.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHubLoan.SelectedValue), cmbCountryLoan);
            else
                cmbCountryLoan.DataSource = null;
        }

        private void cmbCountryLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountryLoan.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountryLoan.SelectedValue), cmbStateLoan);
            else
                cmbStateLoan.DataSource = null;
        }

        private void cmbStateLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbStateLoan.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbStateLoan.SelectedValue), cmbDistrictLoan);
            else
                cmbDistrictLoan.DataSource = null;
        }

        private void cmbDistrictLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrictLoan.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrictLoan.SelectedValue), cmbSlumLoan);
            else
                cmbSlumLoan.DataSource = null;
        }

        private void cmbSlumLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int value = Convert.ToInt32(cmbSlumLoan.SelectedValue);
                if (cmbSlumLoan.SelectedIndex > 0)
                {
                    FillCombos1(Convert.ToInt32(LocationCode.Member), Convert.ToInt32(cmbSlumLoan.SelectedValue), cmbMemberName);
                }
                else
                {
                    cmbMemberName.DataSource = null;
                }
                
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void FillCombos1(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {            
            CodeValues1 objCodeValues1 = new CodeValues1();
            objCodeValues1.CodeTypeID = CodeTypeID;
            objCodeValues1.ParentID = ParentID;

            DataSet dsCodeValues1 = objCodeValues1.GetCodeValues1(false);
            if (dsCodeValues1 != null && dsCodeValues1.Tables.Count > 0 && dsCodeValues1.Tables[0].Rows.Count > 0)
            {
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues1.Tables[0];
            }

            
        }

        private void txtLoanAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtSC_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbLoanWithdraw_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoanWithdraw.Text.Trim() == "Withdrawal")
            {
                lblSC.Visible = false;
                txtSC.Visible = false;
            }
            if (cmbLoanWithdraw.Text.Trim() == "New Loan")
            {
                lblSC.Visible = true;
                txtSC.Visible = true;
            }
        }

        private string DoValidationsLoanWithdrawal()
        {
            string ErrorMsgLoan = string.Empty;

            if (cmbSlumLoan.SelectedIndex <= 0)
                ErrorMsgLoan = Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            if (cmbLoanWithdraw.SelectedIndex < 0)
                ErrorMsgLoan += Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;            
            if (txtLoanAmt.Text.Trim().Length == 0)
                ErrorMsgLoan += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            if (cmbMemberName.SelectedIndex < 0)
                ErrorMsgLoan += Messages.FULLNAME + Messages.COMMAWITHSPACE;

            if (cmbLoanWithdraw.Text.Trim() == "New Loan" && txtSC.Text.Trim() == "")
            {
                ErrorMsgLoan += Messages.SERVICECHARGE + Messages.COMMAWITHSPACE;
            }

            if (ErrorMsgLoan.Trim().Length > 0)
            {
                ErrorMsgLoan = Messages.REQUIREDFIELDS + ErrorMsgLoan;
                ErrorMsgLoan = ErrorMsgLoan.Substring(0, ErrorMsgLoan.Length - 2);
                lblLoanMsg.ForeColor = Color.Red;
            }

            return ErrorMsgLoan;
        }

        private void btnLoanAdd_Click(object sender, EventArgs e)
        {
            string strMessageLoan = DoValidationsLoanWithdrawal();
            if (strMessageLoan.Length > 0)
            {
                lblLoanMsg.Text = strMessageLoan;
            }

            else
            {
                lblLoanMsg.Text = string.Empty;
                dgvLoan.Rows.Add();

                int Row = 0;
                Row = dgvLoan.Rows.Count - 1;

                dgvLoan[0, Row].Value = dtpAccountDate.Text;
                dgvLoan[1, Row].Value = cmbSlumLoan.Text;
                dgvLoan[2, Row].Value = cmbSlumLoan.SelectedValue;
                dgvLoan[3, Row].Value = cmbLoanWithdraw.Text;

                dgvLoan[4, Row].Value = cmbMemberName.SelectedValue;
                dgvLoan[5, Row].Value = cmbMemberName.Text;
                dgvLoan[6, Row].Value = txtLoanAmt.Text;
                dgvLoan[7, Row].Value = txtSC.Text;

                dgvLoan.Refresh();
                cmbMemberName.SelectedItem = -1;
                cmbLoanWithdraw.SelectedIndex = -1;
              //  cmbSlumLoan.SelectedIndex = 0;
                txtLoanAmt.Text = string.Empty;
                txtSC.Text = string.Empty;
            }
        }


        DateTime LowiDate;
        Int64 LoanSlumId;
        string Loanorwith;
        Int64 memid; 
        double LoanwiAmount;
        double? Sc;
        private void btnSaveLoan_Click(object sender, EventArgs e)
        {
            if (dgvLoan.Rows.Count > 0)
            {
                for (int i = 0; i < dgvLoan.Rows.Count; i++)
                {
                    LowiDate = Convert.ToDateTime(dgvLoan.Rows[i].Cells["LoanDate"].Value);
                    LoanSlumId = Convert.ToInt64(dgvLoan.Rows[i].Cells["LoanSettlementID"].Value);
                    Loanorwith = Convert.ToString(dgvLoan.Rows[i].Cells["LoanType"].Value);
                    memid = Convert.ToInt64(dgvLoan.Rows[i].Cells["Memberid"].Value);
                    LoanwiAmount = Convert.ToDouble(dgvLoan.Rows[i].Cells["LoanAmount"].Value);
                    if (Convert.ToString(dgvLoan.Rows[i].Cells["ServiceCharge"].Value) == "")
                    {
                        Sc = null;
                    }
                    else
                    Sc = Convert.ToDouble(dgvLoan.Rows[i].Cells["ServiceCharge"].Value);

                    SqlParameter[] sqlParam = new SqlParameter[8];
                    int iCounter = 0;

                    sqlParam[iCounter] = new SqlParameter("@TransDate", SqlDbType.DateTime);
                    sqlParam[iCounter++].Value = LowiDate;

                    sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = LoanSlumId;

                    sqlParam[iCounter] = new SqlParameter("@Type", SqlDbType.NVarChar, 20);
                    sqlParam[iCounter++].Value = Loanorwith;

                    sqlParam[iCounter] = new SqlParameter("@fk_memId", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = memid;

                    sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
                    sqlParam[iCounter++].Value = LoanwiAmount;

                    sqlParam[iCounter] = new SqlParameter("@SC", SqlDbType.Float);
                    sqlParam[iCounter++].Value = Sc;

                    sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = GlobalValues.User_PkId; ;

                    sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
                    sqlParam[iCounter++].Value = GlobalValues.User_PkId; 

                    SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_InsertLoan_Withdrawal", sqlParam);
                }
                
                dgvLoan.Rows.Clear();
            }
            else
            {
                MessageBox.Show("Enter data to save");
            }
        }

        private void dgvLoan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvLoan.Columns["DeleteLoan"].Index) //To check that we are in the right column
            {
                foreach (DataGridViewCell oneCell in dgvLoan.SelectedCells)
                {
                    if (oneCell.Selected)
                        dgvLoan.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
        }

    }
}
