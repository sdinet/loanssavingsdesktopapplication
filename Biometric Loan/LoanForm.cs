﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using System.Configuration;
using System.Configuration;

namespace SPARC
{
    public partial class LoanForm : Form
    {
        public Int64 memid;

        public LoanForm(Int64 MemberId)
        {
            InitializeComponent();
            memid = MemberId;
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);

        private void LoanForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("LoanDetails");

                string cmdText = @"select l.LoanAppliedDate, l.AccountNumber, m.passbooknumber, m.Name, l.RelationName, l.Address, l.AreaFederation,
                                    l.Phone, l.SizeOfHouse, l.HousePattaOrSecurity, l.TotalHouseCost, l.DepositWithLoanReq, l.ConstructionTime, l.LoanAmount,
                                    l.Tenure, l.EMISchedule,m.photo,
                                    (select name from codevalue where id=l.WaterSource) as WaterSource,
                                    (select name from codevalue where id=l.ToiletFacility) as ToiletFacility,
                                    (select name from codevalue where id=l.HouseRoof) as HouseRoof,
                                    (select name from codevalue where id=l.Walls) as Walls,
                                    (select name from codevalue where id=l.HouseFloor) as HouseFloor,
                                    (select name from codevalue where id=l.ConstructionType) as ConstructionType,
                                    (select name from codevalue where id=l.ConstructionDone) as ConstructionDone,
                                    (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName,
                                    (Select name from Codevalue where id=[dbo].[udf_FetchStateBySlum] ([fk_SlumId])) as StateName,
                                    (select name from codevalue where id=m.fk_SlumId) as SettlementName,
                                    (select name from codevalue where id=l.LoanType) as LoanType
                                    from Members M inner join vwLoanSummary L
                                    on m.Id = l.fk_MemberId
                                    where l.Status = 'Approved' and m.id = '" + memid + "';";

                SqlCommand cmd;
                SqlDataReader dr;
                cmd = new SqlCommand(cmdText, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.LoanForm.rdlc";

                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));


                this.reportViewer1.RefreshReport();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
       
    }
}
