﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace SPARC
{
    static class Constants
    {
        public static string SAVE = "Save";
        public static string UPDATE = "Update";
        public static string APPROVE = "Approve";

        public static string SELECTONE = "--Select One--";

        public static string CONFIRMDELETE = "Delete confirmation";

        public static string MASTERFORMHEADING = "Saving & Loan System";
        public static string WELCOME = "Welcome ";
        public static string DOBFORMAT = "d/M/yyyy";
        public static string DEBITTRANSABBREV = "DEB";
        public static string CREDITTRANSABBREV = "CRE";

        public static string CREATEMEMBER = "Create Member";
        public static string UPDATEMEMBER = "Update Member";
        public static string CREATESAVINGSACC = "Create Savings Account";
        public static string UPDATESAVINGSACC = "Update Savings Account";
        public static string DEPOSITSAVINGSTRANS = "Deposit Amount";
        public static string DEPOSITMULTIPLESAVINGSTRANS = "Multiple Deposit";
        public static string WITHDRAWSAVINGSTRANS = "Withdraw Amount";
        public static string WITHDRAWMULTIPLESAVINGSTRANS = "Multiple Withdraw";
        public static string EMIPAYLOANTRANS = "EMI Payment Transaction";
        public static string LOANDISPERSALTRANS = "Dispersal Transaction";
        public static string MULTIPLEINSTALLMENT = "Multiple Loan Installment";

        public static string LOANUPDATE = "Update Loan Account";
        public static string LOANCREATE = "Create Loan Account";
        public static string LOANAPPROVE = "Approve Loan Account";

        public static string MASTERDATAMANAGER = "Master Data Management";
        public static string USERMANAGER = "User Management";

        public static string CREATEMAMEMBERFAMILY = "Create Member Family";
        public static string UPDATEMEMBERFAMILY = "Update Member Family";

    }

    static class PayMode
    {
        public static string CASH = "Cash";
        public static string BANK = "Bank";
    }

    static class EMIType
    {
        public static string DAILY = "Daily";
        public static string WEEKLY = "Weekly";
        public static string MONTHLY = "Monthly";
    }

    static class UserRoles
    {

        public static string SUPERADMIN = "Super Admin";
        public static string ADMINLEVEL1 = "Hub Admin";
        public static string ADMINLEVEL2 = "Country Admin";
        public static string ADMINLEVEL3 = "State Admin";
        public static string ADMINLEVEL4 = "District Admin";
        public static string USER = "User";
       
    }
    static class Status
    {
        public static string ACTIVE = "Active";
        public static string INITIATED = "Initiated";
        public static string CLOSED = "Closed";
        public static string APPROVED = "Approved";
        public static string WRITEOFF = "Write-off";

    }

    static class Messages
    {
        public static string PHOTO_NONIMAGE = "The Data in Clipboard is not an image format.";
        public static string PHOTO_EMPTYCB = "The Clipboard is empty";
        public static string ERRORMESSAGE = "Error occured! Contact Administrator for support.";
        public static string CLOSEWINDOWS = "Close all open windows to select a new member.";
        public static string CLOSEOPENFORM = "Save and close the current window to proceed.";
        public static string SELECTMEMBER = "Select a member to update and proceed.";
        public static string INVALIDUSER = "Invalid Username/password!";

        public static string SAVED_SUCCESS = "Details saved successfully.";
        public static string DELETED_SUCCESS = "Details deleted successfully.";
        public static string UPDATED_SUCCESS = "Details updated successfully.";
        public static string APPROVED_SUCCESS = "Details approved successfully.";
        public static string DELETE_PROCESSING = "Data is in use and so it cant be deleted";
        public static string DELETE_CONFIRM = "Are you sure to delete the selected data?";
        public static string DELETE_ACCOUNT = "Account cant be deleted as it has some transactions. Delete the transactions before deleting the account.";

        public static string DAYBOOKGEN_SUCCESS = "Day book (re)generated successfully.";
        public static string DAYBOOKGEN_ERROR = "Error while generating the data. Please try again.";
        public static string ERROR_PROCESSING = "Error while processing the data. Please try again.";

        public static string UPF_PAYOUT_XN_AVAILABLE = "Cannot delete the account. Please delete the pay-out transactions belongs to this account first.";

        public static string AGENTCREATED = "Leader created successfully.";
        public static string ERRAGENTSAVE = "Error in saving the Leader details.";
        //EDITED BY ARCHANA--start
        public static string REQUIREDFIELDS = "Enter values for the above fields for saving: ";
        //EDITED BY ARCHANA--end
        public static string REQ = "Enter values for the Above fields for saving:";
        public static string VALIDVALUES = "Enter valid values for: ";
        public static string INVALIDDOB = "Invalid Date of Birth";
        public static string DISPERSLIMITCHK = "Error: Amount exceeds the pending amount to be dispersed.";
        public static string AMTUNEQUAL = "Error: Amount is not tallying between Principal and Charge.";
        public static string DISPERSALPENDING = "Error: Transactions cant be made if the dispersal amount is pending.";
        public static string EMIEXCEEDSLIMIT = "Error: Amount exceeds the pending amount to be paid.";
        public static string ACCNILBALANCE = "Account should have nil balance before closure.";
        public static string AVAILABLEBALCHECK = "Error: Amount exceeds the available balance.";
        public static string INTERESTPAYDATECHK = "Interest pay should be greater than Transaction date.";
        public static string NOPENDINGPAYOUTS = "There is no pending UPF interest payouts.";
        public static string MEMBERNAME = "Name";
        public static string MEMBERPASSBOOKNUMBER = "PassbookNumber";
        public static string MEMBERVOTERID = "Voterid";
        public static string MEMBERADHAARID = "Adhaarid";
        public static string INVALIDEMAIL = "Invalid Email ID";
        public static string AMOUNTEXCEED = "Loan Amount to be paid is more than Savings Account Balance";

        public static string MEMBERFATHERNAME = "Father Name";
        public static string MEMBERGENDER = "Gender";
        public static string MEMBERADDRESS1 = "Address1";
        public static string MEMBERADDRESS2 = "Address2";
        public static string MEMBERSLUM = "Slum";
        public static string MEMBERZipcode = "Zipcode";
        public static string AGENTNAME = "Leader Name";
        public static string PAYMODE = "Pay Mode";
        public static string PAYTHRUAGENTNAME = "Pay thru Leader Name";
        public static string AMOUNT = "Amount";
        public static string AMOUNTCHARGES = "Amount is less than Charges to be paid, Please enter correct Amount";
        public static string PRINCIPALAMOUNT = "Principal Amt";
        public static string CHARGEAMOUNT = "Charge Amt";
        public static string PRINCIPALORCHARGEAMOUNT = "Principal Amt or Charge Amount";
        public static string PASSBOOKNO = "Passbook No";
        public static string INTERESTAMOUNT = "Enter Correct Interest Amount";
        public static string PRINCIPALAMOUNTTXT = "Amount exceeds the Principal Amount";
        public static string SELECTRADIOBUTTON = "Select the option have to be paid";
        public static string AMOUNTTHRUSAVINGS = "Amount Exceeds More Than Current Balance";

        public static string CODEVALUE = "Value";
        public static string ABBREVIATION = "Abbreviation";
        public static string PARENT = "Parent";
        public static string DISPLAYORDER = "Display Order";
        public static string FILTERBYPARENT = "Filter by Parent";

        public static string LOANAMOUNT = "Loan Amount";
        public static string LOANTENURE = "Tenure";
        public static string LOANName = "Loan Type";
        public static string EMIPAYCYCLE = "EMI Pay Cycle";

        public static string CATEGORY = "Category";
        public static string MODEOFPAYMENT = "Mode of Payment";
        public static string DESCRIPTION = "Description";

        public static string FULLNAME = "Name";
        public static string CONTACTNO = "Contact Number";
        public static string USERNAME = "User Name";
        public static string PASSWORD = "Password";

        public static string COMMAWITHSPACE = ", ";
        public static string HYPHENWITHSPACE = " - ";

        public static string SAVED_CURRENTMEMBER = "Current member is selected for further action.";
        public static string SAVED_CURRENTSBACC = "Savings account is selected for further action.";
        public static string SAVED_CURRENTLOANACC = "Loan account is selected for further action.";
        public static string SBACCALREADYEXIST = "Savings Account already exists. Click Update menu to edit.";
        public static string SBACCNOTEXIST = "Savings Account doesn't exists. Click Create menu to create one.";

        public static string LOANACCNOTAPPROVED = "This Loan account has to be approved for making a transaction.";
        public static string LOANACCNOTEXIST = "Loan Account doesn't exists. Click Create menu to create one.";
        public static string LOANACCALREADYEXIST = "Loan Account already exists. Click Update menu to edit.";

        public static string AmountValidation = "Allocating Amount exceeds actual fund amount";
        public static string DataNotInserted = "No data Saved";

        public static string CURRENCYTYPE = "Currency Type";
        public static string FUNDTYPE = "Fund Type";

        public static string FUNDERNAME = "Funder Name";
        public static string ADDRESS1 = "Address1";
        public static string ADDRESS2 = "Address2";
        public static string ZIP = "Zip";
        public static string EMAIL = "Email";
        public static string TELEPHONE = "Telephone";
       // public static string FAX = "Fax";
        public static string CONTACTPERSON = "Contact Person";
        public static string City = "City";
        public static string CENTERNAME = "Center";

        public static string SAVINGACCOUNTNO = "Savings Account Number";
        public static string DEVICEID = "Device Id";
        public static string MACADDRESS = "Mac Address";
        public static string ABBREVATIONEXIST = "The above mentioned Abbreviation already exist. Please Change.";
        public static string MULTIPLEUSERNAME = "The above mentioned UserName already exist. Please Change.";
        public static string DUPLICATEMACADDRESS = "The above mentioned Mac Address already exist. Please Change.";
        public static string SAMEMOBNUM = "Member Mobile Number And Contact Person's Number cant be same";
        public static string SAMEPASSBOOKNUM = "The above mentioned Passbook Number already exist. Please Change. ";

        public static string SAVINGTYPE = "Saving Type";
        public static string SERVICECHARGE = "Service Charge";
        public static string ESTABLISHEDYEAR = "Establishedyear";
        public static string MALE = "NoofMales";
        public static string FEMALE = "NoofFemales";
        public static string TOTALSAVINGS = "Totalsavings";
        public static string LAND = "Land";
        public static string LOANFROMSAVINGS = "loanfromsavings";
        public static string LOANFROMSAVINGSREPAID = "loanfromsavingsrepaid";
        public static string INTEREST = "Interest";
        public static string CASH = "cash amount";
        public static string SHORT = "Short";
        public static string BANK = "Bankname";
        public static string BANKCHARGES = "Bankcharges";
        public static string BUSINESSTYPE = " business type";
        public static string BUSINESSTYPEOTHER = " businesstype other";
        public static string WITHDRAWALREASONS = "Withdrawalreasons";

        public static string SAVEGROUP = "Group Details Saved Successfully!!";
        public static string UPDATEGROUP = "Group Details Updated Successfully!!";
        public static string SEARCHEDGROUP = "Group Details Searched Successfully!!";
        public static string NOGROUPSEARCH = "No Group Exists!!";
        public static string Groupname = "Groupname";
        public static string MULTIPLEGROUPNAMES = "The above mentioned GroupName already exist.Please Change.";
        public static string NODATASYNC = "There is no data to sync";
        public static string DATANOTSYNCED = "Neither record was written to database";
        public static string Datasynced = "Data successfully synced";
        public static string lblid11 = "Please select the row from the datagridview for insertion";
        public static string AMOUNT1 = "The amount entered must be less than  unallocated amount";
        public static string AMOUNT2 = "The amount entered must be more than zero and less than  unallocated amount";
        public static string Groupmessage = "please select the group";
        public static string GroupTrans = "Group Transactions saved successfully";
        public static string GroupTransupdate = "Group Transactions Updated successfully";
        public static string GroupTransNoupdate = "Please Save the data and then Update";
        public static string GroupTransupnodata = "There is no Transaction for this group";
        public static string GroupNodata = "Please enter the details to save";
        public static string SaveGroupTrans = "Please  save the Transactions";
        public static string Dataexist = "Data Already Exist.";
        public static string Transsaved = "This Transaction is already saved.Please enter new one.";
        public static string SelectGroup = "Please select the group to save the data";
        public enum MsgType
        {
            success = 1,
            failure = 2,
            warning = 3,
            Info = 4
        }
        public static void ShowMessage(string msg, MsgType msgType, Exception ex)
        {
            if (msgType.Equals(MsgType.success))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (msgType.Equals(MsgType.failure))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (msgType.Equals(MsgType.warning))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (msgType.Equals(MsgType.Info))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static void ShowMessage(string msg, MsgType msgType)
        {
            if (msgType.Equals(MsgType.success))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (msgType.Equals(MsgType.failure))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (msgType.Equals(MsgType.warning))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (msgType.Equals(MsgType.Info))
            {
                MessageBox.Show(msg, "Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }

    static class FamilyMessages
    {
        public static string SLUMNAME = "Select the Slum Name";
        public static string NAMEOFSETTLEMENT = "Name Of the Settlement";
        public static string HOUSEHUTNO = "House/Hut Number";
        public static string RATIONNUMBER = " Ration No.";
        public static string FAMILYSTAY = "How many year the family is staying";
        public static string FAMILYHEADNAME = "Family head name";
        public static string HUSBANDWIFENAME = "Husband and Wife Name.";
        public static string RADIOYESANDNO = "Check Yes or No";
        public static string YESRADIOBUTTON = "Details for yes";
        public static string HOUSEFORBISUNESS = "House for Bisuness";
        public static string CASTE = "Caste";
        public static string MONTHLYINCOME = "Monthly Income";
        public static string HOUSEDETAILS = "Hose Details";
        public static string MOTHERTONGUE = "Mother tongue";
        public static string STATE = "State";
        public static string WATER = "Water";
        public static string TOILET = "Toilet";
        public static string ELECTRICITY = "Eletricity";
        public static string COOKINGMEDIUM = " Cooking Medium";

        public static string SAVED_SUCCESS = "Details saved successfully.";
        public static string DELETED_SUCCESS = "Details deleted successfully.";
        public static string UPDATED_SUCCESS = "Details updated successfully.";
        public static string APPROVED_SUCCESS = "Details approved successfully.";
        public static string DELETE_PROCESSING = "Data is in use and so it cant be deleted";
        public static string DELETE_CONFIRM = "Are you sure to delete the selected data?";
        public static string DELETE_ACCOUNT = "Account cant be deleted as it has some transactions. Delete the transactions before deleting the account.";

        public static string COMMAWITHSPACE = ", ";
        public static string HYPHENWITHSPACE = " - ";


        public static string NAME = " Name";
        public static string Gender = "Gender";
        public static string Age = "Age";
        public static string Qualification = "Qualification";
        public static string IsMember = "IsMember";
        public static string MemberID = " Enter the Valid MemberID ";
        public static string MEMBERIDDUPLICATE = "Entered MemberId Is Dupliacte";

    }

    static class GlobalValues
    {
        private static Int64 m_MemberID;
        private static string m_Name;
        private static string m_MemberFullID;

        private static Int64 m_SavingsAccId;
        private static string m_SavingsAccNo;

        private static Int64 m_LoanAccId;
        private static string m_LoanAccNo;
        private static DateTime m_EMIDueDate;

        private static Int64 m_UserID;
        private static string m_UserFullName;
        private static string m_UserRole;
        private static int m_UserCenterId;

        private static string m_UPFAccNo;
        private static Int64 m_UPFAccId;

        private static Int64 m_SavingType;

        public static Int64 SavingType
        {
            get { return m_SavingType; }
            set { m_SavingType = value; }
        }

        public static Int64 UPFAccId
        {
            get { return m_UPFAccId; }
            set { m_UPFAccId = value; }
        }             

        //Edited by Shriti -- Start
        private static int mf_MemberFamilyId;
        private static Int64 mf_FamMemberId;
        //Edited by Shriti  --End

        public static Int64 User_PkId
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }
        public static string User_FullName
        {
            get { return m_UserFullName; }
            set { m_UserFullName = value; }
        }
        public static string User_Role
        {
            get { return m_UserRole; }
            set { m_UserRole = value; }
        }

        public static Int64 Member_PkId
        {
            get { return m_MemberID; }
            set { m_MemberID = value; }
        }
        public static Int64 Savings_PkId
        {
            get { return m_SavingsAccId; }
            set { m_SavingsAccId = value; }
        }
        public static string Member_Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public static string Member_ID
        {
            get { return m_MemberFullID; }
            set { m_MemberFullID = value; }
        }
        public static string Savings_AccNumber
        {
            get { return m_SavingsAccNo; }
            set { m_SavingsAccNo = value; }
        }

        public static Int64 Loan_PkId
        {
            get { return m_LoanAccId; }
            set { m_LoanAccId = value; }
        }

        public static string Loan_AccNumber
        {
            get { return m_LoanAccNo; }
            set { m_LoanAccNo = value; }
        }

        public static DateTime Loan_EMIDueDate
        {
            get { return m_EMIDueDate; }
            set { m_EMIDueDate = value; }
        }

        public static int User_CenterId
        {
            get { return m_UserCenterId; }
            set { m_UserCenterId = value; }
        }

        public static string UPF_AccNumber
        {
            get { return m_UPFAccNo; }
            set { m_UPFAccNo = value; }
        }

        //edited by shriti  -- start
        public static Int64 FamilyMember_PkId
        {
            get { return mf_FamMemberId; }
            set { mf_FamMemberId = value; }
        }
        public static int MemberFamilyId
        {
            get { return mf_MemberFamilyId; }
            set { mf_MemberFamilyId = value; }
        }

        //edited by shriti  -- end

        private static int m_GroupId;

        public static int GroupId
        {
            get { return m_GroupId; }
            set { m_GroupId = value; }
        }
    }
}
