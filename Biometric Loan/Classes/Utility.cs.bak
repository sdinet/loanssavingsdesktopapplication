﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace SPARC
{
    static class Constants
    {
        public static string SAVE = "Save";
        public static string UPDATE = "Update";
        public static string APPROVE = "Approve";

        public static string SELECTONE = "--Select One--";

        public static string CONFIRMDELETE = "Delete confirmation";

        public static string MASTERFORMHEADING = "Bio Metric Saving & Loan System";
        public static string WELCOME = "Welcome ";
        public static string DOBFORMAT = "d/M/yyyy";
        public static string DEBITTRANSABBREV = "DEB";
        public static string CREDITTRANSABBREV = "CRE";

        public static string CREATEMEMBER = "Create Member";
        public static string UPDATEMEMBER = "Update Member";
        public static string CREATESAVINGSACC = "Create Savings Account";
        public static string UPDATESAVINGSACC = "Update Savings Account";
        public static string DEPOSITSAVINGSTRANS = "Deposit Amount";
        public static string WITHDRAWSAVINGSTRANS = "Withdraw Amount";
        public static string EMIPAYLOANTRANS = "EMI Payment Transaction";
        public static string LOANDISPERSALTRANS = "Dispersal Transaction";

        public static string LOANUPDATE = "Update Loan Account";
        public static string LOANCREATE = "Create Loan Account";
        public static string LOANAPPROVE = "Approve Loan Account";

    }

    static class PayMode
    {
        public static string CASH = "Cash";
        public static string BANK = "Bank";
    }

    static class EMIType
    {
        public static string WEEKLY = "Weekly";
        public static string MONTHLY = "Monthly";
    }

    static class UserRoles
    {
        public static string ADMIN = "Admin";
        public static string USER = "User";
    }
    static class Status
    {
        public static string ACTIVE = "Active";
        public static string INITIATED = "Initiated";
        public static string CLOSED = "Closed";
        public static string APPROVED = "Approved";
        public static string WRITEOFF = "Write-off";

    }

    static class Messages
    {
        public static string PHOTO_NONIMAGE = "The Data in Clipboard is not an image format.";
        public static string PHOTO_EMPTYCB = "The Clipboard is empty";
        public static string ERRORMESSAGE = "Error occured! Contact Administrator for support.";
        public static string CLOSEWINDOWS = "Close all open windows to select a new member.";
        public static string CLOSEOPENFORM = "Save and close the current window to proceed.";
        public static string SELECTMEMBER = "Select a member to update and proceed.";
        public static string INVALIDUSER = "Invalid Username/password!";

        public static string SAVED_SUCCESS = "Details saved successfully.";
        public static string DELETED_SUCCESS = "Details deleted successfully.";
        public static string UPDATED_SUCCESS = "Details updated successfully.";
        public static string APPROVED_SUCCESS = "Details approved successfully.";
        public static string DELETE_PROCESSING = "Data is in use and so it cant be deleted";
        public static string DELETE_CONFIRM = "Are you sure to delete the selected data?";
        public static string DELETE_ACCOUNT = "Account cant be deleted as it has some transactions. Delete the transactions before deleting the account.";

        public static string DAYBOOKGEN_SUCCESS = "Day book (re)generated successfully.";
        public static string DAYBOOKGEN_ERROR = "Error while generating the data. Please try again.";
        public static string ERROR_PROCESSING = "Error while processing the data. Please try again.";

        public static string UPF_PAYOUT_XN_AVAILABLE = "Cannot delete the account. Please delete the pay-out transactions belongs to this account first.";

        public static string AGENTCREATED = "Agent created successfully.";
        public static string ERRAGENTSAVE = "Error in saving the Agent details.";

        public static string REQUIREDFIELDS = "Enter values for the below fields for saving: ";
        public static string VALIDVALUES = "Enter valid values for: ";
        public static string INVALIDDOB = "Invalid Date of Birth";
        public static string DISPERSLIMITCHK = "Error: Amount exceeds the pending amount to be dispersed.";
        public static string AMTUNEQUAL = "Error: Amount is not tallying between Principal and Charge.";
        public static string DISPERSALPENDING = "Error: Transactions cant be made if the dispersal amount is pending.";
        public static string EMIEXCEEDSLIMIT = "Error: Amount exceeds the pending amount to be paid.";
        public static string ACCNILBALANCE = "Account should have nil balance before closure.";
        public static string AVAILABLEBALCHECK = "Error: Amount exceeds the available balance.";
        public static string INTERESTPAYDATECHK = "Interest pay should be greater than Transaction date.";
        public static string NOPENDINGPAYOUTS = "There is no pending UPF interest payouts.";
        public static string MEMBERNAME = "Name";
        public static string MEMBERFATHERNAME = "Father Name";
        public static string MEMBERGENDER = "Gender";
        public static string MEMBERADDRESS1 = "Address1";
        public static string MEMBERADDRESS2 = "Address2";
        public static string MEMBERSLUM = "Slum";
        public static string MEMBERZipcode = "Zipcode";
        public static string AGENTNAME = "Agent Name";
        public static string PAYMODE = "Pay Mode";
        public static string PAYTHRUAGENTNAME = "Pay thru Agent Name";
        public static string AMOUNT = "Amount";
        public static string PRINCIPALAMOUNT = "Principal Amt";
        public static string CHARGEAMOUNT = "Charge Amt";
        public static string PRINCIPALORCHARGEAMOUNT = "Principal Amt or Charge Amount";

        public static string CODEVALUE = "Value";
        public static string ABBREVIATION = "Abbreviation";
        public static string PARENT = "Parent";
        public static string DISPLAYORDER = "Display Order";
        public static string FILTERBYPARENT = "Filter by Parent";

        public static string LOANAMOUNT = "Loan Amount";
        public static string LOANTENURE = "Tenure";
        public static string LOANName = "Loan Name";
        public static string EMIPAYCYCLE = "EMI Pay Cycle";

        public static string CATEGORY = "Category";
        public static string MODEOFPAYMENT = "Mode of Payment";
        public static string DESCRIPTION = "Description";

        public static string USERNAME = "User Name";
        public static string PASSWORD = "Password";

        public static string COMMAWITHSPACE = ", ";
        public static string HYPHENWITHSPACE = " - ";

        public static string SAVED_CURRENTMEMBER = "Current member is selected for further action.";
        public static string SAVED_CURRENTSBACC = "Savings account is selected for further action.";
        public static string SAVED_CURRENTLOANACC = "Loan account is selected for further action.";
        public static string SBACCALREADYEXIST = "Savings Account already exists. Click Update menu to edit.";
        public static string SBACCNOTEXIST = "Savings Account doesn't exists. Click Create menu to create one.";
        
        public static string LOANACCNOTAPPROVED = "This Loan account has to be approved for making a transaction.";
        public static string LOANACCNOTEXIST = "Loan Account doesn't exists. Click Create menu to create one.";
        public static string LOANACCALREADYEXIST = "Loan Account already exists. Click Update menu to edit.";

        public enum MsgType
        {
            success=1,
            failure=2,
            warning=3,
            Info=4
        }
        public static void ShowMessage(string msg,MsgType msgType, Exception ex)
        {
            if (msgType.Equals(MsgType.success))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (msgType.Equals(MsgType.failure))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (msgType.Equals(MsgType.warning))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (msgType.Equals(MsgType.Info))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static void ShowMessage(string msg, MsgType msgType)
        {
            if (msgType.Equals(MsgType.success))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (msgType.Equals(MsgType.failure))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (msgType.Equals(MsgType.warning))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (msgType.Equals(MsgType.Info))
            {
                MessageBox.Show(msg, "BioMetric Saving & Loan", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }

    static class GlobalValues
    {
        private static Int64 m_MemberID;
        private static string m_Name;
        private static string m_MemberFullID;

        private static Int64 m_SavingsAccId;
        private static string m_SavingsAccNo;

        private static Int64 m_LoanAccId;
        private static string m_LoanAccNo;
        private static DateTime m_EMIDueDate;

        private static Int64 m_UserID;
        private static string m_UserFullName;
        private static string m_UserRole;

        public static Int64 User_PkId
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }
        public static string User_FullName
        {
            get { return m_UserFullName; }
            set { m_UserFullName = value; }
        }
        public static string User_Role
        {
            get { return m_UserRole; }
            set { m_UserRole = value; }
        }

        public static Int64 Member_PkId
        {
            get { return m_MemberID; }
            set { m_MemberID = value; }
        }
        public static Int64 Savings_PkId
        {
            get { return m_SavingsAccId; }
            set { m_SavingsAccId = value; }
        }
        public static string Member_Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public static string Member_ID
        {
            get { return m_MemberFullID; }
            set { m_MemberFullID = value; }
        }
        public static string Savings_AccNumber
        {
            get { return m_SavingsAccNo; }
            set { m_SavingsAccNo = value; }
        }

        public static Int64 Loan_PkId
        {
            get { return m_LoanAccId; }
            set { m_LoanAccId = value; }
        }

        public static string Loan_AccNumber
        {
            get { return m_LoanAccNo; }
            set { m_LoanAccNo = value; }
        }

        public static DateTime Loan_EMIDueDate
        {
            get { return m_EMIDueDate; }
            set { m_EMIDueDate = value; }
        }
    }

}
