﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;
using System.Configuration;


namespace SPARC
{
    public partial class frmCreateMember : Form
    {
        //byte[] picture;
        //Object photo;
        //Image stuPhoto;
        MemoryStream ms = new MemoryStream();
        WebCam webcam;
        bool IsPhotoChanged = false;
        Image modifiedimage;
        string convertedtobase64;
        byte[] byteconvertedimage;
        string MemberDocImagePath;
        //#region WebCam API
        //const short WM_CAP = 1024;
        //const int WM_CAP_DRIVER_CONNECT = WM_CAP + 10;
        //const int WM_CAP_DRIVER_DISCONNECT = WM_CAP + 11;
        //const int WM_CAP_EDIT_COPY = WM_CAP + 30;
        //const int WM_CAP_SET_PREVIEW = WM_CAP + 50;
        //const int WM_CAP_SET_PREVIEWRATE = WM_CAP + 52;
        //const int WM_CAP_SET_SCALE = WM_CAP + 53;
        //const int WS_CHILD = 1073741824;
        //const int WS_VISIBLE = 268435456;
        //const short SWP_NOMOVE = 2;
        //const short SWP_NOSIZE = 1;
        //const short SWP_NOZORDER = 4;
        //const short HWND_BOTTOM = 1;
        //int iDevice = 0;
        //int hHwnd;
        //[System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessageA")]
        //static extern int SendMessage(int hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.AsAny)]object lparam);
        //[System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SetWindowPos")]
        //static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        //[System.Runtime.InteropServices.DllImport("user32")]
        //static extern bool DestroyWindow(int hndw);
        //[System.Runtime.InteropServices.DllImport("avicap32.dll")]
        //static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int x, int y, int nWidth, short nHeight, int nWndParent, int nID);
        //[System.Runtime.InteropServices.DllImport("avicap32.dll")]
        //static extern bool capGetDriverDescriptionA(short wDriver, string lpszName, int cbName, string lpszVer, int cbVer);

        //private void OpenPreviewWindow()
        //{
        //    int iHeight = 143;
        //    int iWidth = 189;
        //    hHwnd = capCreateCaptureWindowA(iDevice.ToString(), (WS_VISIBLE | WS_CHILD), 0, 0, 640, 480, imgCapture.Handle.ToInt32(), 0);
        //    if (SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) == 1)
        //    {
        //        // 
        //        // Set the preview scale
        //        // 
        //        SendMessage(hHwnd, WM_CAP_SET_SCALE, 1, 0);
        //        // 
        //        // Set the preview rate in milliseconds
        //        // 
        //        SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0);
        //        // 
        //        // Start previewing the image from the camera
        //        // 
        //        SendMessage(hHwnd, WM_CAP_SET_PREVIEW, 1, 0);
        //        // 
        //        //  Resize window to fit in picturebox
        //        // 
        //        SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, iWidth, iHeight, (SWP_NOMOVE | SWP_NOZORDER));
        //    }
        //    else
        //    {
        //        // 
        //        //  Error connecting to device close window
        //        //  
        //        DestroyWindow(hHwnd);
        //    }
        //}

        //private void ClosePreviewWindow()
        //{
        //    // 
        //    //  Disconnect from device
        //    // 
        //    SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0);
        //    // 
        //    //  close window
        //    // 
        //    DestroyWindow(hHwnd);
        //}

        //#endregion

        public Int16 intMemberOperationType;

        public frmCreateMember()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void CreateMember_Load(object sender, EventArgs e)
        {

            try
            {
                SqlConnection conn = new SqlConnection();
                conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["connstringindia"]);  //here we are getting the actual conn string for all the required operation
                string BackupDBName = conn.Database.ToString();   // Here we getting the DB file for which the back-up has to be taken

                if (BackupDBName != "Loans_and_Savings_SajjanRao_Staging")
                {
                    lblAdhaar.Visible = false;
                    txtAdhaarID.Visible = false;
                }
                //The Above Code Is Used To Hide The Label and Textbox Used To Enter The Aadhar Details If The Country Is Other Than India

                CreateBackupFolderMemberDocuments();
                dtpDOB.MaxDate = DateTime.Today;
                webcam = new WebCam();
                webcam.InitializeWebCam(ref imgVideo);

                FillCombos(Convert.ToInt32(LocationCode.DocumentType), 0, cmbDocType); //Fill Document Type
                FillCombos(Convert.ToInt32(LocationCode.Gender), 0, cmbGender); //Fill Gender
                //FillCombogroup(GlobalValues.GroupId);
                ClearControls();
                int CodeType = 0;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {
                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }
                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;
                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }
                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }

                if (intMemberOperationType == 1)
                {
                    lblMemberID.Visible = false;
                    txtMemberID.Visible = false;
                    btnDelete.Visible = false;
                    this.Text = Constants.CREATEMEMBER;
                }
                else
                {
                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                    {
                        btnDelete.Visible = true;
                    }
                    this.Text = Constants.UPDATEMEMBER + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

                    Members objMemberFetch = new Members();
                    objMemberFetch.Id = GlobalValues.Member_PkId;
                    LoadControls(objMemberFetch);
                    btnClear.Visible = false;
                    cmbCountry.Enabled = false;
                    cmbState.Enabled = false;
                    cmbHub.Enabled = false;
                    cmbDistrict.Enabled = false;
                    cmbSlum.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void CreateBackupFolderMemberDocuments()
        {
            string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
            string bpath = directory + "MembersDocuments";
            bool folderExists = Directory.Exists(bpath);
            if (!folderExists)
                Directory.CreateDirectory(bpath);
        }

        private void LoadControls(Members objMemberFetch)
        {
            DataSet dsMemberFetch = objMemberFetch.LoadMemberById();
            if (dsMemberFetch != null && dsMemberFetch.Tables.Count > 0 && dsMemberFetch.Tables[0].Rows.Count > 0)
            {
                lblID.Text = dsMemberFetch.Tables[0].Rows[0]["Id"].ToString();
                txtName.Text = dsMemberFetch.Tables[0].Rows[0]["Name"].ToString();
                txtFatherName.Text = dsMemberFetch.Tables[0].Rows[0]["FatherName"].ToString();
                txtEmail.Text = dsMemberFetch.Tables[0].Rows[0]["Email"].ToString();
                cmbGender.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["Gender"]);
                dtpDOB.Text = dsMemberFetch.Tables[0].Rows[0]["DateofBirth"].ToString();
                txtMobileNo.Text = dsMemberFetch.Tables[0].Rows[0]["Mobile"].ToString();
                txtHomePhone.Text = dsMemberFetch.Tables[0].Rows[0]["HomePhone"].ToString();
                txtMemberID.Text = dsMemberFetch.Tables[0].Rows[0]["MemberID"].ToString(); ;
                cmbHub.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_hubid"]);
                cmbCountry.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_CountryId"]);
                cmbState.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_StateId"]);
                cmbDistrict.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_DistrictId"]);
                cmbSlum.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_SlumId"]);
                txtAddress1.Text = dsMemberFetch.Tables[0].Rows[0]["Address1"].ToString();
                txtAddress2.Text = dsMemberFetch.Tables[0].Rows[0]["Address2"].ToString();
                txtTown.Text = dsMemberFetch.Tables[0].Rows[0]["Town"].ToString();
                txtZipcode.Text = dsMemberFetch.Tables[0].Rows[0]["Zip"].ToString();
                txtLandmark.Text = dsMemberFetch.Tables[0].Rows[0]["Landmark"].ToString();
                txtContactPerson.Text = dsMemberFetch.Tables[0].Rows[0]["ContactpersonName"].ToString();
                txtContactPhone.Text = dsMemberFetch.Tables[0].Rows[0]["ContactpersonNumber"].ToString();
                txtBiometricID.Text = dsMemberFetch.Tables[0].Rows[0]["BioMetricId"].ToString();
                txtPassbook.Text = dsMemberFetch.Tables[0].Rows[0]["PassbookNumber"].ToString();
                txtAdhaarID.Text = dsMemberFetch.Tables[0].Rows[0]["Adhaarid"].ToString();
                txtVoterID.Text = dsMemberFetch.Tables[0].Rows[0]["Voterid"].ToString();
                txtRemarks.Text = dsMemberFetch.Tables[0].Rows[0]["Remarks"].ToString();
                cmbGname.SelectedValue = dsMemberFetch.Tables[0].Rows[0]["fk_GroupId"].ToString();
                try
                {
                    if (dsMemberFetch.Tables[0].Rows[0]["Photo"] != null && dsMemberFetch.Tables[0].Rows[0]["Photo"] != DBNull.Value)
                    {
                        if (imgCapture.Image != null)
                        {
                            imgCapture.Image.Dispose();
                        }
                        MemoryStream ms = new MemoryStream((byte[])dsMemberFetch.Tables[0].Rows[0]["Photo"]);//create memory stream by passing byte array of the image
                        imgCapture.Image = Image.FromStream(ms);//set image property of the picture box by creating a image from stream 
                        imgCapture.SizeMode = PictureBoxSizeMode.StretchImage;//set size mode property of the picture box to stretch 
                        imgCapture.Refresh();//refresh picture box

                        //byte[] image = (byte[])dsMemberFetch.Tables[0].Rows[0]["Photo"];
                        //MemoryStream ms1 = new MemoryStream(image);
                        //imgCapture.Image = Bitmap.FromStream(ms1); 

                        //FileStream FS1 = new FileStream("image.jpg", FileMode.Create);
                        //byte[] blob = (byte[])dsMemberFetch.Tables[0].Rows[0]["Photo"];
                        //FS1.Write(blob, 0, blob.Length);
                        //FS1.Close();
                        //FS1 = null;
                        //imgCapture.Image = Image.FromFile("image.jpg");
                        //imgCapture.SizeMode = PictureBoxSizeMode.StretchImage;
                        //imgCapture.Refresh();


                        //byte[] result= (byte[])dsMemberFetch.Tables[0].Rows[0]["Photo"];
                        //MemoryStream stream = new MemoryStream(result);
                        ////MemoryStream stream = new MemoryStream(result, 78, result.Length - 78);
                        //imgCapture.Image = Image.FromStream(stream);

                        //Byte[] data;// = new Byte[0];
                        //data = (Byte[])(dsMemberFetch.Tables[0].Rows[0]["Photo"]);
                        //MemoryStream mem = new MemoryStream(data,0,data.Length);
                        //imgCapture.Image = Image.FromStream(mem,true);


                        //byte[] barrImg = (byte[])dsMemberFetch.Tables[0].Rows[0]["Photo"];
                        //string strfn = Convert.ToString(DateTime.Now.ToFileTime());
                        //FileStream fs = new FileStream(strfn, FileMode.CreateNew, FileAccess.Write);
                        //fs.Write(barrImg, 0, barrImg.Length);
                        //fs.Flush();
                        //fs.Close();
                        //imgCapture.Image = Image.FromFile(strfn);

                        //photo = dsMemberFetch.Tables[0].Rows[0]["Photo"];
                        //byte[] imageContent = (byte[])photo;
                        //MemoryStream ms = new MemoryStream(imageContent);
                        //stuPhoto = Image.FromStream(ms);
                        //pictureBox2.Image = stuPhoto;
                    }
                    if (dsMemberFetch.Tables[0].Rows[0]["ScanedImage"] != null && dsMemberFetch.Tables[0].Rows[0]["ScanedImage"] != DBNull.Value)
                        if (scanedImage.Image != null)
                        {
                            scanedImage.Image.Dispose();
                        }
                    MemoryStream ms1 = new MemoryStream((byte[])dsMemberFetch.Tables[0].Rows[0]["ScanedImage"]);//create memory stream by passing byte array of the image
                    scanedImage.Image = Image.FromStream(ms1);//set image property of the picture box by creating a image from stream 
                    scanedImage.SizeMode = PictureBoxSizeMode.StretchImage;//set size mode property of the picture box to stretch 
                    scanedImage.Refresh();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessage.Text = strMessage;
                }
                else
                {
                    lblMessage.Text = string.Empty;
                    Members objMember = new Members();
                    if (intMemberOperationType == 1)
                        objMember.Id = 0;
                    else
                        objMember.Id = GlobalValues.Member_PkId;

                    //if (imgCapture.Image != null && IsPhotoChanged)
                    //{
                    //    string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Image.Jpeg";
                    //    //imgCapture.Image.Save(@"" + path + "Image.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //    imgCapture.Image.Save(@path, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //    FileStream FS = new FileStream(@path, FileMode.Open, FileAccess.Read); //create a file stream object associate to user selected file 
                    //    byte[] img = new byte[FS.Length]; //create a byte array with size of user select file stream length
                    //    FS.Read(img, 0, Convert.ToInt32(FS.Length));//read user selected file stream in to byte array
                    //    objMember.Picture = img;

                    //    //System.Drawing.Image imag = imgCapture.Image;
                    //    //objMember.Picture = ConvertImageToByteArray(imag, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //    //byte[] byMemberPhoto = new byte[0];
                    //    //byMemberPhoto = convertPicBoxImageToByte(imgCapture);
                    //    //objMember.Picture = byMemberPhoto;
                    //}
                    //else
                    //    objMember.Picture = null;

                    objMember.Name = txtName.Text.Trim();
                    objMember.PassbookNumber = txtPassbook.Text.Trim();
                    objMember.Voterid = txtVoterID.Text.Trim();
                    objMember.Adhaarid = txtAdhaarID.Text.Trim();
                    objMember.FatherName = txtFatherName.Text.Trim();
                    objMember.Mobile = txtMobileNo.Text.Trim();
                    objMember.HomePhone = txtHomePhone.Text.Trim();
                    objMember.Email = txtEmail.Text.Trim();
                    objMember.DateofBirth = dtpDOB.Value;
                    objMember.Gender = Convert.ToInt16(cmbGender.SelectedValue);
                    objMember.Address1 = txtAddress1.Text.Trim();
                    objMember.Address2 = txtAddress2.Text.Trim();
                    objMember.Town = txtTown.Text.Trim();
                    objMember.Landmark = txtLandmark.Text.Trim();
                    objMember.Zip = txtZipcode.Text.Trim();
                    objMember.ContactpersonName = txtContactPerson.Text.Trim();
                    objMember.ContactpersonNumber = txtContactPhone.Text.Trim();
                    objMember.BioMetricId = txtBiometricID.Text.Trim();
                    objMember.Remarks = txtRemarks.Text.Trim();
                    objMember.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                    objMember.fk_CountryId = Convert.ToInt32(cmbCountry.SelectedValue);
                    objMember.fk_StateId = Convert.ToInt32(cmbState.SelectedValue);
                    objMember.fk_DistricId = Convert.ToInt32(cmbDistrict.SelectedValue);
                    objMember.fk_SlumId = Convert.ToInt32(cmbSlum.SelectedValue);
                    objMember.Groupname = Convert.ToInt32(cmbGname.SelectedValue);
                    objMember.Createdby = GlobalValues.User_PkId;
                    objMember.Updatedby = GlobalValues.User_PkId;
                    objMember.Status = Status.ACTIVE;//to be checked

                    int saveResult = objMember.SaveMember();
                    if (saveResult > 0)
                    {
                        if (objMember.Id == 0) GlobalValues.Member_PkId = saveResult;
                        GlobalValues.Member_Name = txtName.Text.ToUpper();
                        GlobalValues.Member_ID = lblMemberID.Text;

                        //saving picture
                        if (imgCapture.Image != null && IsPhotoChanged)
                        {
                            string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Image.Jpeg";
                            imgCapture.Image.Save(@path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            FileStream FS = new FileStream(@path, FileMode.Open, FileAccess.Read); //create a file stream object associate to user selected file 
                            byte[] img = new byte[FS.Length]; //create a byte array with size of user select file stream length
                            FS.Read(img, 0, Convert.ToInt32(FS.Length));//read user selected file stream in to byte array
                            Members objMem_Photo = new Members();
                            objMem_Photo.UpdatePhoto(img, GlobalValues.Member_PkId);
                        }

                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();
                        if (objMember.Id == 0)
                        {
                            lblMessage.Text = Messages.SAVED_SUCCESS + Messages.SAVED_CURRENTMEMBER;
                            lblMessage.ForeColor = Color.Green;
                            intMemberOperationType = 2;
                            btnSave.Text = Constants.UPDATE;
                            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                            {
                                btnDelete.Visible = true;
                            }
                            this.Text = Constants.UPDATEMEMBER + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                        }
                        else
                        {
                            lblMessage.Text = Messages.UPDATED_SUCCESS;
                            lblMessage.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        }
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert,
                               System.Drawing.Imaging.ImageFormat formatOfImage)
        {
            byte[] Ret;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    imageToConvert.Save(ms, formatOfImage);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }
            return Ret;
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = -1;

                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();
            
        
            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbGname.DisplayMember = "Group_Name";
                cmbGname.ValueMember = "Id";
                cmbGname.DataSource = dsgroup.Tables[0];

                DataRow dr = dsgroup.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dsgroup.Tables[0].Rows.InsertAt(dr, 0);   
                  

                  
                
            }
            else
            {
                cmbGname.DataSource = null;
                //cmbGname.SelectedValue="--Select--";
                DataRow dr = dsgroup.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            if (cmbSlum.SelectedIndex > 0)
            {
                Members ObjPassCheck = new Members();
                ObjPassCheck.PassbookNumber = txtPassbook.Text.Trim();
                ObjPassCheck.fk_SlumId = cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (int?)null;
                ObjPassCheck.OperationType = intMemberOperationType;
                ObjPassCheck.Id = GlobalValues.Member_PkId;
                DataSet dspasscheck = new DataSet();
                dspasscheck = ObjPassCheck.FetchPassbook();
                if (dspasscheck.Tables.Count > 0 && dspasscheck.Tables[0].Rows.Count > 0)
                {
                    ErrorMsg = Messages.SAMEPASSBOOKNUM + Messages.COMMAWITHSPACE;
                }
            }
            //  Regex RX = new Regex("^[a-zA-Z0-9]{1,20}@[a-zA-Z0-9]{1,20}.[a-zA-Z]{2,3}$");

            if (txtPassbook.Text.Trim().Length == 0)
                ErrorMsg = Messages.MEMBERPASSBOOKNUMBER + Messages.COMMAWITHSPACE;
            if (txtName.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERNAME + Messages.COMMAWITHSPACE;
            //CR-12 start
            if (txtFatherName.Text.Trim().Length == 0)
                //CR-12 end
                ErrorMsg += Messages.MEMBERFATHERNAME + Messages.COMMAWITHSPACE;
            //if (txtPassbook.Text.Trim().Length == 0)
            //    ErrorMsg = Messages.MEMBERPASSBOOKNUMBER + Messages.COMMAWITHSPACE;

            if (Convert.ToInt16(cmbGender.SelectedValue) <= 0)
                ErrorMsg += Messages.MEMBERGENDER + Messages.COMMAWITHSPACE;
            if (dtpDOB.Value >= DateTime.Today)
                ErrorMsg += Messages.INVALIDDOB + Messages.COMMAWITHSPACE;
            //if (txtEmail.Text.Trim().Length > 0 && !RX.IsMatch(txtEmail.Text))
            //    ErrorMsg += Messages.INVALIDEMAIL + Messages.COMMAWITHSPACE;
            if (cmbSlum.SelectedIndex <= 0)
                ErrorMsg += Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            if (txtAddress1.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERADDRESS1 + Messages.COMMAWITHSPACE;
            if (txtAddress2.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERADDRESS2 + Messages.COMMAWITHSPACE;
            if (txtZipcode.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERZipcode + Messages.COMMAWITHSPACE;
            if (txtMobileNo.Text.Trim().Length != 0 && txtContactPhone.Text.Trim().Length != 0)
            {
                if (txtMobileNo.Text.Trim() == txtContactPhone.Text.Trim())
                    ErrorMsg += Messages.SAMEMOBNUM + Messages.COMMAWITHSPACE;
            }

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                lblMessage.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private byte[] convertPicBoxImageToByte(System.Windows.Forms.PictureBox pbImage)
        {
            MemoryStream ms1 = new MemoryStream();
            Image ImgConverted = this.resizeImage(pbImage.Image, new Size(125, 150));
            ImgConverted.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);//pbImage.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms1.ToArray();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void ClearControls()
        {
            txtName.Text = string.Empty;
            txtPassbook.Text = string.Empty;
            txtVoterID.Text = string.Empty;
            txtAdhaarID.Text = string.Empty;
            txtFatherName.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtHomePhone.Text = string.Empty;
            txtEmail.Text = string.Empty;
            cmbGender.SelectedIndex = 0;
            dtpDOB.Value = DateTime.Today;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtLandmark.Text = string.Empty;
            txtZipcode.Text = string.Empty;
            txtTown.Text = string.Empty;
            //cmbHub.SelectedIndex = 0;
            txtContactPerson.Text = string.Empty;
            txtContactPhone.Text = string.Empty;
            txtBiometricID.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            rdbCameraClick.Checked = true;
            scanedImage.Image = null;
            lblMessage.Text = string.Empty;
            cmbGname.Text = string.Empty;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                ofdPhoto.ShowDialog();
                ofdPhoto.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                if (ofdPhoto.ShowDialog() == DialogResult.OK)
                {
                    Image img = Image.FromFile(ofdPhoto.FileName);
                    imgCapture.Image = img;
                    //imgCapture.Image = new Bitmap(ofdPhoto.FileName);
                    IsPhotoChanged = true;
                    //imgCapture.Image.Width = 156;
                    //imgCapture.Image.Height = 161;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        private void rdbCameraClick_CheckedChanged(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnStop.Enabled = true;
            btnContinue.Enabled = true;
            btnCapture.Enabled = true;
            btnVideoSource.Enabled = true;
            btnBrowse.Enabled = false;
            txtFileUpload.Enabled = false;
        }

        private void rdbFileUpload_CheckedChanged(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            btnContinue.Enabled = false;
            btnCapture.Enabled = false;
            btnVideoSource.Enabled = false;
            btnBrowse.Enabled = true;
            txtFileUpload.Enabled = true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            webcam.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            webcam.Stop();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            webcam.Continue();
        }

        private void btnCapture_Click_1(object sender, EventArgs e)
        {
            imgCapture.Image = imgVideo.Image;
            IsPhotoChanged = true;
        }

        private void btnVideoSource_Click(object sender, EventArgs e)
        {
            webcam.AdvanceSetting();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                DeleteData();
            }
        }

        private void DeleteData()
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessage.Text = string.Empty;
            Members objMembers = new Members();
            objMembers.Id = GlobalValues.Member_PkId;
            dsDeleteStatus = objMembers.DeleteMember(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    objBioMetric.btnSearch_Click(null, null);
                    GlobalValues.Member_PkId = 0;
                    GlobalValues.Member_Name = string.Empty;
                    GlobalValues.Member_ID = string.Empty;
                    objBioMetric.FetchMemberDetailsByID();
                    objBioMetric.InsertMemberDetailsToGrid();
                    lblMessage.Text = Messages.DELETED_SUCCESS;
                    lblMessage.ForeColor = Color.Green;
                    this.Text = Constants.UPDATEMEMBER;
                    this.Close();
                }
                else if (iResult == 0)
                {
                    lblMessage.Text = Messages.DELETE_PROCESSING;
                    lblMessage.ForeColor = Color.Red;
                }
                else
                {
                    lblMessage.Text = Messages.ERROR_PROCESSING;
                    lblMessage.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessage.Text = Messages.ERROR_PROCESSING;
                lblMessage.ForeColor = Color.Red;
            }
        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblFatherName_Click(object sender, EventArgs e)
        {

        }

        private void txtFatherName_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblGender_Click(object sender, EventArgs e)
        {

        }

        private void lblDOB_Click(object sender, EventArgs e)
        {

        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lblMobile_Click(object sender, EventArgs e)
        {

        }

        private void txtMobileNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHomePhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblHomephone_Click(object sender, EventArgs e)
        {

        }

        private void lblEmail_Click(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtPassbook_TextChanged(object sender, EventArgs e)
        {

        }

        private void pnlMemberInfo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                int CodeType = 0;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {

                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }
                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        public void SaveImage(int width = 0, int height = 0, Image img = null)
        {
            Image originalImage = img;
            if (width > 0 && height > 0)
            {
                Image.GetThumbnailImageAbort myCallback =
                new Image.GetThumbnailImageAbort(ThumbnailCallback1);
                Image imageToSave = originalImage.GetThumbnailImage
                    (width, height, myCallback, IntPtr.Zero);
                // modifiedimage = imageToSave;
                string filePath2 = AppDomain.CurrentDomain.BaseDirectory + "savedNameafterreduce";
                imageToSave.Save(filePath2, System.Drawing.Imaging.ImageFormat.Jpeg);
                ImageToByte2(imageToSave);
            }
        }

        //public void SaveScannedImage(int width = 0, int height = 0, Image img = null)
        //{
        //    Image originalImage = img;
        //    if (width > 0 && height > 0)
        //    {
        //        Image.GetThumbnailImageAbort myCallback =
        //        new Image.GetThumbnailImageAbort(ThumbnailCallback1);
        //        Image imageToSave = originalImage.GetThumbnailImage
        //            (width, height, myCallback, IntPtr.Zero);
        //        string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
        //        string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "_" + txtImage.Text;
        //        MemberDocImagePath = FileNamePath.Substring(20);
        //        imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);

        //        // modifiedimage = imageToSave;
        //        //string filePath2 = AppDomain.CurrentDomain.BaseDirectory + "savedNameafterreduce";
        //        //imageToSave.Save(filePath2, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        //ImageToByte2(imageToSave);
        //    }
        //}

        public void SaveScannedImage(int width = 0, int height = 0, Image img = null)
        {
            Image originalImage = img;
            if (width > 0 && height > 0)
            {
                Image.GetThumbnailImageAbort myCallback =
                new Image.GetThumbnailImageAbort(ThumbnailCallback1);
                Image imageToSave = originalImage.GetThumbnailImage
                    (width, height, myCallback, IntPtr.Zero);
                string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
                string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "_" + txtImage.Text;
                MemberDocImagePath = FileNamePath.Substring(20);
                imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                // modifiedimage = imageToSave;
                //string filePath2 = AppDomain.CurrentDomain.BaseDirectory + "savedNameafterreduce";
                //imageToSave.Save(filePath2, System.Drawing.Imaging.ImageFormat.Jpeg);
                //ImageToByte2(imageToSave);
            }
            //Image imageToSave = originalImage;
            //string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
            //string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "_" + txtImage.Text;
            //MemberDocImagePath = FileNamePath.Substring(20);
            //imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);               
        }

        public byte[] ImageToByte2(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                //stream.Close();

                byteArray = stream.ToArray();
                byteconvertedimage = byteArray;
            }
            return byteArray;
        }

        private static bool ThumbnailCallback1() { return false; }

        private static bool ThumbnailCallback() { return false; }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessage.Text = strMessage;
                }
                else
                {
                    lblMessage.Text = string.Empty;

                    Members objMember = new Members();
                    if (intMemberOperationType == 1)
                        objMember.Id = 0;
                    else
                        objMember.Id = GlobalValues.Member_PkId;

                    //if (imgCapture.Image != null && IsPhotoChanged)
                    //{
                    //    string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Image.Jpeg";
                    //    //imgCapture.Image.Save(@"" + path + "Image.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //    imgCapture.Image.Save(@path, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //    FileStream FS = new FileStream(@path, FileMode.Open, FileAccess.Read); //create a file stream object associate to user selected file 
                    //    byte[] img = new byte[FS.Length]; //create a byte array with size of user select file stream length
                    //    FS.Read(img, 0, Convert.ToInt32(FS.Length));//read user selected file stream in to byte array
                    //    objMember.Picture = img;

                    //    //System.Drawing.Image imag = imgCapture.Image;
                    //    //objMember.Picture = ConvertImageToByteArray(imag, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //    //byte[] byMemberPhoto = new byte[0];
                    //    //byMemberPhoto = convertPicBoxImageToByte(imgCapture);
                    //    //objMember.Picture = byMemberPhoto;
                    //}
                    //else
                    //    objMember.Picture = null;

                    objMember.Name = txtName.Text.Trim();
                    objMember.PassbookNumber = txtPassbook.Text.Trim();
                    objMember.Voterid = txtVoterID.Text.Trim();
                    objMember.Adhaarid = txtAdhaarID.Text.Trim();
                    objMember.FatherName = txtFatherName.Text.Trim();
                    objMember.Mobile = txtMobileNo.Text.Trim();
                    objMember.HomePhone = txtHomePhone.Text.Trim();
                    objMember.Email = txtEmail.Text.Trim();
                    objMember.DateofBirth = dtpDOB.Value;
                    objMember.Gender = Convert.ToInt16(cmbGender.SelectedValue);
                    objMember.Address1 = txtAddress1.Text.Trim();
                    objMember.Address2 = txtAddress2.Text.Trim();
                    objMember.Town = txtTown.Text.Trim();
                    objMember.Landmark = txtLandmark.Text.Trim();
                    objMember.Zip = txtZipcode.Text.Trim();
                    objMember.ContactpersonName = txtContactPerson.Text.Trim();
                    objMember.ContactpersonNumber = txtContactPhone.Text.Trim();
                    objMember.BioMetricId = txtBiometricID.Text.Trim();
                    objMember.Remarks = txtRemarks.Text.Trim();
                    objMember.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                    objMember.fk_CountryId = Convert.ToInt32(cmbCountry.SelectedValue);
                    objMember.fk_StateId = Convert.ToInt32(cmbState.SelectedValue);
                    objMember.fk_DistricId = Convert.ToInt32(cmbDistrict.SelectedValue);
                    objMember.fk_SlumId = Convert.ToInt32(cmbSlum.SelectedValue);
                    objMember.Createdby = GlobalValues.User_PkId;
                    objMember.Updatedby = GlobalValues.User_PkId;
                    objMember.Status = Status.ACTIVE;//to be checked
                    objMember.Groupname = Convert.ToInt32(cmbGname.SelectedValue);
                    int saveResult = objMember.SaveMember();

                    if (saveResult > 0)
                    {
                        if (objMember.Id == 0) GlobalValues.Member_PkId = saveResult;
                        GlobalValues.Member_Name = txtName.Text.ToUpper();
                        GlobalValues.Member_ID = lblMemberID.Text;

                        //saving picture
                        IsPhotoChanged = true;
                        if (imgCapture.Image != null && IsPhotoChanged)
                        {
                            Image originalImage = imgCapture.Image;//Image.FromFile(@"F:\Flower-Garden-Photos\15-2-16\552.jpg");
                            string filePath = AppDomain.CurrentDomain.BaseDirectory + "savedNamebeforereduce.Jpeg";
                            originalImage.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                            int newWidth = originalImage.Width * 50 / 100;
                            int newHeight = originalImage.Height * 50 / 100;
                            SaveImage(newWidth, newHeight, originalImage);

                            // Image img = modifiedimage;
                            Members objMem_Photo = new Members();
                            objMem_Photo.UpdatePhoto(byteconvertedimage, GlobalValues.Member_PkId);


                            //string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Image.Jpeg";

                            //imgCapture.Image.Save(@path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            //FileStream FS = new FileStream(@path, FileMode.Open, FileAccess.Read); //create a file stream object associate to user selected file 
                            //byte[] img = new byte[FS.Length]; //create a byte array with size of user select file stream length
                            //FS.Read(img, 0, Convert.ToInt32(FS.Length));//read user selected file stream in to byte array
                            //Members objMem_Photo = new Members();
                            //objMem_Photo.UpdatePhoto(img, GlobalValues.Member_PkId);
                        }

                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();

                        if (scanedImage.Image != null && IsPhotoChanged1)
                        {
                            Image originalImagescn = scanedImage.Image;
                            int newWidth = originalImagescn.Width * 50 / 100;
                            int newHeight = originalImagescn.Height * 50 / 100;

                            SaveScannedImage(newWidth, newHeight, originalImagescn);
                            Members objMem_Img = new Members();
                            objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                        }

                        if (objMember.Id == 0)
                        {
                            lblMessage.Text = Messages.SAVED_SUCCESS + Messages.SAVED_CURRENTMEMBER;
                            lblMessage.ForeColor = Color.Green;
                            intMemberOperationType = 2;
                            btnSave.Text = Constants.UPDATE;
                            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                            {
                                btnDelete.Visible = true;
                            }
                            this.Text = Constants.UPDATEMEMBER + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                        }
                        else
                        {
                            lblMessage.Text = Messages.UPDATED_SUCCESS;
                            lblMessage.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        }
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                DeleteData();
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbState_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
            cmbGname.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
            cmbGname.DataSource = null;
        }

        private void btnStart_Click_1(object sender, EventArgs e)
        {
            webcam.Start();
        }

        private void btnStop_Click_1(object sender, EventArgs e)
        {
            webcam.Stop();
        }

        private void btnContinue_Click_1(object sender, EventArgs e)
        {
            webcam.Continue();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            imgCapture.Image = imgVideo.Image;
            IsPhotoChanged = true;
        }

        private void btnVideoSource_Click_1(object sender, EventArgs e)
        {
            webcam.AdvanceSetting();
        }

        private void rdbCameraClick_CheckedChanged_1(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnStop.Enabled = true;
            btnContinue.Enabled = true;
            btnCapture.Enabled = true;
            btnVideoSource.Enabled = true;
            btnBrowse.Enabled = false;
            txtFileUpload.Enabled = false;
        }

        private void rdbFileUpload_CheckedChanged_1(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            btnContinue.Enabled = false;
            btnCapture.Enabled = false;
            btnVideoSource.Enabled = false;
            btnBrowse.Enabled = true;
            txtFileUpload.Enabled = true;
        }

        private void txtFileUpload_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click_1(object sender, EventArgs e)
        {
            try
            {
                ofdPhoto.ShowDialog();
                ofdPhoto.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                if (ofdPhoto.ShowDialog() == DialogResult.OK)
                {
                    Image img = Image.FromFile(ofdPhoto.FileName);
                    imgCapture.Image = img;
                    //imgCapture.Image = new Bitmap(ofdPhoto.FileName);
                    IsPhotoChanged = true;
                    //imgCapture.Image.Width = 156;
                    //imgCapture.Image.Height = 161;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void lblMessage_Click(object sender, EventArgs e)
        {

        }

        private void pnlPhoto_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dtpDOB_ValueChanged_1(object sender, EventArgs e)
        {

        }

        private void imgVideo_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }


        // Old House photo
        private Image img;

        // New House photo
        private Image ScannedImg;

        // family photo
        // private Image img1;

        // byte[] housePhoto; // house image
        byte[] ScannedImage; // house image
        //byte[] familyPhoto;// family image
        bool IsPhotoChanged1 = false;
        private void btnImage_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbDocType.SelectedIndex > 0)
                {
                    ofdPhoto.ShowDialog();
                    ofdPhoto.Multiselect = true;
                    ofdPhoto.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                    if (ofdPhoto.ShowDialog() == DialogResult.OK)
                    {
                        string path = ofdPhoto.FileName;

                        txtImage.Text = new DirectoryInfo(path).Name;
                        Image img = Image.FromFile(ofdPhoto.FileName);
                        scanedImage.Image = img;

                        //imgCapture.Image = new Bitmap(ofdPhoto.FileName);
                        IsPhotoChanged1 = true;
                        //imgCapture.Image.Width = 156;
                        //imgCapture.Image.Height = 161;
                    }
                }
                else
                    MessageBox.Show("Select document type");
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        private bool isfill = false;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //scanedImage.Top = (int)(scanedImage.Top - (scanedImage.Height * 0.025));
            //scanedImage.Left = (int)(scanedImage.Left - (scanedImage.Width * 0.025));
            //scanedImage.Height = (int)(scanedImage.Height + (scanedImage.Height * 0.05));
            //scanedImage.Width = (int)(scanedImage.Width + (scanedImage.Width * 0.05));
            // CaptureScreen();
        }

        private void scanedImage_MouseClick(object sender, MouseEventArgs e)
        {
            EnlargeImage EnImg = new EnlargeImage(this.scanedImage.Image);
            if (this.scanedImage.Image == null)
            {
                MessageBox.Show("There is no image to show");
            }
            else
            {
                EnImg.WindowState = FormWindowState.Maximized;
                EnImg.Show();
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void txtMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtHomePhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtContactPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbSlum_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSlum.SelectedIndex > 0)

                FillCombogroup(Convert.ToInt32(cmbSlum.SelectedValue));
            else
                cmbGname.DataSource = null;
        }


        private void imgCapture_Click(object sender, EventArgs e)
        {

        }

        private void cmbSlum_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbSlum.SelectedIndex > 0)

                FillCombogroup(Convert.ToInt32(cmbSlum.SelectedValue));
            else
                cmbGname.DataSource = null;
        }
    }




}