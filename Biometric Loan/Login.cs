﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Configuration;
namespace SPARC
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (txtUserName.Text.Trim().Length == 0)
                ErrorMsg = Messages.USERNAME + Messages.COMMAWITHSPACE;
            if (txtPassword.Text.Trim().Length == 0)
                ErrorMsg += Messages.PASSWORD+ Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.VALIDVALUES + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessage.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }


        private  void PageInitialize ()
        {
            try
            {

                //if (Cmbcountry.SelectedItem == "Nepal")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringNepal"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncNepal"].Value;
                //    config.AppSettings.Settings["Country"].Value = "NEPAL";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}
                //if (Cmbcountry.SelectedItem == "India")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringIndia"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncIndia"].Value;
                //    config.AppSettings.Settings["Country"].Value = "INDIA";

                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //    ConfigurationManager.RefreshSection("Country");

                //}
                //if (Cmbcountry.SelectedItem == "Uganda")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringUganda"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncUganda"].Value;
                //    config.AppSettings.Settings["Country"].Value = "UGANDA";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}
                //if (Cmbcountry.SelectedItem == "Kenya")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringKenya"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncKenya"].Value;
                //    config.AppSettings.Settings["Country"].Value = "KENYA";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}

                //if (Cmbcountry.SelectedItem == "Ghana")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringGhana"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncGhana"].Value;
                //    config.AppSettings.Settings["Country"].Value = "GHANA";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}
                //if (Cmbcountry.SelectedItem == "Namibia")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringNam"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnStringNam"].Value;
                //    config.AppSettings.Settings["Country"].Value = "NAMIBIA";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}

                //if (Cmbcountry.SelectedItem == "Sierra Leone")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringSL"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncSL"].Value;
                //    config.AppSettings.Settings["Country"].Value = "SIERRA LEONE";

                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}
                //if (Cmbcountry.SelectedItem == "Zimbabwe")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringZim"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncZim"].Value;
                //    config.AppSettings.Settings["Country"].Value = "ZIMBABWE";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}
                //if (Cmbcountry.SelectedItem == "Srilanka")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringSri"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncSri"].Value;
                //    config.AppSettings.Settings["Country"].Value = "Srilanka";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}

                //if (Cmbcountry.SelectedItem == "Philippinnes")
                //{
                //    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //    config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringPhi"].Value;
                //    config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncPhi"].Value;
                //    config.AppSettings.Settings["Country"].Value = "Philippinnes";
                //    config.Save(ConfigurationSaveMode.Modified);

                //    ConfigurationManager.RefreshSection("ConnString");
                //}

                //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["ConnStringSri"].Value;
                //config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncSri"].Value;
                //config.AppSettings.Settings["Country"].Value = "Srilanka";
                //config.Save(ConfigurationSaveMode.Modified);

                //ConfigurationManager.RefreshSection("ConnString");

                /////////////////////////////////////
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["ConnString"].Value = config.AppSettings.Settings["connstringindia"].Value;
                config.AppSettings.Settings["ConnSync"].Value = config.AppSettings.Settings["ConnSyncIndia"].Value;
                config.AppSettings.Settings["Country"].Value = "INDIA";

                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("ConnString");
                ConfigurationManager.RefreshSection("Country");
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter the userid and password to login to your country");
            }
        }
        private void btnlogin_Click(object sender, EventArgs e)        
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {                
                    string strMessage = DoValidations();
                    if (strMessage.Length > 0)
                        lblMessage.Text = strMessage;
                    else
                    {
                        PageInitialize();

                        Users objUser = new Users();
                        objUser.UserName = txtUserName.Text.Trim();
                        objUser.Password = txtPassword.Text.Trim();
                        try
                        {

                            DataSet dsUser = objUser.GetUserDetails();

                            if ((dsUser != null && dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0))
                            {

                                GlobalValues.User_PkId = Convert.ToInt64(dsUser.Tables[0].Rows[0]["Id"].ToString());
                                GlobalValues.User_FullName = dsUser.Tables[0].Rows[0]["FullName"].ToString();
                                GlobalValues.User_Role = dsUser.Tables[0].Rows[0]["Role"].ToString();
                                if (dsUser.Tables[0].Rows[0]["fk_CenterId"] is DBNull)
                                {
                                    GlobalValues.User_CenterId = 0;
                                }
                                else
                                {
                                    GlobalValues.User_CenterId = Convert.ToInt32(dsUser.Tables[0].Rows[0]["fk_CenterId"].ToString());
                                }
                                this.Hide();

                                frmBioMetricSystem objMain = new frmBioMetricSystem();
                                objMain.Show();
                            }

                            else
                            {
                                lblMessage.Text = Messages.INVALIDUSER;
                                lblMessage.ForeColor = Color.Red;
                                objUser = null;
                            

                            }
                        }
                         catch (Exception ex)
                        {
                            MessageBox.Show("Unable To Connect Please Try Again.");
                        }


                    }
               
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            Cursor.Current = Cursors.Default;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
         

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageInitialize();
        }
    }
}