﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Timers;
using System.Windows.Threading;
using System.IO;
using SPARC;
using System.Reflection;



namespace SPARC
{
    public partial  class frmBioMetricSystem : Form
    {
        byte[] picture;
        Object objMemPhoto;
        Image imgMemPhoto;
        MemoryStream ms = new MemoryStream();

        private int childFormNumber = 0;
        private bool isMemberSearched = false;
        string strHelpPath = System.IO.Path.Combine(Application.StartupPath, "LoansSavings.chm");
        //private string m_MemberId;
        public frmBioMetricSystem()
        {
            InitializeComponent();
        }

        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private void BioMaster_Load(object sender, EventArgs e)
        {         
            ShowAdminMenus();
            ShowFunderMenus();
            //ShowReportMenus();

            scMain.Panel1.HorizontalScroll.Enabled = true;
            scMain.Panel1.HorizontalScroll.Visible = true;
            //edited by Pawan--start
            fetchtransactiondetails();
            //edited by Pawan--end
            this.Text = Constants.MASTERFORMHEADING + Messages.HYPHENWITHSPACE + Constants.WELCOME + GlobalValues.User_FullName;

            //set version info
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            this.toolStripStatuslblVersion.Text = String.Format(this.toolStripStatuslblVersion.Text, version.Major, version.Minor, version.Build, version.Revision);

            //string strControlVal = "Data Sync"; //Admin Tool Strip Menu Item Disable

            //foreach (ToolStripMenuItem item in Admin.DropDownItems)
            //{
            //    if (strControlVal == item.Name)
            //    {
            //        item.Visible = false;
            //    }
            //}
           

            int CodeType=0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
             
            }
            
            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
           
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;
           
            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }
                if (dr["CodeType"].ToString() == "0")
                {
                    //check for the country type code and populate country with countries
                 //   FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);

                  // cmbHub.SelectedValue = Id;
                    
                    //cmbHub.SelectedValue = GlobalValues.User_CenterId;
                    //cmbHub.Enabled = false;
                    //FillCombos(Convert.ToInt32(LocationCode.Country),GlobalValues.User_CenterId, cmbcountry);
                    //cmbcountry.SelectedValue = Id;
                    //cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                   
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the state type code and populate state 
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                    cmbcountry.SelectedValue = Id;
                    cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the District type code and populate district
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                    cmbstate.SelectedValue = Id;
                    cmbstate.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the Slum type code and populate Slum
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                    cmbdistrict.SelectedValue = Id;
                    cmbdistrict.Enabled = false;
                }               
            }
            lblCountry.Text = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["Country"].Value;
           
            helpProvider1.HelpNamespace = strHelpPath;
        }

      
        private void ShowAdminMenus()
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
            {                
                Admin.Visible = true;             
            }
            else
            {                
                Admin.Visible = false;                
            }
        }

        private void ShowFunderMenus()
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
            {
                fundersToolStripMenuItem.Visible = true;               
            }
            else
            {                
                fundersToolStripMenuItem.Visible = false;                
            }
        }

        private void ShowReportMenus()
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
            {
                Reports.Visible = true;                
            }
            else
            {                
                Reports.Visible = false;                
            }
        }

        #region Populate Location 
        private void cmbhub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }
        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }
        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }
        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }
      
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;
            
            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues!=null&& dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);


                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        private void Load_User_Location_Preferences()
        {
            string xml_path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Preferences.xml";
            DataSet ds = new DataSet();
            ds.ReadXml(xml_path);
            DataRow dr = ds.Tables[0].Rows[0];
            string country = (string)dr["Country"];
            string state = (string)dr["State"];
            string district = (string)dr["District"];
            string taluk = (string)dr["Taluk"];
            string slum = (string)dr["Slum"];
            if (country.Trim().Length > 0)
            {
                cmbcountry.SelectedValue = country;
            }
            if (country.Trim().Length > 0 && cmbcountry.SelectedIndex>0)//populate state
            {
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
                if (state.Trim().Length > 0)
                    cmbstate.SelectedValue = state;
            }
            if (state.Trim().Length > 0 && cmbstate.SelectedIndex > 0)//populate district
            {
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
                if (district.Trim().Length > 0)
                    cmbdistrict.SelectedValue = district;
            }
            if (district.Trim().Length > 0 && cmbdistrict.SelectedIndex > 0)//populate taluk
            {
                FillCombos(Convert.ToInt32(LocationCode.Taluk), Convert.ToInt32(cmbdistrict.SelectedValue), cmbHub);
                if (taluk.Trim().Length > 0)
                    cmbHub.SelectedValue = taluk;
            }
            if (taluk.Trim().Length > 0 && cmbHub.SelectedIndex > 0)//populate slum
            {
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbHub.SelectedValue), cmbslum);
                if (slum.Trim().Length > 0)
                    cmbslum.SelectedValue = slum;
            }
        }
        #endregion       

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void agentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Master_Pages.frmUserManager objUser = new Master_Pages.frmUserManager();
            objUser.TopLevel = false;
            objUser.Parent = scMain.Panel1;
            objUser.WindowState = FormWindowState.Maximized;
            objUser.Show();
        }

        private bool OpenFormsFound()
        {
            bool bOpenForms = false;
            if (this.scMain.Panel1.Controls.Count > 2)
            {
                bOpenForms = true;
            }
            return bOpenForms;
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            isMemberSearched = true;
            Members objMember = new Members();
            objMember.MemberId = txtmemberid.Text.Trim();
            objMember.Name = txtname.Text.Trim();
            objMember.BioMetricId = txtbioid.Text.Trim();
            objMember.PassbookNumber = txtPassbook.Text.Trim();
            objMember.fk_HubId = cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (int?)null;
            objMember.fk_CountryId = cmbcountry.SelectedIndex > 0 ? Convert.ToInt16(cmbcountry.SelectedValue.ToString()) : (int?) null;
            objMember.fk_StateId = cmbstate.SelectedIndex > 0 ? Convert.ToInt32(cmbstate.SelectedValue.ToString()) : (int?) null;
            objMember.fk_DistricId = cmbdistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbdistrict.SelectedValue.ToString()) : (int?) null;            
            objMember.fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?) null;

            DataSet dsMembers = objMember.SearchMember();
            if (dsMembers.Tables[0].Rows.Count > 0)
            {
                dgMembers.AutoGenerateColumns = false;
                dgMembers.DataSource = dsMembers.Tables[0];
                Cursor.Current = Cursors.Default;
            }
            else
            {
                Cursor.Current = Cursors.Default;
                dgMembers.DataSource = null;
                MessageBox.Show("Member does not exist");               
            }
        }

        //public string GetCompletedLoanIntemation(string an)
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[1];
        //    int intCounter = 0;

        //    sqlParam[intCounter] = new SqlParameter("@MemberId", SqlDbType.NVarChar, 20);
        //    sqlParam[intCounter++].Value = an;
        //    return Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_LoanCloseIntemation", sqlParam));
        //}

        public string GetCompletedLoanIntemation(Int64 an)
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            int intCounter = 0;

            sqlParam[intCounter] = new SqlParameter("@MemberId", SqlDbType.NVarChar, 20);
            sqlParam[intCounter++].Value = an;
            return Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_LoanCloseIntemation", sqlParam));
        }

        public string GetCompletedLoanIntemationNext(string an2)
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            int intCounter = 0;

            sqlParam[intCounter] = new SqlParameter("@MemberId", SqlDbType.NVarChar, 20);
            sqlParam[intCounter++].Value = an2;
            return Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_LoanCloseIntemationNext", sqlParam));
        }
        
        private void dgMembers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)        
        {
            Cursor.Current = Cursors.WaitCursor;
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEWINDOWS, Messages.MsgType.warning);
            else
            {
                if (e.RowIndex >= 0)
                {
                    //GlobalValues.Member_PkId = Convert.ToInt32(dgMembers.Rows[e.RowIndex].Cells[0].Value.ToString());

                    GlobalValues.Member_PkId = Convert.ToInt64(dgMembers.Rows[e.RowIndex].Cells[0].Value.ToString());
                    GlobalValues.Member_ID = dgMembers.Rows[e.RowIndex].Cells[1].Value.ToString();
                    GlobalValues.Member_Name = dgMembers.Rows[e.RowIndex].Cells[2].Value.ToString().ToUpper();
                    
                    FetchMemberDetailsByID();
                    InsertMemberDetailsToGrid();

                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                    {
                        string as1 = GetCompletedLoanIntemation(GlobalValues.Member_PkId);                       
                        if (as1 != null && as1 == "0" )//&& as2 != null && as2 == "0")
                        {
                            MessageBox.Show("Loan for this Member has been completed please close it", "Important Message");
                        }
                    }
                }
                else
                {
                    Messages.ShowMessage("Please select a Member by double clicking inside the row", Messages.MsgType.Info);
                }
            }
            Cursor.Current = Cursors.Default;
        }
        //edited by Pawan--start
        // Code to fetch last transaction date for savings, repayment and last member added
        // new changes also made in the str proc(usp_TransactionDetailsSelect) need to update it in org app & server
        private void fetchtransactiondetails()
        {
            Members member = new Members();
            DataSet fetchdetails = null;
            fetchdetails = member.FetchTransactionDetails();
            if (fetchdetails != null && fetchdetails.Tables.Count > 0 && fetchdetails.Tables[0].Rows.Count > 0)
            {
                String TransactionDate = fetchdetails.Tables[0].Rows[0]["TransactionDate"].ToString();
                String DayDifference = fetchdetails.Tables[0].Rows[0]["DayDifference"].ToString();
                label11.Text = "Last Savings : " + TransactionDate + ", " + DayDifference + " Days Ago.";

                String TransactionDate1 = fetchdetails.Tables[1].Rows[0]["TransactionDate"].ToString();
                String DayDifference1 = fetchdetails.Tables[1].Rows[0]["DayDifference"].ToString();
                label13.Text = "Last Loan     : " + TransactionDate1 + ", " + DayDifference1 + " Days Ago.";

                String TransactionDate2 = fetchdetails.Tables[2].Rows[0]["TransactionDate"].ToString();
                String DayDifference2 = fetchdetails.Tables[2].Rows[0]["DayDifference"].ToString();
                label15.Text = "Last Member  : " + TransactionDate2 + ", " + DayDifference2 + " Days Ago.";

            }
        }

        //edited by Pawan-end

        private void ClearQuickInfoControls()
        {
            lblname.Text = string.Empty;
            lblMemberID.Text = string.Empty;
            lblSlum.Text = string.Empty;
            lblSavingAccNo.Text = string.Empty;
            lblSavingBalance.Text = string.Empty;
            lblLoanStatusValue.Text = string.Empty;
            lblLoanAccNo.Text = string.Empty;
            lblEMIDue.Text = string.Empty;
            lblLoanAmount.Text = string.Empty;
            pbMember.Image = null;
            lblSavingAccStatus.Text = string.Empty;
        }

        /// <summary>
        /// Displays Quick Info of Member
        /// </summary>
        public void FetchMemberDetailsByID()
        {
            ClearQuickInfoControls();
            Members objMembers = new Members();
            objMembers.Id = GlobalValues.Member_PkId;
            DataSet dsMemberQuickInfo = objMembers.FetchQuickInfoByID();
            if (dsMemberQuickInfo!=null && dsMemberQuickInfo.Tables[0].Rows.Count > 0)
            {
                scSearch.Panel2Collapsed = false;
                lblname.Text = dsMemberQuickInfo.Tables[0].Rows[0]["Name"].ToString();
                lblMemberID.Text = dsMemberQuickInfo.Tables[0].Rows[0]["MemberId"].ToString();
                GlobalValues.Member_ID = dsMemberQuickInfo.Tables[0].Rows[0]["MemberId"].ToString();
                lblSlum.Text = dsMemberQuickInfo.Tables[0].Rows[0]["Slum"].ToString();
                //lblSavingAccNo.Text = dsMemberQuickInfo.Tables[0].Rows[0]["SavingAccNo"].ToString();
                //lblSavingBalance.Text = "Rs. " + dsMemberQuickInfo.Tables[0].Rows[0]["SavingBalance"].ToString();
                //lblLoanAccNo.Text = dsMemberQuickInfo.Tables[0].Rows[0]["LoanAccNo"].ToString();
                //lblLoanAmount.Text = dsMemberQuickInfo.Tables[0].Rows[0]["LoanAmount"].ToString();               
                String date = dsMemberQuickInfo.Tables[0].Rows[0]["TransactionDate"].ToString();
                //lblEMIDue.Text = dsMemberQuickInfo.Tables[0].Rows[0]["BalancePrinciple"].ToString();               
                //lblLoanStatusValue.Text = dsMemberQuickInfo.Tables[0].Rows[0]["LoanStatus"].ToString();

                //TimeSpan difference;
                //if (date != "")
                //{
                //    DateTime now = DateTime.Now;
                //    difference = now - Convert.ToDateTime(date);
                //    lblSavingAccStatus.Text = dsMemberQuickInfo.Tables[0].Rows[0]["Stat"].ToString();
                //    if (lblSavingAccStatus.Text == "Active")
                //    {
                //        if (difference.Days > 90)
                //        {
                //            lblSavingAccStatus.Text = "Inoperative";
                //            lblSavingAccStatus.ForeColor = Color.Orange;                            
                //        }
                //        else
                //            lblSavingAccStatus.ForeColor = Color.Green;
                //    }
                //    else if (lblSavingAccStatus.Text == "Closed")
                //    {
                //        lblSavingAccStatus.ForeColor = Color.Red;
                //    }
                //}
                //else if (lblSavingAccNo.Text != "")
                //{
                //    lblSavingAccStatus.Text = "Active";
                //    lblSavingAccStatus.ForeColor = Color.Green;
                //}               
                try
                {
                    if (dsMemberQuickInfo.Tables[0].Rows[0]["Photo"] != null && dsMemberQuickInfo.Tables[0].Rows[0]["Photo"] != DBNull.Value)
                    {
                        if (pbMember.Image != null)
                        {
                            pbMember.Image.Dispose();
                        }
                        MemoryStream ms = new MemoryStream((byte[])dsMemberQuickInfo.Tables[0].Rows[0]["Photo"]);//create memory stream by passing byte array of the image
                        pbMember.Image = Image.FromStream(ms);//set image property of the picture box by creating a image from stream 
                        pbMember.SizeMode = PictureBoxSizeMode.StretchImage;//set size mode property of the picture box to stretch 
                        pbMember.Refresh();//refresh picture box
                    }
                }
                catch (Exception ex)
                {
                }

                //Customize the menu for Admin
                if (!(lblLoanStatusValue.Text.ToUpper() == Status.INITIATED.ToUpper() && GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper()))
                {
                    //ViewApproveLoanAcc.Visible = false;
                    //ViewUpdateLoanAcc.Visible = true;
                }
                else
                {
                    //ViewApproveLoanAcc.Visible = true;
                    //ViewUpdateLoanAcc.Visible = false;
                }
                
                if (dsMemberQuickInfo.Tables[0].Rows[0]["SavingsAccId"] == DBNull.Value)
                    GlobalValues.Savings_PkId = 0;
                else
                {
                   GlobalValues.Savings_PkId = Convert.ToInt64(dsMemberQuickInfo.Tables[0].Rows[0]["SavingsAccId"]);
                  //  GlobalValues.Savings_AccNumber = lblSavingAccNo.Text;

                 //   GlobalValues.Loan_PkId = dsMemberQuickInfo.Tables[0].Rows[0]["LoanAccId"]==DBNull.Value?0:Convert.ToInt64(dsMemberQuickInfo.Tables[0].Rows[0]["LoanAccId"]);
                 //   GlobalValues.Loan_AccNumber = dsMemberQuickInfo.Tables[0].Rows[0]["LoanAccNo"].ToString();
                 //   GlobalValues.Loan_EMIDueDate = Convert.ToDateTime(dsMemberQuickInfo.Tables[0].Rows[0]["EMIDue"]);

                    GlobalValues.UPF_AccNumber = dsMemberQuickInfo.Tables[0].Rows[0]["UPFAccNo"].ToString();
                    GlobalValues.UPFAccId = dsMemberQuickInfo.Tables[0].Rows[0]["UPFAccId"] == DBNull.Value ? 0 : Convert.ToInt64(dsMemberQuickInfo.Tables[0].Rows[0]["UPFAccId"]);                       
                }               
               // lblEMIDue.Text = dsMemberQuickInfo.Tables[0].Rows[0]["BalancePrinciple"].ToString();
            }
        }

        public void InsertMemberDetailsToGrid()
        {
            Members objMemDet = new Members();
            objMemDet.Id = GlobalValues.Member_PkId;
            DataSet dsMemberDetailsGrid = objMemDet.FetchMemberInfo();
            if (dsMemberDetailsGrid.Tables[0].Rows.Count > 0)
            {
                //dgSavLoanDetails.AutoGenerateColumns = false;
                dgSavLoanDetails.DataSource = dsMemberDetailsGrid.Tables[0];
            }
            else
            {
                dgSavLoanDetails.DataSource = null;
            }
        }

        private void UpdateMember_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else
            {
                frmCreateMember frmCreateMember = new frmCreateMember();
                frmCreateMember.TopLevel = false;
                frmCreateMember.MdiParent = this;
                frmCreateMember.Parent = scMain.Panel1;
                frmCreateMember.WindowState = FormWindowState.Maximized;
                frmCreateMember.intMemberOperationType = 2;
                frmCreateMember.Show();
            }
        }

        private void CreateMember_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                frmCreateMember frmCreateMember = new frmCreateMember();
                frmCreateMember.TopLevel = false;
                frmCreateMember.MdiParent = this;
                frmCreateMember.Parent = scMain.Panel1;
                frmCreateMember.WindowState = FormWindowState.Maximized;
                frmCreateMember.intMemberOperationType = 1;
                frmCreateMember.Show();
            }
        }       

        private void ExitApp_Click(object sender, EventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }            
        }

        private void BioMetricSystem_MdiChildActivate(object sender, EventArgs e)
        {

        }

        private void CreateSavingsAccount_Click(object sender, EventArgs e)
        {
            OpenSavingsAccForm(1);
        }

        private void ViewUpdateSavingsAccount_Click(object sender, EventArgs e)
        {
            OpenSavingsAccForm(2);               
        }

        private void OpenSavingsAccForm(int iAction)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            //else if ( iAction == 1 && GlobalValues.Savings_PkId > 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCALREADYEXIST, Messages.MsgType.Info);
            //}
            //else if ((iAction == 2) && (GlobalValues.Savings_PkId <= 0))
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmSavingsAccount frmSavingsAcc = new frmSavingsAccount(iAction);
                frmSavingsAcc.TopLevel = false;
                frmSavingsAcc.MdiParent = this;                
                frmSavingsAcc.Parent = scMain.Panel1;
                frmSavingsAcc.WindowState = FormWindowState.Maximized;
                frmSavingsAcc.Show();
            }
        }

        private void CreateLoanAcc_Click(object sender, EventArgs e)
        {
            OpenLoanAccForm(1);
        }

        private void OpenLoanAccForm(Int16 iAction)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            { 
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            //else if (GlobalValues.Savings_PkId <= 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            //else if (iAction == 1 && GlobalValues.Loan_PkId > 0)
            //{
            //    Messages.ShowMessage(Messages.LOANACCALREADYEXIST, Messages.MsgType.Info);
            //}
            //else if (iAction>1 &&  GlobalValues.Loan_PkId <= 0)
            //{
            //    Messages.ShowMessage(Messages.LOANACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmLoanAccount frmLoanAcc = new frmLoanAccount(iAction);
                frmLoanAcc.TopLevel = false;
                frmLoanAcc.MdiParent = this;
                frmLoanAcc.iAction = iAction;
                frmLoanAcc.Parent = scMain.Panel1;
                frmLoanAcc.WindowState = FormWindowState.Maximized;
                frmLoanAcc.Show();
            }
        }

        private void ViewUpdateLoanAcc_Click(object sender, EventArgs e)
        {
            OpenLoanAccForm(2);
        }

        private void ViewApproveLoanAcc_Click(object sender, EventArgs e)
        {
            OpenLoanAccForm(3);
        }

        private void DepositSavingsTrans_Click(object sender, EventArgs e)
        {
            SavingsTransLoad(Constants.CREDITTRANSABBREV);
        }

        private void WithdrawSavingsTrans_Click(object sender, EventArgs e)
        {
            SavingsTransLoad(Constants.DEBITTRANSABBREV);
        }

        private void LoanTransLoad(string TransType)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            //else if (GlobalValues.Savings_PkId <= 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            //else if (lblLoanStatusValue.Text == Status.INITIATED)
            //{
            //    Messages.ShowMessage(Messages.LOANACCNOTAPPROVED, Messages.MsgType.Info);
            //}
            //else if (GlobalValues.Loan_AccNumber.Trim().Length <=0)
            //{
            //    Messages.ShowMessage(Messages.LOANACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmLoanAccTrans LoanTrans = new frmLoanAccTrans();
                LoanTrans.TopLevel = false;
                LoanTrans.MdiParent = this;
                LoanTrans.Parent = scMain.Panel1;
                LoanTrans.WindowState = FormWindowState.Maximized;
                LoanTrans.TransType = TransType;
                LoanTrans.Show();
            }
        }

        private void SavingsTransLoad(string TransType)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            //else if (GlobalValues.Savings_PkId <= 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmSavingsAccTrans DepositTrans = new frmSavingsAccTrans();
                DepositTrans.TopLevel = false;
                DepositTrans.MdiParent = this;
                DepositTrans.Parent = scMain.Panel1;
                DepositTrans.WindowState = FormWindowState.Maximized;
                DepositTrans.TransType = TransType;
                DepositTrans.Show();
            }
        }

        private void DispersalLoanTrans_Click(object sender, EventArgs e)
        {
            LoanTransLoad(Constants.DEBITTRANSABBREV);
        }

        private void EMIPaymentLoanAcc_Click(object sender, EventArgs e)
        {
            LoanTransLoad(Constants.CREDITTRANSABBREV);
        }

        private void CreateUPFAccount_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }

            //else if (GlobalValues.UPFAccId <= 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmUPFAccount objUPFAccount = new frmUPFAccount();
                objUPFAccount.TopLevel = false;
                objUPFAccount.MdiParent = this;
                objUPFAccount.Parent = scMain.Panel1;
                objUPFAccount.WindowState = FormWindowState.Maximized;
                objUPFAccount.Show();
            }
        }

        private void UPFInterestPayout_Click(object sender, EventArgs e)
        {

            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            //else if (GlobalValues.UPFAccId <= 0)
            //{
            //    Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            //}
            else
            {
                frmUPFInterestPayOut objUPFAccount = new frmUPFInterestPayOut();
                objUPFAccount.TopLevel = false;
                objUPFAccount.MdiParent = this;
                objUPFAccount.Parent = scMain.Panel1;
                objUPFAccount.WindowState = FormWindowState.Maximized;
                objUPFAccount.Show();
            }
        }

        private void GenerateDayBook_Click(object sender, EventArgs e)
        {
            //frmDayBook objDayBook = new frmDayBook();
            //objDayBook.TopLevel = false;
            //objDayBook.MdiParent = this;
            //objDayBook.Parent = scMain.Panel1;
            //objDayBook.WindowState = FormWindowState.Maximized;
            //objDayBook.Show();
        }

        private void Expenses_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                frmExpenses objExpenses = new frmExpenses();
                objExpenses.TopLevel = false;
                objExpenses.MdiParent = this;
                objExpenses.Parent = scMain.Panel1;
                objExpenses.WindowState = FormWindowState.Maximized;
                objExpenses.Show();
            }
        }

        private void frmBioMetricSystem_FormClosed(object sender, FormClosedEventArgs e)
        {
            //try
            //{
            ////save user preferences
            //string xml_path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Preferences.xml";
            //DataSet ds = new DataSet();
            //ds.ReadXml(xml_path);

            //ds.Tables[0].Rows[0]["Country"] = cmbcountry.SelectedIndex>0 ? cmbcountry.SelectedValue.ToString() : "";
            //ds.Tables[0].Rows[0]["State"] = cmbstate.SelectedIndex > 0 ? cmbstate.SelectedValue.ToString() : "";
            //ds.Tables[0].Rows[0]["District"] = cmbdistrict.SelectedIndex > 0 ? cmbdistrict.SelectedValue.ToString() : "";
            //ds.Tables[0].Rows[0]["Taluk"] = cmbHub.SelectedIndex > 0 ? cmbHub.SelectedValue.ToString() : "";
            //ds.Tables[0].Rows[0]["Slum"] = cmbslum.SelectedIndex > 0 ? cmbslum.SelectedValue.ToString() : "";
            //ds.WriteXml(xml_path, XmlWriteMode.WriteSchema);
            //}
            // catch (Exception ex)
            //{
            //    Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            //} 
        }

        private void MasterDataManagement_Click(object sender, EventArgs e)
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                if (OpenFormsFound())
                    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
                else
                {
                    Master_Pages.MasterDataManager objMasterData = new Master_Pages.MasterDataManager();
                    objMasterData.TopLevel = false;
                    objMasterData.MdiParent = this;
                    objMasterData.Parent = scMain.Panel1;
                    objMasterData.WindowState = FormWindowState.Maximized;
                    objMasterData.Show();
                }

            }
            else
            {
                MessageBox.Show("Please check your internet connection");

            }
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                btnSearch_Click(sender, e);
        }

        private void txtmemberid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(sender, e);
        }

        private void txtbioid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(sender, e);
        }
        private void UserManagement_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Master_Pages.frmUserManager objUser = new Master_Pages.frmUserManager();
                objUser.TopLevel = false;
                objUser.MdiParent = this;
                objUser.Parent = scMain.Panel1;
                objUser.WindowState = FormWindowState.Maximized;
                objUser.Show();
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void savingAccountReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                SavingAccountForm formreport = new SavingAccountForm();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }

        }

        private void savingAccountTransactionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                SavingAccountTransaction formreport = new SavingAccountTransaction();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void savingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (OpenFormsFound())
            //    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            //else
            //{
            //    DirectSaving formsaving = new DirectSaving();
            //    formsaving.TopLevel = false;
            //    formsaving.MdiParent = this;
            //    formsaving.Parent = scMain.Panel1;
            //    formsaving.WindowState = FormWindowState.Maximized;
            //    //formreport.intMemberOperationType = 1;
            //    formsaving.Show();
            //}
        }

        private void withdrawalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (OpenFormsFound())
            //    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            //else
            //{
            //    DirectWithdrawal formwithdrawal = new DirectWithdrawal();
            //    formwithdrawal.TopLevel = false;
            //    formwithdrawal.MdiParent = this;
            //    formwithdrawal.Parent = scMain.Panel1;
            //    formwithdrawal.WindowState = FormWindowState.Maximized;
            //    //formreport.intMemberOperationType = 1;
            //    formwithdrawal.Show();
            //}
        }

        private void memberReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else
            {
                MemberReport formreport = new MemberReport(GlobalValues.Member_PkId);
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void btnMemberReport_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else
            {
                MemberReport formreport = new MemberReport(GlobalValues.Member_PkId);
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void unpaidEMIReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                UnpaidEMIReport formreport = new UnpaidEMIReport();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void upcomingEMIReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                UpcomingEMIReport formreport = new UpcomingEMIReport();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                NonActiveSavingsAccountReport formreport = new NonActiveSavingsAccountReport();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void multipleSavingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (OpenFormsFound())
            //    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            //else
            //{
            //    Multiple_Saving formsaving = new Multiple_Saving();
            //    formsaving.TopLevel = false;
            //    formsaving.MdiParent = this;
            //    formsaving.Parent = scMain.Panel1;
            //    formsaving.WindowState = FormWindowState.Maximized;
            //    //formsaving.intMemberOperationType = 1;
            //    formsaving.Show();
            //}
        }

        private void createUpdateFundersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Master_Pages.FundersForm objfrmFunders = new Master_Pages.FundersForm();
                objfrmFunders.TopLevel = false;
                objfrmFunders.MdiParent = this;
                objfrmFunders.Parent = scMain.Panel1;
                objfrmFunders.WindowState = FormWindowState.Maximized;
                objfrmFunders.Show();
            }
        }

        private void funderTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Master_Pages.FundsDetails objfrmFunders = new Master_Pages.FundsDetails();
                objfrmFunders.TopLevel = false;
                objfrmFunders.MdiParent = this;
                objfrmFunders.Parent = scMain.Panel1;
                objfrmFunders.WindowState = FormWindowState.Maximized;
                objfrmFunders.Show();
            }
        }

        private void funderAllocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Master_Pages.FunderAllocation objfrmFunders = new Master_Pages.FunderAllocation();
                objfrmFunders.TopLevel = false;
                objfrmFunders.MdiParent = this;
                objfrmFunders.Parent = scMain.Panel1;
                objfrmFunders.WindowState = FormWindowState.Maximized;
                objfrmFunders.Show();
            }
        }

        private void fundersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dayReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                DayReportForm formreport = new DayReportForm();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void createMemberFamilyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                CreateMemberFamily frmMemberFamily = new CreateMemberFamily();
                frmMemberFamily.TopLevel = false;
                frmMemberFamily.MdiParent = this;
                frmMemberFamily.Parent = scMain.Panel1;
                frmMemberFamily.WindowState = FormWindowState.Maximized;
                // frmMemberFamily.intMemberOperationType = 1;
                frmMemberFamily.Show();

            }
        }

        private void searchMemberFamilyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                SearchMemberFamily formSearchMemberFamily = new SearchMemberFamily();
                formSearchMemberFamily.TopLevel = false;
                formSearchMemberFamily.MdiParent = this;
                formSearchMemberFamily.Parent = scMain.Panel1;
                formSearchMemberFamily.WindowState = FormWindowState.Maximized;
                // frmMemberFamily.intMemberOperationType = 1;
                formSearchMemberFamily.Show();
            }
        }

        private void deviceManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                DeviceManagement objMasterData = new DeviceManagement();
                objMasterData.TopLevel = false;
                objMasterData.MdiParent = this;
                objMasterData.Parent = scMain.Panel1;
                objMasterData.WindowState = FormWindowState.Maximized;
                objMasterData.Show();
            }
        }

        private void multipleSavingsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MultipleSavingsTransLoad(Constants.CREDITTRANSABBREV);
            
        }

        private void multipleWithdrawalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MultipleSavingsTransLoad(Constants.DEBITTRANSABBREV);
          
        }

        private void MultipleSavingsTransLoad(string TransType)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Multiple_Saving formsaving = new Multiple_Saving();
                formsaving.TopLevel = false;
                formsaving.MdiParent = this;
                formsaving.Parent = scMain.Panel1;
                formsaving.WindowState = FormWindowState.Maximized;
                formsaving.TransType = TransType;
                formsaving.Show();
            }
        }

        private void multipleLoanEMIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MultipleLoanTransLoad(Constants.CREDITTRANSABBREV);
        }

        private void MultipleLoanTransLoad(string TransType)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                MultipleInstallment MultipleLoanTrans = new MultipleInstallment();
                MultipleLoanTrans.TopLevel = false;
                MultipleLoanTrans.MdiParent = this;
                MultipleLoanTrans.Parent = scMain.Panel1;
                MultipleLoanTrans.WindowState = FormWindowState.Maximized;
                MultipleLoanTrans.TransType = TransType;
                MultipleLoanTrans.Show();
            }
        }

        private void masterDataSettlementWiseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                DayReportSlumWise slumwiseformreport = new DayReportSlumWise();
                slumwiseformreport.TopLevel = false;
                slumwiseformreport.MdiParent = this;
                slumwiseformreport.Parent = scMain.Panel1;
                slumwiseformreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                slumwiseformreport.Show();
            }
        }

        private void memberTransactionReportUpToCurrentDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                MemberTransactionReportUptoCurrentDate memberrepuptocurdate = new MemberTransactionReportUptoCurrentDate();
                memberrepuptocurdate.TopLevel = false;
                memberrepuptocurdate.MdiParent = this;
                memberrepuptocurdate.Parent = scMain.Panel1;
                memberrepuptocurdate.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                memberrepuptocurdate.Show();
            }
        }

        private void scMain_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void savingsAndLoanSumParticularDayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                SettlementwiseTotalCollection memberrepuptocurdate = new SettlementwiseTotalCollection();
                memberrepuptocurdate.TopLevel = false;
                memberrepuptocurdate.MdiParent = this;
                memberrepuptocurdate.Parent = scMain.Panel1;
                memberrepuptocurdate.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                memberrepuptocurdate.Show();
            }
        }

        private void dgSavLoanDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dailyAccountBalanceSheetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                DailyAccountsEntry frmDailyAccount = new DailyAccountsEntry();
                frmDailyAccount.TopLevel = false;
                frmDailyAccount.MdiParent = this;
                frmDailyAccount.Parent = scMain.Panel1;
                frmDailyAccount.WindowState = FormWindowState.Maximized;
                //frmDailyAccount.intMemberOperationType = 1;
                frmDailyAccount.Show();
            }
        }

        private void createGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                GroupInfo frmGroupInfo = new GroupInfo();
                frmGroupInfo.TopLevel = false;
                frmGroupInfo.MdiParent = this;
                frmGroupInfo.Parent = scMain.Panel1;
                frmGroupInfo.WindowState = FormWindowState.Maximized;
                //frmDailyAccount.intMemberOperationType = 1;
                frmGroupInfo.Show();
            }
        }

        private void dataSyncToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (OpenFormsFound())
            //    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            //else
            //{
            //    DatabaseSync syncfrm = new DatabaseSync();
            //    syncfrm.TopLevel = false;
            //    syncfrm.MdiParent = this;
            //    syncfrm.Parent = scMain.Panel1;
            //    syncfrm.WindowState = FormWindowState.Maximized;

            //    syncfrm.Show();
            //}    
        }

        private void groupTransactionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                GroupTransaction grptrans = new GroupTransaction();
                grptrans.TopLevel = false;
                grptrans.TopLevel = false;
                grptrans.MdiParent = this;
                grptrans.Parent = scMain.Panel1;
                grptrans.WindowState=FormWindowState.Maximized;
                grptrans.Show();

            }
        }

        private void groupTransactionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {  if (OpenFormsFound()) 
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning); 
            else 
            {

                GroupTransReport grprep = new GroupTransReport();
                grprep.TopLevel = false;
                grprep.TopLevel = false;
                grprep.MdiParent = this;
                grprep.Parent = scMain.Panel1;
                grprep.WindowState = FormWindowState.Maximized;
                grprep.Show();
              }
        

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Newform grprep = new Newform();
            grprep.TopLevel = false;
            grprep.TopLevel = false;
            grprep.MdiParent = this;
            grprep.Parent = scMain.Panel1;
            grprep.WindowState = FormWindowState.Maximized;
            grprep.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                frmCreateMember frmCreateMember = new frmCreateMember();
                frmCreateMember.TopLevel = false;
                frmCreateMember.MdiParent = this;
                frmCreateMember.Parent = scMain.Panel1;
                frmCreateMember.WindowState = FormWindowState.Maximized;
                frmCreateMember.intMemberOperationType = 1;
                frmCreateMember.Show();
            }
        }

        private void groupInterestPayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                GroupUPFpayout frmgrpupf = new GroupUPFpayout();
                frmgrpupf.TopLevel = false;
                frmgrpupf.MdiParent = this;
                frmgrpupf.Parent = scMain.Panel1;
                frmgrpupf.WindowState = FormWindowState.Maximized;
                frmgrpupf.Show();
            }

        }

        private void createGroupUPFAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                CreateGroupUPF frmgrpupfacc = new CreateGroupUPF();
                frmgrpupfacc.TopLevel = false;
                frmgrpupfacc.MdiParent = this;
                frmgrpupfacc.Parent = scMain.Panel1;
                frmgrpupfacc.WindowState = FormWindowState.Maximized;
                frmgrpupfacc.Show();
            }
        }

        private void fundAllocationReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {

                Funds_Allocation_Report Far = new Funds_Allocation_Report();
                Far.TopLevel = false;
                Far.MdiParent = this;
                Far.Parent = scMain.Panel1;
                Far.WindowState = FormWindowState.Maximized;
                Far.Show();
            }

        }

        private void dailyCenterAccountBalanceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {

                DailyTotalAmount fdr = new DailyTotalAmount();
                fdr.TopLevel = false;
                fdr.MdiParent = this;
                fdr.Parent = scMain.Panel1;
                fdr.WindowState = FormWindowState.Maximized;
                fdr.Show();
            }
        }

        private void notificationToolStripMenuItem_Click(object sender, EventArgs e)

        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {

                NotificationForm1 nt = new NotificationForm1();
                nt.TopLevel = false;
                nt.MdiParent = this;
                nt.Parent = scMain.Panel1;
                nt.WindowState = FormWindowState.Maximized;
                nt.Show();
            }

        }

        private void groupUPFTransactionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {

                GroupUPFAccountTransactionsReport grupf = new GroupUPFAccountTransactionsReport();
            
                grupf.TopLevel = false;
                grupf.MdiParent = this;
                grupf.Parent = scMain.Panel1;
                grupf.WindowState = FormWindowState.Maximized;
                grupf.Show();
            }
        }

        private void scannedDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else
            {
                ScannedDocuments sd = new ScannedDocuments();
                sd.TopLevel = false;
                sd.MdiParent = this;
                sd.Parent = scMain.Panel1;
                sd.WindowState = FormWindowState.Maximized;
                sd.Show();

            } 
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Master_Pages.frmUserManager objUser = new Master_Pages.frmUserManager();
                objUser.TopLevel = false;
                objUser.MdiParent = this;
                objUser.Parent = scMain.Panel1;
                objUser.WindowState = FormWindowState.Maximized;
                objUser.Show();
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void membersReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                Memrepfrm mr = new Memrepfrm();
                mr.TopLevel = false;
                mr.MdiParent = this;
                mr.Parent = scMain.Panel1;
                mr.WindowState = FormWindowState.Maximized;
                mr.Show();
            }
        }

        private void UPF_Click(object sender, EventArgs e)
        {

        }

        private void moToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                MonthwiseReport mr = new MonthwiseReport();
                mr.TopLevel = false;
                mr.MdiParent = this;
                mr.Parent = scMain.Panel1;
                mr.WindowState = FormWindowState.Maximized;
                mr.Show();
            }
        }

        private void dataTableSyncToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //    if (OpenFormsFound())
            //    Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            //else
            //{
            //    TabeleDataSnc mr = new TabeleDataSnc();
            //    mr.TopLevel = false;
            //    mr.MdiParent = this;
            //    mr.Parent = scMain.Panel1;
            //    mr.WindowState = FormWindowState.Maximized;
            //    mr.Show();
            //}
        }

        private void savingAccountReportNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                SavingAccountForm formreport = new SavingAccountForm();
                formreport.TopLevel = false;
                formreport.MdiParent = this;
                formreport.Parent = scMain.Panel1;
                formreport.WindowState = FormWindowState.Maximized;
                //formreport.intMemberOperationType = 1;
                formreport.Show();
            }
        }

        private void Reports_Click(object sender, EventArgs e)
        {

        }

        private void Admin_Click(object sender, EventArgs e)
        {

        }

        private void dataSyncToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                DatabaseSync syncfrm = new DatabaseSync();
                syncfrm.TopLevel = false;
                syncfrm.MdiParent = this;
                syncfrm.Parent = scMain.Panel1;
                syncfrm.WindowState = FormWindowState.Maximized;

                syncfrm.Show();
            }    
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            Help.ShowHelp(this, helpProvider1.HelpNamespace, HelpNavigator.TableOfContents);
        }
        
        
        
    }
}
