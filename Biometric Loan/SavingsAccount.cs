﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class frmSavingsAccount : Form
    {
        private int iAction;
        public frmSavingsAccount(int iActionvalue)
        {
            InitializeComponent();
            iAction = iActionvalue;
        }
        public string TransType = string.Empty;
        private double RemainingPrincipal = 0;
        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                GlobalValues.Savings_PkId = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    SavingAccount objSBAcc = new SavingAccount();

                    if (iAction == 1)
                        objSBAcc.Id = 0;
                    else
                        objSBAcc.Id = GlobalValues.Savings_PkId;

                    objSBAcc.Remarks = txtRemarks.Text.Trim();
                    objSBAcc.fk_MemberId = GlobalValues.Member_PkId;
                    objSBAcc.Createdby = GlobalValues.User_PkId;
                    objSBAcc.Updatedby = GlobalValues.User_PkId;
                    objSBAcc.Status = cmbStatus.Text;
                    objSBAcc.OpenedBy =Convert.ToInt32(cmbAgentName.SelectedValue);
                    objSBAcc.Opendate = dtpOpenDate.Value;
                    objSBAcc.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
                    objSBAcc.PassbookNo = txtSavingPbNo.Text.Trim();

                    int saveResult = objSBAcc.SaveSBAcc();

                    if (saveResult > 0)
                    {
                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();

                        if (objSBAcc.Id == 0)
                        {
                            lblMessages.Text = Messages.SAVED_SUCCESS + Messages.SAVED_CURRENTSBACC;
                            lblMessages.ForeColor = Color.Green;
                            
                            GlobalValues.Savings_PkId = saveResult;

                            iAction = 2; //Change Mode to update
                            btnSave.Text = Constants.UPDATE;

                            this.Text = Constants.UPDATESAVINGSACC + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                            lblAccValue.Text = GlobalValues.Savings_AccNumber;
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        public void LoadSavingsDepTrans()
        {
            //Loads the grid
            SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();
            objSavingsDepTrans.fk_MemberId = GlobalValues.Member_PkId;
            objSavingsDepTrans.Tran_type = TransType;
            objSavingsDepTrans.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
            objSavingsDepTrans.LoanType = Convert.ToInt64(cmbSavingType.SelectedValue);
            DataSet dsDepositTrans = objSavingsDepTrans.GetSavingsDepositTransactions();
            if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[2].Rows.Count > 0)
            {
                RemainingPrincipal = Convert.ToDouble(dsDepositTrans.Tables[2].Rows[0][0].ToString());
            }
        }
        private void LoadControls(SavingAccount objSavingsAccFetch)
        {
            DataSet dsSBAccFetch = objSavingsAccFetch.LoadSBAccById();
            if (dsSBAccFetch != null && dsSBAccFetch.Tables.Count > 0 && dsSBAccFetch.Tables[0].Rows.Count > 0 && iAction == 2)
            {
                GlobalValues.Savings_PkId = Convert.ToInt64(dsSBAccFetch.Tables[0].Rows[0]["Id"]);     
                lblMemberValue.Text = dsSBAccFetch.Tables[0].Rows[0]["MemberId"].ToString();
                lblAccValue.Text = dsSBAccFetch.Tables[0].Rows[0]["AccountNumber"].ToString();
                cmbStatus.Text = dsSBAccFetch.Tables[0].Rows[0]["Status"].ToString();
                lblCurrBalanceValue.Text = dsSBAccFetch.Tables[0].Rows[0]["CurrentBalance"].ToString();
                cmbAgentName.SelectedValue = Convert.ToInt16(dsSBAccFetch.Tables[0].Rows[0]["OpenedBy"]);
                txtSavingPbNo.Text = dsSBAccFetch.Tables[0].Rows[0]["passbookno"].ToString();
                dtpOpenDate.Text = Convert.ToDateTime(dsSBAccFetch.Tables[0].Rows[0]["Opendate"]).ToString();
                txtRemarks.Text = dsSBAccFetch.Tables[0].Rows[0]["Remarks"].ToString();
            }
            else if(iAction == 2)
            {
                MessageBox.Show("The selected Account type does not exist.");
                this.SavingsAccount_Load(null, null);
            }
            if (dsSBAccFetch != null && dsSBAccFetch.Tables.Count > 0 && dsSBAccFetch.Tables[0].Rows.Count > 0 && iAction == 1)
            {
                MessageBox.Show("The selected Saving Account already exists");
                this.SavingsAccount_Load(null, null);
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;

            if(cmbSavingType.SelectedIndex == 0)
                ErrorMsg = Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;

            if (txtSavingPbNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.PASSBOOKNO + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                if (cmbStatus.Text ==  Status.CLOSED && Convert.ToDouble(lblCurrBalanceValue.Text) > 0)
                {
                    ErrorMsg = Messages.ACCNILBALANCE;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            return ErrorMsg;
        }

        private void SavingsAccount_Load(object sender, EventArgs e)
        {
            try
            {
                if (iAction == 1)
                {
                    FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbSavingType); //Fill Savingtype                
                }
                else if (iAction == 2)
                {
                    RetrieveSavingsName();  
                }
                dtpOpenDate.MaxDate = DateTime.Now;
                lblMemberValue.Text = GlobalValues.Member_ID;
                cmbStatus.SelectedIndex = 0;
                FillAgents(true);
                lblMessages.Text = string.Empty;
                lblRemarks.Text = string.Empty;
                //if (GlobalValues.Savings_PkId <= 0) //Create
                //{
                //    iAction = 1;
                //    cmbStatus.Enabled = false;
                //    this.Text = Constants.CREATESAVINGSACC + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                //}
                //else
                //{
                //    iAction = 2;
                //    this.Text = Constants.UPDATESAVINGSACC + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

                //    //SavingAccount objSavingsAccFetch = new SavingAccount();
                //    //objSavingsAccFetch.Id = GlobalValues.Savings_PkId;
                //    //LoadControls(objSavingsAccFetch);
                //    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                //    {
                //        btnDelete.Visible = true;
                //    }
                //}

                if (iAction == 1) //Create
                {                    
                    cmbStatus.Enabled = false;
                    this.Text = Constants.CREATESAVINGSACC + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                }

                else if(iAction == 2)
                {                    
                    this.Text = Constants.UPDATESAVINGSACC + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

                    //SavingAccount objSavingsAccFetch = new SavingAccount();
                    //objSavingsAccFetch.Id = GlobalValues.Savings_PkId;
                    //LoadControls(objSavingsAccFetch);
                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                    {
                        btnDelete.Visible = true;
                    }
                }

                btnSave.Focus();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void RetrieveSavingsName()
        {
            CodeValues objSavingsName = new CodeValues();
            objSavingsName.fk_memberId = GlobalValues.Member_PkId;
            DataSet dsSavingname = objSavingsName.GetSavingsNamenew();
            if (dsSavingname != null && dsSavingname.Tables.Count > 0 && dsSavingname.Tables[0].Rows.Count > 0)
            {
                cmbSavingType.DisplayMember = "Name";
                cmbSavingType.ValueMember = "Id";
                cmbSavingType.DataSource = dsSavingname.Tables[0];
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                DeleteData();
            }
        }

        private void DeleteData()
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            SavingAccount objSavingAcc = new SavingAccount();
            objSavingAcc.Id = GlobalValues.Savings_PkId;

            dsDeleteStatus = objSavingAcc.DeleteSavingAccount(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    objBioMetric.FetchMemberDetailsByID();
                    objBioMetric.InsertMemberDetailsToGrid();
                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Red;
                    
                    this.Close();
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_ACCOUNT;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void cmbSavingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt64(cmbSavingType.SelectedValue) > 0)
            {                
                    SavingAccount objSavingsAccFetch = new SavingAccount();
                    objSavingsAccFetch.Id = GlobalValues.Member_PkId;
                    objSavingsAccFetch.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
                    LoadControls(objSavingsAccFetch);                
            }
        }
    }
}
