﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace SPARC
{
    public partial class GroupUPFpayout : Form
    {
        public GroupUPFpayout()
        {
            InitializeComponent();
        }

        private Int32 iUPFID = 0;
        private Int32 iUPFAccID = 0;

        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();
            DataRow dr = dsgroup.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = "--Select One--";

            dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbgrp.DisplayMember = "Group_Name";
                cmbgrp.ValueMember = "Id";
                cmbgrp.DataSource = dsgroup.Tables[0];


            }
            else
            {
                cmbgrp.DataSource = null;
                //cmbGname.SelectedValue="--Select--";

            }
        }

      
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();

            objCodeValues.CodeTypeID = CodeTypeID;
            if (CodeTypeID == 3)
            {
                string queryString = "select Id from CodeValue where fk_CodeTypeId=2";
                string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                SqlConnection con = new SqlConnection(ConnectionString);
                SqlCommand command = new SqlCommand(queryString, con);
                con.Open();
                var result = command.ExecuteScalar();
                objCodeValues.ParentID = Convert.ToInt32(result);
                DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
                if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = -1;

                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                    cmbLocation.DisplayMember = "Name";
                    cmbLocation.ValueMember = "Id";
                    cmbLocation.DataSource = dsCodeValues.Tables[0];
                }
            }
            else
            {
                objCodeValues.ParentID = ParentID;
                DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
                if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = -1;

                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                    cmbLocation.DisplayMember = "Name";
                    cmbLocation.ValueMember = "Id";
                    cmbLocation.DataSource = dsCodeValues.Tables[0];
                }
            }
        }

        private void GroupUPFpayout_Load(object sender, EventArgs e)
        {
            cmbPayMode.SelectedIndex = 0;
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.State), 24, cmbstate);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    //FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    //cmbHub.SelectedValue = Id;
                    //cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    ////check for the state type code and populate state 
                    //FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                    //cmbcountry.SelectedValue = Id;
                    //cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the District type code and populate district
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                    cmbstate.SelectedValue = Id;
                    cmbstate.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the Slum type code and populate Slum
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                    cmbdistrict.SelectedValue = Id;
                    cmbdistrict.Enabled = false;
                }
            }

            txtRemarks.Enabled = false;
            txtIntPayout.Enabled = false;
            txtPrincipalWithdraw.Enabled = false;
           // ClearControls();
            //LoadGroupInterestPayOut();
            FillAgents(true);

        }


        private void cmbstate_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private void cmbslum_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbslum.SelectedIndex > 0)
            {
                FillCombogroup(Convert.ToInt32(cmbslum.SelectedValue));
            }
            else
            {
                cmbgrp.DataSource = null;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbgrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbgrp.SelectedIndex > 0)
            {
                decimal Amount;
                int Days;
                GroupUPF objupf = new GroupUPF();
               

                objupf.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
                LoadGroupInterestPayOut();
                decimal InterestRate = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["UPF_Interest"]);
                decimal simpleInterest = 0;
                // cmbgrp.SelectedValue = Nameofthegroup.Text;
                DataSet dsUPF = objupf.LoadAllUPFTrans();
                if (dsUPF.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("No Transaction for this group");
                }
                else
                {
                    foreach (DataRow dr in dsUPF.Tables[0].Rows)
                    {
                        if (Convert.ToDateTime(dr[3]) > Convert.ToDateTime(dr[7]))
                        {
                            // Calculate interest when no initial interest is done
                            if (dr[9].ToString() == "CRE")
                            {
                                Amount = Convert.ToDecimal(dr[6]);
                                Days = Convert.ToInt32((Convert.ToDateTime(dr[3]) - Convert.ToDateTime(dr[7])).TotalDays);
                                simpleInterest = Math.Round(simpleInterest + CalculateInterest(Amount, Days, InterestRate), 2);
                            }

                            if (dr[9].ToString() == "DEB" && dr[10].ToString() == "PRINCIPAL PAYOUT")
                            {
                                Amount = Convert.ToDecimal(dr[6]);
                                Days = Convert.ToInt32((Convert.ToDateTime(dr[3]) - Convert.ToDateTime(dr[7])).TotalDays);
                                simpleInterest = Math.Round(simpleInterest - CalculateInterest(Amount, Days, InterestRate), 2);
                            }

                            if (dr[9].ToString() == "DEB" && dr[10].ToString() == "INT PAYOUT")
                            {
                                Amount = Convert.ToDecimal(dr[6]);
                                simpleInterest = Math.Round((simpleInterest - Amount), 2);
                            }
                        }
                    }
                }
                lblIntToBePaid.Text = simpleInterest.ToString();
                // LoadAllUPFInterestPendings();

                this.Text += Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
            }
            else
            {
                //cmbgrp.DataSource = null;
            }
            }
        private decimal CalculateInterest(decimal Amount, int Days, decimal InterestRate)
        {
            decimal interestAmount;
            interestAmount = (Amount * (1 + (InterestRate / 100) * (Days) / 365)) - Amount;
            return interestAmount;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }



        private void LoadGroupInterestPayOut()
        {
            try
            {
                //Loads the grid
                GroupUPF objupf = new GroupUPF();

                objupf.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
                DataSet dsUPF = objupf.LoadInterestPayOut();




                lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["opendate"]).ToString("dd-MMM-yyyy");
                lblDepAmtVal.Text = dsUPF.Tables[1].Rows[0]["CurrentBalance"].ToString();
              //  lblStatusVal.Text = dsUPF.Tables[1].Rows[0]["Status"].ToString();
                label2.Text = dsUPF.Tables[1].Rows[0]["Status"].ToString();
                lblIntDueVal.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["PayOutEndDate"]).ToString("dd-MMM-yyyy");
                if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[1].Rows.Count > 0)
                {
                    iUPFID = Convert.ToInt32(dsUPF.Tables[1].Rows[0]["GT_ID"]);
                    iUPFAccID = Convert.ToInt32(dsUPF.Tables[1].Rows[0]["Id"]);
                    // lblIntToBePaid.Text = dsUPF.Tables[0].Rows[0]["InterestAmount"].ToString();
                    //  lblCheck.Text = dsUPF.Tables[0].Rows[0]["InterestAsOnDate"].ToString();
                  dgUPFAccount.AutoGenerateColumns = false;
                    
                 //  AdjustColumnOrder();
                    
                    dgUPFAccount.DataSource = dsUPF.Tables[0];


                    for (int i = 0; i < dgUPFAccount.Rows.Count; i++)
                    {
                        if (dgUPFAccount.Rows[i].Cells["Status"].Value.ToString().ToLower().Equals("active"))
                        {
                            dgUPFAccount.Rows[i].Cells["btnDelete"].Value = string.Empty;
                        }
                    }
                }
                else
                {
                    //lblMessages.Text = Messages.NOPENDINGPAYOUTS;
                    //lblMessages.ForeColor = Color.Green;

                    //dgUPFAccount.DataSource = null;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnCalIntAsOnDate_Click(object sender, EventArgs e)
        {
            decimal Amount;
            int Days;
            decimal InterestRate = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["UPF_Interest"]);
            decimal simpleInterest = 0;
            GroupUPF objupf = new GroupUPF();

            objupf.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
            DataSet dsUPF = objupf.LoadAllUPFTrans();
            
           

            foreach (DataRow dr in dsUPF.Tables[0].Rows)
            {
                if (DateTime.Now > Convert.ToDateTime(dr[7]))
                {
                    // Calculate interest when no initial interest is done
                    if (dr[9].ToString() == "CRE")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        Days = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(dr[7])).TotalDays);
                        simpleInterest = Math.Round(simpleInterest + CalculateInterest(Amount, Days, InterestRate), 2);
                    }

                    if (dr[9].ToString() == "DEB" && dr[10].ToString() == "PRINCIPAL PAYOUT")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        Days = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(dr[7])).TotalDays);
                        simpleInterest = Math.Round(simpleInterest - CalculateInterest(Amount, Days, InterestRate), 2);
                    }

                    if (dr[9].ToString() == "DEB" && dr[10].ToString() == "INT PAYOUT")
                    {
                        Amount = Convert.ToDecimal(dr[6]);
                        simpleInterest = Math.Round((simpleInterest - Amount), 2);
                    }
                }
            }
            lblIntAsOnDate.Text = simpleInterest.ToString();
        }

        private void radIntPayout_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void radPricipalAmount_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void radIntPayout_CheckedChanged_1(object sender, EventArgs e)
        {
            txtIntPayout.Enabled = false;
            txtIntPayout.Text = lblIntAsOnDate.Text;
            txtRemarks.Text = "INT PAYOUT";
            //txtRemarks.Enabled = false;
        }

        private void radPricipalAmount_CheckedChanged_1(object sender, EventArgs e)
        {
            txtIntPayout.Text = "";
            txtRemarks.Text = "PRINCIPAL PAYOUT";
            txtPrincipalWithdraw.Enabled = true;
        }
        private void txtIntPayout_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtPrincipalWithdraw_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbgrp.SelectedIndex > 0)
                {
                    string strMessage = DoValidations();
                    if (lblIntDueVal.Text.Trim().Length > 0)
                    {
                        try
                        {
                            if (Convert.ToDateTime(lblIntDueVal.Text + " 00:00:00") > DateTime.Today)
                            {
                                DialogResult dsConfirmation = MessageBox.Show("You are paying before the due date. Are you sure you want to continue to pay?", "SPARC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dsConfirmation == DialogResult.No)
                                    return;
                            }
                        }
                        catch
                        {
                            //MessageBox.Show("Please enter all  the fields to save the data");
                        }
                    }

                   
                    if (strMessage.Length > 0)
                    {
                        lblMessages.Text = strMessage;
                    }
                    else
                    {
                        lblMessages.Text = string.Empty;
                        GroupUPF objUPFAcc = new GroupUPF();
                        objUPFAcc.Id = iUPFID;
                        objUPFAcc.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
                        objUPFAcc.Status = SPARC.Status.ACTIVE;
                        objUPFAcc.Tran_type = Constants.DEBITTRANSABBREV;
                        objUPFAcc.AccId = iUPFAccID;

                        if (radIntPayout.Checked == true)
                            objUPFAcc.Amount = Convert.ToDecimal(txtIntPayout.Text);

                        if (radPricipalAmount.Checked == true)
                            objUPFAcc.Amount = Convert.ToDecimal(txtPrincipalWithdraw.Text);

                        objUPFAcc.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                        objUPFAcc.Opendate = dtpTransDate.Value;
                        objUPFAcc.Remarks = txtRemarks.Text;
                        objUPFAcc.PayOutEndDate = Convert.ToDateTime(lblIntDueVal.Text);
                        objUPFAcc.Createdby = GlobalValues.User_PkId;
                        objUPFAcc.Updatedby = GlobalValues.User_PkId;
                        objUPFAcc.Pay_Mode = cmbPayMode.Text;

                        DataSet dsUPFAcc = null;
                        dsUPFAcc  =objUPFAcc.UpdateUPFAccount(1);
                      

                        if (dsUPFAcc != null && dsUPFAcc.Tables.Count > 0 && dsUPFAcc.Tables[0].Rows.Count > 0)
                        {
                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;

                            //frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                            //objBioMetric.FetchMemberDetailsByID();
                            //objBioMetric.InsertMemberDetailsToGrid();
                        }
                        else
                        {
                            lblMessages.Text = Messages.ERROR_PROCESSING;
                            lblMessages.ForeColor = Color.Red;
                        }
                        LoadInterestPayOut();
                       // LoadGroupInterestPayOut();
                     //  LoadAllUPFInterestPendings();
                       ClearControls();
                                                        
                    }
                }
                else
                {
                    lblMessages.Text = Messages.SelectGroup;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        
        }
        private void ClearControls()
        {
          
         //   lblAccValue.Text = GlobalValues.UPF_AccNumber;
            ////   lblDepAmtVal.Text = string.Empty;
            //  txtRemarks.Text = string.Empty;
            //  lblIntDueVal.Text = string.Empty;
            //lblIntAsOnDate.Text = string.Empty;
            //lblAccValue.Text = string.Empty;
            //lblIntToBePaid.Text = string.Empty;
            txtPrincipalWithdraw.Text = string.Empty;
            txtIntPayout.Text = string.Empty;
           // lblStatusVal.Text = "";
            label2.Text = "";
            cmbPayMode.SelectedIndex = 0;
            cmbAgentName.SelectedIndex = 0;
            dtpTransDate.Value = DateTime.Today;

            //btnPay.Enabled = false;
            cmbAgentName.Enabled = true;

            //lblMessages.Text = string.Empty;
            //lblMessages.ForeColor = Color.Black;
        }
        private string DoValidations()
        
        {
            string ErrorMsg = string.Empty;

            if (cmbPayMode.Text == PayMode.CASH && cmbAgentName.SelectedIndex == 0)
            {
                ErrorMsg = Messages.PAYTHRUAGENTNAME + Messages.COMMAWITHSPACE;
            }
            //if (cmbAgentName.Text == "--Select One--") 
            //    ErrorMsg = Messages.PAYTHRUAGENTNAME + Messages.COMMAWITHSPACE;
       // if  ( cmbAgentName.SelectedIndex == 0)
           //   ErrorMsg = Messages.COMMAWITHSPACE;
            if (radIntPayout.Checked == true || radPricipalAmount.Checked == true)
            {

            }
            else
            {
                ErrorMsg += Messages.SELECTRADIOBUTTON + Messages.COMMAWITHSPACE;
            }

            if (radIntPayout.Checked == true)
            {
                if ((txtIntPayout.Text.Trim().Length == 0) || Convert.ToDouble(txtIntPayout.Text) != Convert.ToDouble(lblIntAsOnDate.Text))
                    ErrorMsg += Messages.INTERESTAMOUNT + Messages.COMMAWITHSPACE;
            }
            if (radPricipalAmount.Checked == true)
            {
                if (Convert.ToDouble(txtPrincipalWithdraw.Text) > Convert.ToDouble(lblDepAmtVal.Text))
                    ErrorMsg += Messages.PRINCIPALAMOUNTTXT + Messages.COMMAWITHSPACE;
            }

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void LoadInterestPayOut()
        {
            //Loads the grid
            GroupUPF objupf = new GroupUPF();

            objupf.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
      
            DataSet dsUPF = objupf.LoadInterestPayOut();
            lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["opendate"]).ToString("dd-MMM-yyyy");
            lblDepAmtVal.Text = dsUPF.Tables[1].Rows[0]["CurrentBalance"].ToString();
           
            label2.Text = dsUPF.Tables[1].Rows[0]["Status"].ToString();
            lblIntDueVal.Text = Convert.ToDateTime(dsUPF.Tables[1].Rows[0]["PayOutEndDate"]).ToString("dd-MMM-yyyy");
            if (cmbgrp.SelectedIndex > 0)
            {
               // label3.Text = dsUPF.Tables[1].Rows[0]["GroupName"].ToString();
            }
            if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[1].Rows.Count > 0)
            {
                iUPFID = Convert.ToInt32(dsUPF.Tables[1].Rows[0]["GT_ID"]);
                iUPFAccID = Convert.ToInt32(dsUPF.Tables[1].Rows[0]["Id"]);

                // lblIntToBePaid.Text = dsUPF.Tables[0].Rows[0]["InterestAmount"].ToString();
                //  lblCheck.Text = dsUPF.Tables[0].Rows[0]["InterestAsOnDate"].ToString();
                dgUPFAccount.AutoGenerateColumns = false;
                dgUPFAccount.DataSource = dsUPF.Tables[0];
               // AdjustColumnOrder();

                for (int i = 0; i < dgUPFAccount.Rows.Count; i++)
                {
                    if (dgUPFAccount.Rows[i].Cells["Status"].Value.ToString().ToLower().Equals("active"))
                    {
                        dgUPFAccount.Rows[i].Cells["btnDelete"].Value = string.Empty;
                    }
                }
            }
            else
            {
                lblMessages.Text = Messages.NOPENDINGPAYOUTS;
                lblMessages.ForeColor = Color.Green;

                dgUPFAccount.DataSource = null;
            }
        }

        private void dgUPFAccount_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }
        private void DeleteData(Int32 DataId,Int64 TransId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            GroupUPF objupf = new GroupUPF();
            objupf.Id = DataId;

            dsDeleteStatus = objupf.ReOpenUPFAccount(GlobalValues.User_PkId,TransId);
            //if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            //{
            //    iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
            //    if (iResult == 1)
            //    {
                    LoadInterestPayOut();
                    //LoadAllUPFInterestPendings();
                   // ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;

                    //frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    //objBioMetric.FetchMemberDetailsByID();
                    //objBioMetric.InsertMemberDetailsToGrid();
                //}
                //else
                //{
                //    lblMessages.Text = Messages.ERROR_PROCESSING;
                //    lblMessages.ForeColor = Color.Red;
                //}
            //}
            //else
            //{
            //    lblMessages.Text = Messages.ERROR_PROCESSING;
            //    lblMessages.ForeColor = Color.Red;
            //}
        }

        private void cmbPayMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbPayMode.Text == PayMode.BANK)
                {
                    cmbAgentName.SelectedIndex = 0;
                    cmbAgentName.Enabled = false;
                }
                else
                {
                    cmbAgentName.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void AdjustColumnOrder()
        {
            dgUPFAccount.Columns["AccountNumber"].DisplayIndex = 0;
            dgUPFAccount.Columns["GroupName"].DisplayIndex = 1;
          //  dgUPFAccount.Columns["Opendate"].DisplayIndex = 2;
            dgUPFAccount.Columns["CurrentBalance"].DisplayIndex = 3;
            dgUPFAccount.Columns["Amount"].DisplayIndex = 4;
            dgUPFAccount.Columns["TransactionDate"].DisplayIndex = 5;
            dgUPFAccount.Columns["Remarks"].DisplayIndex = 6;
            dgUPFAccount.Columns["Status"].DisplayIndex = 7;
            dgUPFAccount.Columns["Delete"].DisplayIndex = 8;
        }

        private void dgUPFAccount_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgUPFAccount.Columns[e.ColumnIndex].Name == "btnDelete")
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[0].Value), Convert.ToInt64(dgUPFAccount.CurrentRow.Cells[11].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);

            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }

    
}
