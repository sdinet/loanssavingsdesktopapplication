﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;
using System.Configuration;
using System.Data.SqlClient;
namespace SPARC
{
    public partial class frmUPFAccount : Form
    {
        private Int32 iSavDepID = 0;
        private Int32 UPFTransno = 0;

        public frmUPFAccount()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        public string type;
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {

                    if (type == "DEB")
                    {
                        MessageBox.Show("DEB transactions cannot be updated!!Instead can be deleted and reinserted.");

                    }
                    else
                    {
                        Int32 UPFAccId;
                        lblMessages.Text = string.Empty;

                        UPFAccount objUPFAcc = new UPFAccount();
                        objUPFAcc.Id = iSavDepID;
                        objUPFAcc.fk_MemberId = GlobalValues.Member_PkId;
                        objUPFAcc.Status = "Active"; //lblStatusVal.Text;
                        objUPFAcc.Tran_type = Constants.CREDITTRANSABBREV;
                        objUPFAcc.Amount = Convert.ToDecimal(txtAmount.Text);
                        objUPFAcc.InterestPercent = Convert.ToDouble(ConfigurationSettings.AppSettings["UPF_Interest"].ToString());
                        objUPFAcc.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                        objUPFAcc.Opendate = dtpTransDate.Value;
                        objUPFAcc.Remarks = txtRemarks.Text;
                        //objUPFAcc.PayOutEndDate = dtpIntPay.Value;
                        objUPFAcc.PayOutEndDate = dtpTransDate.Value.AddYears(1);
                        objUPFAcc.Createdby = GlobalValues.User_PkId;
                        objUPFAcc.Updatedby = GlobalValues.User_PkId;
                        objUPFAcc.AccId = UPFTransno;
                        
                        DataSet dsUPFAcc = null;
                        if (iSavDepID == 0)
                        {
                            SqlConnection conn = new SqlConnection();
                            conn.ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

                            conn.Open();


                            string query = "SELECT TOP 1 Id FROM UPFAccount WHERE UPFAccount.fk_MemberId = @fk_MemberId";

                            SqlCommand cmd = new SqlCommand(query, conn);
                            cmd.Parameters.Add(new SqlParameter("@fk_MemberId", GlobalValues.Member_PkId));
                            Int32 UpfAccIdd;
                            UpfAccIdd = Convert.ToInt32(cmd.ExecuteScalar());
                            if (UpfAccIdd == 0)
                            {
                                UPFAccId = objUPFAcc.SaveUPFAccount();
                                dsUPFAcc = objUPFAcc.SaveUPFAccounttrans(UPFAccId);
                            }
                            else
                            {
                                dsUPFAcc = objUPFAcc.SaveUPFAccounttrans(UpfAccIdd);
                                ClearControls();
                                btnSave.Text = "Save"; 
                            }
                        }
                        else
                        {
                     objUPFAcc.UpdateUPFAccountnew();
                           dsUPFAcc = objUPFAcc.UpdateUPFAccount(0);
                           lblStatus.Visible = false;
                           lblStatusVal.Visible = false;
                           lblStatusVal.Text = "";
                           txtAmount.Text = string.Empty;
                           cmbAgentName.SelectedIndex = 0;

                           txtRemarks.Text = string.Empty;
                           dtpTransDate.Value = DateTime.Today;
                           //lblAccValue.Text = string.Empty;

                           lblMessages.Text = string.Empty;
                           lblMessages.ForeColor = Color.Black;

                           //dtpIntPay.Value = dtpTransDate.Value.AddYears(1);
                           btnSave.Enabled = true;
                           btnSave.Text = "Save";
                         

                        }
                        if (dsUPFAcc != null && dsUPFAcc.Tables.Count > 0 && dsUPFAcc.Tables[0].Rows.Count > 0)
                        {
                            if (iSavDepID == 0)
                            {
                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;

                                btnSave.Text = Constants.UPDATE;
                              //  UPFTransno = Convert.ToInt32(dsUPFAcc.Tables[0].Rows[0]["TransId"]);
                               // iSavDepID = Convert.ToInt32(dsUPFAcc.Tables[1].Rows[0]["Id"]);
                              //  GlobalValues.UPF_AccNumber = dsUPFAcc.Tables[1].Rows[0]["AccountNumber"].ToString();
                            }
                            else
                            {
                                lblMessages.Text = Messages.UPDATED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                        }
                        else
                        {
                            Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                        }

                        LoadAllUPFAccounts();
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void LoadAllUPFAccounts()
        {
            //Loads the grid
            SPARC.UPFAccount objUPFAccounts = new SPARC.UPFAccount   ();
            objUPFAccounts.fk_MemberId = GlobalValues.Member_PkId;
            lblAccValue.Text = GlobalValues.UPF_AccNumber;
            DataSet dsUPF = objUPFAccounts.LoadAllUPFAccs();
            if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[0].Rows.Count > 0)
            {
                lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[0].Rows[0]["UPFAccOpenDate"]).ToString("dd-MMM-yyyy");
                lblCurrBalValue.Text = dsUPF.Tables[0].Rows[0]["CurrentBalance"].ToString();
                dgUPFAccount.AutoGenerateColumns = false;
                dgUPFAccount.DataSource = dsUPF.Tables[0];
            }
            else
            {
                dgUPFAccount.DataSource = null;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void UPFAccount_Load(object sender, EventArgs e)
        {
            try
            {
                dtpTransDate.MaxDate = DateTime.Now;
                this.Text += Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

                lblMemberValue.Text = GlobalValues.Member_ID;
                lblAccValue.Text = GlobalValues.UPF_AccNumber;
                FillAgents(true);
                ClearControls();

                LoadAllUPFAccounts();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }      

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;

            if (txtAmount.Text.Trim() == string.Empty || Convert.ToDouble(txtAmount.Text) <= 0)
            {
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            }
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
          //  else
           // {
             //   if (dtpIntPay.Value <= dtpTransDate.Value)
               // {
                //    ErrorMsg = Messages.INTERESTPAYDATECHK;
                 //   lblMessages.ForeColor = Color.Red;
                //}
            //}
            return ErrorMsg;
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void ClearControls()
        {
            iSavDepID = 0;
            lblStatus.Visible = false;
            lblStatusVal.Visible = false;
            lblStatusVal.Text = "";
            txtAmount.Text = string.Empty;
            cmbAgentName.SelectedIndex = 0;

            txtRemarks.Text = string.Empty;
            dtpTransDate.Value = DateTime.Today;
            //lblAccValue.Text = string.Empty;

            lblMessages.Text = string.Empty;
            lblMessages.ForeColor = Color.Black;

            //dtpIntPay.Value = dtpTransDate.Value.AddYears(1);
            btnSave.Enabled = true;
            btnSave.Text = "Save";
            
        }

        private void dtpTransDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
               // dtpIntPay.Value = dtpTransDate.Value.AddYears(1);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgUPFAccount_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ClearControls();
                lblStatusVal.Visible = true;
                lblStatus.Visible = true;
                if (e.RowIndex >= 0)
                {
                    cmbAgentName.SelectedValue = dgUPFAccount.Rows[e.RowIndex].Cells["AgentID"].Value;
                    dtpTransDate.Value = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["TransactionDate"].Value);
                    txtAmount.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                    txtRemarks.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                    //dtpIntPay.Value = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["PayOutEndDate"].Value);
                    lblAccValue.Text = dgUPFAccount.Rows[e.RowIndex].Cells["AccountNumber"].Value.ToString();
                    lblStatusVal.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Status"].Value.ToString();
                    type = dgUPFAccount.Rows[e.RowIndex].Cells["TransType"].Value.ToString();
                   
                    btnSave.Text = Constants.UPDATE;
                    iSavDepID = Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["Id"].Value);
                    UPFTransno = Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["UPFTransID"].Value); 

                    if (lblStatusVal.Text == SPARC.Status.CLOSED)
                        btnSave.Enabled = false;
                    else
                        btnSave.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar)) // && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId, Int32 AccId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            UPFAccount objUPFAccount = new UPFAccount();
            objUPFAccount.Id = DataId;
            objUPFAccount.AccId = AccId;

            
            dsDeleteStatus = objUPFAccount.DeleteOpenUPFAccount(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadAllUPFAccounts();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else if (iResult == -1)
                {
                    lblMessages.Text = Messages.UPF_PAYOUT_XN_AVAILABLE;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgUPFAccount_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgUPFAccount.Columns[e.ColumnIndex].Name== "btnDelete")
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[11].Value), Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[0].Value));                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgUPFAccount_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
            {
                dgUPFAccount.Columns["btnDelete"].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearControls();
        }
    }
}