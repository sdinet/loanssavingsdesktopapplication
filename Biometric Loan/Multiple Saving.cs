﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Timers;
using System.Windows.Threading;

namespace SPARC
{
    public partial class Multiple_Saving : Form
    {       
        private Int32 iSavDepID = 0;
        public string TransType1 = string.Empty;
        private double dblBalance = 0;
        private double dblCurrentOldBalance = 0;
        string pb = "";
        string memname = "";
        Int64 memberid;
        Int64 accounid;
        // System.Threading.Timer timer,Timeout;
        public string TransType = string.Empty;
        public Int16 intMemberOperationType;
        private double RemainingPrincipal = 0;
        public Multiple_Saving()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            if (cmbAgentName.Items.Count > 0) cmbAgentName.SelectedIndex = 0;            

            iSavDepID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
            dblCurrentOldBalance = 0;
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;           

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                //Check available balance before withdrawal
                if (TransType == Constants.DEBITTRANSABBREV)
                {
                    // if (iSavDepID == 0 && (dblBalance <= 0 || Convert.ToDouble(txtAmount.Text) > dblBalance))
                    //{
                    //  ErrorMsg = Messages.AVAILABLEBALCHECK;
                    //lblMessages.ForeColor = Color.Red;
                    //}
                    //else if (iSavDepID > 0 && (Convert.ToDouble(txtAmount.Text) > (dblBalance + dblCurrentOldBalance)))
                    //{
                    //  ErrorMsg = Messages.AVAILABLEBALCHECK;
                    // lblMessages.ForeColor = Color.Red;
                    //}

                }
            }

            return ErrorMsg;
        }

        private void RetrieveSavingsName()
        {
            CodeValues objSavingsName = new CodeValues();
            objSavingsName.fk_memberId = GlobalValues.Member_PkId;
            DataSet dsSavingname = objSavingsName.GetSavingsName();
            if (dsSavingname != null && dsSavingname.Tables.Count > 0 && dsSavingname.Tables[0].Rows.Count > 0)
            {
                cmbSavingType.DisplayMember = "Name";
                cmbSavingType.ValueMember = "Id";
                cmbSavingType.DataSource = dsSavingname.Tables[0];
            }
        }

        private void Multiple_Saving_Load(object sender, EventArgs e)
        {
            try
            {
              FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbSavingType); //Fill Savingtype    
            //edited by archana--start
              // RetrieveSavingsName();
                //edited by archana--end .yy
                int CodeType = 0;
                if (TransType == Constants.CREDITTRANSABBREV)
                    this.Text = Constants.DEPOSITMULTIPLESAVINGSTRANS;
                else
                    this.Text = Constants.WITHDRAWMULTIPLESAVINGSTRANS;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {
                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }

                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the state type code and populate state 
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                        cmbcountry.SelectedValue = Id;
                        cmbcountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the District type code and populate district
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                        cmbstate.SelectedValue = Id;
                        cmbstate.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the Slum type code and populate Slum
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                        cmbdistrict.SelectedValue = Id;
                        cmbdistrict.Enabled = false;
                    }
                }  

                //dtpTransDate.MaxDate = DateTime.Now;
                //  edit by pawan
                dtpTransDate.MaxDate = DateTime.Today.AddDays(1);
                DateTime now = DateTime.Now;
                dtpTransDate.Value = new DateTime(dtpTransDate.Value.Year, dtpTransDate.Value.Month, dtpTransDate.Value.Day, now.Hour, now.Minute, now.Second);
                //  edit by pawan end

                if (TransType == Constants.CREDITTRANSABBREV)
                    this.Text = Constants.DEPOSITMULTIPLESAVINGSTRANS ;

                else
                    this.Text = Constants.WITHDRAWMULTIPLESAVINGSTRANS ;                

                FillAgents(true);
                ClearControls();                
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }          
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        //private void RetrieveSavingsName()
        //{
        //    CodeValues objSavingsName = new CodeValues();
        //    objSavingsName.fk_memberId = GlobalValues.Member_PkId;
        //    DataSet dsSavingname = objSavingsName.GetSavingsNamenew();
        //    if (dsSavingname != null && dsSavingname.Tables.Count > 0 && dsSavingname.Tables[0].Rows.Count > 0)
        //    {
        //        cmbSavingType.DisplayMember = "Name";
        //        cmbSavingType.ValueMember = "Id";
        //        cmbSavingType.DataSource = dsSavingname.Tables[0];
        //    }
        //}
        private void txtPassbookNo_TextChanged(object sender, EventArgs e)
        {                      
            pb = txtPassbookNo.Text;            
        }

        private void txtPassbookNo_Leave(object sender, EventArgs e)
        {
            //Do your stuff
            //String text = txtPassbookNo.Text;
        }
        SavingAccountTransactions objmulsav = new SavingAccountTransactions();
       int member_pkid;
        private void txtPassbookNo_Leave_1(object sender, EventArgs e)
        {
            try
            {
                txtAccountNo.Text = string.Empty;
                txtMemberName.Text = string.Empty;
                //cmbSavingType.SelectedIndex = 0;
                objmulsav.PassbookNo = txtPassbookNo.Text.Trim();
                objmulsav.Fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?)null;
                pb = txtPassbookNo.Text;
                objmulsav.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
                string ac = "";
                if (cmbslum.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select the Slum Name");
                }
                //else if (cmbSavingType.SelectedIndex <= 0)
                //{
                //    MessageBox.Show("Please select the Saving Type");
                //}
                else if (txtPassbookNo.Text == "")
                {
                    MessageBox.Show("Please enter the PassbookNumber.");
                }
                else
                {
                    DataSet dsSavAcc = objmulsav.GetAccountNumber();
                    if (dsSavAcc != null && dsSavAcc.Tables.Count > 0 && dsSavAcc.Tables[0].Rows.Count > 0)
                    {
                        txtAccountNo.Text = dsSavAcc.Tables[0].Rows[0]["AccountNumber"].ToString();
                        ac = dsSavAcc.Tables[0].Rows[0]["AccountNumber"].ToString();
                        memname = dsSavAcc.Tables[0].Rows[0]["Name"].ToString();
                        txtMemberName.Text = dsSavAcc.Tables[0].Rows[0]["Name"].ToString();
                        member_pkid = Convert.ToInt32(dsSavAcc.Tables[0].Rows[0]["fk_MemberId"]);
                    }
                }
                if (txtAccountNo.Text != "")
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

                    conn.Open();
                    string query = "select m.Id as fk_MemberId from Members m inner join SavingAccount s on m.Id=s.fk_MemberId where s.passbookno= @passbooknumber and m.fk_SlumId = @slumid and s.fk_Savingtype = @savingtype";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add(new SqlParameter("@PassbookNumber", txtPassbookNo.Text));
                    cmd.Parameters.Add(new SqlParameter("@slumid", cmbslum.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@savingtype", cmbSavingType.SelectedValue));
                    memberid = Convert.ToInt64(cmd.ExecuteScalar());
                    conn.Close();
                    conn.Open();
                    string query1 = "select s.Id as fk_Accountid from Members m inner join SavingAccount s on m.Id=s.fk_MemberId where s.passbookno=@PassbookNumb and fk_SlumId = @slumid and  s.fk_Savingtype = @savingtype;";
                    SqlCommand cmd1 = new SqlCommand(query1, conn);
                    cmd1.Parameters.Add(new SqlParameter("@PassbookNumb", txtPassbookNo.Text));
                    cmd1.Parameters.Add(new SqlParameter("@slumid", cmbslum.SelectedValue));
                    cmd1.Parameters.Add(new SqlParameter("@savingtype", cmbSavingType.SelectedValue));
                    accounid = Convert.ToInt64(cmd1.ExecuteScalar());
                    conn.Close();
                }
                else
                {
                    MessageBox.Show("Entered Passbook Number is Incorrect Or Savings Account is not created/closed");
                }
            }
            catch (Exception ex)
            {
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }       
        string passbookno;
        string accountno;
        Int64 fkmemid;
        Int64 SavId;
        Int64 AgntId;
        double amount;
        DateTime date;
        Int64 savingtype;
        private void btnSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                passbookno = Convert.ToString(dataGridView1.Rows[i].Cells["PassbookNumber"].Value);
                accountno = Convert.ToString(dataGridView1.Rows[i].Cells["Accountnumber"].Value);
                fkmemid = Convert.ToInt64(dataGridView1.Rows[i].Cells["fkMemberId"].Value);
                SavId = Convert.ToInt64(dataGridView1.Rows[i].Cells["SavingsId"].Value);
                amount = Convert.ToDouble(dataGridView1.Rows[i].Cells["TransactionAmount"].Value);
                date = Convert.ToDateTime(dataGridView1.Rows[i].Cells["TransactionDate"].Value);
                AgntId = Convert.ToInt64(dataGridView1.Rows[i].Cells["AgentId"].Value);
                savingtype = Convert.ToInt64(dataGridView1.Rows[i].Cells["SavingType"].Value);
                if (passbookno != "" && amount > 0)
                {
                    SavetheAmount();  //this.Text = Constants.DEPOSITSAVINGSTRANS;
                }
                else if (Convert.ToDouble(dataGridView1.Rows[0].Cells["TransactionAmount"].Value) <= 0)
                {
                    MessageBox.Show("Please enter the Amount");
                }

                


            }
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Please enter data to save");
            }
        }

        public void SavetheAmount()
        {            
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;
                    SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();
                    objSavingsDepTrans.Id = iSavDepID;
                    objSavingsDepTrans.fk_MemberId = fkmemid;
                    objSavingsDepTrans.Tran_type = TransType;
                    objSavingsDepTrans.fk_Accountid = SavId;
                    objSavingsDepTrans.Amount = Convert.ToDouble(amount);
                    objSavingsDepTrans.fk_AgentId = Convert.ToInt32(AgntId);
                    objSavingsDepTrans.TransactionDate = Convert.ToDateTime(date);
                    objSavingsDepTrans.Remarks = "";
                    objSavingsDepTrans.Createdby = GlobalValues.User_PkId;
                    objSavingsDepTrans.Updatedby = GlobalValues.User_PkId;
                    objSavingsDepTrans.SavingType = savingtype;
                    int iResult = 0;
                    if (iSavDepID == 0)
                        iResult = objSavingsDepTrans.SaveSavingsDepositTransactions();
                    else
                        iResult = objSavingsDepTrans.SaveSavingsDepositTransactions();

                    if (iResult > 0)
                    {
                        //Update Balance
                        if (iSavDepID == 0) //New
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance += Convert.ToDouble(amount);
                            }
                            else
                            {
                                dblBalance -= Convert.ToDouble(amount);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(amount);
                        }
                        else
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance -= dblCurrentOldBalance;
                                dblBalance += Convert.ToDouble(amount);
                            }
                            else
                            {
                                dblBalance += dblCurrentOldBalance;
                                dblBalance -= Convert.ToDouble(amount);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(amount);
                        }

                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();

                        if (objSavingsDepTrans.Id == 0)
                        {
                            // btnSave.Text = Constants.UPDATE;
                            btnSave.Visible = false;
                            btnAdd.Visible = false;
                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);
                           // dataGridView1.Rows.Clear();
                            iSavDepID = iResult;
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        }
                        //sat.LoadSavingsDepTrans();
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private string DoValidationsSavings()
        {
            string ErrorMsg = string.Empty;

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;            
            if (txtPassbookNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.PASSBOOKNO + Messages.COMMAWITHSPACE;
            if (txtAccountNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.SAVINGACCOUNTNO + Messages.COMMAWITHSPACE;
            if (txtMemberName.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERNAME + Messages.COMMAWITHSPACE;
            if (txtAmount.Text.Trim().Length == 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE; 
            if(cmbSavingType.SelectedIndex == 0)
                ErrorMsg += Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;           
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                lblMessages.ForeColor = Color.Red;
            }
            else
            {
               
               
                SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();
                objSavingsDepTrans.fk_MemberId = member_pkid;
                objSavingsDepTrans.Tran_type = TransType;
                objSavingsDepTrans.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
                objSavingsDepTrans.LoanType = Convert.ToInt64(cmbSavingType.SelectedValue);
                DataSet dsDepositTrans = objSavingsDepTrans.GetSavingsDepositTransactions();
                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {
                  
                        dblBalance = Convert.ToDouble(dsDepositTrans.Tables[1].Rows[0][0].ToString());
                }

                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[2].Rows.Count > 0)
                {
                    RemainingPrincipal = Convert.ToDouble(dsDepositTrans.Tables[2].Rows[0][0].ToString());
                }


                //Check available balance before withdrawal
                if (TransType == Constants.DEBITTRANSABBREV)
                {
                    if (RemainingPrincipal >= dblBalance)
                    {
                        ErrorMsg = Messages.AMOUNTEXCEED;
                        lblMessages.ForeColor = Color.Red;
                    }
                    if (iSavDepID == 0 && (dblBalance <= 0 || Convert.ToDouble(txtAmount.Text) > dblBalance))
                    {
                        ErrorMsg = Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }
                    else if (iSavDepID > 0 && (Convert.ToDouble(txtAmount.Text) > (dblBalance + dblCurrentOldBalance)))
                    {
                        ErrorMsg = Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }
                }
            }

            return ErrorMsg;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidationsSavings();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;
                    dataGridView1.Rows.Add();
                    //
                    // Get Row number ,
                    // it is reduced by two because
                    // the first row number is zero, after adding
                    // new row to allready
                    // existing one.
                    //
                    int Row = 0;
                    Row = dataGridView1.Rows.Count - 1;

                    //
                    // Store values from text boxes to DataGridView
                    //

                    dataGridView1[0, Row].Value = txtMemberName.Text;
                    dataGridView1[1, Row].Value = txtPassbookNo.Text;
                    dataGridView1[2, Row].Value = txtAmount.Text;

                    DateTime now = dtpTransDate.Value;
                    dtpTransDate.Value = new DateTime(dtpTransDate.Value.Year, dtpTransDate.Value.Month, dtpTransDate.Value.Day, now.Hour, now.Minute, now.Second);
                    dataGridView1[3, Row].Value = dtpTransDate.Value;
                    dataGridView1[5, Row].Value = memberid;
                    dataGridView1[6, Row].Value = txtAccountNo.Text;
                    dataGridView1[7, Row].Value = accounid;
                    dataGridView1[8, Row].Value = cmbAgentName.SelectedValue;
                    dataGridView1[9, Row].Value = cmbSavingType.SelectedValue;

                    dataGridView1.Refresh();

                    //  edit by pawan
                    DateTime now1 = DateTime.Now;
                    dtpTransDate.Value = new DateTime(dtpTransDate.Value.Year, dtpTransDate.Value.Month, dtpTransDate.Value.Day, now1.Hour, now1.Minute, now1.Second);
                    //  edit by pawan  end


                    //dtpTransDate.Value = DateTime.Now.Date;                       
                    txtAmount.Text = string.Empty;
                    txtPassbookNo.Text = string.Empty;
                    txtAccountNo.Text = string.Empty;
                    txtMemberName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Delete"].Index) //To check that we are in the right column
            {
                foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
                {
                    if (oneCell.Selected)
                        dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }

        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private void cmbSavingType_SelectedIndexChanged(object sender, EventArgs e)
        {


            txtPassbookNo.Text = string.Empty;
            txtAccountNo.Text = string.Empty;
            txtMemberName.Text = string.Empty;

        }
    }
}



                    


               
