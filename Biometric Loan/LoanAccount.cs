﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;

namespace SPARC
{
    public partial class frmLoanAccount : Form
    {
        public int iAction;
        public Int32 m_ApprovedBy;
        private double CurrentBalance = 0;
        private bool LoanYesNo; 
        public frmLoanAccount(int iActionvalue)
        {
            InitializeComponent();
            iAction = iActionvalue;
        }

        //protected override void WndProc(ref Message message)
        //{
        //    const int WM_SYSCOMMAND = 0x0112;
        //    const int SC_MOVE = 0xF010;

        //    switch (message.Msg)
        //    {
        //        case WM_SYSCOMMAND:
        //            int command = message.WParam.ToInt32() & 0xfff0;
        //            if (command == SC_MOVE)
        //                return;
        //            break;
        //    }

        //    base.WndProc(ref message);
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                GlobalValues.Loan_PkId = 0;
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbLoanName.SelectedIndex == 0)
                ErrorMsg += Messages.LOANName + Messages.COMMAWITHSPACE;

            if (cmbEMIType.SelectedIndex == 0)
                ErrorMsg += Messages.EMIPAYCYCLE + Messages.COMMAWITHSPACE;

            if (txtLoanAmt.Text.Trim() == string.Empty)
                ErrorMsg += Messages.LOANAMOUNT + Messages.COMMAWITHSPACE;

            if (txtTenure.Text.Trim() == string.Empty)
                ErrorMsg += Messages.LOANTENURE + Messages.COMMAWITHSPACE;

            if (txtLoanPassbook.Text.Trim() == string.Empty)
                ErrorMsg += Messages.MEMBERPASSBOOKNUMBER + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void LoanSave()
        {
            LoanAccount objLoanAcc = new LoanAccount();

            if (iAction == 1)
                objLoanAcc.Id = 0;
            else
                objLoanAcc.Id = GlobalValues.Loan_PkId;

            objLoanAcc.AccountNumber = lblAccValue.Text;
            objLoanAcc.fk_MemberId = GlobalValues.Member_PkId;
            objLoanAcc.LoanType = Convert.ToInt64(cmbLoanName.SelectedValue);
            objLoanAcc.LoanAmount = Convert.ToDouble(txtLoanAmt.Text.Trim());
            objLoanAcc.EMISchedule = cmbEMIType.Text.Substring(0, 1);
            objLoanAcc.InterestPercent = Convert.ToDouble(cmbInterest.Text);
            objLoanAcc.Tenure = Convert.ToInt16(txtTenure.Text);
            objLoanAcc.LoanAppliedDate = dtpAppliedDate.Value;
            objLoanAcc.IsUpfLoan = chkIsUpfLoan.Checked ? true : false;

            objLoanAcc.RelationName = txtRelation.Text;
            objLoanAcc.Address = txtAddress.Text;
            objLoanAcc.AreaFederation = txtAreaFederation.Text;
            objLoanAcc.Phone = txtPhone.Text;
            if (Convert.ToInt64(cmbWtrSrc.SelectedValue) <= 0)
            {
                objLoanAcc.WaterSource = null;
            }
            else
                objLoanAcc.WaterSource = (Int64)cmbWtrSrc.SelectedValue;

            if (Convert.ToInt64(cmbToiletFacility.SelectedValue) == 0)
            {
                objLoanAcc.ToiletFacility = null;
            }
            else
                objLoanAcc.ToiletFacility = (Int64)cmbToiletFacility.SelectedValue;

            objLoanAcc.HouseSize = txtHouseSize.Text;

            if (Convert.ToInt64(cmbHouseRoof.SelectedValue) == 0)
            {
                objLoanAcc.HouseRoof = null;
            }
            else
                objLoanAcc.HouseRoof = (Int64)cmbHouseRoof.SelectedValue;

            if (Convert.ToInt64(cmbWalls.SelectedValue) == 0)
            {
                objLoanAcc.Walls = null;
            }
            else
                objLoanAcc.Walls = (Int64)cmbWalls.SelectedValue;

            if (Convert.ToInt64(cmbFloor.SelectedValue) == 0)
            {
                objLoanAcc.HouseFloor = null;
            }
            else
                objLoanAcc.HouseFloor = (Int64)cmbFloor.SelectedValue;

            objLoanAcc.HousePatta = txtHousePatta.Text;

            if (Convert.ToInt64(cmbConstType.SelectedValue) == 0)
            {
                objLoanAcc.ConstructionType = null;
            }
            else
                objLoanAcc.ConstructionType = (Int64)cmbConstType.SelectedValue;

            if (Convert.ToInt64(cmbConstDone.SelectedValue) == 0)
            {
                objLoanAcc.ConstructionDone = null;
            }
            else
                objLoanAcc.ConstructionDone = (Int64)cmbConstDone.SelectedValue;

            objLoanAcc.HouseCost = txtCostOfHouse.Text;
            objLoanAcc.ConstructionTime = txtConstTime.Text;
            objLoanAcc.DepositGiven = txtDeposit.Text;

            if (iAction == 3 || cmbStatus.Text.Trim().ToLower() == Status.APPROVED.ToLower())
            {
                objLoanAcc.Status = Status.APPROVED;
                objLoanAcc.LoanSanctionedDate = dtpSanctionDate.Value;
                objLoanAcc.ApprovedBy = m_ApprovedBy > 0 ? m_ApprovedBy : GlobalValues.User_PkId;
            }
            else if (iAction < 3)
            {
                objLoanAcc.Status = cmbStatus.Text;
                objLoanAcc.LoanSanctionedDate = dtpSanctionDate.Value;//Convert.ToDateTime("1-1-1990");
                objLoanAcc.ApprovedBy = 0;
            }
            else
            {
                objLoanAcc.Status = cmbStatus.Text;
                objLoanAcc.LoanSanctionedDate = dtpSanctionDate.Value;
                objLoanAcc.ApprovedBy = m_ApprovedBy;//0; // lblSanctionedByName.Text;
            }

            objLoanAcc.EMIPlannedEnd = dtpEMIEndDate.Value;
            objLoanAcc.EMIPlannedStart = dtpStartDate.Value;

            string Str;
            double Num;

            Str = lblEMIAmtVal.Text.Trim();
            bool isNum = double.TryParse(Str, out Num);
            if (isNum)
                objLoanAcc.EMIPrincipal = Convert.ToDouble(lblEMIAmtVal.Text);

            //Str = lblEMICharges.Text.Trim();
            //isNum = double.TryParse(Str, out Num);
            //if (isNum)
            //    objLoanAcc.EMICharges = Convert.ToDouble(lblEMICharges.Text);

            //Str = lblEMITotal.Text.Trim();
            //isNum = double.TryParse(Str, out Num);
            //if (isNum)
            //    objLoanAcc.EMITotal = Convert.ToDouble(lblEMITotal.Text);

            Str = lblChargeAmtVal.Text.Trim();
            isNum = double.TryParse(Str, out Num);
            if (isNum)
                objLoanAcc.TotalCharges = Convert.ToDouble(lblChargeAmtVal.Text);//to be chk - numeric validation

            objLoanAcc.Remarks = txtRemarks.Text.Trim();

            objLoanAcc.Createdby = GlobalValues.User_PkId;
            objLoanAcc.Updatedby = GlobalValues.User_PkId;
            objLoanAcc.LoanPassbook = txtLoanPassbook.Text;

            DataSet dsLoan = objLoanAcc.SaveLoanAcc();


            if (dsLoan != null && dsLoan.Tables.Count > 0 && dsLoan.Tables[0].Rows.Count > 0)
            {
                btnPrint.Visible = true;
                frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                objBioMetric.FetchMemberDetailsByID();
                objBioMetric.InsertMemberDetailsToGrid();

                if (objLoanAcc.Id == 0)
                {
                    btnSave.Text = Constants.UPDATE;
                    //Messages.ShowMessage(Messages.SAVED_SUCCESS + Messages.SAVED_CURRENTLOANACC, Messages.MsgType.success);
                    GlobalValues.Loan_PkId = Convert.ToInt64(dsLoan.Tables[0].Rows[0]["Id"]);

                    lblMessages.Text = Messages.SAVED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iAction == 2)
                {
                    lblMessages.Text = Messages.UPDATED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                    //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                }
                else if (iAction == 3)
                {
                    //Messages.ShowMessage(Messages.APPROVED_SUCCESS, Messages.MsgType.success);
                    lblMessages.Text = Messages.APPROVED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;

                    btnApprove.Visible = false;
                    btnSave.Text = "Update";
                    btnCalculate.Enabled = false;
                    cmbStatus.SelectedIndex = cmbStatus.FindString(Status.APPROVED);
                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                        cmbStatus.Enabled = true;
                    lblAccValue.Text = dsLoan.Tables[0].Rows[0]["AccountNumber"].ToString();
                    cmbStatus.Items.RemoveAt(cmbStatus.FindString(Status.INITIATED));
                    DisableControlsAfterApprove();//DONT REMOVE - if this needs to be removed, EMI calculations need to be re-performed for every saving including EMI End date
                    pnlPaidDetails.Visible = true;
                    lblNextDueDateValue.Text = dtpStartDate.Value.ToShortDateString();
                    lblBalPrincipalValue.Text = txtLoanAmt.Text;
                    lblSanctionedByName.Text = GlobalValues.User_FullName;
                    pnlSanction.Enabled = false;
                }
                if (cmbStatus.SelectedIndex == cmbStatus.FindString(Status.INITIATED) && GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                {
                    btnApprove.Visible = true;
                    cmbStatus.Enabled = false;
                    SanctionPanelShow(true);
                    dtpSanctionDate.Value = DateTime.Now;
                }
                else if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
                {
                    cmbStatus.Enabled = false;
                    btnApprove.Visible = false;
                }
                if (cmbStatus.SelectedIndex == cmbStatus.FindString(Status.WRITEOFF) || cmbStatus.SelectedIndex == cmbStatus.FindString(Status.CLOSED))
                {
                    cmbStatus.Enabled = false;
                }
                GlobalValues.Loan_AccNumber = dsLoan.Tables[0].Rows[0]["AccountNumber"].ToString();
            }
            else
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure);
            }
        }

        private void LoanValidations()
        {            
            LoanAccount objLoanAccS = new LoanAccount();
            objLoanAccS.fk_MemberId = GlobalValues.Member_PkId;
            objLoanAccS.SavingType = Convert.ToInt64(cmbLoanName.SelectedValue);
            DataSet dsLoanSav = objLoanAccS.GetSavingsCurrBalance();
            if (dsLoanSav.Tables.Count > 0 && dsLoanSav.Tables[0].Rows.Count > 0)
            {
                CurrentBalance = Convert.ToDouble(dsLoanSav.Tables[0].Rows[0][0].ToString());
            }

            if (((CurrentBalance * 2) <= Convert.ToDouble(txtLoanAmt.Text)))
            {
                LoanYesNo = true;
            }
            else
                LoanYesNo = false;                        
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);


                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        private void DisableControlsAfterApprove()
        {
            txtLoanAmt.Enabled = false;
            cmbInterest.Enabled = false;
            cmbEMIType.Enabled = false;
            txtTenure.Enabled = false;
            dtpStartDate.Enabled = false;
            cmbLoanName.Enabled = false;
            chkIsUpfLoan.Enabled = false;
        }
        private void SaveData()
        {
            //confirm if the status changed to write-off
            if (cmbStatus.Enabled && cmbStatus.SelectedIndex == cmbStatus.FindString(Status.WRITEOFF))
            {
                DialogResult dsConfirmation = MessageBox.Show("Do you want to Write-off the loan status? This action cannot be rollbacked", "SPARC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dsConfirmation == DialogResult.No)
                    return;
            }
            if (cmbStatus.Enabled && cmbStatus.SelectedIndex == cmbStatus.FindString(Status.CLOSED) && Convert.ToDouble(lblBalPrincipalValue.Text)>0)
            {
                MessageBox.Show("Loan cannot be closed when there is remaining principal to be paid.", "SPARC", MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            }

            string strMessage = DoValidations();
            if (strMessage.Length > 0)
            {
                lblMessages.Text = strMessage;
            }
            else
            {
                lblMessages.Text = string.Empty;
                LoanValidations();
                if (LoanYesNo == true)
                {
                    if (MessageBox.Show("Loan Amount exceeding the permissable level (Saving amount less than loan being taken), Are you sure you want to continue?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    {
                        LoanSave();
                    }                   
                }
                else
                {
                    LoanSave();
                }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {                        
            try
            {
                iAction = 2;
                SaveData();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void RetrieveLoanName()
        {
            CodeValues objLoanName = new CodeValues();
            objLoanName.fk_memberId = GlobalValues.Member_PkId;
            DataSet dsLoangname = objLoanName.GetLoanName();
            if (dsLoangname != null && dsLoangname.Tables.Count > 0 && dsLoangname.Tables[0].Rows.Count > 0)
            {
                cmbLoanName.DisplayMember = "Name";
                cmbLoanName.ValueMember = "Id";
                cmbLoanName.DataSource = dsLoangname.Tables[0];
            }
        }

        private void frmLoanAccount_Load(object sender, EventArgs e)
        {
            try
            {
              //  lblAccValue.Text = Convert.ToString(GlobalValues.Loan_PkId);
              //  FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbLoanType); //Fill Loantype                
                dtpAppliedDate.MaxDate = DateTime.Now.Date;
                //dtpStartDate.MinDate = dtpAppliedDate.MaxDate;
                lblMemberValue.Text = GlobalValues.Member_ID;
                FillCombos(Convert.ToInt32(OtherCodeTypes.EMISchedule), 0, cmbEMIType);
                if (iAction == 1)
                {
                    FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbLoanName); // Fill LoanType
                }
                else if (iAction == 2)
                {
                    RetrieveLoanName();  
                }
                FillCombos(Convert.ToInt32(LocationCode.WaterSource), 0 , cmbWtrSrc);
                FillCombos(Convert.ToInt32(LocationCode.ToiletFacility), 0, cmbToiletFacility);
                FillCombos(Convert.ToInt32(LocationCode.HouseRoof), 0, cmbHouseRoof);
                FillCombos(Convert.ToInt32(LocationCode.Walls), 0, cmbWalls);
                FillCombos(Convert.ToInt32(LocationCode.HouseFloor), 0, cmbFloor);
                FillCombos(Convert.ToInt32(LocationCode.ConstructionType), 0, cmbConstType);
                FillCombos(Convert.ToInt32(LocationCode.ConstructionDone), 0, cmbConstDone);

                ResetControls();
                //if (GlobalValues.Loan_PkId <= 0) //Create
                if(iAction == 1)
                {
                    btnPrint.Visible = false;
                    cmbStatus.Enabled = false;
                    btnApprove.Visible = false;
                }
                //else//UPDATE MODE
                //{
                //    //LoanAccount objLoanAccFetch = new LoanAccount();
                //   // objLoanAccFetch.Id = GlobalValues.Loan_PkId;
                //   // objLoanAccFetch.LoanType = cmbLoanName.Text;

                //    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                //    {
                //        btnDelete.Visible = true;
                //    }
                //    //FETCH AND POPULATE THE DATA
                //   // LoadControls(objLoanAccFetch);

                //    if (cmbStatus.Text == Status.INITIATED)
                //    {
                //        btnCalculate.Enabled = true;
                //        cmbStatus.Enabled = false;
                //        pnlPaidDetails.Visible = false;
                //        if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                //        {
                //            btnApprove.Visible = true;
                //            SanctionPanelShow(true);
                //            dtpSanctionDate.Value = DateTime.Now;
                //        }
                //        else
                //        {
                //            btnApprove.Visible = false;
                //        }
                //    }
                //    else
                //    {
                //        cmbStatus.Items.RemoveAt(cmbStatus.FindString(Status.INITIATED));
                //        btnCalculate.Enabled = false;
                //        if (cmbStatus.Text != Status.WRITEOFF && cmbStatus.Text != Status.CLOSED)
                //        {
                //            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                //                cmbStatus.Enabled = true;
                //            else
                //                cmbStatus.Enabled = false;
                //        }
                //        else
                //        {
                //            cmbStatus.Enabled = false;
                //        }
                //        SanctionPanelShow(true);
                //        pnlPaidDetails.Visible = true;
                //        btnApprove.Visible = false;
                //    }                   
                //    Calculate();                   
                //}

                btnSave.Focus();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private void ResetControls()
        {
            cmbLoanName.SelectedIndex = 0;
            cmbEMIType.SelectedIndex = 0;
            cmbInterest.SelectedIndex = 0;

            lblMessages.Text = string.Empty;

            if (iAction == 1)
            {
                //LoanAccount objLoanAccS = new LoanAccount();
                //objLoanAccS.fk_MemberId = GlobalValues.Member_PkId;
                //DataSet dsLoanSav = objLoanAccS.GetSavingsCurrBalance();
                //if (dsLoanSav.Tables.Count > 0 && dsLoanSav.Tables[0].Rows.Count > 0)
                //{
                //    CurrentBalance = Convert.ToDouble(dsLoanSav.Tables[0].Rows[0][0].ToString());
                //}

                btnSave.Text = Constants.SAVE;
                cmbStatus.Text = Status.INITIATED;

                SanctionPanelShow(false);

                pnlPaidDetails.Visible = false;

                this.Text = Constants.LOANCREATE + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
            }
            else if (iAction == 2)
            {
                btnSave.Text = Constants.UPDATE;
                this.Text = Constants.LOANUPDATE + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
            }
            else if (iAction == 3)
            {
                SanctionPanelShow(true);
                this.Text = Constants.LOANAPPROVE + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
            }
        }
        private void SanctionPanelShow(bool bState)
        {
            pnlSanction.Visible = bState;
            lblSanctionDate.Visible = bState;
            lblSanctionedBy.Visible = bState;
            lblSanctionedByName.Visible = bState;
            dtpSanctionDate.Visible = bState;
        }
        private void LoadControls(LoanAccount objLoanAccFetch)
        {
            DataSet dsLoanAccFetch = objLoanAccFetch.LoadLoanAccById();
            //if (dsLoanAccFetch != null && dsLoanAccFetch.Tables.Count > 0 && !(dsLoanAccFetch.Tables[1].Rows.Count > 0) && iAction == 1)
                if ( (dsLoanAccFetch.Tables[1].Rows.Count == 0) && iAction == 1)
            {
                MessageBox.Show("The Saving account of selected Loan type does not exist. Please create Saving account to Initiate loan");
                cmbLoanName.SelectedIndex = 0;
            }

            if (dsLoanAccFetch != null && dsLoanAccFetch.Tables.Count > 0 && dsLoanAccFetch.Tables[0].Rows.Count > 0 && iAction == 1)
            {
                MessageBox.Show("The selected Loan Account already exists");
                this.frmLoanAccount_Load(null, null);
            }
            if (dsLoanAccFetch != null && dsLoanAccFetch.Tables.Count > 0 && dsLoanAccFetch.Tables[0].Rows.Count > 0 && iAction == 2)
            {
                GlobalValues.Loan_PkId = Convert.ToInt64(dsLoanAccFetch.Tables[0].Rows[0]["Id"]);
                chkIsUpfLoan.Checked = Convert.ToBoolean(dsLoanAccFetch.Tables[0].Rows[0]["IsUPFLoan"]);
                lblAccValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["AccountNumber"].ToString();
                cmbStatus.SelectedIndex = cmbStatus.FindString(dsLoanAccFetch.Tables[0].Rows[0]["Status"].ToString());
                if (dsLoanAccFetch.Tables[0].Rows[0]["Status"].ToString().Trim() != Status.INITIATED)
                {
                    DisableControlsAfterApprove();//DONT REMOVE - if this needs to be removed, EMI calculations need to be re-performed for every saving including EMI End date
                }
                //cmbStatus.Text = dsLoanAccFetch.Tables[0].Rows[0]["Status"].ToString();
                dtpAppliedDate.Value = Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["LoanAppliedDate"]);

                cmbLoanName.Text = dsLoanAccFetch.Tables[0].Rows[0]["LoanType"].ToString();
                if (dsLoanAccFetch.Tables[0].Rows[0]["EMISchedule"].ToString() == "W")
                    cmbEMIType.Text = EMIType.WEEKLY;
                else if (dsLoanAccFetch.Tables[0].Rows[0]["EMISchedule"].ToString() == "D")
                    cmbEMIType.Text = EMIType.DAILY;
                else
                    cmbEMIType.Text = EMIType.MONTHLY;
                dtpStartDate.Value = Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["EMIPlannedStart"]);
                dtpEMIEndDate.Value = Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["EMIPlannedEnd"]);
                txtLoanAmt.Text = dsLoanAccFetch.Tables[0].Rows[0]["LoanAmount"].ToString();
                txtTenure.Text = dsLoanAccFetch.Tables[0].Rows[0]["Tenure"].ToString();
                cmbInterest.Text = dsLoanAccFetch.Tables[0].Rows[0]["InterestPercent"].ToString();
                //lblBalPrincipalValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["BalancePrinciple"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["BalancePrinciple"].ToString();
                txtRemarks.Text = dsLoanAccFetch.Tables[0].Rows[0]["Remarks"].ToString();

                lblSanctionedByName.Text = dsLoanAccFetch.Tables[0].Rows[0]["ApproverName"].ToString();
                if (dsLoanAccFetch.Tables[0].Rows[0]["ApprovedBy"] != DBNull.Value && dsLoanAccFetch.Tables[0].Rows[0]["ApprovedBy"].ToString().Trim().Length > 0)
                    m_ApprovedBy = Convert.ToInt32(dsLoanAccFetch.Tables[0].Rows[0]["ApprovedBy"].ToString());
                if(dsLoanAccFetch.Tables[0].Rows[0]["Status"].ToString().Trim() == Status.APPROVED)
                    dtpSanctionDate.Value = Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["LoanSanctionedDate"]);

                //if (dsLoanAccFetch.Tables.Count > 1 && dsLoanAccFetch.Tables[0].Rows.Count > 0)
                //{
                    lblTotalPaidValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["EMITotalPaid"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["EMITotalPaid"].ToString();
                    lblPrincipalPaidValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["EMIPrincipalPaid"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["EMIPrincipalPaid"].ToString();
                    lblInterestPaidValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["EMIChargesPaid"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["EMIChargesPaid"].ToString();
                    lblBalPrincipalValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["RemainingPrincipal"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["RemainingPrincipal"].ToString();//Convert.ToString(Convert.ToDouble(txtLoanAmt.Text) - Convert.ToDouble(lblPrincipalPaidValue.Text));
                //}

                //if (dsLoanAccFetch.Tables.Count > 2 && dsLoanAccFetch.Tables[2].Rows.Count > 0)
                //{
                    lblLastPaidDateVal.Text = dsLoanAccFetch.Tables[0].Rows[0]["LastPaidDate"] != DBNull.Value ? Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["LastPaidDate"]).ToString("dd-MMM-yyyy") : "NA";
                //}

                //if (dsLoanAccFetch.Tables.Count > 3 && dsLoanAccFetch.Tables[3].Rows.Count > 0)
                //{
                    lblNextDueDateValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["EMIDueDate"] != DBNull.Value ? Convert.ToDateTime(dsLoanAccFetch.Tables[0].Rows[0]["EMIDueDate"]).ToString("dd-MMM-yyyy") : "NA";
                //}

                //if (dsLoanAccFetch.Tables.Count > 4 && dsLoanAccFetch.Tables[4].Rows.Count > 0)
                //{
                    lblAmtDispersedValue.Text = dsLoanAccFetch.Tables[0].Rows[0]["AmtDispersed"].ToString() == string.Empty ? "0" : dsLoanAccFetch.Tables[0].Rows[0]["AmtDispersed"].ToString();
                //}

                if (iAction == 2 && dsLoanAccFetch.Tables[0].Rows[0]["ApproverName"].ToString().Trim().Length > 0)
                {
                    LockControls();
                }

                txtRelation.Text = dsLoanAccFetch.Tables[0].Rows[0]["RelationName"].ToString();
                txtAddress.Text = dsLoanAccFetch.Tables[0].Rows[0]["Address"].ToString();
                txtAreaFederation.Text = dsLoanAccFetch.Tables[0].Rows[0]["AreaFederation"].ToString();
                txtPhone.Text = dsLoanAccFetch.Tables[0].Rows[0]["Phone"].ToString();
                cmbWtrSrc.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["WaterSource"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["WaterSource"]);
               
                cmbToiletFacility.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["ToiletFacility"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["ToiletFacility"]);
             
                txtHouseSize.Text = dsLoanAccFetch.Tables[0].Rows[0]["SizeOfHouse"].ToString();
                cmbHouseRoof.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["HouseRoof"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["HouseRoof"]);
              
                cmbWalls.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["Walls"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["Walls"]);
                
                cmbFloor.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["HouseFloor"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["HouseFloor"]);
                
                txtHousePatta.Text = dsLoanAccFetch.Tables[0].Rows[0]["HousePattaOrSecurity"].ToString();
                cmbConstType.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["ConstructionType"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["ConstructionType"]);
                
                cmbConstDone.SelectedValue = dsLoanAccFetch.Tables[0].Rows[0]["ConstructionDone"] == DBNull.Value ? 0 : Convert.ToInt16(dsLoanAccFetch.Tables[0].Rows[0]["ConstructionDone"]);
                
                txtCostOfHouse.Text = dsLoanAccFetch.Tables[0].Rows[0]["TotalHouseCost"].ToString();
                txtDeposit.Text = dsLoanAccFetch.Tables[0].Rows[0]["DepositWithLoanReq"].ToString();
                txtConstTime.Text = dsLoanAccFetch.Tables[0].Rows[0]["ConstructionTime"].ToString();
                txtLoanPassbook.Text = dsLoanAccFetch.Tables[0].Rows[0]["passbookno"].ToString();

            }
            else if (iAction == 2)
            {
                MessageBox.Show("The selected Account type does not exist.");
                this.frmLoanAccount_Load(null, null);
            }            

        }
        private void LockControls()
        {
            dtpSanctionDate.Enabled = false;
            dtpAppliedDate.Enabled = false;
            cmbLoanName.Enabled = false;
            cmbEMIType.Enabled = false;
            dtpStartDate.Enabled = false;
            dtpEMIEndDate.Enabled = false;
            txtLoanAmt.Enabled = false;
            txtTenure.Enabled = false;
            cmbInterest.Enabled = false;
            btnCalculate.Enabled = false;
            chkIsUpfLoan.Enabled = false;
        }
        private void Calculate()
        {
            string strMessage = DoValidations();
            if (strMessage.Length > 0)
            {
                lblMessages.Text = strMessage;
            }
            else
            {
                lblMessages.Text = string.Empty;
                int intTenure = Convert.ToInt16(txtTenure.Text);
                double dblLoanAmt = Convert.ToDouble(txtLoanAmt.Text);
                intTenure = intTenure == 0 ? 1 : intTenure;                
                //EMI 
                lblEMIAmtVal.Text = (Math.Round(dblLoanAmt / Convert.ToDouble(intTenure))).ToString();
                double EMIPrincipal = Convert.ToDouble(lblEMIAmtVal.Text);

                //double dblIntAmt = (Convert.ToDouble(txtLoanAmt.Text) / 100) * (Convert.ToDouble(cmbInterest.Text));
                //lblEMICharges.Text = cmbEMIType.Text == EMIType.WEEKLY ? Convert.ToString(Math.Round(Convert.ToDouble(dblIntAmt / 4.0))) : Convert.ToString(Math.Round(dblIntAmt));//Convert.ToString(dblIntAmt);
                //lblEMITotal.Text = (Math.Round(dblLoanAmt / Convert.ToDouble(intTenure)) + Convert.ToDouble(lblEMICharges.Text)).ToString();
                //lblChargeAmtVal.Text = Convert.ToString(Convert.ToDouble(lblEMICharges.Text)*Convert.ToDouble(intTenure)); //cmbEMIType.Text == EMIType.WEEKLY ? Convert.ToString(Math.Round(dblIntAmt * Convert.ToDouble(intTenure / 4.0))) : Convert.ToString(dblIntAmt * Convert.ToDouble(intTenure));

                //total scheduled charges
                double charges=0.00;
                for (int i = intTenure; i>= 1; i--)
                {
                    charges += Convert.ToDouble((EMIPrincipal * i) * (Convert.ToDouble(cmbInterest.Text)) / 100);
                }
                if (cmbEMIType.Text == EMIType.WEEKLY)
                    charges = (charges / 4.0);
                else if (cmbEMIType.Text == EMIType.DAILY)
                    charges = (charges / 30.0);
                else
                    charges = charges;
                lblChargeAmtVal.Text = charges.ToString("0.00");

                double dblTotalAmt = Convert.ToDouble(lblChargeAmtVal.Text) + Convert.ToDouble(txtLoanAmt.Text);
                lblTotalAmtVal.Text = Convert.ToString(dblTotalAmt);

                if (cmbEMIType.Text == EMIType.WEEKLY)
                {
                    dtpEMIEndDate.Value = dtpStartDate.Value.AddDays(7 * intTenure);
                }
                else if (cmbEMIType.Text == EMIType.DAILY)
                {
                    dtpEMIEndDate.Value = dtpStartDate.Value.AddDays(intTenure);
                }
                else
                {
                    dtpEMIEndDate.Value = dtpStartDate.Value.AddMonths(intTenure);
                }

                //pnlCalculations.Enabled = true;
                btnSave.Enabled = true;
            }
        }
        private void btnCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                Calculate();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private void cmbEMIType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbEMIType.Text == EMIType.WEEKLY)
            {
                lblTenure.Text = "Tenure (Weeks) *";
            }
            else if (cmbEMIType.Text == EMIType.DAILY)
            {
                lblTenure.Text = "Tenure (Daily) *";
            }
            else
            {
                lblTenure.Text = "Tenure (Months) *";
            }
        }
        private void txtTenure_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtTenure_KeyPress(object sender, KeyPressEventArgs e)
        {
            NumericKeyPress(sender, e);
        }
        private void txtLoanAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            NumericKeyPress(sender, e);
        }
        private void NumericKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                    && !char.IsDigit(e.KeyChar))
            //&& e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }
        private void cmbLoanName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt64(cmbLoanName.SelectedValue) > 0)
                {
                    LoanAccount objLoanAccFetch = new LoanAccount();
                    objLoanAccFetch.Id = GlobalValues.Member_PkId;
                    objLoanAccFetch.LoanType = Convert.ToInt64(cmbLoanName.SelectedValue);
                    LoadControls(objLoanAccFetch);
                }
                if (cmbLoanName.Text.ToUpper() == "HOUSING" || cmbLoanName.Text.ToUpper() == "SANITATION")
                    cmbInterest.Text = "1";
                else
                    cmbInterest.Text = "2";

                if (GlobalValues.Loan_PkId != 0)
                {
                    //LoanAccount objLoanAccFetch = new LoanAccount();
                    // objLoanAccFetch.Id = GlobalValues.Loan_PkId;
                    // objLoanAccFetch.LoanType = cmbLoanName.Text;

                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                    {
                        btnDelete.Visible = true;
                    }
                    //FETCH AND POPULATE THE DATA
                    // LoadControls(objLoanAccFetch);

                    if (cmbStatus.Text == Status.INITIATED)
                    {
                        btnCalculate.Enabled = true;
                        cmbStatus.Enabled = false;
                        pnlPaidDetails.Visible = false;
                        if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                        {
                            btnApprove.Visible = true;
                            SanctionPanelShow(true);
                            dtpSanctionDate.Value = DateTime.Now;
                        }
                        else
                        {
                            btnApprove.Visible = false;
                        }
                    }
                    else
                    {
                        cmbStatus.Items.RemoveAt(cmbStatus.FindString(Status.INITIATED));
                        btnCalculate.Enabled = false;
                        if (cmbStatus.Text != Status.WRITEOFF && cmbStatus.Text != Status.CLOSED)
                        {
                            if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper())
                                cmbStatus.Enabled = true;
                            else
                                cmbStatus.Enabled = false;
                        }
                        else
                        {
                            cmbStatus.Enabled = false;
                        }
                        SanctionPanelShow(true);
                        pnlPaidDetails.Visible = true;
                        btnApprove.Visible = false;
                    }
                    Calculate();
                }
            }
            catch (Exception)
            {
            }

        }
        private void btnApprove_Click(object sender, EventArgs e)
        {
            iAction=3;
            SaveData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                DeleteData();
            }
        }

        private void DeleteData()
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            LoanAccount objLoanAccount = new LoanAccount();
            objLoanAccount.Id = GlobalValues.Loan_PkId;

            dsDeleteStatus = objLoanAccount.DeleteLoanAccount(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    objBioMetric.FetchMemberDetailsByID();
                    objBioMetric.InsertMemberDetailsToGrid();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                    this.Close();
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_ACCOUNT;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure);
                lblMessages.ForeColor = Color.Red;
            }
        }       

        private void txtLoanAmt_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtTenure_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                //if (e.KeyChar == '.'
                //    && (sender as TextBox).Text.IndexOf('.') > -1)
                //{
                //    e.Handled = true;
                //}
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dtpAppliedDate_ValueChanged(object sender, EventArgs e)
        {
            dtpStartDate.MinDate = dtpAppliedDate.Value;
            dtpSanctionDate.MinDate = dtpAppliedDate.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoanForm frm2 = new LoanForm(GlobalValues.Member_PkId);
            frm2.Show();
        }       
    }
}