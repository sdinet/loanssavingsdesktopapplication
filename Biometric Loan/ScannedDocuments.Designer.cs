﻿namespace SPARC
{
    partial class ScannedDocuments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMemberID = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.typlbl = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.cmbDocType = new System.Windows.Forms.ComboBox();
            this.nmelbl = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btndlt = new System.Windows.Forms.Button();
            this.btnImage = new System.Windows.Forms.Button();
            this.lblImage = new System.Windows.Forms.Label();
            this.txtImage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMemberID
            // 
            this.txtMemberID.AutoSize = true;
            this.txtMemberID.Location = new System.Drawing.Point(704, 28);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(0, 13);
            this.txtMemberID.TabIndex = 7;
            this.txtMemberID.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(733, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 8;
            // 
            // typlbl
            // 
            this.typlbl.AutoSize = true;
            this.typlbl.Location = new System.Drawing.Point(211, 17);
            this.typlbl.Name = "typlbl";
            this.typlbl.Size = new System.Drawing.Size(83, 13);
            this.typlbl.TabIndex = 2;
            this.typlbl.Text = "Document Type";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(335, 66);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmbDocType
            // 
            this.cmbDocType.AllowDrop = true;
            this.cmbDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocType.FormattingEnabled = true;
            this.cmbDocType.Location = new System.Drawing.Point(300, 14);
            this.cmbDocType.Name = "cmbDocType";
            this.cmbDocType.Size = new System.Drawing.Size(121, 21);
            this.cmbDocType.TabIndex = 1;
            this.cmbDocType.SelectedIndexChanged += new System.EventHandler(this.cmbDocType_SelectedIndexChanged);
            // 
            // nmelbl
            // 
            this.nmelbl.AutoSize = true;
            this.nmelbl.Location = new System.Drawing.Point(46, 17);
            this.nmelbl.Name = "nmelbl";
            this.nmelbl.Size = new System.Drawing.Size(35, 13);
            this.nmelbl.TabIndex = 1;
            this.nmelbl.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Menu;
            this.txtName.Location = new System.Drawing.Point(87, 14);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.listBox1);
            this.panel1.Controls.Add(this.btndlt);
            this.panel1.Controls.Add(this.btnImage);
            this.panel1.Controls.Add(this.lblImage);
            this.panel1.Controls.Add(this.txtImage);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.nmelbl);
            this.panel1.Controls.Add(this.typlbl);
            this.panel1.Controls.Add(this.cmbDocType);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Location = new System.Drawing.Point(214, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 102);
            this.panel1.TabIndex = 9;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 70);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(93, 17);
            this.checkBox1.TabIndex = 229;
            this.checkBox1.Text = "Select Images";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(124, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(15, 4);
            this.listBox1.TabIndex = 228;
            this.listBox1.Visible = false;
            // 
            // btndlt
            // 
            this.btndlt.Location = new System.Drawing.Point(226, 66);
            this.btndlt.Name = "btndlt";
            this.btndlt.Size = new System.Drawing.Size(91, 23);
            this.btndlt.TabIndex = 227;
            this.btndlt.Text = "Delete";
            this.btndlt.UseVisualStyleBackColor = true;
            this.btndlt.Click += new System.EventHandler(this.btndlt_Click);
            // 
            // btnImage
            // 
            this.btnImage.Location = new System.Drawing.Point(108, 66);
            this.btnImage.Name = "btnImage";
            this.btnImage.Size = new System.Drawing.Size(100, 23);
            this.btnImage.TabIndex = 224;
            this.btnImage.Text = "Upload";
            this.btnImage.UseVisualStyleBackColor = true;
            this.btnImage.Click += new System.EventHandler(this.btnImage_Click);
            // 
            // lblImage
            // 
            this.lblImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.Color.Black;
            this.lblImage.Location = new System.Drawing.Point(87, 41);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(18, 12);
            this.lblImage.TabIndex = 226;
            this.lblImage.Text = "Scanned Image:";
            this.lblImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblImage.Visible = false;
            // 
            // txtImage
            // 
            this.txtImage.BackColor = System.Drawing.SystemColors.Window;
            this.txtImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtImage.Location = new System.Drawing.Point(108, 40);
            this.txtImage.Name = "txtImage";
            this.txtImage.ReadOnly = true;
            this.txtImage.Size = new System.Drawing.Size(10, 20);
            this.txtImage.TabIndex = 225;
            this.txtImage.Visible = false;
            this.txtImage.TextChanged += new System.EventHandler(this.txtImage_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(739, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(754, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 11;
            // 
            // ScannedDocuments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1117, 459);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScannedDocuments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ScannedDocuments";
            this.Load += new System.EventHandler(this.ScannedDocuments_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtMemberID;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label typlbl;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cmbDocType;
        private System.Windows.Forms.Label nmelbl;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btndlt;
        private System.Windows.Forms.Button btnImage;
        internal System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtImage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}