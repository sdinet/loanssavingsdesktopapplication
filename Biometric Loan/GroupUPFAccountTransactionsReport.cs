﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using SPARC;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using Microsoft.Reporting.WinForms;

namespace SPARC
{
    public partial class GroupUPFAccountTransactionsReport : Form
    {
        public GroupUPFAccountTransactionsReport()
        {
            InitializeComponent();
        }

        SqlDataReader dr;
        SqlCommand cmd;

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);

        private void GroupUPFAccountTransactionsReport_Load(object sender, EventArgs e)
        {
            dtpupf.MaxDate = DateTime.Now.Date;
            this.reportViewer1.RefreshReport();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }
                
                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;

                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }
            }
            this.reportViewer1.RefreshReport();
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

            private void btnsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("DataTable1");
                SqlDataReader dr;
                SqlCommand cmd;


                string cmdText = @"select GAT.Fk_GroupId as 'GroupId',  GA.Group_Name as 'GroupName',c.Name as 'SettlementName', GA.fk_slumId as 'fk_SlumId', SUM(GAT.Amount) as 'Savings', 0  as 'InterestPaid', 0 as 'Payout' from GroupUPFAccountTransactions GAT 
left join GroupInfo as GA on GAT.Fk_GroupId=GA.Id left join CodeValue c on GA.fk_slumId = c.Id
where Tran_type='CRE' and cast(GAT.TransactionDate as DATE) = cast(@TransDate as date) AND
    (dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId OR @fk_SlumId IS NULL)
AND (GAT.Fk_GroupId=@fk_GroupId  OR @fk_GroupId IS NULL)
group by GAT.Fk_GroupId, GA.Group_Name, GA.fk_slumId, c.Name

UNION 

select GAT.Fk_GroupId as 'GroupId',  GA.Group_Name as 'GroupName', c.Name as 'SettlementName', GA.fk_slumId as 'fk_SlumId', 0 as 'Savings', SUM(GAT.Amount)  as 'InterestPaid', 0 as 'Payout' from GroupUPFAccountTransactions GAT 
left join GroupInfo as GA on GAT.Fk_GroupId=GA.Id left join CodeValue c on GA.fk_slumId = c.Id 
where Tran_type='DEB' and GAT.Remarks = 'INT ' and cast(GAT.TransactionDate as DATE) = cast(@TransDate as date) AND
(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId OR @fk_SlumId IS NULL)
AND (GAT.Fk_GroupId=@fk_GroupId  OR @fk_GroupId IS NULL)
group by GAT.Fk_GroupId, GA.Group_Name, GA.fk_slumId, c.Name

UNION 

select GAT.Fk_GroupId as 'GroupId',  GA.Group_Name as 'GroupName', c.Name as 'SettlementName', GA.fk_slumId as 'fk_SlumId', 0 as 'Savings',  0  as 'InterestPaid', SUM(GAT.Amount) as 'Payout' from GroupUPFAccountTransactions GAT 
left join GroupInfo as GA on GAT.Fk_GroupId=GA.Id left join CodeValue c on GA.fk_slumId = c.Id
where Tran_type='DEB' and GAT.Remarks = 'Principal' and cast(GAT.TransactionDate as DATE) = cast(@TransDate as date) AND
(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId OR @fk_SlumId IS NULL)    
AND (GAT.Fk_GroupId=@fk_GroupId  OR @fk_GroupId IS NULL)
group by GAT.Fk_GroupId, GA.Group_Name, GA.fk_slumId, c.Name;";


                cmd = new SqlCommand(cmdText, con);
                cmd.Parameters.Add("@TransDate", dtpupf.Value);
                cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_GroupId", cmbGname.SelectedIndex > 0 ? Convert.ToInt32(cmbGname.SelectedValue.ToString()) : (object)DBNull.Value);
                //Convert.ToInt32(cmbSlum.SelectedValue));
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();
                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.Report11.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet11", dt));
                this.reportViewer1.RefreshReport();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
   
       }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }       

       

        private void cmbSlum_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillCombogroup(Convert.ToInt32(cmbSlum.SelectedValue));
                        
        }

       
        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();


            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbGname.DisplayMember = "Group_Name";
                cmbGname.ValueMember = "Id";
                cmbGname.DataSource = dsgroup.Tables[0];

                DataRow dr = dsgroup.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dsgroup.Tables[0].Rows.InsertAt(dr, 0);

            }
            else
            {
                cmbGname.DataSource = null;
                //cmbGname.SelectedValue="--Select--";
                DataRow dr = dsgroup.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            }
        }

        private void cmbDistrict_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }        
    }
}
