﻿namespace SPARC
{
    partial class frmCreateMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClear = new System.Windows.Forms.Button();
            this.lblContactPerson = new System.Windows.Forms.Label();
            this.txtContactPerson = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtContactPhone = new System.Windows.Forms.TextBox();
            this.lblContactPhone = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbGname = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.txtAdhaarID = new System.Windows.Forms.TextBox();
            this.txtVoterID = new System.Windows.Forms.TextBox();
            this.txtPassbook = new System.Windows.Forms.TextBox();
            this.lblAdhaar = new System.Windows.Forms.Label();
            this.lblVoterID = new System.Windows.Forms.Label();
            this.lblAdress1 = new System.Windows.Forms.Label();
            this.txtLandmark = new System.Windows.Forms.TextBox();
            this.lblPassbook = new System.Windows.Forms.Label();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.lblLandmark = new System.Windows.Forms.Label();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.txtZipcode = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.lblZip = new System.Windows.Forms.Label();
            this.lblTown = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.cmbSlum = new System.Windows.Forms.ComboBox();
            this.txtBiometricID = new System.Windows.Forms.TextBox();
            this.cmbDistrict = new System.Windows.Forms.ComboBox();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.lblBiometricID = new System.Windows.Forms.Label();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtHomePhone = new System.Windows.Forms.TextBox();
            this.lblHomephone = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.lblMobile = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.lblDOB = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.ofdPhoto = new System.Windows.Forms.OpenFileDialog();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblID = new System.Windows.Forms.Label();
            this.pnlMemberInfo = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.pnlPhoto = new System.Windows.Forms.Panel();
            this.cmbDocType = new System.Windows.Forms.ComboBox();
            this.lblDocType = new System.Windows.Forms.Label();
            this.scanedImage = new System.Windows.Forms.PictureBox();
            this.btnImage = new System.Windows.Forms.Button();
            this.lblImage = new System.Windows.Forms.Label();
            this.txtImage = new System.Windows.Forms.TextBox();
            this.btnVideoSource = new System.Windows.Forms.Button();
            this.btnCapture = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.rdbFileUpload = new System.Windows.Forms.RadioButton();
            this.rdbCameraClick = new System.Windows.Forms.RadioButton();
            this.lblPhotoType = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblPhotoUpload = new System.Windows.Forms.Label();
            this.txtFileUpload = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2.SuspendLayout();
            this.pnlMemberInfo.SuspendLayout();
            this.pnlPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scanedImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(455, 677);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(53, 23);
            this.btnClear.TabIndex = 33;
            this.btnClear.Text = "C&lear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click_1);
            // 
            // lblContactPerson
            // 
            this.lblContactPerson.AutoSize = true;
            this.lblContactPerson.Location = new System.Drawing.Point(11, 378);
            this.lblContactPerson.Name = "lblContactPerson";
            this.lblContactPerson.Size = new System.Drawing.Size(80, 13);
            this.lblContactPerson.TabIndex = 98;
            this.lblContactPerson.Text = "Contact Person";
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.Location = new System.Drawing.Point(107, 378);
            this.txtContactPerson.MaxLength = 50;
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.ShortcutsEnabled = false;
            this.txtContactPerson.Size = new System.Drawing.Size(298, 20);
            this.txtContactPerson.TabIndex = 27;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(107, 497);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(298, 59);
            this.txtRemarks.TabIndex = 29;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(11, 512);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 100;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtContactPhone
            // 
            this.txtContactPhone.Location = new System.Drawing.Point(107, 430);
            this.txtContactPhone.MaxLength = 15;
            this.txtContactPhone.Name = "txtContactPhone";
            this.txtContactPhone.ShortcutsEnabled = false;
            this.txtContactPhone.Size = new System.Drawing.Size(150, 20);
            this.txtContactPhone.TabIndex = 28;
            this.txtContactPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContactPhone_KeyPress);
            // 
            // lblContactPhone
            // 
            this.lblContactPhone.AutoSize = true;
            this.lblContactPhone.Location = new System.Drawing.Point(11, 430);
            this.lblContactPhone.Name = "lblContactPhone";
            this.lblContactPhone.Size = new System.Drawing.Size(88, 13);
            this.lblContactPhone.TabIndex = 99;
            this.lblContactPhone.Text = "Contact\'s Phone ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.cmbGname);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cmbHub);
            this.panel2.Controls.Add(this.txtAdhaarID);
            this.panel2.Controls.Add(this.txtVoterID);
            this.panel2.Controls.Add(this.txtPassbook);
            this.panel2.Controls.Add(this.lblAdhaar);
            this.panel2.Controls.Add(this.lblVoterID);
            this.panel2.Controls.Add(this.lblAdress1);
            this.panel2.Controls.Add(this.txtLandmark);
            this.panel2.Controls.Add(this.lblPassbook);
            this.panel2.Controls.Add(this.txtAddress1);
            this.panel2.Controls.Add(this.txtMemberID);
            this.panel2.Controls.Add(this.lblLandmark);
            this.panel2.Controls.Add(this.lblAddress2);
            this.panel2.Controls.Add(this.lblMemberID);
            this.panel2.Controls.Add(this.txtZipcode);
            this.panel2.Controls.Add(this.txtAddress2);
            this.panel2.Controls.Add(this.lblZip);
            this.panel2.Controls.Add(this.lblTown);
            this.panel2.Controls.Add(this.txtTown);
            this.panel2.Controls.Add(this.cmbSlum);
            this.panel2.Controls.Add(this.txtBiometricID);
            this.panel2.Controls.Add(this.cmbDistrict);
            this.panel2.Controls.Add(this.cmbState);
            this.panel2.Controls.Add(this.lblBiometricID);
            this.panel2.Controls.Add(this.cmbCountry);
            this.panel2.Controls.Add(this.lblCountry);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtEmail);
            this.panel2.Controls.Add(this.lblEmail);
            this.panel2.Controls.Add(this.txtHomePhone);
            this.panel2.Controls.Add(this.lblHomephone);
            this.panel2.Controls.Add(this.txtMobileNo);
            this.panel2.Controls.Add(this.lblMobile);
            this.panel2.Controls.Add(this.dtpDOB);
            this.panel2.Controls.Add(this.cmbGender);
            this.panel2.Controls.Add(this.lblDOB);
            this.panel2.Controls.Add(this.txtFatherName);
            this.panel2.Controls.Add(this.lblName);
            this.panel2.Controls.Add(this.lblGender);
            this.panel2.Controls.Add(this.lblFatherName);
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Location = new System.Drawing.Point(8, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(411, 609);
            this.panel2.TabIndex = 74;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // cmbGname
            // 
            this.cmbGname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGname.FormattingEnabled = true;
            this.cmbGname.Location = new System.Drawing.Point(96, 582);
            this.cmbGname.Name = "cmbGname";
            this.cmbGname.Size = new System.Drawing.Size(298, 21);
            this.cmbGname.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 585);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 129;
            this.label1.Text = "Group Name";
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(96, 211);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(198, 21);
            this.cmbHub.TabIndex = 9;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // txtAdhaarID
            // 
            this.txtAdhaarID.Location = new System.Drawing.Point(96, 503);
            this.txtAdhaarID.MaxLength = 16;
            this.txtAdhaarID.Name = "txtAdhaarID";
            this.txtAdhaarID.Size = new System.Drawing.Size(298, 20);
            this.txtAdhaarID.TabIndex = 20;
            // 
            // txtVoterID
            // 
            this.txtVoterID.Location = new System.Drawing.Point(96, 530);
            this.txtVoterID.MaxLength = 15;
            this.txtVoterID.Name = "txtVoterID";
            this.txtVoterID.Size = new System.Drawing.Size(298, 20);
            this.txtVoterID.TabIndex = 21;
            // 
            // txtPassbook
            // 
            this.txtPassbook.Location = new System.Drawing.Point(96, 6);
            this.txtPassbook.MaxLength = 20;
            this.txtPassbook.Name = "txtPassbook";
            this.txtPassbook.Size = new System.Drawing.Size(298, 20);
            this.txtPassbook.TabIndex = 1;
            // 
            // lblAdhaar
            // 
            this.lblAdhaar.AutoSize = true;
            this.lblAdhaar.Location = new System.Drawing.Point(1, 506);
            this.lblAdhaar.Name = "lblAdhaar";
            this.lblAdhaar.Size = new System.Drawing.Size(53, 13);
            this.lblAdhaar.TabIndex = 127;
            this.lblAdhaar.Text = "Aadhar Id";
            // 
            // lblVoterID
            // 
            this.lblVoterID.AutoSize = true;
            this.lblVoterID.Location = new System.Drawing.Point(3, 534);
            this.lblVoterID.Name = "lblVoterID";
            this.lblVoterID.Size = new System.Drawing.Size(60, 13);
            this.lblVoterID.TabIndex = 126;
            this.lblVoterID.Text = "National ID";
            // 
            // lblAdress1
            // 
            this.lblAdress1.AutoSize = true;
            this.lblAdress1.Location = new System.Drawing.Point(3, 348);
            this.lblAdress1.Name = "lblAdress1";
            this.lblAdress1.Size = new System.Drawing.Size(58, 13);
            this.lblAdress1.TabIndex = 117;
            this.lblAdress1.Text = "Address1 *";
            // 
            // txtLandmark
            // 
            this.txtLandmark.Location = new System.Drawing.Point(96, 452);
            this.txtLandmark.MaxLength = 100;
            this.txtLandmark.Name = "txtLandmark";
            this.txtLandmark.Size = new System.Drawing.Size(298, 20);
            this.txtLandmark.TabIndex = 18;
            // 
            // lblPassbook
            // 
            this.lblPassbook.AutoSize = true;
            this.lblPassbook.Location = new System.Drawing.Point(4, 6);
            this.lblPassbook.Name = "lblPassbook";
            this.lblPassbook.Size = new System.Drawing.Size(61, 13);
            this.lblPassbook.TabIndex = 124;
            this.lblPassbook.Text = "Passbook *";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(96, 348);
            this.txtAddress1.MaxLength = 500;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(298, 20);
            this.txtAddress1.TabIndex = 14;
            // 
            // txtMemberID
            // 
            this.txtMemberID.Enabled = false;
            this.txtMemberID.Location = new System.Drawing.Point(96, 556);
            this.txtMemberID.MaxLength = 15;
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(298, 20);
            this.txtMemberID.TabIndex = 22;
            // 
            // lblLandmark
            // 
            this.lblLandmark.AutoSize = true;
            this.lblLandmark.Location = new System.Drawing.Point(3, 452);
            this.lblLandmark.Name = "lblLandmark";
            this.lblLandmark.Size = new System.Drawing.Size(55, 13);
            this.lblLandmark.TabIndex = 121;
            this.lblLandmark.Text = "LandMark";
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(3, 374);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(58, 13);
            this.lblAddress2.TabIndex = 118;
            this.lblAddress2.Text = "Address2 *";
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(6, 559);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(59, 13);
            this.lblMemberID.TabIndex = 104;
            this.lblMemberID.Text = "Member ID";
            // 
            // txtZipcode
            // 
            this.txtZipcode.Location = new System.Drawing.Point(96, 426);
            this.txtZipcode.MaxLength = 15;
            this.txtZipcode.Name = "txtZipcode";
            this.txtZipcode.Size = new System.Drawing.Size(298, 20);
            this.txtZipcode.TabIndex = 17;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(96, 374);
            this.txtAddress2.MaxLength = 500;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(298, 20);
            this.txtAddress2.TabIndex = 15;
            // 
            // lblZip
            // 
            this.lblZip.AutoSize = true;
            this.lblZip.Location = new System.Drawing.Point(3, 426);
            this.lblZip.Name = "lblZip";
            this.lblZip.Size = new System.Drawing.Size(53, 13);
            this.lblZip.TabIndex = 120;
            this.lblZip.Text = "Zipcode *";
            // 
            // lblTown
            // 
            this.lblTown.AutoSize = true;
            this.lblTown.Location = new System.Drawing.Point(3, 400);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(56, 13);
            this.lblTown.TabIndex = 119;
            this.lblTown.Text = "City/Town";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(96, 400);
            this.txtTown.MaxLength = 30;
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(298, 20);
            this.txtTown.TabIndex = 16;
            // 
            // cmbSlum
            // 
            this.cmbSlum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSlum.FormattingEnabled = true;
            this.cmbSlum.Location = new System.Drawing.Point(96, 320);
            this.cmbSlum.Name = "cmbSlum";
            this.cmbSlum.Size = new System.Drawing.Size(198, 21);
            this.cmbSlum.TabIndex = 13;
            this.cmbSlum.SelectedIndexChanged += new System.EventHandler(this.cmbSlum_SelectedIndexChanged_1);
            // 
            // txtBiometricID
            // 
            this.txtBiometricID.Location = new System.Drawing.Point(96, 478);
            this.txtBiometricID.MaxLength = 25;
            this.txtBiometricID.Name = "txtBiometricID";
            this.txtBiometricID.Size = new System.Drawing.Size(298, 20);
            this.txtBiometricID.TabIndex = 19;
            // 
            // cmbDistrict
            // 
            this.cmbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrict.FormattingEnabled = true;
            this.cmbDistrict.Location = new System.Drawing.Point(96, 293);
            this.cmbDistrict.Name = "cmbDistrict";
            this.cmbDistrict.Size = new System.Drawing.Size(198, 21);
            this.cmbDistrict.TabIndex = 12;
            this.cmbDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbDistrict_SelectedIndexChanged_1);
            // 
            // cmbState
            // 
            this.cmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(96, 267);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(198, 21);
            this.cmbState.TabIndex = 11;
            this.cmbState.SelectedIndexChanged += new System.EventHandler(this.cmbState_SelectedIndexChanged_1);
            // 
            // lblBiometricID
            // 
            this.lblBiometricID.AutoSize = true;
            this.lblBiometricID.Location = new System.Drawing.Point(3, 478);
            this.lblBiometricID.Name = "lblBiometricID";
            this.lblBiometricID.Size = new System.Drawing.Size(65, 13);
            this.lblBiometricID.TabIndex = 101;
            this.lblBiometricID.Text = "BioMetric ID";
            // 
            // cmbCountry
            // 
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(96, 238);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(198, 21);
            this.cmbCountry.TabIndex = 10;
            this.cmbCountry.SelectedIndexChanged += new System.EventHandler(this.cmbCountry_SelectedIndexChanged_1);
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(2, 241);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(43, 13);
            this.lblCountry.TabIndex = 112;
            this.lblCountry.Text = "Country";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 116;
            this.label2.Text = "Slum/Settlement *";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 113;
            this.label3.Text = "State/Province";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 115;
            this.label4.Text = "Hub";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 293);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 114;
            this.label5.Text = "District/City";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(96, 186);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(298, 20);
            this.txtEmail.TabIndex = 8;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(4, 189);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 101;
            this.lblEmail.Text = "Email";
            // 
            // txtHomePhone
            // 
            this.txtHomePhone.Location = new System.Drawing.Point(96, 160);
            this.txtHomePhone.MaxLength = 15;
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.ShortcutsEnabled = false;
            this.txtHomePhone.Size = new System.Drawing.Size(128, 20);
            this.txtHomePhone.TabIndex = 7;
            this.txtHomePhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHomePhone_KeyPress);
            // 
            // lblHomephone
            // 
            this.lblHomephone.AutoSize = true;
            this.lblHomephone.Location = new System.Drawing.Point(4, 163);
            this.lblHomephone.Name = "lblHomephone";
            this.lblHomephone.Size = new System.Drawing.Size(69, 13);
            this.lblHomephone.TabIndex = 97;
            this.lblHomephone.Text = "Home Phone";
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Location = new System.Drawing.Point(96, 134);
            this.txtMobileNo.MaxLength = 15;
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.ShortcutsEnabled = false;
            this.txtMobileNo.Size = new System.Drawing.Size(128, 20);
            this.txtMobileNo.TabIndex = 6;
            this.txtMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobileNo_KeyPress);
            // 
            // lblMobile
            // 
            this.lblMobile.AutoSize = true;
            this.lblMobile.Location = new System.Drawing.Point(4, 137);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(55, 13);
            this.lblMobile.TabIndex = 95;
            this.lblMobile.Text = "Mobile No";
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Location = new System.Drawing.Point(96, 106);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(105, 20);
            this.dtpDOB.TabIndex = 5;
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged_1);
            // 
            // cmbGender
            // 
            this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(96, 79);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(128, 21);
            this.cmbGender.TabIndex = 4;
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(4, 109);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(66, 13);
            this.lblDOB.TabIndex = 70;
            this.lblDOB.Text = "Date of Birth";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(96, 53);
            this.txtFatherName.MaxLength = 50;
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(298, 20);
            this.txtFatherName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(4, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(42, 13);
            this.lblName.TabIndex = 58;
            this.lblName.Text = "Name *";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(4, 85);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(49, 13);
            this.lblGender.TabIndex = 59;
            this.lblGender.Text = "Gender *";
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Location = new System.Drawing.Point(4, 55);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(75, 13);
            this.lblFatherName.TabIndex = 66;
            this.lblFatherName.Text = "Father Name *";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(96, 27);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(298, 20);
            this.txtName.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(376, 677);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(58, 23);
            this.btnSave.TabIndex = 32;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(607, 677);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(53, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(128, 594);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(0, 13);
            this.lblID.TabIndex = 102;
            this.lblID.Visible = false;
            // 
            // pnlMemberInfo
            // 
            this.pnlMemberInfo.AutoScroll = true;
            this.pnlMemberInfo.Controls.Add(this.lblMessage);
            this.pnlMemberInfo.Controls.Add(this.pnlPhoto);
            this.pnlMemberInfo.Controls.Add(this.panel2);
            this.pnlMemberInfo.Controls.Add(this.btnClear);
            this.pnlMemberInfo.Controls.Add(this.btnDelete);
            this.pnlMemberInfo.Controls.Add(this.btnSave);
            this.pnlMemberInfo.Controls.Add(this.btnClose);
            this.pnlMemberInfo.Location = new System.Drawing.Point(9, -5);
            this.pnlMemberInfo.Name = "pnlMemberInfo";
            this.pnlMemberInfo.Size = new System.Drawing.Size(972, 703);
            this.pnlMemberInfo.TabIndex = 1;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(7, 656);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 82;
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblMessage.Click += new System.EventHandler(this.lblMessage_Click);
            // 
            // pnlPhoto
            // 
            this.pnlPhoto.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPhoto.Controls.Add(this.cmbDocType);
            this.pnlPhoto.Controls.Add(this.lblDocType);
            this.pnlPhoto.Controls.Add(this.scanedImage);
            this.pnlPhoto.Controls.Add(this.btnImage);
            this.pnlPhoto.Controls.Add(this.lblImage);
            this.pnlPhoto.Controls.Add(this.txtImage);
            this.pnlPhoto.Controls.Add(this.btnVideoSource);
            this.pnlPhoto.Controls.Add(this.btnCapture);
            this.pnlPhoto.Controls.Add(this.btnContinue);
            this.pnlPhoto.Controls.Add(this.btnStop);
            this.pnlPhoto.Controls.Add(this.btnStart);
            this.pnlPhoto.Controls.Add(this.imgCapture);
            this.pnlPhoto.Controls.Add(this.imgVideo);
            this.pnlPhoto.Controls.Add(this.rdbFileUpload);
            this.pnlPhoto.Controls.Add(this.rdbCameraClick);
            this.pnlPhoto.Controls.Add(this.lblPhotoType);
            this.pnlPhoto.Controls.Add(this.btnBrowse);
            this.pnlPhoto.Controls.Add(this.lblPhotoUpload);
            this.pnlPhoto.Controls.Add(this.txtFileUpload);
            this.pnlPhoto.Controls.Add(this.lblID);
            this.pnlPhoto.Controls.Add(this.lblContactPerson);
            this.pnlPhoto.Controls.Add(this.txtContactPerson);
            this.pnlPhoto.Controls.Add(this.txtRemarks);
            this.pnlPhoto.Controls.Add(this.lblRemarks);
            this.pnlPhoto.Controls.Add(this.txtContactPhone);
            this.pnlPhoto.Controls.Add(this.lblContactPhone);
            this.pnlPhoto.Location = new System.Drawing.Point(439, 40);
            this.pnlPhoto.Name = "pnlPhoto";
            this.pnlPhoto.Size = new System.Drawing.Size(420, 613);
            this.pnlPhoto.TabIndex = 76;
            this.pnlPhoto.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPhoto_Paint);
            // 
            // cmbDocType
            // 
            this.cmbDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocType.FormattingEnabled = true;
            this.cmbDocType.Location = new System.Drawing.Point(32, 589);
            this.cmbDocType.Name = "cmbDocType";
            this.cmbDocType.Size = new System.Drawing.Size(10, 21);
            this.cmbDocType.TabIndex = 30;
            this.cmbDocType.Visible = false;
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.Location = new System.Drawing.Point(305, 597);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(0, 13);
            this.lblDocType.TabIndex = 223;
            // 
            // scanedImage
            // 
            this.scanedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scanedImage.Location = new System.Drawing.Point(16, 597);
            this.scanedImage.Name = "scanedImage";
            this.scanedImage.Size = new System.Drawing.Size(10, 10);
            this.scanedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.scanedImage.TabIndex = 222;
            this.scanedImage.TabStop = false;
            this.scanedImage.Visible = false;
            this.scanedImage.MouseClick += new System.Windows.Forms.MouseEventHandler(this.scanedImage_MouseClick);
            // 
            // btnImage
            // 
            this.btnImage.Location = new System.Drawing.Point(59, 595);
            this.btnImage.Name = "btnImage";
            this.btnImage.Size = new System.Drawing.Size(10, 10);
            this.btnImage.TabIndex = 31;
            this.btnImage.Text = "Upload";
            this.btnImage.UseVisualStyleBackColor = true;
            this.btnImage.Visible = false;
            this.btnImage.Click += new System.EventHandler(this.btnImage_Click);
            // 
            // lblImage
            // 
            this.lblImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.Color.Black;
            this.lblImage.Location = new System.Drawing.Point(75, 594);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(10, 10);
            this.lblImage.TabIndex = 221;
            this.lblImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtImage
            // 
            this.txtImage.BackColor = System.Drawing.SystemColors.Window;
            this.txtImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtImage.Location = new System.Drawing.Point(48, 589);
            this.txtImage.Name = "txtImage";
            this.txtImage.ReadOnly = true;
            this.txtImage.Size = new System.Drawing.Size(10, 20);
            this.txtImage.TabIndex = 31;
            this.txtImage.Visible = false;
            // 
            // btnVideoSource
            // 
            this.btnVideoSource.Location = new System.Drawing.Point(308, 188);
            this.btnVideoSource.Name = "btnVideoSource";
            this.btnVideoSource.Size = new System.Drawing.Size(97, 23);
            this.btnVideoSource.TabIndex = 40;
            this.btnVideoSource.Text = "Video Source";
            this.btnVideoSource.UseVisualStyleBackColor = true;
            this.btnVideoSource.Click += new System.EventHandler(this.btnVideoSource_Click_1);
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(234, 188);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(68, 23);
            this.btnCapture.TabIndex = 39;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(117, 189);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(68, 23);
            this.btnContinue.TabIndex = 38;
            this.btnContinue.Text = "Continue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click_1);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(63, 188);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(48, 23);
            this.btnStop.TabIndex = 37;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click_1);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(14, 188);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(42, 23);
            this.btnStart.TabIndex = 36;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click_1);
            // 
            // imgCapture
            // 
            this.imgCapture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgCapture.Location = new System.Drawing.Point(234, 6);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(171, 164);
            this.imgCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCapture.TabIndex = 112;
            this.imgCapture.TabStop = false;
            // 
            // imgVideo
            // 
            this.imgVideo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgVideo.Location = new System.Drawing.Point(14, 7);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(171, 164);
            this.imgVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgVideo.TabIndex = 111;
            this.imgVideo.TabStop = false;
            this.imgVideo.Click += new System.EventHandler(this.imgVideo_Click);
            // 
            // rdbFileUpload
            // 
            this.rdbFileUpload.AutoSize = true;
            this.rdbFileUpload.Location = new System.Drawing.Point(204, 248);
            this.rdbFileUpload.Name = "rdbFileUpload";
            this.rdbFileUpload.Size = new System.Drawing.Size(75, 17);
            this.rdbFileUpload.TabIndex = 25;
            this.rdbFileUpload.TabStop = true;
            this.rdbFileUpload.Text = "FileUpload";
            this.rdbFileUpload.UseVisualStyleBackColor = true;
            this.rdbFileUpload.CheckedChanged += new System.EventHandler(this.rdbFileUpload_CheckedChanged_1);
            // 
            // rdbCameraClick
            // 
            this.rdbCameraClick.AutoSize = true;
            this.rdbCameraClick.Location = new System.Drawing.Point(107, 248);
            this.rdbCameraClick.Name = "rdbCameraClick";
            this.rdbCameraClick.Size = new System.Drawing.Size(86, 17);
            this.rdbCameraClick.TabIndex = 24;
            this.rdbCameraClick.TabStop = true;
            this.rdbCameraClick.Text = "Camera click";
            this.rdbCameraClick.UseVisualStyleBackColor = true;
            this.rdbCameraClick.CheckedChanged += new System.EventHandler(this.rdbCameraClick_CheckedChanged_1);
            // 
            // lblPhotoType
            // 
            this.lblPhotoType.AutoSize = true;
            this.lblPhotoType.Location = new System.Drawing.Point(11, 250);
            this.lblPhotoType.Name = "lblPhotoType";
            this.lblPhotoType.Size = new System.Drawing.Size(58, 13);
            this.lblPhotoType.TabIndex = 108;
            this.lblPhotoType.Text = "Photo type";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(308, 342);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 26;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click_1);
            // 
            // lblPhotoUpload
            // 
            this.lblPhotoUpload.AutoSize = true;
            this.lblPhotoUpload.Location = new System.Drawing.Point(11, 307);
            this.lblPhotoUpload.Name = "lblPhotoUpload";
            this.lblPhotoUpload.Size = new System.Drawing.Size(81, 13);
            this.lblPhotoUpload.TabIndex = 106;
            this.lblPhotoUpload.Text = "Upload a Photo";
            // 
            // txtFileUpload
            // 
            this.txtFileUpload.Location = new System.Drawing.Point(107, 307);
            this.txtFileUpload.MaxLength = 50;
            this.txtFileUpload.Name = "txtFileUpload";
            this.txtFileUpload.Size = new System.Drawing.Size(298, 20);
            this.txtFileUpload.TabIndex = 23;
            this.txtFileUpload.TextChanged += new System.EventHandler(this.txtFileUpload_TextChanged);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(529, 677);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(56, 23);
            this.btnClose.TabIndex = 34;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmCreateMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(993, 710);
            this.ControlBox = false;
            this.Controls.Add(this.pnlMemberInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmCreateMember";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CreateMember_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlMemberInfo.ResumeLayout(false);
            this.pnlMemberInfo.PerformLayout();
            this.pnlPhoto.ResumeLayout(false);
            this.pnlPhoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scanedImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        //private System.Windows.Forms.PictureBox newHouse;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblContactPerson;
        private System.Windows.Forms.TextBox txtContactPerson;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.TextBox txtContactPhone;
        private System.Windows.Forms.Label lblContactPhone;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtAdhaarID;
        private System.Windows.Forms.TextBox txtVoterID;
        private System.Windows.Forms.TextBox txtPassbook;
        private System.Windows.Forms.Label lblAdhaar;
        private System.Windows.Forms.Label lblVoterID;
        private System.Windows.Forms.Label lblAdress1;
        private System.Windows.Forms.TextBox txtLandmark;
        private System.Windows.Forms.Label lblPassbook;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtMemberID;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label lblLandmark;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.TextBox txtZipcode;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label lblZip;
        private System.Windows.Forms.Label lblTown;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.ComboBox cmbSlum;
        private System.Windows.Forms.TextBox txtBiometricID;
        private System.Windows.Forms.ComboBox cmbDistrict;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.Label lblBiometricID;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtHomePhone;
        private System.Windows.Forms.Label lblHomephone;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.ComboBox cmbGender;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog ofdPhoto;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Panel pnlMemberInfo;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel pnlPhoto;
        private System.Windows.Forms.Button btnVideoSource;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.RadioButton rdbFileUpload;
        private System.Windows.Forms.RadioButton rdbCameraClick;
        private System.Windows.Forms.Label lblPhotoType;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblPhotoUpload;
        private System.Windows.Forms.TextBox txtFileUpload;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnImage;
        internal System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.TextBox txtImage;
        private System.Windows.Forms.PictureBox scanedImage;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.ComboBox cmbDocType;
        private System.Windows.Forms.ComboBox cmbGname;
        private System.Windows.Forms.Label label1;

    }
}

