﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using Microsoft.Reporting.WinForms;

namespace SPARC
{
    public partial class NonActiveSavingsAccountReport : Form
    {
        public NonActiveSavingsAccountReport()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        private void NonActiveSavingsAccountReport_Load(object sender, EventArgs e)
        {
            //FillCombos(Convert.ToInt32(LocationCode.Country), 0, cmbCountry);
            this.reportViewer1.RefreshReport();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();

            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }

            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbCountry_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }
        
        private void cmbState_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
            //try
            //{

            //    //Fetch District
            //    if (cmbState.SelectedIndex > 0)
            //    {
            //        //string myString = ((ComboBoxItem)cmbState.SelectedItem).Content.ToString();
            //       // string myString = cmbState.SelectedValue.Text.ToString();
            //        string myString = cmbState.Text;
            //        if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && GlobalValues.User_FullName == cmbState.Text || GlobalValues.User_FullName == "Administrator")



            //            FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            //        else
            //            cmbDistrict.DataSource = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            //}

        }

        private void cmbDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch Taluk
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
            //try
            //{
            //    //Fetch Taluk
            //    if (cmbDistrict.SelectedIndex > 0)
            //        FillCombos(Convert.ToInt32(LocationCode.Taluk), Convert.ToInt32(cmbDistrict.SelectedValue), cmbTaluk);
            //    else
            //        cmbTaluk.DataSource = null;
            //}
            //catch (Exception ex)
            //{
            //}
        }        

        private void btnSubmit_Click(object sender, System.EventArgs e)
        {
             if (cmbHub.SelectedIndex > 1)
            {
            this.reportViewer1.Reset();
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("NonActive1");

                SqlCommand cmd;
                SqlDataReader dr;
                string cmdText = @"(select m.Name,m.PassbookNumber,c.Name as Slum_Name,s.AccountNumber as Savings_Account_Number,s.Opendate as Account_Open_Date,
                                    MAX(st.TransactionDate) as Last_Transaction_Date,s.CurrentBalance,
                                    (select d.Name from CodeValue c, CodeValue d where d.Id=c.fk_ParentId and c.Id=m.fk_SlumId) as DistrictName
                                    from Members M
                                    inner join SavingAccount S
                                    on m.Id = s.fk_MemberId
                                    inner join SavingAccountTransactions ST
                                    on m.Id = ST.fk_MemberId
                                    inner join CodeValue C
                                    on m.fk_SlumId = C.Id and
(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)
                                    where TransactionDate <= DATEDIFF(day, 90, GETDATE()) and c.Id=m.fk_SlumId
                                    group by m.Name,m.PassbookNumber,s.AccountNumber,s.Opendate,s.CurrentBalance,c.Name,m.fk_SlumId)    
                                    order by Last_Transaction_Date desc ;";
                    




//@"(select m.Name,c.Name as Slum_Name,s.AccountNumber as Savings_Account_Number,s.Opendate as Account_Open_Date,
//                                    MAX(st.TransactionDate) as Last_Transaction_Date,s.CurrentBalance
//                                    from Members M
//                                    inner join SavingAccount S
//                                    on m.Id = s.fk_MemberId
//                                    inner join SavingAccountTransactions ST
//                                    on m.Id = ST.fk_MemberId
//                                    inner join CodeValue C
//                                    on m.fk_SlumId = C.Id
//                                    where TransactionDate <= DATEDIFF(day, 90, GETDATE()) and c.Id=@value
//                                    group by m.Name,s.AccountNumber,s.Opendate,s.CurrentBalance,c.Name)
//                                    order by Last_Transaction_Date desc ;";

                cmd = new SqlCommand(cmdText, con);
               // cmd.Parameters.Add("@value", Convert.ToInt32(cmbSlum.SelectedValue));
                cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.NonActiveSavingAccountReport.rdlc";


                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", dt));

                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
            }
             else
             {
                 this.reportViewer1.RefreshReport();
             }
            
        }

        private void cmbHub_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }
    }
}
