﻿namespace SPARC
{
    partial class frmSavingsAccTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblPassbook = new System.Windows.Forms.Label();
            this.lblPassbookNo = new System.Windows.Forms.Label();
            this.cmbSavingType = new System.Windows.Forms.ComboBox();
            this.lblSavingType = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.dtpOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransDate = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblMessages = new System.Windows.Forms.Label();
            this.dgSavingsAccTrans = new System.Windows.Forms.DataGridView();
            this.lblInfo = new System.Windows.Forms.Label();
            this.Savingtype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SavingsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSavingsAccTrans)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.lblMessages);
            this.panel1.Location = new System.Drawing.Point(2, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 297);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblPassbook);
            this.groupBox1.Controls.Add(this.lblPassbookNo);
            this.groupBox1.Controls.Add(this.cmbSavingType);
            this.groupBox1.Controls.Add(this.lblSavingType);
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.lblRemarks);
            this.groupBox1.Controls.Add(this.dtpOpenDate);
            this.groupBox1.Controls.Add(this.lblTransDate);
            this.groupBox1.Controls.Add(this.cmbAgentName);
            this.groupBox1.Controls.Add(this.lblAgentName);
            this.groupBox1.Controls.Add(this.lblAccValue);
            this.groupBox1.Controls.Add(this.lblAccountID);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Location = new System.Drawing.Point(235, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 267);
            this.groupBox1.TabIndex = 98;
            this.groupBox1.TabStop = false;
            // 
            // lblPassbook
            // 
            this.lblPassbook.AutoSize = true;
            this.lblPassbook.Location = new System.Drawing.Point(175, 68);
            this.lblPassbook.Name = "lblPassbook";
            this.lblPassbook.Size = new System.Drawing.Size(19, 13);
            this.lblPassbook.TabIndex = 97;
            this.lblPassbook.Text = "<>";
            // 
            // lblPassbookNo
            // 
            this.lblPassbookNo.AutoSize = true;
            this.lblPassbookNo.Location = new System.Drawing.Point(46, 68);
            this.lblPassbookNo.Name = "lblPassbookNo";
            this.lblPassbookNo.Size = new System.Drawing.Size(74, 13);
            this.lblPassbookNo.TabIndex = 96;
            this.lblPassbookNo.Text = "Passbook No.";
            // 
            // cmbSavingType
            // 
            this.cmbSavingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSavingType.FormattingEnabled = true;
            this.cmbSavingType.Location = new System.Drawing.Point(178, 19);
            this.cmbSavingType.Name = "cmbSavingType";
            this.cmbSavingType.Size = new System.Drawing.Size(157, 21);
            this.cmbSavingType.TabIndex = 1;
            this.cmbSavingType.SelectedIndexChanged += new System.EventHandler(this.cmbSavingType_SelectedIndexChanged);
            // 
            // lblSavingType
            // 
            this.lblSavingType.AutoSize = true;
            this.lblSavingType.Location = new System.Drawing.Point(46, 22);
            this.lblSavingType.Name = "lblSavingType";
            this.lblSavingType.Size = new System.Drawing.Size(67, 13);
            this.lblSavingType.TabIndex = 94;
            this.lblSavingType.Text = "Saving Type";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(178, 237);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 7;
            this.btnNew.Text = "&New Entry";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(178, 142);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 4;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(259, 237);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(97, 237);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(178, 168);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(157, 59);
            this.txtRemarks.TabIndex = 5;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(46, 189);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 93;
            this.lblRemarks.Text = "Remarks";
            // 
            // dtpOpenDate
            // 
            this.dtpOpenDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenDate.Location = new System.Drawing.Point(178, 89);
            this.dtpOpenDate.Name = "dtpOpenDate";
            this.dtpOpenDate.Size = new System.Drawing.Size(157, 20);
            this.dtpOpenDate.TabIndex = 2;
            // 
            // lblTransDate
            // 
            this.lblTransDate.AutoSize = true;
            this.lblTransDate.Location = new System.Drawing.Point(46, 89);
            this.lblTransDate.Name = "lblTransDate";
            this.lblTransDate.Size = new System.Drawing.Size(89, 13);
            this.lblTransDate.TabIndex = 90;
            this.lblTransDate.Text = "Transaction Date";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(178, 115);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(157, 21);
            this.cmbAgentName.TabIndex = 3;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(46, 116);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(78, 13);
            this.lblAgentName.TabIndex = 88;
            this.lblAgentName.Text = "Leader Name *";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(175, 47);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(19, 13);
            this.lblAccValue.TabIndex = 87;
            this.lblAccValue.Text = "<>";
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(46, 47);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 86;
            this.lblAccountID.Text = "Account ID";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(46, 143);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(65, 13);
            this.lblAmount.TabIndex = 84;
            this.lblAmount.Text = "Amount (Rs)";
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(10, 278);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 97;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgSavingsAccTrans
            // 
            this.dgSavingsAccTrans.AllowUserToAddRows = false;
            this.dgSavingsAccTrans.AllowUserToDeleteRows = false;
            this.dgSavingsAccTrans.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSavingsAccTrans.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSavingsAccTrans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSavingsAccTrans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.AgentID,
            this.TransDate,
            this.AgentName,
            this.Amount,
            this.Remarks,
            this.btnDelete,
            this.SavingsId,
            this.Savingtype});
            this.dgSavingsAccTrans.Location = new System.Drawing.Point(6, 322);
            this.dgSavingsAccTrans.Margin = new System.Windows.Forms.Padding(0);
            this.dgSavingsAccTrans.Name = "dgSavingsAccTrans";
            this.dgSavingsAccTrans.ReadOnly = true;
            this.dgSavingsAccTrans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSavingsAccTrans.Size = new System.Drawing.Size(892, 270);
            this.dgSavingsAccTrans.TabIndex = 99;
            this.dgSavingsAccTrans.TabStop = false;
            this.dgSavingsAccTrans.VirtualMode = true;
            this.dgSavingsAccTrans.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSavingsDeposit_CellClick);
            this.dgSavingsAccTrans.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSavingsDeposit_CellDoubleClick);
            this.dgSavingsAccTrans.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgSavingsAccTrans_DataBindingComplete);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(5, 304);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(142, 13);
            this.lblInfo.TabIndex = 100;
            this.lblInfo.Text = "Double click on a row to edit";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Savingtype
            // 
            this.Savingtype.DataPropertyName = "fk_Savingtype";
            this.Savingtype.HeaderText = "Savingtype";
            this.Savingtype.Name = "Savingtype";
            this.Savingtype.ReadOnly = true;
            this.Savingtype.Visible = false;
            // 
            // SavingsId
            // 
            this.SavingsId.DataPropertyName = "Id";
            this.SavingsId.HeaderText = "SavingsId";
            this.SavingsId.Name = "SavingsId";
            this.SavingsId.ReadOnly = true;
            this.SavingsId.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            this.Remarks.Visible = false;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.Amount.FillWeight = 170.4545F;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // AgentName
            // 
            this.AgentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AgentName.DataPropertyName = "AgentName";
            dataGridViewCellStyle2.Format = "dd/month/yyyy";
            this.AgentName.DefaultCellStyle = dataGridViewCellStyle2;
            this.AgentName.FillWeight = 64.77273F;
            this.AgentName.HeaderText = "Leader Name";
            this.AgentName.Name = "AgentName";
            this.AgentName.ReadOnly = true;
            // 
            // TransDate
            // 
            this.TransDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TransDate.DataPropertyName = "TransactionDate";
            dataGridViewCellStyle1.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.TransDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.TransDate.FillWeight = 64.77273F;
            this.TransDate.HeaderText = "Transaction Date";
            this.TransDate.Name = "TransDate";
            this.TransDate.ReadOnly = true;
            // 
            // AgentID
            // 
            this.AgentID.DataPropertyName = "fk_AgentId";
            this.AgentID.HeaderText = "AgentID";
            this.AgentID.Name = "AgentID";
            this.AgentID.ReadOnly = true;
            this.AgentID.Visible = false;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "SavTransID";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // frmSavingsAccTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(906, 601);
            this.ControlBox = false;
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.dgSavingsAccTrans);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSavingsAccTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.SavingsAccDepositTrans_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSavingsAccTrans)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpOpenDate;
        private System.Windows.Forms.Label lblTransDate;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.TextBox txtAmount;
        public System.Windows.Forms.DataGridView dgSavingsAccTrans;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbSavingType;
        private System.Windows.Forms.Label lblSavingType;
        private System.Windows.Forms.Label lblPassbookNo;
        private System.Windows.Forms.Label lblPassbook;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn SavingsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Savingtype;
    }
}