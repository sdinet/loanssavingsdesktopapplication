﻿namespace SPARC
{
    partial class CreateMemberFamily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbDistrict = new System.Windows.Forms.ComboBox();
            this.ofdPhoto = new System.Windows.Forms.OpenFileDialog();
            this.upcomingEMIReportDataSet = new SPARC.UpcomingEMIReportDataSet();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbSlum = new System.Windows.Forms.ComboBox();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.AddFamilyMessages = new System.Windows.Forms.Label();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.lblFamilyMessages = new System.Windows.Forms.Label();
            this.btnVideoSource = new System.Windows.Forms.Button();
            this.btnCapture = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFileUpload = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.rdbFileUpload = new System.Windows.Forms.RadioButton();
            this.rdbCameraClick = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.richtxtHouseDetails = new System.Windows.Forms.RichTextBox();
            this.richtxtHouseBisiness = new System.Windows.Forms.RichTextBox();
            this.redioBtnNo = new System.Windows.Forms.RadioButton();
            this.redioBtnYes = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtElectricity = new System.Windows.Forms.TextBox();
            this.txtToilte = new System.Windows.Forms.TextBox();
            this.txtSate = new System.Windows.Forms.TextBox();
            this.txtMotherTng = new System.Windows.Forms.TextBox();
            this.txtCookingMedium = new System.Windows.Forms.Label();
            this.txtEletricity = new System.Windows.Forms.Label();
            this.txtToilet = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.Label();
            this.txtMotherTongue = new System.Windows.Forms.Label();
            this.txtFamilyIncome = new System.Windows.Forms.TextBox();
            this.txtCast = new System.Windows.Forms.TextBox();
            this.txtHusWifeNm = new System.Windows.Forms.TextBox();
            this.txtFamilyHeadName = new System.Windows.Forms.TextBox();
            this.txtFamilyStay = new System.Windows.Forms.TextBox();
            this.txtRationNo = new System.Windows.Forms.TextBox();
            this.txtHouseNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.newLoanDataSet = new SPARC.NewLoanDataSet();
            this.memberFamilyDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memberFamilyDetailsTableAdapter = new SPARC.NewLoanDataSetTableAdapters.MemberFamilyDetailsTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qualifiactionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isMemberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MemberID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fkMembersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MembersId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.InsertButton = new System.Windows.Forms.Button();
            this.UpdatedataGrid = new System.Windows.Forms.Button();
            this.DeleteGrid = new System.Windows.Forms.Button();
            this.CookMediumList = new System.Windows.Forms.ComboBox();
            this.WaterList = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.GenderList = new System.Windows.Forms.ComboBox();
            this.FMGender = new System.Windows.Forms.Label();
            this.FMDetailsNm = new System.Windows.Forms.TextBox();
            this.FMNm = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.FMAge = new System.Windows.Forms.Label();
            this.CheckBoxFMIsMember = new System.Windows.Forms.CheckBox();
            this.FMQualifiaction = new System.Windows.Forms.Label();
            this.FMMem = new System.Windows.Forms.Label();
            this.FMID = new System.Windows.Forms.Label();
            this.FMDetailsAge = new System.Windows.Forms.TextBox();
            this.FMDetailsQualifiaction = new System.Windows.Forms.TextBox();
            this.FMDetailsMemberID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.upcomingEMIReportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newLoanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbDistrict
            // 
            this.cmbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrict.FormattingEnabled = true;
            this.cmbDistrict.Location = new System.Drawing.Point(856, 6);
            this.cmbDistrict.Name = "cmbDistrict";
            this.cmbDistrict.Size = new System.Drawing.Size(154, 21);
            this.cmbDistrict.TabIndex = 4;
            this.cmbDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbDistrict_SelectedIndexChanged_1);
            // 
            // ofdPhoto
            // 
            this.ofdPhoto.FileName = "openFileDialog1";
            // 
            // upcomingEMIReportDataSet
            // 
            this.upcomingEMIReportDataSet.DataSetName = "UpcomingEMIReportDataSet";
            this.upcomingEMIReportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(772, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 149;
            this.label15.Text = "District/City";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(466, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 148;
            this.label14.Text = "State/Province";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(242, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 147;
            this.label12.Text = "Country";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(42, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 146;
            this.label11.Text = "Hub";
            // 
            // cmbSlum
            // 
            this.cmbSlum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSlum.FormattingEnabled = true;
            this.cmbSlum.Location = new System.Drawing.Point(246, 36);
            this.cmbSlum.Name = "cmbSlum";
            this.cmbSlum.Size = new System.Drawing.Size(179, 21);
            this.cmbSlum.TabIndex = 5;
            // 
            // cmbState
            // 
            this.cmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(577, 6);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(166, 21);
            this.cmbState.TabIndex = 3;
            this.cmbState.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged_1);
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(86, 6);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(142, 21);
            this.cmbHub.TabIndex = 1;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // AddFamilyMessages
            // 
            this.AddFamilyMessages.AutoSize = true;
            this.AddFamilyMessages.Location = new System.Drawing.Point(39, 718);
            this.AddFamilyMessages.Name = "AddFamilyMessages";
            this.AddFamilyMessages.Size = new System.Drawing.Size(0, 13);
            this.AddFamilyMessages.TabIndex = 141;
            // 
            // imgCapture
            // 
            this.imgCapture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgCapture.ErrorImage = null;
            this.imgCapture.Location = new System.Drawing.Point(704, 33);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(181, 169);
            this.imgCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCapture.TabIndex = 126;
            this.imgCapture.TabStop = false;
            // 
            // lblFamilyMessages
            // 
            this.lblFamilyMessages.AutoSize = true;
            this.lblFamilyMessages.Location = new System.Drawing.Point(490, 641);
            this.lblFamilyMessages.Name = "lblFamilyMessages";
            this.lblFamilyMessages.Size = new System.Drawing.Size(0, 13);
            this.lblFamilyMessages.TabIndex = 125;
            // 
            // btnVideoSource
            // 
            this.btnVideoSource.Location = new System.Drawing.Point(785, 208);
            this.btnVideoSource.Name = "btnVideoSource";
            this.btnVideoSource.Size = new System.Drawing.Size(122, 23);
            this.btnVideoSource.TabIndex = 23;
            this.btnVideoSource.Text = "Video Source";
            this.btnVideoSource.UseVisualStyleBackColor = true;
            this.btnVideoSource.Click += new System.EventHandler(this.btnVideoSource_Click_1);
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(704, 208);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(75, 23);
            this.btnCapture.TabIndex = 22;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click_1);
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(585, 208);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(75, 23);
            this.btnContinue.TabIndex = 21;
            this.btnContinue.Text = "Continue";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click_1);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(532, 208);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(47, 23);
            this.btnStop.TabIndex = 20;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click_1);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(473, 208);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(53, 23);
            this.btnStart.TabIndex = 19;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click_1);
            // 
            // imgVideo
            // 
            this.imgVideo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgVideo.Location = new System.Drawing.Point(467, 36);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(181, 166);
            this.imgVideo.TabIndex = 119;
            this.imgVideo.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(703, 277);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(115, 24);
            this.btnBrowse.TabIndex = 27;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click_1);
            // 
            // txtFileUpload
            // 
            this.txtFileUpload.Location = new System.Drawing.Point(614, 280);
            this.txtFileUpload.Multiline = true;
            this.txtFileUpload.Name = "txtFileUpload";
            this.txtFileUpload.Size = new System.Drawing.Size(65, 20);
            this.txtFileUpload.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(470, 284);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 116;
            this.label18.Text = "Upload a Photo";
            // 
            // rdbFileUpload
            // 
            this.rdbFileUpload.AutoSize = true;
            this.rdbFileUpload.Location = new System.Drawing.Point(141, 7);
            this.rdbFileUpload.Name = "rdbFileUpload";
            this.rdbFileUpload.Size = new System.Drawing.Size(78, 17);
            this.rdbFileUpload.TabIndex = 26;
            this.rdbFileUpload.TabStop = true;
            this.rdbFileUpload.Text = "File Upload";
            this.rdbFileUpload.UseVisualStyleBackColor = true;
            this.rdbFileUpload.CheckedChanged += new System.EventHandler(this.rdbFileUpload_CheckedChanged_1);
            // 
            // rdbCameraClick
            // 
            this.rdbCameraClick.AutoSize = true;
            this.rdbCameraClick.Location = new System.Drawing.Point(6, 10);
            this.rdbCameraClick.Name = "rdbCameraClick";
            this.rdbCameraClick.Size = new System.Drawing.Size(86, 17);
            this.rdbCameraClick.TabIndex = 25;
            this.rdbCameraClick.TabStop = true;
            this.rdbCameraClick.Text = "Camara click";
            this.rdbCameraClick.UseVisualStyleBackColor = true;
            this.rdbCameraClick.CheckedChanged += new System.EventHandler(this.rdbCameraClick_CheckedChanged_1);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(470, 246);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 113;
            this.label17.Text = "Photo Type";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(100, 636);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // richtxtHouseDetails
            // 
            this.richtxtHouseDetails.Location = new System.Drawing.Point(246, 345);
            this.richtxtHouseDetails.MaxLength = 25;
            this.richtxtHouseDetails.Name = "richtxtHouseDetails";
            this.richtxtHouseDetails.Size = new System.Drawing.Size(179, 21);
            this.richtxtHouseDetails.TabIndex = 17;
            this.richtxtHouseDetails.Text = "";
            // 
            // richtxtHouseBisiness
            // 
            this.richtxtHouseBisiness.Location = new System.Drawing.Point(246, 251);
            this.richtxtHouseBisiness.MaxLength = 100;
            this.richtxtHouseBisiness.Name = "richtxtHouseBisiness";
            this.richtxtHouseBisiness.Size = new System.Drawing.Size(179, 33);
            this.richtxtHouseBisiness.TabIndex = 14;
            this.richtxtHouseBisiness.Text = "";
            // 
            // redioBtnNo
            // 
            this.redioBtnNo.AutoSize = true;
            this.redioBtnNo.Location = new System.Drawing.Point(123, 12);
            this.redioBtnNo.Name = "redioBtnNo";
            this.redioBtnNo.Size = new System.Drawing.Size(39, 17);
            this.redioBtnNo.TabIndex = 13;
            this.redioBtnNo.TabStop = true;
            this.redioBtnNo.Text = "No";
            this.redioBtnNo.UseVisualStyleBackColor = true;
            this.redioBtnNo.CheckedChanged += new System.EventHandler(this.redioBtnNo_CheckedChanged_1);
            // 
            // redioBtnYes
            // 
            this.redioBtnYes.AutoSize = true;
            this.redioBtnYes.Location = new System.Drawing.Point(6, 12);
            this.redioBtnYes.Name = "redioBtnYes";
            this.redioBtnYes.Size = new System.Drawing.Size(43, 17);
            this.redioBtnYes.TabIndex = 12;
            this.redioBtnYes.TabStop = true;
            this.redioBtnYes.Text = "Yes";
            this.redioBtnYes.UseVisualStyleBackColor = true;
            this.redioBtnYes.CheckedChanged += new System.EventHandler(this.redioBtnYes_CheckedChanged_1);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(263, 636);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 43;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // txtElectricity
            // 
            this.txtElectricity.Location = new System.Drawing.Point(614, 334);
            this.txtElectricity.MaxLength = 25;
            this.txtElectricity.Name = "txtElectricity";
            this.txtElectricity.Size = new System.Drawing.Size(271, 20);
            this.txtElectricity.TabIndex = 29;
            // 
            // txtToilte
            // 
            this.txtToilte.Location = new System.Drawing.Point(614, 307);
            this.txtToilte.MaxLength = 25;
            this.txtToilte.Name = "txtToilte";
            this.txtToilte.Size = new System.Drawing.Size(271, 20);
            this.txtToilte.TabIndex = 28;
            // 
            // txtSate
            // 
            this.txtSate.Location = new System.Drawing.Point(614, 423);
            this.txtSate.MaxLength = 25;
            this.txtSate.Name = "txtSate";
            this.txtSate.Size = new System.Drawing.Size(276, 20);
            this.txtSate.TabIndex = 32;
            // 
            // txtMotherTng
            // 
            this.txtMotherTng.Location = new System.Drawing.Point(245, 389);
            this.txtMotherTng.MaxLength = 25;
            this.txtMotherTng.Name = "txtMotherTng";
            this.txtMotherTng.Size = new System.Drawing.Size(179, 20);
            this.txtMotherTng.TabIndex = 18;
            // 
            // txtCookingMedium
            // 
            this.txtCookingMedium.AutoSize = true;
            this.txtCookingMedium.Location = new System.Drawing.Point(474, 372);
            this.txtCookingMedium.Name = "txtCookingMedium";
            this.txtCookingMedium.Size = new System.Drawing.Size(86, 13);
            this.txtCookingMedium.TabIndex = 100;
            this.txtCookingMedium.Text = "Cooking Medium";
            // 
            // txtEletricity
            // 
            this.txtEletricity.AutoSize = true;
            this.txtEletricity.Location = new System.Drawing.Point(474, 341);
            this.txtEletricity.Name = "txtEletricity";
            this.txtEletricity.Size = new System.Drawing.Size(52, 13);
            this.txtEletricity.TabIndex = 99;
            this.txtEletricity.Text = "Electricity";
            // 
            // txtToilet
            // 
            this.txtToilet.AutoSize = true;
            this.txtToilet.Location = new System.Drawing.Point(473, 316);
            this.txtToilet.Name = "txtToilet";
            this.txtToilet.Size = new System.Drawing.Size(33, 13);
            this.txtToilet.TabIndex = 98;
            this.txtToilet.Text = "Toilet";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(474, 402);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 97;
            this.label13.Text = "Water";
            // 
            // txtState
            // 
            this.txtState.AutoSize = true;
            this.txtState.Location = new System.Drawing.Point(474, 430);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(144, 13);
            this.txtState.TabIndex = 96;
            this.txtState.Text = "State( State to State Migrate)";
            // 
            // txtMotherTongue
            // 
            this.txtMotherTongue.AutoSize = true;
            this.txtMotherTongue.Location = new System.Drawing.Point(45, 396);
            this.txtMotherTongue.Name = "txtMotherTongue";
            this.txtMotherTongue.Size = new System.Drawing.Size(80, 13);
            this.txtMotherTongue.TabIndex = 95;
            this.txtMotherTongue.Text = "Mother Tongue";
            // 
            // txtFamilyIncome
            // 
            this.txtFamilyIncome.Location = new System.Drawing.Point(246, 318);
            this.txtFamilyIncome.MaxLength = 10;
            this.txtFamilyIncome.Name = "txtFamilyIncome";
            this.txtFamilyIncome.ShortcutsEnabled = false;
            this.txtFamilyIncome.Size = new System.Drawing.Size(179, 20);
            this.txtFamilyIncome.TabIndex = 16;
            this.txtFamilyIncome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIncome_KeyPress);
            // 
            // txtCast
            // 
            this.txtCast.Location = new System.Drawing.Point(246, 291);
            this.txtCast.MaxLength = 50;
            this.txtCast.Name = "txtCast";
            this.txtCast.Size = new System.Drawing.Size(179, 20);
            this.txtCast.TabIndex = 15;
            // 
            // txtHusWifeNm
            // 
            this.txtHusWifeNm.Location = new System.Drawing.Point(245, 175);
            this.txtHusWifeNm.MaxLength = 50;
            this.txtHusWifeNm.Name = "txtHusWifeNm";
            this.txtHusWifeNm.Size = new System.Drawing.Size(179, 20);
            this.txtHusWifeNm.TabIndex = 10;
            // 
            // txtFamilyHeadName
            // 
            this.txtFamilyHeadName.Location = new System.Drawing.Point(246, 145);
            this.txtFamilyHeadName.MaxLength = 50;
            this.txtFamilyHeadName.Name = "txtFamilyHeadName";
            this.txtFamilyHeadName.Size = new System.Drawing.Size(179, 20);
            this.txtFamilyHeadName.TabIndex = 9;
            // 
            // txtFamilyStay
            // 
            this.txtFamilyStay.Location = new System.Drawing.Point(246, 119);
            this.txtFamilyStay.MaxLength = 3;
            this.txtFamilyStay.Name = "txtFamilyStay";
            this.txtFamilyStay.ShortcutsEnabled = false;
            this.txtFamilyStay.Size = new System.Drawing.Size(179, 20);
            this.txtFamilyStay.TabIndex = 8;
            this.txtFamilyStay.TextChanged += new System.EventHandler(this.txtFamilyStay_TextChanged);
            this.txtFamilyStay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFamilyStay_KeyPress);
            // 
            // txtRationNo
            // 
            this.txtRationNo.Location = new System.Drawing.Point(246, 93);
            this.txtRationNo.MaxLength = 25;
            this.txtRationNo.Name = "txtRationNo";
            this.txtRationNo.Size = new System.Drawing.Size(179, 20);
            this.txtRationNo.TabIndex = 7;
            // 
            // txtHouseNo
            // 
            this.txtHouseNo.Location = new System.Drawing.Point(246, 66);
            this.txtHouseNo.MaxLength = 10;
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Size = new System.Drawing.Size(179, 20);
            this.txtHouseNo.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 356);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 87;
            this.label10.Text = "House Details";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 316);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 13);
            this.label9.TabIndex = 86;
            this.label9.Text = "Family Monthly Income";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 290);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 85;
            this.label8.Text = "Caste";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(188, 13);
            this.label7.TabIndex = 84;
            this.label7.Text = "Do you use part of House for Business";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 83;
            this.label6.Text = "Husband/Wife Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 82;
            this.label5.Text = "Family Head Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 13);
            this.label4.TabIndex = 81;
            this.label4.Text = "How many years the family is staying";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 80;
            this.label3.Text = "Ration Card No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "House/Hut No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 78;
            this.label1.Text = "Name Of Slum/Settlement";
            // 
            // newLoanDataSet
            // 
            this.newLoanDataSet.DataSetName = "NewLoanDataSet";
            this.newLoanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // memberFamilyDetailsBindingSource
            // 
            this.memberFamilyDetailsBindingSource.DataMember = "MemberFamilyDetails";
            this.memberFamilyDetailsBindingSource.DataSource = this.newLoanDataSet;
            // 
            // memberFamilyDetailsTableAdapter
            // 
            this.memberFamilyDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.Gender,
            this.ageDataGridViewTextBoxColumn,
            this.qualifiactionDataGridViewTextBoxColumn,
            this.isMemberDataGridViewTextBoxColumn,
            this.MemberID,
            this.fkMembersDataGridViewTextBoxColumn,
            this.MembersId});
            this.dataGridView1.Location = new System.Drawing.Point(476, 446);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(644, 182);
            this.dataGridView1.TabIndex = 33;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
            // 
            // IdDataGridViewTextBoxColumn
            // 
            this.IdDataGridViewTextBoxColumn.DataPropertyName = "MemebersDetailsID";
            this.IdDataGridViewTextBoxColumn.HeaderText = "Id";
            this.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn";
            this.IdDataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Gender
            // 
            this.Gender.DataPropertyName = "Gender";
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            // 
            // ageDataGridViewTextBoxColumn
            // 
            this.ageDataGridViewTextBoxColumn.DataPropertyName = "Age";
            this.ageDataGridViewTextBoxColumn.HeaderText = "Age";
            this.ageDataGridViewTextBoxColumn.Name = "ageDataGridViewTextBoxColumn";
            // 
            // qualifiactionDataGridViewTextBoxColumn
            // 
            this.qualifiactionDataGridViewTextBoxColumn.DataPropertyName = "Qualifiaction";
            this.qualifiactionDataGridViewTextBoxColumn.HeaderText = "Qualifiaction";
            this.qualifiactionDataGridViewTextBoxColumn.Name = "qualifiactionDataGridViewTextBoxColumn";
            // 
            // isMemberDataGridViewTextBoxColumn
            // 
            this.isMemberDataGridViewTextBoxColumn.DataPropertyName = "IsMember";
            this.isMemberDataGridViewTextBoxColumn.HeaderText = "IsMember";
            this.isMemberDataGridViewTextBoxColumn.Name = "isMemberDataGridViewTextBoxColumn";
            this.isMemberDataGridViewTextBoxColumn.ReadOnly = true;
            this.isMemberDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.isMemberDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // MemberID
            // 
            this.MemberID.DataPropertyName = "MemberId";
            this.MemberID.HeaderText = "MemberId";
            this.MemberID.Name = "MemberID";
            this.MemberID.ReadOnly = true;
            // 
            // fkMembersDataGridViewTextBoxColumn
            // 
            this.fkMembersDataGridViewTextBoxColumn.DataPropertyName = "Fk_Members";
            this.fkMembersDataGridViewTextBoxColumn.HeaderText = "Fk_Members";
            this.fkMembersDataGridViewTextBoxColumn.Name = "fkMembersDataGridViewTextBoxColumn";
            this.fkMembersDataGridViewTextBoxColumn.Visible = false;
            // 
            // MembersId
            // 
            this.MembersId.DataPropertyName = "Id";
            this.MembersId.HeaderText = "MembersId";
            this.MembersId.Name = "MembersId";
            this.MembersId.ToolTipText = "MembersId";
            this.MembersId.Visible = false;
            // 
            // cmbCountry
            // 
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(291, 6);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(145, 21);
            this.cmbCountry.TabIndex = 2;
            this.cmbCountry.SelectedIndexChanged += new System.EventHandler(this.cmbCountry_SelectedIndexChanged_2);
            // 
            // InsertButton
            // 
            this.InsertButton.Location = new System.Drawing.Point(27, 175);
            this.InsertButton.Name = "InsertButton";
            this.InsertButton.Size = new System.Drawing.Size(75, 23);
            this.InsertButton.TabIndex = 39;
            this.InsertButton.Text = "Insert";
            this.InsertButton.UseVisualStyleBackColor = true;
            this.InsertButton.Click += new System.EventHandler(this.InsertButton_Click);
            // 
            // UpdatedataGrid
            // 
            this.UpdatedataGrid.Location = new System.Drawing.Point(135, 175);
            this.UpdatedataGrid.Name = "UpdatedataGrid";
            this.UpdatedataGrid.Size = new System.Drawing.Size(75, 23);
            this.UpdatedataGrid.TabIndex = 40;
            this.UpdatedataGrid.Text = "Update";
            this.UpdatedataGrid.UseVisualStyleBackColor = true;
            this.UpdatedataGrid.Click += new System.EventHandler(this.UpdatedataGrid_Click);
            // 
            // DeleteGrid
            // 
            this.DeleteGrid.Location = new System.Drawing.Point(249, 175);
            this.DeleteGrid.Name = "DeleteGrid";
            this.DeleteGrid.Size = new System.Drawing.Size(75, 23);
            this.DeleteGrid.TabIndex = 41;
            this.DeleteGrid.Text = "Delete";
            this.DeleteGrid.UseVisualStyleBackColor = true;
            this.DeleteGrid.Click += new System.EventHandler(this.DeleteGrid_Click);
            // 
            // CookMediumList
            // 
            this.CookMediumList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CookMediumList.FormattingEnabled = true;
            this.CookMediumList.Items.AddRange(new object[] {
            "--Select--",
            "Gas",
            "CharCoal\t",
            "Stove",
            "Wood"});
            this.CookMediumList.Location = new System.Drawing.Point(614, 364);
            this.CookMediumList.Name = "CookMediumList";
            this.CookMediumList.Size = new System.Drawing.Size(271, 21);
            this.CookMediumList.TabIndex = 30;
            this.CookMediumList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // WaterList
            // 
            this.WaterList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WaterList.FormattingEnabled = true;
            this.WaterList.Items.AddRange(new object[] {
            "--Select---",
            "Individual Taps",
            "Community  Taps",
            "Boreholes/Wells",
            "Dams",
            "Springs",
            "Rivers",
            "Water Tankers",
            "Streem"});
            this.WaterList.Location = new System.Drawing.Point(614, 394);
            this.WaterList.Name = "WaterList";
            this.WaterList.Size = new System.Drawing.Size(271, 21);
            this.WaterList.TabIndex = 31;
            this.WaterList.SelectedIndexChanged += new System.EventHandler(this.WaterList_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.redioBtnNo);
            this.groupBox1.Controls.Add(this.redioBtnYes);
            this.groupBox1.Location = new System.Drawing.Point(246, 208);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 33);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbFileUpload);
            this.groupBox2.Controls.Add(this.rdbCameraClick);
            this.groupBox2.Location = new System.Drawing.Point(562, 237);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(271, 27);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(45, 264);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 13);
            this.label16.TabIndex = 162;
            this.label16.Text = "If yes please fill the Info ";
            // 
            // GenderList
            // 
            this.GenderList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GenderList.FormattingEnabled = true;
            this.GenderList.Items.AddRange(new object[] {
            "--Select--",
            "Male",
            "Female"});
            this.GenderList.Location = new System.Drawing.Point(154, 40);
            this.GenderList.Name = "GenderList";
            this.GenderList.Size = new System.Drawing.Size(179, 21);
            this.GenderList.TabIndex = 34;
            this.GenderList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // FMGender
            // 
            this.FMGender.AutoSize = true;
            this.FMGender.Location = new System.Drawing.Point(24, 40);
            this.FMGender.Name = "FMGender";
            this.FMGender.Size = new System.Drawing.Size(42, 13);
            this.FMGender.TabIndex = 131;
            this.FMGender.Text = "Gender";
            // 
            // FMDetailsNm
            // 
            this.FMDetailsNm.Location = new System.Drawing.Point(154, 9);
            this.FMDetailsNm.MaxLength = 50;
            this.FMDetailsNm.Name = "FMDetailsNm";
            this.FMDetailsNm.Size = new System.Drawing.Size(179, 20);
            this.FMDetailsNm.TabIndex = 33;
            // 
            // FMNm
            // 
            this.FMNm.AutoSize = true;
            this.FMNm.Location = new System.Drawing.Point(24, 16);
            this.FMNm.Name = "FMNm";
            this.FMNm.Size = new System.Drawing.Size(35, 13);
            this.FMNm.TabIndex = 130;
            this.FMNm.Text = "Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.FMNm);
            this.groupBox3.Controls.Add(this.FMDetailsNm);
            this.groupBox3.Controls.Add(this.FMGender);
            this.groupBox3.Controls.Add(this.GenderList);
            this.groupBox3.Controls.Add(this.FMAge);
            this.groupBox3.Controls.Add(this.CheckBoxFMIsMember);
            this.groupBox3.Controls.Add(this.DeleteGrid);
            this.groupBox3.Controls.Add(this.FMQualifiaction);
            this.groupBox3.Controls.Add(this.UpdatedataGrid);
            this.groupBox3.Controls.Add(this.FMMem);
            this.groupBox3.Controls.Add(this.InsertButton);
            this.groupBox3.Controls.Add(this.FMID);
            this.groupBox3.Controls.Add(this.FMDetailsAge);
            this.groupBox3.Controls.Add(this.FMDetailsQualifiaction);
            this.groupBox3.Controls.Add(this.FMDetailsMemberID);
            this.groupBox3.Location = new System.Drawing.Point(42, 430);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 198);
            this.groupBox3.TabIndex = 163;
            this.groupBox3.TabStop = false;
            // 
            // FMAge
            // 
            this.FMAge.AutoSize = true;
            this.FMAge.Location = new System.Drawing.Point(24, 68);
            this.FMAge.Name = "FMAge";
            this.FMAge.Size = new System.Drawing.Size(26, 13);
            this.FMAge.TabIndex = 132;
            this.FMAge.Text = "Age";
            // 
            // CheckBoxFMIsMember
            // 
            this.CheckBoxFMIsMember.AutoSize = true;
            this.CheckBoxFMIsMember.Location = new System.Drawing.Point(155, 125);
            this.CheckBoxFMIsMember.Name = "CheckBoxFMIsMember";
            this.CheckBoxFMIsMember.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxFMIsMember.TabIndex = 37;
            this.CheckBoxFMIsMember.UseVisualStyleBackColor = true;
            this.CheckBoxFMIsMember.CheckedChanged += new System.EventHandler(this.CheckBoxFMIsMember_CheckedChanged);
            // 
            // FMQualifiaction
            // 
            this.FMQualifiaction.AutoSize = true;
            this.FMQualifiaction.Location = new System.Drawing.Point(24, 96);
            this.FMQualifiaction.Name = "FMQualifiaction";
            this.FMQualifiaction.Size = new System.Drawing.Size(65, 13);
            this.FMQualifiaction.TabIndex = 133;
            this.FMQualifiaction.Text = "Qualification";
            // 
            // FMMem
            // 
            this.FMMem.AutoSize = true;
            this.FMMem.Location = new System.Drawing.Point(24, 125);
            this.FMMem.Name = "FMMem";
            this.FMMem.Size = new System.Drawing.Size(53, 13);
            this.FMMem.TabIndex = 134;
            this.FMMem.Text = "IsMember";
            // 
            // FMID
            // 
            this.FMID.AutoSize = true;
            this.FMID.Location = new System.Drawing.Point(24, 151);
            this.FMID.Name = "FMID";
            this.FMID.Size = new System.Drawing.Size(56, 13);
            this.FMID.TabIndex = 135;
            this.FMID.Text = "MemberID";
            // 
            // FMDetailsAge
            // 
            this.FMDetailsAge.Location = new System.Drawing.Point(155, 68);
            this.FMDetailsAge.MaxLength = 3;
            this.FMDetailsAge.Name = "FMDetailsAge";
            this.FMDetailsAge.ShortcutsEnabled = false;
            this.FMDetailsAge.Size = new System.Drawing.Size(179, 20);
            this.FMDetailsAge.TabIndex = 35;
            this.FMDetailsAge.TextChanged += new System.EventHandler(this.FMDetailsAge_TextChanged);
            this.FMDetailsAge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FMDetailsAge_KeyPress);
            // 
            // FMDetailsQualifiaction
            // 
            this.FMDetailsQualifiaction.Location = new System.Drawing.Point(155, 96);
            this.FMDetailsQualifiaction.MaxLength = 25;
            this.FMDetailsQualifiaction.Name = "FMDetailsQualifiaction";
            this.FMDetailsQualifiaction.Size = new System.Drawing.Size(179, 20);
            this.FMDetailsQualifiaction.TabIndex = 36;
            // 
            // FMDetailsMemberID
            // 
            this.FMDetailsMemberID.Location = new System.Drawing.Point(154, 151);
            this.FMDetailsMemberID.MaxLength = 20;
            this.FMDetailsMemberID.Name = "FMDetailsMemberID";
            this.FMDetailsMemberID.Size = new System.Drawing.Size(179, 20);
            this.FMDetailsMemberID.TabIndex = 38;
            // 
            // CreateMemberFamily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1161, 750);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.WaterList);
            this.Controls.Add(this.CookMediumList);
            this.Controls.Add(this.cmbCountry);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmbDistrict);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbSlum);
            this.Controls.Add(this.cmbState);
            this.Controls.Add(this.cmbHub);
            this.Controls.Add(this.AddFamilyMessages);
            this.Controls.Add(this.imgCapture);
            this.Controls.Add(this.lblFamilyMessages);
            this.Controls.Add(this.btnVideoSource);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.btnContinue);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.imgVideo);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtFileUpload);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.richtxtHouseDetails);
            this.Controls.Add(this.richtxtHouseBisiness);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtElectricity);
            this.Controls.Add(this.txtToilte);
            this.Controls.Add(this.txtSate);
            this.Controls.Add(this.txtMotherTng);
            this.Controls.Add(this.txtCookingMedium);
            this.Controls.Add(this.txtEletricity);
            this.Controls.Add(this.txtToilet);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtState);
            this.Controls.Add(this.txtMotherTongue);
            this.Controls.Add(this.txtFamilyIncome);
            this.Controls.Add(this.txtCast);
            this.Controls.Add(this.txtHusWifeNm);
            this.Controls.Add(this.txtFamilyHeadName);
            this.Controls.Add(this.txtFamilyStay);
            this.Controls.Add(this.txtRationNo);
            this.Controls.Add(this.txtHouseNo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CreateMemberFamily";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "  ";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CreateMemberFamily_Load);
            ((System.ComponentModel.ISupportInitialize)(this.upcomingEMIReportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newLoanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbDistrict;
        private System.Windows.Forms.OpenFileDialog ofdPhoto;
        private UpcomingEMIReportDataSet upcomingEMIReportDataSet;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbSlum;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label AddFamilyMessages;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.Label lblFamilyMessages;
        private System.Windows.Forms.Button btnVideoSource;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFileUpload;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton rdbFileUpload;
        private System.Windows.Forms.RadioButton rdbCameraClick;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RichTextBox richtxtHouseDetails;
        private System.Windows.Forms.RichTextBox richtxtHouseBisiness;
        private System.Windows.Forms.RadioButton redioBtnNo;
        private System.Windows.Forms.RadioButton redioBtnYes;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtElectricity;
        private System.Windows.Forms.TextBox txtToilte;
        private System.Windows.Forms.TextBox txtSate;
        private System.Windows.Forms.TextBox txtMotherTng;
        private System.Windows.Forms.Label txtCookingMedium;
        private System.Windows.Forms.Label txtEletricity;
        private System.Windows.Forms.Label txtToilet;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label txtState;
        private System.Windows.Forms.Label txtMotherTongue;
        private System.Windows.Forms.TextBox txtFamilyIncome;
        private System.Windows.Forms.TextBox txtCast;
        private System.Windows.Forms.TextBox txtHusWifeNm;
        private System.Windows.Forms.TextBox txtFamilyHeadName;
        private System.Windows.Forms.TextBox txtFamilyStay;
        private System.Windows.Forms.TextBox txtRationNo;
        private System.Windows.Forms.TextBox txtHouseNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private NewLoanDataSet newLoanDataSet;
        private System.Windows.Forms.BindingSource memberFamilyDetailsBindingSource;
        private NewLoanDataSetTableAdapters.MemberFamilyDetailsTableAdapter memberFamilyDetailsTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Button InsertButton;
        private System.Windows.Forms.Button UpdatedataGrid;
        private System.Windows.Forms.Button DeleteGrid;
        private System.Windows.Forms.ComboBox CookMediumList;
        private System.Windows.Forms.ComboBox WaterList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qualifiactionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isMemberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberID;
        private System.Windows.Forms.DataGridViewTextBoxColumn fkMembersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MembersId;
        private System.Windows.Forms.ComboBox GenderList;
        private System.Windows.Forms.Label FMGender;
        private System.Windows.Forms.TextBox FMDetailsNm;
        private System.Windows.Forms.Label FMNm;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label FMAge;
        private System.Windows.Forms.CheckBox CheckBoxFMIsMember;
        private System.Windows.Forms.Label FMQualifiaction;
        private System.Windows.Forms.Label FMMem;
        private System.Windows.Forms.Label FMID;
        private System.Windows.Forms.TextBox FMDetailsAge;
        private System.Windows.Forms.TextBox FMDetailsQualifiaction;
        private System.Windows.Forms.TextBox FMDetailsMemberID;
    }
}