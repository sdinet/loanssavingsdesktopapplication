﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;

namespace SPARC
{
    public partial class ScannedDocuments : Form
    {
        public ScannedDocuments()
        {
            InitializeComponent();
        }
        public string MemberDocImagePath { get; set; }
        public string path { get; set; }
        public string[] arrfiles { get; set; }
        public string files { get; set; }
      //  string MemberDocImagePath;
        byte[] byteconvertedimage;
        MemoryStream ms = new MemoryStream();

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        int x = 0;
        int y = 15;

        int top_align = 1;




        private void ScannedDocuments_Load(object sender, EventArgs e)
        {
            CreateBackupFolderMemberDocuments();
            FillCombos(Convert.ToInt32(LocationCode.DocumentType), 0, cmbDocType);
            ClearControls();
            Members objMemberFetch = new Members();
            objMemberFetch.Id = GlobalValues.Member_PkId;
            LoadControls(objMemberFetch);

        }


        private void LoadControls(Members objMemberFetch)
        {
            DataSet dsMemberFetch = objMemberFetch.LoadMemberById();
            if (dsMemberFetch != null && dsMemberFetch.Tables.Count > 0 && dsMemberFetch.Tables[0].Rows.Count > 0)
            {
                txtMemberID.Text = dsMemberFetch.Tables[0].Rows[0]["MemberID"].ToString();
                txtName.Text = dsMemberFetch.Tables[0].Rows[0]["Name"].ToString();
                try
                {
                    //if (dsMemberFetch.Tables[0].Rows[0]["ScanedImage"] != null && dsMemberFetch.Tables[0].Rows[0]["ScanedImage"] != DBNull.Value)
                    //    if (scanedImage.Image != null)
                    //    {
                    //        scanedImage.Image.Dispose();
                    //    }
                    //MemoryStream ms = new MemoryStream((byte[])dsMemberFetch.Tables[0].Rows[0]["ScanedImage"]);//create memory stream by passing byte array of the image
                    //scanedImage.Image = Image.FromStream(ms);//set image property of the picture box by creating a image from stream 
                    //scanedImage.SizeMode = PictureBoxSizeMode.StretchImage;//set size mode property of the picture box to stretch 
                    //scanedImage.Refresh();
                }
                catch (Exception ex)
                {

                }

            }


        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {

            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void ClearControls()
        {
            PBs.Clear();
            txtName.Text = string.Empty;
            cmbDocType.Text = string.Empty;
            txtMemberID.Text = string.Empty;
         
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }





        private List<PictureBox> PBs = new List<PictureBox>();

        private void getbtn_Click(object sender, EventArgs e)  // To generate Images
        {
         //   label1.Text = string.Empty;

         //   Members objMember = new Members();
         //   if (cmbDocType.SelectedIndex > 0)
         //   {
         //       string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
         //       string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Membership Form";
         //       if (Directory.Exists(FileNamePath))
         //       {

         //           string newpath = path;


         //           //if (flowLayoutPanel1.Controls.Count > 0)
         //           //{
         //           //foreach (var item in this.flowLayoutPanel1.Controls)
         //           // {
         //           foreach (var items in arrfiles)
         //           {
         //               Image img = Image.FromFile(items);
         //               //PictureBox pic;
         //               //pic = new PictureBox();
         //               //pic.Image = img;

         //               //  Image originalImagescn = img;

         //               int newWidth = img.Width * 50 / 100;
         //               int newHeight = img.Height * 50 / 100;

         //               SaveScannedImage(newWidth, newHeight, img);
         //               Members objMem_Img = new Members();

         //               // objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);

         //               // }
         //           }
         //       }

         //       if (objMember.Id == 0)
         //       {
         //           label2.Text = Messages.SAVED_SUCCESS;
         //           label2.ForeColor = Color.Green;
         //       }

         //       //// GET IMAGES
         //       //foreach (PictureBox pb in PBs)
         //       //{
         //       //    pb.Dispose();
         //       //}
         //       //PBs.Clear();

         //       //if (con.State != ConnectionState.Open)
         //       //    con.Open();
         //       //  string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());

         //       //if (Directory.Exists(FileNamePath))
         //       //{

         //       //    string[] list = Directory.GetFiles(FileNamePath);
         //       //    if (list.Length > 0)
         //       //    {
         //       //        label1.Text = "";
         //       //        PictureBox PB;
         //       //        int y = 0;
         //       //        int x = 0;
         //       //        for (int index = 0; index < list.Length; index++)
         //       //        {
         //       //            PB = new PictureBox();

         //       //            if (x % 3 == 0)
         //       //            {
         //       //                y = y + 150; // 3 images per rows, first image will be at (20,150)
         //       //                x = 0;
         //       //            }
         //       //            PB.Location = new Point(x * 230 + 20, y);
         //       //            PB.Size = new Size(200, 150);
         //       //            x++;

         //       //            PB.Size = new Size(200, 100);
         //       //            PB.Image = Image.FromFile(list[index]);
         //       //            PB.SizeMode = PictureBoxSizeMode.StretchImage;

         //       //            PB.Click += new EventHandler(picturebox_Click);
         //       //            PBs.Add(PB);
         //       //            this.Controls.Add(PB);
         //       //            // cmbDocType_SelectedIndexChanged(PB, e);
         //       //        }
         //       //    }

         //       //    else
         //       //    {
         //       //        label1.Text = "No Images to display";
         //       //        label1.ForeColor = Color.Red;
         //       //    }

         //       //    con.Close();
         //       //}
         //       //else
         //       //{
         //       //    label1.Text = "No Images to display";
         //       //    label1.ForeColor = Color.Red;
         //       //}

         //   }
         //   else
         //   {
         //       MessageBox.Show("Please select the Document Type");
         //   }
         ////  add_chkbx();
        }




        void picturebox_Click(object sender, System.EventArgs e)                            //  SINGLE CLICK
        {

            EnlargeImage1 EnImg = new EnlargeImage1(((PictureBox)sender).Image);
            
            if (EnImg == null)
            {
                MessageBox.Show("There is no image to show");
            }
            else
            {
                EnImg.WindowState = FormWindowState.Maximized;
                EnImg.Show();
            }

        }

        

        //void picturebox_doubleClick(object sender, System.EventArgs e)
        //{

        //    add_chkbx();
        //}







        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }


        private List<PictureBox> PBs1 = new List<PictureBox>();
        private void cmbDocType_SelectedIndexChanged(object sender, EventArgs e)         // CMB DOC type change index
        {
            checkBox1.Checked = false;
            if (cmbDocType.SelectedIndex > 0)
            {
            
                getimages();
                
            }

           

          if(cmbDocType.SelectedIndex==0)
            {

                foreach (PictureBox pb1 in PBs1)
                {
                    pb1.Dispose();
                }
                PBs1.Clear();

             // cmbDocType.DataSource = null;

            }
            
        }

       

        private Image imgg;
        private Image ScannedImg;

        byte[] ScannedImage;
        bool IsPhotoChanged1 = false;


        private void btnImage_Click(object sender, EventArgs e)
        {
            try
            {
                checkBox1.Checked = false;
                if (cmbDocType.SelectedIndex > 0)
                {
                    label3.Text = "";
                    System.IO.Stream myStream;
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Title = "Please Select Multiple images";
                    ofd.Multiselect = true;
                    ofd.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                    DialogResult dr = ofd.ShowDialog();
                    List<System.IO.FileInfo> fList = new List<System.IO.FileInfo>();
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        //if(ofd.ShowDialog()==DialogResult.OK)
                        //{
                        path = ofd.FileName;
                        arrfiles = ofd.FileNames;



                     foreach (String file in ofd.FileNames)
                           
                        {
                           txtImage.Text = new DirectoryInfo(file).Name;

                            string newtext = txtImage.Text;
                         
                          listBox1.Text = new DirectoryInfo(file).Name;      //3 may
                          string st = listBox1.Text;
                       
                      bool sp = hasSpecialChar(st);                                     // 9th MAY to check image format
                      if (sp == true)
                      {
                          //MessageBox.Show("Please select the Image name without any special characters");
                          Regex Regex=new Regex(st);
                          newtext = Regex.Replace(st, "[~,#,%,&,*,{,},\\,:,<,>,?,/,+,',|]", "", RegexOptions.Compiled);
                          
                      }
                      //else  //image format
                      //{
                          try
                          {
                              if ((myStream = ofd.OpenFile()) != null)
                              {
                                  using (myStream)
                                  {
                                      fList.Add(new System.IO.FileInfo(newtext));
                                      listBox1.Items.Add(newtext);

                                  }
                              }
                          }

                          catch (Exception ex)
                          {
                              MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                          }
                          //txtImage.Text = arrfiles;

                          Image imgg = Image.FromFile(file);
                          // scanedImage.Image = imgg;
                          int x = 0;
                          int y = 0;
                        



                          PictureBox pic = new PictureBox();
                          if (x % 3 == 0)
                          {
                              y = y + 150; // 3 images per rows, first image will be at (20,150)
                              x = 20;
                          }
                          pic.Image = Image.FromFile(file);
                          int picWidth = Convert.ToInt16(pic.Image.PhysicalDimension.Width * (0.25));
                          int picHeight = Convert.ToInt16(pic.Image.PhysicalDimension.Height * (0.25));

                          Size sz = new Size(picWidth, picHeight);

                          pic.Size = sz;
                          //pic.SizeMode = PictureBoxSizeMode.CenterImage;
                          pic.Location = new Point(x * 230 + 20, y);

                          this.Controls.Add(pic);
                          //  flowLayoutPanel1.Controls.Add(pic);
                          //pic.Size = new Size(picWidth, picHeight);
                          x++;

               //    } //9th may   image format
                   
                        }
                    }

                    label1.Text = string.Empty;

                    Members objMember = new Members();
                    //if (cmbDocType.SelectedIndex > 0)
                    //{
                    string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
                    string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Membership Form";
                    if (Directory.Exists(FileNamePath))
                    {

                        string newpath = path;


                        //if (flowLayoutPanel1.Controls.Count > 0)
                        //{
                        //foreach (var item in this.flowLayoutPanel1.Controls)
                        // {
                        foreach (var items in arrfiles)
                        {
                            Image img = Image.FromFile(items);


                            int newWidth = img.Width * 50 / 100;
                            int newHeight = img.Height * 50 / 100;

                            SaveScannedImage(newWidth, newHeight, img);
                            Members objMem_Img = new Members();

                            // objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);

                           
                        }
                    }



                    //if (objMember.Id == 0)
                    //{
                    //    label2.Text = Messages.SAVED_SUCCESS;
                    //    label2.ForeColor = Color.Green;
                    //}

                    //getimages();
                    //label2.Text = Messages.SAVED_SUCCESS;
                    //label2.ForeColor = Color.Green;

                }

                else
                {
                    // cmbDocType.DataSource = null;
                    MessageBox.Show("Please Select the Document Type");

                }

                // add_chkbx();
            }
            catch(Exception )
            {

            }


        }

        public string specialChar;
        public static bool hasSpecialChar(string input)
        {
            string specialChar = @"~,#,%,&,*,{,},\\,:,<,>,?,/,+,',|";
            foreach (var item in specialChar)
            {
                if (input.Contains(item)) return true;
            }

            return false;
        }




        private void CreateBackupFolderMemberDocuments()
        {
            string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());


            string bpath1 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Membership Form ";
            bool folderExists1 = Directory.Exists(bpath1);
            if (!folderExists1)
                Directory.CreateDirectory(bpath1);

            string bpath2 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Loan Form";
            bool folderExists2 = Directory.Exists(bpath2);
            if (!folderExists2)
                Directory.CreateDirectory(bpath2);

            string bpath3 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Passbook";
            bool folderExists3 = Directory.Exists(bpath3);
            if (!folderExists3)
                Directory.CreateDirectory(bpath3);

            string bpath4 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Family Card";
            bool folderExists4 = Directory.Exists(bpath4);
            if (!folderExists4)
                Directory.CreateDirectory(bpath4);

            string bpath5 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Incremental Loan Form";
            bool folderExists5 = Directory.Exists(bpath5);
            if (!folderExists5)
                Directory.CreateDirectory(bpath5);

            string bpath6 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + "Others ";
            bool folderExists6 = Directory.Exists(bpath6);
            if (!folderExists6)
                Directory.CreateDirectory(bpath6);


        }

        public void SaveScannedImage(int width = 0, int height = 0, Image img = null)  //Modified by Pawan
        {

            CreateBackupFolderMemberDocuments();
            Members objMem_Img = new Members();
            while (listBox1.Items.Count > 0)
            {
                foreach (var items in arrfiles)
                {
                    Image originalImage = Image.FromFile(items);
                    // Image originalImage = img;

                    if (width > 0 && height > 0)
                    {
                        Image.GetThumbnailImageAbort myCallback =
                        new Image.GetThumbnailImageAbort(ThumbnailCallback1);
                        Image imageToSave = originalImage.GetThumbnailImage
                            (width, height, myCallback, IntPtr.Zero);
                        string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());

                        string s = listBox1.Items[0] as string;
                        if (cmbDocType.SelectedIndex == 4)
                        {

                            //string FileNamePath = directory + "MembersDocuments\\" + "Membership Form\\" + GlobalValues.Member_ID + "_" + txtImage.Text;
                            //directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Membership Form";
                            string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Membership Form" + "\\" + "MembershipForm" + "_" + s;
                            MemberDocImagePath = FileNamePath.Substring(52);
                            try
                            {
                                if (!(File.Exists(FileNamePath)))
                                {

                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                    
                                
                                }
                                else
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }

                        }

                        else if (cmbDocType.SelectedIndex == 3)
                        {
                            string FileNamePath1 = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Loan Form" + "\\" + "LoanForm" + "_" + s;
                        string FileNamePath = FileNamePath1.Replace("'", "''");
                            MemberDocImagePath = FileNamePath.Substring(46);
                            try
                            {
                                if (File.Exists(FileNamePath))
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }
                                else
                                {
                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }
                        }
                        else if (cmbDocType.SelectedIndex == 6)
                        {
                            string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Passbook" + "\\" + "Passbook" + "_" + s;
                            MemberDocImagePath = FileNamePath.Substring(45);
                            try
                            {
                                if (!(File.Exists(FileNamePath)))
                                {
                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                }
                                else
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }

                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }
                        }
                        else if (cmbDocType.SelectedIndex == 1)
                        {
                            try
                            {

                                string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Family Card" + "\\" + "FamilyCard" + "_" + s;
                                MemberDocImagePath = FileNamePath.Substring(48);
                                if (!(File.Exists(FileNamePath)))
                                {
                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                }

                                else
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }
                        }
                        else if (cmbDocType.SelectedIndex == 2)
                        {
                            try
                            {
                                string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Incremental Loan Form" + "\\" + "IncrementalLoanForm" + "_" + s;
                                MemberDocImagePath = FileNamePath.Substring(58);
                                if (!(File.Exists(FileNamePath)))
                                {
                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                }
                                else
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }

                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }
                        }
                        else if (cmbDocType.SelectedIndex == 5)
                        {
                            try
                            {
                                string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\Others" + "\\" + "Others" + "_" + s;
                                MemberDocImagePath = FileNamePath.Substring(43);
                                if (!(File.Exists(FileNamePath)))
                                {
                                    imageToSave.Save(FileNamePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    objMem_Img.UpdateImage(GlobalValues.Member_PkId, MemberDocImagePath, Convert.ToInt32(cmbDocType.SelectedValue), GlobalValues.User_PkId, GlobalValues.User_PkId);
                                    getimages();
                                    label2.Text = Messages.SAVED_SUCCESS;
                                    label2.ForeColor = Color.Green;
                                }
                                else
                                {
                                    MessageBox.Show("File Already Exists");
                                    getimages();
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Please Check if file Already Exists");
                            }

                        }

                        else
                        {
                            MessageBox.Show("Please Select The Document Type");
                        }
                        listBox1.Items.RemoveAt(0);
                    }

                }
            }
          
        }
        //------------------------------------------------------------//
        public byte[] ImageToByte2(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                //stream.Close();

                byteArray = stream.ToArray();
                byteconvertedimage = byteArray;
            }
            return byteArray;
        }

        private static bool ThumbnailCallback1() { return false; }

        private static bool ThumbnailCallback() { return false; }




        
       private void btndlt_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (checkBox1.Checked ==true)
            
           {

               //if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
               //{
               //             // loop through all the controls and identify the picture box and go inside the same.
                            foreach (Control cont in this.Controls)
                            {
                                if (!(cont is PictureBox))
                                    continue;

                                // fetch the check box which is inside the picture box
                                foreach (Control chkcont in cont.Controls)
                                {
                                    if (chkcont is CheckBox)
                                    {
                                        // check if the check box is checked
                                        if (((CheckBox)chkcont).Checked == true)
                                        {
                                            //label3.Text = "";
                                            //if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                            //{

                                                // if the check box is checked then get the file name which is already binded to the Tag property of the check box
                                                string chktag = chkcont.Tag.ToString();
                                                string result = Path.GetFileName(chktag);
                                        // int id=

                                        
                                                int doc = Convert.ToInt32(cmbDocType.SelectedValue);
                             
                                                // delete the file from the path


                                                File.Delete(chktag);
                                                // now write a query to delete the entry from the database

                                                con.Open();

                                                string qry = "delete from MembersFiles where FileName='" + result + "' and Document_Type='" + doc + "' and fk_MemberId= '" + GlobalValues.Member_PkId + "'";
                                             
                                                    using (SqlCommand cmd = new SqlCommand(qry, con))
                                                    {

                                                        cmd.ExecuteNonQuery();
                                                        j++;

                                                    }
                                              
                                               
                                                con.Close();

                                           // }
                                            
                                           
                                        }
                                    }
                                }

                              
                                //CheckBox chk = new CheckBox();
                                //// chk.Text = picturebox_Click;
                                //chk.Size = new System.Drawing.Size(30, 30);
                                //chk.Name = "picturebox_Click" + cont.Name;
                                //string name = chk.Name;

                                //chk.BackColor = Color.Transparent; // transparent color for checkbox
                                //cont.Controls.Add(chk);
                             // chk.CheckedChanged += new EventHandler(chk_CheckedChanged);
                               
                        }
                           
                        //getimages();
                        //add_chkbx();
                        if (j > 0)
                        {
                                getimages();
                                add_chkbx();
                                label3.Text = "Image has deleted successfully";
                                label3.ForeColor = Color.Green;
                          
                        }
                        else
                        {
                            MessageBox.Show("Please Select the Images to delete ");
                        }
                       
                        //label3.Text = "Image has deleted successfully";
                        //label3.ForeColor = Color.Green;
                  
                //    }
            //} //del

           }
            else
            {

                MessageBox.Show("Please Select the Images to delete ");
            }

         
                                        
        }


      



        // private void add_chkbx_CheckedChanged(object sender, EventArgs e)
        //{
        //   // this.pi.Image = // Your image here

        //}


     //  public CheckBox chk;
        public void add_chkbx()
        {
            // for each picture box add check box
            foreach (Control cont in this.Controls)
            {
                if (!(cont is PictureBox))
                    continue;
                try
                {
                  String picLocation = cont.Tag.ToString();
                    CheckBox chk = new CheckBox();

                 chk.Tag = picLocation;
                    chk.Size = new System.Drawing.Size(30, 30);
                    chk.BackColor = Color.Transparent; // transparent color for checkbox
                    cont.Controls.Add(chk);
              
                    chk.CheckedChanged += new EventHandler(chk_CheckedChanged);
                }
                catch
                {
                }
            }
            
        }

        public CheckBox chk;
        public bool resutlt { get;  set; }

    private void chk_CheckedChanged(object sender, EventArgs e)
        {

            label3.Text = "";
            CheckBox chk = (CheckBox)sender;
            bool resutlt = chk.Checked;
           
        }

     
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            add_chkbx();
          
        }

        private void txtImage_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  flowLayoutPanel1.Controls.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           // add_chkbx();
        }


        private void getimages()                   // method to fetch images
        {
            DataTable dt = new DataTable();
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";

            foreach (PictureBox pb1 in PBs1)
            {
                pb1.Dispose();
            }
            PBs1.Clear();

            if (con.State != ConnectionState.Open)
                con.Open();
            string directory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory().ToString());
         
            string FileNamePath = directory + "MembersDocuments\\" + GlobalValues.Member_ID + "\\" + cmbDocType.Text;
            int doc = Convert.ToInt32(cmbDocType.SelectedValue);
         if (Directory.Exists(FileNamePath))
        {
            List<string> dbFileNames = null;
            string qry = "select FileName from MembersFiles where fk_MemberId='" + GlobalValues.Member_PkId + "' and Document_Type='" + doc + "'";
            using (SqlDataAdapter da = new SqlDataAdapter(qry, con))
            {
              //  con.Open();
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                dbFileNames = dt1.Rows.OfType<DataRow>()
                    .Select(r => r[0].ToString()).ToList();
                //con.Close();
            }
            string[] fileNames = dbFileNames.Select(f =>
                 Path.Combine(FileNamePath, f)).ToArray();
       // }
         //   string[] fileNames = Directory.GetFiles(FileNamePath); //, "FamilyCard_*.*"); 

            if (fileNames.Length > 0)
            {
                label1.Text = "";
                PictureBox PB1;
                int y = 0;
                int x = 0;
                //try {

                    // adding
                    DateTime[] creationTimes = new DateTime[fileNames.Length];
                    for (int i = 0; i < fileNames.Length; i++)
                        creationTimes[i] = new FileInfo(fileNames[i]).CreationTime;
                    Array.Sort(creationTimes, fileNames);


                    for (int i = 0; i < fileNames.Length; i++)
                    {

                        Console.WriteLine("{0}: {1}", creationTimes[i], fileNames[i]);

                        PB1 = new PictureBox();

                        if (x % 3 == 0)
                        {
                            y = y + 150; // 3 images per rows, first image will be at (20,150)
                            x = 0;
                        }
                        PB1.Location = new Point(x * 230 + 20, y);

                        PB1.Size = new Size(200, 150);
                        x++;

                        PB1.Size = new Size(200, 100);

                        System.IO.FileStream fs;
                        //' Specify a valid picture file path on your computer.
                        fs = new System.IO.FileStream(fileNames[i], System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        PB1.Image = System.Drawing.Image.FromStream(fs);
                        fs.Close();
                        //PB1.Image = Image.FromFile(list[index]);
                        PB1.Tag = fileNames[i];
                        PB1.SizeMode = PictureBoxSizeMode.StretchImage;
                        PB1.Click += new EventHandler(picturebox_Click);

                        PBs1.Add(PB1);
                        this.Controls.Add(PB1);
                        //} //12 May
                    }
            //}
            // catch
            // {
            // }

                }
          
            else
            {
                label1.Text = "No Images to display";
                label1.ForeColor = Color.Red;
            }

                con.Close();
            }
            else
            {
                label1.Text = "No Images to display";
                label1.ForeColor = Color.Red;
            }
          
        }

       
        
        
        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (cmbDocType.SelectedIndex > 0)
            {
                if (checkBox1.Checked == true)
                {
                    label2.Text = "";
                    add_chkbx();
                }
                else
                {
                    getimages();
                }
            }
            else
            {
                MessageBox.Show("Please select Document Type and proceed further");
                
            }
           
        }



    }
}