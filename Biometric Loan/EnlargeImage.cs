﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;

namespace SPARC
{

    public partial class EnlargeImage : Form
    {
        public Int16 intMemberOperationType;
        public EnlargeImage()
        {
            InitializeComponent();            
        }
        Image picQr;
        public EnlargeImage(Image qrCodeimage)
        {

            InitializeComponent();
            picQr = qrCodeimage;
        }
        OpenFileDialog openFileDialog1 = new OpenFileDialog();
        private void EnlargeImage_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = picQr;

            Bitmap bt = new Bitmap(picQr);
            pictureBox1.Image = bt;
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;           
        }
       
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();                       
        }       
    }
}
