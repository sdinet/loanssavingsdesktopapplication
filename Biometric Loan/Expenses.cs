﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class frmExpenses : Form
    {
        private Int64 iSavExpenseID = 0;
        public frmExpenses()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;

            txtDescription.Text = String.Empty;
            if (cmbPayMode.Items.Count > 0) cmbPayMode.SelectedIndex = 0;
            if (cmbCategory.Items.Count > 0) cmbCategory.SelectedIndex = 0;
            dtpExpenseDate.Value = DateTime.Today;
            txtAmount.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            iSavExpenseID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbCategory.SelectedIndex == 0)
                ErrorMsg = Messages.CATEGORY+ Messages.COMMAWITHSPACE;
            if (cmbPayMode.SelectedIndex == 0)
                ErrorMsg += Messages.MODEOFPAYMENT + Messages.COMMAWITHSPACE;
            if (txtDescription.Text.Trim().Length == 0)
                ErrorMsg += Messages.DESCRIPTION + Messages.COMMAWITHSPACE;
            if ((txtAmount.Text.Trim().Length == 0) || Convert.ToDouble(txtAmount.Text) <= 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void NumericKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                    && !char.IsDigit(e.KeyChar))
            //&& e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;
                    Expenses objExpenses = new Expenses();
                    objExpenses.ExpenseId = iSavExpenseID;
                    objExpenses.Category = Convert.ToInt32(cmbCategory.SelectedValue);
                    objExpenses.ModeOfPayment = Convert.ToInt32(cmbPayMode.SelectedValue);
                    objExpenses.Description = txtDescription.Text.Trim();
                    objExpenses.Amount = Convert.ToDouble(txtAmount.Text);
                    objExpenses.ExpenseDate = dtpExpenseDate.Value;
                    objExpenses.Remarks = txtRemarks.Text;
                    objExpenses.Createdby = GlobalValues.User_PkId;
                    objExpenses.CreatedDate = DateTime.Today;
                    objExpenses.Updatedby = GlobalValues.User_PkId;
                    objExpenses.UpdatedDate = DateTime.Today;

                    Int64 iResult = 0;
                    iResult = objExpenses.SaveExpenses(iSavExpenseID);
                    if (iResult > 0)
                    {
                        if (objExpenses.ExpenseId == 0)
                        {
                            btnSave.Text = Constants.UPDATE;
                            
                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;

                            iSavExpenseID = iResult;
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                        }
                        LoadExpenses();
                    }
                    else
                    {
                        lblMessages.Text = Messages.ERROR_PROCESSING;
                        lblMessages.ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void Expenses_Load(object sender, EventArgs e)
        {
            try
            {               
                dtpExpenseDate.MaxDate = DateTime.Now.Date;
                dtpFromDate.MaxDate = DateTime.Now.Date;
                dtpToDate.MaxDate = DateTime.Now.Date;
                FillCombos(Convert.ToInt32(OtherCodeTypes.Category), 0, cmbCategory);
                FillCombos(Convert.ToInt32(OtherCodeTypes.ModeOfPayment), 0, cmbPayMode);
                ClearControls();

                FillCombos(Convert.ToInt32(OtherCodeTypes.Category), 0, cmbSearchCategory);

                dtpFromDate.Value = DateTime.Today.AddMonths(-1);
                LoadExpenses();               
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void LoadExpenses()
        {
            //Loads the grid
            Expenses objExpenses = new Expenses();
            objExpenses.Category = Convert.ToInt64(cmbSearchCategory.SelectedValue);
            DataSet dsExpenses = objExpenses.LoadExpenses(dtpFromDate.Value, dtpToDate.Value);
            if (dsExpenses != null && dsExpenses.Tables.Count > 0 && dsExpenses.Tables[0].Rows.Count > 0)
            {
                dgExpenses.AutoGenerateColumns = false;
                dgExpenses.DataSource = dsExpenses.Tables[0];
            }
            else 
            {
                dgExpenses.DataSource = null;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                NumericKeyPress(sender, e);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadExpenses();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);

            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void dgExpenses_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                cmbCategory.Text = dgExpenses.Rows[e.RowIndex].Cells["Category"].Value.ToString();
                cmbPayMode.Text = dgExpenses.Rows[e.RowIndex].Cells["ModeOfPayment"].Value.ToString();
                dtpExpenseDate.Value = Convert.ToDateTime(dgExpenses.Rows[e.RowIndex].Cells["Date"].Value);
                txtAmount.Text = dgExpenses.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                txtRemarks.Text = dgExpenses.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                txtDescription.Text = dgExpenses.Rows[e.RowIndex].Cells["Description"].Value.ToString();

                btnSave.Text = Constants.UPDATE;
                iSavExpenseID = Convert.ToInt32(dgExpenses.Rows[e.RowIndex].Cells["ExpenseId"].Value);
            }
            catch (Exception ex)
            {
               // Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgExpenses_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgExpenses.Columns[e.ColumnIndex].Index == 7)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgExpenses.CurrentRow.Cells[0].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 ExpenseId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            Expenses objExpenses = new Expenses();
            objExpenses.ExpenseId = ExpenseId;

            dsDeleteStatus = objExpenses.DeleteExpenses(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadExpenses();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    this.ControlBox = true;
                    lblMessages.ForeColor = Color.Green;
                }
                //else if (iResult == 0)
                //{
                //    lblMessages.Text = Messages.DELETE_PROCESSING;
                //    lblMessages.ForeColor = Color.Red;
                //}
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgExpenses_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
            {
                dgExpenses.Columns["btnDelete"].Visible = false;
            }
        }
    }
}
