﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using System.Configuration;

namespace SPARC
{
    public partial class GroupTransReport : Form
    {
        public GroupTransReport()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        private void GroupTransReport_Load(object sender, EventArgs e)
        {
            cmbtype.Items.Insert(0, "--Select--");
            cmbtype.SelectedIndex = 0;
            this.reportViewer1.RefreshReport();
            dtpTransDate.MaxDate = DateTime.Now.Date;
            this.reportViewer1.RefreshReport();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
               // FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                
               
            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                   
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }
            }            
        }

       
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private void cmbslum_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillCombogroup(Convert.ToInt32(cmbslum.SelectedValue));
                 

        }


        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();
            DataRow dr = dsgroup.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = "--Select One--";

            dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbgrp.DisplayMember = "Group_Name";
                cmbgrp.ValueMember = "Id";
                cmbgrp.DataSource = dsgroup.Tables[0];


            }
            else
            {
                cmbgrp.DataSource = null;
                //cmbGname.SelectedValue="--Select--";

            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbslum.SelectedIndex ==0)
                {
                    if (con.State != ConnectionState.Open)
                        con.Open();
                    reportViewer1.Reset();
                    DataTable dt = new DataTable("datatable1");
                    SqlCommand cmd;
                    SqlDataReader dr;
                    string cmdText = @"Select GI.Group_Name as Groupname,C.Name as Settlement, G.Type,G.Amount,G.CreatedDate as Date from GroupTransactions G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId left join GroupInfo GI on GI.Id=G.fk_GroupId where  G.TransactionDate=@TransDate and(dbo.udf_FetchHubBySlum(G.Fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND (dbo.udf_FetchCountryBySlum(G.Fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND (dbo.udf_FetchStateBySlum(G.Fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND (dbo.udf_FetchDistrictBySlum(G.Fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL)  and (G.TransactionDate=@TransDate)";
                    cmd = new SqlCommand(cmdText, con);
                    cmd.Parameters.Add("@TransDate", dtpTransDate.Value);
                    cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                    cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                    cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                    cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                   // cmd.Parameters.Add("@fk_SlumId", cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (object)DBNull.Value);
                    //cmd.Parameters.Add("@fk_GroupId", cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (object)DBNull.Value);
                   
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) dt.Load(dr);
                    dr.Close();
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.ReportAllgrouptrans.rdlc";
                    this.reportViewer1.LocalReport.EnableHyperlinks = true;
                    this.reportViewer1.LocalReport.DataSources.Add(
                       new Microsoft.Reporting.WinForms.ReportDataSource("DataSetGrouptrans", dt));
                    this.reportViewer1.RefreshReport();
                }
             
                    if (cmbgrp.SelectedIndex > 0 && cmbtype.SelectedIndex <= 0)
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        reportViewer1.Reset();
                        DataTable dt = new DataTable("datatable1");
                        SqlCommand cmd;
                        SqlDataReader dr;
                        string cmdText = @"Select GI.Group_Name as Groupname,C.Name as Settlement, G.Type,G.Amount,G.CreatedDate as Date from GroupTransactions G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId left join GroupInfo GI on GI.Id=G.fk_GroupId where G.Fk_SlumId=@fk_SlumId  and G.fk_GroupId=@fk_GroupId  and  G.TransactionDate=@TransDate and(dbo.udf_FetchHubBySlum(G.Fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND (dbo.udf_FetchCountryBySlum(G.Fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND (dbo.udf_FetchStateBySlum(G.Fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND (dbo.udf_FetchDistrictBySlum(G.Fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND(G.Fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)  and(G.fk_GroupId=@fk_GroupId) and (G.TransactionDate=@TransDate)";
                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@TransDate", dtpTransDate.Value);
                        cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_SlumId", cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_GroupId", cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (object)DBNull.Value);
                        // cmd.Parameters.Add("@type", cmbtype.SelectedIndex > 0 ? Convert.ToInt32(cmbtype.SelectedItem.ToString()) : (object)DBNull.Value);
                        // cmd.Parameters.Add("@type", cmbtype.SelectedItem.ToString());
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.ReportAllgrouptrans.rdlc";
                        this.reportViewer1.LocalReport.EnableHyperlinks = true;
                        this.reportViewer1.LocalReport.DataSources.Add(
                           new Microsoft.Reporting.WinForms.ReportDataSource("DataSetGrouptrans", dt));
                        this.reportViewer1.RefreshReport();
                    }
                   if (cmbslum.SelectedIndex >0 && cmbgrp.SelectedIndex <= 0)
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        reportViewer1.Reset();
                        DataTable dt = new DataTable("datatable1");
                        SqlCommand cmd;
                        SqlDataReader dr;
                        string cmdText = @"Select GI.Group_Name as Groupname,C.Name as Settlement, G.Type,G.Amount,G.CreatedDate as Date from GroupTransactions G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId left join GroupInfo GI on GI.Id=G.fk_GroupId where G.Fk_SlumId=@fk_SlumId  and G.TransactionDate=@TransDate and(dbo.udf_FetchHubBySlum(G.Fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND (dbo.udf_FetchCountryBySlum(G.Fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND (dbo.udf_FetchStateBySlum(G.Fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND (dbo.udf_FetchDistrictBySlum(G.Fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND(G.Fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)   and(G.TransactionDate=@TransDate)";
                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@TransDate", dtpTransDate.Value);
                        cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_SlumId", cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_GroupId", cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (object)DBNull.Value);
                        // cmd.Parameters.Add("@type", cmbtype.SelectedIndex > 0 ? Convert.ToInt32(cmbtype.SelectedItem.ToString()) : (object)DBNull.Value);
                        //  cmd.Parameters.Add("@type", cmbtype.SelectedItem.ToString());
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.ReportAllgrouptrans.rdlc";
                        this.reportViewer1.LocalReport.EnableHyperlinks = true;
                        this.reportViewer1.LocalReport.DataSources.Add(
                           new Microsoft.Reporting.WinForms.ReportDataSource("DataSetGrouptrans", dt));
                        this.reportViewer1.RefreshReport();
                    }
                
              
                    if (cmbtype.SelectedIndex > 0)
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        reportViewer1.Reset();
                        DataTable dt = new DataTable("datatable1");
                        SqlCommand cmd;
                        SqlDataReader dr;
                        string cmdText = @"Select GI.Group_Name as Groupname,C.Name as Settlement, G.Type,G.Amount,G.CreatedDate as Date from GroupTransactions G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId left join GroupInfo GI on GI.Id=G.fk_GroupId where G.Fk_SlumId=@fk_SlumId  and G.fk_GroupId=@fk_GroupId  and G.Type=@type and G.TransactionDate=@TransDate and(dbo.udf_FetchHubBySlum(G.Fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND (dbo.udf_FetchCountryBySlum(G.Fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND (dbo.udf_FetchStateBySlum(G.Fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND (dbo.udf_FetchDistrictBySlum(G.Fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND(G.Fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)  and(G.fk_GroupId=@fk_GroupId) and (G.Type=@type) and(G.TransactionDate=@TransDate)";
                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@TransDate", dtpTransDate.Value);
                        cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_SlumId", cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_GroupId", cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (object)DBNull.Value);
                        // cmd.Parameters.Add("@type", cmbtype.SelectedIndex > 0 ? Convert.ToInt32(cmbtype.SelectedItem.ToString()) : (object)DBNull.Value);
                          cmd.Parameters.Add("@type", cmbtype.SelectedItem.ToString());
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.ReportAllgrouptrans.rdlc";
                        this.reportViewer1.LocalReport.EnableHyperlinks = true;
                        this.reportViewer1.LocalReport.DataSources.Add(
                           new Microsoft.Reporting.WinForms.ReportDataSource("DataSetGrouptrans", dt));
                        this.reportViewer1.RefreshReport();
                    }
                    if (cmbslum.SelectedIndex >0 && cmbtype.SelectedIndex < 0)
                    {
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        reportViewer1.Reset();
                        DataTable dt = new DataTable("datatable1");
                        SqlCommand cmd;
                        SqlDataReader dr;
                        string cmdText = @"Select GI.Group_Name as Groupname,C.Name as Settlement, G.Type,G.Amount,G.CreatedDate as Date from GroupTransactions G LEFT JOIN Codevalue AS C on C.Id=G.fk_SlumId left join GroupInfo GI on GI.Id=G.fk_GroupId where G.Fk_SlumId=@fk_SlumId  and G.fk_GroupId=@fk_GroupId   and G.TransactionDate=@TransDate and(dbo.udf_FetchHubBySlum(G.Fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND (dbo.udf_FetchCountryBySlum(G.Fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND (dbo.udf_FetchStateBySlum(G.Fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND (dbo.udf_FetchDistrictBySlum(G.Fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND(G.Fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)  and(G.fk_GroupId=@fk_GroupId) and(G.TransactionDate=@TransDate)";
                        cmd = new SqlCommand(cmdText, con);
                        cmd.Parameters.Add("@TransDate", dtpTransDate.Value);
                        cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_SlumId", cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (object)DBNull.Value);
                        cmd.Parameters.Add("@fk_GroupId", cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (object)DBNull.Value);
                        // cmd.Parameters.Add("@type", cmbtype.SelectedIndex > 0 ? Convert.ToInt32(cmbtype.SelectedItem.ToString()) : (object)DBNull.Value);
                          cmd.Parameters.Add("@type", cmbtype.SelectedItem.ToString());
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows) dt.Load(dr);
                        dr.Close();
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.ReportAllgrouptrans.rdlc";
                        this.reportViewer1.LocalReport.EnableHyperlinks = true;
                        this.reportViewer1.LocalReport.DataSources.Add(
                           new Microsoft.Reporting.WinForms.ReportDataSource("DataSetGrouptrans", dt));
                        this.reportViewer1.RefreshReport();
                    }
                }
           
            catch
            {
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbmonth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbtype.SelectedIndex < 0)
            { 

                cmbtype.Text = "Please, select any value";
            }
            else
            {
                cmbtype.Text = cmbtype.SelectedText;
            }
        }

        private void cmbgrp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
