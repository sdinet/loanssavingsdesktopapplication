﻿namespace SPARC
{
    partial class frmDayBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMessages = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCreateDayBook = new System.Windows.Forms.Button();
            this.dtpOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransDate = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblMessages);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnCreateDayBook);
            this.panel1.Controls.Add(this.dtpOpenDate);
            this.panel1.Controls.Add(this.lblTransDate);
            this.panel1.Location = new System.Drawing.Point(7, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(496, 120);
            this.panel1.TabIndex = 0;
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(5, 99);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 96;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(164, 57);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 95;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCreateDayBook
            // 
            this.btnCreateDayBook.Location = new System.Drawing.Point(43, 57);
            this.btnCreateDayBook.Name = "btnCreateDayBook";
            this.btnCreateDayBook.Size = new System.Drawing.Size(115, 23);
            this.btnCreateDayBook.TabIndex = 94;
            this.btnCreateDayBook.Text = "&Generate Day Book";
            this.btnCreateDayBook.UseVisualStyleBackColor = true;
            this.btnCreateDayBook.Click += new System.EventHandler(this.btnCreateDayBook_Click);
            // 
            // dtpOpenDate
            // 
            this.dtpOpenDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenDate.Location = new System.Drawing.Point(114, 8);
            this.dtpOpenDate.Name = "dtpOpenDate";
            this.dtpOpenDate.Size = new System.Drawing.Size(105, 20);
            this.dtpOpenDate.TabIndex = 93;
            // 
            // lblTransDate
            // 
            this.lblTransDate.AutoSize = true;
            this.lblTransDate.Location = new System.Drawing.Point(5, 8);
            this.lblTransDate.Name = "lblTransDate";
            this.lblTransDate.Size = new System.Drawing.Size(89, 13);
            this.lblTransDate.TabIndex = 0;
            this.lblTransDate.Text = "Transaction Date";
            // 
            // frmDayBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(653, 273);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmDayBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmDayBook_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTransDate;
        private System.Windows.Forms.Button btnCreateDayBook;
        private System.Windows.Forms.DateTimePicker dtpOpenDate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblMessages;
    }
}