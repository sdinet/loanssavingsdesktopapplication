﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.IO;
using SPARC;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using Microsoft.Reporting.WinForms;

namespace SPARC
{
    public partial class DayReportSlumWise : Form
    {
        public DayReportSlumWise()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);

        private void DayReportSlumWise_Load(object sender, EventArgs e)
        {           
            dtpTransDateFrom.MaxDate = DateTime.Now.Date;
            dtpTransDateTo.MaxDate = DateTime.Now.Date;
            this.reportViewer1.RefreshReport();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {            
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("usp_dailyReport");
                SqlCommand cmd;
                SqlDataReader dr;
                string cmdText = @"select cv.Name AS SlumName,TransactionDate, fk_memberid, fk_agentid, m.Name,m.PassbookNumber, u.Fullname, amount as 'Savings_withdraw',0 as 'Savings_Deposit',0 as 'Loan_Paid',0 as 'Loan_EMI', tran_type from 
SavingAccountTransactions sc, Members m,Users U,CodeValue CV
 where Tran_type='DEB'  
and cast(TransactionDate as DATE) between cast(@TransDatefrom as date) and cast(@TransDateto as date) 
and fk_memberid=m.Id and fk_agentid=u.Id and  m.fk_SlumId = cv.Id and
	(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)

UNION

select cv.Name AS SlumName,TransactionDate, fk_memberid, fk_agentid, m.Name,m.PassbookNumber, u.Fullname, 0 as 'Savings_withdraw',amount as 'Savings_Deposit',0 as 'Loan_Paid',0 as 'Loan_EMI', tran_type from 
SavingAccountTransactions sc,Members m,Users U,CodeValue CV
 where Tran_type='CRE' 
and cast(TransactionDate as DATE) between cast(@TransDatefrom as date) and cast(@TransDateto as date)
and fk_memberid=m.Id and fk_agentid=u.Id and m.fk_SlumId=cv.Id and
	(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)

UNION

select cv.Name AS SlumName,TransactionDate, fk_memberid, fk_agentid, m.Name,m.PassbookNumber, u.Fullname, 0 as 'Savings_withdraw',0 as 'Savings_Deposit',amount as 'Loan_Paid',0 as 'Loan_EMI', tran_type from 
LoanAccountTransactions sc, Members m,Users U,CodeValue CV
 where Tran_type='DEB' 
and cast(TransactionDate as DATE) between cast(@TransDatefrom as date) and cast(@TransDateto as date) 
and fk_memberid=m.Id and fk_agentid=u.Id and m.fk_SlumId=cv.Id and
	(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)

UNION

select cv.Name AS SlumName,TransactionDate, fk_memberid, fk_agentid, m.Name,m.PassbookNumber, u.Fullname, 0 as 'Savings_withdraw',0 as 'Savings_Deposit',0 as 'Loan_Paid',amount as 'Loan_EMI', tran_type from 
LoanAccountTransactions sc,Members m,Users U,CodeValue CV
 where Tran_type='CRE' 
and cast(TransactionDate as DATE) between cast(@TransDatefrom as date) and cast(@TransDateto as date) 
and fk_memberid=m.Id and fk_agentid=u.Id and m.fk_SlumId=cv.Id and
	(dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)

ORDER BY TransactionDate desc,Name,Fullname;";
                cmd = new SqlCommand(cmdText, con);
                cmd.Parameters.Add("@TransDatefrom", dtpTransDateFrom.Value);
                cmd.Parameters.Add("@TransDateto", dtpTransDateTo.Value);
                cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);

                //Convert.ToInt32(cmbSlum.SelectedValue));
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.DailySettlementWiseReport.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                   new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }
    }
}
