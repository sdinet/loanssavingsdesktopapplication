﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using System.Configuration;

namespace SPARC
{
    public partial class MemberReport : Form
    {
        public string memid = "";

        public MemberReport()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);

        public MemberReport(long MemberId)
        {
            InitializeComponent();
            memid = MemberId.ToString();
        }

        private void MemberReport_Load(object sender, EventArgs e)
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("Members");
                DataTable dt2 = new DataTable("Loan");
                DataTable dt3 = new DataTable("PrevLoan");
                DataTable dt4 = new DataTable("SavingAccount");
                DataTable dt5 = new DataTable("UPFAccount");


                string cmdText = @"SELECT m.Name,m.MemberId,m.PassbookNumber,m.FatherName,m.DateofBirth,m.Mobile,m.HomePhone,m.Address1,m.Address2,m.Town,m.Zip,m.Photo,
								(Select name from Codevalue where id=[dbo].[udf_FetchHubBySlum] ([fk_SlumId])) as Hub, 
                                (Select name from Codevalue where id=[dbo].[udf_FetchCountryBySlum] ([fk_SlumId])) as CountryName,
                                (Select name from Codevalue where id=[dbo].[udf_FetchStateBySlum] ([fk_SlumId])) as StateName, 
                                (Select name from Codevalue where id=[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId])) as DistrictName,                                
                                (select name from Codevalue where id=[fk_SlumId]) as slumname , 
                                (select name from codevalue where id=[Gender]) as Gender
                                FROM Members M 
                                 WHERE  m.Id=" + Convert.ToInt32(memid) + " ORDER BY [Name];";

                string cmd2 = @"SELECT l.AccountNumber as LoanAccNo,l.LoanTypeValue,l.LoanAmount,l.InterestPercent,l.LoanAppliedDate,l.Status,
                                l.EMISchedule,l.Tenure,l.LoanSanctionedDate,l.AmtDispersed,l.RemainingPrincipal,
                                l.EMITotalPaid,l.EMIPlannedStart,l.EMIPlannedEnd,l.LastPaidDate,l.EMIDueDate
                                FROM Members M inner join vwLoanSummary L
                                on m.Id=l.fk_MemberId
                                WHERE l.Status='Approved' and m.Id=" + Convert.ToInt32(memid) + ";";

                //changing query for member report --start
                string cmd3 = @"select m.Id,m.MemberId,l.AccountNumber as LoanAccNo,l.LoanType as Loantype,l.LoanSanctionedDate as LoanSanctioneddate,
                                l.LoanAmount as LoanAmount,l.InterestPercent as LoanInterestPercentage,l.Status as LoanStatus,
                                l.EMIPrincipalPaid as LoanPrincipalPaid
                                FROM Members M inner join vwLoanSummary L
                                on m.Id=l.fk_MemberId
                                WHERE m.Id=" + Convert.ToInt32(memid) + " and (l.Status='Closed' or l.Status='Write-off') ;";
                //changing query for member report --End

                string cmd4 = @"SELECT
                                s.AccountNumber as SavingsAccNo, s.CurrentBalance,s.Opendate
                                FROM Members M inner join SavingAccount S
                                on  m.Id=s.fk_MemberId 
                                WHERE  m.Id=" + Convert.ToInt32(memid) + " ORDER BY [Name]";

                string cmd5 = @"select u.AccountNumber,opendate as DepositDate,uc.amount as Amount,u.Status,
                                ud.transactiondate as DuePaidDate,ud.Amount as InterestPaid
                                from upfaccount u
                                left join upfaccounttransactions uc on uc.fk_accounid=u.id and uc.Tran_type='CRE' 
                                left join upfaccounttransactions ud on ud.fk_accounid=u.id and ud.Tran_type='DEB' 
                                inner join Members m on m.Id=u.fk_MemberId
                                where m.Id=" + Convert.ToInt32(memid) + " ";

                SqlCommand cmd;
                SqlDataReader dr;
                cmd = new SqlCommand(cmdText, con);
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();


                SqlCommand cmdq = new SqlCommand(cmd2, con);
                SqlDataReader dr2 = cmdq.ExecuteReader();
                if (dr2.HasRows) dt2.Load(dr2);
                dr2.Close();

                SqlCommand cmdp = new SqlCommand(cmd3, con);
                SqlDataReader dr3 = cmdp.ExecuteReader();
                if (dr3.HasRows) dt3.Load(dr3);
                dr3.Close();

                SqlCommand cmds = new SqlCommand(cmd4, con);
                SqlDataReader dr4 = cmds.ExecuteReader();
                if (dr4.HasRows) dt4.Load(dr4);
                dr4.Close();

                SqlCommand cmdt = new SqlCommand(cmd5, con);
                SqlDataReader dr5 = cmdt.ExecuteReader();
                if (dr5.HasRows) dt5.Load(dr5);
                dr5.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.MemberReport.rdlc";

                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));

                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", dt2));

                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet4", dt3));

                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet3", dt4));

                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet5", dt5));

                this.reportViewer1.RefreshReport();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
