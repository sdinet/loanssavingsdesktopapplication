﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using System.Configuration;

namespace SPARC
{
    public partial class UpcomingEMIReport : Form
    {
        public UpcomingEMIReport()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        private void UpcomingEMIReport_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();

            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }

            }                                   
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("UpcomingEMIToday");
                DataTable dt2 = new DataTable("UpcomingEMITomo");
                DataTable dt3 = new DataTable("UpcomingEMIWeek");


                string cmdText = @"select ls.EMIDueDate, m.Name,(select name from Codevalue where id=[fk_SlumId]) as SlumName,ls.AccountNumber,
                                    ls.LoanAmount,(select Name from CodeValue where Id=ls.LoanType) as LoanType,ls.EMIPrincipal as ScheduledEMIAmount,ls.EMIPrincipalPaid as PrincipalPaidTillNow,
                                    ls.EMIChargesPaid as InterestPaidTillNow ,ls.LastPaidDate,ls.RemainingPrincipal as DuePrincipalAmountTillDate,
                                    ls.totalcharges - ls.EMIChargesPaid as DueChargeAmountTillDate from Members M
                                    inner join vwLoanSummary LS 
                                    on m.Id=ls.fk_MemberId
                                    inner join CodeValue C
                                    on m.fk_SlumId = C.Id and
    (dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)
                                    where cast(ls.EMIDueDate as DATE) = CAST(GETDATE() AS DATE)  and ls.Status = 'Approved' and ls.AmtDispersed = ls.LoanAmount;";

                string cmd2 = @"select ls.EMIDueDate, m.Name,(select name from Codevalue where id=[fk_SlumId]) as SlumName,ls.AccountNumber,
                                ls.LoanAmount,(select Name from CodeValue where Id=ls.LoanType) as LoanType,ls.EMIPrincipal as ScheduledEMIAmount,ls.EMIPrincipalPaid as PrincipalPaidTillNow,
                                ls.EMIChargesPaid as InterestPaidTillNow ,ls.LastPaidDate,ls.RemainingPrincipal as DuePrincipalAmountTillDate,
                                ls.totalcharges - ls.EMIChargesPaid as DueChargeAmountTillDate from Members M
                                inner join vwLoanSummary LS 
                                on m.Id=ls.fk_MemberId
                                inner join CodeValue C
                                on m.fk_SlumId = C.Id and
    (dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)
                                where cast(ls.EMIDueDate as DATE) = cast(DATEADD(day, 1, GETDATE()) as DATE)  and ls.Status = 'Approved' and ls.AmtDispersed = ls.LoanAmount;";

                string cmd3 = @"select ls.EMIDueDate, m.Name,(select name from Codevalue where id=[fk_SlumId]) as SlumName,ls.AccountNumber,
                                ls.LoanAmount,(select Name from CodeValue where Id=ls.LoanType) as LoanType,ls.EMIPrincipal as ScheduledEMIAmount,ls.EMIPrincipalPaid as PrincipalPaidTillNow,
                                ls.EMIChargesPaid as InterestPaidTillNow ,ls.LastPaidDate,ls.RemainingPrincipal as DuePrincipalAmountTillDate,
                                ls.totalcharges - ls.EMIChargesPaid as DueChargeAmountTillDate from Members M
                                inner join vwLoanSummary LS 
                                on m.Id=ls.fk_MemberId
                                inner join CodeValue C
                                on m.fk_SlumId = C.Id and
    (dbo.udf_FetchHubBySlum(fk_SlumId) =@fk_HubId  OR @fk_HubId IS NULL) AND 
	(dbo.udf_FetchCountryBySlum(fk_SlumId)=@fk_CountryId  OR @fk_CountryId IS NULL) AND 
	(dbo.udf_FetchStateBySlum(fk_SlumId)=@fk_StateId  OR @fk_StateId IS NULL) AND 
	(dbo.udf_FetchDistrictBySlum(fk_SlumId)=@fk_DistrictId  OR @fk_DistrictId IS NULL) AND 
	(fk_SlumId=@fk_SlumId  OR @fk_SlumId IS NULL)
                                where cast(ls.EMIDueDate as DATE) between cast(DATEADD(day, 2, GETDATE()) as DATE) and cast(DATEADD(day, 6, GETDATE()) as DATE)   and ls.Status = 'Approved' and ls.AmtDispersed = ls.LoanAmount;";


                SqlCommand cmd;
                SqlDataReader dr;
                cmd = new SqlCommand(cmdText, con);
                cmd.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmd.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);
                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();
               

                SqlCommand cmda;
                SqlDataReader da;
                cmda = new SqlCommand(cmd2, con);
                cmda.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmda.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmda.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmda.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmda.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);
                da = cmda.ExecuteReader();
                if (da.HasRows) dt2.Load(da);
                da.Close();

                SqlCommand cmdb;
                SqlDataReader db;
                cmdb = new SqlCommand(cmd3, con);
                cmdb.Parameters.Add("@fk_HubId", cmbHub.SelectedIndex > 0 ? Convert.ToInt32(cmbHub.SelectedValue.ToString()) : (object)DBNull.Value);
                cmdb.Parameters.Add("@fk_CountryId", cmbCountry.SelectedIndex > 0 ? Convert.ToInt16(cmbCountry.SelectedValue.ToString()) : (object)DBNull.Value);
                cmdb.Parameters.Add("@fk_StateId", cmbState.SelectedIndex > 0 ? Convert.ToInt32(cmbState.SelectedValue.ToString()) : (object)DBNull.Value);
                cmdb.Parameters.Add("@fk_DistrictId", cmbDistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbDistrict.SelectedValue.ToString()) : (object)DBNull.Value);
                cmdb.Parameters.Add("@fk_SlumId", cmbSlum.SelectedIndex > 0 ? Convert.ToInt32(cmbSlum.SelectedValue.ToString()) : (object)DBNull.Value);
                db = cmdb.ExecuteReader();
                if (db.HasRows) dt3.Load(db);
                db.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.UpcomingEMIReport.rdlc";


                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));

                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", dt2));

                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet3", dt3));

                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;            
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Taluk
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }
    }
}
