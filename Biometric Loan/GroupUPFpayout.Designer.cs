﻿namespace SPARC
{
    partial class GroupUPFpayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbgrp = new System.Windows.Forms.ComboBox();
            this.Group = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMessages = new System.Windows.Forms.Label();
            this.dgUPFAccount = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpenDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayOutEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.GT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.lblIntAsOnDate = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblIntToBePaid = new System.Windows.Forms.Label();
            this.lblAccOpenDate = new System.Windows.Forms.Label();
            this.lblIntDueVal = new System.Windows.Forms.Label();
            this.lblDepAmtVal = new System.Windows.Forms.Label();
            this.Nameofthegroup = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.cmbPayMode = new System.Windows.Forms.ComboBox();
            this.lblPayMode = new System.Windows.Forms.Label();
            this.txtPrincipalWithdraw = new System.Windows.Forms.TextBox();
            this.txtIntPayout = new System.Windows.Forms.TextBox();
            this.radPricipalAmount = new System.Windows.Forms.RadioButton();
            this.radIntPayout = new System.Windows.Forms.RadioButton();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.dtpTransDate = new System.Windows.Forms.DateTimePicker();
            this.btnCalIntAsOnDate = new System.Windows.Forms.Button();
            this.lblInterestTobePaid = new System.Windows.Forms.Label();
            this.lblIntPayDate = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbgrp
            // 
            this.cmbgrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbgrp.FormattingEnabled = true;
            this.cmbgrp.Location = new System.Drawing.Point(713, 9);
            this.cmbgrp.Name = "cmbgrp";
            this.cmbgrp.Size = new System.Drawing.Size(107, 21);
            this.cmbgrp.TabIndex = 163;
            this.cmbgrp.SelectedIndexChanged += new System.EventHandler(this.cmbgrp_SelectedIndexChanged);
            // 
            // Group
            // 
            this.Group.AutoSize = true;
            this.Group.Location = new System.Drawing.Point(660, 14);
            this.Group.Name = "Group";
            this.Group.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Group.Size = new System.Drawing.Size(36, 13);
            this.Group.TabIndex = 162;
            this.Group.Text = "Group";
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(515, 12);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(123, 21);
            this.cmbslum.TabIndex = 155;
            this.cmbslum.SelectedIndexChanged += new System.EventHandler(this.cmbslum_SelectedIndexChanged);
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(288, 9);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(121, 21);
            this.cmbdistrict.TabIndex = 154;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(424, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 161;
            this.label7.Text = "Slum/Settlement";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(221, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 160;
            this.label5.Text = "District/City";
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(109, 6);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(106, 21);
            this.cmbstate.TabIndex = 153;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 159;
            this.label8.Text = "State/Province";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblMessages);
            this.groupBox1.Controls.Add(this.dgUPFAccount);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lblIntAsOnDate);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblAgentName);
            this.groupBox1.Controls.Add(this.cmbAgentName);
            this.groupBox1.Controls.Add(this.lblIntToBePaid);
            this.groupBox1.Controls.Add(this.lblAccOpenDate);
            this.groupBox1.Controls.Add(this.lblIntDueVal);
            this.groupBox1.Controls.Add(this.lblDepAmtVal);
            this.groupBox1.Controls.Add(this.Nameofthegroup);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.lblRemarks);
            this.groupBox1.Controls.Add(this.cmbPayMode);
            this.groupBox1.Controls.Add(this.lblPayMode);
            this.groupBox1.Controls.Add(this.txtPrincipalWithdraw);
            this.groupBox1.Controls.Add(this.txtIntPayout);
            this.groupBox1.Controls.Add(this.radPricipalAmount);
            this.groupBox1.Controls.Add(this.radIntPayout);
            this.groupBox1.Controls.Add(this.lblTransactionDate);
            this.groupBox1.Controls.Add(this.dtpTransDate);
            this.groupBox1.Controls.Add(this.btnCalIntAsOnDate);
            this.groupBox1.Controls.Add(this.lblInterestTobePaid);
            this.groupBox1.Controls.Add(this.lblIntPayDate);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(42, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(896, 575);
            this.groupBox1.TabIndex = 164;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(157, 355);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 153;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgUPFAccount
            // 
            this.dgUPFAccount.AllowUserToAddRows = false;
            this.dgUPFAccount.AllowUserToDeleteRows = false;
            this.dgUPFAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUPFAccount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUPFAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUPFAccount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.AccountNumber,
            this.GroupName,
            this.OpenDate,
            this.CurrentBalance,
            this.Amount,
            this.TransactionDate,
            this.Remarks,
            this.Status,
            this.PayOutEndDate,
            this.btnDelete,
            this.GT_ID});
            this.dgUPFAccount.Location = new System.Drawing.Point(34, 388);
            this.dgUPFAccount.Margin = new System.Windows.Forms.Padding(0);
            this.dgUPFAccount.Name = "dgUPFAccount";
            this.dgUPFAccount.ReadOnly = true;
            this.dgUPFAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUPFAccount.Size = new System.Drawing.Size(846, 187);
            this.dgUPFAccount.TabIndex = 152;
            this.dgUPFAccount.TabStop = false;
            this.dgUPFAccount.VirtualMode = true;
            this.dgUPFAccount.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellClick_1);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // AccountNumber
            // 
            this.AccountNumber.DataPropertyName = "GroupNumber";
            this.AccountNumber.HeaderText = "AccountNumber";
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.ReadOnly = true;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            this.GroupName.HeaderText = "Group Name";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            // 
            // OpenDate
            // 
            this.OpenDate.DataPropertyName = "Opendate";
            this.OpenDate.HeaderText = "OpenDate";
            this.OpenDate.Name = "OpenDate";
            this.OpenDate.ReadOnly = true;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentBalance";
            this.CurrentBalance.HeaderText = "CurrentBalance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Interest Paid";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            this.TransactionDate.HeaderText = "Interest PaidOn";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // PayOutEndDate
            // 
            this.PayOutEndDate.DataPropertyName = "PayOutEndDate";
            this.PayOutEndDate.HeaderText = "PayOutEndDate";
            this.PayOutEndDate.Name = "PayOutEndDate";
            this.PayOutEndDate.ReadOnly = true;
            this.PayOutEndDate.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            // 
            // GT_ID
            // 
            this.GT_ID.DataPropertyName = "GT_Id";
            this.GT_ID.HeaderText = "GT_ID";
            this.GT_ID.Name = "GT_ID";
            this.GT_ID.ReadOnly = true;
            this.GT_ID.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(265, 325);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 150;
            this.button1.Text = "Pay";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblIntAsOnDate
            // 
            this.lblIntAsOnDate.AutoSize = true;
            this.lblIntAsOnDate.Location = new System.Drawing.Point(517, 143);
            this.lblIntAsOnDate.Name = "lblIntAsOnDate";
            this.lblIntAsOnDate.Size = new System.Drawing.Size(0, 13);
            this.lblIntAsOnDate.TabIndex = 149;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(587, 64);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 147;
            this.lblStatus.Text = "Status";
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(198, 255);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(107, 13);
            this.lblAgentName.TabIndex = 146;
            this.lblAgentName.Text = "Pay through Leader *";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(322, 252);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(130, 21);
            this.cmbAgentName.TabIndex = 145;
            // 
            // lblIntToBePaid
            // 
            this.lblIntToBePaid.AutoSize = true;
            this.lblIntToBePaid.Location = new System.Drawing.Point(702, 43);
            this.lblIntToBePaid.Name = "lblIntToBePaid";
            this.lblIntToBePaid.Size = new System.Drawing.Size(0, 13);
            this.lblIntToBePaid.TabIndex = 140;
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.AutoSize = true;
            this.lblAccOpenDate.Location = new System.Drawing.Point(702, 16);
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Size = new System.Drawing.Size(0, 13);
            this.lblAccOpenDate.TabIndex = 139;
            // 
            // lblIntDueVal
            // 
            this.lblIntDueVal.AutoSize = true;
            this.lblIntDueVal.Location = new System.Drawing.Point(180, 63);
            this.lblIntDueVal.Name = "lblIntDueVal";
            this.lblIntDueVal.Size = new System.Drawing.Size(0, 13);
            this.lblIntDueVal.TabIndex = 138;
            // 
            // lblDepAmtVal
            // 
            this.lblDepAmtVal.AutoSize = true;
            this.lblDepAmtVal.Location = new System.Drawing.Point(173, 28);
            this.lblDepAmtVal.Name = "lblDepAmtVal";
            this.lblDepAmtVal.Size = new System.Drawing.Size(0, 13);
            this.lblDepAmtVal.TabIndex = 137;
            // 
            // Nameofthegroup
            // 
            this.Nameofthegroup.AutoSize = true;
            this.Nameofthegroup.Location = new System.Drawing.Point(157, 43);
            this.Nameofthegroup.Name = "Nameofthegroup";
            this.Nameofthegroup.Size = new System.Drawing.Size(0, 13);
            this.Nameofthegroup.TabIndex = 136;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(371, 325);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 23);
            this.btnClose.TabIndex = 134;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemarks.Location = new System.Drawing.Point(322, 280);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(195, 39);
            this.txtRemarks.TabIndex = 132;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(198, 287);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 131;
            this.lblRemarks.Text = "Remarks";
            // 
            // cmbPayMode
            // 
            this.cmbPayMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPayMode.FormattingEnabled = true;
            this.cmbPayMode.Items.AddRange(new object[] {
            "Select",
            "Cash",
            "Bank"});
            this.cmbPayMode.Location = new System.Drawing.Point(322, 223);
            this.cmbPayMode.Name = "cmbPayMode";
            this.cmbPayMode.Size = new System.Drawing.Size(130, 21);
            this.cmbPayMode.TabIndex = 129;
            this.cmbPayMode.SelectedIndexChanged += new System.EventHandler(this.cmbPayMode_SelectedIndexChanged);
            // 
            // lblPayMode
            // 
            this.lblPayMode.AutoSize = true;
            this.lblPayMode.Location = new System.Drawing.Point(198, 223);
            this.lblPayMode.Name = "lblPayMode";
            this.lblPayMode.Size = new System.Drawing.Size(55, 13);
            this.lblPayMode.TabIndex = 130;
            this.lblPayMode.Text = "Pay Mode";
            // 
            // txtPrincipalWithdraw
            // 
            this.txtPrincipalWithdraw.Location = new System.Drawing.Point(322, 163);
            this.txtPrincipalWithdraw.MaxLength = 10;
            this.txtPrincipalWithdraw.Name = "txtPrincipalWithdraw";
            this.txtPrincipalWithdraw.Size = new System.Drawing.Size(130, 20);
            this.txtPrincipalWithdraw.TabIndex = 126;
            // 
            // txtIntPayout
            // 
            this.txtIntPayout.Location = new System.Drawing.Point(323, 138);
            this.txtIntPayout.MaxLength = 10;
            this.txtIntPayout.Name = "txtIntPayout";
            this.txtIntPayout.Size = new System.Drawing.Size(130, 20);
            this.txtIntPayout.TabIndex = 125;
            // 
            // radPricipalAmount
            // 
            this.radPricipalAmount.AutoSize = true;
            this.radPricipalAmount.Location = new System.Drawing.Point(198, 164);
            this.radPricipalAmount.Name = "radPricipalAmount";
            this.radPricipalAmount.Size = new System.Drawing.Size(121, 17);
            this.radPricipalAmount.TabIndex = 124;
            this.radPricipalAmount.TabStop = true;
            this.radPricipalAmount.Text = "Principal Withdrawal";
            this.radPricipalAmount.UseVisualStyleBackColor = true;
            this.radPricipalAmount.CheckedChanged += new System.EventHandler(this.radPricipalAmount_CheckedChanged_1);
            // 
            // radIntPayout
            // 
            this.radIntPayout.AutoSize = true;
            this.radIntPayout.Location = new System.Drawing.Point(198, 139);
            this.radIntPayout.Name = "radIntPayout";
            this.radIntPayout.Size = new System.Drawing.Size(96, 17);
            this.radIntPayout.TabIndex = 123;
            this.radIntPayout.TabStop = true;
            this.radIntPayout.Text = "Interest Payout";
            this.radIntPayout.UseVisualStyleBackColor = true;
            this.radIntPayout.CheckedChanged += new System.EventHandler(this.radIntPayout_CheckedChanged_1);
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(198, 197);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(109, 13);
            this.lblTransactionDate.TabIndex = 128;
            this.lblTransactionDate.Text = "Interest Pay Out Date";
            // 
            // dtpTransDate
            // 
            this.dtpTransDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransDate.Location = new System.Drawing.Point(322, 191);
            this.dtpTransDate.Name = "dtpTransDate";
            this.dtpTransDate.Size = new System.Drawing.Size(130, 20);
            this.dtpTransDate.TabIndex = 127;
            // 
            // btnCalIntAsOnDate
            // 
            this.btnCalIntAsOnDate.Location = new System.Drawing.Point(286, 102);
            this.btnCalIntAsOnDate.Name = "btnCalIntAsOnDate";
            this.btnCalIntAsOnDate.Size = new System.Drawing.Size(181, 30);
            this.btnCalIntAsOnDate.TabIndex = 122;
            this.btnCalIntAsOnDate.Text = "Calculate Interest As On Date";
            this.btnCalIntAsOnDate.UseVisualStyleBackColor = true;
            this.btnCalIntAsOnDate.Click += new System.EventHandler(this.btnCalIntAsOnDate_Click);
            // 
            // lblInterestTobePaid
            // 
            this.lblInterestTobePaid.AutoSize = true;
            this.lblInterestTobePaid.Location = new System.Drawing.Point(587, 43);
            this.lblInterestTobePaid.Name = "lblInterestTobePaid";
            this.lblInterestTobePaid.Size = new System.Drawing.Size(93, 13);
            this.lblInterestTobePaid.TabIndex = 121;
            this.lblInterestTobePaid.Text = "Interest to be Paid";
            // 
            // lblIntPayDate
            // 
            this.lblIntPayDate.AutoSize = true;
            this.lblIntPayDate.Location = new System.Drawing.Point(64, 63);
            this.lblIntPayDate.Name = "lblIntPayDate";
            this.lblIntPayDate.Size = new System.Drawing.Size(80, 13);
            this.lblIntPayDate.TabIndex = 120;
            this.lblIntPayDate.Text = "Interest Due on";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(64, 28);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(70, 13);
            this.lblAmount.TabIndex = 119;
            this.lblAmount.Text = "Total Deposit";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(587, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 118;
            this.label1.Text = "Account open date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(702, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 154;
            // 
            // GroupUPFpayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1000, 603);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbgrp);
            this.Controls.Add(this.Group);
            this.Controls.Add(this.cmbslum);
            this.Controls.Add(this.cmbdistrict);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbstate);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GroupUPFpayout";
            this.Text = "GroupUPFpayout";
            this.Load += new System.EventHandler(this.GroupUPFpayout_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbgrp;
        private System.Windows.Forms.Label Group;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblIntPayDate;
        private System.Windows.Forms.Label lblInterestTobePaid;
        private System.Windows.Forms.Button btnCalIntAsOnDate;
        private System.Windows.Forms.TextBox txtPrincipalWithdraw;
        private System.Windows.Forms.TextBox txtIntPayout;
        private System.Windows.Forms.RadioButton radPricipalAmount;
        private System.Windows.Forms.RadioButton radIntPayout;
        private System.Windows.Forms.Label lblTransactionDate;
        private System.Windows.Forms.DateTimePicker dtpTransDate;
        private System.Windows.Forms.ComboBox cmbPayMode;
        private System.Windows.Forms.Label lblPayMode;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblDepAmtVal;
        private System.Windows.Forms.Label Nameofthegroup;
        private System.Windows.Forms.Label lblIntDueVal;
        private System.Windows.Forms.Label lblIntToBePaid;
        private System.Windows.Forms.Label lblAccOpenDate;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblIntAsOnDate;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.DataGridView dgUPFAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OpenDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayOutEndDate;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn GT_ID;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Label label2;
    }
}