﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Timers;
using System.Windows.Threading;

namespace SPARC
{
    public partial class GroupTransaction : Form
    {
        public GroupTransaction()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private Int64 iGtransID = 0;
        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;


        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {

                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }

        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }
        private string DoValidationsSavRep()
        {
            string ErrorMsg = string.Empty;

            if (cmbslum.SelectedIndex <= 0)
                ErrorMsg = Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            if (cmbTypeSavRe.SelectedIndex < 0)
                ErrorMsg += Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;
            if (txtSavReTotal.Text.Trim().Length == 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            if (cmbgrp.SelectedIndex <= 0)
                ErrorMsg = Messages.Groupmessage + Messages.COMMAWITHSPACE;
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                lblMessages.ForeColor = Color.Red;
            }

            return ErrorMsg;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //dgvSavRe.DataBindings.Clear();
         //   dgvSavRe.DataSource = null;
            lblMessages.Text = string.Empty;
            lblmsg.Text = string.Empty;
            string strMessage = DoValidationsSavRep();
            if (strMessage.Length > 0)
            {
                lblMessages.Text = strMessage;
            }

            else
            {
                lblMessages.Text = string.Empty;

                if (dgvSavRe.Rows.Count == 0)
                {
                    int Row = 0;
                    DataGridViewColumn col = new DataGridViewTextBoxColumn();

                    dgvSavRe.Rows.Add();
                    Row = dgvSavRe.Rows.Count - 1;
                    dgvSavRe[0, Row].Value = dtpAccountDate.Text;
                    dgvSavRe[1, Row].Value = cmbTypeSavRe.Text;
                    dgvSavRe[2, Row].Value = txtSavReTotal.Text;
                    dgvSavRe[3, Row].Value = cmbgrp.Text;
                    dgvSavRe[4, Row].Value = cmbslum.Text;
                    dgvSavRe[5, Row].Value = "New";
                    dgvSavRe[6, Row].Value = iGtransID;
                    dgvSavRe[8, Row].Value = textBox1.Text;
                    dgvSavRe[9, Row].Value = cmbslum.SelectedValue;
                    dgvSavRe[10, Row].Value = cmbgrp.SelectedValue;
                    txtSavReTotal.Text = string.Empty;
                    cmbTypeSavRe.SelectedIndex = -1;
                    cmbslum.SelectedIndex = 0;

                }
                else
                {
                    try
                    {
                        DataTable dt = new DataTable();

                        DataTable dataTable = (DataTable)dgvSavRe.DataSource;
                        DataRow drToAdd = dataTable.NewRow();

                        drToAdd[0] = dtpAccountDate.Text;
                        drToAdd[1] = cmbTypeSavRe.Text;
                        drToAdd[2] = txtSavReTotal.Text;
                        drToAdd[3] = cmbgrp.Text ;
                        drToAdd[4] = cmbslum.Text;
                        drToAdd[5] = "New";
                        drToAdd[6] = iGtransID;
                        drToAdd[7] = textBox1.Text;
                        drToAdd[9] = cmbslum.SelectedValue;
                        drToAdd[10] = cmbgrp.SelectedValue;


                        dataTable.Rows.Add(drToAdd);
                        dataTable.AcceptChanges();
                        txtSavReTotal.Text = String.Empty;
                        cmbgrp.SelectedValue = 0;

                        cmbTypeSavRe.SelectedIndex = -1;

                        txtSavReTotal.Text = string.Empty;
                       textBox1.Text = string.Empty;
                    }
                    catch
                    {
                        MessageBox.Show("Please save the Transactions before adding New Transaction ");
                    }

                }
              //  if (dgvSavRe.RowCount == 0)
              //  {
              //      dgvSavRe.Rows.Add();
              //  }
              //  int Row = 0;
              //Row = dgvSavRe.Rows.Count - 1;
              //  dgvSavRe[0, Row].Value = dtpAccountDate.Text;
              //  dgvSavRe[1, Row].Value = cmbTypeSavRe.Text;
              //  dgvSavRe[2, Row].Value = txtSavReTotal.Text;
              //  dgvSavRe[3, Row].Value = cmbgrp.Text;
              //  dgvSavRe[4, Row].Value = cmbslum.Text;
              //  dgvSavRe[5, Row].Value = "New";
              //  dgvSavRe[6, Row].Value = iGtransID;
              //  dgvSavRe[8, Row].Value = textBox1.Text;
              //  dgvSavRe[9, Row].Value = cmbslum.SelectedValue;
              //  dgvSavRe[10, Row].Value = cmbgrp.SelectedValue;
              //  dgvSavRe.Refresh();
              //  txtSavReTotal.Text = string.Empty;
              //  cmbTypeSavRe.SelectedIndex = -1;
              //  cmbslum.SelectedIndex = 0;
                foreach (DataGridViewRow row in dgvSavRe.Rows)
                {
                    string RowType = row.Cells[5].Value.ToString();

                if (RowType == "New")
                {
                    row.DefaultCellStyle.BackColor = Color.LightBlue;
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
                   
                }
            }  
        }
        DateTime SavReDate;
        Int64 SlumId;
        string SavReType;
        double SavReAmount;
        Int64 GroupId;
        string Remarkss;
       
        private void btnSavReSave_Click(object sender, EventArgs e)
        {
            lblMessages.Text = string.Empty;
            lblmsg.Text = string.Empty;
            int i = 0;
            if (dgvSavRe.Rows.Count > 0)
            {
                Int32 iResult;
                for (i = 0; i < dgvSavRe.Rows.Count; i++)
                {
                    if (dgvSavRe.Rows[i].Cells[5].Value == "New")
                    {

                        SlumId = Convert.ToInt64(dgvSavRe.Rows[i].Cells[9].Value);
                        SavReDate = Convert.ToDateTime(dgvSavRe.Rows[i].Cells["Date"].Value);


                        SavReType = Convert.ToString(dgvSavRe.Rows[i].Cells["Type"].Value);
                        SavReAmount = Convert.ToDouble(dgvSavRe.Rows[i].Cells["Amount"].Value);
                        GroupId = Convert.ToInt64(dgvSavRe.Rows[i].Cells[10].Value);
                        Remarkss = Convert.ToString(dgvSavRe.Rows[i].Cells["Remarks"].Value);

                        GroupTrans objgr = new GroupTrans();
                        objgr.Id = iGtransID;
                        objgr.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                        objgr.fk_CountryId = Convert.ToInt32(cmbcountry.SelectedValue);
                        objgr.fk_StateId = Convert.ToInt32(cmbstate.SelectedValue);
                        objgr.fk_DistricId = Convert.ToInt32(cmbdistrict.SelectedValue);
                        objgr.fk_SlumId = Convert.ToInt32(SlumId);
                        objgr.GroupId = Convert.ToInt32(GroupId);
                        objgr.Date = SavReDate;
                        objgr.Savingtype = Convert.ToString(SavReType);
                        objgr.Amount = Convert.ToDouble(SavReAmount);
                        objgr.Createdby = GlobalValues.User_PkId;
                        objgr.Updatedby = GlobalValues.User_PkId;
                        objgr.CreatedDate = DateTime.Today;
                        objgr.UpdatedDate = DateTime.Today;
                        objgr.Remarks = Remarkss;

                       if(objgr.Savingtype=="Savings")
                        {
                            objgr.Tran_type="CRE";
                        }
                        if (objgr.Savingtype=="UPF")
                        {
                            objgr.Tran_type = "CRE";
                        }
                        if (objgr.Savingtype=="Repayment" ) 
                        {
                            objgr.Tran_type = "DEB";
                        }
                        if (objgr.Savingtype == "Withdrawal")
                        {
                            objgr.Tran_type = "DEB";
                        }
                        iResult = objgr.SaveGroupTrans();
                        //objgr.UpdateCurrentbalance();
                  
                        if (iResult > 0)
                        {
                           // if (iGtransID == 0)
                         //   {
                                dgvSavRe.Rows[i].Cells[5].Value = "";
                                //dgvSavRe.DataSource = null;
                                // dgvSavRe.Rows.Clear();
                                txtSavReTotal.Text = string.Empty;
                                cmbTypeSavRe.SelectedIndex = -1;
                                cmbslum.SelectedIndex = 0;
                                textBox1.Text = string.Empty;
                                dgvSavRe.Refresh();
                                lblmsg.Text = Messages.GroupTrans;
                                lblmsg.ForeColor = Color.Green;

                           // }
                           // else
                            //{
                              

                           // }
                        }
                    }
                    else
                    {
                        //lblmsg.Text = Messages.Transsaved;
                        //lblmsg.ForeColor = Color.Green;
                        //dgvSavRe.DataSource = null;
                        //dgvSavRe.Rows.Clear();
                    }

                }                       
              }  
            else    
            {
                      lblmsg.Text = Messages.GroupNodata;
                      lblmsg.ForeColor = Color.Red;
             }
        }
        private void cmbslum_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessages.Text = string.Empty;
            lblmsg.Text = string.Empty;
            int j = 0;
            if (dgvSavRe.Rows.Count > 0)
            {
                for (int i = 0; i < dgvSavRe.Rows.Count; i++)
                {
                    if (dgvSavRe.Rows[i].Cells[5].Value == "New")
                    {
                        //lblmsg.Text = Messages.SaveGroupTrans;
                        //lblmsg.ForeColor = Color.Green;

                        j++;
                    }
                }
                if (j == 0)
                {
                    if (cmbslum.SelectedIndex > 0)
                    {
                        GroupTrans objgt = new GroupTrans();
                        objgt.fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?)null;
                        objgt.Date = dtpAccountDate.Value;
                        DataSet dsgrp = objgt.SearchSlumTrans();
                        if (dsgrp.Tables[0].Rows.Count > 0)
                        {

                            dgvSavRe.AutoGenerateColumns = false;
                            dgvSavRe.DataSource = dsgrp.Tables[0];
                        }
                        else
                        {
                            lblmsg.Text = Messages.GroupTransupnodata;
                            lblmsg.ForeColor = Color.Green;
                            dgvSavRe.DataSource = null;
                        }
                        txtSavReTotal.Text = string.Empty;
                        cmbTypeSavRe.SelectedIndex = -1;
                    }
                    else
                    {
                        dgvSavRe.DataSource = null;
                    }

                }


            }
            else
            {
                if (cmbslum.SelectedIndex > 0)
                {
                    GroupTrans objgt = new GroupTrans();
                    objgt.fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?)null;
                    objgt.Date = dtpAccountDate.Value;
                    DataSet dsgrp = objgt.SearchSlumTrans();
                    if (dsgrp.Tables[0].Rows.Count > 0)
                    {

                        dgvSavRe.AutoGenerateColumns = false;
                        dgvSavRe.DataSource = dsgrp.Tables[0];
                    }
                    else
                    {
                        lblmsg.Text = Messages.GroupTransupnodata;
                        lblmsg.ForeColor = Color.Green;
                        dgvSavRe.DataSource = null;
                    }
                    txtSavReTotal.Text = string.Empty;
                    cmbTypeSavRe.SelectedIndex = -1;
                }
            }
          
                
                txtSavReTotal.Text = string.Empty;
                cmbTypeSavRe.SelectedIndex = -1;
                

               FillCombogroup(Convert.ToInt32(cmbslum.SelectedValue));

           

        }  
            
            
        
        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();
            DataRow dr = dsgroup.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = "--Select One--";

            dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbgrp.DisplayMember = "Group_Name";
                cmbgrp.ValueMember = "Id";
                cmbgrp.DataSource = dsgroup.Tables[0];


            }
            else
            {
                cmbgrp.DataSource = null;
              

            }
        }

        private void GroupTransaction_Load(object sender, EventArgs e)
        {
            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
        
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                
            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }
         
            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the state type code and populate state 
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                    cmbcountry.SelectedValue = Id;
                    cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the District type code and populate district
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                    cmbstate.SelectedValue = Id;
                    cmbstate.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the Slum type code and populate Slum
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                    cmbdistrict.SelectedValue = Id;
                    cmbdistrict.Enabled = false;
                }
            }

            dtpAccountDate.MaxDate = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dtpAccountDate_ValueChanged(object sender, EventArgs e)
        {

            GroupTrans objgts = new GroupTrans();
            objgts.Date = dtpAccountDate.Value;
            DataSet dsgrp = objgts.SearchGroupDateTrans();
            if (dsgrp.Tables[0].Rows.Count > 0)
            {

                dgvSavRe.AutoGenerateColumns = false;
                dgvSavRe.DataSource = dsgrp.Tables[0];
            }
            else
            {
                dgvSavRe.DataSource = null;
            }
         
        }

        private void dgvSavRe_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }

        private void dgvSavRe_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        public void loadcontrols(GroupTrans obj)
        {
            try
            {
                DataSet dsgrptrans = obj.getdetails();

                if (dsgrptrans != null && dsgrptrans.Tables.Count > 0 && dsgrptrans.Tables[0].Rows.Count > 0)
                {


                    cmbHub.SelectedValue = Convert.ToInt16(dsgrptrans.Tables[0].Rows[0]["fk_HubId"]);
                    cmbcountry.SelectedValue = dsgrptrans.Tables[0].Rows[0]["fk_CountryId"];
                    cmbstate.SelectedValue = Convert.ToInt16(dsgrptrans.Tables[0].Rows[0]["fk_StateId"]);
                    cmbdistrict.SelectedValue = Convert.ToInt16(dsgrptrans.Tables[0].Rows[0]["fk_DistrictId"]);
                    cmbslum.SelectedValue = Convert.ToInt16(dsgrptrans.Tables[0].Rows[0]["fk_SlumId"]);
                    txtSavReTotal.SelectedText = dsgrptrans.Tables[0].Rows[0]["Amount"].ToString();
                    cmbTypeSavRe.SelectedItem = dsgrptrans.Tables[0].Rows[0]["type"].ToString();
                    cmbgrp.SelectedValue = dsgrptrans.Tables[0].Rows[0]["fk_GroupId"];
                    dtpAccountDate.Value = Convert.ToDateTime(dsgrptrans.Tables[0].Rows[0]["TransactionDate"]);
                    textBox1.Text = dsgrptrans.Tables[0].Rows[0]["Remarks"].ToString();
                }
            }
            catch (Exception)
            {

            }
        }

        private void dgvSavRe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           



        }

        private void dgvSavRe_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
              
                GroupTrans obj = new GroupTrans();
                if (e.RowIndex >= 0)
                {

                    obj.Id = Convert.ToInt32(dgvSavRe.Rows[e.RowIndex].Cells[6].Value.ToString());
                }
                ClearControls();
                loadcontrols(obj);
                dgvSavRe.AutoGenerateColumns = false;
                iGtransID = obj.Id;
            }
            catch (Exception)
            {

            }
        }
        private void dgvSavRe_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                GroupTrans obj = new GroupTrans();
                if (e.RowIndex >= 0)
                {

                    obj.Id = Convert.ToInt32(dgvSavRe.Rows[e.RowIndex].Cells[6].Value.ToString());
                }
                ClearControls();
                loadcontrols(obj);
                dgvSavRe.AutoGenerateColumns = false;
                iGtransID = obj.Id;
            }
            catch (Exception)
            {

            }
            
        }
        private void ClearControls()
        {
            txtSavReTotal.Text = string.Empty;

            cmbslum.SelectedValue = 0;
            cmbTypeSavRe.SelectedIndex = -1;
            cmbgrp.SelectedValue=0;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSavRe.Rows.Count > 0)
                {
                    GroupTrans objgr = new GroupTrans();
                    objgr.Id = iGtransID;
                    objgr.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                    objgr.fk_CountryId = Convert.ToInt32(cmbcountry.SelectedValue);
                    objgr.fk_StateId = Convert.ToInt32(cmbstate.SelectedValue);
                    objgr.fk_DistricId = Convert.ToInt32(cmbdistrict.SelectedValue);
                    objgr.fk_SlumId = Convert.ToInt32(cmbslum.SelectedValue);
                    objgr.GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
                    objgr.Date = dtpAccountDate.Value;
                    objgr.Savingtype = Convert.ToString(cmbTypeSavRe.SelectedItem);
                    objgr.Amount = Convert.ToDouble(txtSavReTotal.Text);
                    objgr.Createdby = GlobalValues.User_PkId;
                    objgr.Updatedby = GlobalValues.User_PkId;
                    objgr.CreatedDate = DateTime.Today;
                    objgr.UpdatedDate = DateTime.Today;
                    objgr.Remarks = textBox1.Text;
                    if (objgr.Savingtype == "Savings")
                    {
                        objgr.Tran_type = "CRE";
                    }
                    if (objgr.Savingtype == "UPF")
                    {
                        objgr.Tran_type = "CRE";
                    }
                    if (objgr.Savingtype == "Repayment")
                    {
                        objgr.Tran_type = "DEB";
                    }
                    if (objgr.Savingtype == "Withdrawal")
                    {
                        objgr.Tran_type = "DEB";
                    }
                    // Int64 iResult = 0;
                    DataSet dsgrp = objgr.UpdateGroupTrans();
                  

                    lblmsg.Text = Messages.GroupTransupdate;
                    lblmsg.ForeColor = Color.Green;
                    loadTransaction();
                }
                else
                {
                    lblmsg.Text = Messages.GroupTransNoupdate;
                    lblmsg.ForeColor = Color.Green;
                }
            
            
        }
            catch (Exception)
            {

            } 
        }
        
        private void loadTransaction()
        {
            //loads the grid
            GroupTrans objgrp = new GroupTrans();
          objgrp.fk_SlumId=Convert.ToInt32(cmbslum.SelectedValue);
          objgrp.GroupId = Convert.ToInt32(cmbgrp.SelectedValue);

          DataSet ds = objgrp.LoadTransactiontogrid();
          if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
          {
              dgvSavRe.AutoGenerateColumns = false;
              dgvSavRe.DataSource = ds.Tables[0];
          }
          

        }

        private void cmbgrp_SelectedIndexChanged(object sender, EventArgs e)
        { 
            int j=0;


            if (dgvSavRe.Rows.Count > 0)
            {

                for (int i = 0; i < dgvSavRe.Rows.Count; i++)
                {
                    if (dgvSavRe.Rows[i].Cells[5].Value == "New")
                    {
                     
                        j++;
                    }

                }
                if (j == 0)
                {
                    if (cmbgrp.SelectedIndex > 0)
                    {
                        GroupTrans objgts = new GroupTrans();

                        objgts.GroupId = cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (int?)null;
                        objgts.Date = dtpAccountDate.Value;
                        DataSet dsgrp = objgts.SearchGroupTrans();
                        if (dsgrp.Tables[0].Rows.Count > 0)
                        {

                            dgvSavRe.AutoGenerateColumns = false;
                            dgvSavRe.DataSource = dsgrp.Tables[0];
                        }
                        else
                        {
                            dgvSavRe.DataSource = null;
                        }
                    }
                }

            }
            else
            
            {
                if (cmbgrp.SelectedIndex > 0)
                {
                    GroupTrans objgts = new GroupTrans();

                    objgts.GroupId = cmbgrp.SelectedIndex > 0 ? Convert.ToInt32(cmbgrp.SelectedValue.ToString()) : (int?)null;
                    objgts.Date = dtpAccountDate.Value;
                    DataSet dsgrp = objgts.SearchGroupTrans();
                    if (dsgrp.Tables[0].Rows.Count > 0)
                    {

                        dgvSavRe.AutoGenerateColumns = false;
                        dgvSavRe.DataSource = dsgrp.Tables[0];
                    }
                    else
                    {
                        dgvSavRe.DataSource = null;
                    }
                }
                else
                {
                    dgvSavRe.DataSource = null;
                }
            }
           
           
        }

        private void dgvSavRe_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
           
            
        }

        private void dgvSavRe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgvSavRe.Columns[e.ColumnIndex].Index == 7)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            GroupTrans ob = new GroupTrans();
                            DeleteData(Convert.ToInt32((dgvSavRe.CurrentRow.Cells[6].Value)));
                            dgvSavRe.Refresh();
                           
                            
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;
            lblMessages.Text = string.Empty;
            GroupTrans objgrp = new GroupTrans();
            objgrp.Id= DataId;

            dsDeleteStatus = objgrp.DeleteGroup(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    foreach (DataGridViewCell oneCell in dgvSavRe.SelectedCells)
                    {
                        if (oneCell.Selected)
                            dgvSavRe.Rows.RemoveAt(oneCell.RowIndex);
                    }
                  
                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                   
                    

                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged_1(object sender, EventArgs e)
        {
            GroupTrans objgts = new GroupTrans();
            objgts.Date = dtpAccountDate.Value;
            DataSet dsgrp = objgts.SearchGroupDateTrans();
            if (dsgrp.Tables[0].Rows.Count > 0)
            {

                dgvSavRe.AutoGenerateColumns = false;
                dgvSavRe.DataSource = dsgrp.Tables[0];
            }
            else
            {
                dgvSavRe.DataSource = null;
            }
        }

        private void dtpAccountDate_ValueChanged_1(object sender, EventArgs e)
        {
            if (cmbslum.SelectedIndex > 0)
            {
                GroupTrans objgt = new GroupTrans();
                objgt.Date = dtpAccountDate.Value;
                objgt.fk_SlumId = Convert.ToInt32(cmbslum.SelectedValue);
                DataSet dsgrpnew = objgt.SearchGroupDateslumTrans();
                if (dsgrpnew.Tables[0].Rows.Count > 0)
                {

                    dgvSavRe.AutoGenerateColumns = false;
                    dgvSavRe.DataSource = dsgrpnew.Tables[0];
                }
                else
                {
                    dgvSavRe.DataSource = null;
                }
            }
            else
            {

                GroupTrans objgts = new GroupTrans();
                objgts.Date = dtpAccountDate.Value;
              
                DataSet dsgrp = objgts.SearchGroupDateTrans();
                if (dsgrp.Tables[0].Rows.Count > 0)
                {

                    dgvSavRe.AutoGenerateColumns = false;
                    dgvSavRe.DataSource = dsgrp.Tables[0];
                }
                else
                {
                    dgvSavRe.DataSource = null;
                }
            }
            
        }

        private void grpSavRep_Enter(object sender, EventArgs e)
        {

        }

        private void txtSavReTotal_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void txtSavReTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

       
    }
}
    