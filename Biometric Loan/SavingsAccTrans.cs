﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class frmSavingsAccTrans : Form
    {
        private Int32 iSavDepID=0;
        public string TransType = string.Empty;
        private double dblBalance = 0;
        private double RemainingPrincipal = 0;
        private double dblCurrentOldBalance = 0;

        public frmSavingsAccTrans()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;

            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void RetrieveSavingsName()
        {
            CodeValues objSavingsName = new CodeValues();
            objSavingsName.fk_memberId = GlobalValues.Member_PkId;
            DataSet dsSavingname = objSavingsName.GetSavingsName();
            if (dsSavingname != null && dsSavingname.Tables.Count > 0 && dsSavingname.Tables[0].Rows.Count > 0)
            {
                cmbSavingType.DisplayMember = "Name";
                cmbSavingType.ValueMember = "Id";
                cmbSavingType.DataSource = dsSavingname.Tables[0];
            }
        }

        private void SavingsAccDepositTrans_Load(object sender, EventArgs e)
        {
            try
            {
               // FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbSavingType); //Fill Savingtype    
                RetrieveSavingsName();  
              //  dtpOpenDate.MaxDate = DateTime.Now;
                // edited by pawan
                dtpOpenDate.MaxDate = DateTime.Today.AddDays(1);
                DateTime now = DateTime.Now;
                dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                // edited by pawan end

                if (TransType == Constants.CREDITTRANSABBREV)
                    this.Text = Constants.DEPOSITSAVINGSTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

                else
                    this.Text = Constants.WITHDRAWSAVINGSTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;

              //  lblAccValue.Text = GlobalValues.Savings_AccNumber;
                FillAgents(true);
                
                ClearControls();
                //LoadSavingsDepTrans();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        //private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        //{
        //    CodeValues objCodeValues = new CodeValues();
        //    objCodeValues.CodeTypeID = CodeTypeID;
        //    objCodeValues.ParentID = ParentID;

        //    DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
        //    if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
        //    {
        //        cmbLocation.DisplayMember = "Name";
        //        cmbLocation.ValueMember = "Id";
        //        cmbLocation.DataSource = dsCodeValues.Tables[0];
        //    }
        //}

        public void LoadSavingsDepTrans()
        {
            //Loads the grid
            SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();
            objSavingsDepTrans.fk_MemberId = GlobalValues.Member_PkId;
            objSavingsDepTrans.Tran_type = TransType;
            objSavingsDepTrans.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
            objSavingsDepTrans.LoanType = Convert.ToInt64(cmbSavingType.SelectedValue);
            DataSet dsDepositTrans = objSavingsDepTrans.GetSavingsDepositTransactions();
            if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[2].Rows.Count > 0)
            {
                RemainingPrincipal = Convert.ToDouble(dsDepositTrans.Tables[2].Rows[0][0].ToString());
            }
            if (dsDepositTrans != null)
            {
                if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count > 0)
                {
                    dgSavingsAccTrans.AutoGenerateColumns = false;
                    dgSavingsAccTrans.DataSource = dsDepositTrans.Tables[0];
                }
                else
                {
                    dgSavingsAccTrans.DataSource = null;
                }

                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {
                    dblBalance = Convert.ToDouble(dsDepositTrans.Tables[1].Rows[0][0].ToString());
                }
            }
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            if (cmbAgentName.Items.Count > 0)  cmbAgentName.SelectedIndex = 0;
            txtRemarks.Text = string.Empty;
            txtAmount.Text = string.Empty;
            dtpOpenDate.Value = DateTime.Now.Date;
            cmbSavingType.SelectedIndex = 0;
            iSavDepID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
            dblCurrentOldBalance = 0;
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if(cmbSavingType.SelectedIndex == 0)
                ErrorMsg = Messages.SAVINGTYPE + Messages.COMMAWITHSPACE;
            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg += Messages.AGENTNAME + Messages.COMMAWITHSPACE;
            if ((txtAmount.Text.Trim().Length == 0) || Convert.ToDouble(txtAmount.Text) <= 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                //Check available balance before withdrawal
                if (TransType == Constants.DEBITTRANSABBREV)
                {
                    if (RemainingPrincipal >= dblBalance)
                    {
                        ErrorMsg = Messages.AMOUNTEXCEED;
                        lblMessages.ForeColor = Color.Red;
                    }
                   // else if ((dblBalance - RemainingPrincipal) >= Convert.ToDouble(txtAmount.Text))
                    //{
                        
                    //}
                    if (iSavDepID == 0 && (dblBalance <= 0 || Convert.ToDouble(txtAmount.Text) > dblBalance))
                    {
                        ErrorMsg += Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }
                    else if (iSavDepID > 0 && (Convert.ToDouble(txtAmount.Text) > (dblBalance + dblCurrentOldBalance)))
                    {
                        ErrorMsg += Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }

                }
            }

            return ErrorMsg;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();

                    objSavingsDepTrans.Id = iSavDepID;
                    objSavingsDepTrans.fk_MemberId = GlobalValues.Member_PkId;
                    objSavingsDepTrans.Tran_type = TransType;
                    objSavingsDepTrans.fk_Accountid = GlobalValues.Savings_PkId;
                    objSavingsDepTrans.Amount = Convert.ToDouble(txtAmount.Text);
                    objSavingsDepTrans.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                    // edited by pawan
                    DateTime now = DateTime.Now;
                    dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                    objSavingsDepTrans.TransactionDate = dtpOpenDate.Value;
                    // edited by pawan end


                    //objSavingsDepTrans.TransactionDate = dtpOpenDate.Value;
                    objSavingsDepTrans.Remarks = txtRemarks.Text;
                    objSavingsDepTrans.Createdby = GlobalValues.User_PkId;
                    objSavingsDepTrans.Updatedby = GlobalValues.User_PkId;
                    objSavingsDepTrans.SavingType = GlobalValues.SavingType;

                    int iResult = 0;
                    if (iSavDepID==0)
                        iResult = objSavingsDepTrans.SaveSavingsDepositTransactions();
                    else
                        iResult = objSavingsDepTrans.UpdateSavingsDepositTransactions();

                    if (iResult > 0)
                    {
                        //Update Balance
                        if (iSavDepID == 0) //New
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance += Convert.ToDouble(txtAmount.Text);
                            }
                            else
                            {
                                dblBalance -= Convert.ToDouble(txtAmount.Text);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(txtAmount.Text);
                        }
                        else
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance -= dblCurrentOldBalance;
                                dblBalance += Convert.ToDouble(txtAmount.Text);
                            }
                            else
                            {
                                dblBalance += dblCurrentOldBalance;
                                dblBalance -= Convert.ToDouble(txtAmount.Text);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(txtAmount.Text);
                        }

                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();

                        if (objSavingsDepTrans.Id == 0)
                        {
                            btnSave.Text = Constants.UPDATE;

                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);

                            iSavDepID = iResult;
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        }
                        
                        LoadSavingsDepTrans();
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
            ClearControls();
            SavingAccount objSavingsAccFetch = new SavingAccount();
            LoadControlsnew(objSavingsAccFetch);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgSavingsDeposit_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgSavingsAccTrans.Rows[e.RowIndex].Cells["AgentID"].Value != DBNull.Value)
                {
                    cmbAgentName.SelectedValue = dgSavingsAccTrans.Rows[e.RowIndex].Cells["AgentID"].Value;
                }
                else
                {
                    cmbAgentName.SelectedIndex = 0;
                }
                dtpOpenDate.Value = Convert.ToDateTime(dgSavingsAccTrans.Rows[e.RowIndex].Cells["TransDate"].Value);
                txtAmount.Text = dgSavingsAccTrans.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                dblCurrentOldBalance = Convert.ToDouble(txtAmount.Text);

                txtRemarks.Text = dgSavingsAccTrans.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();

                btnSave.Text = Constants.UPDATE;
                iSavDepID = Convert.ToInt32(dgSavingsAccTrans.Rows[e.RowIndex].Cells["Id"].Value);
                cmbSavingType.SelectedValue = Convert.ToInt32(dgSavingsAccTrans.Rows[e.RowIndex].Cells[8].Value);
            }
            catch (Exception ex)
            {
                //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId, Int32 AccId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;

            SavingAccountTransactions objSavingAccTrans = new SavingAccountTransactions();
            objSavingAccTrans.Id = DataId;
            objSavingAccTrans.fk_Accountid = AccId;
            dsDeleteStatus = objSavingAccTrans.DeleteSavingAccTrans(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadSavingsDepTrans();
                 

                   lblMessages.Text = string.Empty;
                   if (cmbAgentName.Items.Count > 0) cmbAgentName.SelectedIndex = 0;
                   txtRemarks.Text = string.Empty;
                   txtAmount.Text = string.Empty;
                   dtpOpenDate.Value = DateTime.Now.Date;
                   iSavDepID = 0; //Create
                   btnSave.Text = Constants.SAVE;
                   btnSave.Focus();
                   dblCurrentOldBalance = 0;
                    frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                    objBioMetric.FetchMemberDetailsByID();
                    objBioMetric.InsertMemberDetailsToGrid();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgSavingsDeposit_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgSavingsAccTrans.Columns[e.ColumnIndex].Index == 6)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgSavingsAccTrans.CurrentRow.Cells[0].Value), Convert.ToInt32(dgSavingsAccTrans.CurrentRow.Cells[7].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgSavingsAccTrans_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (GlobalValues.User_Role.ToUpper() == UserRoles.USER.ToUpper())
            {
                dgSavingsAccTrans.Columns["btnDelete"].Visible = false;
            }
        }

        private void cmbSavingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt64(cmbSavingType.SelectedValue) > 0)
            {
                SavingAccount objSavingsAccFetch = new SavingAccount(); 
                objSavingsAccFetch.Id = GlobalValues.Member_PkId;
                objSavingsAccFetch.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue);
                GlobalValues.SavingType = Convert.ToInt64(cmbSavingType.SelectedValue); 
                LoadControls(objSavingsAccFetch);   
            }
        }

        private void LoadControls(SavingAccount objSavingsAccFetch)
        {
            DataSet dsSBAccFetch = objSavingsAccFetch.LoadSBAccById();
            if (dsSBAccFetch != null && dsSBAccFetch.Tables.Count > 0 && dsSBAccFetch.Tables[0].Rows.Count > 0)
            {
                GlobalValues.Savings_PkId = Convert.ToInt64(dsSBAccFetch.Tables[0].Rows[0]["Id"]);
               // lblMemberValue.Text = dsSBAccFetch.Tables[0].Rows[0]["MemberId"].ToString();
                lblAccValue.Text = dsSBAccFetch.Tables[0].Rows[0]["AccountNumber"].ToString();
                lblPassbook.Text = dsSBAccFetch.Tables[0].Rows[0]["passbookno"].ToString();
                //cmbStatus.Text = dsSBAccFetch.Tables[0].Rows[0]["Status"].ToString();
                //lblCurrBalanceValue.Text = dsSBAccFetch.Tables[0].Rows[0]["CurrentBalance"].ToString();
                //cmbAgentName.SelectedValue = Convert.ToInt16(dsSBAccFetch.Tables[0].Rows[0]["OpenedBy"]);
                //txtSavingPbNo.Text = dsSBAccFetch.Tables[0].Rows[0]["passbookno"].ToString();
               // dtpOpenDate.Text = Convert.ToDateTime(dsSBAccFetch.Tables[0].Rows[0]["Opendate"]).ToString();
               // txtRemarks.Text = dsSBAccFetch.Tables[0].Rows[0]["Remarks"].ToString();
                LoadSavingsDepTrans();
            }
            else //if (iAction == 2)
            {
                MessageBox.Show("The selected Account type does not exist.");
                this.SavingsAccDepositTrans_Load(null, null);
               // dgSavingsAccTrans.               
            }
            //if (dsSBAccFetch != null && dsSBAccFetch.Tables.Count > 0 && dsSBAccFetch.Tables[0].Rows.Count > 0 && iAction == 1)
            //{
            //    MessageBox.Show("The selected Saving Account already exists");
            //    this.SavingsAccount_Load(null, null);
            //}
        }
        private void LoadControlsnew(SavingAccount objSavingsAccFetch)
        {
            DataSet dsSBAccFetch = objSavingsAccFetch.LoadSBAccById();
            if (dsSBAccFetch != null && dsSBAccFetch.Tables.Count > 0 && dsSBAccFetch.Tables[0].Rows.Count > 0)
            {
                GlobalValues.Savings_PkId = Convert.ToInt64(dsSBAccFetch.Tables[0].Rows[0]["Id"]);
                // lblMemberValue.Text = dsSBAccFetch.Tables[0].Rows[0]["MemberId"].ToString();
                lblAccValue.Text = dsSBAccFetch.Tables[0].Rows[0]["AccountNumber"].ToString();
                lblPassbook.Text = dsSBAccFetch.Tables[0].Rows[0]["passbookno"].ToString();
                //cmbStatus.Text = dsSBAccFetch.Tables[0].Rows[0]["Status"].ToString();
                //lblCurrBalanceValue.Text = dsSBAccFetch.Tables[0].Rows[0]["CurrentBalance"].ToString();
                //cmbAgentName.SelectedValue = Convert.ToInt16(dsSBAccFetch.Tables[0].Rows[0]["OpenedBy"]);
                //txtSavingPbNo.Text = dsSBAccFetch.Tables[0].Rows[0]["passbookno"].ToString();
                // dtpOpenDate.Text = Convert.ToDateTime(dsSBAccFetch.Tables[0].Rows[0]["Opendate"]).ToString();
                // txtRemarks.Text = dsSBAccFetch.Tables[0].Rows[0]["Remarks"].ToString();
                LoadSavingsDepTrans();
            }
           
        }
     
    }
}