﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;


namespace SPARC
{
    public partial class CreateGroupUPF : Form
    {
        private Int32 iGsavId = 0;
        private Int32 GroupUPFTransno = 0;
        public CreateGroupUPF()
        {
            InitializeComponent();
        }

       

      

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
            {
                lblAccOpenDate.Text = string.Empty;
                lblCurrBalValue.Text = string.Empty;
                ClearControls();
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            }
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
            {
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
                lblAccOpenDate.Text = string.Empty;
                lblCurrBalValue.Text = string.Empty;
                ClearControls();
            }
            else
                cmbslum.DataSource = null;
        }
        private void FillCombogroup(int GroupId)
        {
            Group objgrp = new Group();

            objgrp.fk_SlumId = GroupId;
            DataSet dsgroup = objgrp.GetGrpcombo();
            DataRow dr = dsgroup.Tables[0].NewRow();
            dr[0] = 0;
            dr[1] = "--Select One--";

            dsgroup.Tables[0].Rows.InsertAt(dr, 0);
            if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
            {
                cmbgrp.DisplayMember = "Group_Name";
                cmbgrp.ValueMember = "Id";
                cmbgrp.DataSource = dsgroup.Tables[0];


            }
            else
            {
                cmbgrp.DataSource = null;
                //cmbGname.SelectedValue="--Select--";

            }
        }
        private void cmbslum_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbslum.SelectedIndex > 0)
            {
                FillCombogroup(Convert.ToInt32(cmbslum.SelectedValue));
                lblAccOpenDate.Text = string.Empty;
                lblCurrBalValue.Text = string.Empty;
                ClearControls();
            }
            else
            {
                cmbgrp.DataSource = null;
            }
        }

        private void cmbgrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbgrp.SelectedIndex > 0)
                {
                 //   lblGrpName.Text = Convert.ToString(cmbgrp.SelectedValue);
                    dtpTransDate.MaxDate = DateTime.Now;
                    LoadAllUPFAccounts();
                }
                else
                {
                    //cmbgrp.DataSource = null;
                }
            }
            catch
            {

            }
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
           
            objCodeValues.CodeTypeID = CodeTypeID;
            if (CodeTypeID == 3)
            {
                string queryString = "select Id from CodeValue where fk_CodeTypeId=2";
                string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                SqlConnection con = new SqlConnection(ConnectionString);
                SqlCommand command = new SqlCommand(queryString, con);
                con.Open();
                var result = command.ExecuteScalar();
                objCodeValues.ParentID = Convert.ToInt32(result);
                DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
                if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = -1;

                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                   
                    cmbLocation.DisplayMember = "Name";
                    cmbLocation.ValueMember = "Id";
                    cmbLocation.DataSource = dsCodeValues.Tables[0];
                }
            }
            else
            {
                objCodeValues.ParentID = ParentID;
                DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
                if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dsCodeValues.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = "--Select One--";
                    dr[2] = -1;

                    dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                    cmbLocation.DisplayMember = "Name";
                    cmbLocation.ValueMember = "Id";
                    cmbLocation.DataSource = dsCodeValues.Tables[0];
                }
            }
        }
        int ParentId = 0;
        private void CreateGroupUPF_Load(object sender, EventArgs e)
        {
            int CodeType = 0;
            
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {

                FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);

            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                 ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    //FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    //cmbHub.SelectedValue = Id;
                    //cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the state type code and populate state 
                    //FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                    //cmbcountry.SelectedValue = Id;
                    //cmbcountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the District type code and populate district
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                    cmbstate.SelectedValue = Id;
                    cmbstate.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the Slum type code and populate Slum
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                    cmbdistrict.SelectedValue = Id;
                    cmbdistrict.Enabled = false;
                }
            }
            FillAgents(true);
            ClearControls();

        }
        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            objAgent.centerId = null;

            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else

                {
                    if (type == "DEB")
                    {
                        MessageBox.Show("DEB transactions cannot be updated");
                    
                      }
                    else
                    {
                        GroupUPF objupf = new GroupUPF();
                        objupf.Id = iGsavId;
                        // objupf.fk_HubId = Convert.ToInt32(cmbHub.SelectedValue);
                        //objupf.fk_CountryId = Convert.ToInt32(cmbcountry.SelectedValue);
                        //objupf.fk_StateId = Convert.ToInt32(cmbstate.SelectedValue);
                        //objupf.fk_DistricId = Convert.ToInt32(cmbdistrict.SelectedValue);
                        //objupf.fk_SlumId = Convert.ToInt32(cmbslum.SelectedValue);
                        objupf.fk_GroupId = Convert.ToInt32(cmbgrp.SelectedValue);
                        objupf.Tran_type = Constants.CREDITTRANSABBREV;
                        objupf.Amount = Convert.ToDecimal(txtAmount.Text);
                        objupf.InterestPercent = Convert.ToDouble(ConfigurationSettings.AppSettings["UPF_Interest"].ToString());
                        objupf.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);

                        objupf.Opendate = dtpTransDate.Value;
                        objupf.Remarks = txtRemarks.Text;
                        //objUPFAcc.PayOutEndDate = dtpIntPay.Value;
                        objupf.PayOutEndDate = dtpTransDate.Value.AddYears(1);
                        objupf.Createdby = GlobalValues.User_PkId;
                        objupf.Updatedby = GlobalValues.User_PkId;
                        objupf.Status = "Active";
                        objupf.AccId = GroupUPFTransno;

                        DataSet dsUPFAcc = null;
                       Int32 UpfAccId;
                      
                        if (objupf.Tran_type != "DEB")
                        {
                            if (iGsavId == 0)
                            {
                                SqlConnection conn = new SqlConnection();
                                conn.ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

                                conn.Open();

                              
                                string query = "SELECT TOP 1 Id FROM GroupUPFAccount WHERE GroupUPFAccount.Fk_GroupId = @fk_GroupId";

                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.Parameters.Add(new SqlParameter("@fk_GroupId", cmbgrp.SelectedValue));
                                Int32 UpfAccIdd;
                                 UpfAccIdd = Convert.ToInt32(cmd.ExecuteScalar());
                              
                                if (UpfAccIdd == 0)
                                {
                                   UpfAccId = objupf.SaveUPFAccount();
                                   dsUPFAcc = objupf.SaveUPFAccounttrans(UpfAccId);
                                }
                                else
                                {
                                    dsUPFAcc = objupf.SaveUPFAccounttrans(UpfAccIdd);
                                }
                            }
                            else
                            {
                              //  objupf.UpdateUPFAccountnew();
                                Int32 Idd = objupf.UpdateUPFAccountnew();
                                dsUPFAcc = objupf.UpdateUPFAccount(0);
                             //   dsUPFAcc = objupf.UpdateUPFAccount(0);
                            }
                            if (dsUPFAcc != null && dsUPFAcc.Tables.Count > 0 && dsUPFAcc.Tables[0].Rows.Count > 0)
                            {
                                if (iGsavId == 0)
                                {
                                    lblMessages.Text = Messages.SAVED_SUCCESS;
                                    lblMessages.ForeColor = Color.Green;

                                    btnSave.Text = Constants.UPDATE;
                                  //  GroupUPFTransno = Convert.ToInt32(dsUPFAcc.Tables[0].Rows[0]["TransId"]);
                                 //   iGsavId = Convert.ToInt32(dsUPFAcc.Tables[1].Rows[0]["Id"]);
                                    //GlobalValues.UPF_AccNumber = dsUPFAcc.Tables[1].Rows[0]["AccountNumber"].ToString();                                
                                }
                                else
                                {
                                    lblMessages.Text = Messages.UPDATED_SUCCESS;
                                    lblMessages.ForeColor = Color.Green;
                                }
                            }
                            else
                            {
                                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                            }

                            LoadAllUPFAccounts();
                            ClearControls();
                        }
                    }
                }
            
        }

            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                // ReadError();
            }
        }
        //public static void ErrorLogging(Exception ex)
        //{
        //    string strPath = @"D:\Errors\Log.txt";
        //    if (!File.Exists(strPath))
        //    {
        //        File.Create(strPath).Dispose();
        //    }
        //    using (StreamWriter sw = File.AppendText(strPath))
        //    {
        //        sw.WriteLine("=============Error Logging ===========");
        //        sw.WriteLine("===========Start============= " + DateTime.Now);
        //        sw.WriteLine("Error Message: " + ex.Message);
        //        sw.WriteLine("Stack Trace: " + ex.StackTrace);
        //        sw.WriteLine("===========End============= " + DateTime.Now);

        //    }
        //}

        //public static void ReadError()
        //{
        //    string strPath = @"D:\Errors\Log.txt";
        //    using (StreamReader sr = new StreamReader(strPath))
        //    {
        //        string line;
        //        while ((line = sr.ReadLine()) != null)
        //        {
        //            Console.WriteLine(line);
        //        }                                                         
        //    }
        //}
        private void LoadAllUPFAccounts()
        {
            //Loads the grid
            GroupUPF objgrp = new GroupUPF();
            objgrp.fk_GroupId= Convert.ToInt32(cmbgrp.SelectedValue);
            DataSet dsUPF = objgrp.LoadAllUPFAccs();
            if (dsUPF != null && dsUPF.Tables.Count > 0 && dsUPF.Tables[0].Rows.Count > 0)
            {
              //  lblStatusVal.Text = dsUPF.Tables[0].Rows[0]["Status"].ToString();
                lblAccOpenDate.Text = Convert.ToDateTime(dsUPF.Tables[0].Rows[0]["UPFAccOpenDate"]).ToString("dd-MMM-yyyy");
                lblCurrBalValue.Text = dsUPF.Tables[0].Rows[0]["CurrentBalance"].ToString();
               // label1.Text = dsUPF.Tables[0].Rows[0][" GroupName"].ToString();
                dgUPFAccount.AutoGenerateColumns = false;
                dgUPFAccount.DataSource = dsUPF.Tables[0];

            }
            if (dsUPF.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("There is no data for the selected option");
            }
           
         
        }

        private void ClearControls()
        {
            try
            {
                iGsavId = 0;
                // //lblStatus.Visible = false;
                // lblStatusVal.Visible = false;
                // lblStatusVal.Text = "";
                txtAmount.Text = string.Empty;
              //  cmbAgentName.SelectedIndex = 0;

                txtRemarks.Text = string.Empty;
                dtpTransDate.Value = DateTime.Today;
                //lblAccValue.Text = string.Empty;

                //lblMessages.Text = string.Empty;
                //lblMessages.ForeColor = Color.Black;

                //dtpIntPay.Value = dtpTransDate.Value.AddYears(1);
                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }
            catch (Exception)
            {

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string type;
        private void dgUPFAccount_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ClearControls();
              //  lblStatusVal.Visible = true;
                //lblStatus.Visible = true;
                if (e.RowIndex >= 0)
                {
                    cmbAgentName.SelectedValue = dgUPFAccount.Rows[e.RowIndex].Cells["AgentId"].Value;
                    dtpTransDate.Value = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["TransactionDate"].Value);
                    txtAmount.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                    txtRemarks.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                    //dtpIntPay.Value = Convert.ToDateTime(dgUPFAccount.Rows[e.RowIndex].Cells["PayOutEndDate"].Value);
                    lblAccValue.Text = dgUPFAccount.Rows[e.RowIndex].Cells["AccountNumber"].Value.ToString();
                    //lblStatusVal.Text = dgUPFAccount.Rows[e.RowIndex].Cells["Status"].Value.ToString();
                    type = dgUPFAccount.Rows[e.RowIndex].Cells["TransType"].Value.ToString();
                    btnSave.Text = Constants.UPDATE;
                    iGsavId = Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["Id"].Value);
                    GroupUPFTransno = Convert.ToInt32(dgUPFAccount.Rows[e.RowIndex].Cells["UPFTransID"].Value);

                    //if (lblStatusVal.Text == SPARC.Status.CLOSED)
                    //    btnSave.Enabled = false;
                    //else
                    //    btnSave.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            
        }

        private void dgUPFAccount_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgUPFAccount.Columns[e.ColumnIndex].Name == "btnDelete")
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[10].Value), Convert.ToInt32(Convert.ToInt32(dgUPFAccount.CurrentRow.Cells[0].Value)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private void DeleteData(Int32 DataId,Int32 AccId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;
            GroupUPF objupf = new GroupUPF();

            objupf.Id = DataId;
            objupf.AccId = AccId;


            dsDeleteStatus = objupf.DeleteOpenUPFAccount(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadAllUPFAccounts();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else if (iResult == -1)
                {
                    lblMessages.Text = Messages.UPF_PAYOUT_XN_AVAILABLE;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    
                    
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }

        private void dgUPFAccount_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            if(cmbslum.SelectedIndex==-1)
                ErrorMsg = Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;
            

            if (txtAmount.Text.Trim() == string.Empty || Convert.ToDouble(txtAmount.Text) <= 0)
            {
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;
            }
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            //  else
            // {
            //   if (dtpIntPay.Value <= dtpTransDate.Value)
            // {
            //    ErrorMsg = Messages.INTERESTPAYDATECHK;
            //   lblMessages.ForeColor = Color.Red;
            //}
            //}
            return ErrorMsg;
        }

    
        private void CreateGroupUPF_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtAmount_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar)) // && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        }
    }

