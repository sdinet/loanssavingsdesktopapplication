﻿namespace SPARC
{
    partial class SearchMemberFamily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.lblHub = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDistrict = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.lblSearch = new System.Windows.Forms.Button();
            this.lblSlum = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.familyheadNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberFamilyIds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fkSlumIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.houseNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlumsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memberFamilyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memberFamilyDataset = new SPARC.MemberFamilyDataset();
            this.memberFamilyTableAdapter = new SPARC.MemberFamilyDatasetTableAdapters.MemberFamilyTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyDataset)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(10, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(96, 13);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(168, 20);
            this.txtName.TabIndex = 1;
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(96, 44);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(168, 21);
            this.cmbHub.TabIndex = 2;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // lblHub
            // 
            this.lblHub.AutoSize = true;
            this.lblHub.Location = new System.Drawing.Point(10, 52);
            this.lblHub.Name = "lblHub";
            this.lblHub.Size = new System.Drawing.Size(27, 13);
            this.lblHub.TabIndex = 3;
            this.lblHub.Text = "Hub";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Country";
            // 
            // lblDistrict
            // 
            this.lblDistrict.AutoSize = true;
            this.lblDistrict.Location = new System.Drawing.Point(10, 166);
            this.lblDistrict.Name = "lblDistrict";
            this.lblDistrict.Size = new System.Drawing.Size(61, 13);
            this.lblDistrict.TabIndex = 5;
            this.lblDistrict.Text = "District/City";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(10, 128);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(79, 13);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "State/Province";
            // 
            // cmbcountry
            // 
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(96, 82);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(168, 21);
            this.cmbcountry.TabIndex = 3;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(96, 120);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(168, 21);
            this.cmbstate.TabIndex = 4;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(96, 158);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(168, 21);
            this.cmbdistrict.TabIndex = 5;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.Location = new System.Drawing.Point(86, 238);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(75, 23);
            this.lblSearch.TabIndex = 7;
            this.lblSearch.Text = "Search";
            this.lblSearch.UseVisualStyleBackColor = true;
            this.lblSearch.Click += new System.EventHandler(this.lblSearch_Click);
            // 
            // lblSlum
            // 
            this.lblSlum.AutoSize = true;
            this.lblSlum.Location = new System.Drawing.Point(10, 202);
            this.lblSlum.Name = "lblSlum";
            this.lblSlum.Size = new System.Drawing.Size(85, 13);
            this.lblSlum.TabIndex = 11;
            this.lblSlum.Text = "Slum/Settlement";
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(96, 194);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(168, 21);
            this.cmbslum.TabIndex = 6;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.familyheadNameDataGridViewTextBoxColumn,
            this.MemberFamilyIds,
            this.fkSlumIDDataGridViewTextBoxColumn,
            this.houseNumberDataGridViewTextBoxColumn,
            this.SlumsName});
            this.dataGridView1.DataSource = this.memberFamilyBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(209, 314);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(443, 222);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // familyheadNameDataGridViewTextBoxColumn
            // 
            this.familyheadNameDataGridViewTextBoxColumn.DataPropertyName = "FamilyheadName";
            this.familyheadNameDataGridViewTextBoxColumn.HeaderText = "Family Head Name";
            this.familyheadNameDataGridViewTextBoxColumn.Name = "familyheadNameDataGridViewTextBoxColumn";
            this.familyheadNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // MemberFamilyIds
            // 
            this.MemberFamilyIds.DataPropertyName = "MemberFamilyId";
            this.MemberFamilyIds.HeaderText = "Member Family Id";
            this.MemberFamilyIds.Name = "MemberFamilyIds";
            this.MemberFamilyIds.ReadOnly = true;
            // 
            // fkSlumIDDataGridViewTextBoxColumn
            // 
            this.fkSlumIDDataGridViewTextBoxColumn.DataPropertyName = "Fk_SlumID";
            this.fkSlumIDDataGridViewTextBoxColumn.HeaderText = "Slum";
            this.fkSlumIDDataGridViewTextBoxColumn.Name = "fkSlumIDDataGridViewTextBoxColumn";
            this.fkSlumIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.fkSlumIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // houseNumberDataGridViewTextBoxColumn
            // 
            this.houseNumberDataGridViewTextBoxColumn.DataPropertyName = "HouseNumber";
            this.houseNumberDataGridViewTextBoxColumn.HeaderText = "House Number";
            this.houseNumberDataGridViewTextBoxColumn.Name = "houseNumberDataGridViewTextBoxColumn";
            this.houseNumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // SlumsName
            // 
            this.SlumsName.DataPropertyName = "SlumName";
            this.SlumsName.HeaderText = "Slum/Settlement";
            this.SlumsName.Name = "SlumsName";
            this.SlumsName.ReadOnly = true;
            // 
            // memberFamilyBindingSource
            // 
            this.memberFamilyBindingSource.DataMember = "MemberFamily";
            this.memberFamilyBindingSource.DataSource = this.memberFamilyDataset;
            // 
            // memberFamilyDataset
            // 
            this.memberFamilyDataset.DataSetName = "MemberFamilyDataset";
            this.memberFamilyDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // memberFamilyTableAdapter
            // 
            this.memberFamilyTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(179, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbslum);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lblSlum);
            this.groupBox1.Controls.Add(this.lblSearch);
            this.groupBox1.Controls.Add(this.cmbdistrict);
            this.groupBox1.Controls.Add(this.cmbstate);
            this.groupBox1.Controls.Add(this.cmbcountry);
            this.groupBox1.Controls.Add(this.lblState);
            this.groupBox1.Controls.Add(this.lblDistrict);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblHub);
            this.groupBox1.Controls.Add(this.cmbHub);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Location = new System.Drawing.Point(267, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(334, 267);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // SearchMemberFamily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(849, 577);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SearchMemberFamily";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.SearchMemberFamily_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memberFamilyDataset)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label lblHub;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDistrict;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Button lblSearch;
        private System.Windows.Forms.Label lblSlum;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MemberFamilyDataset memberFamilyDataset;
        private System.Windows.Forms.BindingSource memberFamilyBindingSource;
        private MemberFamilyDatasetTableAdapters.MemberFamilyTableAdapter memberFamilyTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn familyheadNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberFamilyIds;
        private System.Windows.Forms.DataGridViewTextBoxColumn fkSlumIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn houseNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlumsName;
    }
}