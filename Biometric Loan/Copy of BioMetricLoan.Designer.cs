﻿namespace SPARC
{
    partial class frmBioMetricSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.Member = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateMember = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateMember = new System.Windows.Forms.ToolStripMenuItem();
            this.SavingsAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateSavingsAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewUpdateSavingsAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.SavingsSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.DepositSavingsTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.WithdrawSavingsTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.loanAcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewUpdateLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.LoanSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.DispersalLoanTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.EMIPaymentLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.UPF = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateUPFAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewUpdateUPFAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.UPFSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.DepositUPFTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.UPFInterestPayout = new System.Windows.Forms.ToolStripMenuItem();
            this.Admin = new System.Windows.Forms.ToolStripMenuItem();
            this.UserManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.AgentManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.LocationManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitApp = new System.Windows.Forms.ToolStripMenuItem();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.scSearch = new System.Windows.Forms.SplitContainer();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgMembers = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateofBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbtaluk = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.txtmemberid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbioid = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEMIDue = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblLoanAccNo = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblSavingBalance = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblSavingAccNo = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblSlum = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pbMember = new System.Windows.Forms.PictureBox();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSearch)).BeginInit();
            this.scSearch.Panel1.SuspendLayout();
            this.scSearch.Panel2.SuspendLayout();
            this.scSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMember)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Member,
            this.SavingsAcc,
            this.loanAcToolStripMenuItem,
            this.UPF,
            this.Admin,
            this.ExitApp});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // Member
            // 
            this.Member.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateMember,
            this.UpdateMember});
            this.Member.Name = "Member";
            this.Member.Size = new System.Drawing.Size(57, 20);
            this.Member.Text = "Member";
            // 
            // CreateMember
            // 
            this.CreateMember.Name = "CreateMember";
            this.CreateMember.Size = new System.Drawing.Size(161, 22);
            this.CreateMember.Text = "Create Member";
            this.CreateMember.Click += new System.EventHandler(this.CreateMember_Click);
            // 
            // UpdateMember
            // 
            this.UpdateMember.Name = "UpdateMember";
            this.UpdateMember.Size = new System.Drawing.Size(161, 22);
            this.UpdateMember.Text = "Update Member";
            this.UpdateMember.Click += new System.EventHandler(this.UpdateMember_Click);
            // 
            // SavingsAcc
            // 
            this.SavingsAcc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateSavingsAccount,
            this.ViewUpdateSavingsAccount,
            this.SavingsSeparator,
            this.DepositSavingsTrans,
            this.WithdrawSavingsTrans});
            this.SavingsAcc.Name = "SavingsAcc";
            this.SavingsAcc.Size = new System.Drawing.Size(98, 20);
            this.SavingsAcc.Text = "Savings Account";
            // 
            // CreateSavingsAccount
            // 
            this.CreateSavingsAccount.Name = "CreateSavingsAccount";
            this.CreateSavingsAccount.Size = new System.Drawing.Size(198, 22);
            this.CreateSavingsAccount.Text = "Create Account";
            this.CreateSavingsAccount.Click += new System.EventHandler(this.CreateSavingsAccount_Click);
            // 
            // ViewUpdateSavingsAccount
            // 
            this.ViewUpdateSavingsAccount.Name = "ViewUpdateSavingsAccount";
            this.ViewUpdateSavingsAccount.Size = new System.Drawing.Size(198, 22);
            this.ViewUpdateSavingsAccount.Text = "View && Update Account";
            this.ViewUpdateSavingsAccount.Click += new System.EventHandler(this.ViewUpdateSavingsAccount_Click);
            // 
            // SavingsSeparator
            // 
            this.SavingsSeparator.Name = "SavingsSeparator";
            this.SavingsSeparator.Size = new System.Drawing.Size(195, 6);
            // 
            // DepositSavingsTrans
            // 
            this.DepositSavingsTrans.Name = "DepositSavingsTrans";
            this.DepositSavingsTrans.Size = new System.Drawing.Size(198, 22);
            this.DepositSavingsTrans.Text = "Deposit Transaction";
            this.DepositSavingsTrans.Click += new System.EventHandler(this.DepositSavingsTrans_Click);
            // 
            // WithdrawSavingsTrans
            // 
            this.WithdrawSavingsTrans.Name = "WithdrawSavingsTrans";
            this.WithdrawSavingsTrans.Size = new System.Drawing.Size(198, 22);
            this.WithdrawSavingsTrans.Text = "Withdrawal Transaction";
            this.WithdrawSavingsTrans.Click += new System.EventHandler(this.WithdrawSavingsTrans_Click);
            // 
            // loanAcToolStripMenuItem
            // 
            this.loanAcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateLoanAcc,
            this.ViewUpdateLoanAcc,
            this.LoanSeparator,
            this.DispersalLoanTrans,
            this.EMIPaymentLoanAcc});
            this.loanAcToolStripMenuItem.Name = "loanAcToolStripMenuItem";
            this.loanAcToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.loanAcToolStripMenuItem.Text = "Loan Account";
            // 
            // CreateLoanAcc
            // 
            this.CreateLoanAcc.Name = "CreateLoanAcc";
            this.CreateLoanAcc.Size = new System.Drawing.Size(197, 22);
            this.CreateLoanAcc.Text = "Create Account";
            this.CreateLoanAcc.Click += new System.EventHandler(this.CreateLoanAcc_Click);
            // 
            // ViewUpdateLoanAcc
            // 
            this.ViewUpdateLoanAcc.Name = "ViewUpdateLoanAcc";
            this.ViewUpdateLoanAcc.Size = new System.Drawing.Size(197, 22);
            this.ViewUpdateLoanAcc.Text = "View && Update Account";
            this.ViewUpdateLoanAcc.Click += new System.EventHandler(this.ViewUpdateLoanAcc_Click);
            // 
            // LoanSeparator
            // 
            this.LoanSeparator.Name = "LoanSeparator";
            this.LoanSeparator.Size = new System.Drawing.Size(194, 6);
            // 
            // DispersalLoanTrans
            // 
            this.DispersalLoanTrans.Name = "DispersalLoanTrans";
            this.DispersalLoanTrans.Size = new System.Drawing.Size(197, 22);
            this.DispersalLoanTrans.Text = "Dispersal Transaction";
            // 
            // EMIPaymentLoanAcc
            // 
            this.EMIPaymentLoanAcc.Name = "EMIPaymentLoanAcc";
            this.EMIPaymentLoanAcc.Size = new System.Drawing.Size(197, 22);
            this.EMIPaymentLoanAcc.Text = "EMI Payment";
            // 
            // UPF
            // 
            this.UPF.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateUPFAccount,
            this.ViewUpdateUPFAcc,
            this.UPFSeparator,
            this.DepositUPFTrans,
            this.UPFInterestPayout});
            this.UPF.Name = "UPF";
            this.UPF.Size = new System.Drawing.Size(38, 20);
            this.UPF.Text = "UPF";
            // 
            // CreateUPFAccount
            // 
            this.CreateUPFAccount.Name = "CreateUPFAccount";
            this.CreateUPFAccount.Size = new System.Drawing.Size(197, 22);
            this.CreateUPFAccount.Text = "Create Account";
            // 
            // ViewUpdateUPFAcc
            // 
            this.ViewUpdateUPFAcc.Name = "ViewUpdateUPFAcc";
            this.ViewUpdateUPFAcc.Size = new System.Drawing.Size(197, 22);
            this.ViewUpdateUPFAcc.Text = "View && Update Account";
            // 
            // UPFSeparator
            // 
            this.UPFSeparator.Name = "UPFSeparator";
            this.UPFSeparator.Size = new System.Drawing.Size(194, 6);
            // 
            // DepositUPFTrans
            // 
            this.DepositUPFTrans.Name = "DepositUPFTrans";
            this.DepositUPFTrans.Size = new System.Drawing.Size(197, 22);
            this.DepositUPFTrans.Text = "Deposit Transaction";
            // 
            // UPFInterestPayout
            // 
            this.UPFInterestPayout.Name = "UPFInterestPayout";
            this.UPFInterestPayout.Size = new System.Drawing.Size(197, 22);
            this.UPFInterestPayout.Text = "Interest Payout";
            // 
            // Admin
            // 
            this.Admin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UserManagement,
            this.AgentManagement,
            this.LocationManagement});
            this.Admin.Name = "Admin";
            this.Admin.Size = new System.Drawing.Size(48, 20);
            this.Admin.Text = "Admin";
            // 
            // UserManagement
            // 
            this.UserManagement.Name = "UserManagement";
            this.UserManagement.Size = new System.Drawing.Size(190, 22);
            this.UserManagement.Text = "User Management";
            // 
            // AgentManagement
            // 
            this.AgentManagement.Name = "AgentManagement";
            this.AgentManagement.Size = new System.Drawing.Size(190, 22);
            this.AgentManagement.Text = "Agent Management";
            // 
            // LocationManagement
            // 
            this.LocationManagement.Name = "LocationManagement";
            this.LocationManagement.Size = new System.Drawing.Size(190, 22);
            this.LocationManagement.Text = "Location Management";
            // 
            // ExitApp
            // 
            this.ExitApp.Name = "ExitApp";
            this.ExitApp.Size = new System.Drawing.Size(37, 20);
            this.ExitApp.Text = "Exit";
            this.ExitApp.Click += new System.EventHandler(this.ExitApp_Click);
            // 
            // scMain
            // 
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.Location = new System.Drawing.Point(0, 24);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.scSearch);
            this.scMain.Size = new System.Drawing.Size(1028, 559);
            this.scMain.SplitterDistance = 697;
            this.scMain.SplitterWidth = 6;
            this.scMain.TabIndex = 5;
            // 
            // scSearch
            // 
            this.scSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scSearch.Location = new System.Drawing.Point(0, 0);
            this.scSearch.Name = "scSearch";
            this.scSearch.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSearch.Panel1
            // 
            this.scSearch.Panel1.AutoScroll = true;
            this.scSearch.Panel1.Controls.Add(this.btnSearch);
            this.scSearch.Panel1.Controls.Add(this.dgMembers);
            this.scSearch.Panel1.Controls.Add(this.label8);
            this.scSearch.Panel1.Controls.Add(this.cmbstate);
            this.scSearch.Panel1.Controls.Add(this.label7);
            this.scSearch.Panel1.Controls.Add(this.cmbslum);
            this.scSearch.Panel1.Controls.Add(this.label6);
            this.scSearch.Panel1.Controls.Add(this.cmbtaluk);
            this.scSearch.Panel1.Controls.Add(this.label5);
            this.scSearch.Panel1.Controls.Add(this.cmbdistrict);
            this.scSearch.Panel1.Controls.Add(this.label4);
            this.scSearch.Panel1.Controls.Add(this.cmbcountry);
            this.scSearch.Panel1.Controls.Add(this.txtmemberid);
            this.scSearch.Panel1.Controls.Add(this.label3);
            this.scSearch.Panel1.Controls.Add(this.txtbioid);
            this.scSearch.Panel1.Controls.Add(this.label2);
            this.scSearch.Panel1.Controls.Add(this.txtname);
            this.scSearch.Panel1.Controls.Add(this.label1);
            // 
            // scSearch.Panel2
            // 
            this.scSearch.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.scSearch.Panel2.Controls.Add(this.lblEMIDue);
            this.scSearch.Panel2.Controls.Add(this.label22);
            this.scSearch.Panel2.Controls.Add(this.lblLoanAccNo);
            this.scSearch.Panel2.Controls.Add(this.label20);
            this.scSearch.Panel2.Controls.Add(this.lblSavingBalance);
            this.scSearch.Panel2.Controls.Add(this.label18);
            this.scSearch.Panel2.Controls.Add(this.lblSavingAccNo);
            this.scSearch.Panel2.Controls.Add(this.label16);
            this.scSearch.Panel2.Controls.Add(this.lblSlum);
            this.scSearch.Panel2.Controls.Add(this.label14);
            this.scSearch.Panel2.Controls.Add(this.lblMemberID);
            this.scSearch.Panel2.Controls.Add(this.label12);
            this.scSearch.Panel2.Controls.Add(this.lblname);
            this.scSearch.Panel2.Controls.Add(this.label9);
            this.scSearch.Panel2.Controls.Add(this.pbMember);
            this.scSearch.Size = new System.Drawing.Size(325, 559);
            this.scSearch.SplitterDistance = 360;
            this.scSearch.SplitterWidth = 6;
            this.scSearch.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(90, 190);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgMembers
            // 
            this.dgMembers.AllowUserToAddRows = false;
            this.dgMembers.AllowUserToDeleteRows = false;
            this.dgMembers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMembers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMembers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.MemberId,
            this.MemberName,
            this.DateofBirth});
            this.dgMembers.Location = new System.Drawing.Point(8, 225);
            this.dgMembers.Name = "dgMembers";
            this.dgMembers.ReadOnly = true;
            this.dgMembers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMembers.Size = new System.Drawing.Size(313, 129);
            this.dgMembers.TabIndex = 16;
            this.dgMembers.VirtualMode = true;
            this.dgMembers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMembers_CellContentClick);
            this.dgMembers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMembers_CellDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // MemberId
            // 
            this.MemberId.DataPropertyName = "MemberId";
            this.MemberId.HeaderText = "Member ID";
            this.MemberId.Name = "MemberId";
            this.MemberId.ReadOnly = true;
            // 
            // MemberName
            // 
            this.MemberName.DataPropertyName = "Name";
            this.MemberName.HeaderText = "Name";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // DateofBirth
            // 
            this.DateofBirth.DataPropertyName = "DateofBirth";
            this.DateofBirth.HeaderText = "Date of Birth";
            this.DateofBirth.Name = "DateofBirth";
            this.DateofBirth.ReadOnly = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "State";
            // 
            // cmbstate
            // 
            this.cmbstate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(90, 96);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(226, 21);
            this.cmbstate.TabIndex = 14;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Slum";
            // 
            // cmbslum
            // 
            this.cmbslum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(90, 165);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(226, 21);
            this.cmbslum.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Taluk";
            // 
            // cmbtaluk
            // 
            this.cmbtaluk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbtaluk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtaluk.FormattingEnabled = true;
            this.cmbtaluk.Location = new System.Drawing.Point(90, 142);
            this.cmbtaluk.Name = "cmbtaluk";
            this.cmbtaluk.Size = new System.Drawing.Size(226, 21);
            this.cmbtaluk.TabIndex = 10;
            this.cmbtaluk.SelectedIndexChanged += new System.EventHandler(this.cmbtaluk_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "District";
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(90, 119);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(226, 21);
            this.cmbdistrict.TabIndex = 8;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Country";
            // 
            // cmbcountry
            // 
            this.cmbcountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(90, 73);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(226, 21);
            this.cmbcountry.TabIndex = 6;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // txtmemberid
            // 
            this.txtmemberid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmemberid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmemberid.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmemberid.Location = new System.Drawing.Point(90, 27);
            this.txtmemberid.Name = "txtmemberid";
            this.txtmemberid.Size = new System.Drawing.Size(224, 21);
            this.txtmemberid.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Member ID";
            // 
            // txtbioid
            // 
            this.txtbioid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbioid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbioid.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbioid.Location = new System.Drawing.Point(90, 50);
            this.txtbioid.Name = "txtbioid";
            this.txtbioid.Size = new System.Drawing.Size(226, 21);
            this.txtbioid.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "BioMetric ID";
            // 
            // txtname
            // 
            this.txtname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(90, 4);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(224, 21);
            this.txtname.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // lblEMIDue
            // 
            this.lblEMIDue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEMIDue.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIDue.Location = new System.Drawing.Point(97, 162);
            this.lblEMIDue.Name = "lblEMIDue";
            this.lblEMIDue.Size = new System.Drawing.Size(221, 15);
            this.lblEMIDue.TabIndex = 14;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(5, 162);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 15);
            this.label22.TabIndex = 13;
            this.label22.Text = "EMI Due Date";
            // 
            // lblLoanAccNo
            // 
            this.lblLoanAccNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoanAccNo.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAccNo.Location = new System.Drawing.Point(97, 137);
            this.lblLoanAccNo.Name = "lblLoanAccNo";
            this.lblLoanAccNo.Size = new System.Drawing.Size(221, 15);
            this.lblLoanAccNo.TabIndex = 12;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(5, 137);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 15);
            this.label20.TabIndex = 11;
            this.label20.Text = "Loan A/c No.";
            // 
            // lblSavingBalance
            // 
            this.lblSavingBalance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavingBalance.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSavingBalance.Location = new System.Drawing.Point(97, 112);
            this.lblSavingBalance.Name = "lblSavingBalance";
            this.lblSavingBalance.Size = new System.Drawing.Size(221, 15);
            this.lblSavingBalance.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(5, 112);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 15);
            this.label18.TabIndex = 9;
            this.label18.Text = "Saving A/c Bal";
            // 
            // lblSavingAccNo
            // 
            this.lblSavingAccNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavingAccNo.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSavingAccNo.Location = new System.Drawing.Point(97, 87);
            this.lblSavingAccNo.Name = "lblSavingAccNo";
            this.lblSavingAccNo.Size = new System.Drawing.Size(221, 15);
            this.lblSavingAccNo.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(5, 87);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 15);
            this.label16.TabIndex = 7;
            this.label16.Text = "Saving A/c No.";
            // 
            // lblSlum
            // 
            this.lblSlum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSlum.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlum.Location = new System.Drawing.Point(97, 62);
            this.lblSlum.Name = "lblSlum";
            this.lblSlum.Size = new System.Drawing.Size(221, 15);
            this.lblSlum.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 15);
            this.label14.TabIndex = 5;
            this.label14.Text = "Slum";
            // 
            // lblMemberID
            // 
            this.lblMemberID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMemberID.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemberID.Location = new System.Drawing.Point(97, 37);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(221, 15);
            this.lblMemberID.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 15);
            this.label12.TabIndex = 3;
            this.label12.Text = "Member ID";
            // 
            // lblname
            // 
            this.lblname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblname.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(97, 12);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(221, 15);
            this.lblname.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Name";
            // 
            // pbMember
            // 
            this.pbMember.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMember.Location = new System.Drawing.Point(220, 44);
            this.pbMember.Name = "pbMember";
            this.pbMember.Size = new System.Drawing.Size(100, 100);
            this.pbMember.TabIndex = 0;
            this.pbMember.TabStop = false;
            // 
            // frmBioMetricSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 583);
            this.Controls.Add(this.scMain);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.Name = "frmBioMetricSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bio Metric Saving & Loan System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BioMaster_Load);
            this.MdiChildActivate += new System.EventHandler(this.BioMetricSystem_MdiChildActivate);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.scSearch.Panel1.ResumeLayout(false);
            this.scSearch.Panel1.PerformLayout();
            this.scSearch.Panel2.ResumeLayout(false);
            this.scSearch.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSearch)).EndInit();
            this.scSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMember)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.SplitContainer scSearch;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.TextBox txtmemberid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbioid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem Member;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbtaluk;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dgMembers;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateofBirth;
        private System.Windows.Forms.PictureBox pbMember;
        private System.Windows.Forms.Label lblEMIDue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblLoanAccNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblSavingBalance;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblSavingAccNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblSlum;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem CreateMember;
        private System.Windows.Forms.ToolStripMenuItem UpdateMember;
        private System.Windows.Forms.ToolStripMenuItem SavingsAcc;
        private System.Windows.Forms.ToolStripMenuItem CreateSavingsAccount;
        private System.Windows.Forms.ToolStripMenuItem ViewUpdateSavingsAccount;
        private System.Windows.Forms.ToolStripMenuItem DepositSavingsTrans;
        private System.Windows.Forms.ToolStripMenuItem WithdrawSavingsTrans;
        private System.Windows.Forms.ToolStripMenuItem loanAcToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator SavingsSeparator;
        private System.Windows.Forms.ToolStripMenuItem CreateLoanAcc;
        private System.Windows.Forms.ToolStripMenuItem ViewUpdateLoanAcc;
        private System.Windows.Forms.ToolStripSeparator LoanSeparator;
        private System.Windows.Forms.ToolStripMenuItem DispersalLoanTrans;
        private System.Windows.Forms.ToolStripMenuItem EMIPaymentLoanAcc;
        private System.Windows.Forms.ToolStripMenuItem UPF;
        private System.Windows.Forms.ToolStripMenuItem CreateUPFAccount;
        private System.Windows.Forms.ToolStripMenuItem ViewUpdateUPFAcc;
        private System.Windows.Forms.ToolStripSeparator UPFSeparator;
        private System.Windows.Forms.ToolStripMenuItem DepositUPFTrans;
        private System.Windows.Forms.ToolStripMenuItem UPFInterestPayout;
        private System.Windows.Forms.ToolStripMenuItem Admin;
        private System.Windows.Forms.ToolStripMenuItem UserManagement;
        private System.Windows.Forms.ToolStripMenuItem AgentManagement;
        private System.Windows.Forms.ToolStripMenuItem LocationManagement;
        private System.Windows.Forms.ToolStripMenuItem ExitApp;
    }
}



