﻿namespace SPARC
{
    partial class frmUPFInterestPayOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSAcc = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalIntAsOnDate = new System.Windows.Forms.Button();
            this.lblAccOpenDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIntAsOnDate = new System.Windows.Forms.Label();
            this.lblIntDueVal = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPrincipalWithdraw = new System.Windows.Forms.TextBox();
            this.cmbPayMode = new System.Windows.Forms.ComboBox();
            this.txtIntPayout = new System.Windows.Forms.TextBox();
            this.radPricipalAmount = new System.Windows.Forms.RadioButton();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblPayMode = new System.Windows.Forms.Label();
            this.radIntPayout = new System.Windows.Forms.RadioButton();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.dtpTransDate = new System.Windows.Forms.DateTimePicker();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblIntToBePaid = new System.Windows.Forms.Label();
            this.lblInterestTobePaid = new System.Windows.Forms.Label();
            this.lblDepAmtVal = new System.Windows.Forms.Label();
            this.lblStatusVal = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPay = new System.Windows.Forms.Button();
            this.lblIntPayDate = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblMemberValue = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.dgUPFAccount = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpenDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayOutEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMessages = new System.Windows.Forms.Label();
            this.pnlSAcc.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSAcc
            // 
            this.pnlSAcc.Controls.Add(this.groupBox1);
            this.pnlSAcc.Controls.Add(this.lblInfo);
            this.pnlSAcc.Controls.Add(this.dgUPFAccount);
            this.pnlSAcc.Location = new System.Drawing.Point(1, 2);
            this.pnlSAcc.Name = "pnlSAcc";
            this.pnlSAcc.Size = new System.Drawing.Size(840, 706);
            this.pnlSAcc.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnCalIntAsOnDate);
            this.groupBox1.Controls.Add(this.lblMessages);
            this.groupBox1.Controls.Add(this.lblAccOpenDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblIntAsOnDate);
            this.groupBox1.Controls.Add(this.lblIntDueVal);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.lblIntToBePaid);
            this.groupBox1.Controls.Add(this.lblInterestTobePaid);
            this.groupBox1.Controls.Add(this.lblDepAmtVal);
            this.groupBox1.Controls.Add(this.lblStatusVal);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnPay);
            this.groupBox1.Controls.Add(this.lblIntPayDate);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.lblAccValue);
            this.groupBox1.Controls.Add(this.lblAccountID);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblMemberValue);
            this.groupBox1.Controls.Add(this.lblMemberID);
            this.groupBox1.Location = new System.Drawing.Point(156, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 499);
            this.groupBox1.TabIndex = 120;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 121;
            this.label3.Text = "6%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 120;
            this.label2.Text = "Interest Percent";
            // 
            // btnCalIntAsOnDate
            // 
            this.btnCalIntAsOnDate.Location = new System.Drawing.Point(166, 153);
            this.btnCalIntAsOnDate.Name = "btnCalIntAsOnDate";
            this.btnCalIntAsOnDate.Size = new System.Drawing.Size(181, 51);
            this.btnCalIntAsOnDate.TabIndex = 119;
            this.btnCalIntAsOnDate.Text = "Calculate Interest As On Date";
            this.btnCalIntAsOnDate.UseVisualStyleBackColor = true;
            this.btnCalIntAsOnDate.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.AutoSize = true;
            this.lblAccOpenDate.Location = new System.Drawing.Point(408, 20);
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Size = new System.Drawing.Size(0, 13);
            this.lblAccOpenDate.TabIndex = 118;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(287, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 117;
            this.label1.Text = "Account open date";
            // 
            // lblIntAsOnDate
            // 
            this.lblIntAsOnDate.AutoSize = true;
            this.lblIntAsOnDate.Location = new System.Drawing.Point(385, 153);
            this.lblIntAsOnDate.Name = "lblIntAsOnDate";
            this.lblIntAsOnDate.Size = new System.Drawing.Size(0, 13);
            this.lblIntAsOnDate.TabIndex = 116;
            // 
            // lblIntDueVal
            // 
            this.lblIntDueVal.AutoSize = true;
            this.lblIntDueVal.Location = new System.Drawing.Point(150, 108);
            this.lblIntDueVal.Name = "lblIntDueVal";
            this.lblIntDueVal.Size = new System.Drawing.Size(0, 13);
            this.lblIntDueVal.TabIndex = 114;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPrincipalWithdraw);
            this.panel1.Controls.Add(this.cmbPayMode);
            this.panel1.Controls.Add(this.txtIntPayout);
            this.panel1.Controls.Add(this.radPricipalAmount);
            this.panel1.Controls.Add(this.lblAgentName);
            this.panel1.Controls.Add(this.lblPayMode);
            this.panel1.Controls.Add(this.radIntPayout);
            this.panel1.Controls.Add(this.lblTransactionDate);
            this.panel1.Controls.Add(this.cmbAgentName);
            this.panel1.Controls.Add(this.dtpTransDate);
            this.panel1.Controls.Add(this.txtRemarks);
            this.panel1.Controls.Add(this.lblRemarks);
            this.panel1.Location = new System.Drawing.Point(34, 203);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(509, 226);
            this.panel1.TabIndex = 1;
            // 
            // txtPrincipalWithdraw
            // 
            this.txtPrincipalWithdraw.Location = new System.Drawing.Point(183, 41);
            this.txtPrincipalWithdraw.MaxLength = 10;
            this.txtPrincipalWithdraw.Name = "txtPrincipalWithdraw";
            this.txtPrincipalWithdraw.Size = new System.Drawing.Size(130, 20);
            this.txtPrincipalWithdraw.TabIndex = 3;
            this.txtPrincipalWithdraw.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrincipalWithdraw_KeyPress);
            // 
            // cmbPayMode
            // 
            this.cmbPayMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPayMode.FormattingEnabled = true;
            this.cmbPayMode.Items.AddRange(new object[] {
            "Select",
            "Cash",
            "Bank"});
            this.cmbPayMode.Location = new System.Drawing.Point(183, 99);
            this.cmbPayMode.Name = "cmbPayMode";
            this.cmbPayMode.Size = new System.Drawing.Size(130, 21);
            this.cmbPayMode.TabIndex = 5;
            this.cmbPayMode.SelectedIndexChanged += new System.EventHandler(this.cmbPayMode_SelectedIndexChanged);
            // 
            // txtIntPayout
            // 
            this.txtIntPayout.Location = new System.Drawing.Point(183, 10);
            this.txtIntPayout.MaxLength = 10;
            this.txtIntPayout.Name = "txtIntPayout";
            this.txtIntPayout.Size = new System.Drawing.Size(130, 20);
            this.txtIntPayout.TabIndex = 2;
            this.txtIntPayout.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntPayout_KeyPress);
            // 
            // radPricipalAmount
            // 
            this.radPricipalAmount.AutoSize = true;
            this.radPricipalAmount.Location = new System.Drawing.Point(48, 44);
            this.radPricipalAmount.Name = "radPricipalAmount";
            this.radPricipalAmount.Size = new System.Drawing.Size(121, 17);
            this.radPricipalAmount.TabIndex = 1;
            this.radPricipalAmount.TabStop = true;
            this.radPricipalAmount.Text = "Principal Withdrawal";
            this.radPricipalAmount.UseVisualStyleBackColor = true;
            this.radPricipalAmount.CheckedChanged += new System.EventHandler(this.radPricipalAmount_CheckedChanged);
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(48, 129);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(89, 13);
            this.lblAgentName.TabIndex = 77;
            this.lblAgentName.Text = "Pay thru Leader *";
            // 
            // lblPayMode
            // 
            this.lblPayMode.AutoSize = true;
            this.lblPayMode.Location = new System.Drawing.Point(48, 102);
            this.lblPayMode.Name = "lblPayMode";
            this.lblPayMode.Size = new System.Drawing.Size(55, 13);
            this.lblPayMode.TabIndex = 111;
            this.lblPayMode.Text = "Pay Mode";
            // 
            // radIntPayout
            // 
            this.radIntPayout.AutoSize = true;
            this.radIntPayout.Location = new System.Drawing.Point(48, 13);
            this.radIntPayout.Name = "radIntPayout";
            this.radIntPayout.Size = new System.Drawing.Size(96, 17);
            this.radIntPayout.TabIndex = 0;
            this.radIntPayout.TabStop = true;
            this.radIntPayout.Text = "Interest Payout";
            this.radIntPayout.UseVisualStyleBackColor = true;
            this.radIntPayout.CheckedChanged += new System.EventHandler(this.radIntPayout_CheckedChanged);
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(48, 75);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(109, 13);
            this.lblTransactionDate.TabIndex = 89;
            this.lblTransactionDate.Text = "Interest Pay Out Date";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(183, 126);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(130, 21);
            this.cmbAgentName.TabIndex = 6;
            // 
            // dtpTransDate
            // 
            this.dtpTransDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransDate.Location = new System.Drawing.Point(183, 69);
            this.dtpTransDate.Name = "dtpTransDate";
            this.dtpTransDate.Size = new System.Drawing.Size(130, 20);
            this.dtpTransDate.TabIndex = 4;
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemarks.Location = new System.Drawing.Point(183, 156);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(195, 59);
            this.txtRemarks.TabIndex = 7;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(48, 156);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 4;
            this.lblRemarks.Text = "Remarks";
            // 
            // lblIntToBePaid
            // 
            this.lblIntToBePaid.AutoSize = true;
            this.lblIntToBePaid.Location = new System.Drawing.Point(408, 108);
            this.lblIntToBePaid.Name = "lblIntToBePaid";
            this.lblIntToBePaid.Size = new System.Drawing.Size(0, 13);
            this.lblIntToBePaid.TabIndex = 108;
            // 
            // lblInterestTobePaid
            // 
            this.lblInterestTobePaid.AutoSize = true;
            this.lblInterestTobePaid.Location = new System.Drawing.Point(287, 108);
            this.lblInterestTobePaid.Name = "lblInterestTobePaid";
            this.lblInterestTobePaid.Size = new System.Drawing.Size(93, 13);
            this.lblInterestTobePaid.TabIndex = 107;
            this.lblInterestTobePaid.Text = "Interest to be Paid";
            // 
            // lblDepAmtVal
            // 
            this.lblDepAmtVal.AutoSize = true;
            this.lblDepAmtVal.Location = new System.Drawing.Point(150, 77);
            this.lblDepAmtVal.Name = "lblDepAmtVal";
            this.lblDepAmtVal.Size = new System.Drawing.Size(0, 13);
            this.lblDepAmtVal.TabIndex = 105;
            // 
            // lblStatusVal
            // 
            this.lblStatusVal.AutoSize = true;
            this.lblStatusVal.Location = new System.Drawing.Point(408, 77);
            this.lblStatusVal.Name = "lblStatusVal";
            this.lblStatusVal.Size = new System.Drawing.Size(0, 13);
            this.lblStatusVal.TabIndex = 104;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(256, 446);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPay
            // 
            this.btnPay.Location = new System.Drawing.Point(153, 446);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(82, 23);
            this.btnPay.TabIndex = 8;
            this.btnPay.Text = "&Pay";
            this.btnPay.UseVisualStyleBackColor = true;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // lblIntPayDate
            // 
            this.lblIntPayDate.AutoSize = true;
            this.lblIntPayDate.Location = new System.Drawing.Point(31, 108);
            this.lblIntPayDate.Name = "lblIntPayDate";
            this.lblIntPayDate.Size = new System.Drawing.Size(80, 13);
            this.lblIntPayDate.TabIndex = 80;
            this.lblIntPayDate.Text = "Interest Due on";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(31, 77);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(70, 13);
            this.lblAmount.TabIndex = 75;
            this.lblAmount.Text = "Total Deposit";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(150, 46);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(19, 13);
            this.lblAccValue.TabIndex = 74;
            this.lblAccValue.Text = "<>";
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(31, 46);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 73;
            this.lblAccountID.Text = "Account ID";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(287, 77);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Status";
            // 
            // lblMemberValue
            // 
            this.lblMemberValue.AutoSize = true;
            this.lblMemberValue.Location = new System.Drawing.Point(150, 18);
            this.lblMemberValue.Name = "lblMemberValue";
            this.lblMemberValue.Size = new System.Drawing.Size(35, 13);
            this.lblMemberValue.TabIndex = 1;
            this.lblMemberValue.Text = "label1";
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(31, 18);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(59, 13);
            this.lblMemberID.TabIndex = 2;
            this.lblMemberID.Text = "Member ID";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(11, 494);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(139, 13);
            this.lblInfo.TabIndex = 102;
            this.lblInfo.Text = "Double click on a row to edit";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgUPFAccount
            // 
            this.dgUPFAccount.AllowUserToAddRows = false;
            this.dgUPFAccount.AllowUserToDeleteRows = false;
            this.dgUPFAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUPFAccount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUPFAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUPFAccount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.AccountNumber,
            this.OpenDate,
            this.CurrentBalance,
            this.Amount,
            this.TransactionDate,
            this.Remarks,
            this.btnDelete,
            this.Status,
            this.PayOutEndDate});
            this.dgUPFAccount.Location = new System.Drawing.Point(11, 511);
            this.dgUPFAccount.Margin = new System.Windows.Forms.Padding(0);
            this.dgUPFAccount.Name = "dgUPFAccount";
            this.dgUPFAccount.ReadOnly = true;
            this.dgUPFAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUPFAccount.Size = new System.Drawing.Size(822, 187);
            this.dgUPFAccount.TabIndex = 101;
            this.dgUPFAccount.TabStop = false;
            this.dgUPFAccount.VirtualMode = true;
            this.dgUPFAccount.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellClick);
            this.dgUPFAccount.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgUPFAccount_DataBindingComplete);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // AccountNumber
            // 
            this.AccountNumber.DataPropertyName = "AccountNumber";
            this.AccountNumber.HeaderText = "AccountNumber";
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.ReadOnly = true;
            // 
            // OpenDate
            // 
            this.OpenDate.DataPropertyName = "Opendate";
            this.OpenDate.HeaderText = "OpenDate";
            this.OpenDate.Name = "OpenDate";
            this.OpenDate.ReadOnly = true;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentBalance";
            this.CurrentBalance.HeaderText = "CurrentBalance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Interest Paid";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            this.TransactionDate.HeaderText = "Interest PaidOn";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            // 
            // btnDelete
            // 
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // PayOutEndDate
            // 
            this.PayOutEndDate.DataPropertyName = "PayOutEndDate";
            this.PayOutEndDate.HeaderText = "PayOutEndDate";
            this.PayOutEndDate.Name = "PayOutEndDate";
            this.PayOutEndDate.ReadOnly = true;
            this.PayOutEndDate.Visible = false;
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(34, 471);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 87;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmUPFInterestPayOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(853, 709);
            this.ControlBox = false;
            this.Controls.Add(this.pnlSAcc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmUPFInterestPayOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.UPFInterestPayOut_Load);
            this.pnlSAcc.ResumeLayout(false);
            this.pnlSAcc.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSAcc;
        private System.Windows.Forms.Label lblStatusVal;
        private System.Windows.Forms.Label lblInfo;
        public System.Windows.Forms.DataGridView dgUPFAccount;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Label lblIntPayDate;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblMemberValue;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label lblDepAmtVal;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label lblIntToBePaid;
        private System.Windows.Forms.Label lblInterestTobePaid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbPayMode;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.Label lblPayMode;
        private System.Windows.Forms.Label lblTransactionDate;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.DateTimePicker dtpTransDate;
        private System.Windows.Forms.Label lblIntDueVal;
        private System.Windows.Forms.Label lblIntAsOnDate;
        private System.Windows.Forms.Label lblAccOpenDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCalIntAsOnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn OpenDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayOutEndDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPrincipalWithdraw;
        private System.Windows.Forms.TextBox txtIntPayout;
        private System.Windows.Forms.RadioButton radPricipalAmount;
        private System.Windows.Forms.RadioButton radIntPayout;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}