﻿namespace SPARC
{
    partial class GroupInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Group_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlumName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btndelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.lblMessage = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmb_wr = new System.Windows.Forms.ComboBox();
            this.txt_bto = new System.Windows.Forms.TextBox();
            this.txt_bt = new System.Windows.Forms.TextBox();
            this.txt_bnkc = new System.Windows.Forms.TextBox();
            this.txt_bank = new System.Windows.Forms.TextBox();
            this.txt_short = new System.Windows.Forms.TextBox();
            this.txt_cash = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbDistrict = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbSlum = new System.Windows.Forms.ComboBox();
            this.txt_Int = new System.Windows.Forms.TextBox();
            this.txt_LFSR = new System.Windows.Forms.TextBox();
            this.txt_LFS = new System.Windows.Forms.TextBox();
            this.txt_land = new System.Windows.Forms.TextBox();
            this.txt_eyear = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_gname = new System.Windows.Forms.TextBox();
            this.txt_male = new System.Windows.Forms.TextBox();
            this.txt_ts = new System.Windows.Forms.TextBox();
            this.txt_female = new System.Windows.Forms.TextBox();
            this.Hub = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbohub = new System.Windows.Forms.ComboBox();
            this.cmbocountry = new System.Windows.Forms.ComboBox();
            this.cmbostate = new System.Windows.Forms.ComboBox();
            this.cmbodis = new System.Windows.Forms.ComboBox();
            this.cmboslum = new System.Windows.Forms.ComboBox();
            this.txt_grpname = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMessages = new System.Windows.Forms.Label();
            this.lblmsg = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Group_Name,
            this.SlumName,
            this.Id,
            this.btndelete});
            this.dataGridView1.Location = new System.Drawing.Point(169, 565);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(677, 193);
            this.dataGridView1.TabIndex = 38;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick_1);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
            // 
            // Group_Name
            // 
            this.Group_Name.DataPropertyName = "Group_Name";
            this.Group_Name.HeaderText = "Groupname";
            this.Group_Name.Name = "Group_Name";
            this.Group_Name.ReadOnly = true;
            // 
            // SlumName
            // 
            this.SlumName.DataPropertyName = "SlumName";
            this.SlumName.HeaderText = "SlumName";
            this.SlumName.Name = "SlumName";
            this.SlumName.ReadOnly = true;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // btndelete
            // 
            this.btndelete.HeaderText = "Delete";
            this.btndelete.Name = "btndelete";
            this.btndelete.ReadOnly = true;
            this.btndelete.Text = "Delete";
            this.btndelete.UseColumnTextForLinkValue = true;
            this.btndelete.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(49, 312);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 55;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmb_wr);
            this.groupBox2.Controls.Add(this.txt_bto);
            this.groupBox2.Controls.Add(this.txt_bt);
            this.groupBox2.Controls.Add(this.txt_bnkc);
            this.groupBox2.Controls.Add(this.txt_bank);
            this.groupBox2.Controls.Add(this.txt_short);
            this.groupBox2.Controls.Add(this.txt_cash);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cmbDistrict);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.cmbState);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.cmbSlum);
            this.groupBox2.Controls.Add(this.txt_Int);
            this.groupBox2.Controls.Add(this.txt_LFSR);
            this.groupBox2.Controls.Add(this.txt_LFS);
            this.groupBox2.Controls.Add(this.txt_land);
            this.groupBox2.Controls.Add(this.txt_eyear);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txt_gname);
            this.groupBox2.Controls.Add(this.txt_male);
            this.groupBox2.Controls.Add(this.txt_ts);
            this.groupBox2.Controls.Add(this.txt_female);
            this.groupBox2.Controls.Add(this.Hub);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbHub);
            this.groupBox2.Controls.Add(this.cmbCountry);
            this.groupBox2.Controls.Add(this.btn_clear);
            this.groupBox2.Controls.Add(this.Close);
            this.groupBox2.Controls.Add(this.btn_save);
            this.groupBox2.Controls.Add(this.lblMessage);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(848, 338);
            this.groupBox2.TabIndex = 59;
            this.groupBox2.TabStop = false;
            // 
            // cmb_wr
            // 
            this.cmb_wr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_wr.FormattingEnabled = true;
            this.cmb_wr.Location = new System.Drawing.Point(666, 267);
            this.cmb_wr.Name = "cmb_wr";
            this.cmb_wr.Size = new System.Drawing.Size(112, 21);
            this.cmb_wr.TabIndex = 21;
            // 
            // txt_bto
            // 
            this.txt_bto.Location = new System.Drawing.Point(666, 227);
            this.txt_bto.MaxLength = 15;
            this.txt_bto.Name = "txt_bto";
            this.txt_bto.Size = new System.Drawing.Size(102, 20);
            this.txt_bto.TabIndex = 20;
            this.txt_bto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bank_KeyPress_2);
            // 
            // txt_bt
            // 
            this.txt_bt.Location = new System.Drawing.Point(666, 186);
            this.txt_bt.MaxLength = 15;
            this.txt_bt.Name = "txt_bt";
            this.txt_bt.Size = new System.Drawing.Size(102, 20);
            this.txt_bt.TabIndex = 19;
            this.txt_bt.TextChanged += new System.EventHandler(this.txt_bt_TextChanged);
            this.txt_bt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bank_KeyPress_2);
            // 
            // txt_bnkc
            // 
            this.txt_bnkc.Location = new System.Drawing.Point(666, 42);
            this.txt_bnkc.MaxLength = 7;
            this.txt_bnkc.Name = "txt_bnkc";
            this.txt_bnkc.ShortcutsEnabled = false;
            this.txt_bnkc.Size = new System.Drawing.Size(102, 20);
            this.txt_bnkc.TabIndex = 15;
            this.txt_bnkc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bnkc_KeyPress_2);
            // 
            // txt_bank
            // 
            this.txt_bank.Location = new System.Drawing.Point(666, 151);
            this.txt_bank.MaxLength = 40;
            this.txt_bank.Name = "txt_bank";
            this.txt_bank.Size = new System.Drawing.Size(102, 20);
            this.txt_bank.TabIndex = 18;
            this.txt_bank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bank_KeyPress_2);
            // 
            // txt_short
            // 
            this.txt_short.Location = new System.Drawing.Point(666, 109);
            this.txt_short.MaxLength = 10;
            this.txt_short.Name = "txt_short";
            this.txt_short.ShortcutsEnabled = false;
            this.txt_short.Size = new System.Drawing.Size(102, 20);
            this.txt_short.TabIndex = 17;
            this.txt_short.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bnkc_KeyPress_2);
            // 
            // txt_cash
            // 
            this.txt_cash.Location = new System.Drawing.Point(666, 74);
            this.txt_cash.MaxLength = 10;
            this.txt_cash.Name = "txt_cash";
            this.txt_cash.ShortcutsEnabled = false;
            this.txt_cash.Size = new System.Drawing.Size(102, 20);
            this.txt_cash.TabIndex = 16;
            this.txt_cash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_bnkc_KeyPress_2);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(541, 269);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 13);
            this.label17.TabIndex = 210;
            this.label17.Text = "Reasons for Withdrawal";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(541, 229);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 13);
            this.label16.TabIndex = 209;
            this.label16.Text = "Business type other";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(541, 188);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 208;
            this.label15.Text = "Business type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(541, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 207;
            this.label14.Text = "Bank Charges";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(541, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 206;
            this.label13.Text = "Bank Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(541, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 205;
            this.label12.Text = "Short";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(541, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 204;
            this.label11.Text = "Cash";
            // 
            // cmbDistrict
            // 
            this.cmbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrict.FormattingEnabled = true;
            this.cmbDistrict.Location = new System.Drawing.Point(123, 183);
            this.cmbDistrict.Name = "cmbDistrict";
            this.cmbDistrict.Size = new System.Drawing.Size(118, 21);
            this.cmbDistrict.TabIndex = 5;
            this.cmbDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbDistrict_SelectedIndexChanged_2);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(22, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 202;
            this.label20.Text = "District";
            // 
            // cmbState
            // 
            this.cmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(123, 148);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(118, 21);
            this.cmbState.TabIndex = 4;
            this.cmbState.SelectedIndexChanged += new System.EventHandler(this.cmbState_SelectedIndexChanged_2);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(22, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 200;
            this.label19.Text = "State";
            // 
            // cmbSlum
            // 
            this.cmbSlum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSlum.FormattingEnabled = true;
            this.cmbSlum.Location = new System.Drawing.Point(123, 224);
            this.cmbSlum.Name = "cmbSlum";
            this.cmbSlum.Size = new System.Drawing.Size(118, 21);
            this.cmbSlum.TabIndex = 6;
            // 
            // txt_Int
            // 
            this.txt_Int.Location = new System.Drawing.Point(404, 267);
            this.txt_Int.MaxLength = 3;
            this.txt_Int.Name = "txt_Int";
            this.txt_Int.ShortcutsEnabled = false;
            this.txt_Int.Size = new System.Drawing.Size(104, 20);
            this.txt_Int.TabIndex = 14;
            this.txt_Int.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Int_KeyPress_2);
            // 
            // txt_LFSR
            // 
            this.txt_LFSR.Location = new System.Drawing.Point(404, 227);
            this.txt_LFSR.MaxLength = 10;
            this.txt_LFSR.Name = "txt_LFSR";
            this.txt_LFSR.ShortcutsEnabled = false;
            this.txt_LFSR.Size = new System.Drawing.Size(105, 20);
            this.txt_LFSR.TabIndex = 13;
            this.txt_LFSR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Int_KeyPress_2);
            // 
            // txt_LFS
            // 
            this.txt_LFS.Location = new System.Drawing.Point(404, 186);
            this.txt_LFS.MaxLength = 10;
            this.txt_LFS.Name = "txt_LFS";
            this.txt_LFS.ShortcutsEnabled = false;
            this.txt_LFS.Size = new System.Drawing.Size(106, 20);
            this.txt_LFS.TabIndex = 12;
            this.txt_LFS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Int_KeyPress_2);
            // 
            // txt_land
            // 
            this.txt_land.Location = new System.Drawing.Point(404, 151);
            this.txt_land.MaxLength = 7;
            this.txt_land.Name = "txt_land";
            this.txt_land.ShortcutsEnabled = false;
            this.txt_land.Size = new System.Drawing.Size(106, 20);
            this.txt_land.TabIndex = 11;
            this.txt_land.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_land_KeyPress_1);
            // 
            // txt_eyear
            // 
            this.txt_eyear.Location = new System.Drawing.Point(123, 264);
            this.txt_eyear.MaxLength = 4;
            this.txt_eyear.Name = "txt_eyear";
            this.txt_eyear.ShortcutsEnabled = false;
            this.txt_eyear.Size = new System.Drawing.Size(118, 20);
            this.txt_eyear.TabIndex = 7;
            this.txt_eyear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_eyear_KeyPress_2);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(269, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 193;
            this.label10.Text = "Interest";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(269, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 13);
            this.label9.TabIndex = 192;
            this.label9.Text = "Loan from Savings  Repaid";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(269, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 13);
            this.label8.TabIndex = 191;
            this.label8.Text = "Loan from Savings";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 190;
            this.label7.Text = "Land";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 189;
            this.label4.Text = "Established year";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 188;
            this.label3.Text = "Slum/Settlement*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 176;
            this.label2.Text = "GroupName*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 177;
            this.label6.Text = "Total Savings";
            // 
            // txt_gname
            // 
            this.txt_gname.Location = new System.Drawing.Point(123, 38);
            this.txt_gname.MaxLength = 50;
            this.txt_gname.Name = "txt_gname";
            this.txt_gname.Size = new System.Drawing.Size(118, 20);
            this.txt_gname.TabIndex = 1;
            this.txt_gname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_gname_KeyPress_1);
            // 
            // txt_male
            // 
            this.txt_male.Location = new System.Drawing.Point(404, 42);
            this.txt_male.MaxLength = 5;
            this.txt_male.Name = "txt_male";
            this.txt_male.ShortcutsEnabled = false;
            this.txt_male.Size = new System.Drawing.Size(107, 20);
            this.txt_male.TabIndex = 88;
            this.txt_male.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_female_KeyPress_2);
            // 
            // txt_ts
            // 
            this.txt_ts.Location = new System.Drawing.Point(404, 109);
            this.txt_ts.MaxLength = 10;
            this.txt_ts.Name = "txt_ts";
            this.txt_ts.ShortcutsEnabled = false;
            this.txt_ts.Size = new System.Drawing.Size(106, 20);
            this.txt_ts.TabIndex = 10;
            this.txt_ts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ts_KeyPress_1);
            // 
            // txt_female
            // 
            this.txt_female.Location = new System.Drawing.Point(404, 74);
            this.txt_female.MaxLength = 6;
            this.txt_female.Name = "txt_female";
            this.txt_female.ShortcutsEnabled = false;
            this.txt_female.Size = new System.Drawing.Size(107, 20);
            this.txt_female.TabIndex = 9;
            this.txt_female.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_female_KeyPress_2);
            // 
            // Hub
            // 
            this.Hub.AutoSize = true;
            this.Hub.Location = new System.Drawing.Point(22, 73);
            this.Hub.Name = "Hub";
            this.Hub.Size = new System.Drawing.Size(27, 13);
            this.Hub.TabIndex = 180;
            this.Hub.Text = "Hub";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(269, 73);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 185;
            this.label21.Text = "No of Female";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 105);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 181;
            this.label18.Text = "Country";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(269, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 184;
            this.label5.Text = "No  of Male";
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(123, 71);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(118, 21);
            this.cmbHub.TabIndex = 2;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // cmbCountry
            // 
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(123, 106);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(118, 21);
            this.cmbCountry.TabIndex = 3;
            this.cmbCountry.SelectedIndexChanged += new System.EventHandler(this.cmbCountry_SelectedIndexChanged_1);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(300, 290);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 23;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click_1);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(404, 290);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 24;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click_1);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(200, 290);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 22;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group Name";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(61, 61);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Hub";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(273, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Country";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(273, 64);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "State";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(516, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "District";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(512, 73);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(85, 13);
            this.label26.TabIndex = 5;
            this.label26.Text = "Slum/Settlement";
            // 
            // cmbohub
            // 
            this.cmbohub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbohub.FormattingEnabled = true;
            this.cmbohub.Location = new System.Drawing.Point(138, 61);
            this.cmbohub.Name = "cmbohub";
            this.cmbohub.Size = new System.Drawing.Size(103, 21);
            this.cmbohub.TabIndex = 29;
            this.cmbohub.SelectedIndexChanged += new System.EventHandler(this.cmbohub_SelectedIndexChanged);
            // 
            // cmbocountry
            // 
            this.cmbocountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbocountry.FormattingEnabled = true;
            this.cmbocountry.Location = new System.Drawing.Point(349, 22);
            this.cmbocountry.Name = "cmbocountry";
            this.cmbocountry.Size = new System.Drawing.Size(116, 21);
            this.cmbocountry.TabIndex = 27;
            this.cmbocountry.SelectedIndexChanged += new System.EventHandler(this.cmbocountry_SelectedIndexChanged);
            // 
            // cmbostate
            // 
            this.cmbostate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbostate.FormattingEnabled = true;
            this.cmbostate.Location = new System.Drawing.Point(349, 62);
            this.cmbostate.Name = "cmbostate";
            this.cmbostate.Size = new System.Drawing.Size(116, 21);
            this.cmbostate.TabIndex = 30;
            this.cmbostate.SelectedIndexChanged += new System.EventHandler(this.cmbostate_SelectedIndexChanged);
            // 
            // cmbodis
            // 
            this.cmbodis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbodis.FormattingEnabled = true;
            this.cmbodis.Location = new System.Drawing.Point(603, 22);
            this.cmbodis.Name = "cmbodis";
            this.cmbodis.Size = new System.Drawing.Size(121, 21);
            this.cmbodis.TabIndex = 28;
            this.cmbodis.SelectedIndexChanged += new System.EventHandler(this.cmbodis_SelectedIndexChanged);
            // 
            // cmboslum
            // 
            this.cmboslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboslum.FormattingEnabled = true;
            this.cmboslum.Location = new System.Drawing.Point(603, 70);
            this.cmboslum.Name = "cmboslum";
            this.cmboslum.Size = new System.Drawing.Size(121, 21);
            this.cmboslum.TabIndex = 31;
            this.cmboslum.SelectedIndexChanged += new System.EventHandler(this.cmboslum_SelectedIndexChanged);
            // 
            // txt_grpname
            // 
            this.txt_grpname.Location = new System.Drawing.Point(138, 22);
            this.txt_grpname.Name = "txt_grpname";
            this.txt_grpname.Size = new System.Drawing.Size(103, 20);
            this.txt_grpname.TabIndex = 26;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(349, 89);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 32;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMessages);
            this.groupBox1.Controls.Add(this.lblmsg);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.cmboslum);
            this.groupBox1.Controls.Add(this.btn_search);
            this.groupBox1.Controls.Add(this.cmbodis);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbostate);
            this.groupBox1.Controls.Add(this.txt_grpname);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.cmbohub);
            this.groupBox1.Controls.Add(this.cmbocountry);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Location = new System.Drawing.Point(3, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox1.Size = new System.Drawing.Size(845, 117);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Location = new System.Drawing.Point(474, 94);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 14;
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Location = new System.Drawing.Point(756, 64);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 13);
            this.lblmsg.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(46, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(848, 338);
            this.panel1.TabIndex = 218;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(46, 434);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(848, 128);
            this.panel2.TabIndex = 219;
            // 
            // GroupInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1027, 649);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GroupInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GroupInfo";
            this.Load += new System.EventHandler(this.Newform_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblMessage;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbohub;
        private System.Windows.Forms.ComboBox cmbocountry;
        private System.Windows.Forms.ComboBox cmbostate;
        private System.Windows.Forms.ComboBox cmbodis;
        private System.Windows.Forms.ComboBox cmboslum;
        private System.Windows.Forms.TextBox txt_grpname;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.ComboBox cmb_wr;
        private System.Windows.Forms.TextBox txt_bto;
        private System.Windows.Forms.TextBox txt_bt;
        private System.Windows.Forms.TextBox txt_bnkc;
        private System.Windows.Forms.TextBox txt_bank;
        private System.Windows.Forms.TextBox txt_short;
        private System.Windows.Forms.TextBox txt_cash;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbDistrict;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbSlum;
        private System.Windows.Forms.TextBox txt_Int;
        private System.Windows.Forms.TextBox txt_LFSR;
        private System.Windows.Forms.TextBox txt_LFS;
        private System.Windows.Forms.TextBox txt_land;
        private System.Windows.Forms.TextBox txt_eyear;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_gname;
        private System.Windows.Forms.TextBox txt_male;
        private System.Windows.Forms.TextBox txt_ts;
        private System.Windows.Forms.TextBox txt_female;
        private System.Windows.Forms.Label Hub;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Group_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlumName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewLinkColumn btndelete;
    }
}