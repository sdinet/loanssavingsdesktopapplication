﻿namespace SPARC
{
    partial class CreateGroupUPF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmbgrp = new System.Windows.Forms.ComboBox();
            this.Group = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblCurrBalValue = new System.Windows.Forms.Label();
            this.lblCurrBal = new System.Windows.Forms.Label();
            this.lblAccOpenDate = new System.Windows.Forms.Label();
            this.lblAccOpenlbl = new System.Windows.Forms.Label();
            this.dtpTransDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.dgUPFAccount = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaderName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayOutEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.UPFTransID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMessages = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbgrp
            // 
            this.cmbgrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbgrp.FormattingEnabled = true;
            this.cmbgrp.Location = new System.Drawing.Point(782, 12);
            this.cmbgrp.Name = "cmbgrp";
            this.cmbgrp.Size = new System.Drawing.Size(121, 21);
            this.cmbgrp.TabIndex = 162;
            this.cmbgrp.SelectedIndexChanged += new System.EventHandler(this.cmbgrp_SelectedIndexChanged);
            // 
            // Group
            // 
            this.Group.AutoSize = true;
            this.Group.Location = new System.Drawing.Point(725, 16);
            this.Group.Name = "Group";
            this.Group.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Group.Size = new System.Drawing.Size(36, 13);
            this.Group.TabIndex = 161;
            this.Group.Text = "Group";
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(561, 12);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(144, 21);
            this.cmbslum.TabIndex = 155;
            this.cmbslum.SelectedIndexChanged += new System.EventHandler(this.cmbslum_SelectedIndexChanged);
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(323, 12);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(131, 21);
            this.cmbdistrict.TabIndex = 154;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(470, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 160;
            this.label7.Text = "Slum/Settlement";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(253, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 159;
            this.label5.Text = "District/City";
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(108, 13);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(122, 21);
            this.cmbstate.TabIndex = 153;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 158;
            this.label8.Text = "State/Province";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cmbAgentName);
            this.groupBox1.Controls.Add(this.lblAgentName);
            this.groupBox1.Controls.Add(this.lblCurrBalValue);
            this.groupBox1.Controls.Add(this.lblCurrBal);
            this.groupBox1.Controls.Add(this.lblAccOpenDate);
            this.groupBox1.Controls.Add(this.lblAccOpenlbl);
            this.groupBox1.Controls.Add(this.dtpTransDate);
            this.groupBox1.Controls.Add(this.lblTransactionDate);
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.lblAccValue);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.lblRemarks);
            this.groupBox1.Location = new System.Drawing.Point(186, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 289);
            this.groupBox1.TabIndex = 163;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 243);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 112;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(165, 79);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(126, 21);
            this.cmbAgentName.TabIndex = 110;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(53, 79);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(78, 13);
            this.lblAgentName.TabIndex = 111;
            this.lblAgentName.Text = "Leader Name *";
            // 
            // lblCurrBalValue
            // 
            this.lblCurrBalValue.AutoSize = true;
            this.lblCurrBalValue.Location = new System.Drawing.Point(174, 44);
            this.lblCurrBalValue.Name = "lblCurrBalValue";
            this.lblCurrBalValue.Size = new System.Drawing.Size(0, 13);
            this.lblCurrBalValue.TabIndex = 108;
            // 
            // lblCurrBal
            // 
            this.lblCurrBal.AutoSize = true;
            this.lblCurrBal.Location = new System.Drawing.Point(53, 44);
            this.lblCurrBal.Name = "lblCurrBal";
            this.lblCurrBal.Size = new System.Drawing.Size(83, 13);
            this.lblCurrBal.TabIndex = 107;
            this.lblCurrBal.Text = "Current Balance";
            // 
            // lblAccOpenDate
            // 
            this.lblAccOpenDate.AutoSize = true;
            this.lblAccOpenDate.Location = new System.Drawing.Point(162, 16);
            this.lblAccOpenDate.Name = "lblAccOpenDate";
            this.lblAccOpenDate.Size = new System.Drawing.Size(0, 13);
            this.lblAccOpenDate.TabIndex = 106;
            // 
            // lblAccOpenlbl
            // 
            this.lblAccOpenlbl.AutoSize = true;
            this.lblAccOpenlbl.Location = new System.Drawing.Point(53, 16);
            this.lblAccOpenlbl.Name = "lblAccOpenlbl";
            this.lblAccOpenlbl.Size = new System.Drawing.Size(99, 13);
            this.lblAccOpenlbl.TabIndex = 105;
            this.lblAccOpenlbl.Text = "Account OpenDate";
            // 
            // dtpTransDate
            // 
            this.dtpTransDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpTransDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransDate.Location = new System.Drawing.Point(162, 146);
            this.dtpTransDate.Name = "dtpTransDate";
            this.dtpTransDate.Size = new System.Drawing.Size(122, 20);
            this.dtpTransDate.TabIndex = 2;
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(53, 146);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(69, 13);
            this.lblTransactionDate.TabIndex = 89;
            this.lblTransactionDate.Text = "Deposit Date";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(162, 116);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(125, 20);
            this.txtAmount.TabIndex = 1;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress_1);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(330, 243);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(80, 243);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(53, 112);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(89, 13);
            this.lblAmount.TabIndex = 75;
            this.lblAmount.Text = "Deposit Amount *";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(174, 16);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(0, 13);
            this.lblAccValue.TabIndex = 74;
            this.lblAccValue.Visible = false;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(165, 178);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(195, 59);
            this.txtRemarks.TabIndex = 4;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(53, 182);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 4;
            this.lblRemarks.Text = "Remarks";
            // 
            // dgUPFAccount
            // 
            this.dgUPFAccount.AllowUserToAddRows = false;
            this.dgUPFAccount.AllowUserToDeleteRows = false;
            this.dgUPFAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgUPFAccount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUPFAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUPFAccount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.AccountNumber,
            this.TransactionDate,
            this.LeaderName,
            this.TransType,
            this.Amount,
            this.Status,
            this.PayOutEndDate,
            this.Remarks,
            this.btnDelete,
            this.UPFTransID,
            this.CurrentBalance,
            this.AgentID,
            this.GroupName});
            this.dgUPFAccount.Location = new System.Drawing.Point(97, 402);
            this.dgUPFAccount.Margin = new System.Windows.Forms.Padding(0);
            this.dgUPFAccount.Name = "dgUPFAccount";
            this.dgUPFAccount.ReadOnly = true;
            this.dgUPFAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUPFAccount.Size = new System.Drawing.Size(847, 191);
            this.dgUPFAccount.TabIndex = 165;
            this.dgUPFAccount.TabStop = false;
            this.dgUPFAccount.VirtualMode = true;
            this.dgUPFAccount.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellClick);
            this.dgUPFAccount.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellContentDoubleClick);
            this.dgUPFAccount.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUPFAccount_CellDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // AccountNumber
            // 
            this.AccountNumber.DataPropertyName = "GroupNumber";
            this.AccountNumber.HeaderText = "Account ID";
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.ReadOnly = true;
            this.AccountNumber.Visible = false;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            dataGridViewCellStyle3.Format = "dd/MMM/yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.TransactionDate.DefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionDate.HeaderText = "Transaction Date";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            // 
            // LeaderName
            // 
            this.LeaderName.DataPropertyName = "AgentName";
            this.LeaderName.HeaderText = "Leader Name";
            this.LeaderName.Name = "LeaderName";
            this.LeaderName.ReadOnly = true;
            // 
            // TransType
            // 
            this.TransType.DataPropertyName = "TranType";
            this.TransType.HeaderText = "TransType";
            this.TransType.Name = "TransType";
            this.TransType.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle4;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // PayOutEndDate
            // 
            this.PayOutEndDate.DataPropertyName = "PayOutEndDate";
            this.PayOutEndDate.HeaderText = "Interest Pay Date";
            this.PayOutEndDate.Name = "PayOutEndDate";
            this.PayOutEndDate.ReadOnly = true;
            this.PayOutEndDate.Visible = false;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "TransRemarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.btnDelete.HeaderText = "Delete";
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseColumnTextForLinkValue = true;
            this.btnDelete.Width = 5;
            // 
            // UPFTransID
            // 
            this.UPFTransID.DataPropertyName = "UPFTrans";
            this.UPFTransID.HeaderText = "UPFTransID";
            this.UPFTransID.Name = "UPFTransID";
            this.UPFTransID.ReadOnly = true;
            this.UPFTransID.Visible = false;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentBalance";
            this.CurrentBalance.HeaderText = "CurrentBalance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            this.CurrentBalance.Visible = false;
            // 
            // AgentID
            // 
            this.AgentID.DataPropertyName = "fk_AgentId";
            this.AgentID.HeaderText = "AgentId";
            this.AgentID.Name = "AgentID";
            this.AgentID.ReadOnly = true;
            this.AgentID.Visible = false;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            this.GroupName.HeaderText = "Group Name";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Location = new System.Drawing.Point(194, 367);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 166;
            // 
            // CreateGroupUPF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 602);
            this.ControlBox = false;
            this.Controls.Add(this.lblMessages);
            this.Controls.Add(this.dgUPFAccount);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbgrp);
            this.Controls.Add(this.Group);
            this.Controls.Add(this.cmbslum);
            this.Controls.Add(this.cmbdistrict);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbstate);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateGroupUPF";
            this.Text = "CreateGroupUPF";
            this.Load += new System.EventHandler(this.CreateGroupUPF_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CreateGroupUPF_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUPFAccount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbgrp;
        private System.Windows.Forms.Label Group;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCurrBalValue;
        private System.Windows.Forms.Label lblCurrBal;
        private System.Windows.Forms.Label lblAccOpenDate;
        private System.Windows.Forms.Label lblAccOpenlbl;
        private System.Windows.Forms.DateTimePicker dtpTransDate;
        private System.Windows.Forms.Label lblTransactionDate;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        public System.Windows.Forms.DataGridView dgUPFAccount;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeaderName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayOutEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewLinkColumn btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn UPFTransID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button button1;

    }
}