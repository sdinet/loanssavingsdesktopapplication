﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using SPARC;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace SPARC
{
    public partial class MultipleInstallment : Form
    {
        private Int32 iSavDepID = 0;
        private Int32 iSavDepIDSaving = 0;
        public string TransType = string.Empty;
        private double OldAmountForUpdate = 0;
        private double totalAmount_Scheduled;
        private double totalEMI_Paid;
        private double CurrentBalance = 0;

        //
         string pb = "";
        string memname = "";
        Int64 memberid;
        Int64 accounid;
        //
        public MultipleInstallment()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        //private void RetrieveLoanName()
        //{
        //    CodeValues objLoanName = new CodeValues();
        //    objLoanName.fk_memberId = GlobalValues.Member_PkId;
        //    DataSet dsLoangname = objLoanName.GetLoanName();
        //    if (dsLoangname != null && dsLoangname.Tables.Count > 0 && dsLoangname.Tables[0].Rows.Count > 0)
        //    {
        //        cmbLoanType.DisplayMember = "Name";
        //        cmbLoanType.ValueMember = "Id";
        //        cmbLoanType.DataSource = dsLoangname.Tables[0];
        //    }
        //}

        private void MultipleInstallment_Load(object sender, EventArgs e)
        {
            try
            {
                FillCombos(Convert.ToInt32(OtherCodeTypes.LoanandSavingType), 0, cmbLoanType); //Fill Loantype     
                //RetrieveLoanName();
                int CodeType = 0;
                //if (TransType == Constants.CREDITTRANSABBREV)
                //    this.Text = Constants.DEPOSITMULTIPLESAVINGSTRANS;

                //else
                //    this.Text = Constants.WITHDRAWMULTIPLESAVINGSTRANS;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {
                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }

                Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }

                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the state type code and populate state 
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbcountry);
                        cmbcountry.SelectedValue = Id;
                        cmbcountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the District type code and populate district
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbstate);
                        cmbstate.SelectedValue = Id;
                        cmbstate.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the Slum type code and populate Slum
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbdistrict);
                        cmbdistrict.SelectedValue = Id;
                        cmbdistrict.Enabled = false;
                    }
                }
                
                //if (TransType == Constants.CREDITTRANSABBREV)
                //    this.Text = Constants.DEPOSITMULTIPLESAVINGSTRANS;
                //else
                //    this.Text = Constants.WITHDRAWMULTIPLESAVINGSTRANS;
                this.Text = Constants.MULTIPLEINSTALLMENT;
                FillAgents(true);
                // edited by pawan
                dtpOpenDate.MaxDate = DateTime.Today.AddDays(1);
                DateTime now = DateTime.Now;
                dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                // edited by pawan end


            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {

                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        LoanAccountTransactions objmulsav = new LoanAccountTransactions();
        private void txtPassbookNo_Leave(object sender, EventArgs e)
        {
            txtAccountNo.Text = string.Empty;
            txtMemberName.Text = string.Empty;
            objmulsav.PassbookNo = txtPassbookNo.Text.Trim();
            objmulsav.Fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?)null;
            pb = txtPassbookNo.Text;
            objmulsav.LoanType = Convert.ToInt64(cmbLoanType.SelectedValue);
            string ac = "";
            if (cmbslum.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select the Slum Name");
            }
            else if (cmbLoanType.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select the Loan Type");
            }
            else if (txtPassbookNo.Text == "")
            {
                MessageBox.Show("Please enter the PassbookNumber.");
            }
            else
            {
                DataSet dsSavAcc = objmulsav.GetAccountNumber();
                if (dsSavAcc != null && dsSavAcc.Tables.Count > 0 && dsSavAcc.Tables[0].Rows.Count > 0)
                {
                    txtAccountNo.Text = dsSavAcc.Tables[0].Rows[0]["AccountNumber"].ToString();
                    ac = dsSavAcc.Tables[0].Rows[0]["AccountNumber"].ToString();
                    memname = dsSavAcc.Tables[0].Rows[0]["Name"].ToString();
                    txtMemberName.Text = dsSavAcc.Tables[0].Rows[0]["Name"].ToString();
                }
            }
            if (txtAccountNo.Text != "")
            {
                //this.Text = Constants.DEPOSITSAVINGSTRANS + Messages.HYPHENWITHSPACE + ac;
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                //conn.ConnectionString = "Data Source=AIT3-PC\\SQLEXPRESS;Initial Catalog=Loan;Integrated Security=True";

                conn.Open();
                string query = "select m.Id as fk_MemberId from Members m inner join LoanAccount s on m.Id=s.fk_MemberId where s.passbookno = @PassbookNumber and m.fk_SlumId = @slumid and s.LoanType = @LoanType and s.Status<>'Closed'";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("@PassbookNumber", txtPassbookNo.Text));
                cmd.Parameters.Add(new SqlParameter("@slumid", cmbslum.SelectedValue));
                cmd.Parameters.Add(new SqlParameter("@LoanType", cmbLoanType.SelectedValue));
                memberid = Convert.ToInt64(cmd.ExecuteScalar());
                conn.Close();
                conn.Open();
                string query1 = "select s.Id as fk_Accountid from Members m inner join LoanAccount s on m.Id=s.fk_MemberId where s.passbookno=@PassbookNumb and m.fk_SlumId = @slumid and s.LoanType = @LoanType and s.Status<>'Closed'";
                SqlCommand cmd1 = new SqlCommand(query1, conn);
                cmd1.Parameters.Add(new SqlParameter("@PassbookNumb", txtPassbookNo.Text));
                cmd1.Parameters.Add(new SqlParameter("@slumid", cmbslum.SelectedValue));
                cmd1.Parameters.Add(new SqlParameter("@LoanType", cmbLoanType.SelectedValue));
                accounid = Convert.ToInt64(cmd1.ExecuteScalar());
                conn.Close();
            }
            else
            {
                MessageBox.Show("Entered Passbook Number is Incorrect Or Loan Account is not created");
            }

            try
            {
                // edited by pawan
                dtpOpenDate.MaxDate = DateTime.Today.AddDays(1);
                DateTime now = DateTime.Now;
                dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                // edited by pawan end


               // dtpOpenDate.MaxDate = DateTime.Now.Date;
                if (TransType == Constants.CREDITTRANSABBREV)//EMI payment
                {
                    this.Text = Constants.EMIPAYLOANTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                    //lblPendingCharge.Visible = true;
                    //lblPendingChargeVal.Visible = true;
                    //lblPendingPrincipal.Visible = true;
                    //lblPendingPrincipalVal.Visible = true;
                    lblRemainingPrincipal.Visible = true;
                    lblRemainingPrincipalVal.Visible = true;
                    //lblEMIDue.Visible = true;
                    //lblEMIDueVal.Visible = true;
                    //chkPayFromSaving.Checked = false;
                }
              //  lblAccValue.Text = GlobalValues.Loan_AccNumber;
               // FillAgents(true);
                ClearControls();
                LoadLoanAccTrans();

                if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && TransType == Constants.CREDITTRANSABBREV)
                {
                    txtPrincipalAmt.Enabled = true;
                    txtChargeAmt.Enabled = true;
                }
                else
                {
                    txtChargeAmt.Enabled = false;
                    txtPrincipalAmt.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            //edited by pawan start 
            //if (cmbAgentName.Items.Count > 0) cmbAgentName.SelectedIndex = 0;
            //edited by pawan end 
            //txtRemarks.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtPrincipalAmt.Text = string.Empty;
            txtChargeAmt.Text = string.Empty;
            //dtpOpenDate.Value = DateTime.Now.Date;

            iSavDepID = 0; //Create
            iSavDepIDSaving = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
            //txtRemarks.Enabled = true;
            //chkPayFromSaving.Checked = false;
        }

        private void LoadLoanAccTrans()
        {
            //Loads the grid
            LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
            objLoanTrans.fk_Accountid = accounid;//GlobalValues.Loan_PkId;
            objLoanTrans.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();
            if (dsDepositTrans != null)
            {
                //if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count > 0)
                //{
                //    dgLoanDeposit.AutoGenerateColumns = false;
                //    dgLoanDeposit.DataSource = dsDepositTrans.Tables[0];
                //}
                //else
                //{
                //    dgLoanDeposit.DataSource = null;
                //}
                //if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count == 0)
                //{
                //    lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
                //}
                //else lblPendingChargeVal.Text = "0";

                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {
                    lblDispersalBalVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingDispersal"].ToString();
                    lblLoanAmtVal.Text = dsDepositTrans.Tables[1].Rows[0]["LoanAmount"].ToString();

                    lblRemainingPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["RemainingPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["RemainingPrincipal"].ToString();
                    lblPendingPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingPrincipal"].ToString();//to be checked and included

                    //CR-13 Start
                    if (radioChargesPartwise.Checked == true)
                    {
                        lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString();
                    }

                    if (radioChargesFull.Checked == true)
                    {
                        if (dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString())
                        {
                            lblPendingChargeVal.Text = "0";
                        }

                        if (Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"]) > 0)
                        {
                            lblPendingChargeVal.Text = "0";
                        }
                        else
                            lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
                    }

                    //CR-13 END
                  //  lblPrincipalEMIPaidVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPrincipalPaid"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIPrincipalPaid"].ToString();
                   // lblChargesEMIPaidVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString();
                    totalEMI_Paid = (dsDepositTrans.Tables[1].Rows[0]["EMITotalPaid"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[1].Rows[0]["EMITotalPaid"].ToString()));

                  //  lblEMIDueVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIDueDate"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIDueDate"]).ToString("dd-MMM-yyyy") : "NA";//4

                   // lblEMIPrincipalVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPrincipal"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMIPrincipal"].ToString();
                    //lblEMIChargesVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMICharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["EMICharges"].ToString();
                   
                   // lblEMIEndDateVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPlannedEnd"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIPlannedEnd"]).ToString("dd-MMM-yyyy") : "NA";
                    lblEMIStartDateVal.Text = dsDepositTrans.Tables[1].Rows[0]["EMIPlannedStart"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[1].Rows[0]["EMIPlannedStart"]).ToString("dd-MMM-yyyy") : "NA"; ;
                    dtpOpenDate.MinDate = Convert.ToDateTime(lblEMIStartDateVal.Text);

                }



                //if (dsDepositTrans.Tables.Count > 2 && dsDepositTrans.Tables[2].Rows.Count > 0)
                //{
                //    lblLastPaidDateVal.Text = dsDepositTrans.Tables[2].Rows[0]["LastPaidDate"] != DBNull.Value ? Convert.ToDateTime(dsDepositTrans.Tables[2].Rows[0]["LastPaidDate"]).ToString("dd-MMM-yyyy") : "NA";
                //}
                //if (dsDepositTrans.Tables.Count > 4 && dsDepositTrans.Tables[1].Rows.Count > 0)
                //{
                //}

                //if (dsDepositTrans.Tables.Count > 5 && dsDepositTrans.Tables[5].Rows.Count > 0)
                //{
                //}
                if (TransType == Constants.CREDITTRANSABBREV)//EMI PAYMENT
                {
                    if (dsDepositTrans.Tables.Count > 5 && dsDepositTrans.Tables[5].Rows.Count > 0)
                    {
                        //totalAmount_Scheduled = (dsDepositTrans.Tables[5].Rows[0]["EMITotal"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[5].Rows[0]["EMITotal"].ToString())) * (dsDepositTrans.Tables[5].Rows[0]["Tenure"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dsDepositTrans.Tables[5].Rows[0]["Tenure"].ToString()));
                    }
                }
            }

        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            //CR-13 Start
            LoanAccountTransactions objLoanTrans1 = new LoanAccountTransactions();
            objLoanTrans1.fk_Accountid = accounid;//GlobalValues.Loan_PkId;
            objLoanTrans1.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans1.GetLoanTransactions();

            //CR-13 End

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;
            if ((txtAmount.Text.Trim().Length == 0) || Convert.ToDouble(txtAmount.Text) <= 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;

            if (txtPassbookNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.PASSBOOKNO + Messages.COMMAWITHSPACE;
            if (txtAccountNo.Text.Trim().Length == 0)
                ErrorMsg += Messages.SAVINGACCOUNTNO + Messages.COMMAWITHSPACE;
            if (txtMemberName.Text.Trim().Length == 0)
                ErrorMsg += Messages.MEMBERNAME + Messages.COMMAWITHSPACE;

            else if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && TransType == Constants.CREDITTRANSABBREV)
            {
                if (((txtPrincipalAmt.Text.Trim().Length == 0 || Convert.ToDouble(txtPrincipalAmt.Text) <= 0)) && (((txtChargeAmt.Text.Trim().Length == 0) || Convert.ToDouble(txtChargeAmt.Text) <= 0)))
                    ErrorMsg += Messages.PRINCIPALORCHARGEAMOUNT + Messages.COMMAWITHSPACE;
            }

            //CR-13 Start
            //if (radioChargesFull.Checked == true)
            //{
            //    if (dsDepositTrans.Tables.Count > 0 && dsDepositTrans.Tables[0].Rows.Count == 0)
            //    {
            //        if (Convert.ToDouble(txtAmount.Text) < Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["totalcharges"]))
            //        {
            //            ErrorMsg += Messages.AMOUNTCHARGES + Messages.COMMAWITHSPACE;
            //            //MessageBox.Show("Amount is less than Charges to be paid, Please enter correct Amount", "Important Message");
            //        }
            //    }

            //}
            //CR-13 End

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                if (TransType == Constants.CREDITTRANSABBREV)//EMI PAYMENT
                {
                    if (GlobalValues.User_Role.ToUpper() == UserRoles.SUPERADMIN.ToUpper() && (Convert.ToDouble(txtPrincipalAmt.Text) + Convert.ToDouble(txtChargeAmt.Text) != Convert.ToDouble(txtAmount.Text)))
                    {
                        ErrorMsg = Messages.AMTUNEQUAL;
                        lblMessages.ForeColor = Color.Red;
                    }

                    if (Convert.ToDouble(lblDispersalBalVal.Text) > 0)
                    {
                        ErrorMsg = Messages.DISPERSALPENDING;
                        lblMessages.ForeColor = Color.Red;
                    }
                    //check if amount is exceeding the pending needs to be paid
                    if (iSavDepID == 0 && (Convert.ToDouble(txtAmount.Text) > (Convert.ToDouble(lblRemainingPrincipalVal.Text) + Convert.ToDouble(lblPendingChargeVal.Text))))//if(Convert.ToDouble(txtAmount.Text)>(totalAmount_Scheduled-totalEMI_Paid))
                    {

                        ErrorMsg = Messages.EMIEXCEEDSLIMIT;
                        lblMessages.ForeColor = Color.Red;
                    }
                    else if ((iSavDepID > 0) && (Convert.ToDouble(txtAmount.Text) > (OldAmountForUpdate + Convert.ToDouble(lblPendingChargeVal.Text) + Convert.ToDouble(lblRemainingPrincipalVal.Text))))
                    {
                        ErrorMsg = Messages.EMIEXCEEDSLIMIT;
                        lblMessages.ForeColor = Color.Red;
                    }
                }

                if (TransType != Constants.CREDITTRANSABBREV)//DISPERSAL
                {
                    //Checks disperse limit during insert
                    if (iSavDepID == 0 && Convert.ToDouble(txtAmount.Text) > Convert.ToDouble(lblDispersalBalVal.Text))
                    {
                        ErrorMsg = Messages.DISPERSLIMITCHK;
                        lblMessages.ForeColor = Color.Red;
                    }
                    //Checks disperse limit during update
                    //else if (iUPFAccID > 0 && ((Convert.ToDouble(lblDispersalBalVal.Text) - Convert.ToDouble(txtAmount.Text)) + OldAmountForUpdate) > Convert.ToDouble(lblDispersalBalVal.Text))
                    else if (iSavDepID > 0 && (Convert.ToDouble(lblDispersalBalVal.Text) + OldAmountForUpdate) < Convert.ToDouble(txtAmount.Text))
                    {
                        ErrorMsg = Messages.DISPERSLIMITCHK;
                        lblMessages.ForeColor = Color.Red;
                    }
                }
            }
            return ErrorMsg;
        }

        private void txtPassbookNo_TextChanged(object sender, EventArgs e)
        {
            pb = txtPassbookNo.Text;            
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbcountry);
            else
                cmbcountry.DataSource = null;
        }

        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbdistrict.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        string passbookno;
        string accountno;
        Int64 fkmemid;
        Int64 LoansId;
        Int64 AgntId;
        double amount;
        DateTime date;
        double principalAmt;
        double ChargeAmt;
        Int64 loantype;
        private void btnSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                passbookno = Convert.ToString(dataGridView1.Rows[i].Cells["PassbookNumber"].Value);
                accountno = Convert.ToString(dataGridView1.Rows[i].Cells["Accountno2"].Value);
                fkmemid = Convert.ToInt64(dataGridView1.Rows[i].Cells["fk_MemberId"].Value);
                LoansId = Convert.ToInt64(dataGridView1.Rows[i].Cells["LoanId"].Value);
                amount = Convert.ToDouble(dataGridView1.Rows[i].Cells["Amount2"].Value);
                principalAmt = Convert.ToDouble(dataGridView1.Rows[i].Cells["PrincipalPart"].Value);
                ChargeAmt = Convert.ToDouble(dataGridView1.Rows[i].Cells["InterestPart"].Value);
                date = Convert.ToDateTime(dataGridView1.Rows[i].Cells["TransactionDate"].Value);
                AgntId = Convert.ToInt64(dataGridView1.Rows[i].Cells["AgentId"].Value);
                loantype = Convert.ToInt64(dataGridView1.Rows[i].Cells["LoanType"].Value);
                if (passbookno != "" && amount > 0)
                {
                    SavetheAmount();  //this.Text = Constants.DEPOSITSAVINGSTRANS;
                }
                else if (Convert.ToDouble(dataGridView1.Rows[0].Cells["Amount"].Value) <= 0)
                {
                    MessageBox.Show("Please enter the Amount");
                }
            }
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Please enter data to save");
            }
        }

        public void SavetheAmount()
        {
            try
            {
                LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();

                objLoanTrans.Id = iSavDepID;
                objLoanTrans.fk_MemberId = fkmemid;//GlobalValues.Member_PkId;
                objLoanTrans.Tran_type = TransType;
                objLoanTrans.fk_Accountid = LoansId;//GlobalValues.Loan_PkId;
                objLoanTrans.Amount = Convert.ToDouble(amount);
                objLoanTrans.fk_AgentId = Convert.ToInt32(AgntId);
                objLoanTrans.TransactionDate = Convert.ToDateTime(date);
                objLoanTrans.PrinciplePart = Convert.ToDouble(principalAmt);  //txtPrincipalAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtPrincipalAmt.Text) : 0;
                objLoanTrans.InterestPart = Convert.ToDouble(ChargeAmt);  //txtChargeAmt.Text.Trim().Length > 0 ? Convert.ToDouble(txtChargeAmt.Text) : 0;
                objLoanTrans.Remarks = "";
                objLoanTrans.Createdby = GlobalValues.User_PkId;
                objLoanTrans.Updatedby = GlobalValues.User_PkId;
                objLoanTrans.PaidFromSavings = false;
                objLoanTrans.LoanType = loantype;

                int iResult = 0;
                if (iSavDepID == 0)
                    iResult = objLoanTrans.SaveLoanAccTransactions();
                else
                    iResult = objLoanTrans.SaveLoanAccTransactions();

                if (iResult > 0)
                {
                    if (objLoanTrans.Id == 0)
                    {
                        //btnSave.Text = Constants.UPDATE;
                        //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);
                        //iUPFAccID = iResult;
                        btnSave.Visible = false;
                        lblMessages.Text = Messages.SAVED_SUCCESS;
                        lblMessages.ForeColor = Color.Green;
                    }
                    else
                    {
                        //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        lblMessages.Text = Messages.UPDATED_SUCCESS;
                        lblMessages.ForeColor = Color.Green;
                    }
                    //LoadLoanAccTrans();
                    ClearControls();
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                    //Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                }

            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            //return;
            try
            {
                if (txtAmount.Text.Trim().Length > 0)
                {
                    //txtChargeAmt.Text = (Convert.ToDouble(lblPendingChargeVal.Text) > 0) ? Convert.ToDouble(lblPendingChargeVal.Text) > Convert.ToDouble(txtAmount.Text) ? txtAmount.Text : lblPendingChargeVal.Text : "0";
                    //txtPrincipalAmt.Text = (Convert.ToDouble(lblPendingChargeVal.Text) > 0) ? Convert.ToDouble(lblPendingChargeVal.Text) > Convert.ToDouble(txtAmount.Text) ? "0" : (Convert.ToDouble(txtAmount.Text) - Convert.ToDouble(lblPendingChargeVal.Text)).ToString() : txtAmount.Text;
                    if (lblPendingChargeVal.Text.Trim() != "0")//to be checked
                    {
                        if (Convert.ToDouble(lblPendingChargeVal.Text) >= Convert.ToDouble(txtAmount.Text))
                        {
                            txtChargeAmt.Text = txtAmount.Text;
                            txtPrincipalAmt.Text = "0";
                        }
                        else
                        {
                            txtChargeAmt.Text = lblPendingChargeVal.Text;
                            txtPrincipalAmt.Text = Convert.ToString(Convert.ToDouble(txtAmount.Text) - Convert.ToDouble(lblPendingChargeVal.Text));
                        }
                    }
                    else
                    {
                        txtPrincipalAmt.Text = txtAmount.Text;
                        txtChargeAmt.Text = "0";
                    }
                }
                else
                {
                    txtPrincipalAmt.Text = "";
                    txtChargeAmt.Text = "";
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void radioChargesFull_CheckedChanged(object sender, EventArgs e)
        {
            LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
            objLoanTrans.fk_Accountid = accounid;//GlobalValues.Loan_PkId;
            objLoanTrans.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();

            if (dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"].ToString())
            {
                lblPendingChargeVal.Text = "0";
            }

            if (Convert.ToInt32(dsDepositTrans.Tables[1].Rows[0]["EMIChargesPaid"]) > 0)
            {
                lblPendingChargeVal.Text = "0";
            }
            else
                lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();

            // lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["totalcharges"].ToString();
        }

        private void radioChargesPartwise_CheckedChanged(object sender, EventArgs e)
        {
            LoanAccountTransactions objLoanTrans = new LoanAccountTransactions();
            objLoanTrans.fk_Accountid = accounid;//GlobalValues.Loan_PkId;
            objLoanTrans.Tran_type = TransType;
            DataSet dsDepositTrans = objLoanTrans.GetLoanTransactions();
            if (dsDepositTrans != null)
            {


                if (dsDepositTrans.Tables.Count > 1 && dsDepositTrans.Tables[1].Rows.Count > 0)
                {

                    lblPendingChargeVal.Text = dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString() == string.Empty ? "0" : dsDepositTrans.Tables[1].Rows[0]["PendingCharge"].ToString();
                }
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtPrincipalAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtChargeAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
             string strMessage = DoValidations();
             if (strMessage.Length > 0)
             {
                 lblMessages.Text = strMessage;
             }
             else
             {
                 lblMessages.Text = string.Empty;
                 dataGridView1.Rows.Add();

                 int Row = 0;
                 Row = dataGridView1.Rows.Count - 1;
                 
                 dataGridView1[0, Row].Value = txtMemberName.Text;
                 dataGridView1[1, Row].Value = txtPassbookNo.Text;
                 dataGridView1[2, Row].Value = txtAmount.Text;
                 dataGridView1[3, Row].Value = txtPrincipalAmt.Text;
                 dataGridView1[4, Row].Value = txtChargeAmt.Text;
               //  dataGridView1[5, Row].Value = dtpOpenDate.Text;
                 // edited by pawan
                 DateTime now = DateTime.Now;
                 dtpOpenDate.Value = new DateTime(dtpOpenDate.Value.Year, dtpOpenDate.Value.Month, dtpOpenDate.Value.Day, now.Hour, now.Minute, now.Second);
                 dataGridView1[5, Row].Value = dtpOpenDate.Value;
              
                 // edited by pawan end


                 dataGridView1[7, Row].Value = memberid;
                 dataGridView1[8, Row].Value = txtAccountNo.Text;
                 dataGridView1[9, Row].Value = accounid;
                 dataGridView1[10, Row].Value = cmbAgentName.SelectedValue;
                 dataGridView1[11, Row].Value = cmbLoanType.SelectedValue;

                 dataGridView1.Refresh();

                 //dtpOpenDate.Value = DateTime.Now.Date;
                 txtAmount.Text = string.Empty;
                 txtPrincipalAmt.Text = string.Empty;
                 txtChargeAmt.Text = string.Empty; 
                 txtPassbookNo.Text = string.Empty;
                 txtAccountNo.Text = string.Empty;
                 txtMemberName.Text = string.Empty;
                 lblLoanAmtVal.Text = "0";
                 lblDispersalBalVal.Text = "0";
                 lblPendingPrincipalVal.Text = "0";
                 lblPendingChargeVal.Text = "0";
                 lblRemainingPrincipalVal.Text = "0";
             }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Delete"].Index) //To check that we are in the right column
            {
                foreach (DataGridViewCell oneCell in dataGridView1.SelectedCells)
                {
                    if (oneCell.Selected)
                        dataGridView1.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
        }

    }
}
