﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPARC;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SPARC
{
    class CodeValues1
    {
        public int ID { get; set; }
        public int CodeTypeID { get; set; }
        public int? ParentID { get; set; }

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public DataSet GetCodeValues1(bool bAllValues1)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@CodeTypeID", SqlDbType.Int);
                param[0].Value = CodeTypeID;
                param[1] = new SqlParameter("@ParentID", SqlDbType.Int);
                param[1].Value = ParentID == -1 ? null : ParentID;
                param[2] = new SqlParameter("@AllValues", SqlDbType.Bit);
                param[2].Value = bAllValues1 == true ? 1 : 0;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetCodeValues6", param);//run
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}

