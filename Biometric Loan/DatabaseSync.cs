﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;


namespace SPARC
{
    public partial class DatabaseSync : Form
    {
        public DatabaseSync()
        {
            InitializeComponent();
        }

        int i;
        int j;
        //int l = 0;
        int count = 0;
        DataSet ds = new DataSet();
        DataSet dj = new DataSet();
        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["connstringindia"].Value;
        private Label label1;
        private Button btnClose;
        private Button btnSync;
        private static string ConnectionStringSync = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnSyncIndia"].Value;
            //ConfigurationManager.AppSettings["ConnSync"];

        //define array to hold Select Queries
        string[] selectQueries = new string[49];
        string[] insertUpdateQueries = new string[49];
        string[] syncdataTable = new string[49];

        private void cleanDataset()
        {
            // removing items from a collection
            while (ds.Tables.Count > 0)
            {
                DataTable table = ds.Tables[0];
                if (ds.Tables.CanRemove(table))
                {
                    ds.Tables.Remove(table);
                }
            }
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                SqlDataAdapter da;
                //create a array of sync data
                CreateSyncData();

                SqlConnection conn = new SqlConnection(ConnectionString);
                conn.Open();
                SqlConnection _connsync = new SqlConnection(ConnectionStringSync);
                _connsync.Open();
                int counter=0;
                //loop through the data for all tables
                //pick N or U sync status and insert or Update server db accordingly
               
                    for (i = 0; i < selectQueries.Length; i++)   // the for method to insert all the select data from the local table to the server table if the select queries contains any data which is added to dataset
                    {
                        da = new SqlDataAdapter(selectQueries[i], conn);
                        cleanDataset();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DBSync(i, _connsync);
                            counter++;                          
                        }
                         this.Close();                                               
                    }
                    handleDeleteSync();      

                    /// the 'counter' variable is incremented for the table values sync  and 'count' var is incremented for the deletelog table sync
                   if (counter==0 && count==0)
                   {   
                        MessageBox.Show("No data to sync!");
                   }
                   else
                   {
                        MessageBox.Show("Database Synced successfully!");
                   }
                }         
            catch (Exception ex)
            {
              MessageBox.Show("Something went wrong , Try to Sync again");
               // this.Close();
            }
            btnSync.Enabled = true;
            btnClose.Enabled = true;
            btnSync.Enabled = true;
            btnClose.Enabled = true;
            label1.Text = "Click on Sync Data to Server to push data to server";
            Cursor.Current = Cursors.Default;
        }
       
        private void DBSync(int i, SqlConnection connsync)
        {

            SqlCommand ccmd = new SqlCommand(insertUpdateQueries[i], connsync);
            SqlCommand ccmd1 = new SqlCommand(insertUpdateQueries[i], connsync);
            ccmd.CommandType = CommandType.StoredProcedure;
            ccmd1.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(ccmd1);
            Int32 serverId;
            Int32 localId;

            for (j = 0; j < ds.Tables[0].Rows.Count; j++)
            {
                SqlTransaction servertxn = connsync.BeginTransaction();

                try
                {

                    ccmd.Parameters.Clear();
                    //capture the local id
                    localId = Convert.ToInt32(ds.Tables[0].Rows[j][0]);
                    //loop through the columns of each row and insert the values into db
                    for (int kk = 0; kk < ds.Tables[0].Columns.Count - 1; kk++)
                    {
                        SqlParameter prm = new SqlParameter(ccmd1.Parameters[kk + 1].ToString(), ccmd1.Parameters[kk + 1].SqlDbType);
                        prm.Value = ds.Tables[0].Rows[j][kk + 1];
                        ccmd.Parameters.Add(prm);

                    }

                    ccmd.Transaction = servertxn;
                    //capture the generated identity in the server
                    serverId = Convert.ToInt32(ccmd.ExecuteScalar());

                    //update the local table with the serverId which will be used for updates
                    updateLocalTable(localId, serverId, syncdataTable[i]);
                  
                        servertxn.Commit();
                        servertxn.Dispose();
                    
                }
                catch (Exception ex)
                {
                    if (ex.GetType().IsAssignableFrom(typeof(System.ArgumentOutOfRangeException)))
                    {
                        MessageBox.Show("No data to Sync and try again");
                        servertxn.Rollback();
                    }
                    if (ex.GetType().IsAssignableFrom(typeof(SqlException)))
                    {
                        MessageBox.Show("Please Check your Internet Connection and Try to Sync again");
                    }
                    else
                    {
                        MessageBox.Show("Error while processing the data - " + ex.ToString());
                        servertxn.Rollback();
                    }                    
                }
            }
        }

        // this method is used to update the local--table(table) with serverid value(ser) fetched from the server table while performing the data tables sync 
        //from local to server  for the tables with the id value(loc) and the sync_type is made as null after the updation has made from local to server
        private void updateLocalTable(int loc, int ser, string table)
        {
            //code to update local table with server id

            SqlConnection Con = new SqlConnection(ConnectionString);
            Con.Open();
            // Data is accessible through the DataReader object here.
            string sql = "";
            if (table == "MemberFamily")
            {
                sql = "Update " + table + " set Server_Id=" + ser + ",Sync_type=NULL where  MemberFamilyId=" + loc;
            }
            else if (table == "Expenses")
            {
                sql = "Update " + table + " set Server_Id=" + ser + ",Sync_type=NULL where   ExpenseID=" + loc;
            }          
            else
            {
                sql = "Update " + table + " set Server_Id=" + ser + ",Sync_type=NULL where Id= " + loc;
            }
            SqlCommand cmd = new SqlCommand(sql, Con);

            cmd.ExecuteNonQuery();

            Con.Close();
        }

        private void handleDeleteSync()
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            conn.Open();
            SqlConnection _connsync = new SqlConnection(ConnectionStringSync);
            _connsync.Open();

            Int32 localId;
            string sql = "select * from deletelog where sync_type='D'";
            //SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            //create array for sync delete
            string[,] deleteArray = new string[, ]
	            {            
	                    {"Saving Acc Trans", "usp_savingaccounttransactionsdelete"},
	                    {"Members", "usp_MembersDelete"},
                        {"Expenses","usp_ExpensesDelete"},
                        {"User","usp_UsersDelete"},
                        {"Loan Account","usp_LoanAccountDelete"},
                        {"GroupInfo","usp_DeleteGroupdata"},   /// not found in deletelog table
                        {"Saving Account","usp_SavingAccountDelete"}, 
                        {"Loan Acc Trans","usp_LoanAccountTransactionsDelete"},
                        {"Device","usp_DeviceDelete"},
                        {"CodeValue","usp_CodeValueDelete"},
                        {"GroupTransactions","usp_DeleteGroupTransdata"},  /// not found in deletelog table
                        {"UPF Acc Trans","usp_UPFAccountTransactionsDelete"},  
                        {"UPF Acc Trans","usp_UPFAccountReOpen"}, 
                        {"GroupUPF Acc Trans","usp_GroupUPFAccountTransactionsDelete"},  /// not found in deletelog table
                        {"Group UPF Acc Trans","usp_GroupUPFAccountReOpen"},              /// not found in deletelog table
                        {"Memberfamilydetails","Deletememberfamilydetails"}              /// not found in deletelog table
	            };

            int i = 0;
            count = 0;
            while (i < ds.Tables[0].Rows.Count)
            {
                SqlCommand ccmd;
                SqlCommand ccmd1;
                for (int j = 0; j < deleteArray.Length / 2; j++)
                {
                    if (ds.Tables[0].Rows[i][1].ToString().Trim() == deleteArray[j, 0])
                    
                    {
                        ccmd = new SqlCommand(deleteArray[j, 1], conn);  // the ccmd is used to get all the parameters for the execution of ccmd1 stored procedure
                        ccmd.CommandType = CommandType.StoredProcedure;
                        ccmd1 = new SqlCommand(deleteArray[j, 1], _connsync);
                        ccmd1.CommandType = CommandType.StoredProcedure;
                        ccmd1.Parameters.Clear();
                        SqlCommandBuilder.DeriveParameters(ccmd);            //here we are deriving all the parameters

                        //send Id value
                        SqlParameter prm1 = new SqlParameter(ccmd.Parameters[1].ToString(), ccmd.Parameters[1].SqlDbType);  //this line is used to get first parameter from all the delete SP 
                        prm1.Value = ds.Tables[0].Rows[i][6];    //here in the rows[i][j] the count will start from zero for both i and j, this line is used to get the Server_Id from the deletelog table 
                        ccmd1.Parameters.Add(prm1);                 // the parameter name, type and its value is added here 

                        //send userid value
                        SqlParameter prm2 = new SqlParameter(ccmd.Parameters[2].ToString(), ccmd.Parameters[2].SqlDbType);   //here in the Parameters[i],, the i value start from 1 not from the default value zero
                        prm2.Value = GlobalValues.User_PkId;
                        ccmd1.Parameters.Add(prm2);

                        if (ds.Tables[0].Rows[i][1].ToString().Trim() == "Saving Acc Trans")      // sp--'usp_savingaccounttransactionsdelete'
                        {
                            //send accountid value
                            SqlParameter prm3 = new SqlParameter(ccmd.Parameters[3].ToString(), ccmd.Parameters[3].SqlDbType);

                            prm3.Value = ds.Tables[0].Rows[i][7];                  // here the 3rd parameter '@fk_Accountid' is made zero since it is not required for the sp which exist inside the main 'SP' which is 'usp_UpdateSavingsAccCurrentBalance' 
                            ccmd1.Parameters.Add(prm3);
                        }

                        if (ds.Tables[0].Rows[i][1].ToString().Trim() == "UPF Acc Trans")          // sp--  'usp_UPFAccountTransactionsDelete' and 'usp_UPFAccountReOpen'
                        {
                            //send accountid value
                            SqlParameter prm3 = new SqlParameter(ccmd.Parameters[3].ToString(), ccmd.Parameters[3].SqlDbType);  ///'@fk_Accountid'
                            prm3.Value = ds.Tables[0].Rows[i][7];
                            ccmd1.Parameters.Add(prm3);
                        }

                        if (ds.Tables[0].Rows[i][1].ToString().Trim() == "GroupUPF Acc Trans")
                        {
                            //send accountid value
                            SqlParameter prm3 = new SqlParameter(ccmd.Parameters[3].ToString(), ccmd.Parameters[3].SqlDbType);
                            prm3.Value = ds.Tables[0].Rows[i][7];
                            ccmd1.Parameters.Add(prm3);
                        }
                        if (ds.Tables[0].Rows[i][1].ToString().Trim() == "Group UPF Acc Trans")
                        {
                            //send accountid value
                            SqlParameter prm3 = new SqlParameter(ccmd.Parameters[3].ToString(), ccmd.Parameters[3].SqlDbType);
                            prm3.Value = ds.Tables[0].Rows[i][7];
                            ccmd1.Parameters.Add(prm3);
                        }
                        if (ds.Tables[0].Rows[i][6] != DBNull.Value)
                        {
                            localId = Convert.ToInt32(ds.Tables[0].Rows[i][6]);
                            ccmd1.ExecuteScalar();
                            updateDeleteLocalTable(localId);
                        }
                        break;
                    }
                }
                i++;
                count++;
            }
          conn.Close();
         _connsync.Close();
       }


                                                           // This method is used to update the local table column snc_type = null after the delete has done in the server table based on the server id value in 
        private void updateDeleteLocalTable(int loc)  ///the local table which in turn is present in the corresponding tables in local table which is fetched from the server table while doing the insertion from local to server table
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            conn.Open();
            string sql = "update Deletelog set Sync_Type=null where Server_Id=" + loc;

            SqlCommand cmd = new SqlCommand(sql, conn);

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void CreateSyncData()
        {
            //   selectQueries[1] = "select MemberId as localId ,photo from Members where Sync_type='U'";
            // Group Info Table Select Query for insert and update Checked
            selectQueries[0] = "Select g.Id as localId ,0 as ID,Group_Name, [dbo].[udf_FetchHubBySlum] ([fk_SlumId])  as [fk_hubid],[dbo].[udf_FetchCountryBySlum] ([fk_SlumId]) as [fk_countryid],[dbo].[udf_FetchStateBySlum]([fk_SlumId]) as [fk_stateid],[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId]) as [fk_districtid],c1.Server_Id as Fk_slumId, Est_year, Male, Female, tot_sav, Land, Ln_frm_sav, Ln_frm_sav_rep, interest, cash, short, bank, Bnk_chrg, Busi_type, Busi_typeother, c.Server_Id as wd_reasons, g.CreatedBy, g.CreatedDate, g.UpdatedBy, g.UpdatedDate, g.Isdeleted from GroupInfo g left join Codevalue c on c.Id=g.wd_reasons left join CodeValue c1 on c1.Id=g.fk_slumId where g.Sync_Type='N'";
            selectQueries[1] = "Select g.Id as localId, g.Server_Id as ID,Group_Name, [dbo].[udf_FetchHubBySlum] ([fk_SlumId])  as [fk_hubid],[dbo].[udf_FetchCountryBySlum] ([fk_SlumId]) as [fk_countryid],[dbo].[udf_FetchStateBySlum]([fk_SlumId]) as [fk_stateid],[dbo].[udf_FetchDistrictBySlum] ([fk_SlumId]) as [fk_districtid],c1.Server_Id as Fk_slumId, Est_year, Male, Female, tot_sav, Land, Ln_frm_sav, Ln_frm_sav_rep, interest, cash, short, bank, Bnk_chrg, Busi_type, Busi_typeother,  c.Server_Id as wd_reasons, g.Createdby, g.CreatedDate, g.UpdatedBy, g.UpdatedDate, g.Isdeleted from GroupInfo g left join Codevalue c on c.Id=g.wd_reasons left join CodeValue c1 on c1.Id=g.fk_slumId where g.Sync_Type='U'";
            
            // Members Table Select Query for insert and update  Checked
            selectQueries[2] = "Select m.Id as localId, 0 as ID, m.Name, m.FatherName, m.Gender,m. DateofBirth, m.Mobile, m.HomePhone,m.Email,[dbo].[udf_FetchHubBySlum] (m.[fk_SlumId])  as [fk_hubid],[dbo].[udf_FetchCountryBySlum] (m.[fk_SlumId]) as [fk_countryid],[dbo].[udf_FetchStateBySlum](m.[fk_SlumId]) as [fk_stateid],[dbo].[udf_FetchDistrictBySlum] (m.[fk_SlumId]) as [fk_districtid] ,c.Server_Id as fk_SlumId, m.Address1,m. Address2, m.Town, m.Landmark,m. Zip, m.BioMetricId, m.ContactpersonName, m.ContactpersonNumber, m.Remarks, m.Status, m.Photo, m.CreatedBy, m.UpdatedBy, m.PassbookNumber, m.Voterid,m. Adhaarid,g.server_Id as fk_GroupId from Members m left join GroupInfo g  on m.fk_GroupId=g.Id left join CodeValue c on m.fk_SlumId=c.Id where m.Sync_Type='N'";
            selectQueries[3] = "Select m.Id as localId, m.Server_Id as ID, m.Name, m.FatherName, m.Gender,m. DateofBirth, m.Mobile, m.HomePhone,m.Email,[dbo].[udf_FetchHubBySlum] (m.[fk_SlumId])  as [fk_hubid],[dbo].[udf_FetchCountryBySlum] (m.[fk_SlumId]) as [fk_countryid],[dbo].[udf_FetchStateBySlum](m.[fk_SlumId]) as [fk_stateid],[dbo].[udf_FetchDistrictBySlum] (m.[fk_SlumId]) as [fk_districtid] ,c.Server_Id as fk_SlumId, m.Address1, m.Address2,m. Town,m. Landmark, m.Zip, m.BioMetricId,m.ContactpersonName, m.ContactpersonNumber,  m.Remarks, m.Status, m.Photo, m.CreatedBy,m.UpdatedBy, m.PassbookNumber, m.Voterid, m.Adhaarid,g.server_Id as fk_GroupId from Members m left join GroupInfo g on m.fk_GroupId=g.Id left join CodeValue c on m.fk_SlumId=c.Id where m.Sync_Type='U'";

            // MemberFamily Table Select Query for insert and update Checked
            selectQueries[4] = "Select  MemberFamilyId as localId,0 as ID,c.Server_Id as Fk_SlumID, HouseNumber, RationCardNumber, HowManyYearsTheFamilyIsStaying, FamilyheadName, [Husband/WifeName], PartOfHouseForBusiness, BusinessDetails, Caste, FamilyMounthlyIncome, HouseDetails, MotherToungue, State, Water, Toilet, Electricity, CookingMedium, m.Createdby, m.Updatedby,m.FamilyPhotograph from MemberFamily m left join CodeValue c on c.Id=m.fk_slumId where m.Sync_Type='N'";
            selectQueries[5] = "Select  MemberFamilyId as localId,m.Server_Id as ID ,c.Server_Id as Fk_SlumID, HouseNumber, RationCardNumber, HowManyYearsTheFamilyIsStaying, FamilyheadName, [Husband/WifeName], PartOfHouseForBusiness, BusinessDetails, Caste, FamilyMounthlyIncome, HouseDetails, MotherToungue, State, Water, Toilet, Electricity, CookingMedium, m.Createdby, m.Updatedby,m.FamilyPhotograph  from MemberFamily  m left join CodeValue c on c.Id=m.fk_slumId where m.Sync_Type='U'";

            // Users Table Select Query for insert and update Checked
            selectQueries[6] = "Select u.Id as localId, Fullname,ContactNumber,Username,Password,Role,IsAgent ,u.Createdby, c.Server_Id as fk_CenterId from Users u left join CodeValue c on c.Id=u.fk_CenterId  where u.Sync_Type='N'";
            selectQueries[7] = "Select u.Id as localId,u.Server_Id as ID,Fullname,ContactNumber,Username,Password,Role,IsAgent ,u.Updatedby, c.Server_Id as fk_CenterId from Users u left join CodeValue c on c.Id=u.fk_CenterId  where u.Sync_Type='U'";

            // Expenses Table Select Query for insert and update Checked ---here created and updated date data is fetched using both query but this parameter not used in insert or updated date parameter only used in update
            selectQueries[8] = "Select ExpenseId as localId, 0 as ID,ExpenseDate, Amount, fk_ModeOfPayment, fk_category, Description, Remarks, CreatedDate, CreatedBy,UpdatedDate, UpdatedBy from Expenses where Sync_Type='N'";
            selectQueries[9] = "select  ExpenseId as localId,Server_Id as ID ,ExpenseDate, Amount, fk_ModeOfPayment, fk_category, Description, Remarks, CreatedDate, CreatedBy,UpdatedDate, UpdatedBy from Expenses  where Sync_Type='U'";

            // CodeValue Table Select Query for insert and update Checked ---here the server id(ID) is null for some record for the update query  ----here we are joining the code value table again to get the exact Id and fk_ParentId values -- error in the update query 'b.Server_Id' is used twice ,, think it shld be 'a.Server_Id as ID' in update query
            selectQueries[10] = "Select  a.Id as localId,a.Name, a.fk_CodeTypeId, b.Server_Id as fk_ParentId, a.DisplayOrder, a.Abbreviation, a.Createdby, a.Updatedby from CodeValue a left join CodeValue b on a.fk_ParentId =b.Id where a.Sync_Type='N'";
            selectQueries[11] = "Select  a.Id as localId,b.Server_Id as ID,a.Name, a.fk_CodeTypeId,b.Server_Id as  fk_ParentId, a.DisplayOrder, a.Abbreviation,  a.Updatedby from CodeValue a  left join CodeValue b on a.fk_ParentId =b.Id  where a.Sync_Type='U'";


            // CodeType Table Select Query for insert and update Checked
            selectQueries[12] = "Select Id as localId, Code, Isdeleted, Createdby, CreatedDate, Updatedby, UpdatedDate from CodeType where Sync_Type='N'";
            selectQueries[13] = "Select  Id as localId,Server_Id as ID, Code, Isdeleted, Createdby, CreatedDate, Updatedby, UpdatedDate from CodeType where Sync_Type='U'";


            // Funders Table Select Query for insert and update Checked
            selectQueries[14] = "select Id as localId,FunderName,Address1,Address2,City,State,Country, Zip, Telephone,Fax,EmailAddress,ContactPerson,Contactnumber,Createdby,Updatedby,Hub from Funders where Sync_Type='N'";
            selectQueries[15] = "select Id as localId, Server_Id as ID,FunderName,Address1,Address2,City,State,Country, Zip, Telephone,Fax,EmailAddress,ContactPerson,Contactnumber,Updatedby,Hub from Funders where Sync_Type='U'";

            // FundDetails Table Select Query for insert and update Checked
            selectQueries[16] = "select fd.Id as localId,f.Server_Id as fk_FunderId,Sponser_Type,Amount,Currency,Notes,fd.Createdby,fd.Updatedby,DateOfTransaction,Project from FundDetails fd left join Funders f on fd.fk_FunderId=f.Id where fd.Sync_Type='N'";
            selectQueries[17] = "select fd.Id as localId,fd.Server_Id as ID,f.Server_Id  as fk_FunderId,Sponser_Type,Amount,Currency,Notes,fd.Updatedby,DateOfTransaction,Project from FundDetails fd left join Funders f on fd.fk_FunderId=f.Id where fd.Sync_Type='U'";

            // FundsAllocation Table Select Query for insert and update Checked --here we r joining the funderdetails table to get the fundertransactionid using funddetails fd 'fd.Server_Id as fk_FunderTransactionId'  
            selectQueries[18] = "select fa.Id as localId,fa.Amount,fa.Currency,fa.Notes,fa.fk_CenterId,fa.Createdby,fa.Updatedby,fd.Server_Id as fk_FunderTransactionId,fa.DateOfTransaction,fa.Remaining_Amount_For_Loan_Allocation,fa.project from FundsAllocation fa left join  FundDetails fd on fa.fk_FunderTransactionId=fd.Id where fa.Sync_Type='N'";
            //selectQueries[19] = "select Id as localId ,fk_LoanAccountId,fk_FunderId,Amount, fk_FundsAlloc from LoanFundSourcing where Sync_Type='N'";
            //selectQueries[20] = "select   Id as localId,Server_Id as ID,fk_LoanAccountId,fk_FunderId,Amount, fk_FundsAlloc from LoanFundSourcing where Sync_Type='U'";

            // SavingAccount Table Select Query for insert and update 
            selectQueries[19] = "select sa.Id as localId, mem.Server_Id as fk_MemberId, sa.Status,sa.Remarks,interest,Opendate,u.Server_Id as OpenedBy,OpenedByName,sa.Createdby,sa.Updatedby,fk_Savingtype,passbookno from SavingAccount sa left join Members mem on sa.fk_MemberId=mem.Id join Users u on sa.OpenedBy=u.Id where  sa.Sync_Type='N'";
            selectQueries[20] = "select sa.Id as localId,sa.Server_Id as ID,mem.Server_Id as fk_MemberId, sa.Status,sa.Remarks,sa.Opendate, u.Server_Id as OpenedBy,sa.Createdby,sa.Updatedby,fk_Savingtype,passbookno from SavingAccount sa left join Members mem on  sa.fk_MemberId=mem.Id join Users u on sa.OpenedBy=u.Id where sa.Sync_Type='U'";

            // SavingAccountTransactions Table Select Query for insert and update
            selectQueries[21] = "select sat.Id as localId,mem.Server_Id as fk_MemberId,Tran_type,sa.server_id as fk_Accountid,Amount,u.Server_Id as fk_AgentId,TransactionDate,sat.Remarks,sat.Createdby,sat.Updatedby,CurrentBal from SavingAccountTransactions sat left join SavingAccount sa on sat.fk_Accountid=sa.Id left join members mem on sat.fk_MemberId=mem.Id join Users u on sat.fk_AgentId=u.Id where sat.Sync_Type='N'";
            selectQueries[22] = "select  sat.Id as localId,sat.Server_Id as ID, mem.Server_Id as fk_MemberId,sat.tran_type,sa.server_id as fk_Accountid,sat.Amount,u.Server_Id as fk_AgentId,sat.TransactionDate,sat.remarks,sat.Updatedby from SavingAccountTransactions sat left join SavingAccount sa on sat.fk_Accountid=sa.Id left join members mem on sat.fk_MemberId=mem.Id join Users u on sat.fk_AgentId=u.Id  where sat.Sync_Type='U'";

            // LoanAccount Table Select Query for insert and update
            selectQueries[23] = "select la.Id as localId,0 as ID, mem.Server_Id as fk_MemberId,la.AccountNumber,la.LoanType,la.Status,la.LoanAmount,la.EMISchedule,la.InterestPercent,la.Tenure,la.LoanAppliedDate,la.LoanSanctionedDate,la.EMIPlannedStart,la.EMIPlannedEnd,la.ApprovedBy,la.EMIPrincipal,la.totalcharges,la.Remarks,la.Createdby,la.Updatedby,la.IsUPFLoan,la.RelationName,la.Address,la.AreaFederation,la.Phone,la.WaterSource,la.ToiletFacility,la.SizeOfHouse,la.HouseRoof,la.Walls,la.HouseFloor,la.HousePattaOrSecurity,la.ConstructionType,la.ConstructionDone,la.TotalHouseCost,la.ConstructionTime,la.DepositWithLoanReq,la.passbookno from LoanAccount la left join  Members mem on la.fk_MemberId=mem.Id where la.Sync_Type='N'";
            selectQueries[24] = "select la.Id as localId,la.Server_Id as ID, mem.Server_Id as fk_MemberId,la.AccountNumber,la.LoanType,la.Status,la.LoanAmount,la.EMISchedule,la.InterestPercent,la.Tenure,la.LoanAppliedDate,la.LoanSanctionedDate,la.EMIPlannedStart,la.EMIPlannedEnd,la.EMIPrincipal,la.totalcharges,la.ApprovedBy,la.Remarks,la.Createdby,la.Updatedby,la.IsUPFLoan,la.RelationName,la.Address,la.AreaFederation,la.Phone,la.WaterSource,la.ToiletFacility,la.SizeOfHouse,la.HouseRoof,la.Walls,la.HouseFloor,la.HousePattaOrSecurity,la.ConstructionType,la.ConstructionDone,la.TotalHouseCost,la.ConstructionTime, la.DepositWithLoanReq,la.passbookno from LoanAccount la left join  Members mem on la.fk_MemberId=mem.Id  where la.Sync_Type='U'";

            // LoanAccountTransactions Table Select Query for insert and update
            selectQueries[25] = "select  lat.Id as localId,mem.Server_Id as fk_MemberId,lat.Tran_type,la.server_id as fk_Accountid,lat.Amount,u.Server_Id as fk_AgentId,lat.TransactionDate,lat.PrinciplePart,lat.InterestPart,lat.Remarks,lat.Createdby,lat.Updatedby,lat.PaidFromSavings from LoanAccountTransactions lat left join LoanAccount la on lat.fk_Accountid=la.Id left join members mem on lat.fk_MemberId=mem.Id join Users u on lat.fk_AgentId=u.Id  where lat.Sync_Type='N'";
            selectQueries[26] = "select lat.Id as localId, lat.Server_Id as ID,mem.Server_Id as fk_MemberId,Tran_type,la.server_id as fk_Accountid,Amount,u.Server_Id as fk_AgentId,TransactionDate,PrinciplePart,InterestPart,lat.Remarks,lat.Updatedby,PaidFromSavings from LoanAccountTransactions lat left join members mem on lat.fk_MemberId=mem.Id left join LoanAccount la on lat.fk_Accountid=la.Id join Users u on lat.fk_AgentId=u.Id where lat.Sync_Type='U'";

            // DeviceManagement Table Select Query for insert and update
            selectQueries[27] = "Select d.Id as localId,c.Server_Id as SlumId ,u.Server_Id  as UserId,  MacAddress, d.Createdby, d.Updatedby from DeviceManagement  d left join Users u on d.UserId=u.Id  left join CodeValue c on d.SlumId=c.Id where d.Sync_Type='N'";
            selectQueries[28] = "Select d.Id as localId,d.Server_Id as ID, c.Server_Id as SlumId ,u.Server_Id  as UserId, MacAddress , d.Updatedby from DeviceManagement d left join Users u on d.UserId=u.Id left join CodeValue c on d.SlumId=c.Id where d.Sync_Type='U'";

            // DailyLoanWithdrawal Table Select Query for insert
            selectQueries[29] = "Select d.Id as localId, TransactionDate,  c.Server_Id as fk_SlumId, Type, m.Server_Id as fk_MemberId, Amount, ServiceCharge, d.Createdby,d.Updatedby from DailyLoanWithdrawal d left join Members m on d.fk_MemberId=m.Id left join CodeValue c  on d.fk_SlumId=c.Id where  d.Sync_Type='N'";

            // DailySavingsRepayment Table Select Query for insert 
            selectQueries[30] = "Select Id as localId,TransactionDate, fk_SlumId, Type,Amount, Createdby, Updatedby from DailySavingsRepayment where Sync_Type='N'";

            // MemberFamilyDetails Table Select Query for insert and update
            selectQueries[31] = "select md.Id as localId, md.Name,md.Gender,md.Age, md.Qualifiaction, md.IsMember,mf.Server_Id as Fk_MemberFamilyId, mem.Server_Id as Fk_Members from MemberFamilyDetails md left join  MemberFamily mf on md.Fk_MemberFamilyId=mf.MemberFamilyId left join Members mem on md.Fk_Members=mem.Id where md.Sync_Type='N'";                        
            selectQueries[32] = "select md.Id as localId, md.Server_Id as ID, md.Name,md.Gender,md.Age, md.Qualifiaction, md.IsMember,mf.Server_Id as Fk_MemberFamilyId, mem.Server_Id as Fk_Members from MemberFamilyDetails md left join  MemberFamily mf on md.Fk_MemberFamilyId=mf.MemberFamilyId left join Members mem on md.Fk_Members=mem.Id where md.Sync_Type='U'";

            // MembersFiles Table Select Query for insert 
            selectQueries[33] = "select mf.Id as localId,m.Server_Id as fk_MemberId,mf.FileName,mf.Document_Type,mf.Createdby,mf.Updatedby  from MembersFiles mf  left join Members m on mf.fk_MemberId=m.Id where mf.Sync_type='N'";
            // selectQueries[34] = "Select cast(cast(photo as varbinary(max)) as varchar(max)) as Photo,MemberId as localId From Members where Sync_type='U'";

            // MemberFamily Table Select Query for insert 
            selectQueries[34] = "select FamilyPhotograph,MemberFamilyId as localId from MemberFamily where Sync_Type='N'";

            // GroupTransactions Table Select Query for insert and update
            //selectQueries[34] = "select fk_MemberId,Status,Tran_type,Amount,fk_AgentId,Opendate,Remarks,InterestPercent,PayOutEndDate,Createdby,Updatedby from UPFAccount where Sync_Type='N'";
            selectQueries[35] = "select gt.Id as localId,0 as ID,c.Server_Id as fk_SlumId,g.server_Id as fk_GroupId,gt.TransactionDate,gt.Type,gt.Amount,gt.Createdby,gt.updatedby,gt.Remarks from GroupTransactions gt left join Codevalue c on  gt. fk_SlumId=c.id left join GroupInfo g  on gt.fk_GroupId=g.Id where gt.Sync_type='N'";
            selectQueries[36] = "select gt.Id as localId,gt.Server_Id as ID,c.Server_Id as fk_SlumId,g.server_Id as fk_GroupId,gt.TransactionDate,gt.Type,gt.Amount,gt.Createdby,gt.updatedby,gt.Remarks from GroupTransactions gt left join Codevalue c on  gt. fk_SlumId=c.id left join GroupInfo g  on gt.fk_GroupId=g.Id where gt.Sync_type='U'";

            // GroupUPFAccount Table Select Query for insert
            selectQueries[37] = "select Gu.Id,0 as ID,G.Server_Id as fk_Groupid,Gu.Opendate,Gu.Remarks,Gu.InterestPercent,PayOutEndDate,Gu.Createdby,Gu.Updatedby ,Gu.Status from   GroupUPFAccount Gu left join GroupInfo G on Gu.fk_GroupId=G.Id  where Gu.Sync_Type='N'";

            // GroupUPFAccountTransactions Table Select Query for insert
            selectQueries[38] = "select Gu.Id as localId,0 as ID,G.Server_Id as fk_Groupid,Gu.Tran_type,Gu.Amount,Gu.TransactionDate,Gu.Remarks,Guu.InterestPercent,guu.PayOutEndDate,Gu.Createdby,Gu.Updatedby ,Gu.fk_AgentId,Guu.Status,Guu.Server_Id from  GroupUPFAccountTransactions Gu left join GroupInfo G on Gu.fk_GroupId=G.Id left join GroupUPFAccount Guu on guu.id=gu.fk_AccountId where Gu.Sync_Type='N'";

            // GroupUPFAccountTransactions Table Select Query for insert
            selectQueries[39] = "select G.Id,0 as ID,Status,Amount,fk_AgentId,TransactionDate,Gp.Remarks,PayOutEndDate,Gp.Updatedby,0,PayMode,fk_AccountId from GroupUPFAccountTransactions  Gp left join GroupUPFAccount G on G.Id=Gp.fk_AccountId where Gp.Sync_Type='N'";

            // GroupUPFAccount Table Select Query for update
            selectQueries[41] = "select G.Id as localId,G.Server_Id as ID,G.Status,Gp.Amount,Gp.fk_AgentId,TransactionDate,G.Remarks,PayOutEndDate,G.Updatedby,0,PayMode,Gp.fk_AccountId from GroupUPFAccount G left join GroupUPFAccountTransactions Gp on G.Id=Gp.fk_AccountId where G.Sync_Type='U' and Gp.Tran_type='CRE'";

            // GroupUPFAccount Table Select Query for update
            selectQueries[40] = "select Id,Server_Id as ID,Status,Opendate,Remarks,PayOutEndDate,Updatedby from GroupUPFAccount where  Sync_Type='U'";

            // GroupUPFAccountTransactions Table Select Query for update
            selectQueries[42] = "select Gu.Id,Gu.Server_Id as ID,Status,Amount,fk_AgentId,TransactionDate,Gu.Remarks,PayOutEndDate,G.Updatedby,0,PayMode,Gu.Server_Id from GroupUPFAccountTransactions Gu left join GroupUPFAccount G  on G.Id=Gu.fk_AccountId where Gu.Sync_Type='U' and Gu.Tran_type='CRE' ";

            // UPFAccount Table Select Query for insert
            selectQueries[43] = "select U.Id as localId ,0 as ID,m.Server_Id as fk_MemberId,U.Status,Opendate,U.Remarks,InterestPercent,PayOutEndDate,U.Createdby,U.Updatedby from UPFAccount U left join Members m on m.id=U.fk_MemberId where U.Sync_Type='N' ";

            // UPFAccountTransactions Table Select Query for insert
            selectQueries[44] = "select Up.Id,0 as ID,m.Server_Id as fk_MemberId,U.Status,Tran_type,Amount,fk_AgentId,TransactionDate,Up.Remarks,InterestPercent,PayOutEndDate,Up.Createdby,Up.Updatedby,U.Server_Id from  UPFAccountTransactions Up left join UPFAccount U on U.Id=Up.fk_Accounid  left join Members m on m.id=U.fk_MemberId where Up.Sync_Type='N'";

            // UPFAccountTransactions Table Select Query for insert
            selectQueries[45] = "select Up.Id as localId,0 as ID,U.Status,Up.Amount,Up.fk_AgentId,Up.TransactionDate,Up.Remarks,U.PayOutEndDate,Up.Updatedby,1,Up.PayMode,Up.fk_Accounid from UPFAccountTransactions Up left join UPFAccount U on U.Id = Up.fk_Accounid where Up.Sync_Type='N'";

            // UPFAccount Table Select Query for update
            selectQueries[46] = "select Id,Server_Id as ID,Status,Opendate,Remarks,PayOutEndDate,Updatedby from UPFAccount where  Sync_Type='U'";

            // UPFAccount Table Select Query for update
            selectQueries[47] = "select U.Id as localId,U.Server_Id as ID,U.status,Up.Amount,Up.fk_AgentId,Up.TransactionDate,U.Remarks,U.PayOutEndDate,U.Updatedby,0,Up.Paymode,Up.fk_Accounid from UPFAccount U left join UPFAccountTransactions Up on U.Id= Up.fk_Accounid where U.Sync_Type='U'and Up.Tran_type='CRE'";

            // UPFAccountTransactions Table Select Query for update
            selectQueries[48] = "select Up.Id,U.Server_Id as ID,U.Status,Up.Amount,Up.fk_AgentId,Up.TransactionDate,Up.Remarks,PayOutEndDate,Up.Updatedby,0,Paymode,Up.Server_Id from UPFAccountTransactions Up left join UPFAccount U on U.Id=Up.fk_Accounid where Up.Sync_Type='U' ";
         
            insertUpdateQueries[0] = "GroupInfoInsert";
            insertUpdateQueries[1] = "GroupInfoInsert";

            insertUpdateQueries[2] = "usp_UpdateMember";
            insertUpdateQueries[3] = "usp_UpdateMember";

            insertUpdateQueries[4] = "usp_MemberFamilyInsert";
            insertUpdateQueries[5] = "usp_MemberFamilyUpdate";

            insertUpdateQueries[6] = "usp_UsersInsert";
            insertUpdateQueries[7] = "usp_UsersUpdate";

            insertUpdateQueries[8] = "usp_ExpensesInsert";
            insertUpdateQueries[9] = "usp_ExpensesInsert";

            insertUpdateQueries[10] = "usp_CodeValueInsert";
            insertUpdateQueries[11] = "usp_CodeValueUpdate";

            insertUpdateQueries[12] = "usp_CodeTypeInsert";
            insertUpdateQueries[13] = "usp_CodeTypeUpdate";

            insertUpdateQueries[14] = "usp_FundersInsert";
            insertUpdateQueries[15] = "usp_FundersUpdate";

            insertUpdateQueries[16] = "usp_FundDetailsInsert";
            insertUpdateQueries[17] = "usp_FundDetailsUpdate";

            insertUpdateQueries[18] = "usp_AllocationTransInsert";
            //insertUpdateQueries[19] = "usp_FunderSourcing";
            //insertUpdateQueries[20] = "usp_FunderSourcingUpdate";
            //insertUpdateQueries[19] = "usp_SavingAccountInsert";

            insertUpdateQueries[19] = "usp_SavingAccountInsert";
            insertUpdateQueries[20] = "usp_SavingAccountUpdate";

            insertUpdateQueries[21] = "usp_SavingAccountTransactionsInsert";
            insertUpdateQueries[22] = "usp_SavingAccountTransactionsUpdate";

            insertUpdateQueries[23] = "usp_LoanAccountInsert";
            insertUpdateQueries[24] = "usp_LoanAccountUpdate";

            insertUpdateQueries[25] = "usp_LoanAccountTransactionsInsert";
            insertUpdateQueries[26] = "usp_LoanAccountTransactionsUpdate";

            insertUpdateQueries[27] = "usp_DeviceInfoInsert";
            insertUpdateQueries[28] = "usp_DeviceInfoUpdate";

            insertUpdateQueries[29] = "usp_InsertLoan_Withdrawal";

            insertUpdateQueries[30] = "usp_InsertTotalSaving_Repayment";

            insertUpdateQueries[31] = "usp_FamilyDetailsGrid_Insert";
            insertUpdateQueries[32] = "usp_FamilyDetailsGrid_Update";

            insertUpdateQueries[33] = "usp_SaveMemberImage";

            // insertUpdateQueries[34] = "usp_SaveMemberPhoto";
            insertUpdateQueries[34] = "usp_SaveFamilyPhoto";
            //insertUpdateQueries[34] = "usp_UPFAccountTransactionsInsert";
            insertUpdateQueries[35] = "usp_InsertGrouptransaction";
            insertUpdateQueries[36] = "usp_updateGroupTransaction";

            insertUpdateQueries[37] = "InsertGroupUPFAccount";

            insertUpdateQueries[38] = "InsertGroupUPFAccounttrans";

            insertUpdateQueries[39] = "InsertGroupUPFPayouttrans";
            insertUpdateQueries[41] = "InsertGroupUPFPayouttrans";

            insertUpdateQueries[40] = "UpdateGroupUpfAccount";

            insertUpdateQueries[42] = "InsertGroupUPFPayouttrans";

            insertUpdateQueries[43] = "InsertUPFAccount";

            insertUpdateQueries[44] = "InsertUPFAccounttrans";
            insertUpdateQueries[45] = "InsertUPFPayouttrans";

            insertUpdateQueries[46] = "UpdateUpfAccount";

            insertUpdateQueries[47] = "InsertUPFPayouttrans";
            insertUpdateQueries[48] = "InsertUPFPayouttrans";

            syncdataTable[0] = "GroupInfo";
            syncdataTable[1] = "GroupInfo";

            syncdataTable[2] = "Members";
            syncdataTable[3] = "Members";

            syncdataTable[4] = "MemberFamily";
            syncdataTable[5] = "MemberFamily";

            syncdataTable[6] = "Users";
            syncdataTable[7] = "Users";

            syncdataTable[8] = "Expenses";
            syncdataTable[9] = "Expenses";

            syncdataTable[10] = "CodeValue";
            syncdataTable[11] = "CodeValue";

            syncdataTable[12] = "CodeType";
            syncdataTable[13] = "CodeType";

            syncdataTable[14] = "Funders";
            syncdataTable[15] = "Funders";

            syncdataTable[16] = "FundDetails";
            syncdataTable[17] = "FundDetails";

            syncdataTable[18] = "FundsAllocation";
            //syncdataTable[19] = "LoanFundSourcing";
            //syncdataTable[20] = "LoanFundSourcing";
            syncdataTable[19] = "SavingAccount";
            syncdataTable[20] = "SavingAccount";

            syncdataTable[21] = "SavingAccountTransactions";
            syncdataTable[22] = "SavingAccountTransactions";

            syncdataTable[23] = "LoanAccount";
            syncdataTable[24] = "LoanAccount";

            syncdataTable[25] = "LoanAccountTransactions";
            syncdataTable[26] = "LoanAccountTransactions";

            syncdataTable[27] = "DeviceManagement";
            syncdataTable[28] = "DeviceManagement";

            syncdataTable[29] = "DailyLoanWithdrawal";
            syncdataTable[30] = "DailySavingsRepayment";

            syncdataTable[31] = "MemberFamilyDetails";
            syncdataTable[32] = "MemberFamilyDetails";

            syncdataTable[33] = "MembersFiles";
            // syncdataTable[34] = "Members";
            syncdataTable[34] = "MemberFamily";

            //syncdataTable[34] = "UPFAccount";
            syncdataTable[35] = "GroupTransactions";
            syncdataTable[36] = "GroupTransactions";

            syncdataTable[37] = "GroupUPFAccount";

            syncdataTable[38] = "GroupUPFAccountTransactions";
            syncdataTable[39] = "GroupUPFAccountTransactions";

            syncdataTable[41] = "GroupUPFAccount";
            syncdataTable[40] = "GroupUPFAccount";

            syncdataTable[42] = "GroupUPFAccountTransactions";

            syncdataTable[43] = "UPFAccount";

            syncdataTable[44] = "UPFAccountTransactions";
            syncdataTable[45] = "UPFAccountTransactions";

            syncdataTable[46] = "UPFAccount";
            syncdataTable[47] = "UPFAccount";

            syncdataTable[48] = "UPFAccountTransactions";

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
