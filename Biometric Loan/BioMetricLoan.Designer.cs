﻿namespace SPARC
{
    partial class frmBioMetricSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBioMetricSystem));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.Member = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateMember = new System.Windows.Forms.ToolStripMenuItem();
            this.createMemberFamilyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchMemberFamilyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scannedDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createGroupUPFAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupInterestPayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SavingsAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateSavingsAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewUpdateSavingsAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.SavingsSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.DepositSavingsTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.WithdrawSavingsTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.loanAcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewApproveLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewUpdateLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.LoanSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.DispersalLoanTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.EMIPaymentLoanAcc = new System.Windows.Forms.ToolStripMenuItem();
            this.UPF = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateUPFAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.UPFSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.UPFInterestPayout = new System.Windows.Forms.ToolStripMenuItem();
            this.Expenses = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyAccountBalanceSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fundersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUpdateFundersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funderTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funderAllocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Admin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MasterDataManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.deviceManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSyncToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleSavingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleSavingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleWithdrawalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleLoanEMIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Reports = new System.Windows.Forms.ToolStripMenuItem();
            this.savingAccountTransactionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unpaidEMIReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upcomingEMIReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dayReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterDataSettlementWiseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savingsAndLoanSumParticularDayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupTransactionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fundAllocationReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyCenterAccountBalanceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupUPFTransactionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.membersReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savingAccountReportNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitApp = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.scSearch = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPassbook = new System.Windows.Forms.TextBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgMembers = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PassbookNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SlumName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.txtmemberid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbioid = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgSavLoanDetails = new System.Windows.Forms.DataGridView();
            this.lblEMIDue = new System.Windows.Forms.Label();
            this.lblSavingAccStatus = new System.Windows.Forms.Label();
            this.btnMemberReport = new System.Windows.Forms.Button();
            this.lblLoanAmount = new System.Windows.Forms.Label();
            this.pbMember = new System.Windows.Forms.PictureBox();
            this.lblLoanStatusValue = new System.Windows.Forms.Label();
            this.lblLoanAccNo = new System.Windows.Forms.Label();
            this.lblSavingBalance = new System.Windows.Forms.Label();
            this.lblSavingAccNo = new System.Windows.Forms.Label();
            this.lblSlum = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatuslblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSearch)).BeginInit();
            this.scSearch.Panel1.SuspendLayout();
            this.scSearch.Panel2.SuspendLayout();
            this.scSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMembers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSavLoanDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMember)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Member,
            this.groupToolStripMenuItem,
            this.SavingsAcc,
            this.loanAcToolStripMenuItem,
            this.UPF,
            this.Expenses,
            this.dailyAccountBalanceSheetToolStripMenuItem,
            this.fundersToolStripMenuItem,
            this.Admin,
            this.multipleSavingsToolStripMenuItem,
            this.Reports,
            this.ExitApp,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1092, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // Member
            // 
            this.Member.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.UpdateMember,
            this.createMemberFamilyToolStripMenuItem,
            this.searchMemberFamilyToolStripMenuItem,
            this.scannedDocumentsToolStripMenuItem});
            this.Member.Name = "Member";
            this.Member.Size = new System.Drawing.Size(64, 20);
            this.Member.Text = "Member";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem3.Text = "Create Member";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // UpdateMember
            // 
            this.UpdateMember.Name = "UpdateMember";
            this.UpdateMember.Size = new System.Drawing.Size(195, 22);
            this.UpdateMember.Text = "Update Member";
            this.UpdateMember.Click += new System.EventHandler(this.UpdateMember_Click);
            // 
            // createMemberFamilyToolStripMenuItem
            // 
            this.createMemberFamilyToolStripMenuItem.Name = "createMemberFamilyToolStripMenuItem";
            this.createMemberFamilyToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.createMemberFamilyToolStripMenuItem.Text = "Create Member Family";
            this.createMemberFamilyToolStripMenuItem.Click += new System.EventHandler(this.createMemberFamilyToolStripMenuItem_Click);
            // 
            // searchMemberFamilyToolStripMenuItem
            // 
            this.searchMemberFamilyToolStripMenuItem.Name = "searchMemberFamilyToolStripMenuItem";
            this.searchMemberFamilyToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.searchMemberFamilyToolStripMenuItem.Text = "Search Member Family";
            this.searchMemberFamilyToolStripMenuItem.Click += new System.EventHandler(this.searchMemberFamilyToolStripMenuItem_Click);
            // 
            // scannedDocumentsToolStripMenuItem
            // 
            this.scannedDocumentsToolStripMenuItem.Name = "scannedDocumentsToolStripMenuItem";
            this.scannedDocumentsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.scannedDocumentsToolStripMenuItem.Text = "Scanned Documents";
            this.scannedDocumentsToolStripMenuItem.Click += new System.EventHandler(this.scannedDocumentsToolStripMenuItem_Click);
            // 
            // groupToolStripMenuItem
            // 
            this.groupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createGroupToolStripMenuItem,
            this.groupTransactionsToolStripMenuItem,
            this.createGroupUPFAccountToolStripMenuItem,
            this.groupInterestPayoutToolStripMenuItem});
            this.groupToolStripMenuItem.Name = "groupToolStripMenuItem";
            this.groupToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.groupToolStripMenuItem.Text = "Group";
            // 
            // createGroupToolStripMenuItem
            // 
            this.createGroupToolStripMenuItem.Name = "createGroupToolStripMenuItem";
            this.createGroupToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.createGroupToolStripMenuItem.Text = "Create Group";
            this.createGroupToolStripMenuItem.Click += new System.EventHandler(this.createGroupToolStripMenuItem_Click);
            // 
            // groupTransactionsToolStripMenuItem
            // 
            this.groupTransactionsToolStripMenuItem.Name = "groupTransactionsToolStripMenuItem";
            this.groupTransactionsToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.groupTransactionsToolStripMenuItem.Text = "GroupTransactions";
            this.groupTransactionsToolStripMenuItem.Click += new System.EventHandler(this.groupTransactionsToolStripMenuItem_Click);
            // 
            // createGroupUPFAccountToolStripMenuItem
            // 
            this.createGroupUPFAccountToolStripMenuItem.Name = "createGroupUPFAccountToolStripMenuItem";
            this.createGroupUPFAccountToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.createGroupUPFAccountToolStripMenuItem.Text = "Create Group UPF Account";
            this.createGroupUPFAccountToolStripMenuItem.Click += new System.EventHandler(this.createGroupUPFAccountToolStripMenuItem_Click);
            // 
            // groupInterestPayoutToolStripMenuItem
            // 
            this.groupInterestPayoutToolStripMenuItem.Name = "groupInterestPayoutToolStripMenuItem";
            this.groupInterestPayoutToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.groupInterestPayoutToolStripMenuItem.Text = "Group Interest Payout";
            this.groupInterestPayoutToolStripMenuItem.Click += new System.EventHandler(this.groupInterestPayoutToolStripMenuItem_Click);
            // 
            // SavingsAcc
            // 
            this.SavingsAcc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateSavingsAccount,
            this.ViewUpdateSavingsAccount,
            this.SavingsSeparator,
            this.DepositSavingsTrans,
            this.WithdrawSavingsTrans});
            this.SavingsAcc.Name = "SavingsAcc";
            this.SavingsAcc.Size = new System.Drawing.Size(107, 20);
            this.SavingsAcc.Text = "Savings Account";
            // 
            // CreateSavingsAccount
            // 
            this.CreateSavingsAccount.Name = "CreateSavingsAccount";
            this.CreateSavingsAccount.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.CreateSavingsAccount.Size = new System.Drawing.Size(218, 22);
            this.CreateSavingsAccount.Text = "Create Account";
            this.CreateSavingsAccount.Click += new System.EventHandler(this.CreateSavingsAccount_Click);
            // 
            // ViewUpdateSavingsAccount
            // 
            this.ViewUpdateSavingsAccount.Name = "ViewUpdateSavingsAccount";
            this.ViewUpdateSavingsAccount.Size = new System.Drawing.Size(218, 22);
            this.ViewUpdateSavingsAccount.Text = "View && Update Account";
            this.ViewUpdateSavingsAccount.Click += new System.EventHandler(this.ViewUpdateSavingsAccount_Click);
            // 
            // SavingsSeparator
            // 
            this.SavingsSeparator.Name = "SavingsSeparator";
            this.SavingsSeparator.Size = new System.Drawing.Size(215, 6);
            // 
            // DepositSavingsTrans
            // 
            this.DepositSavingsTrans.Name = "DepositSavingsTrans";
            this.DepositSavingsTrans.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.DepositSavingsTrans.Size = new System.Drawing.Size(218, 22);
            this.DepositSavingsTrans.Text = "Deposit Transaction";
            this.DepositSavingsTrans.Click += new System.EventHandler(this.DepositSavingsTrans_Click);
            // 
            // WithdrawSavingsTrans
            // 
            this.WithdrawSavingsTrans.Name = "WithdrawSavingsTrans";
            this.WithdrawSavingsTrans.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.WithdrawSavingsTrans.Size = new System.Drawing.Size(218, 22);
            this.WithdrawSavingsTrans.Text = "Withdrawal Transaction";
            this.WithdrawSavingsTrans.Click += new System.EventHandler(this.WithdrawSavingsTrans_Click);
            // 
            // loanAcToolStripMenuItem
            // 
            this.loanAcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateLoanAcc,
            this.ViewApproveLoanAcc,
            this.ViewUpdateLoanAcc,
            this.LoanSeparator,
            this.DispersalLoanTrans,
            this.EMIPaymentLoanAcc});
            this.loanAcToolStripMenuItem.Name = "loanAcToolStripMenuItem";
            this.loanAcToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.loanAcToolStripMenuItem.Text = "Loan Account";
            // 
            // CreateLoanAcc
            // 
            this.CreateLoanAcc.Name = "CreateLoanAcc";
            this.CreateLoanAcc.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.CreateLoanAcc.Size = new System.Drawing.Size(208, 22);
            this.CreateLoanAcc.Text = "Create Account";
            this.CreateLoanAcc.Click += new System.EventHandler(this.CreateLoanAcc_Click);
            // 
            // ViewApproveLoanAcc
            // 
            this.ViewApproveLoanAcc.Name = "ViewApproveLoanAcc";
            this.ViewApproveLoanAcc.Size = new System.Drawing.Size(208, 22);
            this.ViewApproveLoanAcc.Text = "View && Approve Account";
            this.ViewApproveLoanAcc.Visible = false;
            this.ViewApproveLoanAcc.Click += new System.EventHandler(this.ViewApproveLoanAcc_Click);
            // 
            // ViewUpdateLoanAcc
            // 
            this.ViewUpdateLoanAcc.Name = "ViewUpdateLoanAcc";
            this.ViewUpdateLoanAcc.Size = new System.Drawing.Size(208, 22);
            this.ViewUpdateLoanAcc.Text = "View && Update Account";
            this.ViewUpdateLoanAcc.Click += new System.EventHandler(this.ViewUpdateLoanAcc_Click);
            // 
            // LoanSeparator
            // 
            this.LoanSeparator.Name = "LoanSeparator";
            this.LoanSeparator.Size = new System.Drawing.Size(205, 6);
            // 
            // DispersalLoanTrans
            // 
            this.DispersalLoanTrans.Name = "DispersalLoanTrans";
            this.DispersalLoanTrans.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.DispersalLoanTrans.Size = new System.Drawing.Size(208, 22);
            this.DispersalLoanTrans.Text = "Dispersal Transaction";
            this.DispersalLoanTrans.Click += new System.EventHandler(this.DispersalLoanTrans_Click);
            // 
            // EMIPaymentLoanAcc
            // 
            this.EMIPaymentLoanAcc.Name = "EMIPaymentLoanAcc";
            this.EMIPaymentLoanAcc.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.EMIPaymentLoanAcc.Size = new System.Drawing.Size(208, 22);
            this.EMIPaymentLoanAcc.Text = "EMI Payment";
            this.EMIPaymentLoanAcc.Click += new System.EventHandler(this.EMIPaymentLoanAcc_Click);
            // 
            // UPF
            // 
            this.UPF.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateUPFAccount,
            this.UPFSeparator,
            this.UPFInterestPayout});
            this.UPF.Name = "UPF";
            this.UPF.Size = new System.Drawing.Size(40, 20);
            this.UPF.Text = "UPF";
            this.UPF.Click += new System.EventHandler(this.UPF_Click);
            // 
            // CreateUPFAccount
            // 
            this.CreateUPFAccount.Name = "CreateUPFAccount";
            this.CreateUPFAccount.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.CreateUPFAccount.Size = new System.Drawing.Size(189, 22);
            this.CreateUPFAccount.Text = "Manage Accounts";
            this.CreateUPFAccount.Click += new System.EventHandler(this.CreateUPFAccount_Click);
            // 
            // UPFSeparator
            // 
            this.UPFSeparator.Name = "UPFSeparator";
            this.UPFSeparator.Size = new System.Drawing.Size(186, 6);
            // 
            // UPFInterestPayout
            // 
            this.UPFInterestPayout.Name = "UPFInterestPayout";
            this.UPFInterestPayout.Size = new System.Drawing.Size(189, 22);
            this.UPFInterestPayout.Text = "Interest Payout";
            this.UPFInterestPayout.Click += new System.EventHandler(this.UPFInterestPayout_Click);
            // 
            // Expenses
            // 
            this.Expenses.Name = "Expenses";
            this.Expenses.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.Expenses.Size = new System.Drawing.Size(61, 20);
            this.Expenses.Text = "Expense";
            this.Expenses.Click += new System.EventHandler(this.Expenses_Click);
            // 
            // dailyAccountBalanceSheetToolStripMenuItem
            // 
            this.dailyAccountBalanceSheetToolStripMenuItem.Name = "dailyAccountBalanceSheetToolStripMenuItem";
            this.dailyAccountBalanceSheetToolStripMenuItem.Size = new System.Drawing.Size(175, 20);
            this.dailyAccountBalanceSheetToolStripMenuItem.Text = "Daily Center Account Balance";
            this.dailyAccountBalanceSheetToolStripMenuItem.Click += new System.EventHandler(this.dailyAccountBalanceSheetToolStripMenuItem_Click);
            // 
            // fundersToolStripMenuItem
            // 
            this.fundersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUpdateFundersToolStripMenuItem,
            this.funderTransactionToolStripMenuItem,
            this.funderAllocationToolStripMenuItem});
            this.fundersToolStripMenuItem.Name = "fundersToolStripMenuItem";
            this.fundersToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.fundersToolStripMenuItem.Text = "Funders";
            this.fundersToolStripMenuItem.Click += new System.EventHandler(this.fundersToolStripMenuItem_Click);
            // 
            // createUpdateFundersToolStripMenuItem
            // 
            this.createUpdateFundersToolStripMenuItem.Name = "createUpdateFundersToolStripMenuItem";
            this.createUpdateFundersToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.createUpdateFundersToolStripMenuItem.Text = "Create/Update Funders";
            this.createUpdateFundersToolStripMenuItem.Click += new System.EventHandler(this.createUpdateFundersToolStripMenuItem_Click);
            // 
            // funderTransactionToolStripMenuItem
            // 
            this.funderTransactionToolStripMenuItem.Name = "funderTransactionToolStripMenuItem";
            this.funderTransactionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.funderTransactionToolStripMenuItem.Text = "Funder Transaction";
            this.funderTransactionToolStripMenuItem.Click += new System.EventHandler(this.funderTransactionToolStripMenuItem_Click);
            // 
            // funderAllocationToolStripMenuItem
            // 
            this.funderAllocationToolStripMenuItem.Name = "funderAllocationToolStripMenuItem";
            this.funderAllocationToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.funderAllocationToolStripMenuItem.Text = "Funder Allocation";
            this.funderAllocationToolStripMenuItem.Click += new System.EventHandler(this.funderAllocationToolStripMenuItem_Click);
            // 
            // Admin
            // 
            this.Admin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.MasterDataManagement,
            this.deviceManagementToolStripMenuItem,
            this.notificationToolStripMenuItem,
            this.dataSyncToolStripMenuItem});
            this.Admin.Name = "Admin";
            this.Admin.Size = new System.Drawing.Size(55, 20);
            this.Admin.Text = "Admin";
            this.Admin.Click += new System.EventHandler(this.Admin_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(211, 22);
            this.toolStripMenuItem1.Text = "User Management";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // MasterDataManagement
            // 
            this.MasterDataManagement.Name = "MasterDataManagement";
            this.MasterDataManagement.Size = new System.Drawing.Size(211, 22);
            this.MasterDataManagement.Text = "Master Data Management";
            this.MasterDataManagement.Click += new System.EventHandler(this.MasterDataManagement_Click);
            // 
            // deviceManagementToolStripMenuItem
            // 
            this.deviceManagementToolStripMenuItem.Name = "deviceManagementToolStripMenuItem";
            this.deviceManagementToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.deviceManagementToolStripMenuItem.Text = "Device Management";
            this.deviceManagementToolStripMenuItem.Click += new System.EventHandler(this.deviceManagementToolStripMenuItem_Click);
            // 
            // notificationToolStripMenuItem
            // 
            this.notificationToolStripMenuItem.Name = "notificationToolStripMenuItem";
            this.notificationToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.notificationToolStripMenuItem.Text = "Notification";
            this.notificationToolStripMenuItem.Click += new System.EventHandler(this.notificationToolStripMenuItem_Click);
            // 
            // dataSyncToolStripMenuItem
            // 
            this.dataSyncToolStripMenuItem.Name = "dataSyncToolStripMenuItem";
            this.dataSyncToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.dataSyncToolStripMenuItem.Text = "DataSync";
            this.dataSyncToolStripMenuItem.Click += new System.EventHandler(this.dataSyncToolStripMenuItem_Click_1);
            // 
            // multipleSavingsToolStripMenuItem
            // 
            this.multipleSavingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multipleSavingsToolStripMenuItem1,
            this.multipleWithdrawalToolStripMenuItem,
            this.multipleLoanEMIToolStripMenuItem});
            this.multipleSavingsToolStripMenuItem.Name = "multipleSavingsToolStripMenuItem";
            this.multipleSavingsToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.multipleSavingsToolStripMenuItem.Text = "Multiple Transactions";
            this.multipleSavingsToolStripMenuItem.Click += new System.EventHandler(this.multipleSavingsToolStripMenuItem_Click);
            // 
            // multipleSavingsToolStripMenuItem1
            // 
            this.multipleSavingsToolStripMenuItem1.Name = "multipleSavingsToolStripMenuItem1";
            this.multipleSavingsToolStripMenuItem1.Size = new System.Drawing.Size(209, 22);
            this.multipleSavingsToolStripMenuItem1.Text = "Multiple Savings";
            this.multipleSavingsToolStripMenuItem1.Click += new System.EventHandler(this.multipleSavingsToolStripMenuItem1_Click);
            // 
            // multipleWithdrawalToolStripMenuItem
            // 
            this.multipleWithdrawalToolStripMenuItem.Name = "multipleWithdrawalToolStripMenuItem";
            this.multipleWithdrawalToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.multipleWithdrawalToolStripMenuItem.Text = "Multiple Withdrawal";
            this.multipleWithdrawalToolStripMenuItem.Click += new System.EventHandler(this.multipleWithdrawalToolStripMenuItem_Click);
            // 
            // multipleLoanEMIToolStripMenuItem
            // 
            this.multipleLoanEMIToolStripMenuItem.Name = "multipleLoanEMIToolStripMenuItem";
            this.multipleLoanEMIToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.multipleLoanEMIToolStripMenuItem.Text = "Multiple Loan Installment";
            this.multipleLoanEMIToolStripMenuItem.Click += new System.EventHandler(this.multipleLoanEMIToolStripMenuItem_Click);
            // 
            // Reports
            // 
            this.Reports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.savingAccountTransactionReportToolStripMenuItem,
            this.unpaidEMIReportToolStripMenuItem,
            this.upcomingEMIReportToolStripMenuItem,
            this.toolStripMenuItem2,
            this.dayReportToolStripMenuItem,
            this.masterDataSettlementWiseReportToolStripMenuItem,
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem,
            this.savingsAndLoanSumParticularDayToolStripMenuItem,
            this.groupTransactionReportToolStripMenuItem,
            this.fundAllocationReportToolStripMenuItem,
            this.dailyCenterAccountBalanceReportToolStripMenuItem,
            this.groupUPFTransactionReportToolStripMenuItem,
            this.membersReportToolStripMenuItem,
            this.moToolStripMenuItem,
            this.savingAccountReportNewToolStripMenuItem});
            this.Reports.Name = "Reports";
            this.Reports.Size = new System.Drawing.Size(59, 20);
            this.Reports.Text = "Reports";
            this.Reports.Click += new System.EventHandler(this.Reports_Click);
            // 
            // savingAccountTransactionReportToolStripMenuItem
            // 
            this.savingAccountTransactionReportToolStripMenuItem.Name = "savingAccountTransactionReportToolStripMenuItem";
            this.savingAccountTransactionReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.savingAccountTransactionReportToolStripMenuItem.Text = "Saving Account Transaction Report";
            this.savingAccountTransactionReportToolStripMenuItem.Click += new System.EventHandler(this.savingAccountTransactionReportToolStripMenuItem_Click);
            // 
            // unpaidEMIReportToolStripMenuItem
            // 
            this.unpaidEMIReportToolStripMenuItem.Name = "unpaidEMIReportToolStripMenuItem";
            this.unpaidEMIReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.unpaidEMIReportToolStripMenuItem.Text = "Unpaid Installment Report";
            this.unpaidEMIReportToolStripMenuItem.Click += new System.EventHandler(this.unpaidEMIReportToolStripMenuItem_Click);
            // 
            // upcomingEMIReportToolStripMenuItem
            // 
            this.upcomingEMIReportToolStripMenuItem.Name = "upcomingEMIReportToolStripMenuItem";
            this.upcomingEMIReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.upcomingEMIReportToolStripMenuItem.Text = "Upcoming Installment Report";
            this.upcomingEMIReportToolStripMenuItem.Click += new System.EventHandler(this.upcomingEMIReportToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem2.Text = "Non Active Savings Account Report";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // dayReportToolStripMenuItem
            // 
            this.dayReportToolStripMenuItem.Name = "dayReportToolStripMenuItem";
            this.dayReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.dayReportToolStripMenuItem.Text = "Day Report";
            this.dayReportToolStripMenuItem.Click += new System.EventHandler(this.dayReportToolStripMenuItem_Click);
            // 
            // masterDataSettlementWiseReportToolStripMenuItem
            // 
            this.masterDataSettlementWiseReportToolStripMenuItem.Name = "masterDataSettlementWiseReportToolStripMenuItem";
            this.masterDataSettlementWiseReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.masterDataSettlementWiseReportToolStripMenuItem.Text = "Master Savings And Loan Report";
            this.masterDataSettlementWiseReportToolStripMenuItem.Click += new System.EventHandler(this.masterDataSettlementWiseReportToolStripMenuItem_Click);
            // 
            // memberTransactionReportUpToCurrentDateToolStripMenuItem
            // 
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem.Name = "memberTransactionReportUpToCurrentDateToolStripMenuItem";
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem.Text = "Savings And Loan Summary Report";
            this.memberTransactionReportUpToCurrentDateToolStripMenuItem.Click += new System.EventHandler(this.memberTransactionReportUpToCurrentDateToolStripMenuItem_Click);
            // 
            // savingsAndLoanSumParticularDayToolStripMenuItem
            // 
            this.savingsAndLoanSumParticularDayToolStripMenuItem.Name = "savingsAndLoanSumParticularDayToolStripMenuItem";
            this.savingsAndLoanSumParticularDayToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.savingsAndLoanSumParticularDayToolStripMenuItem.Text = "Savings And Loan Sum Particular Day";
            this.savingsAndLoanSumParticularDayToolStripMenuItem.Click += new System.EventHandler(this.savingsAndLoanSumParticularDayToolStripMenuItem_Click);
            // 
            // groupTransactionReportToolStripMenuItem
            // 
            this.groupTransactionReportToolStripMenuItem.Name = "groupTransactionReportToolStripMenuItem";
            this.groupTransactionReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.groupTransactionReportToolStripMenuItem.Text = "GroupTransaction Report";
            this.groupTransactionReportToolStripMenuItem.Click += new System.EventHandler(this.groupTransactionReportToolStripMenuItem_Click);
            // 
            // fundAllocationReportToolStripMenuItem
            // 
            this.fundAllocationReportToolStripMenuItem.Name = "fundAllocationReportToolStripMenuItem";
            this.fundAllocationReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.fundAllocationReportToolStripMenuItem.Text = "Fund Allocation Report";
            this.fundAllocationReportToolStripMenuItem.Click += new System.EventHandler(this.fundAllocationReportToolStripMenuItem_Click);
            // 
            // dailyCenterAccountBalanceReportToolStripMenuItem
            // 
            this.dailyCenterAccountBalanceReportToolStripMenuItem.Name = "dailyCenterAccountBalanceReportToolStripMenuItem";
            this.dailyCenterAccountBalanceReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.dailyCenterAccountBalanceReportToolStripMenuItem.Text = "Daily Center Account Balance Report";
            this.dailyCenterAccountBalanceReportToolStripMenuItem.Click += new System.EventHandler(this.dailyCenterAccountBalanceReportToolStripMenuItem_Click);
            // 
            // groupUPFTransactionReportToolStripMenuItem
            // 
            this.groupUPFTransactionReportToolStripMenuItem.Name = "groupUPFTransactionReportToolStripMenuItem";
            this.groupUPFTransactionReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.groupUPFTransactionReportToolStripMenuItem.Text = "Group UPF Transaction Report";
            this.groupUPFTransactionReportToolStripMenuItem.Click += new System.EventHandler(this.groupUPFTransactionReportToolStripMenuItem_Click);
            // 
            // membersReportToolStripMenuItem
            // 
            this.membersReportToolStripMenuItem.Name = "membersReportToolStripMenuItem";
            this.membersReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.membersReportToolStripMenuItem.Text = "Members Report";
            this.membersReportToolStripMenuItem.Click += new System.EventHandler(this.membersReportToolStripMenuItem_Click);
            // 
            // moToolStripMenuItem
            // 
            this.moToolStripMenuItem.Name = "moToolStripMenuItem";
            this.moToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.moToolStripMenuItem.Text = "Monthwise Report ";
            this.moToolStripMenuItem.Click += new System.EventHandler(this.moToolStripMenuItem_Click);
            // 
            // savingAccountReportNewToolStripMenuItem
            // 
            this.savingAccountReportNewToolStripMenuItem.Name = "savingAccountReportNewToolStripMenuItem";
            this.savingAccountReportNewToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.savingAccountReportNewToolStripMenuItem.Text = "Members Saving Accounts Report";
            this.savingAccountReportNewToolStripMenuItem.Click += new System.EventHandler(this.savingAccountReportNewToolStripMenuItem_Click);
            // 
            // ExitApp
            // 
            this.ExitApp.Name = "ExitApp";
            this.ExitApp.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.ExitApp.Size = new System.Drawing.Size(37, 20);
            this.ExitApp.Text = "Exit";
            this.ExitApp.Click += new System.EventHandler(this.ExitApp_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // scMain
            // 
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.Location = new System.Drawing.Point(0, 24);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.AutoScroll = true;
            this.scMain.Panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("scMain.Panel1.BackgroundImage")));
            this.scMain.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.scMain.Panel1.Controls.Add(this.groupBox1);
            this.scMain.Panel1.Controls.Add(this.lblCountry);
            this.scMain.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.scMain_Panel1_Paint);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.scSearch);
            this.scMain.Size = new System.Drawing.Size(1092, 748);
            this.scMain.SplitterDistance = 750;
            this.scMain.SplitterWidth = 6;
            this.scMain.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(278, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 104);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LAST TRANSACTION DETAILS";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 81);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "label15";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "label13";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "label11";
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountry.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCountry.Location = new System.Drawing.Point(21, 13);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(0, 45);
            this.lblCountry.TabIndex = 0;
            this.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scSearch
            // 
            this.scSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scSearch.Location = new System.Drawing.Point(0, 0);
            this.scSearch.Name = "scSearch";
            this.scSearch.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSearch.Panel1
            // 
            this.scSearch.Panel1.AutoScroll = true;
            this.scSearch.Panel1.Controls.Add(this.label6);
            this.scSearch.Panel1.Controls.Add(this.txtPassbook);
            this.scSearch.Panel1.Controls.Add(this.cmbHub);
            this.scSearch.Panel1.Controls.Add(this.label10);
            this.scSearch.Panel1.Controls.Add(this.btnSearch);
            this.scSearch.Panel1.Controls.Add(this.dgMembers);
            this.scSearch.Panel1.Controls.Add(this.label8);
            this.scSearch.Panel1.Controls.Add(this.cmbstate);
            this.scSearch.Panel1.Controls.Add(this.label7);
            this.scSearch.Panel1.Controls.Add(this.cmbslum);
            this.scSearch.Panel1.Controls.Add(this.label5);
            this.scSearch.Panel1.Controls.Add(this.cmbdistrict);
            this.scSearch.Panel1.Controls.Add(this.label4);
            this.scSearch.Panel1.Controls.Add(this.cmbcountry);
            this.scSearch.Panel1.Controls.Add(this.txtmemberid);
            this.scSearch.Panel1.Controls.Add(this.label3);
            this.scSearch.Panel1.Controls.Add(this.txtbioid);
            this.scSearch.Panel1.Controls.Add(this.label2);
            this.scSearch.Panel1.Controls.Add(this.txtname);
            this.scSearch.Panel1.Controls.Add(this.label1);
            // 
            // scSearch.Panel2
            // 
            this.scSearch.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.scSearch.Panel2.Controls.Add(this.dgSavLoanDetails);
            this.scSearch.Panel2.Controls.Add(this.lblEMIDue);
            this.scSearch.Panel2.Controls.Add(this.lblSavingAccStatus);
            this.scSearch.Panel2.Controls.Add(this.btnMemberReport);
            this.scSearch.Panel2.Controls.Add(this.lblLoanAmount);
            this.scSearch.Panel2.Controls.Add(this.pbMember);
            this.scSearch.Panel2.Controls.Add(this.lblLoanStatusValue);
            this.scSearch.Panel2.Controls.Add(this.lblLoanAccNo);
            this.scSearch.Panel2.Controls.Add(this.lblSavingBalance);
            this.scSearch.Panel2.Controls.Add(this.lblSavingAccNo);
            this.scSearch.Panel2.Controls.Add(this.lblSlum);
            this.scSearch.Panel2.Controls.Add(this.label14);
            this.scSearch.Panel2.Controls.Add(this.lblMemberID);
            this.scSearch.Panel2.Controls.Add(this.label12);
            this.scSearch.Panel2.Controls.Add(this.lblname);
            this.scSearch.Panel2.Controls.Add(this.label9);
            this.scSearch.Size = new System.Drawing.Size(336, 748);
            this.scSearch.SplitterDistance = 462;
            this.scSearch.SplitterWidth = 6;
            this.scSearch.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Hub";
            // 
            // txtPassbook
            // 
            this.txtPassbook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassbook.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassbook.Location = new System.Drawing.Point(140, 8);
            this.txtPassbook.MaxLength = 20;
            this.txtPassbook.Name = "txtPassbook";
            this.txtPassbook.Size = new System.Drawing.Size(162, 20);
            this.txtPassbook.TabIndex = 1;
            // 
            // cmbHub
            // 
            this.cmbHub.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(140, 109);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(162, 21);
            this.cmbHub.TabIndex = 8;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbhub_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(24, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Passbook Number";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(170, 227);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(101, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Search Member";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgMembers
            // 
            this.dgMembers.AllowUserToAddRows = false;
            this.dgMembers.AllowUserToDeleteRows = false;
            this.dgMembers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMembers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMembers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.PassbookNumber,
            this.MemberId,
            this.MemberName,
            this.SlumName});
            this.dgMembers.Location = new System.Drawing.Point(8, 253);
            this.dgMembers.Name = "dgMembers";
            this.dgMembers.ReadOnly = true;
            this.dgMembers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMembers.Size = new System.Drawing.Size(324, 188);
            this.dgMembers.TabIndex = 11;
            this.dgMembers.VirtualMode = true;
            this.dgMembers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMembers_CellDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // PassbookNumber
            // 
            this.PassbookNumber.DataPropertyName = "PassbookNumber";
            this.PassbookNumber.FillWeight = 98.92473F;
            this.PassbookNumber.HeaderText = "PassbookNumber";
            this.PassbookNumber.Name = "PassbookNumber";
            this.PassbookNumber.ReadOnly = true;
            // 
            // MemberId
            // 
            this.MemberId.DataPropertyName = "MemberId";
            this.MemberId.FillWeight = 100.5539F;
            this.MemberId.HeaderText = "Member ID";
            this.MemberId.Name = "MemberId";
            this.MemberId.ReadOnly = true;
            // 
            // MemberName
            // 
            this.MemberName.DataPropertyName = "Name";
            this.MemberName.FillWeight = 100.3406F;
            this.MemberName.HeaderText = "Name";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // SlumName
            // 
            this.SlumName.DataPropertyName = "SlumName";
            this.SlumName.FillWeight = 100.1808F;
            this.SlumName.HeaderText = "Slum/Settlement";
            this.SlumName.Name = "SlumName";
            this.SlumName.ReadOnly = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "State/Province";
            // 
            // cmbstate
            // 
            this.cmbstate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(140, 155);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(162, 21);
            this.cmbstate.TabIndex = 6;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Slum/Settlement";
            // 
            // cmbslum
            // 
            this.cmbslum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(140, 201);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(162, 21);
            this.cmbslum.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "District/City";
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(140, 178);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(162, 21);
            this.cmbdistrict.TabIndex = 7;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Country";
            // 
            // cmbcountry
            // 
            this.cmbcountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(140, 131);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(162, 21);
            this.cmbcountry.TabIndex = 5;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // txtmemberid
            // 
            this.txtmemberid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmemberid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmemberid.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmemberid.Location = new System.Drawing.Point(140, 60);
            this.txtmemberid.MaxLength = 15;
            this.txtmemberid.Name = "txtmemberid";
            this.txtmemberid.Size = new System.Drawing.Size(162, 21);
            this.txtmemberid.TabIndex = 3;
            this.txtmemberid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmemberid_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Member ID";
            // 
            // txtbioid
            // 
            this.txtbioid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtbioid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbioid.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbioid.Location = new System.Drawing.Point(140, 86);
            this.txtbioid.MaxLength = 25;
            this.txtbioid.Name = "txtbioid";
            this.txtbioid.Size = new System.Drawing.Size(162, 21);
            this.txtbioid.TabIndex = 4;
            this.txtbioid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbioid_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "BioMetric ID";
            // 
            // txtname
            // 
            this.txtname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(140, 34);
            this.txtname.MaxLength = 50;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(162, 21);
            this.txtname.TabIndex = 2;
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // dgSavLoanDetails
            // 
            this.dgSavLoanDetails.AllowUserToAddRows = false;
            this.dgSavLoanDetails.AllowUserToDeleteRows = false;
            this.dgSavLoanDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSavLoanDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSavLoanDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSavLoanDetails.Location = new System.Drawing.Point(21, 131);
            this.dgSavLoanDetails.Name = "dgSavLoanDetails";
            this.dgSavLoanDetails.ReadOnly = true;
            this.dgSavLoanDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSavLoanDetails.Size = new System.Drawing.Size(301, 118);
            this.dgSavLoanDetails.TabIndex = 22;
            this.dgSavLoanDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSavLoanDetails_CellContentClick);
            // 
            // lblEMIDue
            // 
            this.lblEMIDue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEMIDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIDue.Location = new System.Drawing.Point(124, 203);
            this.lblEMIDue.Name = "lblEMIDue";
            this.lblEMIDue.Size = new System.Drawing.Size(82, 15);
            this.lblEMIDue.TabIndex = 14;
            // 
            // lblSavingAccStatus
            // 
            this.lblSavingAccStatus.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSavingAccStatus.Location = new System.Drawing.Point(122, 121);
            this.lblSavingAccStatus.Name = "lblSavingAccStatus";
            this.lblSavingAccStatus.Size = new System.Drawing.Size(95, 15);
            this.lblSavingAccStatus.TabIndex = 21;
            // 
            // btnMemberReport
            // 
            this.btnMemberReport.Location = new System.Drawing.Point(13, 88);
            this.btnMemberReport.Name = "btnMemberReport";
            this.btnMemberReport.Size = new System.Drawing.Size(101, 37);
            this.btnMemberReport.TabIndex = 19;
            this.btnMemberReport.Text = "Member Report";
            this.btnMemberReport.UseVisualStyleBackColor = true;
            this.btnMemberReport.Click += new System.EventHandler(this.btnMemberReport_Click);
            // 
            // lblLoanAmount
            // 
            this.lblLoanAmount.Location = new System.Drawing.Point(125, 186);
            this.lblLoanAmount.Name = "lblLoanAmount";
            this.lblLoanAmount.Size = new System.Drawing.Size(95, 17);
            this.lblLoanAmount.TabIndex = 18;
            // 
            // pbMember
            // 
            this.pbMember.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMember.Location = new System.Drawing.Point(221, -28);
            this.pbMember.Name = "pbMember";
            this.pbMember.Size = new System.Drawing.Size(91, 100);
            this.pbMember.TabIndex = 0;
            this.pbMember.TabStop = false;
            // 
            // lblLoanStatusValue
            // 
            this.lblLoanStatusValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoanStatusValue.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanStatusValue.Location = new System.Drawing.Point(125, 164);
            this.lblLoanStatusValue.Name = "lblLoanStatusValue";
            this.lblLoanStatusValue.Size = new System.Drawing.Size(97, 15);
            this.lblLoanStatusValue.TabIndex = 16;
            // 
            // lblLoanAccNo
            // 
            this.lblLoanAccNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoanAccNo.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAccNo.Location = new System.Drawing.Point(125, 143);
            this.lblLoanAccNo.Name = "lblLoanAccNo";
            this.lblLoanAccNo.Size = new System.Drawing.Size(97, 15);
            this.lblLoanAccNo.TabIndex = 12;
            // 
            // lblSavingBalance
            // 
            this.lblSavingBalance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavingBalance.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSavingBalance.Location = new System.Drawing.Point(125, 99);
            this.lblSavingBalance.Name = "lblSavingBalance";
            this.lblSavingBalance.Size = new System.Drawing.Size(97, 15);
            this.lblSavingBalance.TabIndex = 10;
            // 
            // lblSavingAccNo
            // 
            this.lblSavingAccNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavingAccNo.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSavingAccNo.Location = new System.Drawing.Point(125, 77);
            this.lblSavingAccNo.Name = "lblSavingAccNo";
            this.lblSavingAccNo.Size = new System.Drawing.Size(97, 15);
            this.lblSavingAccNo.TabIndex = 8;
            // 
            // lblSlum
            // 
            this.lblSlum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSlum.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSlum.Location = new System.Drawing.Point(125, 55);
            this.lblSlum.Name = "lblSlum";
            this.lblSlum.Size = new System.Drawing.Size(97, 15);
            this.lblSlum.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(10, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Slum";
            // 
            // lblMemberID
            // 
            this.lblMemberID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMemberID.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemberID.Location = new System.Drawing.Point(125, 33);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(97, 15);
            this.lblMemberID.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Member ID";
            // 
            // lblname
            // 
            this.lblname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblname.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(125, 11);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(97, 15);
            this.lblname.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Name";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatuslblVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 750);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1092, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(285, 17);
            this.toolStripStatusLabel1.Text = "      Powered By : Abacus Information Technologies  (";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.IsLink = true;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(108, 17);
            this.toolStripStatusLabel2.Text = "www.ait-india.com";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(462, 17);
            this.toolStripStatusLabel3.Text = ")  |  For 3J Corp, Subsidiary of SDI  |  All rights Reserved by SDI              " +
    "                               ";
            // 
            // toolStripStatuslblVersion
            // 
            this.toolStripStatuslblVersion.Name = "toolStripStatuslblVersion";
            this.toolStripStatuslblVersion.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripStatuslblVersion.Size = new System.Drawing.Size(120, 17);
            this.toolStripStatuslblVersion.Text = "Version : {0}.{1}.{2}.{3}";
            // 
            // frmBioMetricSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 772);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.scMain);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.Name = "frmBioMetricSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Saving & Loan System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBioMetricSystem_FormClosed);
            this.Load += new System.EventHandler(this.BioMaster_Load);
            this.MdiChildActivate += new System.EventHandler(this.BioMetricSystem_MdiChildActivate);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel1.PerformLayout();
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.scSearch.Panel1.ResumeLayout(false);
            this.scSearch.Panel1.PerformLayout();
            this.scSearch.Panel2.ResumeLayout(false);
            this.scSearch.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSearch)).EndInit();
            this.scSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMembers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSavLoanDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMember)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.SplitContainer scSearch;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.TextBox txtmemberid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbioid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem Member;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dgMembers;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblEMIDue;
        private System.Windows.Forms.Label lblLoanAccNo;
        private System.Windows.Forms.Label lblSavingBalance;
        private System.Windows.Forms.Label lblSavingAccNo;
        private System.Windows.Forms.Label lblSlum;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem UpdateMember;
        private System.Windows.Forms.ToolStripMenuItem SavingsAcc;
        private System.Windows.Forms.ToolStripMenuItem CreateSavingsAccount;
        private System.Windows.Forms.ToolStripMenuItem ViewUpdateSavingsAccount;
        private System.Windows.Forms.ToolStripMenuItem DepositSavingsTrans;
        private System.Windows.Forms.ToolStripMenuItem WithdrawSavingsTrans;
        private System.Windows.Forms.ToolStripMenuItem loanAcToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator SavingsSeparator;
        private System.Windows.Forms.ToolStripMenuItem CreateLoanAcc;
        private System.Windows.Forms.ToolStripMenuItem ViewUpdateLoanAcc;
        private System.Windows.Forms.ToolStripSeparator LoanSeparator;
        private System.Windows.Forms.ToolStripMenuItem DispersalLoanTrans;
        private System.Windows.Forms.ToolStripMenuItem EMIPaymentLoanAcc;
        private System.Windows.Forms.ToolStripMenuItem UPF;
        private System.Windows.Forms.ToolStripMenuItem CreateUPFAccount;
        private System.Windows.Forms.ToolStripSeparator UPFSeparator;
        private System.Windows.Forms.ToolStripMenuItem UPFInterestPayout;
        private System.Windows.Forms.ToolStripMenuItem Admin;
        private System.Windows.Forms.ToolStripMenuItem MasterDataManagement;
        private System.Windows.Forms.ToolStripMenuItem ExitApp;
        private System.Windows.Forms.ToolStripMenuItem ViewApproveLoanAcc;
        private System.Windows.Forms.Label lblLoanStatusValue;
        private System.Windows.Forms.ToolStripMenuItem Expenses;
        private System.Windows.Forms.TextBox txtPassbook;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripMenuItem Reports;
        private System.Windows.Forms.ToolStripMenuItem savingAccountTransactionReportToolStripMenuItem;
        private System.Windows.Forms.Label lblLoanAmount;
        private System.Windows.Forms.Button btnMemberReport;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatuslblVersion;
        private System.Windows.Forms.ToolStripMenuItem unpaidEMIReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upcomingEMIReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem multipleSavingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fundersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUpdateFundersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funderTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funderAllocationToolStripMenuItem;
        private System.Windows.Forms.Label lblSavingAccStatus;
        private System.Windows.Forms.ToolStripMenuItem dayReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createMemberFamilyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchMemberFamilyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deviceManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipleSavingsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem multipleWithdrawalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipleLoanEMIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterDataSettlementWiseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memberTransactionReportUpToCurrentDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn PassbookNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlumName;
        private System.Windows.Forms.ToolStripMenuItem savingsAndLoanSumParticularDayToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgSavLoanDetails;
        private System.Windows.Forms.ToolStripMenuItem dailyAccountBalanceSheetToolStripMenuItem;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.ToolStripMenuItem groupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupTransactionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem groupInterestPayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createGroupUPFAccountToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem fundAllocationReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyCenterAccountBalanceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupUPFTransactionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scannedDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem membersReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.PictureBox pbMember;
        private System.Windows.Forms.ToolStripMenuItem savingAccountReportNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSyncToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}



