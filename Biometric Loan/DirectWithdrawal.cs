﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Timers;
using System.Windows.Threading;

namespace SPARC
{
    public partial class DirectWithdrawal : Form
    {
        private Int32 iSavDepID = 0;
        public string TransType1 = string.Empty;
        private double dblBalance = 0;
        private double dblCurrentOldBalance = 0;
        // System.Threading.Timer timer,Timeout;
        public string TransType = string.Empty;
        public DirectWithdrawal()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;


        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;

            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void ClearControls()
        {
            lblMessages.Text = string.Empty;
            if (cmbAgentName.Items.Count > 0) cmbAgentName.SelectedIndex = 0;
            //txtRemarks.Text = string.Empty;
            txtAmount.Text = string.Empty;
            dtpOpenDate.Value = DateTime.Now.Date;

            iSavDepID = 0; //Create
            btnSave.Text = Constants.SAVE;
            btnSave.Focus();
            dblCurrentOldBalance = 0;
        }
        private string DoValidations()
        {
            string ErrorMsg = string.Empty;

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;
            if ((txtAmount.Text.Trim().Length == 0) || Convert.ToDouble(txtAmount.Text) <= 0)
                ErrorMsg += Messages.AMOUNT + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblMessages.ForeColor = Color.Red;
            }
            else
            {
                //Check available balance before withdrawal
                if (TransType == Constants.DEBITTRANSABBREV)
                {
                    if (iSavDepID == 0 && (dblBalance <= 0 || Convert.ToDouble(txtAmount.Text) > dblBalance))
                    {
                        ErrorMsg = Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }
                    else if (iSavDepID > 0 && (Convert.ToDouble(txtAmount.Text) > (dblBalance + dblCurrentOldBalance)))
                    {
                        ErrorMsg = Messages.AVAILABLEBALCHECK;
                        lblMessages.ForeColor = Color.Red;
                    }

                }
            }

            return ErrorMsg;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtPassbookNo.Text == "")
            {
                MessageBox.Show("Please enter the Passbook Number");
            }
            else
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    SavingAccountTransactions objSavingsDepTrans = new SavingAccountTransactions();

                    objSavingsDepTrans.Id = iSavDepID;
                    objSavingsDepTrans.fk_MemberId = memberid;
                    objSavingsDepTrans.Tran_type = "DEB";
                    objSavingsDepTrans.fk_Accountid = accounid;
                    objSavingsDepTrans.Amount = Convert.ToDouble(txtAmount.Text);
                    objSavingsDepTrans.fk_AgentId = Convert.ToInt32(cmbAgentName.SelectedValue);
                    objSavingsDepTrans.TransactionDate = dtpOpenDate.Value;
                    objSavingsDepTrans.Remarks = "";
                    objSavingsDepTrans.Createdby = GlobalValues.User_PkId;
                    objSavingsDepTrans.Updatedby = GlobalValues.User_PkId;

                    int iResult = 0;
                    if (iSavDepID == 0)
                    {
                        if (currrentbalance >= Convert.ToDouble(txtAmount.Text))
                        {
                            iResult = objSavingsDepTrans.SaveSavingsDepositTransactions();
                        }
                        else
                        {
                            MessageBox.Show("Entered Amount exceeds the Current Balance. Please enter the correct amount");
                            iResult = 1;
                        }
                    }
                    else
                        iResult = objSavingsDepTrans.SaveSavingsDepositTransactions();

                    if (iResult > 0)
                    {
                        //Update Balance
                        if (iSavDepID == 0) //New
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance += Convert.ToDouble(txtAmount.Text);
                            }
                            else
                            {
                                dblBalance -= Convert.ToDouble(txtAmount.Text);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(txtAmount.Text);
                        }
                        else
                        {
                            if (TransType == Constants.CREDITTRANSABBREV)
                            {
                                dblBalance -= dblCurrentOldBalance;
                                dblBalance += Convert.ToDouble(txtAmount.Text);
                            }
                            else
                            {
                                dblBalance += dblCurrentOldBalance;
                                dblBalance -= Convert.ToDouble(txtAmount.Text);
                            }
                            dblCurrentOldBalance = Convert.ToDouble(txtAmount.Text);
                        }

                        frmBioMetricSystem objBioMetric = (frmBioMetricSystem)this.Parent.FindForm();
                        objBioMetric.FetchMemberDetailsByID();
                        objBioMetric.InsertMemberDetailsToGrid();

                        if (objSavingsDepTrans.Id == 0)
                        {
                           // btnSave.Text = Constants.UPDATE;
                            btnSave.Visible = false;
                            if (iResult != 1)
                            {
                                lblMessages.Text = Messages.SAVED_SUCCESS;
                                lblMessages.ForeColor = Color.Green;
                            }
                            else btnSave.Visible = true;
                            //Messages.ShowMessage(Messages.SAVED_SUCCESS, Messages.MsgType.success);

                            iSavDepID = iResult;
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            //Messages.ShowMessage(Messages.UPDATED_SUCCESS, Messages.MsgType.success);
                        }
                        //sat.LoadSavingsDepTrans();
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        Int64 memberid;

        Int64 accounid;
        Int64 currrentbalance;
        private void txtPassbookNo_Leave(object sender, EventArgs e)
        {
            string pb = txtPassbookNo.Text;
            string ac="";
            if (txtPassbookNo.Text == "")
            {
                MessageBox.Show("Please enter the Passbook Number");
            }
            else
            {
                 ac=txtAccountNo.Text = GetAccountNumber(pb);
            }
            if (txtAccountNo.Text != "")
            {
               // if (TransType == Constants.DEBITTRANSABBREV)
                    this.Text = Constants.WITHDRAWSAVINGSTRANS + Messages.HYPHENWITHSPACE + ac;
                SqlConnection conn = new SqlConnection();
               // conn.ConnectionString = "Data Source=AIT3-PC\\SQLEXPRESS;Initial Catalog=Loan;Integrated Security=True";
                conn.ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                conn.Open();
                string query = "select m.Id as fk_MemberId from Members m inner join SavingAccount s on m.Id=s.fk_MemberId where PassbookNumber=@PassbookNumber";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("@PassbookNumber", txtPassbookNo.Text));
                memberid = Convert.ToInt64(cmd.ExecuteScalar().ToString());
                conn.Close();
                conn.Open();
                string query1 = "select s.Id as fk_Accountid from Members m inner join SavingAccount s on m.Id=s.fk_MemberId where PassbookNumber=@PassbookNumb";
                SqlCommand cmd1 = new SqlCommand(query1, conn);
                cmd1.Parameters.Add(new SqlParameter("@PassbookNumb", txtPassbookNo.Text));
                accounid = Convert.ToInt64(cmd1.ExecuteScalar().ToString());
                conn.Close();
                conn.Open();
                string query2 = "select s.CurrentBalance from Members m inner join SavingAccount s on m.Id=s.fk_MemberId where PassbookNumber=@PassbookNum";
                SqlCommand cmd2 = new SqlCommand(query2, conn);
                cmd2.Parameters.Add(new SqlParameter("@PassbookNum", txtPassbookNo.Text));
                currrentbalance = Convert.ToInt64(cmd2.ExecuteScalar().ToString());
                conn.Close();
            }
            else
            {
                MessageBox.Show("Entered Passbook Number is Incorrect");
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        public string GetAccountNumber(string pb)
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            int intCounter = 0;

            sqlParam[intCounter] = new SqlParameter("@PassbookNumber", SqlDbType.NVarChar, 20);
            sqlParam[intCounter++].Value = pb;
            return Convert.ToString(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_GetAccountNoFromPassbokNo", sqlParam));
        }
        private void DirectWithdrawal_Load(object sender, EventArgs e)
        {
            try
            {
                dtpOpenDate.MaxDate = DateTime.Now;
                if (TransType == Constants.DEBITTRANSABBREV)
                    this.Text = Constants.WITHDRAWSAVINGSTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Savings_AccNumber;
                else
                    this.Text = Constants.WITHDRAWSAVINGSTRANS + Messages.HYPHENWITHSPACE + GlobalValues.Savings_AccNumber;               
                FillAgents(true);
                ClearControls();              
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar)
                        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        }
    }
