﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using Microsoft.Reporting.WinForms;
using System.Configuration;
using System.Data.SqlTypes;
using System.IO;
using SPARC;

namespace SPARC
{
    public partial class Funds_Allocation_Report : Form
    {
        public Funds_Allocation_Report()
        {
            InitializeComponent();
        }
        //SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnString"]);
        //private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        SqlConnection con = new SqlConnection(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value);
        SqlCommand cmd;

        
        private void Funds_Allocation_Report_Load(object sender, EventArgs e)
        {            
            //FunderCombo.Visible = true;
            FillFunderAllocationCombo();
            this.reportViewer1.RefreshReport();

            
        }

        private void FillFunderAllocationCombo()
        {
            FundAlloc objFundLoad = new FundAlloc();
            DataSet dsLoadFund = objFundLoad.LoadFundersForAlloc2();
            if (dsLoadFund != null && dsLoadFund.Tables.Count > 0 && dsLoadFund.Tables[0].Rows.Count > 0)
            {
                FunderCombo.DisplayMember = "Fundername";
                FunderCombo.ValueMember = "Id";
                FunderCombo.DataSource = dsLoadFund.Tables[0];
            }
        }
          
        //private int FunderID;

        //public String Fundername = "";

        // code to fetch the funder allocation screen transaction details from 'fundsallocation' table and display it in report viewer.
        private void button1_Click(object sender, EventArgs e)
        {
            //Fundername = Convert.ToString(FunderCombo.SelectedItem);
            //Fundername = FunderCombo.SelectedItem.ToString();

            //string Fundername = ((ComboBoxItem)FunderCombo.SelectedItem).Content.ToString();
            int FunderID = Convert.ToInt32(FunderCombo.SelectedValue);

            //int FunderID = Convert.ToInt16(FunderCombo.SelectedIndex);
            try
            {
                if (FunderCombo.SelectedIndex == 0)
                {
                    MessageBox.Show("Please Select The Funder Name From Funder Dropdown");
                }

                if (con.State != ConnectionState.Open)
                    con.Open();
                reportViewer1.Reset();
                DataTable dt = new DataTable("DataTable1");

                SqlCommand cmd;
                SqlDataReader dr;

               //string cmdText = @"select f.Id, f.FunderName , fa.DateOfTransaction, fa.Amount, fa.Remaining_Amount_For_Loan_Allocation,c.name, fa.Project, fa.Notes  from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id Left join CodeValue c on c.Id = fa.fk_CenterId Left join Funders F on F.Id = fd.fk_FunderId where f.Id = '" + FunderID + "'   order by fa.DateOfTransaction asc";
                 //string cmdText = @"select f.Id, f.FunderName , fa.DateOfTransaction, fa.Amount, fa.Remaining_Amount_For_Loan_Allocation,c.name, fa.Project, fa.Notes  from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id Left join CodeValue c on c.Id = fa.fk_CenterId Left join Funders F on F.Id = fd.fk_FunderId where f.FunderName = '" + Fundername + "'  order by fa.DateOfTransaction asc";

                string cmdText = @"select f.Id, f.FunderName , fa.fk_FunderTransactionId, fa.DateOfTransaction, fa.Amount, fa.Remaining_Amount_For_Loan_Allocation,c.name, fa.Project, fa.Notes  
                                 from fundsallocation fa LEFT JOIN FundDetails fd ON fa.fk_FunderTransactionId = fd.Id Left join CodeValue c on c.Id = fa.fk_CenterId 
                                 Left join Funders F on F.Id = fd.fk_FunderId where fa.fk_FunderTransactionId = " + FunderID + "  order by fa.DateOfTransaction asc;";

                cmd = new SqlCommand(cmdText, con);

                dr = cmd.ExecuteReader();
                if (dr.HasRows) dt.Load(dr);
                dr.Close();

                this.reportViewer1.LocalReport.ReportEmbeddedResource = "SPARC.Report3.rdlc";
                this.reportViewer1.LocalReport.EnableHyperlinks = true;
                this.reportViewer1.LocalReport.DataSources.Add(
                new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt));


                //this.DataTable3TableAdapter.Fill(this.dataset1.datatable3, Convert.ToInt64(txtTransactionId.text));
                this.reportViewer1.RefreshReport();

                FunderCombo.SelectedIndex = 0;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Message Box", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
         
        //public String Fundername = "";
         
        private void FunderCombo_SelectedIndexChanged(object sender, EventArgs e)
        {            
               //Fundername = FunderCombo.SelectedText;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
            
   }
}

