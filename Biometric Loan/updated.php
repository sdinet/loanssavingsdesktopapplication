<?php
if (!session_id()) {
    session_start();
    $_SESSION['location_type'] = $_GET['location_type'];
    $_SESSION['location_id'] = $_GET['location_id'];
    $_SESSION['country'] = $_GET['country'];
}
$country = $_GET['country'];
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <!-- BEGIN VENDOR CSS-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>Country Data</title>
        <link rel="apple-touch-icon" sizes="60x60" href="../../../app-assets/images/ico/apple-icon-60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../../../app-assets/images/ico/apple-icon-76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../../../app-assets/images/ico/apple-icon-120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../../../app-assets/images/ico/apple-icon-152.png">
        <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
        <link rel="shortcut icon" type="image/png" href="../../../app-assets/images/ico/favicon-32.png">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/bootstrap.css">
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="../../../app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/sliders/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/extensions/pace.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/app.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/colors.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/plugins/animate/animate.css">

        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <link rel="stylesheet" type="text/css" href="../../../app-assets/css/core/colors/palette-gradient.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
        <style>
            #savingsHover ,#loanHover, #withdrawalHover , #repaymentHover
            {
                display: hidden;
                width: 250px;
                background-color: black;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 10px 0;
                /* Position the tooltip */
                position: absolute;
                z-index: 1;
            }
            #DataVisuztn {
                display: hidden;
                width: 250px;
                background-color: #fff;
                color: black;
                text-align: center;
                border-radius: 6px;
                padding: 10px 0;
                border-style: solid;
                border-color:grey;
                position: absolute;
                z-index: 1;
            }



        </style>
        <!-- END Custom CSS-->
    </head>
    <body data-open="click" data-menu="vertical-overlay-menu" data-col="2-columns" class="vertical-layout vertical-overlay-menu 2-columns  fixed-navbar">
        <!-- navbar-fixed-top-->
        <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-dark navbar-border navbar-shadow navbar-brand-center">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                       <li class="nav-item">
                            <a href="#" class="navbar-brand nav-link">
                                <img alt="branding logo" src="../../../app-assets/images/logo/robust-logo-light.png"
                                     data-expand="../../../app-assets/images/logo/robust-logo-light.png"
                                     data-collapse="../../../app-assets/images/logo/robust-logo-small.png"
                                     class="brand-logo">
                            </a>
                        </li>
                        <li class="nav-item hidden-md-up float-xs-right">
                            <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-container content container-fluid">
                    <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                        <ul class="nav navbar-nav">
                            <!--li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li-->
                            <li class="nav-item nav-home">
                                <a  href="index.php?&location_type=1&location_id=24&country=india" class="nav-link nav-link"><i  class="ficon icon-home3"></i> Home</a>
                            </li>
                            <!--li class="nav-item">
                                <a onclick="window.location.href = 'success-stories.html'" class="nav-link"><i style="font-size: 20px;" class="icon-volume-up"></i></a>
                            </li-->
                            <li class="nav-item">
                                <a onmouseover="document.getElementById('DataVisuztn').style.display = 'block'" onmouseout="document.getElementById('DataVisuztn').style.display = 'none'"  onclick="window.location.href = 'dataAnalysis.php?&location_type=1&location_id=24&country=india'" class="nav-link "><i class="ficon icon-bar-graph-2"></i> Data Visualisation</a>
                                <div id="DataVisuztn" style="display:none">
                                    <span>Location based data visualisation for<br>better Understanding of community savings</span>
                                </div>
                            </li>           
                        </ul>
                  
                        <ul class="nav navbar-nav float-xs-right">
                            <li class="dropdown dropdown-language nav-item"><a id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true"
                                                                               aria-expanded="false" class="dropdown-toggle nav-link"><i class="flag-icon flag-icon-in"></i><span class="selected-language">India</span></a>
                                <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item"><i class="flag-icon flag-icon-in"></i>India</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-ke"></i>Kenya</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-na"></i>Namibia</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-ug"></i>Uganda</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-np"></i>Nepal</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-sl"></i>Sierra Leone</a>
                                    <a href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-gh"></i>Ghana</a>
                                    <a  href="under-maintenance.html" class="dropdown-item"><i class="flag-icon flag-icon-zw"></i>Zimbabwe</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="main-menu menu-fixed menu-dark menu-accordion">
            <!-- main menu header-->
            <div class="main-menu-header">
                <input type="text" placeholder="Search" class="menu-search form-control round" />
            </div>
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="main-menu-content">

            </div>
            <!-- /main menu content-->
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
        </div>
        <div class="app-content content container-fluid">
            <div class="content-wrapper">

                <div class="content-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card bg-gradient-x-white">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-12 border-right-black border-right-lighten-3">
                                            <div class="card-block text-xs-center">
                                                <?php

                                                function moneyConvertion() {
                                                    include ('dbconfig.php');
                                                    $sql = "select sum(amount) as total from dailytransactions where txntype = 'S'";
                                                    $result = pg_query($dbconn, $sql);

                                                    $totalSavings = pg_fetch_all($result);

                                                    $sql1 = "select sum(amount) as total from dailytransactions where txntype = 'W'";
                                                    $result1 = pg_query($dbconn, $sql1);

                                                    $totalWithdrawal = pg_fetch_all($result1);

                                                    $sql2 = "select sum(amount) as total from dailytransactions where txntype = 'L'";
                                                    $result2 = pg_query($dbconn, $sql2);

                                                    $totalLoan = pg_fetch_all($result2);

                                                    $sql3 = "select sum(amount) as total from dailytransactions where txntype = 'R'";
                                                    $result3 = pg_query($dbconn, $sql3);

                                                    $totalRepayment = pg_fetch_all($result3);
                                                    pg_close($dbconn);
                                                    return array($totalSavings[0], $totalWithdrawal[0], $totalLoan[0], $totalRepayment[0]);
                                                }
                                                ?>
                                                <script>
                                                    function addCommas(nStr)
                                                    {
                                                        nStr += '';
                                                        x = nStr.split('.');
                                                        x1 = x[0];
                                                        x2 = x.length > 1 ? '.' + x[1] : '';
                                                        var rgx = /(\d+)(\d{3})/
                                                                ;
                                                        while (rgx.test(x1)) {
                                                            x1 = x1.replace(rgx, '$1' + ',' + '$2');
                                                        }
                                                        return x1 + x2;
                                                    }
                                                    function getRateTotal(from, to) {
                                                        var script = document.createElement('script');
                                                        //script.setAttribute('src', "http://free.currencyconverterapi.com/api/v3/convert?q=INR_USD&compact=ultra?key=INR_USD");
                                                        script.setAttribute('src', "http://free.currencyconverterapi.com/api/v3/convert?q=" + from + "_" + to + "&compact=ultra?&format=json&callback=parseExchangeRate");
                                                        document.body.appendChild(script);
                                                    }

                                                    function parseExchangeRate(data) {
                                                        // var name = data.query.results.row.name;
                                                        rate = parseFloat(data.results.INR_USD.val, 10);
                                                    }
                                                    var rate = getRateTotal("INR", "USD");
                                                    window.onload = function () {
                                                        var venlocValue = <?php echo json_encode(moneyConvertion()); ?>;
                                                        for (var i = 0; i < venlocValue.length; i++) {
                                                            if (i == 0)
                                                                document.getElementById("totalSavings").innerHTML = '<span onmouseout="hoverClear(' + i + ')" onmouseover="hoverMoney(' + i + ',' + venlocValue[i]['total'] + ')"><span id="div1" style="display: none;">Text</span><i class="fa fa-dollar-sign font-large-1" aria-hidden="true" ></i>&nbsp;' + addCommas(~~parseFloat(Math.round(venlocValue[i]['total'] * rate * 100) / 100).toFixed(2)) + '</span>';
                                                            if (i == 1)
                                                                document.getElementById("totalWithdrawal").innerHTML = '<span onmouseout="hoverClear(' + i + ')" onmouseover="hoverMoney(' + i + ',' + venlocValue[i]['total'] + ')"><span id="div1" style="display: none;">Text</span><i class="fa fa-dollar-sign font-large-1" aria-hidden="true" ></i>&nbsp;' + addCommas(~~parseFloat(Math.round(venlocValue[i]['total'] * rate * 100) / 100).toFixed(2)) + '</span>';
                                                            if (i == 2)
                                                                document.getElementById("totalLoan").innerHTML = '<span onmouseout="hoverClear(' + i + ')" onmouseover="hoverMoney(' + i + ',' + venlocValue[i]['total'] + ')"><span id="div1" style="display: none;">Text</span><i class="fa fa-dollar-sign font-large-1" aria-hidden="true" ></i>&nbsp;' + addCommas(~~parseFloat(Math.round(venlocValue[i]['total'] * rate * 100) / 100).toFixed(2)) + '</span>';
                                                            if (i == 3)
                                                                document.getElementById("totalRepayment").innerHTML = '<span onmouseout="hoverClear(' + i + ')" onmouseover="hoverMoney(' + i + ',' + venlocValue[i]['total'] + ')"><span id="div1" style="display: none;">Text</span><i class="fa fa-dollar-sign font-large-1" aria-hidden="true" ></i>&nbsp;' + addCommas(~~parseFloat(Math.round(venlocValue[i]['total'] * rate * 100) / 100).toFixed(2)) + '</span>';
                                                        }
                                                    };
                                                    function hoverMoney(index, value) {
                                                        if (index == 0) {
                                                            document.getElementById("savingsHover").innerHTML = '<i class="fa fa-rupee-sign font-large-1" aria-hidden="true" ></i>&nbsp;<span class="font-large-1">' + addCommas(~~value) + '</span>';
                                                            var x = document.getElementById("savingsHover");
                                                             x.style.display = "block";
                                                        }
                                                        if (index == 1) {
                                                            document.getElementById("withdrawalHover").innerHTML = '<i class="fa fa-rupee-sign font-large-1" aria-hidden="true" ></i>&nbsp;<span class="font-large-1">' + addCommas(~~value) + '</span>';
                                                            var x = document.getElementById("withdrawalHover");
                                                            x.style.display = "block";
                                                        }
                                                        if (index == 2) {
                                                            document.getElementById("loanHover").innerHTML = '<i class="fa fa-rupee-sign font-large-1" aria-hidden="true" ></i>&nbsp;<span class="font-large-1">' + addCommas(~~value) + '</span>';
                                                            var x = document.getElementById("loanHover");
                                                            x.style.display = "block";
                                                        }
                                                        if (index == 3) {
                                                            document.getElementById("repaymentHover").innerHTML = '<i class="fa fa-rupee-sign font-large-1" aria-hidden="true" ></i>&nbsp;<span class="font-large-1">' + addCommas(~~value) + '</span>';
                                                            var x = document.getElementById("repaymentHover");
                                                            x.style.display = "block";
                                                        }
                                                    }
                                                    function hoverClear(index) {
                                                        if (index == 0) {
                                                            var x = document.getElementById("savingsHover");
                                                            x.style.display = "none";
                                                        }
                                                        if (index == 1) {
                                                            var x = document.getElementById("withdrawalHover");
                                                            x.style.display = "none";
                                                        }
                                                        if (index == 2) {
                                                            var x = document.getElementById("loanHover");
                                                            x.style.display = "none";
                                                        }
                                                        if (index == 3) {
                                                            var x = document.getElementById("repaymentHover");
                                                            x.style.display = "none";
                                                        }
                                                    }
                                                </script>
                                                <h1 class="display-4 black"><i class="icon-money font-large-1"></i>&nbsp;<span id="totalSavings" class="font-large-1"></span></h1>
                                                <div id="savingsHover" style="display:none">
                                                </div>
                                                <span class="black">Total Savings</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 border-right-black border-right-lighten-3">
                                            <div class="card-block text-xs-center">
                                                <h1 class="display-4 black"><i class="icon-share font-large-1"></i>&nbsp;<span id="totalWithdrawal" class="font-large-1"></span></h1>
                                                <div id="withdrawalHover" style="display:none">
                                                </div>
                                                <span class="black">Total Withdrawal</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 border-right-black border-right-lighten-3">
                                            <div class="card-block text-xs-center">
                                                <h1 class="display-4 black"><i class="icon-briefcase font-large-1"></i>&nbsp;<span id="totalLoan" class="font-large-1"></span></h1>
                                                <div id="loanHover" style="display:none">
                                                </div>
                                                <span class="black">Total Loan</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="card-block text-xs-center">
                                                <h1 class="display-4 black"><i class="icon-download font-large-1"></i>&nbsp;<span id="totalRepayment" class="font-large-1"></span></h1>
                                                <div id="repaymentHover" style="display:none">
                                                </div>
                                                <span class="black">Total Repayment</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--div style="text-align: center;">
                                    <button type="button" class="btn btn-info width-280 buttonAnimation" data-animation="flash" onclick="window.location.href = 'dataAnalysis.php?&location_type=1&location_id=24&country=india'">Location Based Data Visualization</button>
                                </div-->
                        </div>

                    </div>




                    <!-- Button animation start -->
                    <div class="row match-height">
                        <div class="col-md-9 col-sm-12">
                            <div class="card">
                                <div class="card-body collapse in">
                                    <div class="card-block">
                                        <div id="carousel-example-caption" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li data-target="#carousel-example-caption" data-slide-to="0" class="active"></li>
                                                <li data-target="#carousel-example-caption" data-slide-to="1"></li>
                                                <li data-target="#carousel-example-caption" data-slide-to="2"></li>
                                            </ol>
                                            <div class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <img src="../../../app-assets/images/carousel/corosal1.jpg" alt="First slide">
                                                    <div class="carousel-caption">
                                                        <h3 class="font-large-2">Home for the Homeless</h3>
                                                        <p class="font-medium-2">
                                                            Community savings was able to influence the Government to help people in the slums to build homes for them. Federation was able to work with Government and has build over 1580 homes in various settlements in Bangalore..
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../../app-assets/images/carousel/corosal2.jpg" alt="Second slide">
                                                    <div class="carousel-caption">
                                                        <h3 class="font-large-2">Hope of the Hopeless</h3>
                                                        <p class="font-medium-2">Many families of sex workers in Chikkamagalur settlements were able to see the light at the end of the tunnel by their saving activities. They have switched to the alternate income generation activities through community savings. Savings has helped the KGF Federation in perceiving quality education by availing suitable loans. 
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="../../../app-assets/images/carousel/corosal3.jpg" alt="Third slide">
                                                    <div class="carousel-caption">
                                                        <h3 class="font-large-2">Voice of the Voiceless</h3>
                                                        <p class="font-medium-2">
                                                            Saving activity has brought many women in the villages in Karnataka and TamilNadu together and helped them to fight for their social causes. Apart from savings and making their life better, these women have started getting awareness about their rights and have identified their true potential. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="left carousel-control" href="#carousel-example-caption" role="button" data-slide="prev">
                                                <span class="icon-prev" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-caption" role="button"
                                               data-slide="next">
                                                <span class="icon-next" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Towards Visibility</h4>
                                </div>
                                <div class="card-body collapse in">
                                    <div class="card-block">
                                        <div class="masonry-grid">
                                            <!-- width of .grid-sizer used for columnWidth -->
                                            <div class="grid-sizer"></div>
                                            <div class="grid-item">
                                                <div class="card">
                                                    <div class="card-img-top embed-responsive embed-responsive-item embed-responsive-16by9">
                                                        <iframe class="img-thumbnail" src="https://www.youtube.com/embed/IxYlv84sY3w?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="grid-item">
                                                <div class="card">
                                                    <div class="card-img-top embed-responsive embed-responsive-item embed-responsive-16by9">
                                                        <iframe class="img-thumbnail" src="https://www.youtube.com/embed/-VOsmFEEKmY?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>&nbsp               <!-- ////////////////////////////////////////////////////////////////////////////-->
        <footer class="footer footer-static footer-transparent">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright &copy; 2017 <a href="http://ait-india.com" target="_blank"
                                                                                                  target="_blank" class="text-bold-800 grey darken-2">Abacus Information technologies </a>, All rights
                    reserved. </span>
            </p>
        </footer>
        <!-- BEGIN VENDOR JS-->
        <script src="../../../app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
        <script src="../../../app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="../../../app-assets/vendors/js/animation/jquery.appear.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <!--script src="../../../app-assets/js/scripts/dataAnalysis/homepage.js"
        type="text/javascript"></script-->
        <script src="../../../app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="../../../app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="../../../app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="../../../app-assets/js/scripts/animation/animation.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL JS-->

    </body>
</html>