﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class DeviceManagement : Form
    {
        private Int32 iSavDepID = 0;
        public DeviceManagement()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }


        private void DeviceManagement_Load(object sender, EventArgs e)
        {
            try
            {
                FillAgents(true);
                int CodeType = 0;
                if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
                {

                    FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
                }

                if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
                {
                    CodeType = 1;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
                {
                    CodeType = 2;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
                {
                    CodeType = 3;
                }
                else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
                {
                    CodeType = 4;
                }
                 Users objUsers = new Users();
                DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
                int i;

                foreach (DataRow dr in dsvalues.Tables[0].Rows)
                {
                    int Id = Convert.ToInt32(dr["ID"]);
                    int ParentId = 0;
                    if (dr["PARENTID"] is DBNull)
                    {
                        ParentId = 0;
                    }
                    else
                    {
                        ParentId = Convert.ToInt32(dr["PARENTID"]);
                    }

                    if (dr["CodeType"].ToString() == "1")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                        cmbHub.SelectedValue = Id;
                        cmbHub.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "2")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                        cmbCountry.SelectedValue = Id;
                        cmbCountry.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "3")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                        cmbState.SelectedValue = Id;
                        cmbState.Enabled = false;
                    }
                    if (dr["CodeType"].ToString() == "4")
                    {
                        //check for the country type code and populate country with countries
                        FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                        cmbDistrict.SelectedValue = Id;
                        cmbDistrict.Enabled = false;
                    }
                }
                LoadDevices();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }

        private void FillAgents(bool bAddNewRow)
        {
            Agent objAgent = new Agent();
            objAgent.AgentID = null;
            //Edited By Pawan Start
            objAgent.centerId = Convert.ToString(GlobalValues.User_CenterId);
            //Edited By Pawan End

            DataSet dsAgents = objAgent.GetAgents();
            if (dsAgents != null && dsAgents.Tables.Count > 0 && dsAgents.Tables[0].Rows.Count > 0)
            {
                if (bAddNewRow)
                {
                    DataRow dr = dsAgents.Tables[0].NewRow();
                    dr[0] = 0;
                    dr[1] = Constants.SELECTONE;
                    dr[2] = "";

                    dsAgents.Tables[0].Rows.InsertAt(dr, 0);
                }
                cmbAgentName.DisplayMember = "AgentName";
                cmbAgentName.ValueMember = "Id";
                cmbAgentName.DataSource = dsAgents.Tables[0];
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblMessages.Text = strMessage;
                }
                else
                {
                    lblMessages.Text = string.Empty;

                    DeviceManage objDevicemanage = new DeviceManage();
                    objDevicemanage.Id = iSavDepID;                  
                    objDevicemanage.Fk_SlumId = Convert.ToInt32(cmbSlum.SelectedValue);
                    objDevicemanage.UserId = Convert.ToInt32(cmbAgentName.SelectedValue);
                    objDevicemanage.MacAddress = txtMacAddress.Text;
                    objDevicemanage.Createdby = GlobalValues.User_PkId;
                    objDevicemanage.Updatedby = GlobalValues.User_PkId;

                    DataSet dsDevicemgmt = null;
                    if (iSavDepID == 0)
                        dsDevicemgmt = objDevicemanage.SaveDevice();
                    else
                        dsDevicemgmt = objDevicemanage.UpdateDevice();

                    if (dsDevicemgmt != null && dsDevicemgmt.Tables.Count > 0 && dsDevicemgmt.Tables[0].Rows.Count > 0)
                    {
                        if (iSavDepID == 0)
                        {
                            lblMessages.Text = Messages.SAVED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                            iSavDepID = Convert.ToInt32(dsDevicemgmt.Tables[0].Rows[0]["Id"]);
                            btnSave.Text = Constants.UPDATE;
                            lblDevId.Text = dsDevicemgmt.Tables[1].Rows[0]["DeviceId"].ToString();
                        }
                        else
                        {
                            lblMessages.Text = Messages.UPDATED_SUCCESS;
                            lblMessages.ForeColor = Color.Green;
                        }                        
                        LoadDevices();
                    }
                    else
                    {
                        Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.success);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void LoadDevices()
        {
            //Loads the grid
            DeviceManage objDevice = new DeviceManage();
            DataSet dsdevice = objDevice.LoadDevices();
            if (dsdevice != null && dsdevice.Tables.Count > 0 && dsdevice.Tables[0].Rows.Count > 0)
            {                
                dgDevices.AutoGenerateColumns = false;
                dgDevices.DataSource = dsdevice.Tables[0];
            }
            else
            {
                dgDevices.DataSource = null;
            }
        }

        private string DoValidations()
        {
            string ErrorMsg = string.Empty;
            DeviceManage ObjMac = new DeviceManage();
            ObjMac.MacAddress = txtMacAddress.Text.Trim();
            ObjMac.Id = iSavDepID;
            DataSet dsMac = new DataSet();
            dsMac = ObjMac.FetchMac();
           /* allow multiple mac address. A single device can be issued for a single user with multiple slum members
             if (dsMac.Tables.Count > 0 && dsMac.Tables[0].Rows.Count > 0)
            {
                ErrorMsg = Messages.DUPLICATEMACADDRESS + Messages.COMMAWITHSPACE;
            }*/

            if (cmbAgentName.SelectedIndex == 0)
                ErrorMsg = Messages.AGENTNAME + Messages.COMMAWITHSPACE;            
            if (cmbSlum.Enabled && cmbSlum.Text == string.Empty)
            {
                ErrorMsg += Messages.MEMBERSLUM + Messages.COMMAWITHSPACE;
            }            
            if (txtMacAddress.Text.Trim() == string.Empty)
            {
                ErrorMsg += Messages.MACADDRESS + Messages.COMMAWITHSPACE;
            }

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);
                lblMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void ClearControls()
        {
            iSavDepID = 0;

            lblDevId.Text = string.Empty;
            cmbHub.SelectedIndex = 0;
            cmbAgentName.SelectedIndex = 0;           
            txtMacAddress.Text = string.Empty;
            btnSave.Enabled = true;
            btnSave.Text = "Save";
        }

        private void dgDevices_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgDevices.Rows[e.RowIndex].Cells["UserId"].Value != DBNull.Value)
                {
                    cmbAgentName.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["UserId"].Value;
                }
                else
                {
                    cmbAgentName.SelectedIndex = 0;
                }
                lblDevId.Text = dgDevices.Rows[e.RowIndex].Cells["DeviceId"].Value.ToString();
                txtMacAddress.Text = dgDevices.Rows[e.RowIndex].Cells["MacAddress"].Value.ToString();
                cmbHub.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["Hubid"].Value;
                cmbCountry.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["CountryId"].Value;
                cmbState.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["StateId"].Value;
                cmbDistrict.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["DistrictId"].Value;
                cmbSlum.SelectedValue = dgDevices.Rows[e.RowIndex].Cells["SlumId"].Value;

                btnSave.Text = Constants.UPDATE;
                iSavDepID = Convert.ToInt32(dgDevices.Rows[e.RowIndex].Cells["Id"].Value);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void dgDevices_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0)
                {
                    if (dgDevices.Columns[e.ColumnIndex].Index == 11)
                    {
                        if (MessageBox.Show(Messages.DELETE_CONFIRM, Constants.CONFIRMDELETE, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            DeleteData(Convert.ToInt32(dgDevices.CurrentRow.Cells[0].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void DeleteData(Int32 DataId)
        {
            int iResult;
            DataSet dsDeleteStatus = null;

            lblMessages.Text = string.Empty;
            DeviceManage objDevmng = new DeviceManage();
            objDevmng.Id = DataId;
            dsDeleteStatus = objDevmng.DeleteDevices(GlobalValues.User_PkId);
            if (dsDeleteStatus != null && dsDeleteStatus.Tables.Count > 0 && dsDeleteStatus.Tables[0].Rows.Count > 0)
            {
                iResult = Convert.ToInt16(dsDeleteStatus.Tables[0].Rows[0][0]);
                if (iResult == 1)
                {
                    LoadDevices();
                    ClearControls();

                    lblMessages.Text = Messages.DELETED_SUCCESS;
                    lblMessages.ForeColor = Color.Green;
                }
                else if (iResult == 0)
                {
                    lblMessages.Text = Messages.DELETE_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
                else
                {
                    lblMessages.Text = Messages.ERROR_PROCESSING;
                    lblMessages.ForeColor = Color.Red;
                }
            }
            else
            {
                lblMessages.Text = Messages.ERROR_PROCESSING;
                lblMessages.ForeColor = Color.Red;
            }
        }
    }
}

