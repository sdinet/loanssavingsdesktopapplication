﻿namespace SPARC
{
    partial class frmLoanAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSAcc = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLoanPassbook = new System.Windows.Forms.TextBox();
            this.lblLoanPassbook = new System.Windows.Forms.Label();
            this.txtDeposit = new System.Windows.Forms.TextBox();
            this.lblTotalCost = new System.Windows.Forms.Label();
            this.lblFloor = new System.Windows.Forms.Label();
            this.lblPatta = new System.Windows.Forms.Label();
            this.lblConstructionType = new System.Windows.Forms.Label();
            this.lblConstructionDone = new System.Windows.Forms.Label();
            this.lblConstructionTime = new System.Windows.Forms.Label();
            this.lblDepositGiven = new System.Windows.Forms.Label();
            this.lblWalls = new System.Windows.Forms.Label();
            this.cmbToiletFacility = new System.Windows.Forms.ComboBox();
            this.cmbHouseRoof = new System.Windows.Forms.ComboBox();
            this.cmbWalls = new System.Windows.Forms.ComboBox();
            this.cmbFloor = new System.Windows.Forms.ComboBox();
            this.cmbConstType = new System.Windows.Forms.ComboBox();
            this.cmbConstDone = new System.Windows.Forms.ComboBox();
            this.cmbWtrSrc = new System.Windows.Forms.ComboBox();
            this.txtHousePatta = new System.Windows.Forms.TextBox();
            this.txtAreaFederation = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtCostOfHouse = new System.Windows.Forms.TextBox();
            this.txtConstTime = new System.Windows.Forms.TextBox();
            this.txtHouseSize = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblFederation = new System.Windows.Forms.Label();
            this.lblWaterSrc = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblToiletFacility = new System.Windows.Forms.Label();
            this.lblHouseSize = new System.Windows.Forms.Label();
            this.lblHouseRoof = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtRelation = new System.Windows.Forms.TextBox();
            this.lblRelationName = new System.Windows.Forms.Label();
            this.chkIsUpfLoan = new System.Windows.Forms.CheckBox();
            this.lblIsUpfLoan = new System.Windows.Forms.Label();
            this.lblEMITotal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEMICharges = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTotalAmtVal = new System.Windows.Forms.Label();
            this.lblTotalAmt = new System.Windows.Forms.Label();
            this.lblChargeAmtVal = new System.Windows.Forms.Label();
            this.lblChargeAmt = new System.Windows.Forms.Label();
            this.lblEMIAmtVal = new System.Windows.Forms.Label();
            this.lblEMIAmt = new System.Windows.Forms.Label();
            this.dtpEMIEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblEMIEndDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblEMIStartDate = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.cmbInterest = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTenure = new System.Windows.Forms.TextBox();
            this.txtLoanAmt = new System.Windows.Forms.TextBox();
            this.lblLoanAmt = new System.Windows.Forms.Label();
            this.lblTenure = new System.Windows.Forms.Label();
            this.cmbEMIType = new System.Windows.Forms.ComboBox();
            this.cmbLoanName = new System.Windows.Forms.ComboBox();
            this.lblLoanName = new System.Windows.Forms.Label();
            this.lblEMIType = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.dtpAppliedDate = new System.Windows.Forms.DateTimePicker();
            this.lblAppliedDate = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblMemberValue = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblMessages = new System.Windows.Forms.Label();
            this.pnlPaidDetails = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblAmtDispersedValue = new System.Windows.Forms.Label();
            this.lblAmtDispersed = new System.Windows.Forms.Label();
            this.lblNextDueDateValue = new System.Windows.Forms.Label();
            this.lblNextDueDate = new System.Windows.Forms.Label();
            this.lblLastPaidDateVal = new System.Windows.Forms.Label();
            this.lblLastPaidDate = new System.Windows.Forms.Label();
            this.lblInterestPaidValue = new System.Windows.Forms.Label();
            this.lblInterestPaid = new System.Windows.Forms.Label();
            this.lblBalPrincipalValue = new System.Windows.Forms.Label();
            this.lblBalPrincipal = new System.Windows.Forms.Label();
            this.lblTotalPaidValue = new System.Windows.Forms.Label();
            this.lblTotalPaid = new System.Windows.Forms.Label();
            this.lblPrincipalPaidValue = new System.Windows.Forms.Label();
            this.lblPrincipalPaid = new System.Windows.Forms.Label();
            this.btnApprove = new System.Windows.Forms.Button();
            this.dtpSanctionDate = new System.Windows.Forms.DateTimePicker();
            this.lblSanctionDate = new System.Windows.Forms.Label();
            this.lblSanctionedBy = new System.Windows.Forms.Label();
            this.lblSanctionedByName = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.ofdProjectImage = new System.Windows.Forms.OpenFileDialog();
            this.pnlSanction = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.lblAccValue = new System.Windows.Forms.TextBox();
            this.pnlSAcc.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlPaidDetails.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlSanction.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSAcc
            // 
            this.pnlSAcc.Controls.Add(this.groupBox1);
            this.pnlSAcc.Location = new System.Drawing.Point(4, 5);
            this.pnlSAcc.Name = "pnlSAcc";
            this.pnlSAcc.Size = new System.Drawing.Size(857, 458);
            this.pnlSAcc.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblAccValue);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtLoanPassbook);
            this.groupBox1.Controls.Add(this.lblLoanPassbook);
            this.groupBox1.Controls.Add(this.txtDeposit);
            this.groupBox1.Controls.Add(this.lblTotalCost);
            this.groupBox1.Controls.Add(this.lblFloor);
            this.groupBox1.Controls.Add(this.lblPatta);
            this.groupBox1.Controls.Add(this.lblConstructionType);
            this.groupBox1.Controls.Add(this.lblConstructionDone);
            this.groupBox1.Controls.Add(this.lblConstructionTime);
            this.groupBox1.Controls.Add(this.lblDepositGiven);
            this.groupBox1.Controls.Add(this.lblWalls);
            this.groupBox1.Controls.Add(this.cmbToiletFacility);
            this.groupBox1.Controls.Add(this.cmbHouseRoof);
            this.groupBox1.Controls.Add(this.cmbWalls);
            this.groupBox1.Controls.Add(this.cmbFloor);
            this.groupBox1.Controls.Add(this.cmbConstType);
            this.groupBox1.Controls.Add(this.cmbConstDone);
            this.groupBox1.Controls.Add(this.cmbWtrSrc);
            this.groupBox1.Controls.Add(this.txtHousePatta);
            this.groupBox1.Controls.Add(this.txtAreaFederation);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(this.txtCostOfHouse);
            this.groupBox1.Controls.Add(this.txtConstTime);
            this.groupBox1.Controls.Add(this.txtHouseSize);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.lblFederation);
            this.groupBox1.Controls.Add(this.lblWaterSrc);
            this.groupBox1.Controls.Add(this.lblPhone);
            this.groupBox1.Controls.Add(this.lblToiletFacility);
            this.groupBox1.Controls.Add(this.lblHouseSize);
            this.groupBox1.Controls.Add(this.lblHouseRoof);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.txtRelation);
            this.groupBox1.Controls.Add(this.lblRelationName);
            this.groupBox1.Controls.Add(this.chkIsUpfLoan);
            this.groupBox1.Controls.Add(this.lblIsUpfLoan);
            this.groupBox1.Controls.Add(this.lblEMITotal);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblEMICharges);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblTotalAmtVal);
            this.groupBox1.Controls.Add(this.lblTotalAmt);
            this.groupBox1.Controls.Add(this.lblChargeAmtVal);
            this.groupBox1.Controls.Add(this.lblChargeAmt);
            this.groupBox1.Controls.Add(this.lblEMIAmtVal);
            this.groupBox1.Controls.Add(this.lblEMIAmt);
            this.groupBox1.Controls.Add(this.dtpEMIEndDate);
            this.groupBox1.Controls.Add(this.lblEMIEndDate);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.lblEMIStartDate);
            this.groupBox1.Controls.Add(this.btnCalculate);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.lblRemarks);
            this.groupBox1.Controls.Add(this.cmbInterest);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.txtTenure);
            this.groupBox1.Controls.Add(this.txtLoanAmt);
            this.groupBox1.Controls.Add(this.lblLoanAmt);
            this.groupBox1.Controls.Add(this.lblTenure);
            this.groupBox1.Controls.Add(this.cmbEMIType);
            this.groupBox1.Controls.Add(this.cmbLoanName);
            this.groupBox1.Controls.Add(this.lblLoanName);
            this.groupBox1.Controls.Add(this.lblEMIType);
            this.groupBox1.Controls.Add(this.cmbStatus);
            this.groupBox1.Controls.Add(this.dtpAppliedDate);
            this.groupBox1.Controls.Add(this.lblAppliedDate);
            this.groupBox1.Controls.Add(this.lblAccountID);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblMemberValue);
            this.groupBox1.Controls.Add(this.lblMemberID);
            this.groupBox1.Location = new System.Drawing.Point(8, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(838, 455);
            this.groupBox1.TabIndex = 199;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(65, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 20);
            this.label6.TabIndex = 238;
            this.label6.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(255, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 20);
            this.label5.TabIndex = 237;
            this.label5.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(487, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 20);
            this.label3.TabIndex = 236;
            this.label3.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(255, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 20);
            this.label1.TabIndex = 235;
            this.label1.Text = "*";
            // 
            // txtLoanPassbook
            // 
            this.txtLoanPassbook.Location = new System.Drawing.Point(276, 68);
            this.txtLoanPassbook.Name = "txtLoanPassbook";
            this.txtLoanPassbook.Size = new System.Drawing.Size(99, 20);
            this.txtLoanPassbook.TabIndex = 1;
            // 
            // lblLoanPassbook
            // 
            this.lblLoanPassbook.AutoSize = true;
            this.lblLoanPassbook.Location = new System.Drawing.Point(160, 71);
            this.lblLoanPassbook.Name = "lblLoanPassbook";
            this.lblLoanPassbook.Size = new System.Drawing.Size(101, 13);
            this.lblLoanPassbook.TabIndex = 233;
            this.lblLoanPassbook.Text = "Loan Passbook No.";
            // 
            // txtDeposit
            // 
            this.txtDeposit.Location = new System.Drawing.Point(662, 410);
            this.txtDeposit.Name = "txtDeposit";
            this.txtDeposit.Size = new System.Drawing.Size(121, 20);
            this.txtDeposit.TabIndex = 26;
            // 
            // lblTotalCost
            // 
            this.lblTotalCost.AutoSize = true;
            this.lblTotalCost.Location = new System.Drawing.Point(379, 366);
            this.lblTotalCost.Name = "lblTotalCost";
            this.lblTotalCost.Size = new System.Drawing.Size(116, 13);
            this.lblTotalCost.TabIndex = 229;
            this.lblTotalCost.Text = "Total cost of the house";
            // 
            // lblFloor
            // 
            this.lblFloor.AutoSize = true;
            this.lblFloor.Location = new System.Drawing.Point(379, 253);
            this.lblFloor.Name = "lblFloor";
            this.lblFloor.Size = new System.Drawing.Size(94, 13);
            this.lblFloor.TabIndex = 228;
            this.lblFloor.Text = "Floor of the House";
            // 
            // lblPatta
            // 
            this.lblPatta.AutoSize = true;
            this.lblPatta.Location = new System.Drawing.Point(380, 281);
            this.lblPatta.Name = "lblPatta";
            this.lblPatta.Size = new System.Drawing.Size(259, 13);
            this.lblPatta.TabIndex = 227;
            this.lblPatta.Text = "Does the House have patta or security ? What kind ?";
            // 
            // lblConstructionType
            // 
            this.lblConstructionType.AutoSize = true;
            this.lblConstructionType.Location = new System.Drawing.Point(381, 312);
            this.lblConstructionType.Name = "lblConstructionType";
            this.lblConstructionType.Size = new System.Drawing.Size(105, 13);
            this.lblConstructionType.TabIndex = 226;
            this.lblConstructionType.Text = "Type of Construction";
            // 
            // lblConstructionDone
            // 
            this.lblConstructionDone.AutoSize = true;
            this.lblConstructionDone.Location = new System.Drawing.Point(382, 339);
            this.lblConstructionDone.Name = "lblConstructionDone";
            this.lblConstructionDone.Size = new System.Drawing.Size(125, 13);
            this.lblConstructionDone.TabIndex = 225;
            this.lblConstructionDone.Text = "Construction will be done";
            // 
            // lblConstructionTime
            // 
            this.lblConstructionTime.AutoSize = true;
            this.lblConstructionTime.Location = new System.Drawing.Point(385, 390);
            this.lblConstructionTime.Name = "lblConstructionTime";
            this.lblConstructionTime.Size = new System.Drawing.Size(92, 13);
            this.lblConstructionTime.TabIndex = 224;
            this.lblConstructionTime.Text = "Construction Time";
            // 
            // lblDepositGiven
            // 
            this.lblDepositGiven.AutoSize = true;
            this.lblDepositGiven.Location = new System.Drawing.Point(385, 413);
            this.lblDepositGiven.Name = "lblDepositGiven";
            this.lblDepositGiven.Size = new System.Drawing.Size(182, 13);
            this.lblDepositGiven.TabIndex = 223;
            this.lblDepositGiven.Text = "Deposit to be given with loan request";
            // 
            // lblWalls
            // 
            this.lblWalls.AutoSize = true;
            this.lblWalls.Location = new System.Drawing.Point(379, 229);
            this.lblWalls.Name = "lblWalls";
            this.lblWalls.Size = new System.Drawing.Size(33, 13);
            this.lblWalls.TabIndex = 222;
            this.lblWalls.Text = "Walls";
            // 
            // cmbToiletFacility
            // 
            this.cmbToiletFacility.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToiletFacility.FormattingEnabled = true;
            this.cmbToiletFacility.Location = new System.Drawing.Point(196, 366);
            this.cmbToiletFacility.Name = "cmbToiletFacility";
            this.cmbToiletFacility.Size = new System.Drawing.Size(121, 21);
            this.cmbToiletFacility.TabIndex = 13;
            // 
            // cmbHouseRoof
            // 
            this.cmbHouseRoof.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHouseRoof.FormattingEnabled = true;
            this.cmbHouseRoof.Location = new System.Drawing.Point(196, 419);
            this.cmbHouseRoof.Name = "cmbHouseRoof";
            this.cmbHouseRoof.Size = new System.Drawing.Size(121, 21);
            this.cmbHouseRoof.TabIndex = 14;
            // 
            // cmbWalls
            // 
            this.cmbWalls.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWalls.FormattingEnabled = true;
            this.cmbWalls.Location = new System.Drawing.Point(662, 226);
            this.cmbWalls.Name = "cmbWalls";
            this.cmbWalls.Size = new System.Drawing.Size(121, 21);
            this.cmbWalls.TabIndex = 19;
            // 
            // cmbFloor
            // 
            this.cmbFloor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFloor.FormattingEnabled = true;
            this.cmbFloor.Location = new System.Drawing.Point(662, 250);
            this.cmbFloor.Name = "cmbFloor";
            this.cmbFloor.Size = new System.Drawing.Size(121, 21);
            this.cmbFloor.TabIndex = 20;
            // 
            // cmbConstType
            // 
            this.cmbConstType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConstType.FormattingEnabled = true;
            this.cmbConstType.Location = new System.Drawing.Point(662, 309);
            this.cmbConstType.Name = "cmbConstType";
            this.cmbConstType.Size = new System.Drawing.Size(121, 21);
            this.cmbConstType.TabIndex = 22;
            // 
            // cmbConstDone
            // 
            this.cmbConstDone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConstDone.FormattingEnabled = true;
            this.cmbConstDone.Location = new System.Drawing.Point(662, 336);
            this.cmbConstDone.Name = "cmbConstDone";
            this.cmbConstDone.Size = new System.Drawing.Size(121, 21);
            this.cmbConstDone.TabIndex = 23;
            // 
            // cmbWtrSrc
            // 
            this.cmbWtrSrc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWtrSrc.FormattingEnabled = true;
            this.cmbWtrSrc.Location = new System.Drawing.Point(196, 339);
            this.cmbWtrSrc.Name = "cmbWtrSrc";
            this.cmbWtrSrc.Size = new System.Drawing.Size(121, 21);
            this.cmbWtrSrc.TabIndex = 12;
            // 
            // txtHousePatta
            // 
            this.txtHousePatta.Location = new System.Drawing.Point(662, 278);
            this.txtHousePatta.Name = "txtHousePatta";
            this.txtHousePatta.Size = new System.Drawing.Size(121, 20);
            this.txtHousePatta.TabIndex = 21;
            // 
            // txtAreaFederation
            // 
            this.txtAreaFederation.Location = new System.Drawing.Point(196, 288);
            this.txtAreaFederation.Name = "txtAreaFederation";
            this.txtAreaFederation.Size = new System.Drawing.Size(121, 20);
            this.txtAreaFederation.TabIndex = 10;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(196, 313);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(121, 20);
            this.txtPhone.TabIndex = 11;
            // 
            // txtCostOfHouse
            // 
            this.txtCostOfHouse.Location = new System.Drawing.Point(662, 363);
            this.txtCostOfHouse.Name = "txtCostOfHouse";
            this.txtCostOfHouse.Size = new System.Drawing.Size(121, 20);
            this.txtCostOfHouse.TabIndex = 24;
            // 
            // txtConstTime
            // 
            this.txtConstTime.Location = new System.Drawing.Point(662, 387);
            this.txtConstTime.Name = "txtConstTime";
            this.txtConstTime.Size = new System.Drawing.Size(121, 20);
            this.txtConstTime.TabIndex = 25;
            // 
            // txtHouseSize
            // 
            this.txtHouseSize.Location = new System.Drawing.Point(196, 393);
            this.txtHouseSize.Name = "txtHouseSize";
            this.txtHouseSize.Size = new System.Drawing.Size(121, 20);
            this.txtHouseSize.TabIndex = 14;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(196, 264);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(121, 20);
            this.txtAddress.TabIndex = 9;
            // 
            // lblFederation
            // 
            this.lblFederation.AutoSize = true;
            this.lblFederation.Location = new System.Drawing.Point(13, 291);
            this.lblFederation.Name = "lblFederation";
            this.lblFederation.Size = new System.Drawing.Size(82, 13);
            this.lblFederation.TabIndex = 207;
            this.lblFederation.Text = "Area Federation";
            // 
            // lblWaterSrc
            // 
            this.lblWaterSrc.AutoSize = true;
            this.lblWaterSrc.Location = new System.Drawing.Point(13, 342);
            this.lblWaterSrc.Name = "lblWaterSrc";
            this.lblWaterSrc.Size = new System.Drawing.Size(73, 13);
            this.lblWaterSrc.TabIndex = 206;
            this.lblWaterSrc.Text = "Water Source";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(13, 316);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(38, 13);
            this.lblPhone.TabIndex = 205;
            this.lblPhone.Text = "Phone";
            // 
            // lblToiletFacility
            // 
            this.lblToiletFacility.AutoSize = true;
            this.lblToiletFacility.Location = new System.Drawing.Point(13, 366);
            this.lblToiletFacility.Name = "lblToiletFacility";
            this.lblToiletFacility.Size = new System.Drawing.Size(68, 13);
            this.lblToiletFacility.TabIndex = 204;
            this.lblToiletFacility.Text = "Toilet Facility";
            // 
            // lblHouseSize
            // 
            this.lblHouseSize.AutoSize = true;
            this.lblHouseSize.Location = new System.Drawing.Point(13, 394);
            this.lblHouseSize.Name = "lblHouseSize";
            this.lblHouseSize.Size = new System.Drawing.Size(91, 13);
            this.lblHouseSize.TabIndex = 203;
            this.lblHouseSize.Text = "Size of the House";
            // 
            // lblHouseRoof
            // 
            this.lblHouseRoof.AutoSize = true;
            this.lblHouseRoof.Location = new System.Drawing.Point(13, 419);
            this.lblHouseRoof.Name = "lblHouseRoof";
            this.lblHouseRoof.Size = new System.Drawing.Size(94, 13);
            this.lblHouseRoof.TabIndex = 202;
            this.lblHouseRoof.Text = "Roof of the House";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(13, 264);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 201;
            this.lblAddress.Text = "Address";
            // 
            // txtRelation
            // 
            this.txtRelation.Location = new System.Drawing.Point(196, 233);
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.Size = new System.Drawing.Size(121, 20);
            this.txtRelation.TabIndex = 8;
            // 
            // lblRelationName
            // 
            this.lblRelationName.AutoSize = true;
            this.lblRelationName.Location = new System.Drawing.Point(13, 239);
            this.lblRelationName.Name = "lblRelationName";
            this.lblRelationName.Size = new System.Drawing.Size(155, 13);
            this.lblRelationName.TabIndex = 199;
            this.lblRelationName.Text = "Name of Wife/Husband/Father";
            // 
            // chkIsUpfLoan
            // 
            this.chkIsUpfLoan.AutoSize = true;
            this.chkIsUpfLoan.Location = new System.Drawing.Point(109, 70);
            this.chkIsUpfLoan.Name = "chkIsUpfLoan";
            this.chkIsUpfLoan.Size = new System.Drawing.Size(15, 14);
            this.chkIsUpfLoan.TabIndex = 198;
            this.chkIsUpfLoan.UseVisualStyleBackColor = true;
            // 
            // lblIsUpfLoan
            // 
            this.lblIsUpfLoan.AutoSize = true;
            this.lblIsUpfLoan.Location = new System.Drawing.Point(8, 71);
            this.lblIsUpfLoan.Name = "lblIsUpfLoan";
            this.lblIsUpfLoan.Size = new System.Drawing.Size(91, 13);
            this.lblIsUpfLoan.TabIndex = 197;
            this.lblIsUpfLoan.Text = "Is this UPF Loan?";
            // 
            // lblEMITotal
            // 
            this.lblEMITotal.AutoSize = true;
            this.lblEMITotal.Location = new System.Drawing.Point(723, 53);
            this.lblEMITotal.Name = "lblEMITotal";
            this.lblEMITotal.Size = new System.Drawing.Size(60, 13);
            this.lblEMITotal.TabIndex = 196;
            this.lblEMITotal.Text = "lblEMITotal";
            this.lblEMITotal.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(624, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 195;
            this.label4.Text = "Installment Total";
            this.label4.Visible = false;
            // 
            // lblEMICharges
            // 
            this.lblEMICharges.AutoSize = true;
            this.lblEMICharges.Location = new System.Drawing.Point(503, 53);
            this.lblEMICharges.Name = "lblEMICharges";
            this.lblEMICharges.Size = new System.Drawing.Size(75, 13);
            this.lblEMICharges.TabIndex = 194;
            this.lblEMICharges.Text = "lblEMICharges";
            this.lblEMICharges.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 193;
            this.label2.Text = "Installment Charges";
            this.label2.Visible = false;
            // 
            // lblTotalAmtVal
            // 
            this.lblTotalAmtVal.AutoSize = true;
            this.lblTotalAmtVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmtVal.Location = new System.Drawing.Point(681, 140);
            this.lblTotalAmtVal.Name = "lblTotalAmtVal";
            this.lblTotalAmtVal.Size = new System.Drawing.Size(24, 13);
            this.lblTotalAmtVal.TabIndex = 192;
            this.lblTotalAmtVal.Text = "NA";
            // 
            // lblTotalAmt
            // 
            this.lblTotalAmt.AutoSize = true;
            this.lblTotalAmt.Location = new System.Drawing.Point(612, 140);
            this.lblTotalAmt.Name = "lblTotalAmt";
            this.lblTotalAmt.Size = new System.Drawing.Size(70, 13);
            this.lblTotalAmt.TabIndex = 191;
            this.lblTotalAmt.Text = "Total Amount";
            // 
            // lblChargeAmtVal
            // 
            this.lblChargeAmtVal.AutoSize = true;
            this.lblChargeAmtVal.Location = new System.Drawing.Point(511, 140);
            this.lblChargeAmtVal.Name = "lblChargeAmtVal";
            this.lblChargeAmtVal.Size = new System.Drawing.Size(22, 13);
            this.lblChargeAmtVal.TabIndex = 190;
            this.lblChargeAmtVal.Text = "NA";
            // 
            // lblChargeAmt
            // 
            this.lblChargeAmt.AutoSize = true;
            this.lblChargeAmt.Location = new System.Drawing.Point(380, 140);
            this.lblChargeAmt.Name = "lblChargeAmt";
            this.lblChargeAmt.Size = new System.Drawing.Size(133, 13);
            this.lblChargeAmt.TabIndex = 189;
            this.lblChargeAmt.Text = "Total Charges (Scheduled)";
            // 
            // lblEMIAmtVal
            // 
            this.lblEMIAmtVal.AutoSize = true;
            this.lblEMIAmtVal.Location = new System.Drawing.Point(503, 81);
            this.lblEMIAmtVal.Name = "lblEMIAmtVal";
            this.lblEMIAmtVal.Size = new System.Drawing.Size(22, 13);
            this.lblEMIAmtVal.TabIndex = 188;
            this.lblEMIAmtVal.Text = "NA";
            // 
            // lblEMIAmt
            // 
            this.lblEMIAmt.AutoSize = true;
            this.lblEMIAmt.Location = new System.Drawing.Point(383, 81);
            this.lblEMIAmt.Name = "lblEMIAmt";
            this.lblEMIAmt.Size = new System.Drawing.Size(100, 13);
            this.lblEMIAmt.TabIndex = 187;
            this.lblEMIAmt.Text = "Installment Principal";
            // 
            // dtpEMIEndDate
            // 
            this.dtpEMIEndDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpEMIEndDate.Enabled = false;
            this.dtpEMIEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEMIEndDate.Location = new System.Drawing.Point(721, 106);
            this.dtpEMIEndDate.Name = "dtpEMIEndDate";
            this.dtpEMIEndDate.Size = new System.Drawing.Size(105, 20);
            this.dtpEMIEndDate.TabIndex = 18;
            // 
            // lblEMIEndDate
            // 
            this.lblEMIEndDate.AutoSize = true;
            this.lblEMIEndDate.Location = new System.Drawing.Point(614, 109);
            this.lblEMIEndDate.Name = "lblEMIEndDate";
            this.lblEMIEndDate.Size = new System.Drawing.Size(105, 13);
            this.lblEMIEndDate.TabIndex = 185;
            this.lblEMIEndDate.Text = "Installment End Date";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(503, 103);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(105, 20);
            this.dtpStartDate.TabIndex = 17;
            // 
            // lblEMIStartDate
            // 
            this.lblEMIStartDate.AutoSize = true;
            this.lblEMIStartDate.Location = new System.Drawing.Point(383, 106);
            this.lblEMIStartDate.Name = "lblEMIStartDate";
            this.lblEMIStartDate.Size = new System.Drawing.Size(108, 13);
            this.lblEMIStartDate.TabIndex = 183;
            this.lblEMIStartDate.Text = "Installment Start Date";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(382, 167);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(166, 46);
            this.btnCalculate.TabIndex = 178;
            this.btnCalculate.Text = "Calc&ulate Installment Details";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(196, 151);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(140, 49);
            this.txtRemarks.TabIndex = 6;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(13, 167);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 176;
            this.lblRemarks.Text = "Remarks";
            // 
            // cmbInterest
            // 
            this.cmbInterest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInterest.FormattingEnabled = true;
            this.cmbInterest.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cmbInterest.Location = new System.Drawing.Point(87, 124);
            this.cmbInterest.Name = "cmbInterest";
            this.cmbInterest.Size = new System.Drawing.Size(100, 21);
            this.cmbInterest.TabIndex = 4;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(13, 132);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 174;
            this.label29.Text = "Charge %";
            // 
            // txtTenure
            // 
            this.txtTenure.Location = new System.Drawing.Point(721, 14);
            this.txtTenure.MaxLength = 10;
            this.txtTenure.Name = "txtTenure";
            this.txtTenure.ShortcutsEnabled = false;
            this.txtTenure.Size = new System.Drawing.Size(110, 20);
            this.txtTenure.TabIndex = 16;
            this.txtTenure.TextChanged += new System.EventHandler(this.txtTenure_TextChanged);
            this.txtTenure.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTenure_KeyPress_1);
            // 
            // txtLoanAmt
            // 
            this.txtLoanAmt.Location = new System.Drawing.Point(269, 99);
            this.txtLoanAmt.MaxLength = 10;
            this.txtLoanAmt.Name = "txtLoanAmt";
            this.txtLoanAmt.ShortcutsEnabled = false;
            this.txtLoanAmt.Size = new System.Drawing.Size(108, 20);
            this.txtLoanAmt.TabIndex = 3;
            this.txtLoanAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLoanAmt_KeyPress_1);
            // 
            // lblLoanAmt
            // 
            this.lblLoanAmt.AutoSize = true;
            this.lblLoanAmt.Location = new System.Drawing.Point(191, 97);
            this.lblLoanAmt.Name = "lblLoanAmt";
            this.lblLoanAmt.Size = new System.Drawing.Size(70, 13);
            this.lblLoanAmt.TabIndex = 173;
            this.lblLoanAmt.Text = "Loan Amount";
            // 
            // lblTenure
            // 
            this.lblTenure.AutoSize = true;
            this.lblTenure.Location = new System.Drawing.Point(624, 20);
            this.lblTenure.Name = "lblTenure";
            this.lblTenure.Size = new System.Drawing.Size(92, 13);
            this.lblTenure.TabIndex = 172;
            this.lblTenure.Text = "Tenure (Months) *";
            // 
            // cmbEMIType
            // 
            this.cmbEMIType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEMIType.FormattingEnabled = true;
            this.cmbEMIType.Location = new System.Drawing.Point(506, 15);
            this.cmbEMIType.Name = "cmbEMIType";
            this.cmbEMIType.Size = new System.Drawing.Size(110, 21);
            this.cmbEMIType.TabIndex = 15;
            this.cmbEMIType.SelectedIndexChanged += new System.EventHandler(this.cmbEMIType_SelectedIndexChanged);
            // 
            // cmbLoanName
            // 
            this.cmbLoanName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoanName.FormattingEnabled = true;
            this.cmbLoanName.Location = new System.Drawing.Point(87, 91);
            this.cmbLoanName.Name = "cmbLoanName";
            this.cmbLoanName.Size = new System.Drawing.Size(100, 21);
            this.cmbLoanName.TabIndex = 2;
            this.cmbLoanName.SelectedIndexChanged += new System.EventHandler(this.cmbLoanName_SelectedIndexChanged);
            // 
            // lblLoanName
            // 
            this.lblLoanName.AutoSize = true;
            this.lblLoanName.Location = new System.Drawing.Point(8, 99);
            this.lblLoanName.Name = "lblLoanName";
            this.lblLoanName.Size = new System.Drawing.Size(58, 13);
            this.lblLoanName.TabIndex = 169;
            this.lblLoanName.Text = "Loan Type";
            // 
            // lblEMIType
            // 
            this.lblEMIType.AutoSize = true;
            this.lblEMIType.Location = new System.Drawing.Point(383, 21);
            this.lblEMIType.Name = "lblEMIType";
            this.lblEMIType.Size = new System.Drawing.Size(107, 13);
            this.lblEMIType.TabIndex = 168;
            this.lblEMIType.Text = "Installment Pay Cycle";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Initiated",
            "Approved",
            "Write-off",
            "Closed"});
            this.cmbStatus.Location = new System.Drawing.Point(196, 206);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(121, 21);
            this.cmbStatus.TabIndex = 7;
            // 
            // dtpAppliedDate
            // 
            this.dtpAppliedDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpAppliedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAppliedDate.Location = new System.Drawing.Point(269, 125);
            this.dtpAppliedDate.Name = "dtpAppliedDate";
            this.dtpAppliedDate.Size = new System.Drawing.Size(110, 20);
            this.dtpAppliedDate.TabIndex = 5;
            this.dtpAppliedDate.ValueChanged += new System.EventHandler(this.dtpAppliedDate_ValueChanged);
            // 
            // lblAppliedDate
            // 
            this.lblAppliedDate.AutoSize = true;
            this.lblAppliedDate.Location = new System.Drawing.Point(193, 125);
            this.lblAppliedDate.Name = "lblAppliedDate";
            this.lblAppliedDate.Size = new System.Drawing.Size(68, 13);
            this.lblAppliedDate.TabIndex = 80;
            this.lblAppliedDate.Text = "Applied Date";
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(8, 43);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 73;
            this.lblAccountID.Text = "Account ID";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(13, 206);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Status";
            // 
            // lblMemberValue
            // 
            this.lblMemberValue.AutoSize = true;
            this.lblMemberValue.Location = new System.Drawing.Point(106, 17);
            this.lblMemberValue.Name = "lblMemberValue";
            this.lblMemberValue.Size = new System.Drawing.Size(35, 13);
            this.lblMemberValue.TabIndex = 1;
            this.lblMemberValue.Text = "label1";
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(8, 15);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(59, 13);
            this.lblMemberID.TabIndex = 0;
            this.lblMemberID.Text = "Member ID";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(550, 663);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(392, 663);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 28;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(1, 639);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(55, 13);
            this.lblMessages.TabIndex = 87;
            this.lblMessages.Text = "Messages";
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlPaidDetails
            // 
            this.pnlPaidDetails.Controls.Add(this.groupBox2);
            this.pnlPaidDetails.Location = new System.Drawing.Point(4, 469);
            this.pnlPaidDetails.Name = "pnlPaidDetails";
            this.pnlPaidDetails.Size = new System.Drawing.Size(857, 100);
            this.pnlPaidDetails.TabIndex = 89;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblAmtDispersedValue);
            this.groupBox2.Controls.Add(this.lblAmtDispersed);
            this.groupBox2.Controls.Add(this.lblNextDueDateValue);
            this.groupBox2.Controls.Add(this.lblNextDueDate);
            this.groupBox2.Controls.Add(this.lblLastPaidDateVal);
            this.groupBox2.Controls.Add(this.lblLastPaidDate);
            this.groupBox2.Controls.Add(this.lblInterestPaidValue);
            this.groupBox2.Controls.Add(this.lblInterestPaid);
            this.groupBox2.Controls.Add(this.lblBalPrincipalValue);
            this.groupBox2.Controls.Add(this.lblBalPrincipal);
            this.groupBox2.Controls.Add(this.lblTotalPaidValue);
            this.groupBox2.Controls.Add(this.lblTotalPaid);
            this.groupBox2.Controls.Add(this.lblPrincipalPaidValue);
            this.groupBox2.Controls.Add(this.lblPrincipalPaid);
            this.groupBox2.Location = new System.Drawing.Point(22, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(814, 90);
            this.groupBox2.TabIndex = 199;
            this.groupBox2.TabStop = false;
            // 
            // lblAmtDispersedValue
            // 
            this.lblAmtDispersedValue.AutoSize = true;
            this.lblAmtDispersedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmtDispersedValue.Location = new System.Drawing.Point(159, 16);
            this.lblAmtDispersedValue.Name = "lblAmtDispersedValue";
            this.lblAmtDispersedValue.Size = new System.Drawing.Size(14, 13);
            this.lblAmtDispersedValue.TabIndex = 198;
            this.lblAmtDispersedValue.Text = "0";
            // 
            // lblAmtDispersed
            // 
            this.lblAmtDispersed.AutoSize = true;
            this.lblAmtDispersed.Location = new System.Drawing.Point(45, 16);
            this.lblAmtDispersed.Name = "lblAmtDispersed";
            this.lblAmtDispersed.Size = new System.Drawing.Size(93, 13);
            this.lblAmtDispersed.TabIndex = 197;
            this.lblAmtDispersed.Text = "Amount Dispersed";
            // 
            // lblNextDueDateValue
            // 
            this.lblNextDueDateValue.AutoSize = true;
            this.lblNextDueDateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNextDueDateValue.Location = new System.Drawing.Point(629, 41);
            this.lblNextDueDateValue.Name = "lblNextDueDateValue";
            this.lblNextDueDateValue.Size = new System.Drawing.Size(0, 13);
            this.lblNextDueDateValue.TabIndex = 196;
            // 
            // lblNextDueDate
            // 
            this.lblNextDueDate.AutoSize = true;
            this.lblNextDueDate.Location = new System.Drawing.Point(515, 42);
            this.lblNextDueDate.Name = "lblNextDueDate";
            this.lblNextDueDate.Size = new System.Drawing.Size(78, 13);
            this.lblNextDueDate.TabIndex = 195;
            this.lblNextDueDate.Text = "Next Due Date";
            // 
            // lblLastPaidDateVal
            // 
            this.lblLastPaidDateVal.AutoSize = true;
            this.lblLastPaidDateVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastPaidDateVal.Location = new System.Drawing.Point(622, 67);
            this.lblLastPaidDateVal.Name = "lblLastPaidDateVal";
            this.lblLastPaidDateVal.Size = new System.Drawing.Size(24, 13);
            this.lblLastPaidDateVal.TabIndex = 194;
            this.lblLastPaidDateVal.Text = "NA";
            // 
            // lblLastPaidDate
            // 
            this.lblLastPaidDate.AutoSize = true;
            this.lblLastPaidDate.Location = new System.Drawing.Point(515, 67);
            this.lblLastPaidDate.Name = "lblLastPaidDate";
            this.lblLastPaidDate.Size = new System.Drawing.Size(77, 13);
            this.lblLastPaidDate.TabIndex = 193;
            this.lblLastPaidDate.Text = "Last Paid Date";
            // 
            // lblInterestPaidValue
            // 
            this.lblInterestPaidValue.AutoSize = true;
            this.lblInterestPaidValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterestPaidValue.Location = new System.Drawing.Point(373, 42);
            this.lblInterestPaidValue.Name = "lblInterestPaidValue";
            this.lblInterestPaidValue.Size = new System.Drawing.Size(14, 13);
            this.lblInterestPaidValue.TabIndex = 192;
            this.lblInterestPaidValue.Text = "0";
            // 
            // lblInterestPaid
            // 
            this.lblInterestPaid.AutoSize = true;
            this.lblInterestPaid.Location = new System.Drawing.Point(277, 42);
            this.lblInterestPaid.Name = "lblInterestPaid";
            this.lblInterestPaid.Size = new System.Drawing.Size(70, 13);
            this.lblInterestPaid.TabIndex = 191;
            this.lblInterestPaid.Text = "Charges Paid";
            // 
            // lblBalPrincipalValue
            // 
            this.lblBalPrincipalValue.AutoSize = true;
            this.lblBalPrincipalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalPrincipalValue.Location = new System.Drawing.Point(629, 17);
            this.lblBalPrincipalValue.Name = "lblBalPrincipalValue";
            this.lblBalPrincipalValue.Size = new System.Drawing.Size(0, 13);
            this.lblBalPrincipalValue.TabIndex = 190;
            // 
            // lblBalPrincipal
            // 
            this.lblBalPrincipal.AutoSize = true;
            this.lblBalPrincipal.Location = new System.Drawing.Point(515, 16);
            this.lblBalPrincipal.Name = "lblBalPrincipal";
            this.lblBalPrincipal.Size = new System.Drawing.Size(89, 13);
            this.lblBalPrincipal.TabIndex = 189;
            this.lblBalPrincipal.Text = "Balance Principal";
            // 
            // lblTotalPaidValue
            // 
            this.lblTotalPaidValue.AutoSize = true;
            this.lblTotalPaidValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaidValue.Location = new System.Drawing.Point(373, 67);
            this.lblTotalPaidValue.Name = "lblTotalPaidValue";
            this.lblTotalPaidValue.Size = new System.Drawing.Size(14, 13);
            this.lblTotalPaidValue.TabIndex = 188;
            this.lblTotalPaidValue.Text = "0";
            // 
            // lblTotalPaid
            // 
            this.lblTotalPaid.AutoSize = true;
            this.lblTotalPaid.Location = new System.Drawing.Point(277, 67);
            this.lblTotalPaid.Name = "lblTotalPaid";
            this.lblTotalPaid.Size = new System.Drawing.Size(55, 13);
            this.lblTotalPaid.TabIndex = 187;
            this.lblTotalPaid.Text = "Total Paid";
            // 
            // lblPrincipalPaidValue
            // 
            this.lblPrincipalPaidValue.AutoSize = true;
            this.lblPrincipalPaidValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrincipalPaidValue.Location = new System.Drawing.Point(373, 17);
            this.lblPrincipalPaidValue.Name = "lblPrincipalPaidValue";
            this.lblPrincipalPaidValue.Size = new System.Drawing.Size(14, 13);
            this.lblPrincipalPaidValue.TabIndex = 186;
            this.lblPrincipalPaidValue.Text = "0";
            // 
            // lblPrincipalPaid
            // 
            this.lblPrincipalPaid.AutoSize = true;
            this.lblPrincipalPaid.Location = new System.Drawing.Point(277, 17);
            this.lblPrincipalPaid.Name = "lblPrincipalPaid";
            this.lblPrincipalPaid.Size = new System.Drawing.Size(71, 13);
            this.lblPrincipalPaid.TabIndex = 185;
            this.lblPrincipalPaid.Text = "Principal Paid";
            // 
            // btnApprove
            // 
            this.btnApprove.Location = new System.Drawing.Point(248, 663);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Size = new System.Drawing.Size(139, 23);
            this.btnApprove.TabIndex = 27;
            this.btnApprove.Text = "Update && &Approve";
            this.btnApprove.UseVisualStyleBackColor = true;
            this.btnApprove.Visible = false;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // dtpSanctionDate
            // 
            this.dtpSanctionDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpSanctionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSanctionDate.Location = new System.Drawing.Point(655, 10);
            this.dtpSanctionDate.Name = "dtpSanctionDate";
            this.dtpSanctionDate.Size = new System.Drawing.Size(105, 20);
            this.dtpSanctionDate.TabIndex = 153;
            this.dtpSanctionDate.Value = new System.DateTime(1900, 1, 1, 12, 0, 0, 0);
            // 
            // lblSanctionDate
            // 
            this.lblSanctionDate.AutoSize = true;
            this.lblSanctionDate.Location = new System.Drawing.Point(541, 13);
            this.lblSanctionDate.Name = "lblSanctionDate";
            this.lblSanctionDate.Size = new System.Drawing.Size(87, 13);
            this.lblSanctionDate.TabIndex = 152;
            this.lblSanctionDate.Text = "Sanctioned Date";
            // 
            // lblSanctionedBy
            // 
            this.lblSanctionedBy.AutoSize = true;
            this.lblSanctionedBy.Location = new System.Drawing.Point(68, 13);
            this.lblSanctionedBy.Name = "lblSanctionedBy";
            this.lblSanctionedBy.Size = new System.Drawing.Size(76, 13);
            this.lblSanctionedBy.TabIndex = 150;
            this.lblSanctionedBy.Text = "Sanctioned By";
            // 
            // lblSanctionedByName
            // 
            this.lblSanctionedByName.AutoSize = true;
            this.lblSanctionedByName.Location = new System.Drawing.Point(159, 13);
            this.lblSanctionedByName.Name = "lblSanctionedByName";
            this.lblSanctionedByName.Size = new System.Drawing.Size(0, 13);
            this.lblSanctionedByName.TabIndex = 151;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(471, 663);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 29;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pnlSanction
            // 
            this.pnlSanction.Controls.Add(this.dtpSanctionDate);
            this.pnlSanction.Controls.Add(this.lblSanctionDate);
            this.pnlSanction.Controls.Add(this.lblSanctionedBy);
            this.pnlSanction.Controls.Add(this.lblSanctionedByName);
            this.pnlSanction.Location = new System.Drawing.Point(4, 579);
            this.pnlSanction.Name = "pnlSanction";
            this.pnlSanction.Size = new System.Drawing.Size(854, 43);
            this.pnlSanction.TabIndex = 92;
            this.pnlSanction.Visible = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(631, 663);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 31;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblAccValue
            // 
            this.lblAccValue.Location = new System.Drawing.Point(109, 40);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(100, 20);
            this.lblAccValue.TabIndex = 239;
            // 
            // frmLoanAccount
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(873, 700);
            this.ControlBox = false;
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pnlSanction);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnApprove);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pnlPaidDetails);
            this.Controls.Add(this.pnlSAcc);
            this.Controls.Add(this.lblMessages);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLoanAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmLoanAccount_Load);
            this.pnlSAcc.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlPaidDetails.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlSanction.ResumeLayout(false);
            this.pnlSanction.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlSAcc;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpAppliedDate;
        private System.Windows.Forms.Label lblAppliedDate;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblMemberValue;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Panel pnlPaidDetails;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Button btnApprove;
        private System.Windows.Forms.Label lblTotalAmtVal;
        private System.Windows.Forms.Label lblTotalAmt;
        private System.Windows.Forms.Label lblChargeAmtVal;
        private System.Windows.Forms.Label lblChargeAmt;
        private System.Windows.Forms.Label lblEMIAmtVal;
        private System.Windows.Forms.Label lblEMIAmt;
        private System.Windows.Forms.DateTimePicker dtpEMIEndDate;
        private System.Windows.Forms.Label lblEMIEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label lblEMIStartDate;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.ComboBox cmbInterest;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtTenure;
        private System.Windows.Forms.TextBox txtLoanAmt;
        private System.Windows.Forms.Label lblLoanAmt;
        private System.Windows.Forms.Label lblTenure;
        private System.Windows.Forms.ComboBox cmbEMIType;
        private System.Windows.Forms.ComboBox cmbLoanName;
        private System.Windows.Forms.Label lblLoanName;
        private System.Windows.Forms.Label lblEMIType;
        private System.Windows.Forms.Label lblInterestPaidValue;
        private System.Windows.Forms.Label lblInterestPaid;
        private System.Windows.Forms.Label lblBalPrincipalValue;
        private System.Windows.Forms.Label lblBalPrincipal;
        private System.Windows.Forms.Label lblTotalPaidValue;
        private System.Windows.Forms.Label lblTotalPaid;
        private System.Windows.Forms.Label lblPrincipalPaidValue;
        private System.Windows.Forms.Label lblPrincipalPaid;
        private System.Windows.Forms.DateTimePicker dtpSanctionDate;
        private System.Windows.Forms.Label lblSanctionDate;
        private System.Windows.Forms.Label lblSanctionedBy;
        private System.Windows.Forms.Label lblSanctionedByName;
        private System.Windows.Forms.Label lblEMICharges;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEMITotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblAmtDispersedValue;
        private System.Windows.Forms.Label lblAmtDispersed;
        private System.Windows.Forms.Label lblNextDueDateValue;
        private System.Windows.Forms.Label lblNextDueDate;
        private System.Windows.Forms.Label lblLastPaidDateVal;
        private System.Windows.Forms.Label lblLastPaidDate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox chkIsUpfLoan;
        private System.Windows.Forms.Label lblIsUpfLoan;
        private System.Windows.Forms.OpenFileDialog ofdProjectImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel pnlSanction;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label lblRelationName;
        private System.Windows.Forms.TextBox txtRelation;
        private System.Windows.Forms.Label lblFederation;
        private System.Windows.Forms.Label lblWaterSrc;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblToiletFacility;
        private System.Windows.Forms.Label lblHouseSize;
        private System.Windows.Forms.Label lblHouseRoof;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAreaFederation;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtCostOfHouse;
        private System.Windows.Forms.TextBox txtConstTime;
        private System.Windows.Forms.TextBox txtHouseSize;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtHousePatta;
        private System.Windows.Forms.ComboBox cmbToiletFacility;
        private System.Windows.Forms.ComboBox cmbHouseRoof;
        private System.Windows.Forms.ComboBox cmbWalls;
        private System.Windows.Forms.ComboBox cmbFloor;
        private System.Windows.Forms.ComboBox cmbConstType;
        private System.Windows.Forms.ComboBox cmbConstDone;
        private System.Windows.Forms.ComboBox cmbWtrSrc;
        private System.Windows.Forms.Label lblTotalCost;
        private System.Windows.Forms.Label lblFloor;
        private System.Windows.Forms.Label lblPatta;
        private System.Windows.Forms.Label lblConstructionType;
        private System.Windows.Forms.Label lblConstructionDone;
        private System.Windows.Forms.Label lblConstructionTime;
        private System.Windows.Forms.Label lblDepositGiven;
        private System.Windows.Forms.Label lblWalls;
        private System.Windows.Forms.TextBox txtDeposit;
        private System.Windows.Forms.TextBox txtLoanPassbook;
        private System.Windows.Forms.Label lblLoanPassbook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox lblAccValue;
    }
}