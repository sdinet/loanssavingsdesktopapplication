﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPARC;
using System.Data.SqlClient;

namespace SPARC
{
    public partial class frmBioMetricSystem : Form
    {
        private int childFormNumber = 0;
        private bool isMemberSearched = false;
        public frmBioMetricSystem()
        {
            InitializeComponent();
        }
        private void BioMaster_Load(object sender, EventArgs e)
        {
            FillCombos(Convert.ToInt32(LocationCode.Country), 0, cmbcountry);
        }

        #region Populate Location 
        private void cmbcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbcountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbcountry.SelectedValue), cmbstate);
            else
                cmbstate.DataSource = null;
        }
        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbstate.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbstate.SelectedValue), cmbdistrict);
            else
                cmbdistrict.DataSource = null;
        }
        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Taluk
            if (cmbdistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Taluk), Convert.ToInt32(cmbdistrict.SelectedValue), cmbtaluk);
            else
                cmbtaluk.DataSource = null;
        }
        private void cmbtaluk_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbtaluk.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbtaluk.SelectedValue), cmbslum);
            else
                cmbslum.DataSource = null;
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;
            
            DataSet dsCodeValues = objCodeValues.GetCodeValues();
            if (dsCodeValues!=null&& dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        #endregion
        
        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }
        private void agentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Master_Pages.frmAgentManager objAgent = new Master_Pages.frmAgentManager();
            objAgent.TopLevel = false;
            objAgent.Parent = scMain.Panel1;
            objAgent.WindowState = FormWindowState.Maximized;
            objAgent.Show();
        }

        private bool OpenFormsFound()
        {
            bool bOpenForms = false;
            if (this.scMain.Panel1.Controls.Count > 0)
            {
                bOpenForms = true;
            }
            return bOpenForms;
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            isMemberSearched = true;
            Members objMember = new Members();
            objMember.MemberId = txtmemberid.Text.Trim();
            objMember.Name = txtname.Text.Trim();
            objMember.BioMetricId = txtbioid.Text.Trim();
            objMember.fk_CountryId = cmbcountry.SelectedIndex > 0 ? Convert.ToInt16(cmbcountry.SelectedValue.ToString()) : (int?) null;
            objMember.fk_StateId = cmbstate.SelectedIndex > 0 ? Convert.ToInt32(cmbstate.SelectedValue.ToString()) : (int?) null;
            objMember.fk_DistricId = cmbdistrict.SelectedIndex > 0 ? Convert.ToInt32(cmbdistrict.SelectedValue.ToString()) : (int?) null;
            objMember.fk_TalukId = cmbtaluk.SelectedIndex >0 ? Convert.ToInt32(cmbtaluk.SelectedValue.ToString()) : (int?) null;
            objMember.fk_SlumId = cmbslum.SelectedIndex > 0 ? Convert.ToInt32(cmbslum.SelectedValue.ToString()) : (int?) null;

            DataSet dsMembers = objMember.SearchMember();
            if (dsMembers.Tables[0].Rows.Count > 0)
            {
                dgMembers.AutoGenerateColumns = false;
                dgMembers.DataSource = dsMembers.Tables[0];
            }
            else
            {
                dgMembers.DataSource = null;
                //dgMembers.Rows.Add(
                //dgMembers[1, 1].Value = "No Members Found";

                //using (Graphics grfx = e.Graphics)
                //{
                //    // create a white rectangle so text will be easily readable
                //    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(), new Size(dgMembers.Width, 25)));
                //    // write text on top of the white rectangle just created
                //    grfx.DrawString("No Members found", new Font("Arial", 12), Brushes.Black, new PointF(3, 3));
                //}
            }
        }

        private void dgMembers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEWINDOWS, Messages.MsgType.warning);
            else
            {
                if (e.RowIndex >= 0)
                {
                    GlobalValues.Member_PkId = Convert.ToInt64(dgMembers.Rows[e.RowIndex].Cells[0].Value.ToString());
                    GlobalValues.Member_ID = dgMembers.Rows[e.RowIndex].Cells[1].Value.ToString();
                    GlobalValues.Member_Name = dgMembers.Rows[e.RowIndex].Cells[2].Value.ToString().ToUpper();
                    

                    FetchMemberDetailsByID();
                }
                else
                {
                    Messages.ShowMessage("Please select a Member by double clicking inside the row", Messages.MsgType.Info);
                }
            }
        }

        /// <summary>
        /// Displays Quick Info of Member
        /// </summary>
        public void FetchMemberDetailsByID()
        {
            Members objMembers = new Members();
            objMembers.Id = GlobalValues.Member_PkId;
            DataSet dsMemberQuickInfo = objMembers.FetchQuickInfoByID();
            if (dsMemberQuickInfo.Tables[0].Rows.Count > 0)
            {
                scSearch.Panel2Collapsed = false;
                lblname.Text = dsMemberQuickInfo.Tables[0].Rows[0]["Name"].ToString();
                lblMemberID.Text = dsMemberQuickInfo.Tables[0].Rows[0]["MemberId"].ToString();
                lblSlum.Text = dsMemberQuickInfo.Tables[0].Rows[0]["Slum"].ToString();
                lblSavingAccNo.Text = dsMemberQuickInfo.Tables[0].Rows[0]["SavingAccNo"].ToString();
                lblSavingBalance.Text = dsMemberQuickInfo.Tables[0].Rows[0]["SavingBalance"].ToString();
                lblLoanAccNo.Text = dsMemberQuickInfo.Tables[0].Rows[0]["LoanAccNo"].ToString();
                lblEMIDue.Text = dsMemberQuickInfo.Tables[0].Rows[0]["EMIDue"].ToString();

                this.Text = Constants.MASTERFORMHEADING + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                if (dsMemberQuickInfo.Tables[0].Rows[0]["SavingsAccId"] == DBNull.Value)
                    GlobalValues.Savings_PkId = 0;
                else
                {
                    GlobalValues.Savings_PkId = Convert.ToInt64(dsMemberQuickInfo.Tables[0].Rows[0]["SavingsAccId"]);
                    GlobalValues.Savings_AccNumber = lblSavingAccNo.Text;
                }
            }
        }

        private void UpdateMember_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else
            {
                frmCreateMember frmCreateMember = new frmCreateMember();
                frmCreateMember.TopLevel = false;
                frmCreateMember.MdiParent = this;
                frmCreateMember.Parent = scMain.Panel1;
                frmCreateMember.WindowState = FormWindowState.Maximized;
                frmCreateMember.intMemberOperationType = 2;
                frmCreateMember.Show();
            }
        }

        private void CreateMember_Click(object sender, EventArgs e)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else
            {
                frmCreateMember frmCreateMember = new frmCreateMember();
                frmCreateMember.TopLevel = false;
                frmCreateMember.MdiParent = this;
                frmCreateMember.Parent = scMain.Panel1;
                frmCreateMember.WindowState = FormWindowState.Maximized;
                frmCreateMember.intMemberOperationType = 1;
                frmCreateMember.Show();
            }
        }

        //private void dgMembers_Paint(object sender, PaintEventArgs e)
        //{
        //    DataGridView sndr = (DataGridView)sender;
        //    if (!isMemberSearched)
        //        return;
        //    if (sndr.Rows.Count == 0) // <-- if there are no rows in the DataGridView when it paints, then it will create your message
        //    {
        //        using (Graphics grfx = e.Graphics)
        //        {
        //            // create a white rectangle so text will be easily readable
        //            grfx.FillRectangle(Brushes.White, new Rectangle(new Point(), new Size(sndr.Width, 25)));
        //            // write text on top of the white rectangle just created
        //            grfx.DrawString("No Members found", new Font("Arial", 12), Brushes.Black, new PointF(3, 3));
        //        }
        //    }
        //}

        private void ExitApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BioMetricSystem_MdiChildActivate(object sender, EventArgs e)
        {

        }

        private void dgMembers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CreateSavingsAccount_Click(object sender, EventArgs e)
        {
            OpenSavingsAccForm(1);
        }

        private void ViewUpdateSavingsAccount_Click(object sender, EventArgs e)
        {
            OpenSavingsAccForm(2);               
        }

        private void OpenSavingsAccForm(int iAction)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else if ( iAction == 1 && GlobalValues.Savings_PkId > 0)
            {
                Messages.ShowMessage(Messages.SBACCALREADYEXIST, Messages.MsgType.Info);
            }
            else if ((iAction == 2) && (GlobalValues.Savings_PkId <= 0))
            {
                Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            }
            else
            {
                frmSavingsAccount frmSavingsAcc = new frmSavingsAccount();
                frmSavingsAcc.TopLevel = false;
                frmSavingsAcc.MdiParent = this;
                frmSavingsAcc.Parent = scMain.Panel1;
                frmSavingsAcc.WindowState = FormWindowState.Maximized;
                frmSavingsAcc.Show();
            }
        }

        private void CreateLoanAcc_Click(object sender, EventArgs e)
        {
            frmLoanAccount frmLoanAcc = new frmLoanAccount();
            frmLoanAcc.TopLevel = false;
            frmLoanAcc.MdiParent = this;
            frmLoanAcc.Parent = scMain.Panel1;
            frmLoanAcc.WindowState = FormWindowState.Maximized;
            frmLoanAcc.Show();
        }

        private void ViewUpdateLoanAcc_Click(object sender, EventArgs e)
        {

        }

        private void DepositSavingsTrans_Click(object sender, EventArgs e)
        {
            SavingsTransLoad("CRE");
        }

        private void WithdrawSavingsTrans_Click(object sender, EventArgs e)
        {
            SavingsTransLoad("DEB");
        }

        private void SavingsTransLoad(string TransType)
        {
            if (OpenFormsFound())
                Messages.ShowMessage(Messages.CLOSEOPENFORM, Messages.MsgType.warning);
            else if (GlobalValues.Member_PkId == 0)
            {
                Messages.ShowMessage(Messages.SELECTMEMBER, Messages.MsgType.warning);
            }
            else if (GlobalValues.Savings_PkId <= 0)
            {
                Messages.ShowMessage(Messages.SBACCNOTEXIST, Messages.MsgType.Info);
            }
            else
            {
                frmSavingsAccDepositTrans DepositTrans = new frmSavingsAccDepositTrans();
                DepositTrans.TopLevel = false;
                DepositTrans.MdiParent = this;
                DepositTrans.Parent = scMain.Panel1;
                DepositTrans.WindowState = FormWindowState.Maximized;
                DepositTrans.TransType = TransType;
                DepositTrans.Show();
            }
        }

    }
}
