﻿namespace SPARC
{
    partial class DailyAccountsEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAccDate = new System.Windows.Forms.Label();
            this.dtpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.grpSavRep = new System.Windows.Forms.GroupBox();
            this.lblMessages = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSavReSave = new System.Windows.Forms.Button();
            this.dgvSavRe = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Settlement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Settlment_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbTypeSavRe = new System.Windows.Forms.ComboBox();
            this.txtSavReTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpLoan = new System.Windows.Forms.GroupBox();
            this.cmbLoanWithdraw = new System.Windows.Forms.ComboBox();
            this.lblLoanWithType = new System.Windows.Forms.Label();
            this.btnSaveLoan = new System.Windows.Forms.Button();
            this.btnLoanAdd = new System.Windows.Forms.Button();
            this.lblLoanMsg = new System.Windows.Forms.Label();
            this.dgvLoan = new System.Windows.Forms.DataGridView();
            this.txtSC = new System.Windows.Forms.TextBox();
            this.txtLoanAmt = new System.Windows.Forms.TextBox();
            this.cmbMemberName = new System.Windows.Forms.ComboBox();
            this.lblLoanAmt = new System.Windows.Forms.Label();
            this.lblSC = new System.Windows.Forms.Label();
            this.lblMemName = new System.Windows.Forms.Label();
            this.cmbSlumLoan = new System.Windows.Forms.ComboBox();
            this.cmbDistrictLoan = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbCountryLoan = new System.Windows.Forms.ComboBox();
            this.cmbStateLoan = new System.Windows.Forms.ComboBox();
            this.cmbHubLoan = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LoanDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SettlementName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanSettlementID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Memberid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceCharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteLoan = new System.Windows.Forms.DataGridViewLinkColumn();
            this.grpSavRep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSavRe)).BeginInit();
            this.grpLoan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoan)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAccDate
            // 
            this.lblAccDate.AutoSize = true;
            this.lblAccDate.Location = new System.Drawing.Point(156, 19);
            this.lblAccDate.Name = "lblAccDate";
            this.lblAccDate.Size = new System.Drawing.Size(73, 13);
            this.lblAccDate.TabIndex = 0;
            this.lblAccDate.Text = "Account Date";
            // 
            // dtpAccountDate
            // 
            this.dtpAccountDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAccountDate.Location = new System.Drawing.Point(235, 13);
            this.dtpAccountDate.Name = "dtpAccountDate";
            this.dtpAccountDate.Size = new System.Drawing.Size(116, 20);
            this.dtpAccountDate.TabIndex = 1;
            // 
            // grpSavRep
            // 
            this.grpSavRep.Controls.Add(this.lblMessages);
            this.grpSavRep.Controls.Add(this.cmbslum);
            this.grpSavRep.Controls.Add(this.cmbdistrict);
            this.grpSavRep.Controls.Add(this.label7);
            this.grpSavRep.Controls.Add(this.label5);
            this.grpSavRep.Controls.Add(this.cmbcountry);
            this.grpSavRep.Controls.Add(this.cmbstate);
            this.grpSavRep.Controls.Add(this.cmbHub);
            this.grpSavRep.Controls.Add(this.label6);
            this.grpSavRep.Controls.Add(this.label8);
            this.grpSavRep.Controls.Add(this.label4);
            this.grpSavRep.Controls.Add(this.btnSavReSave);
            this.grpSavRep.Controls.Add(this.dgvSavRe);
            this.grpSavRep.Controls.Add(this.btnAdd);
            this.grpSavRep.Controls.Add(this.cmbTypeSavRe);
            this.grpSavRep.Controls.Add(this.txtSavReTotal);
            this.grpSavRep.Controls.Add(this.lblTotal);
            this.grpSavRep.Controls.Add(this.lblType);
            this.grpSavRep.Location = new System.Drawing.Point(12, 39);
            this.grpSavRep.Name = "grpSavRep";
            this.grpSavRep.Size = new System.Drawing.Size(785, 323);
            this.grpSavRep.TabIndex = 2;
            this.grpSavRep.TabStop = false;
            this.grpSavRep.Text = "Savings / Repayment / New Passbook";
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Location = new System.Drawing.Point(29, 112);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 148;
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(330, 53);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(144, 21);
            this.cmbslum.TabIndex = 6;
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(86, 51);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(131, 21);
            this.cmbdistrict.TabIndex = 5;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(226, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 144;
            this.label7.Text = "Slum/Settlement";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 143;
            this.label5.Text = "District/City";
            // 
            // cmbcountry
            // 
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(330, 19);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(144, 21);
            this.cmbcountry.TabIndex = 3;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(594, 22);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(122, 21);
            this.cmbstate.TabIndex = 4;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(86, 22);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(131, 21);
            this.cmbHub.TabIndex = 2;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 138;
            this.label6.Text = "Hub";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(484, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 139;
            this.label8.Text = "State/Province";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(226, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 137;
            this.label4.Text = "Country";
            // 
            // btnSavReSave
            // 
            this.btnSavReSave.Location = new System.Drawing.Point(288, 289);
            this.btnSavReSave.Name = "btnSavReSave";
            this.btnSavReSave.Size = new System.Drawing.Size(75, 23);
            this.btnSavReSave.TabIndex = 10;
            this.btnSavReSave.Text = "Save";
            this.btnSavReSave.UseVisualStyleBackColor = true;
            this.btnSavReSave.Click += new System.EventHandler(this.btnSavReSave_Click);
            // 
            // dgvSavRe
            // 
            this.dgvSavRe.AllowUserToAddRows = false;
            this.dgvSavRe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSavRe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSavRe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Settlement,
            this.Settlment_id,
            this.Type,
            this.Amount,
            this.Delete});
            this.dgvSavRe.Location = new System.Drawing.Point(37, 133);
            this.dgvSavRe.Name = "dgvSavRe";
            this.dgvSavRe.Size = new System.Drawing.Size(679, 150);
            this.dgvSavRe.TabIndex = 3;
            this.dgvSavRe.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSavRe_CellContentClick);
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Settlement
            // 
            this.Settlement.HeaderText = "Settlement";
            this.Settlement.Name = "Settlement";
            this.Settlement.ReadOnly = true;
            // 
            // Settlment_id
            // 
            this.Settlment_id.HeaderText = "Settlment_id";
            this.Settlment_id.Name = "Settlment_id";
            this.Settlment_id.ReadOnly = true;
            this.Settlment_id.Visible = false;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Delete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForLinkValue = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(487, 84);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbTypeSavRe
            // 
            this.cmbTypeSavRe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeSavRe.FormattingEnabled = true;
            this.cmbTypeSavRe.Items.AddRange(new object[] {
            "Savings",
            "Repayment",
            "NewPassbook",
            "UPF"});
            this.cmbTypeSavRe.Location = new System.Drawing.Point(86, 84);
            this.cmbTypeSavRe.Name = "cmbTypeSavRe";
            this.cmbTypeSavRe.Size = new System.Drawing.Size(131, 21);
            this.cmbTypeSavRe.TabIndex = 7;
            // 
            // txtSavReTotal
            // 
            this.txtSavReTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSavReTotal.Location = new System.Drawing.Point(330, 89);
            this.txtSavReTotal.Name = "txtSavReTotal";
            this.txtSavReTotal.ShortcutsEnabled = false;
            this.txtSavReTotal.Size = new System.Drawing.Size(142, 20);
            this.txtSavReTotal.TabIndex = 8;
            this.txtSavReTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSavReTotal_KeyPress);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(226, 88);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(43, 13);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "Amount";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(18, 87);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 13);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(367, 665);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 23);
            this.btnClose.TabIndex = 151;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grpLoan
            // 
            this.grpLoan.Controls.Add(this.cmbLoanWithdraw);
            this.grpLoan.Controls.Add(this.lblLoanWithType);
            this.grpLoan.Controls.Add(this.btnSaveLoan);
            this.grpLoan.Controls.Add(this.btnLoanAdd);
            this.grpLoan.Controls.Add(this.lblLoanMsg);
            this.grpLoan.Controls.Add(this.dgvLoan);
            this.grpLoan.Controls.Add(this.txtSC);
            this.grpLoan.Controls.Add(this.txtLoanAmt);
            this.grpLoan.Controls.Add(this.cmbMemberName);
            this.grpLoan.Controls.Add(this.lblLoanAmt);
            this.grpLoan.Controls.Add(this.lblSC);
            this.grpLoan.Controls.Add(this.lblMemName);
            this.grpLoan.Controls.Add(this.cmbSlumLoan);
            this.grpLoan.Controls.Add(this.cmbDistrictLoan);
            this.grpLoan.Controls.Add(this.label10);
            this.grpLoan.Controls.Add(this.label11);
            this.grpLoan.Controls.Add(this.cmbCountryLoan);
            this.grpLoan.Controls.Add(this.cmbStateLoan);
            this.grpLoan.Controls.Add(this.cmbHubLoan);
            this.grpLoan.Controls.Add(this.label12);
            this.grpLoan.Controls.Add(this.label13);
            this.grpLoan.Controls.Add(this.label14);
            this.grpLoan.Location = new System.Drawing.Point(12, 368);
            this.grpLoan.Name = "grpLoan";
            this.grpLoan.Size = new System.Drawing.Size(785, 290);
            this.grpLoan.TabIndex = 3;
            this.grpLoan.TabStop = false;
            this.grpLoan.Text = "New Loan / Withdrawal";
            // 
            // cmbLoanWithdraw
            // 
            this.cmbLoanWithdraw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoanWithdraw.FormattingEnabled = true;
            this.cmbLoanWithdraw.Items.AddRange(new object[] {
            "New Loan ",
            "Withdrawal"});
            this.cmbLoanWithdraw.Location = new System.Drawing.Point(73, 90);
            this.cmbLoanWithdraw.Name = "cmbLoanWithdraw";
            this.cmbLoanWithdraw.Size = new System.Drawing.Size(138, 21);
            this.cmbLoanWithdraw.TabIndex = 7;
            this.cmbLoanWithdraw.SelectedIndexChanged += new System.EventHandler(this.cmbLoanWithdraw_SelectedIndexChanged);
            // 
            // lblLoanWithType
            // 
            this.lblLoanWithType.AutoSize = true;
            this.lblLoanWithType.Location = new System.Drawing.Point(12, 90);
            this.lblLoanWithType.Name = "lblLoanWithType";
            this.lblLoanWithType.Size = new System.Drawing.Size(31, 13);
            this.lblLoanWithType.TabIndex = 170;
            this.lblLoanWithType.Text = "Type";
            // 
            // btnSaveLoan
            // 
            this.btnSaveLoan.Location = new System.Drawing.Point(288, 249);
            this.btnSaveLoan.Name = "btnSaveLoan";
            this.btnSaveLoan.Size = new System.Drawing.Size(75, 23);
            this.btnSaveLoan.TabIndex = 11;
            this.btnSaveLoan.Text = "Save";
            this.btnSaveLoan.UseVisualStyleBackColor = true;
            this.btnSaveLoan.Click += new System.EventHandler(this.btnSaveLoan_Click);
            // 
            // btnLoanAdd
            // 
            this.btnLoanAdd.Location = new System.Drawing.Point(641, 84);
            this.btnLoanAdd.Name = "btnLoanAdd";
            this.btnLoanAdd.Size = new System.Drawing.Size(75, 23);
            this.btnLoanAdd.TabIndex = 10;
            this.btnLoanAdd.Text = "Add";
            this.btnLoanAdd.UseVisualStyleBackColor = true;
            this.btnLoanAdd.Click += new System.EventHandler(this.btnLoanAdd_Click);
            // 
            // lblLoanMsg
            // 
            this.lblLoanMsg.AutoSize = true;
            this.lblLoanMsg.Location = new System.Drawing.Point(14, 116);
            this.lblLoanMsg.Name = "lblLoanMsg";
            this.lblLoanMsg.Size = new System.Drawing.Size(0, 13);
            this.lblLoanMsg.TabIndex = 166;
            // 
            // dgvLoan
            // 
            this.dgvLoan.AllowUserToAddRows = false;
            this.dgvLoan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLoan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LoanDate,
            this.SettlementName,
            this.LoanSettlementID,
            this.LoanType,
            this.Memberid,
            this.MemberName,
            this.LoanAmount,
            this.ServiceCharge,
            this.DeleteLoan});
            this.dgvLoan.Location = new System.Drawing.Point(17, 141);
            this.dgvLoan.Name = "dgvLoan";
            this.dgvLoan.Size = new System.Drawing.Size(743, 102);
            this.dgvLoan.TabIndex = 165;
            this.dgvLoan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLoan_CellContentClick);
            // 
            // txtSC
            // 
            this.txtSC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSC.Location = new System.Drawing.Point(505, 86);
            this.txtSC.Name = "txtSC";
            this.txtSC.Size = new System.Drawing.Size(100, 20);
            this.txtSC.TabIndex = 9;
            this.txtSC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSC_KeyPress);
            // 
            // txtLoanAmt
            // 
            this.txtLoanAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoanAmt.Location = new System.Drawing.Point(315, 88);
            this.txtLoanAmt.Name = "txtLoanAmt";
            this.txtLoanAmt.ShortcutsEnabled = false;
            this.txtLoanAmt.Size = new System.Drawing.Size(98, 20);
            this.txtLoanAmt.TabIndex = 8;
            this.txtLoanAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLoanAmt_KeyPress);
            // 
            // cmbMemberName
            // 
            this.cmbMemberName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMemberName.FormattingEnabled = true;
            this.cmbMemberName.Location = new System.Drawing.Point(578, 52);
            this.cmbMemberName.Name = "cmbMemberName";
            this.cmbMemberName.Size = new System.Drawing.Size(131, 21);
            this.cmbMemberName.TabIndex = 6;
            // 
            // lblLoanAmt
            // 
            this.lblLoanAmt.AutoSize = true;
            this.lblLoanAmt.Location = new System.Drawing.Point(235, 92);
            this.lblLoanAmt.Name = "lblLoanAmt";
            this.lblLoanAmt.Size = new System.Drawing.Size(43, 13);
            this.lblLoanAmt.TabIndex = 161;
            this.lblLoanAmt.Text = "Amount";
            // 
            // lblSC
            // 
            this.lblSC.AutoSize = true;
            this.lblSC.Location = new System.Drawing.Point(419, 88);
            this.lblSC.Name = "lblSC";
            this.lblSC.Size = new System.Drawing.Size(80, 13);
            this.lblSC.TabIndex = 160;
            this.lblSC.Text = "Service Charge";
            // 
            // lblMemName
            // 
            this.lblMemName.AutoSize = true;
            this.lblMemName.Location = new System.Drawing.Point(484, 55);
            this.lblMemName.Name = "lblMemName";
            this.lblMemName.Size = new System.Drawing.Size(76, 13);
            this.lblMemName.TabIndex = 159;
            this.lblMemName.Text = "Member Name";
            // 
            // cmbSlumLoan
            // 
            this.cmbSlumLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSlumLoan.FormattingEnabled = true;
            this.cmbSlumLoan.Location = new System.Drawing.Point(330, 54);
            this.cmbSlumLoan.Name = "cmbSlumLoan";
            this.cmbSlumLoan.Size = new System.Drawing.Size(144, 21);
            this.cmbSlumLoan.TabIndex = 5;
            this.cmbSlumLoan.SelectedIndexChanged += new System.EventHandler(this.cmbSlumLoan_SelectedIndexChanged);
            // 
            // cmbDistrictLoan
            // 
            this.cmbDistrictLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrictLoan.FormattingEnabled = true;
            this.cmbDistrictLoan.Location = new System.Drawing.Point(73, 54);
            this.cmbDistrictLoan.Name = "cmbDistrictLoan";
            this.cmbDistrictLoan.Size = new System.Drawing.Size(138, 21);
            this.cmbDistrictLoan.TabIndex = 4;
            this.cmbDistrictLoan.SelectedIndexChanged += new System.EventHandler(this.cmbDistrictLoan_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(226, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 155;
            this.label10.Text = "Slum/Settlement";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 154;
            this.label11.Text = "District/City";
            // 
            // cmbCountryLoan
            // 
            this.cmbCountryLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountryLoan.FormattingEnabled = true;
            this.cmbCountryLoan.Location = new System.Drawing.Point(330, 20);
            this.cmbCountryLoan.Name = "cmbCountryLoan";
            this.cmbCountryLoan.Size = new System.Drawing.Size(144, 21);
            this.cmbCountryLoan.TabIndex = 2;
            this.cmbCountryLoan.SelectedIndexChanged += new System.EventHandler(this.cmbCountryLoan_SelectedIndexChanged);
            // 
            // cmbStateLoan
            // 
            this.cmbStateLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStateLoan.FormattingEnabled = true;
            this.cmbStateLoan.Location = new System.Drawing.Point(578, 19);
            this.cmbStateLoan.Name = "cmbStateLoan";
            this.cmbStateLoan.Size = new System.Drawing.Size(138, 21);
            this.cmbStateLoan.TabIndex = 3;
            this.cmbStateLoan.SelectedIndexChanged += new System.EventHandler(this.cmbStateLoan_SelectedIndexChanged);
            // 
            // cmbHubLoan
            // 
            this.cmbHubLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHubLoan.FormattingEnabled = true;
            this.cmbHubLoan.Location = new System.Drawing.Point(73, 20);
            this.cmbHubLoan.Name = "cmbHubLoan";
            this.cmbHubLoan.Size = new System.Drawing.Size(138, 21);
            this.cmbHubLoan.TabIndex = 1;
            this.cmbHubLoan.SelectedIndexChanged += new System.EventHandler(this.cmbHubLoan_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 149;
            this.label12.Text = "Hub";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(484, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 150;
            this.label13.Text = "State/Province";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(226, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 148;
            this.label14.Text = "Country";
            // 
            // LoanDate
            // 
            this.LoanDate.HeaderText = "Date";
            this.LoanDate.Name = "LoanDate";
            this.LoanDate.ReadOnly = true;
            // 
            // SettlementName
            // 
            this.SettlementName.HeaderText = "SettlementName";
            this.SettlementName.Name = "SettlementName";
            this.SettlementName.ReadOnly = true;
            // 
            // LoanSettlementID
            // 
            this.LoanSettlementID.HeaderText = "SettlementID";
            this.LoanSettlementID.Name = "LoanSettlementID";
            this.LoanSettlementID.ReadOnly = true;
            this.LoanSettlementID.Visible = false;
            // 
            // LoanType
            // 
            this.LoanType.HeaderText = "LoanType";
            this.LoanType.Name = "LoanType";
            this.LoanType.ReadOnly = true;
            // 
            // Memberid
            // 
            this.Memberid.HeaderText = "Memberid";
            this.Memberid.Name = "Memberid";
            this.Memberid.ReadOnly = true;
            this.Memberid.Visible = false;
            // 
            // MemberName
            // 
            this.MemberName.HeaderText = "MemberName";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // LoanAmount
            // 
            this.LoanAmount.HeaderText = "LoanAmount";
            this.LoanAmount.Name = "LoanAmount";
            this.LoanAmount.ReadOnly = true;
            // 
            // ServiceCharge
            // 
            this.ServiceCharge.HeaderText = "ServiceCharge";
            this.ServiceCharge.Name = "ServiceCharge";
            this.ServiceCharge.ReadOnly = true;
            // 
            // DeleteLoan
            // 
            this.DeleteLoan.HeaderText = "Delete";
            this.DeleteLoan.Name = "DeleteLoan";
            this.DeleteLoan.Text = "Delete";
            this.DeleteLoan.UseColumnTextForLinkValue = true;
            // 
            // DailyAccountsEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(809, 700);
            this.ControlBox = false;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpLoan);
            this.Controls.Add(this.grpSavRep);
            this.Controls.Add(this.dtpAccountDate);
            this.Controls.Add(this.lblAccDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DailyAccountsEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.DailyAccountsEntry_Load);
            this.grpSavRep.ResumeLayout(false);
            this.grpSavRep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSavRe)).EndInit();
            this.grpLoan.ResumeLayout(false);
            this.grpLoan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAccDate;
        private System.Windows.Forms.DateTimePicker dtpAccountDate;
        private System.Windows.Forms.GroupBox grpSavRep;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.ComboBox cmbTypeSavRe;
        private System.Windows.Forms.TextBox txtSavReTotal;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgvSavRe;
        private System.Windows.Forms.Button btnSavReSave;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Settlement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Settlment_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;
        private System.Windows.Forms.GroupBox grpLoan;
        private System.Windows.Forms.ComboBox cmbSlumLoan;
        private System.Windows.Forms.ComboBox cmbDistrictLoan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbCountryLoan;
        private System.Windows.Forms.ComboBox cmbStateLoan;
        private System.Windows.Forms.ComboBox cmbHubLoan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblLoanAmt;
        private System.Windows.Forms.Label lblSC;
        private System.Windows.Forms.Label lblMemName;
        private System.Windows.Forms.ComboBox cmbMemberName;
        private System.Windows.Forms.TextBox txtLoanAmt;
        private System.Windows.Forms.TextBox txtSC;
        private System.Windows.Forms.DataGridView dgvLoan;
        private System.Windows.Forms.Label lblLoanMsg;
        private System.Windows.Forms.Button btnLoanAdd;
        private System.Windows.Forms.Button btnSaveLoan;
        private System.Windows.Forms.Label lblLoanWithType;
        private System.Windows.Forms.ComboBox cmbLoanWithdraw;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn SettlementName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanSettlementID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Memberid;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceCharge;
        private System.Windows.Forms.DataGridViewLinkColumn DeleteLoan;
    }
}