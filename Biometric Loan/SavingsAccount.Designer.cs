﻿namespace SPARC
{
    partial class frmSavingsAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSAcc = new System.Windows.Forms.Panel();
            this.grbSavingAcc = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSavingType = new System.Windows.Forms.ComboBox();
            this.txtSavingPbNo = new System.Windows.Forms.TextBox();
            this.lblSavingPBNo = new System.Windows.Forms.Label();
            this.lblSavingType = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblMessages = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblOpenDate = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.lblCurrBalanceValue = new System.Windows.Forms.Label();
            this.lblCurrentBalance = new System.Windows.Forms.Label();
            this.lblAccValue = new System.Windows.Forms.Label();
            this.lblAccountID = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblMemberValue = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.pnlSAcc.SuspendLayout();
            this.grbSavingAcc.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSAcc
            // 
            this.pnlSAcc.Controls.Add(this.grbSavingAcc);
            this.pnlSAcc.Location = new System.Drawing.Point(2, 4);
            this.pnlSAcc.Name = "pnlSAcc";
            this.pnlSAcc.Size = new System.Drawing.Size(795, 477);
            this.pnlSAcc.TabIndex = 0;
            // 
            // grbSavingAcc
            // 
            this.grbSavingAcc.Controls.Add(this.label2);
            this.grbSavingAcc.Controls.Add(this.label3);
            this.grbSavingAcc.Controls.Add(this.label1);
            this.grbSavingAcc.Controls.Add(this.cmbSavingType);
            this.grbSavingAcc.Controls.Add(this.txtSavingPbNo);
            this.grbSavingAcc.Controls.Add(this.lblSavingPBNo);
            this.grbSavingAcc.Controls.Add(this.lblSavingType);
            this.grbSavingAcc.Controls.Add(this.btnDelete);
            this.grbSavingAcc.Controls.Add(this.lblMessages);
            this.grbSavingAcc.Controls.Add(this.btnClose);
            this.grbSavingAcc.Controls.Add(this.btnSave);
            this.grbSavingAcc.Controls.Add(this.dtpOpenDate);
            this.grbSavingAcc.Controls.Add(this.lblOpenDate);
            this.grbSavingAcc.Controls.Add(this.cmbAgentName);
            this.grbSavingAcc.Controls.Add(this.lblAgentName);
            this.grbSavingAcc.Controls.Add(this.lblCurrBalanceValue);
            this.grbSavingAcc.Controls.Add(this.lblCurrentBalance);
            this.grbSavingAcc.Controls.Add(this.lblAccValue);
            this.grbSavingAcc.Controls.Add(this.lblAccountID);
            this.grbSavingAcc.Controls.Add(this.txtRemarks);
            this.grbSavingAcc.Controls.Add(this.lblRemarks);
            this.grbSavingAcc.Controls.Add(this.cmbStatus);
            this.grbSavingAcc.Controls.Add(this.lblStatus);
            this.grbSavingAcc.Controls.Add(this.lblMemberValue);
            this.grbSavingAcc.Controls.Add(this.lblMemberID);
            this.grbSavingAcc.Location = new System.Drawing.Point(80, 31);
            this.grbSavingAcc.Name = "grbSavingAcc";
            this.grbSavingAcc.Size = new System.Drawing.Size(667, 424);
            this.grbSavingAcc.TabIndex = 88;
            this.grbSavingAcc.TabStop = false;
            this.grbSavingAcc.Text = "Savings Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(213, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 20);
            this.label2.TabIndex = 96;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(257, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 20);
            this.label3.TabIndex = 95;
            this.label3.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(213, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 20);
            this.label1.TabIndex = 93;
            this.label1.Text = "*";
            // 
            // cmbSavingType
            // 
            this.cmbSavingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSavingType.FormattingEnabled = true;
            this.cmbSavingType.Location = new System.Drawing.Point(290, 80);
            this.cmbSavingType.Name = "cmbSavingType";
            this.cmbSavingType.Size = new System.Drawing.Size(259, 21);
            this.cmbSavingType.TabIndex = 1;
            this.cmbSavingType.SelectedIndexChanged += new System.EventHandler(this.cmbSavingType_SelectedIndexChanged);
            // 
            // txtSavingPbNo
            // 
            this.txtSavingPbNo.Location = new System.Drawing.Point(290, 110);
            this.txtSavingPbNo.Name = "txtSavingPbNo";
            this.txtSavingPbNo.Size = new System.Drawing.Size(259, 20);
            this.txtSavingPbNo.TabIndex = 2;
            // 
            // lblSavingPBNo
            // 
            this.lblSavingPBNo.AutoSize = true;
            this.lblSavingPBNo.Location = new System.Drawing.Point(146, 110);
            this.lblSavingPBNo.Name = "lblSavingPBNo";
            this.lblSavingPBNo.Size = new System.Drawing.Size(115, 13);
            this.lblSavingPBNo.TabIndex = 89;
            this.lblSavingPBNo.Text = "Savings Passbook No.";
            // 
            // lblSavingType
            // 
            this.lblSavingType.AutoSize = true;
            this.lblSavingType.Location = new System.Drawing.Point(146, 83);
            this.lblSavingType.Name = "lblSavingType";
            this.lblSavingType.Size = new System.Drawing.Size(67, 13);
            this.lblSavingType.TabIndex = 88;
            this.lblSavingType.Text = "Saving Type";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(384, 340);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(15, 379);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 87;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(474, 340);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(290, 340);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpOpenDate
            // 
            this.dtpOpenDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenDate.Location = new System.Drawing.Point(290, 229);
            this.dtpOpenDate.Name = "dtpOpenDate";
            this.dtpOpenDate.Size = new System.Drawing.Size(260, 20);
            this.dtpOpenDate.TabIndex = 5;
            // 
            // lblOpenDate
            // 
            this.lblOpenDate.AutoSize = true;
            this.lblOpenDate.Location = new System.Drawing.Point(146, 236);
            this.lblOpenDate.Name = "lblOpenDate";
            this.lblOpenDate.Size = new System.Drawing.Size(59, 13);
            this.lblOpenDate.TabIndex = 80;
            this.lblOpenDate.Text = "Open Date";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(290, 195);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(260, 21);
            this.cmbAgentName.TabIndex = 4;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(146, 203);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(71, 13);
            this.lblAgentName.TabIndex = 77;
            this.lblAgentName.Text = "Leader Name";
            // 
            // lblCurrBalanceValue
            // 
            this.lblCurrBalanceValue.AutoSize = true;
            this.lblCurrBalanceValue.Location = new System.Drawing.Point(290, 170);
            this.lblCurrBalanceValue.Name = "lblCurrBalanceValue";
            this.lblCurrBalanceValue.Size = new System.Drawing.Size(22, 13);
            this.lblCurrBalanceValue.TabIndex = 76;
            this.lblCurrBalanceValue.Text = "NA";
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.AutoSize = true;
            this.lblCurrentBalance.Location = new System.Drawing.Point(146, 170);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(83, 13);
            this.lblCurrentBalance.TabIndex = 75;
            this.lblCurrentBalance.Text = "Current Balance";
            // 
            // lblAccValue
            // 
            this.lblAccValue.AutoSize = true;
            this.lblAccValue.Location = new System.Drawing.Point(290, 56);
            this.lblAccValue.Name = "lblAccValue";
            this.lblAccValue.Size = new System.Drawing.Size(19, 13);
            this.lblAccValue.TabIndex = 74;
            this.lblAccValue.Text = "<>";
            // 
            // lblAccountID
            // 
            this.lblAccountID.AutoSize = true;
            this.lblAccountID.Location = new System.Drawing.Point(146, 56);
            this.lblAccountID.Name = "lblAccountID";
            this.lblAccountID.Size = new System.Drawing.Size(61, 13);
            this.lblAccountID.TabIndex = 73;
            this.lblAccountID.Text = "Account ID";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(290, 261);
            this.txtRemarks.MaxLength = 1500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(260, 59);
            this.txtRemarks.TabIndex = 6;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(146, 277);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 4;
            this.lblRemarks.Text = "Remarks";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Active",
            "Closed"});
            this.cmbStatus.Location = new System.Drawing.Point(290, 134);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(260, 21);
            this.cmbStatus.TabIndex = 3;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(146, 137);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Status";
            // 
            // lblMemberValue
            // 
            this.lblMemberValue.AutoSize = true;
            this.lblMemberValue.Location = new System.Drawing.Point(290, 23);
            this.lblMemberValue.Name = "lblMemberValue";
            this.lblMemberValue.Size = new System.Drawing.Size(35, 13);
            this.lblMemberValue.TabIndex = 1;
            this.lblMemberValue.Text = "label1";
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(146, 23);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(59, 13);
            this.lblMemberID.TabIndex = 0;
            this.lblMemberID.Text = "Member ID";
            // 
            // frmSavingsAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(809, 493);
            this.ControlBox = false;
            this.Controls.Add(this.pnlSAcc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSavingsAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.SavingsAccount_Load);
            this.pnlSAcc.ResumeLayout(false);
            this.grbSavingAcc.ResumeLayout(false);
            this.grbSavingAcc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSAcc;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblMemberValue;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label lblCurrBalanceValue;
        private System.Windows.Forms.Label lblCurrentBalance;
        private System.Windows.Forms.Label lblAccValue;
        private System.Windows.Forms.Label lblAccountID;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.DateTimePicker dtpOpenDate;
        private System.Windows.Forms.Label lblOpenDate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grbSavingAcc;
        private System.Windows.Forms.Label lblSavingPBNo;
        private System.Windows.Forms.Label lblSavingType;
        private System.Windows.Forms.ComboBox cmbSavingType;
        private System.Windows.Forms.TextBox txtSavingPbNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}