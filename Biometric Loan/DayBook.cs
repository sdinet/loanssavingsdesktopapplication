﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SPARC
{
    public partial class frmDayBook : Form
    {
        public frmDayBook()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            base.WndProc(ref message);
        }

        private void btnCreateDayBook_Click(object sender, EventArgs e)
        {
            try
            {
                DayBook objDayBook = new DayBook();

                objDayBook.TransactionDate = dtpOpenDate.Value;
                objDayBook.Createdby = GlobalValues.User_PkId;
                objDayBook.Updatedby = GlobalValues.User_PkId;
                objDayBook.Particulars = string.Empty;
                objDayBook.Remarks = string.Empty;

                int saveResult = objDayBook.GenerateDayBook();

                lblMessages.Text = Messages.DAYBOOKGEN_SUCCESS;
                lblMessages.ForeColor = Color.Green;
                //Messages.ShowMessage(Messages.DAYBOOKGEN_SUCCESS, Messages.MsgType.success);
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.DAYBOOKGEN_ERROR, Messages.MsgType.failure, ex);
            }
        }

        private void frmDayBook_Load(object sender, EventArgs e)
        {
            dtpOpenDate.MaxDate = DateTime.Now.Date;
            btnCreateDayBook.Focus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
    }
}
