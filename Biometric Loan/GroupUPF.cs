﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SPARC;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
namespace SPARC
{
  public partial  class GroupUPF
    {
        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private double m_InterestPercent;
        private DateTime m_UpdatedDate;
        private DateTime m_CreatedDate;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private string m_PayOutFrequency;
        private bool m_Isdeleted;
        private DateTime m_PayOutEndDate;
        private DateTime m_Opendate;
        private string m_Remarks;
        private string m_Tran_type;
        private decimal m_Amount;
        private string m_PayMode;
        private int m_Id;
        private string g_GroupNumber;
      
        private int? g_fk_SlumId;
        private int? g_fk_HubId;
        private int? g_fk_DistrictId;
        private int? g_fk_StateId;
        private int? g_fk_CountryId;
        private int? g_fk_GroupId;
        private string g_Status;
        private int g_fk_AgentId;
        private Int64 m_AccId;
       


        public double InterestPercent
        {
            get
            {
                return m_InterestPercent;
            }
            set
            {
                m_InterestPercent = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public string PayOutFrequency
        {
            get
            {
                return m_PayOutFrequency;
            }
            set
            {
                m_PayOutFrequency = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public DateTime PayOutEndDate
        {
            get
            {
                return m_PayOutEndDate;
            }
            set
            {
                m_PayOutEndDate = value;
            }
        }
        public DateTime Opendate
        {
            get
            {
                return m_Opendate;
            }
            set
            {
                m_Opendate = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                m_Remarks = value;
            }
        }
        public string Tran_type
        {
            get
            {
                return m_Tran_type;
            }
            set
            {
                m_Tran_type = value;
            }
        }
        public string Pay_Mode
        {
            get
            {
                return m_PayMode;
            }
            set
            {
                m_PayMode = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                m_Amount = value;
            }
        }

        public int? fk_HubId
        {
            get
            {
                return g_fk_HubId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_HubId = value;
            }
        }
        public int? fk_CountryId
        {
            get
            {
                return g_fk_CountryId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_CountryId = value;
            }
        }
        public int? fk_StateId
        {
            get
            {
                return g_fk_StateId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_StateId = value;
            }
        }
        public int? fk_DistricId
        {
            get
            {
                return g_fk_DistrictId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_DistrictId = value;
            }
        }
        public int? fk_SlumId
        {
            get
            {
                return g_fk_SlumId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_SlumId = value;
            }
        }

      public int? fk_GroupId
      {
          get{
              return g_fk_GroupId;
          }
          set
          {
              g_fk_GroupId = value;
          }
          }
      public string   GroupNumber
      {
          get
          {
              return g_GroupNumber;
          }
          set
          {
              g_GroupNumber = value;
          }
      }

      public string Status
      {
          get
          {
              return g_Status;
          }
          set
          {
              g_Status = value;
          }
      }
      public int fk_AgentId
      {
          get
          {
              return g_fk_AgentId;
          }
          set
          {
              g_fk_AgentId = value;
          }
      }
      public Int64 AccId
      {
          get
          {
              return m_AccId;
          }
          set
          {
              m_AccId = value;
          }
      }

        public Int32 SaveUPFAccount()
        {

            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;
            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int, 0);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Fk_GroupId", SqlDbType.Int, 0);
            sqlParam[iCounter++].Value = g_fk_GroupId;
           
           
            //sqlParam[iCounter] = new SqlParameter("@Tran_type", SqlDbType.Char, 3);
            //sqlParam[iCounter++].Value = m_Tran_type;

            //sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            //sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@InterestPercent", SqlDbType.Float, 0);
            sqlParam[iCounter++].Value = m_InterestPercent;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            //sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            //sqlParam[iCounter++].Value = fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = g_Status;

         //   return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupUPFAccountTransactionsInsert", sqlParam);
          
            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "InsertGroupUPFAccount", sqlParam));
        }

        public DataSet SaveUPFAccounttrans(Int32 UPFAccId)
        {

            SqlParameter[] sqlParam = new SqlParameter[13];
            int iCounter = 0;
            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int, 0);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Fk_GroupId", SqlDbType.Int, 0);
            sqlParam[iCounter++].Value = g_fk_GroupId;


            sqlParam[iCounter] = new SqlParameter("@Tran_type", SqlDbType.Char, 3);
            sqlParam[iCounter++].Value = m_Tran_type;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@InterestPercent", SqlDbType.Float, 0);
            sqlParam[iCounter++].Value = m_InterestPercent;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = g_Status;

            sqlParam[iCounter] = new SqlParameter("@fk_AccountId", SqlDbType.Int,0);
            sqlParam[iCounter++].Value = UPFAccId;

            //   return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupUPFAccountTransactionsInsert", sqlParam);
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "InsertGroupUPFAccounttrans", sqlParam);
        }
        public DataSet LoadAllUPFAccs()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_GroupId", SqlDbType.BigInt);
            sqlParam[0].Value = g_fk_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllGroupUPFAccountSelect", sqlParam);
        }
        public DataSet UpdateUPFAccount(int iAction)
        {
            SqlParameter[] sqlParam = new SqlParameter[11];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = g_Status;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@iAction", SqlDbType.Int);
            sqlParam[iCounter++].Value = iAction;

            sqlParam[iCounter] = new SqlParameter("@PayMode", SqlDbType.VarChar, 10);
            sqlParam[iCounter++].Value = Pay_Mode;

            sqlParam[iCounter] = new SqlParameter("@AccId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = AccId;

         
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "InsertGroupUPFPayouttrans", sqlParam);
           // return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "InsertGroupUPFPayouttrans", sqlParam));
        }
        public Int32 UpdateUPFAccountnew()
        {
            SqlParameter[] sqlParam = new SqlParameter[6];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = g_Status;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            // return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "InsertGroupUPFPayouttrans", sqlParam);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "UpdateGroupUpfAccount", sqlParam));
        }
        public DataSet DeleteOpenUPFAccount(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@AccId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_AccId;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupUPFAccountTransactionsDelete", sqlParam);
        }


        public DataSet LoadInterestPayOut()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_GroupId", SqlDbType.BigInt);
            sqlParam[0].Value = g_fk_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoadGroupInterestPayout", sqlParam);
        }
        public DataSet LoadAllUPFTrans()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_GroupId", SqlDbType.BigInt);
            sqlParam[0].Value = g_fk_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllGroupUPFAccountSelectLoad", sqlParam);
        }
        public DataSet ReOpenUPFAccount(Int64 UserId,Int64 transid)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@TransId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = transid;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;
           

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupUPFAccountReOpen", sqlParam);
        }

    }
}
