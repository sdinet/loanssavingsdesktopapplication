﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; // webcam function
using System.Drawing.Drawing2D;
using System.Configuration;
using System.IO;
using SPARC;
using System.Text.RegularExpressions;
using Microsoft.ApplicationBlocks.Data;

namespace SPARC
{
    public partial class CreateMemberFamily : Form
    {
        public string strMessage1 = "";
        public string strMessage2 = "";
        public string result1 = "";
        public bool result;
        public int i;
        public string strMessage = "";
        public Int32 rowindex;  // This var is to get the RowId  from the Grid 
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        MemoryStream ms = new MemoryStream();
        WebCam webcam;
        bool IsPhotoChanged = false;
        public Int16 intMemberOperationType;

        public CreateMemberFamily()
        {
            InitializeComponent();
        }

        protected override void WndProc(ref Message message)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (message.Msg)
            {
                case WM_SYSCOMMAND:
                    int command = message.WParam.ToInt32() & 0xfff0;
                    if (command == SC_MOVE)
                        return;
                    break;
            }

            
            base.WndProc(ref message);
        }

        //Display Data in DataGridView  
        private void DisplayData()
        {

            SqlDataAdapter adapt;
            MemberFamily objMemberfamily = new MemberFamily();

            objMemberfamily.MemberFamilyId = GlobalValues.MemberFamilyId;
            if (objMemberfamily.MemberFamilyId != 0)
            {
                objMemberfamily.MemberFamilyId = GlobalValues.FamilyMember_PkId;
            }
            SqlConnection con = new SqlConnection(ConnectionString);
            DataSet Dsmember = objMemberfamily.GetFamilyGetails();
            con.Open();
            DataTable dt = new DataTable();

            SqlCommand cmd = new SqlCommand("usp_GetMemberDetailsFGrid", con);
            cmd.Parameters.AddWithValue("@MemberFamilyId", objMemberfamily.MemberFamilyId);
            adapt = new SqlDataAdapter(cmd);
            adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            con.Close();
        }

        //Clear Data  
        private void ClearData()
        {
            FMDetailsNm.Text = "";
            //FMDetailsGender.Text = "";
            GenderList.SelectedIndex = 0;
            FMDetailsAge.Text = "";
            FMDetailsQualifiaction.Text = "";
            CheckBoxFMIsMember.Checked = false;
            FMDetailsMemberID.Text = "";
            // AddFamilyMessages.Text = ""; // After inserting the data clearing the Label Text
        }

        //During the Update to Featch the Data From the Database Calling form SearchMemberFamily
        public void LoadControls(MemberFamily objMemberFetch)
        {
            try
            {
                btnSave.Text = "Update";
                DataSet dsMemberFetch = objMemberFetch.LoadMemberFamilyById();
                if (dsMemberFetch != null && dsMemberFetch.Tables.Count > 0 && dsMemberFetch.Tables[0].Rows.Count > 0)
                    GlobalValues.MemberFamilyId = Convert.ToInt32(dsMemberFetch.Tables[0].Rows[0]["MemberFamilyId"]);
                txtHouseNo.Text = dsMemberFetch.Tables[0].Rows[0]["HouseNumber"].ToString();
                txtRationNo.Text = dsMemberFetch.Tables[0].Rows[0]["RationCardNumber"].ToString();
                txtFamilyStay.Text = dsMemberFetch.Tables[0].Rows[0]["HowManyYearsTheFamilyIsStaying"].ToString();
                txtFamilyHeadName.Text = dsMemberFetch.Tables[0].Rows[0]["FamilyheadName"].ToString();
                txtHusWifeNm.Text = dsMemberFetch.Tables[0].Rows[0]["Husband/WifeName"].ToString();
                richtxtHouseBisiness.Text = dsMemberFetch.Tables[0].Rows[0]["PartOfHouseForBusiness"].ToString();
                txtCast.Text = dsMemberFetch.Tables[0].Rows[0]["Caste"].ToString();
                txtFamilyIncome.Text = dsMemberFetch.Tables[0].Rows[0]["FamilyMounthlyIncome"].ToString();
                richtxtHouseDetails.Text = dsMemberFetch.Tables[0].Rows[0]["HouseDetails"].ToString();
                txtMotherTng.Text = dsMemberFetch.Tables[0].Rows[0]["MotherToungue"].ToString();
                // txtWater.Text = dsMemberFetch.Tables[0].Rows[0]["Water"].ToString(); // This is the Textbox Value
                WaterList.SelectedItem = dsMemberFetch.Tables[0].Rows[0]["Water"].ToString(); // This is the Dropdown Value
                txtSate.Text = dsMemberFetch.Tables[0].Rows[0]["State"].ToString();
                txtToilte.Text = dsMemberFetch.Tables[0].Rows[0]["Toilet"].ToString();
                txtElectricity.Text = dsMemberFetch.Tables[0].Rows[0]["Electricity"].ToString();
                // txtCooking.Text = dsMemberFetch.Tables[0].Rows[0]["CookingMedium"].ToString(); //This the Textbox value
                CookMediumList.SelectedItem = dsMemberFetch.Tables[0].Rows[0]["CookingMedium"].ToString();// drop down value
                if (Convert.ToBoolean(dsMemberFetch.Tables[0].Rows[0]["PartOfHouseForBusiness"]) == true)
                {
                    richtxtHouseBisiness.Enabled = true;
                    redioBtnYes.Checked = Convert.ToBoolean(dsMemberFetch.Tables[0].Rows[0]["PartOfHouseForBusiness"]);
                    richtxtHouseBisiness.Text = dsMemberFetch.Tables[0].Rows[0]["BusinessDetails"].ToString();

                }
                else
                {
                    redioBtnNo.Checked = Convert.ToBoolean(dsMemberFetch.Tables[0].Rows[0]["PartOfHouseForBusiness"]);
                    redioBtnNo.Checked = true;

                }

                MemberFamily objMember = new MemberFamily();

                cmbHub.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_hubid"]);
                cmbCountry.SelectedValue = dsMemberFetch.Tables[0].Rows[0]["fk_countryid"];
                cmbState.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_stateid"]);
                cmbDistrict.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_districtid"]);
                cmbSlum.SelectedValue = Convert.ToInt16(dsMemberFetch.Tables[0].Rows[0]["fk_SlumId"]);
                objMember.MemberFamilyId = GlobalValues.MemberFamilyId;
                DataSet dsMember = objMember.GetFamilyGetails(); // To store the data into the grid
                dataGridView1.DataSource = dsMember.Tables[0];

                try
                {
                    if (dsMemberFetch.Tables[0].Rows[0]["FamilyPhotograph"] != null && dsMemberFetch.Tables[0].Rows[0]["FamilyPhotograph"] != DBNull.Value)
                    {
                        if (imgCapture.Image != null)
                        {
                            imgCapture.Image.Dispose();
                        }
                        MemoryStream ms = new MemoryStream((byte[])dsMemberFetch.Tables[0].Rows[0]["FamilyPhotograph"]);//create memory stream by passing byte array of the image
                        imgCapture.Image = Image.FromStream(ms);//set image property of the picture box by creating a image from stream 
                        imgCapture.SizeMode = PictureBoxSizeMode.StretchImage;//set size mode property of the picture box to stretch 
                        imgCapture.Refresh();
                    }
                }
                catch (Exception ex)
                {
                    Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
                }
            }
            catch (IndexOutOfRangeException ex)
            {
               // Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void btnStart_Click_1(object sender, EventArgs e)
        {
            webcam.Start();
        }

        private void btnStop_Click_1(object sender, EventArgs e)
        {
            webcam.Stop();
        }

        private void btnContinue_Click_1(object sender, EventArgs e)
        {
            webcam.Continue();
        }

        private void btnCapture_Click_1(object sender, EventArgs e)
        {
            imgCapture.Image = imgVideo.Image;
            IsPhotoChanged = true;
        }

        private void btnVideoSource_Click_1(object sender, EventArgs e)
        {
            webcam.AdvanceSetting();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnStop.Enabled = true;
            btnContinue.Enabled = true;
            btnCapture.Enabled = true;
            btnVideoSource.Enabled = true;
            btnBrowse.Enabled = false;
            txtFileUpload.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            btnContinue.Enabled = false;
            btnCapture.Enabled = false;
            btnVideoSource.Enabled = false;
            btnBrowse.Enabled = true;
            txtFileUpload.Enabled = true;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                ofdPhoto.ShowDialog();
                ofdPhoto.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                if (ofdPhoto.ShowDialog() == DialogResult.OK)
                {
                    Image img = Image.FromFile(ofdPhoto.FileName);
                    imgCapture.Image = img;

                    //imgCapture.Image = new Bitmap(ofdPhoto.FileName);
                    IsPhotoChanged = true;
                    //imgCapture.Image.Width = 156;
                    //imgCapture.Image.Height = 161;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert,
                            System.Drawing.Imaging.ImageFormat formatOfImage)
        {
            byte[] Ret;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    imageToConvert.Save(ms, formatOfImage);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }
            return Ret;
        }
        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();

            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            return ms.ToArray();
        }
        private byte[] convertPicBoxImageToByte(System.Windows.Forms.PictureBox pbImage)
        {
            MemoryStream ms1 = new MemoryStream();
            Image ImgConverted = this.resizeImage(pbImage.Image, new Size(125, 150));
            ImgConverted.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);//pbImage.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms1.ToArray();
        }
        private Image resizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }
        private Image img;

        private string DoValidations()
        {

            string ErrorMsg = string.Empty;
            if (cmbSlum.SelectedIndex == 0)
                ErrorMsg = FamilyMessages.SLUMNAME + Messages.COMMAWITHSPACE;
            if (txtHouseNo.Text.Trim().Length == 0)
                //CR-12 end
                ErrorMsg += FamilyMessages.HOUSEHUTNO + Messages.COMMAWITHSPACE;
            if (txtRationNo.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.RATIONNUMBER + Messages.COMMAWITHSPACE;
            if (txtFamilyStay.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.FAMILYSTAY + Messages.COMMAWITHSPACE;
            if (txtFamilyHeadName.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.FAMILYHEADNAME + Messages.COMMAWITHSPACE;
            if (txtHusWifeNm.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.HUSBANDWIFENAME + Messages.COMMAWITHSPACE;
            if (redioBtnYes.Checked == false && redioBtnNo.Checked == false)
                ErrorMsg += FamilyMessages.RADIOYESANDNO + Messages.COMMAWITHSPACE;
            if (redioBtnYes.Checked == true && richtxtHouseBisiness.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.YESRADIOBUTTON + Messages.COMMAWITHSPACE;
            if (txtCast.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.CASTE + Messages.COMMAWITHSPACE;
            if (txtFamilyIncome.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.MONTHLYINCOME + Messages.COMMAWITHSPACE;
            if (richtxtHouseDetails.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.HOUSEDETAILS + Messages.COMMAWITHSPACE;
            if (txtMotherTng.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.MOTHERTONGUE + Messages.COMMAWITHSPACE;
            if (txtSate.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.STATE + Messages.COMMAWITHSPACE;
            if (WaterList.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.WATER + Messages.COMMAWITHSPACE;
            if (txtToilte.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.TOILET + Messages.COMMAWITHSPACE;
            if (txtElectricity.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.ELECTRICITY + Messages.COMMAWITHSPACE;
            if (CookMediumList.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.COOKINGMEDIUM + Messages.COMMAWITHSPACE;

            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                lblFamilyMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        //Validation For the Child Data//
        public string FamilyDoValidations()
        {

            string ErrorMsg = string.Empty;
            // string ErrorMsg = null;

            if (FMDetailsNm.Text.Trim().Length == 0)
                ErrorMsg = FamilyMessages.NAME + FamilyMessages.COMMAWITHSPACE;
            if (GenderList.Text.Trim().Length == 0)// here i chandeinto the dropdown name
                ErrorMsg += FamilyMessages.Gender + FamilyMessages.COMMAWITHSPACE;
            if (FMDetailsAge.Text == "")
                ErrorMsg += FamilyMessages.Age + FamilyMessages.COMMAWITHSPACE;
            if (FMDetailsQualifiaction.Text.Trim().Length == 0)
                ErrorMsg += FamilyMessages.Qualification + FamilyMessages.COMMAWITHSPACE;
            if (CheckBoxFMIsMember.Checked == false)
                // ErrorMsg += FamilyMessages.IsMember + FamilyMessages.COMMAWITHSPACE;

                CheckMemberId();
            // if (result == false)
            // {
            //    ErrorMsg += FamilyMessages.MemberID + FamilyMessages.COMMAWITHSPACE;
            // }
            if (ErrorMsg.Trim().Length > 0)
            {
                ErrorMsg = Messages.REQUIREDFIELDS + ErrorMsg;
                ErrorMsg = ErrorMsg.Substring(0, ErrorMsg.Length - 2);

                AddFamilyMessages.ForeColor = Color.Red;
            }
            return ErrorMsg;
        }

        //check the memberId
        public Boolean CheckMemberId()
        {
            MemberFamily objMemberfamily = new MemberFamily();
            DataSet DsMember = objMemberfamily.CollectMember();
            //var value = DsMember;
            foreach (DataRow Dr in DsMember.Tables[0].Rows)
            {
                if ((FMDetailsMemberID.Text) == Dr["MemberId"].ToString())
                {
                    result = true;
                    break;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
        private void FillCombos(int CodeTypeID, int ParentID, ComboBox cmbLocation)
        {
            CodeValues objCodeValues = new CodeValues();
            objCodeValues.CodeTypeID = CodeTypeID;
            objCodeValues.ParentID = ParentID;

            DataSet dsCodeValues = objCodeValues.GetCodeValues(false);
            if (dsCodeValues != null && dsCodeValues.Tables.Count > 0 && dsCodeValues.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dsCodeValues.Tables[0].NewRow();
                dr[0] = 0;
                dr[1] = "--Select One--";
                dr[2] = -1;

                dsCodeValues.Tables[0].Rows.InsertAt(dr, 0);

                cmbLocation.DisplayMember = "Name";
                cmbLocation.ValueMember = "Id";
                cmbLocation.DataSource = dsCodeValues.Tables[0];
            }
        }
        private void cmbCountry_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            //Fetch State
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fetch Slum
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        private void cmbslum_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //To Load the CreateFamily Load//
        private void CreateMemberFamily_Load(object sender, EventArgs e)
        {
            UpdatedataGrid.Enabled = false;
            DeleteGrid.Enabled = false;
            rdbCameraClick.Checked = true;
            AddFamilyMessages.Visible = false; // here i am hiding the AddFamilyMessages
            lblFamilyMessages.Visible = false; // Error and success messages of master form
            try
            {
                if (intMemberOperationType == 0)
                {
                    MemberFamily objMember = new MemberFamily();
                    GlobalValues.MemberFamilyId = 0;
                    objMember.MemberFamilyId = GlobalValues.MemberFamilyId;
                    // changed to check
                    this.Text = Constants.CREATEMAMEMBERFAMILY;
                    InitializePage();
                    ClearControls();
                }
                else
                {
                    this.Text = Constants.UPDATEMEMBERFAMILY;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
            richtxtHouseBisiness.Enabled = false;
            FMDetailsMemberID.Enabled = false;
            DisplayData();
        }
        private void ClearControls()
        {
            cmbSlum.Text = string.Empty;
            txtHouseNo.Text = string.Empty;
            txtRationNo.Text = string.Empty;
            txtFamilyStay.Text = string.Empty;
            txtFamilyHeadName.Text = string.Empty;
            txtHusWifeNm.Text = string.Empty;
            txtCast.Text = string.Empty;
            txtFamilyIncome.Text = string.Empty;
            richtxtHouseDetails.Text = string.Empty;
            txtMotherTng.Text = string.Empty;
            //  WaterList.Text = string.Empty;
            WaterList.SelectedIndex = 0;
            txtToilte.Text = string.Empty;
            imgCapture.Text = string.Empty;
            txtSate.Text = string.Empty;
            txtElectricity.Text = string.Empty;
            CookMediumList.SelectedIndex = 0;  //changed into dropdown name
            GenderList.SelectedIndex = 0;
            rdbCameraClick.Checked = true;
            redioBtnNo.Checked = true;
            richtxtHouseBisiness.Text = string.Empty;
            lblFamilyMessages.Text = string.Empty;
   //edited  by archana
            GlobalValues.FamilyMember_PkId = 0;
            lblFamilyMessages.Text = string.Empty;
            btnSave.Text = "Save";

        }

        private void CheckBoxFMIsMember_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxFMIsMember.Checked == true)
            {
                FMDetailsMemberID.Enabled = true;
            }
            else
            {
                FMDetailsMemberID.Enabled = false;
                FMDetailsMemberID.Enabled = false;
            }
        }

        private void redioBtnYes_CheckedChanged_1(object sender, EventArgs e)
        {
            if (redioBtnYes.Checked == true)
            {
                richtxtHouseBisiness.Enabled = true;
            }
            else
            {
                richtxtHouseBisiness.Enabled = false;
            }
        }

        private void redioBtnNo_CheckedChanged_1(object sender, EventArgs e)
        {
            if (redioBtnNo.Checked == true)
            {
                richtxtHouseBisiness.Enabled = false;
                richtxtHouseBisiness.Text = String.Empty;
            }
        }

        private void btnBrowse_Click_1(object sender, EventArgs e)
        {
            try
            {
                ofdPhoto.ShowDialog();
                ofdPhoto.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
                if (ofdPhoto.ShowDialog() == DialogResult.OK)
                {
                    Image img = Image.FromFile(ofdPhoto.FileName);
                    imgCapture.Image = img;

                    //imgCapture.Image = new Bitmap(ofdPhoto.FileName);
                    IsPhotoChanged = true;
                    //imgCapture.Image.Width = 156;
                    //imgCapture.Image.Height = 161;
                }
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void rdbFileUpload_CheckedChanged_1(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            btnContinue.Enabled = false;
            btnCapture.Enabled = false;
            btnVideoSource.Enabled = false;
            btnBrowse.Enabled = true;
            txtFileUpload.Enabled = true;
        }

        private void rdbCameraClick_CheckedChanged_1(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnStop.Enabled = true;
            btnContinue.Enabled = true;
            btnCapture.Enabled = true;
            btnVideoSource.Enabled = true;
            btnBrowse.Enabled = false;
            rdbCameraClick.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AddFamilyMessages.Visible = true; // here i am hiding the AddFamilyMessages
                lblFamilyMessages.Visible = true;
                string strMessage = DoValidations();
                if (strMessage.Length > 0)
                {
                    lblFamilyMessages.Text = strMessage;
                }
                else
                {
                    lblFamilyMessages.Text = string.Empty;
                    MemberFamily objMemberfamily = new MemberFamily();
                    var a = objMemberfamily.MemberFamilyId;
                    if (intMemberOperationType == 0)
                        objMemberfamily.MemberFamilyId = 0;
                    else
                        objMemberfamily.MemberFamilyId = GlobalValues.FamilyMember_PkId;
                    objMemberfamily.Fk_SlumID = Convert.ToInt32(cmbSlum.SelectedValue);
                    objMemberfamily.HouseNumber = txtHouseNo.Text.Trim();
                    objMemberfamily.RationCardNumber = txtRationNo.Text.Trim();
                    objMemberfamily.HowManyYearsTheFamilyIsStaying = Convert.ToInt32(txtFamilyStay.Text.Trim());
                    objMemberfamily.FamilyheadName = txtFamilyHeadName.Text.Trim();
                    objMemberfamily.HusbandWifeName = txtHusWifeNm.Text.Trim();
                    if (redioBtnYes.Checked == true)
                    {
                        objMemberfamily.PartOfHouseForBusiness = true;
                    }

                    objMemberfamily.BusinessDetails = richtxtHouseBisiness.Text.Trim();
                    objMemberfamily.Caste = txtCast.Text.Trim();
                    objMemberfamily.FamilyMounthlyIncome = Convert.ToInt32(txtFamilyIncome.Text);
                    objMemberfamily.HouseDetails = richtxtHouseDetails.Text.Trim();
                    objMemberfamily.MotherToungue = txtMotherTng.Text.Trim();
                    objMemberfamily.State = txtSate.Text.Trim();
                    // objMemberfamily.Water = txtWater.Text.Trim(); // This the Textbox Value
                    objMemberfamily.Water = WaterList.SelectedItem.ToString();
                    objMemberfamily.Toilet = txtToilte.Text.Trim();
                    objMemberfamily.Electricity = txtElectricity.Text.Trim();
                    //objMemberfamily.CookingMedium = txtCooking.Text.Trim();// this is the textbox 
                    objMemberfamily.CookingMedium = (CookMediumList.SelectedItem).ToString();
                    objMemberfamily.Createdby = GlobalValues.User_PkId;
                    objMemberfamily.Updatedby = GlobalValues.User_PkId;


                    int saveResult = objMemberfamily.SaveMemebrFamily();
                    if (saveResult > 0)
                    {
                        GlobalValues.FamilyMember_PkId = saveResult;

                        //saving picture
                        if (imgCapture.Image != null && IsPhotoChanged)
                        {
                            string path2 = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ImageMember.Jpeg";
                            imgCapture.Image.Save(@path2, System.Drawing.Imaging.ImageFormat.Jpeg);
                            FileStream FS = new FileStream(@path2, FileMode.Open, FileAccess.Read); //create a file stream object associate to user selected file 
                            byte[] img = new byte[FS.Length]; //create a byte array with size of user select file stream length
                            FS.Read(img, 0, Convert.ToInt32(FS.Length));//read user selected file stream in to byte array
                            MemberFamily objMem_Photo = new MemberFamily();
                            objMem_Photo.UpdatePhoto(img, GlobalValues.FamilyMember_PkId);
                            objMemberfamily.Photo = img;
                        }

                    }
                    objMemberfamily.MemberFamilyId = GlobalValues.MemberFamilyId;  // we coud not check this condition for update we to write other method
                    SqlConnection con = new SqlConnection(ConnectionString);
                    objMemberfamily.MemberFamilyId = GlobalValues.FamilyMember_PkId;
                    objMemberfamily.MemberId = (FMDetailsMemberID.Text);

                    //  DataSet DsMember = objMemberfamily.AddMembers();
                    con.Open();

                    //  GridColumns Collection of data  to database    

                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        MemberFamily objMemberDetailsGridToData = new MemberFamily();
                        string lngth = (dataGridView1.Rows[i].Cells[6].Value).ToString();
                        objMemberDetailsGridToData.Name = dataGridView1.Rows[i].Cells[0].Value.ToString();
                        objMemberDetailsGridToData.Gender = dataGridView1.Rows[i].Cells[1].Value.ToString();
                        objMemberDetailsGridToData.Age = Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString());
                        objMemberDetailsGridToData.Qualification = dataGridView1.Rows[i].Cells[3].Value.ToString();
                        objMemberDetailsGridToData.IsMember = Convert.ToBoolean(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        objMemberDetailsGridToData.Fk_MemberFamailyID = Convert.ToInt32(GlobalValues.FamilyMember_PkId.ToString());
                        //objMemberDetailsGridToData.MemberId = (dataGridView1.Rows[i].Cells[8].Value).ToString();
                        if (dataGridView1.Rows[i].Cells[8].Value == DBNull.Value)
                        {
                            objMemberDetailsGridToData.MembersID = 0;
                        }
                        else
                        {
                            objMemberDetailsGridToData.MembersID = Convert.ToInt64(dataGridView1.Rows[i].Cells[8].Value);
                        }// here m changing into mf_FkmemberId value to store int
                        //objMemberDetailsGridToData.MembersID =dataGridView1.Rows[i].Cells[8].Value != DBNull.Value ? Convert.ToInt64(dataGridView1.Rows[i].Cells[8].Value) : null as int?;

                        int iResult = 0;
                        if (dataGridView1.Rows[i].Cells[6].Value.ToString() == "")
                        {
                            iResult = objMemberDetailsGridToData.InsertFamilyDetails();
                        }
                        else
                        {
                            objMemberDetailsGridToData.MemeberFamilyDeatilsId = Convert.ToInt64(dataGridView1.Rows[i].Cells[6].Value);
                            iResult = objMemberDetailsGridToData.UpdateMemberFamilyDetailsGrid();
                        }
                    }
                    if (btnSave.Text == "Save")
                    {
                        ClearMasterFeilds();
                        //  btnSave.Enabled = false;    // here m disabling the Save button after click on the save button
                        //edited by archana--start
                        //if (intMemberOperationType == 0)

                        //{
                        lblFamilyMessages.Text = FamilyMessages.SAVED_SUCCESS;
                        lblFamilyMessages.ForeColor = Color.Green;
                        this.Text = Constants.CREATEMEMBER;
                        intMemberOperationType = 2;

                        //}
                        //else
                        //{
                        //    lblFamilyMessages.Text = Messages.UPDATED_SUCCESS;
                        //    lblFamilyMessages.ForeColor = Color.Green;
                        //    this.Text = Constants.UPDATEMEMBER + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                        //}
                    }     //}
                    if (btnSave.Text == "Update")
                    {
                        ClearMasterFeilds();
                        //  btnSave.Enabled = false;    // here m disabling the Save button after click on the save button



                        lblFamilyMessages.Text = Messages.UPDATED_SUCCESS;
                        lblFamilyMessages.ForeColor = Color.Green;
                        this.Text = Constants.UPDATEMEMBER + Messages.HYPHENWITHSPACE + GlobalValues.Member_Name;
                        //edited  by archana--end
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("");
            }
        }

        private void ClearMasterFeilds()
        {
            ClearControls();
            ClearData();
            //for (int i = 0; i < dataGridView1.Rows.Count; )
            //{
            //    dataGridView1.Rows.RemoveAt(i);
            //    i++;
            //while (dataGridView1.Rows.Count == 0)
            //    break;
            //}

            // edited by archana--start
            int rowCount = dataGridView1.Rows.Count;

            for (int i = 0; i < rowCount; i++)
            {
                dataGridView1.Rows.RemoveAt(0);
            }
            //edited by archana--end
            cmbHub.SelectedIndex = 0;
            imgCapture.Image = null;
        }
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }
        private void cmbHub_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbHub.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Country), Convert.ToInt32(cmbHub.SelectedValue), cmbCountry);
            else
                cmbCountry.DataSource = null;
        }

        private void cmbCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbCountry.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.State), Convert.ToInt32(cmbCountry.SelectedValue), cmbState);
            else
                cmbState.DataSource = null;
        }

        private void cmbstate_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch District
            if (cmbState.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.District), Convert.ToInt32(cmbState.SelectedValue), cmbDistrict);
            else
                cmbDistrict.DataSource = null;
        }

        private void cmbDistrict_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Fetch Taluk
            if (cmbDistrict.SelectedIndex > 0)
                FillCombos(Convert.ToInt32(LocationCode.Slum), Convert.ToInt32(cmbDistrict.SelectedValue), cmbSlum);
            else
                cmbSlum.DataSource = null;
        }

        //To Load the Hub and dependencies//
        public void InitializePage()
        {
            webcam = new WebCam();
            webcam.InitializeWebCam(ref imgVideo);

            int CodeType = 0;
            if (GlobalValues.User_Role == UserRoles.SUPERADMIN)
            {
                FillCombos(Convert.ToInt32(LocationCode.Hub), 0, cmbHub);
            }

            if (GlobalValues.User_Role == UserRoles.ADMINLEVEL1)
            {
                CodeType = 1;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL2)
            {
                CodeType = 2;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL3)
            {
                CodeType = 3;
            }
            else if (GlobalValues.User_Role == UserRoles.ADMINLEVEL4)
            {
                CodeType = 4;
            }

            Users objUsers = new Users();
            DataSet dsvalues = objUsers.GetUserRoles(CodeType, GlobalValues.User_CenterId);
            int i;

            foreach (DataRow dr in dsvalues.Tables[0].Rows)
            {
                int Id = Convert.ToInt32(dr["ID"]);
                int ParentId = 0;
                if (dr["PARENTID"] is DBNull)
                {
                    ParentId = 0;
                }
                else
                {
                    ParentId = Convert.ToInt32(dr["PARENTID"]);
                }

                if (dr["CodeType"].ToString() == "1")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Hub), ParentId, cmbHub);
                    cmbHub.SelectedValue = Id;
                    cmbHub.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "2")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.Country), ParentId, cmbCountry);
                    cmbCountry.SelectedValue = Id;
                    cmbCountry.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "3")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.State), ParentId, cmbState);
                    cmbState.SelectedValue = Id;
                    cmbState.Enabled = false;
                }
                if (dr["CodeType"].ToString() == "4")
                {
                    //check for the country type code and populate country with countries
                    FillCombos(Convert.ToInt32(LocationCode.District), ParentId, cmbDistrict);
                    cmbDistrict.SelectedValue = Id;
                    cmbDistrict.Enabled = false;
                }
            }
        }
        //Click on the Insert Button to store the data in Grid
        private void InsertButton_Click(object sender, EventArgs e)
        {
            try
            {
                AddFamilyMessages.Visible = true;
                DeleteGrid.Enabled = true;
                strMessage2 = ValidateBlankMemberId();
                if (strMessage2 != "")
                {
                    strMessage1 = CheckDuplicate();
                }
                //Validating the Duplicate memberID

                strMessage = FamilyDoValidations();

                if (strMessage != "" || strMessage1 != "")
                {
                    AddFamilyMessages.Text = strMessage1 + strMessage;
                    AddFamilyMessages.ForeColor = Color.Red;
                }
                else
                {
                    AddFamilyMessages.Text = string.Empty;
                    MemberFamily objMemberfamily = new MemberFamily();
                    SqlConnection con = new SqlConnection(ConnectionString);
                    objMemberfamily.MemberFamilyId = GlobalValues.MemberFamilyId;
                    objMemberfamily.MemberId = (FMDetailsMemberID.Text);

                    DataTable dt = (DataTable)dataGridView1.DataSource;
                    DataRow dr = dt.NewRow();
                    dr[0] = FMDetailsNm.Text;

                    dr[1] = GenderList.SelectedItem.ToString();
                    dr[2] = FMDetailsAge.Text; // here i add the int32 july3
                    dr[3] = FMDetailsQualifiaction.Text;
                    DataSet DsMember = null;
                    // added by archana --start
                    if (CheckBoxFMIsMember.Checked = true & FMDetailsMemberID.Text != "")
                    {
                        DsMember = objMemberfamily.AddMembers();
                        if (DsMember != null && DsMember.Tables.Count > 0 && DsMember.Tables[0].Rows.Count > 0)
                        {

                            dr[4] = Convert.ToBoolean(CheckBoxFMIsMember.Checked);
                            dr[7] = FMDetailsMemberID.Text;
                            dr[8] = DsMember.Tables[0].Rows[0]["Id"];
                            dt.Rows.Add(dr);
                            dataGridView1.DataSource = dt;
                            AddFamilyMessages.ForeColor = Color.Green;
                            AddFamilyMessages.Text = "Record Inserted Successfully";
                            ClearData();
                        }
                        else
                        {
                            AddFamilyMessages.Text = "MemberId is not valid. Please try again ";
                            ClearData();
                        }


                    }
                    // added by archana--end

             // edited by archana--start

                    else
                    {

                        // FMDetailsMemberID.Enabled = false;
                        dr[4] = false;
                        dr[7] = FMDetailsMemberID.Text;
                        dr[8] = 0;


                        dt.Rows.Add(dr);
                        dataGridView1.DataSource = dt;
                        AddFamilyMessages.ForeColor = Color.Green;
                        AddFamilyMessages.Text = "Record Inserted Successfully";
                        ClearData();
                    }
                    // edited by archana--end
                }
            }
             catch(Exception)
            {
                MessageBox.Show("Please try again");
             }
            

        }
        // validate the Blank MemberId//
        public string ValidateBlankMemberId()
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[7].Value.ToString() == null)
                {

                }
            }
            return "";
        }
        //Checking the Duplicate MemberID
        public string CheckDuplicate()
        {
            result1 = "";
            for (i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[7].Value.ToString() == FMDetailsMemberID.Text)
                {
                    AddFamilyMessages.Text = FamilyMessages.MEMBERIDDUPLICATE + FamilyMessages.COMMAWITHSPACE;
                    result1 = AddFamilyMessages.Text;
                    break;
                }
            }
            return result1;
        }

        //dataGridView1 RowHeaderMouseClick Event
        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            InsertButton.Enabled = false;
            UpdatedataGrid.Enabled = true;
            DeleteGrid.Enabled = true;
            rowindex = e.RowIndex;
            FMDetailsNm.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            GenderList.SelectedItem = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();  //This is the Dropdownlist Value
            FMDetailsAge.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            FMDetailsQualifiaction.Text = (dataGridView1.Rows[e.RowIndex].Cells[3].Value).ToString();
            CheckBoxFMIsMember.Checked = Convert.ToBoolean(dataGridView1.Rows[e.RowIndex].Cells[4].Value);
            FMDetailsMemberID.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString();
        }
        //Update Gridview Record 
        private void UpdatedataGrid_Click(object sender, EventArgs e)
        {
            AddFamilyMessages.Visible = true;
            MemberFamily objMemberfamily = new MemberFamily();
            objMemberfamily.MemberId = (FMDetailsMemberID.Text);
            //edited by archana--start
            DataSet DsMember = null; 
            if (FMDetailsMemberID.Text != "")
            {
                DsMember = objMemberfamily.AddMembers();
            }
            string strMessage = FamilyDoValidations();

            if (strMessage.Length > 0)
            {
                AddFamilyMessages.Text = strMessage;
            }
            else
            {
                AddFamilyMessages.Text = string.Empty;
                SqlConnection con = new SqlConnection(ConnectionString);

                if (FMDetailsNm.Text != "" && GenderList.SelectedItem != "")
                {
                    dataGridView1.Rows[rowindex].Cells[0].Value = FMDetailsNm.Text;
                    dataGridView1.Rows[rowindex].Cells[1].Value = GenderList.SelectedItem;  // This is the Drop Down selected value
                    dataGridView1.Rows[rowindex].Cells[2].Value = FMDetailsAge.Text;
                    dataGridView1.Rows[rowindex].Cells[3].Value = FMDetailsQualifiaction.Text;
                    dataGridView1.Rows[rowindex].Cells[4].Value = CheckBoxFMIsMember.Checked;
                    dataGridView1.Rows[rowindex].Cells[7].Value = FMDetailsMemberID.Text;
                    if (DsMember != null && DsMember.Tables.Count > 0 && DsMember.Tables[0].Rows.Count > 0)
                    {
                        dataGridView1.Rows[rowindex].Cells[8].Value = DsMember.Tables[0].Rows[0]["Id"].ToString();
                    }
                    else
                    {
                        dataGridView1.Rows[rowindex].Cells[8].Value = 0;
                    }
                    ClearData();
                    InsertButton.Enabled = true;
                    UpdatedataGrid.Enabled = false;
                    DeleteGrid.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Please Select Record to Update");
                }
            }
        }
        //edited by archan--end
        //To Delete the dataGridRow
        private void DeleteGrid_Click(object sender, EventArgs e)
        {
            try
            {
                int iResult;
                DataSet dsDeleteStatus = null;
                MemberFamily obj = new MemberFamily();
                SqlConnection con = new SqlConnection(ConnectionString);
                if (this.dataGridView1.SelectedRows.Count > 0)
                {
                    int x = this.rowindex;
                    obj.MemeberFamilyDeatilsId = Convert.ToInt64(dataGridView1.Rows[x].Cells[6].Value);
                    //string str = "delete from MemberFamilyDetails where Id=@Id";
                    //SqlCommand cmd = new SqlCommand(str, con);
                    //cmd.Parameters.AddWithValue("@Id", obj.MemeberFamilyDeatilsId);
                    //con.Open();
                    //SqlDataReader rdr;
                    //rdr = cmd.ExecuteReader();
                    //con.Close();
                    //MessageBox.Show("Record Deleted");

                    //dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
                    dsDeleteStatus = obj.DeleteMemberfamilydetails(GlobalValues.User_PkId);
                   
                            MessageBox.Show("Record Deleted");
                            
                        
                   
                    ClearData();
                    dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
                    InsertButton.Enabled = true;
                    UpdatedataGrid.Enabled = true;
                    DeleteGrid.Enabled = false;
                    obj.MemberFamilyId = 0;
                }
            }
            catch (Exception)
            {

            }
        }

        //The Salary Text box Accept only numbers       
        private void txtFamilyStay_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtFamilyStay.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtFamilyStay.Text.Remove(txtFamilyStay.Text.Length - 1);
            }
        }

        // The Dow Down List change
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Object s = CookMediumList.SelectedItem;
        }

        private void WaterList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Object s1 = WaterList.SelectedItem;
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Object s1 = GenderList.SelectedItem;
        }

        private void FMDetailsAge_TextChanged(object sender, EventArgs e)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(FMDetailsAge.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
            }
        }

        //For the Income TextBox only enter the Number
        private void txtIncome_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
                //if (!char.IsControl(e.KeyChar)
                //        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                //{
                //    e.Handled = true;
                //}

                //// only allow one decimal point
                //if (e.KeyChar == '.'
                //    && (sender as TextBox).Text.IndexOf('.') > -1)
                //{
                //    e.Handled = true;
                //}
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void txtFamilyStay_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
                //if (!char.IsControl(e.KeyChar)
                //        && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                //{
                //    e.Handled = true;
                //}
               
                //// only allow one decimal point
                //if (e.KeyChar == '.'
                //    && (sender as TextBox).Text.IndexOf('.') > -1)
                //{
                //    e.Handled = true;
                //}
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }

        private void FMDetailsAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

                //// only allow one decimal point
                //if (e.KeyChar == '.'
                //    && (sender as TextBox).Text.IndexOf('.') > -1)
                //{
                //    e.Handled = true;
                //}
            }
            catch (Exception ex)
            {
                Messages.ShowMessage(Messages.ERROR_PROCESSING, Messages.MsgType.failure, ex);
            }
        }


    }
}
