﻿namespace SPARC
{
    partial class GroupTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpSavRep = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblmsg = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dgvSavRe = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fk_SlumId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.New = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btndelete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Slum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Groups = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbgrp = new System.Windows.Forms.ComboBox();
            this.Group = new System.Windows.Forms.Label();
            this.lblMessages = new System.Windows.Forms.Label();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSavReSave = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbTypeSavRe = new System.Windows.Forms.ComboBox();
            this.txtSavReTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblAccDate = new System.Windows.Forms.Label();
            this.dtpAccountDate = new System.Windows.Forms.DateTimePicker();
            this.grpSavRep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSavRe)).BeginInit();
            this.SuspendLayout();
            // 
            // grpSavRep
            // 
            this.grpSavRep.Controls.Add(this.label1);
            this.grpSavRep.Controls.Add(this.textBox1);
            this.grpSavRep.Controls.Add(this.lblmsg);
            this.grpSavRep.Controls.Add(this.button2);
            this.grpSavRep.Controls.Add(this.dgvSavRe);
            this.grpSavRep.Controls.Add(this.button1);
            this.grpSavRep.Controls.Add(this.cmbgrp);
            this.grpSavRep.Controls.Add(this.Group);
            this.grpSavRep.Controls.Add(this.lblMessages);
            this.grpSavRep.Controls.Add(this.cmbslum);
            this.grpSavRep.Controls.Add(this.cmbdistrict);
            this.grpSavRep.Controls.Add(this.label7);
            this.grpSavRep.Controls.Add(this.label5);
            this.grpSavRep.Controls.Add(this.cmbcountry);
            this.grpSavRep.Controls.Add(this.cmbstate);
            this.grpSavRep.Controls.Add(this.cmbHub);
            this.grpSavRep.Controls.Add(this.label6);
            this.grpSavRep.Controls.Add(this.label8);
            this.grpSavRep.Controls.Add(this.label9);
            this.grpSavRep.Controls.Add(this.btnSavReSave);
            this.grpSavRep.Controls.Add(this.btnAdd);
            this.grpSavRep.Controls.Add(this.cmbTypeSavRe);
            this.grpSavRep.Controls.Add(this.txtSavReTotal);
            this.grpSavRep.Controls.Add(this.lblTotal);
            this.grpSavRep.Controls.Add(this.lblType);
            this.grpSavRep.Location = new System.Drawing.Point(25, 54);
            this.grpSavRep.Name = "grpSavRep";
            this.grpSavRep.Size = new System.Drawing.Size(785, 383);
            this.grpSavRep.TabIndex = 6;
            this.grpSavRep.TabStop = false;
            this.grpSavRep.Text = "Savings / Repayment / New Passbook";
            this.grpSavRep.Enter += new System.EventHandler(this.grpSavRep_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(484, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 156;
            this.label1.Text = "Remarks";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(595, 92);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 155;
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Location = new System.Drawing.Point(105, 364);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 13);
            this.lblmsg.TabIndex = 154;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(348, 345);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 153;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgvSavRe
            // 
            this.dgvSavRe.AllowUserToAddRows = false;
            this.dgvSavRe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSavRe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSavRe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Type,
            this.Amount,
            this.GroupName,
            this.Fk_SlumId,
            this.New,
            this.Id,
            this.btndelete,
            this.Remarks,
            this.Slum,
            this.Groups});
            this.dgvSavRe.Location = new System.Drawing.Point(26, 178);
            this.dgvSavRe.Name = "dgvSavRe";
            this.dgvSavRe.ReadOnly = true;
            this.dgvSavRe.Size = new System.Drawing.Size(679, 150);
            this.dgvSavRe.TabIndex = 152;
            this.dgvSavRe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSavRe_CellClick);
            this.dgvSavRe.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSavRe_CellContentClick);
            this.dgvSavRe.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSavRe_CellContentDoubleClick);
            this.dgvSavRe.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSavRe_RowHeaderMouseClick_1);
            this.dgvSavRe.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSavRe_RowHeaderMouseDoubleClick);
            // 
            // Date
            // 
            this.Date.DataPropertyName = "TransactionDate";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "Group_Name";
            this.GroupName.HeaderText = "GroupName";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            // 
            // Fk_SlumId
            // 
            this.Fk_SlumId.DataPropertyName = "Name";
            this.Fk_SlumId.HeaderText = "SlumName";
            this.Fk_SlumId.Name = "Fk_SlumId";
            this.Fk_SlumId.ReadOnly = true;
            // 
            // New
            // 
            this.New.DataPropertyName = "New";
            this.New.HeaderText = "New";
            this.New.Name = "New";
            this.New.ReadOnly = true;
            this.New.Visible = false;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // btndelete
            // 
            this.btndelete.HeaderText = "Delete";
            this.btndelete.Name = "btndelete";
            this.btndelete.ReadOnly = true;
            this.btndelete.Text = "Delete";
            this.btndelete.UseColumnTextForLinkValue = true;
            this.btndelete.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.Name = "Remarks";
            this.Remarks.ReadOnly = true;
            // 
            // Slum
            // 
            this.Slum.DataPropertyName = "slumid";
            this.Slum.HeaderText = "Slum";
            this.Slum.Name = "Slum";
            this.Slum.ReadOnly = true;
            this.Slum.Visible = false;
            // 
            // Groups
            // 
            this.Groups.DataPropertyName = "groupid";
            this.Groups.HeaderText = "Group";
            this.Groups.Name = "Groups";
            this.Groups.ReadOnly = true;
            this.Groups.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(445, 345);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 151;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbgrp
            // 
            this.cmbgrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbgrp.FormattingEnabled = true;
            this.cmbgrp.Location = new System.Drawing.Point(595, 61);
            this.cmbgrp.Name = "cmbgrp";
            this.cmbgrp.Size = new System.Drawing.Size(121, 21);
            this.cmbgrp.TabIndex = 150;
            this.cmbgrp.SelectedIndexChanged += new System.EventHandler(this.cmbgrp_SelectedIndexChanged);
            // 
            // Group
            // 
            this.Group.AutoSize = true;
            this.Group.Location = new System.Drawing.Point(487, 61);
            this.Group.Name = "Group";
            this.Group.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Group.Size = new System.Drawing.Size(36, 13);
            this.Group.TabIndex = 149;
            this.Group.Text = "Group";
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Location = new System.Drawing.Point(18, 136);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 148;
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(330, 53);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(144, 21);
            this.cmbslum.TabIndex = 6;
            this.cmbslum.SelectedIndexChanged += new System.EventHandler(this.cmbslum_SelectedIndexChanged);
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(86, 51);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(131, 21);
            this.cmbdistrict.TabIndex = 5;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(226, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 144;
            this.label7.Text = "Slum/Settlement";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 143;
            this.label5.Text = "District/City";
            // 
            // cmbcountry
            // 
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(330, 19);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(144, 21);
            this.cmbcountry.TabIndex = 3;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(594, 22);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(122, 21);
            this.cmbstate.TabIndex = 4;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(86, 22);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(131, 21);
            this.cmbHub.TabIndex = 2;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 138;
            this.label6.Text = "Hub";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(484, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 139;
            this.label8.Text = "State/Province";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(226, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 137;
            this.label9.Text = "Country";
            // 
            // btnSavReSave
            // 
            this.btnSavReSave.Location = new System.Drawing.Point(236, 345);
            this.btnSavReSave.Name = "btnSavReSave";
            this.btnSavReSave.Size = new System.Drawing.Size(75, 23);
            this.btnSavReSave.TabIndex = 10;
            this.btnSavReSave.Text = "Save";
            this.btnSavReSave.UseVisualStyleBackColor = true;
            this.btnSavReSave.Click += new System.EventHandler(this.btnSavReSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(387, 126);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbTypeSavRe
            // 
            this.cmbTypeSavRe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeSavRe.FormattingEnabled = true;
            this.cmbTypeSavRe.Items.AddRange(new object[] {
            "Savings",
            "Repayment",
            "UPF",
            "Withdrawal"});
            this.cmbTypeSavRe.Location = new System.Drawing.Point(86, 84);
            this.cmbTypeSavRe.Name = "cmbTypeSavRe";
            this.cmbTypeSavRe.Size = new System.Drawing.Size(131, 21);
            this.cmbTypeSavRe.TabIndex = 7;
            // 
            // txtSavReTotal
            // 
            this.txtSavReTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSavReTotal.Location = new System.Drawing.Point(330, 89);
            this.txtSavReTotal.Name = "txtSavReTotal";
            this.txtSavReTotal.ShortcutsEnabled = false;
            this.txtSavReTotal.Size = new System.Drawing.Size(142, 20);
            this.txtSavReTotal.TabIndex = 8;
            this.txtSavReTotal.TextChanged += new System.EventHandler(this.txtSavReTotal_TextChanged);
            this.txtSavReTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSavReTotal_KeyPress);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(226, 88);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(43, 13);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "Amount";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(18, 87);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 13);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // lblAccDate
            // 
            this.lblAccDate.AutoSize = true;
            this.lblAccDate.Location = new System.Drawing.Point(127, 22);
            this.lblAccDate.Name = "lblAccDate";
            this.lblAccDate.Size = new System.Drawing.Size(73, 13);
            this.lblAccDate.TabIndex = 4;
            this.lblAccDate.Text = "Account Date";
            // 
            // dtpAccountDate
            // 
            this.dtpAccountDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpAccountDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAccountDate.Location = new System.Drawing.Point(244, 16);
            this.dtpAccountDate.Name = "dtpAccountDate";
            this.dtpAccountDate.Size = new System.Drawing.Size(121, 20);
            this.dtpAccountDate.TabIndex = 22;
            this.dtpAccountDate.ValueChanged += new System.EventHandler(this.dtpAccountDate_ValueChanged_1);
            // 
            // GroupTransaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 500);
            this.ControlBox = false;
            this.Controls.Add(this.dtpAccountDate);
            this.Controls.Add(this.grpSavRep);
            this.Controls.Add(this.lblAccDate);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GroupTransaction";
            this.Text = "GroupTransaction";
            this.Load += new System.EventHandler(this.GroupTransaction_Load);
            this.grpSavRep.ResumeLayout(false);
            this.grpSavRep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSavRe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSavRep;
        private System.Windows.Forms.ComboBox cmbgrp;
        private System.Windows.Forms.Label Group;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSavReSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbTypeSavRe;
        private System.Windows.Forms.TextBox txtSavReTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblAccDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvSavRe;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.DateTimePicker dtpAccountDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fk_SlumId;
        private System.Windows.Forms.DataGridViewTextBoxColumn New;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewLinkColumn btndelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Slum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Groups;

    }
}