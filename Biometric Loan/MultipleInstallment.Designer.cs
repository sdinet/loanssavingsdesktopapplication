﻿namespace SPARC
{
    partial class MultipleInstallment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbLoanType = new System.Windows.Forms.ComboBox();
            this.lblLoanType = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbcountry = new System.Windows.Forms.ComboBox();
            this.cmbslum = new System.Windows.Forms.ComboBox();
            this.cmbdistrict = new System.Windows.Forms.ComboBox();
            this.cmbstate = new System.Windows.Forms.ComboBox();
            this.cmbHub = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPassbookNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMemberName = new System.Windows.Forms.TextBox();
            this.lblMemberName = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMessages = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRemainingPrincipal = new System.Windows.Forms.Label();
            this.lblRemainingPrincipalVal = new System.Windows.Forms.Label();
            this.lblPendingChargeVal = new System.Windows.Forms.Label();
            this.lblLoanAmt = new System.Windows.Forms.Label();
            this.lblPendingCharge = new System.Windows.Forms.Label();
            this.lblLoanAmtVal = new System.Windows.Forms.Label();
            this.lblPendingPrincipalVal = new System.Windows.Forms.Label();
            this.lblDispersalBal = new System.Windows.Forms.Label();
            this.lblPendingPrincipal = new System.Windows.Forms.Label();
            this.lblDispersalBalVal = new System.Windows.Forms.Label();
            this.radioChargesPartwise = new System.Windows.Forms.RadioButton();
            this.radioChargesFull = new System.Windows.Forms.RadioButton();
            this.lblEMIChargesVal = new System.Windows.Forms.Label();
            this.grpdivision = new System.Windows.Forms.GroupBox();
            this.lblCloseBrace = new System.Windows.Forms.Label();
            this.lblOpenBrace = new System.Windows.Forms.Label();
            this.txtChargeAmt = new System.Windows.Forms.TextBox();
            this.lblChargeAmt = new System.Windows.Forms.Label();
            this.txtPrincipalAmt = new System.Windows.Forms.TextBox();
            this.lblPrincipalAmt = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransDate = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblEMIStartDateVal = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MemberName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PassbookNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrincipalPart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InterestPart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewLinkColumn();
            this.fk_MemberId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Accountno2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpdivision.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.cmbLoanType);
            this.groupBox2.Controls.Add(this.lblLoanType);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmbcountry);
            this.groupBox2.Controls.Add(this.cmbslum);
            this.groupBox2.Controls.Add(this.cmbdistrict);
            this.groupBox2.Controls.Add(this.cmbstate);
            this.groupBox2.Controls.Add(this.cmbHub);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtPassbookNo);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(761, 120);
            this.groupBox2.TabIndex = 141;
            this.groupBox2.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(546, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 20);
            this.label13.TabIndex = 141;
            this.label13.Text = "*";
            // 
            // cmbLoanType
            // 
            this.cmbLoanType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoanType.FormattingEnabled = true;
            this.cmbLoanType.Location = new System.Drawing.Point(593, 52);
            this.cmbLoanType.Name = "cmbLoanType";
            this.cmbLoanType.Size = new System.Drawing.Size(144, 21);
            this.cmbLoanType.TabIndex = 5;
            // 
            // lblLoanType
            // 
            this.lblLoanType.AutoSize = true;
            this.lblLoanType.Location = new System.Drawing.Point(496, 57);
            this.lblLoanType.Name = "lblLoanType";
            this.lblLoanType.Size = new System.Drawing.Size(55, 13);
            this.lblLoanType.TabIndex = 139;
            this.lblLoanType.Text = "LoanType";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(78, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 138;
            this.label9.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(305, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 20);
            this.label3.TabIndex = 137;
            this.label3.Text = "*";
            // 
            // cmbcountry
            // 
            this.cmbcountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcountry.FormattingEnabled = true;
            this.cmbcountry.Location = new System.Drawing.Point(332, 22);
            this.cmbcountry.Name = "cmbcountry";
            this.cmbcountry.Size = new System.Drawing.Size(144, 21);
            this.cmbcountry.TabIndex = 1;
            this.cmbcountry.SelectedIndexChanged += new System.EventHandler(this.cmbcountry_SelectedIndexChanged);
            // 
            // cmbslum
            // 
            this.cmbslum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbslum.FormattingEnabled = true;
            this.cmbslum.Location = new System.Drawing.Point(332, 49);
            this.cmbslum.Name = "cmbslum";
            this.cmbslum.Size = new System.Drawing.Size(144, 21);
            this.cmbslum.TabIndex = 4;
            // 
            // cmbdistrict
            // 
            this.cmbdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdistrict.FormattingEnabled = true;
            this.cmbdistrict.Location = new System.Drawing.Point(78, 49);
            this.cmbdistrict.Name = "cmbdistrict";
            this.cmbdistrict.Size = new System.Drawing.Size(138, 21);
            this.cmbdistrict.TabIndex = 3;
            this.cmbdistrict.SelectedIndexChanged += new System.EventHandler(this.cmbdistrict_SelectedIndexChanged);
            // 
            // cmbstate
            // 
            this.cmbstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbstate.FormattingEnabled = true;
            this.cmbstate.Location = new System.Drawing.Point(593, 22);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Size = new System.Drawing.Size(144, 21);
            this.cmbstate.TabIndex = 2;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // cmbHub
            // 
            this.cmbHub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHub.FormattingEnabled = true;
            this.cmbHub.Location = new System.Drawing.Point(78, 17);
            this.cmbHub.Name = "cmbHub";
            this.cmbHub.Size = new System.Drawing.Size(138, 21);
            this.cmbHub.TabIndex = 0;
            this.cmbHub.SelectedIndexChanged += new System.EventHandler(this.cmbHub_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 129;
            this.label6.Text = "Hub";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(493, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 131;
            this.label8.Text = "State/Province";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(221, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 130;
            this.label7.Text = "Slum/Settlement";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 128;
            this.label5.Text = "District/City";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(221, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 125;
            this.label4.Text = "Country";
            // 
            // txtPassbookNo
            // 
            this.txtPassbookNo.Location = new System.Drawing.Point(97, 88);
            this.txtPassbookNo.MaxLength = 20;
            this.txtPassbookNo.Name = "txtPassbookNo";
            this.txtPassbookNo.Size = new System.Drawing.Size(144, 20);
            this.txtPassbookNo.TabIndex = 6;
            this.txtPassbookNo.TextChanged += new System.EventHandler(this.txtPassbookNo_TextChanged);
            this.txtPassbookNo.Leave += new System.EventHandler(this.txtPassbookNo_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 109;
            this.label1.Text = "Passbook No.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(331, 242);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 20);
            this.label10.TabIndex = 139;
            this.label10.Text = "*";
            // 
            // txtMemberName
            // 
            this.txtMemberName.Location = new System.Drawing.Point(126, 210);
            this.txtMemberName.Name = "txtMemberName";
            this.txtMemberName.ReadOnly = true;
            this.txtMemberName.Size = new System.Drawing.Size(144, 20);
            this.txtMemberName.TabIndex = 121;
            // 
            // lblMemberName
            // 
            this.lblMemberName.AutoSize = true;
            this.lblMemberName.Location = new System.Drawing.Point(26, 217);
            this.lblMemberName.Name = "lblMemberName";
            this.lblMemberName.Size = new System.Drawing.Size(76, 13);
            this.lblMemberName.TabIndex = 120;
            this.lblMemberName.Text = "Member Name";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(353, 241);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(138, 21);
            this.cmbAgentName.TabIndex = 11;
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Location = new System.Drawing.Point(261, 249);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(71, 13);
            this.lblAgentName.TabIndex = 113;
            this.lblAgentName.Text = "Leader Name";
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Location = new System.Drawing.Point(84, 15);
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.ReadOnly = true;
            this.txtAccountNo.Size = new System.Drawing.Size(144, 20);
            this.txtAccountNo.TabIndex = 111;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 110;
            this.label2.Text = "Account No.";
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessages.Location = new System.Drawing.Point(37, 358);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(0, 13);
            this.lblMessages.TabIndex = 144;
            this.lblMessages.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(398, 599);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 143;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(302, 599);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 142;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblRemainingPrincipal);
            this.groupBox1.Controls.Add(this.lblRemainingPrincipalVal);
            this.groupBox1.Controls.Add(this.lblPendingChargeVal);
            this.groupBox1.Controls.Add(this.lblLoanAmt);
            this.groupBox1.Controls.Add(this.lblPendingCharge);
            this.groupBox1.Controls.Add(this.lblLoanAmtVal);
            this.groupBox1.Controls.Add(this.lblPendingPrincipalVal);
            this.groupBox1.Controls.Add(this.lblDispersalBal);
            this.groupBox1.Controls.Add(this.lblPendingPrincipal);
            this.groupBox1.Controls.Add(this.lblDispersalBalVal);
            this.groupBox1.Controls.Add(this.txtAccountNo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(761, 72);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            // 
            // lblRemainingPrincipal
            // 
            this.lblRemainingPrincipal.AutoSize = true;
            this.lblRemainingPrincipal.Location = new System.Drawing.Point(502, 48);
            this.lblRemainingPrincipal.Name = "lblRemainingPrincipal";
            this.lblRemainingPrincipal.Size = new System.Drawing.Size(100, 13);
            this.lblRemainingPrincipal.TabIndex = 169;
            this.lblRemainingPrincipal.Text = "Remaining Principal";
            // 
            // lblRemainingPrincipalVal
            // 
            this.lblRemainingPrincipalVal.AutoSize = true;
            this.lblRemainingPrincipalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainingPrincipalVal.ForeColor = System.Drawing.Color.Black;
            this.lblRemainingPrincipalVal.Location = new System.Drawing.Point(603, 48);
            this.lblRemainingPrincipalVal.Name = "lblRemainingPrincipalVal";
            this.lblRemainingPrincipalVal.Size = new System.Drawing.Size(15, 15);
            this.lblRemainingPrincipalVal.TabIndex = 170;
            this.lblRemainingPrincipalVal.Text = "0";
            // 
            // lblPendingChargeVal
            // 
            this.lblPendingChargeVal.AutoSize = true;
            this.lblPendingChargeVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPendingChargeVal.ForeColor = System.Drawing.Color.Black;
            this.lblPendingChargeVal.Location = new System.Drawing.Point(406, 46);
            this.lblPendingChargeVal.Name = "lblPendingChargeVal";
            this.lblPendingChargeVal.Size = new System.Drawing.Size(15, 15);
            this.lblPendingChargeVal.TabIndex = 174;
            this.lblPendingChargeVal.Text = "0";
            // 
            // lblLoanAmt
            // 
            this.lblLoanAmt.AutoSize = true;
            this.lblLoanAmt.Location = new System.Drawing.Point(264, 16);
            this.lblLoanAmt.Name = "lblLoanAmt";
            this.lblLoanAmt.Size = new System.Drawing.Size(70, 13);
            this.lblLoanAmt.TabIndex = 151;
            this.lblLoanAmt.Text = "Loan Amount";
            // 
            // lblPendingCharge
            // 
            this.lblPendingCharge.AutoSize = true;
            this.lblPendingCharge.Location = new System.Drawing.Point(264, 46);
            this.lblPendingCharge.Name = "lblPendingCharge";
            this.lblPendingCharge.Size = new System.Drawing.Size(141, 13);
            this.lblPendingCharge.TabIndex = 173;
            this.lblPendingCharge.Text = "Pending Installment Charges";
            // 
            // lblLoanAmtVal
            // 
            this.lblLoanAmtVal.AutoSize = true;
            this.lblLoanAmtVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAmtVal.ForeColor = System.Drawing.Color.Black;
            this.lblLoanAmtVal.Location = new System.Drawing.Point(338, 14);
            this.lblLoanAmtVal.Name = "lblLoanAmtVal";
            this.lblLoanAmtVal.Size = new System.Drawing.Size(15, 15);
            this.lblLoanAmtVal.TabIndex = 152;
            this.lblLoanAmtVal.Text = "0";
            // 
            // lblPendingPrincipalVal
            // 
            this.lblPendingPrincipalVal.AutoSize = true;
            this.lblPendingPrincipalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPendingPrincipalVal.ForeColor = System.Drawing.Color.Black;
            this.lblPendingPrincipalVal.Location = new System.Drawing.Point(160, 44);
            this.lblPendingPrincipalVal.Name = "lblPendingPrincipalVal";
            this.lblPendingPrincipalVal.Size = new System.Drawing.Size(15, 15);
            this.lblPendingPrincipalVal.TabIndex = 172;
            this.lblPendingPrincipalVal.Text = "0";
            // 
            // lblDispersalBal
            // 
            this.lblDispersalBal.AutoSize = true;
            this.lblDispersalBal.Location = new System.Drawing.Point(502, 18);
            this.lblDispersalBal.Name = "lblDispersalBal";
            this.lblDispersalBal.Size = new System.Drawing.Size(92, 13);
            this.lblDispersalBal.TabIndex = 149;
            this.lblDispersalBal.Text = "Pending Dispersal";
            // 
            // lblPendingPrincipal
            // 
            this.lblPendingPrincipal.AutoSize = true;
            this.lblPendingPrincipal.Location = new System.Drawing.Point(16, 44);
            this.lblPendingPrincipal.Name = "lblPendingPrincipal";
            this.lblPendingPrincipal.Size = new System.Drawing.Size(142, 13);
            this.lblPendingPrincipal.TabIndex = 171;
            this.lblPendingPrincipal.Text = "Pending Installment Principal";
            // 
            // lblDispersalBalVal
            // 
            this.lblDispersalBalVal.AutoSize = true;
            this.lblDispersalBalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispersalBalVal.ForeColor = System.Drawing.Color.Black;
            this.lblDispersalBalVal.Location = new System.Drawing.Point(602, 16);
            this.lblDispersalBalVal.Name = "lblDispersalBalVal";
            this.lblDispersalBalVal.Size = new System.Drawing.Size(15, 15);
            this.lblDispersalBalVal.TabIndex = 150;
            this.lblDispersalBalVal.Text = "0";
            // 
            // radioChargesPartwise
            // 
            this.radioChargesPartwise.AutoSize = true;
            this.radioChargesPartwise.Location = new System.Drawing.Point(508, 217);
            this.radioChargesPartwise.Name = "radioChargesPartwise";
            this.radioChargesPartwise.Size = new System.Drawing.Size(150, 17);
            this.radioChargesPartwise.TabIndex = 9;
            this.radioChargesPartwise.Text = "Pay Charges In Installment";
            this.radioChargesPartwise.UseVisualStyleBackColor = true;
            this.radioChargesPartwise.CheckedChanged += new System.EventHandler(this.radioChargesPartwise_CheckedChanged);
            // 
            // radioChargesFull
            // 
            this.radioChargesFull.AutoSize = true;
            this.radioChargesFull.Checked = true;
            this.radioChargesFull.Location = new System.Drawing.Point(298, 215);
            this.radioChargesFull.Name = "radioChargesFull";
            this.radioChargesFull.Size = new System.Drawing.Size(134, 17);
            this.radioChargesFull.TabIndex = 8;
            this.radioChargesFull.TabStop = true;
            this.radioChargesFull.Text = "Pay Charges One Time";
            this.radioChargesFull.UseVisualStyleBackColor = true;
            this.radioChargesFull.CheckedChanged += new System.EventHandler(this.radioChargesFull_CheckedChanged);
            // 
            // lblEMIChargesVal
            // 
            this.lblEMIChargesVal.AutoSize = true;
            this.lblEMIChargesVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEMIChargesVal.ForeColor = System.Drawing.Color.Black;
            this.lblEMIChargesVal.Location = new System.Drawing.Point(644, 331);
            this.lblEMIChargesVal.Name = "lblEMIChargesVal";
            this.lblEMIChargesVal.Size = new System.Drawing.Size(15, 15);
            this.lblEMIChargesVal.TabIndex = 182;
            this.lblEMIChargesVal.Text = "0";
            this.lblEMIChargesVal.Visible = false;
            // 
            // grpdivision
            // 
            this.grpdivision.Controls.Add(this.lblCloseBrace);
            this.grpdivision.Controls.Add(this.lblOpenBrace);
            this.grpdivision.Controls.Add(this.txtChargeAmt);
            this.grpdivision.Controls.Add(this.lblChargeAmt);
            this.grpdivision.Controls.Add(this.txtPrincipalAmt);
            this.grpdivision.Controls.Add(this.lblPrincipalAmt);
            this.grpdivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpdivision.Location = new System.Drawing.Point(249, 271);
            this.grpdivision.Name = "grpdivision";
            this.grpdivision.Size = new System.Drawing.Size(392, 38);
            this.grpdivision.TabIndex = 183;
            this.grpdivision.TabStop = false;
            // 
            // lblCloseBrace
            // 
            this.lblCloseBrace.AutoSize = true;
            this.lblCloseBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloseBrace.Location = new System.Drawing.Point(376, 12);
            this.lblCloseBrace.Name = "lblCloseBrace";
            this.lblCloseBrace.Size = new System.Drawing.Size(11, 15);
            this.lblCloseBrace.TabIndex = 164;
            this.lblCloseBrace.Text = ")";
            // 
            // lblOpenBrace
            // 
            this.lblOpenBrace.AutoSize = true;
            this.lblOpenBrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpenBrace.Location = new System.Drawing.Point(6, 12);
            this.lblOpenBrace.Name = "lblOpenBrace";
            this.lblOpenBrace.Size = new System.Drawing.Size(11, 15);
            this.lblOpenBrace.TabIndex = 163;
            this.lblOpenBrace.Text = "(";
            // 
            // txtChargeAmt
            // 
            this.txtChargeAmt.Location = new System.Drawing.Point(272, 12);
            this.txtChargeAmt.MaxLength = 10;
            this.txtChargeAmt.Name = "txtChargeAmt";
            this.txtChargeAmt.Size = new System.Drawing.Size(100, 20);
            this.txtChargeAmt.TabIndex = 160;
            this.txtChargeAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtChargeAmt_KeyPress);
            // 
            // lblChargeAmt
            // 
            this.lblChargeAmt.AutoSize = true;
            this.lblChargeAmt.Location = new System.Drawing.Point(204, 16);
            this.lblChargeAmt.Name = "lblChargeAmt";
            this.lblChargeAmt.Size = new System.Drawing.Size(62, 13);
            this.lblChargeAmt.TabIndex = 162;
            this.lblChargeAmt.Text = "Charge Amt";
            // 
            // txtPrincipalAmt
            // 
            this.txtPrincipalAmt.Location = new System.Drawing.Point(101, 12);
            this.txtPrincipalAmt.MaxLength = 10;
            this.txtPrincipalAmt.Name = "txtPrincipalAmt";
            this.txtPrincipalAmt.Size = new System.Drawing.Size(100, 20);
            this.txtPrincipalAmt.TabIndex = 159;
            this.txtPrincipalAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrincipalAmt_KeyPress);
            // 
            // lblPrincipalAmt
            // 
            this.lblPrincipalAmt.AutoSize = true;
            this.lblPrincipalAmt.Location = new System.Drawing.Point(18, 16);
            this.lblPrincipalAmt.Name = "lblPrincipalAmt";
            this.lblPrincipalAmt.Size = new System.Drawing.Size(68, 13);
            this.lblPrincipalAmt.TabIndex = 161;
            this.lblPrincipalAmt.Text = "Principal Amt";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(573, 331);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 181;
            this.label11.Text = "EMI Charges";
            this.label11.Visible = false;
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(140, 284);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 12;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(433, 333);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 180;
            this.label12.Text = "Messages";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label12.Visible = false;
            // 
            // dtpOpenDate
            // 
            this.dtpOpenDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenDate.Location = new System.Drawing.Point(140, 244);
            this.dtpOpenDate.Name = "dtpOpenDate";
            this.dtpOpenDate.Size = new System.Drawing.Size(105, 20);
            this.dtpOpenDate.TabIndex = 10;
            // 
            // lblTransDate
            // 
            this.lblTransDate.AutoSize = true;
            this.lblTransDate.Location = new System.Drawing.Point(27, 244);
            this.lblTransDate.Name = "lblTransDate";
            this.lblTransDate.Size = new System.Drawing.Size(89, 13);
            this.lblTransDate.TabIndex = 178;
            this.lblTransDate.Text = "Transaction Date";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(27, 287);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(65, 13);
            this.lblAmount.TabIndex = 176;
            this.lblAmount.Text = "Amount (Rs)";
            // 
            // lblEMIStartDateVal
            // 
            this.lblEMIStartDateVal.AutoSize = true;
            this.lblEMIStartDateVal.Location = new System.Drawing.Point(707, 307);
            this.lblEMIStartDateVal.Name = "lblEMIStartDateVal";
            this.lblEMIStartDateVal.Size = new System.Drawing.Size(0, 13);
            this.lblEMIStartDateVal.TabIndex = 187;
            this.lblEMIStartDateVal.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(258, 315);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 37);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MemberName,
            this.PassbookNumber,
            this.Amount2,
            this.PrincipalPart,
            this.InterestPart,
            this.TransactionDate,
            this.Delete,
            this.fk_MemberId,
            this.Accountno2,
            this.LoanId,
            this.AgentId,
            this.LoanType});
            this.dataGridView1.Location = new System.Drawing.Point(30, 377);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(743, 211);
            this.dataGridView1.TabIndex = 189;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // MemberName
            // 
            this.MemberName.HeaderText = "MemberName";
            this.MemberName.Name = "MemberName";
            this.MemberName.ReadOnly = true;
            // 
            // PassbookNumber
            // 
            this.PassbookNumber.HeaderText = "PassbookNumber";
            this.PassbookNumber.Name = "PassbookNumber";
            this.PassbookNumber.ReadOnly = true;
            // 
            // Amount2
            // 
            this.Amount2.HeaderText = "Amount";
            this.Amount2.Name = "Amount2";
            this.Amount2.ReadOnly = true;
            // 
            // PrincipalPart
            // 
            this.PrincipalPart.HeaderText = "PrincipalPart";
            this.PrincipalPart.Name = "PrincipalPart";
            this.PrincipalPart.ReadOnly = true;
            // 
            // InterestPart
            // 
            this.InterestPart.HeaderText = "InterestPart";
            this.InterestPart.Name = "InterestPart";
            this.InterestPart.ReadOnly = true;
            // 
            // TransactionDate
            // 
            this.TransactionDate.HeaderText = "TransactionDate";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForLinkValue = true;
            // 
            // fk_MemberId
            // 
            this.fk_MemberId.HeaderText = "fk_MemberId";
            this.fk_MemberId.Name = "fk_MemberId";
            this.fk_MemberId.ReadOnly = true;
            this.fk_MemberId.Visible = false;
            // 
            // Accountno2
            // 
            this.Accountno2.HeaderText = "Account Number";
            this.Accountno2.Name = "Accountno2";
            this.Accountno2.ReadOnly = true;
            this.Accountno2.Visible = false;
            // 
            // LoanId
            // 
            this.LoanId.HeaderText = "LoanId";
            this.LoanId.Name = "LoanId";
            this.LoanId.ReadOnly = true;
            this.LoanId.Visible = false;
            // 
            // AgentId
            // 
            this.AgentId.HeaderText = "AgentId";
            this.AgentId.Name = "AgentId";
            this.AgentId.ReadOnly = true;
            this.AgentId.Visible = false;
            // 
            // LoanType
            // 
            this.LoanType.HeaderText = "LoanType";
            this.LoanType.Name = "LoanType";
            this.LoanType.Visible = false;
            // 
            // MultipleInstallment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(816, 642);
            this.ControlBox = false;
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblEMIStartDateVal);
            this.Controls.Add(this.radioChargesPartwise);
            this.Controls.Add(this.radioChargesFull);
            this.Controls.Add(this.lblEMIChargesVal);
            this.Controls.Add(this.grpdivision);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dtpOpenDate);
            this.Controls.Add(this.txtMemberName);
            this.Controls.Add(this.lblMemberName);
            this.Controls.Add(this.lblTransDate);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblMessages);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.lblAgentName);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MultipleInstallment";
            this.Load += new System.EventHandler(this.MultipleInstallment_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpdivision.ResumeLayout(false);
            this.grpdivision.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbcountry;
        private System.Windows.Forms.ComboBox cmbslum;
        private System.Windows.Forms.ComboBox cmbdistrict;
        private System.Windows.Forms.ComboBox cmbstate;
        private System.Windows.Forms.ComboBox cmbHub;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMemberName;
        private System.Windows.Forms.Label lblMemberName;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.TextBox txtPassbookNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblLoanAmt;
        private System.Windows.Forms.Label lblLoanAmtVal;
        private System.Windows.Forms.Label lblDispersalBal;
        private System.Windows.Forms.Label lblDispersalBalVal;
        private System.Windows.Forms.Label lblRemainingPrincipal;
        private System.Windows.Forms.Label lblRemainingPrincipalVal;
        private System.Windows.Forms.Label lblPendingChargeVal;
        private System.Windows.Forms.Label lblPendingCharge;
        private System.Windows.Forms.Label lblPendingPrincipalVal;
        private System.Windows.Forms.Label lblPendingPrincipal;
        private System.Windows.Forms.RadioButton radioChargesPartwise;
        private System.Windows.Forms.RadioButton radioChargesFull;
        private System.Windows.Forms.Label lblEMIChargesVal;
        private System.Windows.Forms.GroupBox grpdivision;
        private System.Windows.Forms.Label lblCloseBrace;
        private System.Windows.Forms.Label lblOpenBrace;
        private System.Windows.Forms.TextBox txtChargeAmt;
        private System.Windows.Forms.Label lblChargeAmt;
        private System.Windows.Forms.TextBox txtPrincipalAmt;
        private System.Windows.Forms.Label lblPrincipalAmt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpOpenDate;
        private System.Windows.Forms.Label lblTransDate;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblEMIStartDateVal;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cmbLoanType;
        private System.Windows.Forms.Label lblLoanType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PassbookNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrincipalPart;
        private System.Windows.Forms.DataGridViewTextBoxColumn InterestPart;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewLinkColumn Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn fk_MemberId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Accountno2;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanType;
    }
}