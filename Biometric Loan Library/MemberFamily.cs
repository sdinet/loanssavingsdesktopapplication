﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SPARC;

namespace SPARC
{
    public partial class MemberFamily
    {

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private Int64 mf_MemberFamilyId;
        private Int32 mf_Fk_SlumID;
        private string mf_HouseNumber;
        private string mf_RationCardNumber;
        private Int64 mf_HowManyYearsTheFamilyIsStaying;
        private string mf_FamilyheadName;
        private string mf_HusbandWifeName;
        private bool mf_PartOfHouseForBusiness;
        private string mf_BusinessDetails;
        private string mf_Caste;
        private Int64 mf_FamilyMounthlyIncome;
        private string mf_HouseDetails;
        private string mf_MotherToungue;
        private string mf_State;
        private string mf_Water;
        private string mf_Toilet;
        private string mf_Electricity;
        private string mf_CookingMedium;
        private object mf_FamilyPhotograph;
        private Int64 mf_Createdby;
        private Int64 mf_Updatedby;
        private byte[] mf_picture;
        private DateTime mf_CreatedDate;
        private DateTime mf_UpdatedDate;
        private Int32 mf_FamMemberId;
        private string mfd_Name;
        private string mfd_Gender;
        private Int64 mfd_Age;
        private string mfd_Qualification;
        private Boolean mfd_IsMember;
        private Int64? mfd_Fk_MembersId;
        private Int64 mfd_MemeberFamilyDeatilsId;
        private string m_MemberId;
        private int? m_fk_SlumId;
        private int? m_fk_HubId;
        private int? m_fk_DistrictId;
        private int? m_fk_StateId;
        private int? m_fk_CountryId;
        private string mfd_SlumName;
        private object m_photo;
        public Int64 MemberFamilyId
        {
            get
            {
                return mf_MemberFamilyId;
            }
            set
            {
                mf_MemberFamilyId = value;
            }
        }

        public Int32 Fk_SlumID
        {
            get
            {
                return mf_Fk_SlumID;
            }
            set
            {
                mf_Fk_SlumID = value;
            }
        }
        public string HouseNumber
        {
            get
            {
                return mf_HouseNumber;
            }
            set
            {
                mf_HouseNumber = value;
            }
        }
        public string RationCardNumber
        {
            get
            {
                return mf_RationCardNumber;
            }
            set
            {
                mf_RationCardNumber = value;
            }
        }
        public Int64 HowManyYearsTheFamilyIsStaying
        {
            get
            {
                return mf_HowManyYearsTheFamilyIsStaying;
            }
            set
            {
                mf_HowManyYearsTheFamilyIsStaying = value;
            }
        }
        public string FamilyheadName
        {
            get
            {
                return mf_FamilyheadName;
            }
            set
            {
                mf_FamilyheadName = value;
            }
        }
        public string HusbandWifeName
        {
            get
            {
                return mf_HusbandWifeName;
            }
            set
            {
                mf_HusbandWifeName = value;
            }
        }

        public bool PartOfHouseForBusiness
        {
            get
            {
                return mf_PartOfHouseForBusiness;
            }
            set
            {
                mf_PartOfHouseForBusiness = value;
            }
        }
        public string BusinessDetails
        {
            get
            {
                return mf_BusinessDetails;
            }
            set
            {
                mf_BusinessDetails = value;
            }
        }

        public string Caste
        {
            get
            {
                return mf_Caste;
            }
            set
            {
                mf_Caste = value;
            }
        }
        public Int64 FamilyMounthlyIncome
        {
            get
            {
                return mf_FamilyMounthlyIncome;
            }
            set
            {
                mf_FamilyMounthlyIncome = value;
            }
        }
        public string HouseDetails
        {
            get
            {
                return mf_HouseDetails;
            }
            set
            {
                mf_HouseDetails = value;
            }
        }
        public string MotherToungue
        {
            get
            {
                return mf_MotherToungue;
            }
            set
            {
                mf_MotherToungue = value;
            }
        }
        public string State
        {
            get
            {
                return mf_State;
            }
            set
            {
                mf_State = value;
            }
        }
        public string Water
        {
            get
            {
                return mf_Water;
            }
            set
            {
                mf_Water = value;
            }
        }

        public string Toilet
        {
            get
            {
                return mf_Toilet;
            }
            set
            {
                mf_Toilet = value;
            }
        }
        public string Electricity
        {
            get
            {
                return mf_Electricity;
            }
            set
            {
                mf_Electricity = value;
            }
        }
        public string CookingMedium
        {
            get
            {
                return mf_CookingMedium;
            }
            set
            {
                mf_CookingMedium = value;
            }
        }
        public object FamilyPhotograph
        {

            get
            {
                return mf_FamilyPhotograph;
            }
            set
            {
                mf_FamilyPhotograph = value;
            }

        }
        public Int64 Createdby
        {
            get
            {
                return mf_Createdby;
            }
            set
            {
                mf_Createdby = value;
            }
        }

        public Int64 Updatedby
        {
            get
            {
                return mf_Updatedby;
            }
            set
            {
                mf_Updatedby = value;
            }
        }
        public byte[] Picture
        {
            get
            {
                return mf_picture;
            }
            set
            {
                mf_picture = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return mf_CreatedDate;
            }
            set
            {
                mf_CreatedDate = value;
            }

        }
        public DateTime UpdatedDate
        {
            get
            {
                return mf_UpdatedDate;
            }
            set
            {
                mf_UpdatedDate = value;
            }

        }
        public Int32 FamilyMemberId_Pk
        {
            get
            {
                return mf_FamMemberId;
            }
            set
            {
                //z_bool.mf_FamMemberId = true;
                mf_FamMemberId = value;
            }
        }

        public string Name
        {
            get
            {
                return mfd_Name;
            }
            set
            {
                mfd_Name = value;
            }
        }
        public string Gender
        {
            get
            {
                return mfd_Gender;
            }
            set
            {
                mfd_Gender = value;
            }
        }

        public Int64 Age
        {
            get
            {
                return mfd_Age;
            }
            set
            {
                mfd_Age = value;
            }
        }
        public string Qualification
        {
            get
            {
                return mfd_Qualification;
            }
            set
            {
                mfd_Qualification = value;
            }
        }

        public Boolean IsMember
        {
            get
            {
                return mfd_IsMember;
            }
            set
            {
                mfd_IsMember = value;
            }
        }
        public Int64 Fk_MemberFamailyID
        {
            get
            {
                return mf_MemberFamilyId;
            }
            set
            {
                mf_MemberFamilyId = value;
            }
        }
        public Int64? MembersID
        {
            get
            {
                return mfd_Fk_MembersId;
            }
            set
            {
                mfd_Fk_MembersId = value;
            }
        }
        public string MemberId
        {
            get
            {
                return m_MemberId;
            }
            set
            {
                // z_bool.m_MemberId = true;
                m_MemberId = value;
            }
        }
        public int? fk_SlumId
        {
            get
            {
                return m_fk_SlumId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_SlumId = value;
            }
        }

        public int? fk_HubId
        {
            get
            {
                return m_fk_HubId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_HubId = value;
            }
        }
        public int? fk_DistricId
        {
            get
            {
                return m_fk_DistrictId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_DistrictId = value;
            }
        }
        public int? fk_StateId
        {
            get
            {
                return m_fk_StateId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_StateId = value;
            }
        }
        public int? fk_CountryId
        {
            get
            {
                return m_fk_CountryId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_CountryId = value;
            }
        }
        //Shriti added//
        public Int64 MemeberFamilyDeatilsId
        {

            get
            {
                return mfd_MemeberFamilyDeatilsId;
            }
            set
            {
                mfd_MemeberFamilyDeatilsId = value;
            }
        }
        public string SlumsName
        {
            get
            {
                return mfd_SlumName;

            }
            set
            {
                mfd_SlumName = value;

            }

        }
        public object Photo 
        { get 
        { return m_photo; }
            set { m_photo = value; } 
        }

        public int SaveMemebrFamily()
        {
            SqlParameter[] sqlParam = new SqlParameter[20];

            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@MemberFamilyId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = mf_MemberFamilyId;


            sqlParam[iCounter] = new SqlParameter("@Fk_SlumID", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = mf_Fk_SlumID;

            sqlParam[iCounter] = new SqlParameter("@HouseNumber", SqlDbType.NVarChar, 10);
            sqlParam[iCounter++].Value = mf_HouseNumber;

            sqlParam[iCounter] = new SqlParameter("@RationCardNumber", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_RationCardNumber;

            sqlParam[iCounter] = new SqlParameter("@HowManyYearsTheFamilyIsStaying", SqlDbType.Int);
            sqlParam[iCounter++].Value = mf_HowManyYearsTheFamilyIsStaying;

            sqlParam[iCounter] = new SqlParameter("@FamilyheadName", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = mf_FamilyheadName;

            sqlParam[iCounter] = new SqlParameter("@HusbandWifeName", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = mf_HusbandWifeName;

            sqlParam[iCounter] = new SqlParameter("@PartOfHouseForBusiness", SqlDbType.Bit);
            sqlParam[iCounter++].Value = mf_PartOfHouseForBusiness;

            sqlParam[iCounter] = new SqlParameter("@BusinessDetails", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = mf_BusinessDetails;

            sqlParam[iCounter] = new SqlParameter("@Caste", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = mf_Caste;

            sqlParam[iCounter] = new SqlParameter("@FamilyMounthlyIncome", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = mf_FamilyMounthlyIncome;

            sqlParam[iCounter] = new SqlParameter("@HouseDetails", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = mf_HouseDetails;

            sqlParam[iCounter] = new SqlParameter("@MotherToungue", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_MotherToungue;

            sqlParam[iCounter] = new SqlParameter("@State", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_State;

            sqlParam[iCounter] = new SqlParameter("@Water", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_Water;

            sqlParam[iCounter] = new SqlParameter("@Toilet", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_Toilet;

            sqlParam[iCounter] = new SqlParameter("@Electricity", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_Electricity;

            sqlParam[iCounter] = new SqlParameter("@CookingMedium", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = mf_CookingMedium;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = mf_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = mf_Updatedby;

            string spName;
            if (mf_MemberFamilyId == 0)
                spName = "usp_MemberFamilyInsert";
            else
                spName = "usp_MemberFamilyUpdate";

            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, spName, sqlParam));
        }
        public int UpdatePhoto(byte[] img, long MemberFamilyId)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            if (con.State == ConnectionState.Closed)//check whether connection to database is close or not
                con.Open();//if connection is close then only open the connection
            SqlCommand cmd = new SqlCommand("usp_SaveFamilyPhoto", con);//create a SQL command object by passing name of the stored procedure and database connection 
            cmd.CommandType = CommandType.StoredProcedure; //set command object command type to stored procedure type
            cmd.Parameters.Add("@img", SqlDbType.Image).Value = img;//add parameter to the command object and set value to that parameter
            cmd.Parameters.Add("@MemberFamilyId", SqlDbType.BigInt).Value = MemberFamilyId;//add parameter to the command object and set value to that parameter
            return cmd.ExecuteNonQuery();//execute command                     
        }

        //Update the Members
        public DataSet LoadMemberFamilyById()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@MemberFamilyId", SqlDbType.Int);
            sqlParam[0].Value = mf_MemberFamilyId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_MemberFamilySelect]", sqlParam);
        }


        public DataSet GetFamilyGetails()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@MemberFamilyId", SqlDbType.BigInt);
            sqlParam[0].Value = MemberFamilyId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GetMemberDetailsFGrid]", sqlParam);


        }
        //Here for Validating the MemberID to get the all member Id
        public DataSet CollectMember()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "GetAllMemberId");

        }


        public DataSet DeleteMember()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@MemberFamilyId", SqlDbType.BigInt);
            sqlParam[0].Value = MemberFamilyId;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_DeleteRow]", sqlParam);



        }
        public DataSet AddMembers()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@MemberId", SqlDbType.VarChar, 50);
            sqlParam[0].Value = MemberId;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MemberAdd", sqlParam);

        }
        public DataSet AddMembersValues()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = MemberId;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetMemberID", sqlParam);

        }
        public DataSet SearchMemberFamily()
        {
            SqlParameter[] sqlParam = new SqlParameter[7];

            sqlParam[0] = new SqlParameter("@FamilyheadName", SqlDbType.VarChar, 50);
            sqlParam[0].Value = FamilyheadName;
            sqlParam[1] = new SqlParameter("@fk_CountryId", SqlDbType.Int);
            sqlParam[1].Value = (object)m_fk_CountryId ?? DBNull.Value;

            sqlParam[2] = new SqlParameter("@fk_StateId", SqlDbType.Int);
            sqlParam[2].Value = (object)m_fk_StateId ?? DBNull.Value;

            sqlParam[3] = new SqlParameter("@fk_DistrictId", SqlDbType.Int);
            sqlParam[3].Value = (object)m_fk_DistrictId ?? DBNull.Value;

            sqlParam[4] = new SqlParameter("@fk_HubId", SqlDbType.Int);
            sqlParam[4].Value = (object)m_fk_HubId ?? DBNull.Value;

            sqlParam[5] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[5].Value = (object)m_fk_SlumId ?? DBNull.Value;

            sqlParam[6] = new SqlParameter("@MemberFamilyId", SqlDbType.Int);
            sqlParam[6].Value = (object)mf_MemberFamilyId ?? DBNull.Value;

           // sqlParam[7] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
           // sqlParam[7].Value = (object)mfd_SlumName ?? DBNull.Value;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_MembersFamilySearch]", sqlParam);

        }
        public DataSet FetchQuickInfoFamilyMemberByID()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@MemberFamilyId", SqlDbType.Int);
            sqlParam[0].Value = mf_MemberFamilyId;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_MembersQuickFamilyInfo]", sqlParam);
        }

        //Inserting the Grid Values to Grid in DataBase//
        public int InsertFamilyDetails()
        {
            try
            {
                SqlConnection con = new SqlConnection();
                SqlDataAdapter da;
                SqlDataReader rdr = null;
                DataTable dt = new DataTable();
                con.ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                con.Open();
                SqlParameter[] sqlParam = new SqlParameter[7];
                int intCounter = 0;
                //float query= '';
                sqlParam[intCounter] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
                sqlParam[intCounter++].Value = mfd_Name;

                sqlParam[intCounter] = new SqlParameter("@Gender", SqlDbType.NVarChar, 50);
                sqlParam[intCounter++].Value = mfd_Gender;

                sqlParam[intCounter] = new SqlParameter("@Age", SqlDbType.Int);
                sqlParam[intCounter++].Value = mfd_Age;

                sqlParam[intCounter] = new SqlParameter("@Qualification", SqlDbType.NVarChar, 25);
                sqlParam[intCounter++].Value = mfd_Qualification;

                sqlParam[intCounter] = new SqlParameter("@IsMember", SqlDbType.Bit);
                sqlParam[intCounter++].Value = mfd_IsMember;

                sqlParam[intCounter] = new SqlParameter("@Fk_MemberFamilyId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = mf_MemberFamilyId;

                sqlParam[intCounter] = new SqlParameter("@Fk_Members", SqlDbType.BigInt); // here v have to check a codition
                sqlParam[intCounter++].Value = mfd_Fk_MembersId;

                return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_FamilyDetailsGrid_Insert", sqlParam));

            }
            catch
            {

                throw;

            }
        }
        public Int32 UpdateMemberFamilyDetailsGrid()
        {
            try
            {
                SqlConnection con = new SqlConnection();
                SqlDataAdapter da;
                SqlDataReader rdr = null;
                DataTable dt = new DataTable();
                con.ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
                con.Open();
                SqlParameter[] sqlParam = new SqlParameter[8];
                int intCounter = 0;
                sqlParam[intCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = mfd_MemeberFamilyDeatilsId;

                sqlParam[intCounter] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
                sqlParam[intCounter++].Value = mfd_Name;

                sqlParam[intCounter] = new SqlParameter("@Gender", SqlDbType.NVarChar, 50);
                sqlParam[intCounter++].Value = mfd_Gender;

                sqlParam[intCounter] = new SqlParameter("@Age", SqlDbType.Int);
                sqlParam[intCounter++].Value = mfd_Age;

                sqlParam[intCounter] = new SqlParameter("@Qualification", SqlDbType.NVarChar, 25);
                sqlParam[intCounter++].Value = mfd_Qualification;

                sqlParam[intCounter] = new SqlParameter("@IsMember", SqlDbType.Bit);
                sqlParam[intCounter++].Value = mfd_IsMember;

                sqlParam[intCounter] = new SqlParameter("@Fk_MemberFamilyId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = mf_MemberFamilyId;

                sqlParam[intCounter] = new SqlParameter("@Fk_Members", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = mfd_Fk_MembersId;

                return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_FamilyDetailsGrid_Update", sqlParam));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet DeleteMemberfamilydetails(Int64 UserId)
        {
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[2];

                sqlParam[0] = new SqlParameter("@MemeberFamilyDeatilsId", SqlDbType.BigInt);
                sqlParam[0].Value = MemeberFamilyDeatilsId;

                sqlParam[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
                sqlParam[1].Value = UserId;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "Deletememberfamilydetails", sqlParam);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
