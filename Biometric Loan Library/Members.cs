﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : Members.
    /// </summary>
    public class Members
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        #region local variables
        private string m_Town;
        private Int64 m_Createdby;
        private string m_Mobile;
        private DateTime m_UpdatedDate;
        private int m_Gender;
        //private object m_Photo;
        private string m_HomePhone;
        private string m_Landmark;
        private string m_MemberId;
        private string m_Remarks;
        private string m_Zip;
        private DateTime m_CreatedDate;
        private string m_Address2;
        private bool m_Isdeleted;
        private string m_ContactpersonNumber;
        private Int64 m_Updatedby;
        private string m_BioMetricId;
        private string m_Address1;
        private string m_Name;
        private string m_PassbookNumber;
        private string m_Voterid;
        private string m_Adhaarid;
        private string m_FatherName;
        private int? m_fk_SlumId;
        private int? m_fk_HubId;
        private int? m_fk_DistrictId;
        private int? m_fk_StateId;
        private int? m_fk_CountryId;
        private string m_Email;
        private DateTime? m_DateofBirth;
        private string m_ContactpersonName;
        private Int64 m_Id;
        private string m_Status;
        //private Members_Criteria z_WhereClause;
        private byte[] m_picture;
        private object m_photo;
        private int m_OperationType;
        private Int64 m_groupname;

        #endregion

        #region dirtyvar
        public struct IsDirty_Members
        {
            public bool m_OperationType;
            public bool m_Town;
            public bool m_Createdby;
            public bool m_Mobile;
            public bool m_UpdatedDate;
            public bool m_Gender;
            public bool m_HomePhone;
            public bool m_Landmark;
            public bool m_MemberId;
            public bool m_Remarks;
            public bool m_Zip;
            public bool m_CreatedDate;
            public bool m_Address2;
            public bool m_Isdeleted;
            public bool m_ContactpersonNumber;
            public bool m_Updatedby;
            public bool m_BioMetricId;
            public bool m_Address1;
            public bool m_Name;
            public bool m_FatherName;
            public bool m_fk_SlumId;
            public bool m_Email;
            public bool m_DateofBirth;
            public bool m_ContactpersonName;
            public bool m_Id;
            public bool m_PassbookNumber;
            public bool m_Voterid;
            public bool m_Adhaarid;
            public bool m_groupname;
        }
        public IsDirty_Members z_bool;
        #endregion

        //public Members()
        //{
        //    z_WhereClause = new Members_Criteria();
        //}

        #region getset

        public int OperationType
        {
            get
            {
                return m_OperationType;
            }
            set
            {
                z_bool.m_OperationType = true;
                m_OperationType = value;
            }
        }
        public string Town
        {
            get
            {
                return m_Town;
            }
            set
            {
                z_bool.m_Town = true;
                m_Town = value;
            }
        }

        public byte[] Picture
        {
            get
            {
                return m_picture;
            }
            set
            {
                m_picture = value;
            }
        }

        public object Photo
        {
            get
            {
                return m_photo;
            }
            set
            {
                m_photo = value;
            }
        }

        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
            }
        }
        public string Mobile
        {
            get
            {
                return m_Mobile;
            }
            set
            {
                z_bool.m_Mobile = true;
                m_Mobile = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
            }
        }
        public int Gender
        {
            get
            {
                return m_Gender;
            }
            set
            {
                z_bool.m_Gender = true;
                m_Gender = value;
            }
        }

        public string HomePhone
        {
            get
            {
                return m_HomePhone;
            }
            set
            {
                z_bool.m_HomePhone = true;
                m_HomePhone = value;
            }
        }
        public string Landmark
        {
            get
            {
                return m_Landmark;
            }
            set
            {
                z_bool.m_Landmark = true;
                m_Landmark = value;
            }
        }
        public string MemberId
        {
            get
            {
                return m_MemberId;
            }
            set
            {
                z_bool.m_MemberId = true;
                m_MemberId = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                z_bool.m_Remarks = true;
                m_Remarks = value;
            }
        }
        public string Zip
        {
            get
            {
                return m_Zip;
            }
            set
            {
                z_bool.m_Zip = true;
                m_Zip = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
            }
        }
        public string Address2
        {
            get
            {
                return m_Address2;
            }
            set
            {
                z_bool.m_Address2 = true;
                m_Address2 = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                z_bool.m_Isdeleted = true;
                m_Isdeleted = value;
            }
        }
        public string ContactpersonNumber
        {
            get
            {
                return m_ContactpersonNumber;
            }
            set
            {
                z_bool.m_ContactpersonNumber = true;
                m_ContactpersonNumber = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
            }
        }
        public string BioMetricId
        {
            get
            {
                return m_BioMetricId;
            }
            set
            {
                z_bool.m_BioMetricId = true;
                m_BioMetricId = value;
            }
        }
        public string Address1
        {
            get
            {
                return m_Address1;
            }
            set
            {
                z_bool.m_Address1 = true;
                m_Address1 = value;
            }
        }
        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                z_bool.m_Name = true;
                m_Name = value;
            }
        }
        public string PassbookNumber
        {
            get
            {
                return m_PassbookNumber;
            }
            set
            {
                z_bool.m_PassbookNumber = true;
                m_PassbookNumber = value;
            }
        }
        public string Voterid
        {
            get
            {
                return m_Voterid;
            }
            set
            {
                z_bool.m_Voterid = true;
                m_Voterid = value;
            }
        }
        public string Adhaarid
        {
            get
            {
                return m_Adhaarid;
            }
            set
            {
                z_bool.m_Adhaarid = true;
                m_Adhaarid = value;
            }
        }
        public string FatherName
        {
            get
            {
                return m_FatherName;
            }
            set
            {
                z_bool.m_FatherName = true;
                m_FatherName = value;
            }
        }
        public int? fk_SlumId
        {
            get
            {
                return m_fk_SlumId;
            }
            set
            {
                z_bool.m_fk_SlumId = true;
                m_fk_SlumId = value;
            }
        }

        public int? fk_HubId
        {
            get
            {
                return m_fk_HubId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_HubId = value;
            }
        }
        public int? fk_DistricId
        {
            get
            {
                return m_fk_DistrictId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_DistrictId = value;
            }
        }
        public int? fk_StateId
        {
            get
            {
                return m_fk_StateId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_StateId = value;
            }
        }
        public int? fk_CountryId
        {
            get
            {
                return m_fk_CountryId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                m_fk_CountryId = value;
            }
        }

        public string Email
        {
            get
            {
                return m_Email;
            }
            set
            {
                z_bool.m_Email = true;
                m_Email = value;
            }
        }
        public DateTime? DateofBirth
        {
            get
            {
                return m_DateofBirth;
            }
            set
            {
                z_bool.m_DateofBirth = true;
                m_DateofBirth = value;
            }
        }
        public string ContactpersonName
        {
            get
            {
                return m_ContactpersonName;
            }
            set
            {
                z_bool.m_ContactpersonName = true;
                m_ContactpersonName = value;
            }
        }
        public Int64 Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
            }
        }

        public string Status
        {
            get
            {
                return m_Status;
            }
            set
            {
                //z_bool.m_ContactpersonName = true;
                m_Status = value;
            }
        }

        public Int64 Groupname
        {
            get
            {
                return m_groupname;
            }
            set
            {
                //z_bool.m_ContactpersonName = true;
                m_groupname = value;
            }
        }
        #endregion

        public bool Insert()//string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
        {
            try
            {
                string commandText = "";
                SqlParameter[] commandParameters = new SqlParameter[0];
                if (SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, commandText, commandParameters) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataSet FetchPassbook()
        {
            SqlParameter[] sqlParam = new SqlParameter[4];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@PassbookNumber", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_PassbookNumber;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_SlumId;

            sqlParam[iCounter] = new SqlParameter("@OperationType", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_OperationType;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SelectAllPassbook", sqlParam);
        }

        public DataSet SearchMember()
        {

            SqlParameter[] sqlParam = new SqlParameter[9];

            sqlParam[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            sqlParam[0].Value = m_Name.Length > 0 ? m_Name : null;

            sqlParam[1] = new SqlParameter("@MemberId", SqlDbType.NVarChar, 50);
            sqlParam[1].Value = m_MemberId.Length > 0 ? m_MemberId : null;

            sqlParam[2] = new SqlParameter("@BioMetricId", SqlDbType.NVarChar, 50);
            sqlParam[2].Value = m_BioMetricId.Length > 0 ? m_BioMetricId : null;

            sqlParam[3] = new SqlParameter("@fk_CountryId", SqlDbType.Int);
            sqlParam[3].Value = (object)m_fk_CountryId ?? DBNull.Value;

            sqlParam[4] = new SqlParameter("@fk_StateId", SqlDbType.Int);
            sqlParam[4].Value = (object)m_fk_StateId ?? DBNull.Value;

            sqlParam[5] = new SqlParameter("@fk_DistrictId", SqlDbType.Int);
            sqlParam[5].Value = (object)m_fk_DistrictId ?? DBNull.Value;

            sqlParam[6] = new SqlParameter("@fk_HubId", SqlDbType.Int);
            sqlParam[6].Value = (object)m_fk_HubId ?? DBNull.Value;

            sqlParam[7] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[7].Value = (object)m_fk_SlumId ?? DBNull.Value;

            sqlParam[8] = new SqlParameter("@PassbookNumber", SqlDbType.NVarChar, 20);
            sqlParam[8].Value = m_PassbookNumber.Length > 0 ? m_PassbookNumber : null;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MembersSearch", sqlParam);

        }

        //edited by Pawan--start
        // Dataset which will return the last transaction details using  the specified stored procedure
        public DataSet FetchTransactionDetails()
        {
            //made new changes in the stored procedure need to update in other application and in server
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_TransactionDetailsSelect");
        }
        //edited by Pawan--end

        public int SaveMember()
        {

            SqlParameter[] sqlParam = new SqlParameter[30];

            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_Name.Length > 0 ? m_Name : null;

            sqlParam[iCounter] = new SqlParameter("@FatherName", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_FatherName.Length > 0 ? m_FatherName : null;

            sqlParam[iCounter] = new SqlParameter("@Gender", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_Gender;

            sqlParam[iCounter] = new SqlParameter("@DateOfBirth", SqlDbType.Date);
            sqlParam[iCounter++].Value = m_DateofBirth.ToString().Length > 0 ? m_DateofBirth : null;

            sqlParam[iCounter] = new SqlParameter("@MobileNo", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_Mobile.Length > 0 ? m_Mobile : null;

            sqlParam[iCounter] = new SqlParameter("@HomePhone", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_HomePhone.Length > 0 ? m_HomePhone : null;

            sqlParam[iCounter] = new SqlParameter("@Email", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_Email.Length > 0 ? m_Email : null;

            sqlParam[iCounter] = new SqlParameter("@fk_CountryId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_CountryId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_StateId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_DistrictId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_DistrictId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_HubId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_HubId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_SlumId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@Address1", SqlDbType.NVarChar, 500);
            sqlParam[iCounter++].Value = m_Address1.Length > 0 ? m_Address1 : null;

            sqlParam[iCounter] = new SqlParameter("@Address2", SqlDbType.NVarChar, 500);
            sqlParam[iCounter++].Value = m_Address2.Length > 0 ? m_Address2 : null;

            sqlParam[iCounter] = new SqlParameter("@Town", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = m_Town.Length > 0 ? m_Town : null;

            sqlParam[iCounter] = new SqlParameter("@Landmark", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = m_Landmark.Length > 0 ? m_Landmark : null;

            sqlParam[iCounter] = new SqlParameter("@Zip", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_Zip.Length > 0 ? m_Zip : null;

            sqlParam[iCounter] = new SqlParameter("@BioMetricId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_BioMetricId.Length > 0 ? m_BioMetricId : null;

            sqlParam[iCounter] = new SqlParameter("@ContactPerson", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = m_ContactpersonName.Length > 0 ? m_ContactpersonName : null;

            sqlParam[iCounter] = new SqlParameter("@ContactPersonPhone", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_ContactpersonNumber.Length > 0 ? m_ContactpersonNumber : null;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 1500);
            sqlParam[iCounter++].Value = m_Remarks.Length > 0 ? m_Remarks : null;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.NVarChar, 25);
            sqlParam[iCounter++].Value = (object)m_Status ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@PassbookNumber", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_PassbookNumber.Length > 0 ? m_PassbookNumber : null;

            sqlParam[iCounter] = new SqlParameter("@Voterid", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = (object)m_Voterid ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@Adhaarid", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = (object)m_Adhaarid ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@Photo", SqlDbType.Image);
            if ((object)Picture != DBNull.Value)
            {
                sqlParam[iCounter++].Value = Picture;
            }//sqlParam[iCounter++].Value = m_photo ?? DBNull.Value;
            else
            {
                sqlParam[iCounter++].Value = DBNull.Value;
            }
            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@Groupname", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_groupname;


            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_UpdateMember", sqlParam));
        }

        public void UpdatePhoto(byte[] img, long MemberId)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            if (con.State == ConnectionState.Closed)//check whether connection to database is close or not
                con.Open();//if connection is close then only open the connection
            SqlCommand cmd = new SqlCommand("usp_SaveMemberPhoto", con);//create a SQL command object by passing name of the stored procedure and database connection 
            cmd.CommandType = CommandType.StoredProcedure; //set command object command type to stored procedure type

            cmd.Parameters.Add("@img", SqlDbType.Image).Value = img;//add parameter to the command object and set value to that parameter
            cmd.Parameters.Add("@MemberId", SqlDbType.BigInt).Value = MemberId;//add parameter to the command object and set value to that parameter
            cmd.ExecuteNonQuery();//execute command                     
        }
        public int UpdateImage(Int64 MemberId, string Filename, Int64 DocType, Int64 CreatedBy, Int64 UpdatedBy)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            if (con.State == ConnectionState.Closed)//check whether connection to database is close or not
                con.Open();//if connection is close then only open the connection
            SqlCommand cmd = new SqlCommand("usp_SaveMemberImage", con);//create a SQL command object by passing name of the stored procedure and database connection 
            cmd.CommandType = CommandType.StoredProcedure; //set command object command type to stored procedure type

            cmd.Parameters.Add("@MemberId", SqlDbType.BigInt).Value = MemberId;
            cmd.Parameters.Add("@Filename", SqlDbType.NVarChar, 50).Value = Filename;
            cmd.Parameters.Add("@DocumentType", SqlDbType.BigInt).Value = DocType;
            cmd.Parameters.Add("@Createdby", SqlDbType.BigInt).Value = CreatedBy;
            cmd.Parameters.Add("@Updatedby", SqlDbType.BigInt).Value = UpdatedBy;

            return cmd.ExecuteNonQuery();//execute command                     
        }
        public DataSet LoadMemberById()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = m_Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MembersSelect", sqlParam);
        }
        //public DataSet LoadGroupById()
        //{

        //    SqlParameter[] sqlParam = new SqlParameter[1];

        //    sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
        //    sqlParam[0].Value = m_Id;

        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupSelect", sqlParam);
        //}

        public DataSet FetchQuickInfoByID()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = m_Id;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MembersQuickInfo", sqlParam);
        }

        public DataSet FetchMemberInfo()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = m_Id;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MembersSavLoanInfo", sqlParam);
        }

        public DataSet DeleteMember(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_MembersDelete", sqlParam);
        }



        //public DataSet GetCompletedLoanIntemation()
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[1];
        //     int iCounter = 0;

        //    sqlParam[iCounter] = new SqlParameter("@MemberId", SqlDbType.NVarChar, 20);
        //    sqlParam[iCounter++].Value = m_MemberId;
        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoanCloseIntemation", sqlParam);

        //}



    }

    /// <summary>
    /// Generated Class for Table : Members_Criteria.
    /// </summary>

}