﻿using System;
using System.Data;
using System.Data.OleDb;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : HeadOfAccounts.
    /// </summary>
    public class HeadOfAccounts
    {
        private DateTime m_UpdatedDate;
        private bool m_IsDeleted;
        private string m_AccountName;
        private int m_Createdby;
        private int m_Updatedby;
        private int m_Id;
        private string m_Description;
        private DateTime m_CreatedDate;
        private HeadOfAccounts_Criteria z_WhereClause;

        public struct IsDirty_HeadOfAccounts
        {
            public bool m_UpdatedDate;
            public bool m_IsDeleted;
            public bool m_AccountName;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Description;
            public bool m_CreatedDate;
        }
        public IsDirty_HeadOfAccounts z_bool;

        public HeadOfAccounts()
        {
            z_WhereClause = new HeadOfAccounts_Criteria();
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
            }
        }
        public bool IsDeleted
        {
            get
            {
                return m_IsDeleted;
            }
            set
            {
                z_bool.m_IsDeleted = true;
                m_IsDeleted = value;
            }
        }
        public string AccountName
        {
            get
            {
                return m_AccountName;
            }
            set
            {
                z_bool.m_AccountName = true;
                m_AccountName = value;
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
            }
        }
        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                z_bool.m_Description = true;
                m_Description = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
            }
        }
        public HeadOfAccounts_Criteria Where
        {
            get
            {
                return z_WhereClause;
            }
            set
            {
                z_WhereClause = value;
            }
        }

        public string Insert()
        {
            string z_sep = "";
            string SQL = "INSERT INTO HeadOfAccounts ( ";
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate";
                z_sep = " , ";
            }
            if (z_bool.m_IsDeleted)
            {
                SQL += z_sep + "IsDeleted";
                z_sep = " , ";
            }
            if (z_bool.m_AccountName)
            {
                SQL += z_sep + "AccountName";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id";
                z_sep = " , ";
            }
            if (z_bool.m_Description)
            {
                SQL += z_sep + "Description";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate";
                z_sep = " , ";
            }
            SQL += ") VALUES (";
            z_sep = "";
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "'" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_IsDeleted)
            {
                SQL += z_sep + "'" + m_IsDeleted + "'";
                z_sep = " , ";
            }
            if (z_bool.m_AccountName)
            {
                SQL += z_sep + "'" + m_AccountName + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "'" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "'" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "'" + m_Id + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Description)
            {
                SQL += z_sep + "'" + m_Description + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "'" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            SQL += ")";
            return SQL;
        }
        public string Update()
        {
            string z_sep = "";
            string SQL = "UPDATE HeadOfAccounts SET ";
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_IsDeleted)
            {
                SQL += z_sep + "IsDeleted='" + m_IsDeleted + "'";
                z_sep = " , ";
            }
            if (z_bool.m_AccountName)
            {
                SQL += z_sep + "AccountName='" + m_AccountName + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + m_Id + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Description)
            {
                SQL += z_sep + "Description='" + m_Description + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            z_sep = " WHERE ";
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_IsDeleted)
            {
                SQL += z_sep + "IsDeleted='" + z_WhereClause.IsDeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AccountName)
            {
                SQL += z_sep + "AccountName='" + z_WhereClause.AccountName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Description)
            {
                SQL += z_sep + "Description='" + z_WhereClause.Description + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string Delete()
        {
            string z_sep = " WHERE ";
            string SQL = "DELETE FROM HeadOfAccounts";
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_IsDeleted)
            {
                SQL += z_sep + "IsDeleted='" + z_WhereClause.IsDeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AccountName)
            {
                SQL += z_sep + "AccountName='" + z_WhereClause.AccountName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Description)
            {
                SQL += z_sep + "Description='" + z_WhereClause.Description + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string SearchSQL()
        {
            string z_sep = " WHERE ";
            string SQL = "SELECT * FROM HeadOfAccounts";
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_IsDeleted)
            {
                SQL += z_sep + "IsDeleted='" + z_WhereClause.IsDeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AccountName)
            {
                SQL += z_sep + "AccountName='" + z_WhereClause.AccountName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Description)
            {
                SQL += z_sep + "Description='" + z_WhereClause.Description + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
    }

    /// <summary>
    /// Generated Class for Table : HeadOfAccounts_Criteria.
    /// </summary>
    public class HeadOfAccounts_Criteria
    {
        private DateTime m_UpdatedDate;
        private bool m_IsDeleted;
        private string m_AccountName;
        private int m_Createdby;
        private int m_Updatedby;
        private int m_Id;
        private string m_Description;
        private DateTime m_CreatedDate;
        private string z_MyWhere;
        private string _zWhereClause;
        private string z_sep = " ";

        public struct IsDirty_HeadOfAccounts_Criteria
        {
            public bool m_UpdatedDate;
            public bool m_IsDeleted;
            public bool m_AccountName;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Description;
            public bool m_CreatedDate;
            public bool MyWhere;

        }
        public IsDirty_HeadOfAccounts_Criteria z_bool;

        public HeadOfAccounts_Criteria()
        {
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
                if (z_bool.m_UpdatedDate)
                {
                    _zWhereClause += z_sep + "UpdatedDate='" + UpdatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public bool IsDeleted
        {
            get
            {
                return m_IsDeleted;
            }
            set
            {
                z_bool.m_IsDeleted = true;
                m_IsDeleted = value;
                if (z_bool.m_IsDeleted)
                {
                    _zWhereClause += z_sep + "IsDeleted='" + IsDeleted + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string AccountName
        {
            get
            {
                return m_AccountName;
            }
            set
            {
                z_bool.m_AccountName = true;
                m_AccountName = value;
                if (z_bool.m_AccountName)
                {
                    _zWhereClause += z_sep + "AccountName='" + AccountName + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
                if (z_bool.m_Createdby)
                {
                    _zWhereClause += z_sep + "Createdby='" + Createdby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
                if (z_bool.m_Updatedby)
                {
                    _zWhereClause += z_sep + "Updatedby='" + Updatedby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
                if (z_bool.m_Id)
                {
                    _zWhereClause += z_sep + "Id='" + Id + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                z_bool.m_Description = true;
                m_Description = value;
                if (z_bool.m_Description)
                {
                    _zWhereClause += z_sep + "Description='" + Description + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
                if (z_bool.m_CreatedDate)
                {
                    _zWhereClause += z_sep + "CreatedDate='" + CreatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string MyWhere
        {
            get
            {
                return z_MyWhere;
            }
            set
            {
                z_bool.MyWhere = true;
                z_MyWhere = value;
                if (z_bool.MyWhere)
                {
                    _zWhereClause += z_sep + z_MyWhere;
                    z_sep = " AND ";
                }
            }
        }
        public string WhereClause()
        {
            return _zWhereClause;
        }
    }
}