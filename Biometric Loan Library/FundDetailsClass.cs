﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data;


namespace SPARC
{
   public  class FundDetailsClass
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private Int64 FD_Id;
        private Int64 FD_FK_FunderID;
        private Int64 FD_SponserType;
        private double FD_Amount;
        private Int64 FD_Currency;
        private DateTime FD_Date;
        private Int64 FD_CreatedBy;
        private DateTime FD_CreatedDate;
        private Int64 FD_UpdatedBy;
        private DateTime FD_UpdatedDate;
        private string FD_Notes;
        private string FD_Project;
        
        

        public FundDetailsClass()
        {
        }


        public Int64 Id
        {
            get
            {
                return FD_Id;
            }
            set
            {
                FD_Id = value;
            }
        }
        public Int64 Fk_FunderId
        {
            get
            {
                return FD_FK_FunderID;
            }
            set
            {
                FD_FK_FunderID = value;
            }
        }
        public Int64 Sponser_Type
        {
            get
            {
                return FD_SponserType;
            }
            set
            {
                FD_SponserType = value;
            }
        }
        public double Amount
        {
            get
            {
                return FD_Amount;
            }
            set
            {
                FD_Amount = value;
            }

        }
        public Int64 Currency
        {
            get
            {
                return FD_Currency;

            }
            set
            {
                FD_Currency = value;
            }
        }
        public DateTime DateOfTransaction
        {
            get
            {
                return FD_Date;
            }
            set
            {
                FD_Date = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return FD_CreatedDate;
            }
            set
            {
                FD_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return FD_CreatedBy;
            }
            set
            {
                FD_CreatedBy = value;
            }
        }
        
        public DateTime UpdatedDate
        {
            get
            {
                return FD_UpdatedDate;
            }
            set
            {
                FD_UpdatedDate = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return FD_UpdatedBy;
            }
            set
            {
                FD_UpdatedBy = value;
            }
        }
        public string Notes
        {
            get
            {
                return FD_Notes;
            }
            set
            {
                FD_Notes = value;
            }
        }
        public string Project
        {
            get
            {
                return FD_Project;
            }
            set
            {
                FD_Project = value;
            }
        }
        public DataSet LoadFundersName()
        {

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundersDetailLoad");
        }
        //public DataSet LoadFundingType()
        //{

        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundTypeLoad");
        //}

        //public DataSet LoadCurency()
        //{

        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_Currency");
        //}
        public DataSet SaveFundDetails()
        {
            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;

            

            sqlParam[iCounter] = new SqlParameter("@fk_FunderId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_FK_FunderID;

            sqlParam[iCounter] = new SqlParameter("@Sponser_Type", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_SponserType;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Money);
            sqlParam[iCounter++].Value = FD_Amount;

            sqlParam[iCounter] = new SqlParameter("@Currency", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_Currency;

            sqlParam[iCounter] = new SqlParameter("@Notes", SqlDbType.VarChar,1000);
            sqlParam[iCounter++].Value = FD_Notes;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_CreatedBy;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_UpdatedBy;

            sqlParam[iCounter] = new SqlParameter("@DateOfTransaction", SqlDbType.Date);
            sqlParam[iCounter++].Value = FD_Date;

            //added by pawan--start
            sqlParam[iCounter] = new SqlParameter("@Project", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = FD_Project;
            //added by pawan--end



            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundDetailsInsert",sqlParam);
        }


        public DataSet UpdateFundDetails()
        {
            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_Id;

            sqlParam[iCounter] = new SqlParameter("@fk_FunderId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_FK_FunderID;

            sqlParam[iCounter] = new SqlParameter("@Sponser_Type", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_SponserType;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Money);
            sqlParam[iCounter++].Value = FD_Amount;

            sqlParam[iCounter] = new SqlParameter("@Currency", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_Currency;

            sqlParam[iCounter] = new SqlParameter("@Notes", SqlDbType.VarChar, 1000);
            sqlParam[iCounter++].Value = FD_Notes;
           
            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = FD_UpdatedBy;

            sqlParam[iCounter] = new SqlParameter("@DateOfTransaction", SqlDbType.Date);
            sqlParam[iCounter++].Value = FD_Date;
            //added by pawan--start
            sqlParam[iCounter] = new SqlParameter("@Project", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = FD_Project;
            //added by pawan--end

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundDetailsUpdate", sqlParam);
        }



        public DataSet LoadGridFundDetails()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoadFundDetailsGrid");
        }

    }
    
}
