﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SPARC;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace SPARC
{
   public partial class GroupTrans
    {
        private static string ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

       
       

        private Int64 g_Id;
        private int? g_fk_SlumId;
        private int? g_fk_HubId;
        private int? g_fk_DistrictId;
        private int? g_fk_StateId;
        private int? g_fk_CountryId;
        private int? g_fk_GroupId;
        private double g_Amount;
        private string g_Savingtype;
        private Int64 g_Createdby;
        private DateTime g_CreatedDate;
        private Int64 g_Updatedby;
        private DateTime g_UpdatedDate;
        private DateTime g_date;
        private string g_Remarks;
        private string g_tran_type;
        public Double Amount
        {
            get
            {
                return g_Amount;
            }
            set
            {
                g_Amount = value;
            }
        }

       public Int64 Id
       {
           get
           {
               return g_Id;
           }
           set
           {
               
               g_Id = value;
           }
       }
        public int? fk_HubId
        {
            get
            {
                return g_fk_HubId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_HubId = value;
            }
        }
        public int? fk_CountryId
        {
            get
            {
                return g_fk_CountryId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_CountryId = value;
            }
        }
        public int? fk_StateId
        {
            get
            {
                return g_fk_StateId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_StateId = value;
            }
        }   
        public int? fk_DistricId
        {
            get
            {
                return g_fk_DistrictId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_DistrictId = value;
            }
        }
        public int? fk_SlumId
        {
            get
            {
                return g_fk_SlumId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_SlumId = value;
            }
        }
        public int? GroupId
        {
            get
            {
                return g_fk_GroupId;
            }
            set
            {
                g_fk_GroupId = value;
            }
        }
       
        public DateTime Date
        {
            get
            {
                return g_date;
            }
            set
            {
                g_date = value;
            }
        }
        public String Savingtype
        {
            get
            {
                return g_Savingtype;
            }
            set
            {
                g_Savingtype = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return g_Createdby;
            }
            set
            {
                //z_bool.m_Updatedby = true;
                g_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return g_Updatedby;
            }
            set
            {
                //z_bool.m_Updatedby = true;
                g_Updatedby = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return g_CreatedDate;
            }
            set
            {
                // z_bool.m_CreatedDate = true;
                g_CreatedDate = value;
            }
        }


        
       

        public DateTime UpdatedDate
        {
            get
            {
                return g_UpdatedDate;
            }
            set
            {
                //z_bool.m_UpdatedDate = true;
                g_UpdatedDate = value;
            }
        }


        public String Remarks
        {
            get
            {
                return g_Remarks;
            }
            set
            {
                g_Remarks = value;
            }
        }
        public String Tran_type
        {
            get
            {
                return g_tran_type;
            }
            set
            {
                g_tran_type = value;
            }
        }

        public DataSet getdetails()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = g_Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GroupTransSelect]", sqlParam);
    }
        
        public DataSet UpdateGroupTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_Id;

            //sqlParam[iCounter] = new SqlParameter("@fk_HubId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_HubId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_CountryId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_CountryId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_StateId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_DistrictId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_DistrictId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_SlumId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_GroupId", SqlDbType.Int);
            sqlParam[iCounter++].Value = GroupId;

            sqlParam[iCounter] = new SqlParameter("@TransDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = Date;

            sqlParam[iCounter] = new SqlParameter("@Type", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = Savingtype;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = Amount;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Updatedby;

            //sqlParam[iCounter] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            //sqlParam[iCounter++].Value = g_CreatedDate;

           // sqlParam[iCounter] = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            //sqlParam[iCounter++].Value = g_UpdatedDate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.Text);
            sqlParam[iCounter++].Value = g_Remarks;

            //sqlParam[iCounter] = new SqlParameter("@tran_type", SqlDbType.Text);
            //sqlParam[iCounter++].Value = Tran_type;


            //SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_updateGroupTransaction", sqlParam);
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_updateGroupTransaction", sqlParam);
        }
        public Int32 SaveGroupTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_Id;

            //sqlParam[iCounter] = new SqlParameter("@fk_HubId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_HubId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_CountryId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_CountryId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_StateId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            //sqlParam[iCounter] = new SqlParameter("@fk_DistrictId", SqlDbType.NVarChar, 50);
            //sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_SlumId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_GroupId", SqlDbType.Int);
            sqlParam[iCounter++].Value = GroupId;

            sqlParam[iCounter] = new SqlParameter("@TransDate", SqlDbType.Date);
            sqlParam[iCounter++].Value = Date;

            sqlParam[iCounter] = new SqlParameter("@Type", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = Savingtype;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = Amount;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Updatedby;

            //sqlParam[iCounter] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            //sqlParam[iCounter++].Value = g_CreatedDate;


            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.Text);
            sqlParam[iCounter++].Value = Remarks;

            //sqlParam[iCounter] = new SqlParameter("@tran_type", SqlDbType.Text);
            //sqlParam[iCounter++].Value = Tran_type;

            


            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_InsertGrouptransaction", sqlParam));
          
        }

        public DataSet SearchSlumTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[0].Value = fk_SlumId;

            sqlParam[1] = new SqlParameter("@date", SqlDbType.Date);
            sqlParam[1].Value = Date;

           return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SearchSlumtrans", sqlParam);
        }
        public DataSet SearchGroupTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@fkGroupId", SqlDbType.Int);
            sqlParam[0].Value = GroupId;

            sqlParam[1] = new SqlParameter("@date", SqlDbType.Date);
            sqlParam[1].Value = Date;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SearchGrouptransaction", sqlParam);
        }
        public DataSet SearchGroupDateTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@transdate", SqlDbType.Date);
            sqlParam[0].Value = Date;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SearchDateGrouptrans", sqlParam);
        }
        public DataSet DeleteGroup(Int64 UserId)
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;




            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DeleteGroupTransdata", sqlParam);

        }

        public DataSet LoadTransactiontogrid()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

          
            sqlParam[0] = new SqlParameter("@fkSlumId", SqlDbType.Int);
            sqlParam[0].Value = g_fk_SlumId;

            sqlParam[1] = new SqlParameter("@fkGroupId", SqlDbType.Int);
            sqlParam[1].Value = g_fk_GroupId;

           

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GroupTransGridSelect", sqlParam);
        }
        public DataSet SearchGroupDateslumTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@transdate", SqlDbType.Date);
            sqlParam[0].Value = Date;

            sqlParam[1] = new SqlParameter("@fkSlumId", SqlDbType.Int);
            sqlParam[1].Value = fk_SlumId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SearchDateslumGrouptrans", sqlParam);
        }

        public DataSet LoadAllGroupUPFTrans()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@GroupId", SqlDbType.BigInt);
            sqlParam[0].Value = g_fk_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllUPFGroupSelectLoad", sqlParam);
        }

        public DataSet UpdateCurrentbalance()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = Id;

            sqlParam[1] = new SqlParameter("@fk_GroupId", SqlDbType.BigInt);
            sqlParam[1].Value = g_fk_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UpdateGroupUPFAccCurrentBalance", sqlParam);
        }
    }
}
