using System;
using System.Data;
using System.Data.OleDb;

namespace SPARC
{
	/// <summary>
	/// Generated Class for Table : UPFAccountTransactions.
	/// </summary>
	public class UPFAccountTransactions
	{
		private DateTime m_UpdatedDate;
		private int m_fk_Accounid;
		private DateTime m_TransactionDate;
		private DateTime m_CreatedDate;
		private int m_fk_AgentId;
		private int m_Createdby;
		private int m_Updatedby;
		private string m_Tran_type;
		private int m_fk_MemberId;
		private int m_Id;
		private double m_Amount;
		private string m_Remarks;
		
		
		public DateTime UpdatedDate
		{
			get
			{
				return m_UpdatedDate;
			}
			set
			{
				m_UpdatedDate = value;
			}
		}
		public int fk_Accounid
		{
			get
			{
				return m_fk_Accounid;
			}
			set
			{
				m_fk_Accounid = value;
			}
		}
		public DateTime TransactionDate
		{
			get
			{
				return m_TransactionDate;
			}
			set
			{
				m_TransactionDate = value;
			}
		}
		public DateTime CreatedDate
		{
			get
			{
				return m_CreatedDate;
			}
			set
			{
				m_CreatedDate = value;
			}
		}
		public int fk_AgentId
		{
			get
			{
				return m_fk_AgentId;
			}
			set
			{
				m_fk_AgentId = value;
			}
		}
		public int Createdby
		{
			get
			{
				return m_Createdby;
			}
			set
			{
				m_Createdby = value;
			}
		}
		public int Updatedby
		{
			get
			{
				return m_Updatedby;
			}
			set
			{
				m_Updatedby = value;
			}
		}
		public string Tran_type
		{
			get
			{
				return m_Tran_type;
			}
			set
			{
				m_Tran_type = value;
			}
		}
		public int fk_MemberId
		{
			get
			{
				return m_fk_MemberId;
			}
			set
			{
				m_fk_MemberId = value;
			}
		}
		public int Id
		{
			get
			{
				return m_Id;
			}
			set
			{
				m_Id = value;
			}
		}
		public double Amount
		{
			get
			{
				return m_Amount;
			}
			set
			{
				m_Amount = value;
			}
		}
		public string Remarks
		{
			get
			{
				return m_Remarks;
			}
			set
			{
				m_Remarks = value;
			}
		}
	}
}