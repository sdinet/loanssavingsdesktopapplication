﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SPARC;

namespace SPARC
{
    public partial class Group
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private int? g_fk_SlumId;
        private int? g_fk_HubId;
        private int? g_fk_DistrictId;
        private int? g_fk_StateId;
        private int? g_fk_CountryId;
        private string g_Name;
        private Int32 g_estyear;
        private Int32 g_male;
        private Int32 g_female;
        private double g_totsav;
        private double g_land;
        private double g_LFS;
        private double g_LFSR;
        private double g_int;
        private double g_cash;
        private double g_short;
        private String g_bank;
        private double g_bankchrg;
        private string g_busitype;
        private string g_busitypeother;
        private int? g_wdreasons;
        private Int64 g_GroupId;
      //  private string gr_Name;
        private int m_OperationType;
        private Int64 g_Createdby;
        private DateTime g_CreatedDate;
        private Int64 g_Updatedby;
        private DateTime g_UpdatedDate;
        private bool g_Isdeleted;
 
        public int? fk_HubId
        {
            get
            {
                return g_fk_HubId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_HubId = value;
            }
        }
        public int? fk_CountryId
        {
            get
            {
                return g_fk_CountryId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_CountryId = value;
            }
        }
        public int? fk_StateId
        {
            get
            {
                return g_fk_StateId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_StateId = value;
            }
        }
        public int? fk_DistricId
        {
            get
            {
                return g_fk_DistrictId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_DistrictId = value;
            }
        }
        public int? fk_SlumId
        {
            get
            {
                return g_fk_SlumId;
            }
            set
            {
                //z_bool.m_fk_SlumId = true;
                g_fk_SlumId = value;
            }
        }


        public string GroupName
        {
            get
            {
                return g_Name;
            }
            set
            {
                g_Name = value;
            }
        }

        //public string GrName
        //{
        //    get
        //    {
        //        return gr_Name;
        //    }
        //    set
        //    {
        //        gr_Name = value;
        //    }
        //}
        public Int32 Estyear
        {
            get
            {
                return g_estyear;
            }
            set
            {
                g_estyear = value;
            }

        }
        public Int32 Male
        {
            get
            {
                return g_male;
            }
            set
            {
                g_male = value;
            }

        }

        public Int32 Female
        {
            get
            {
                return g_female;
            }
            set
            {
                g_female = value;
            }

        }
        public double totsav
        {
            get
            {
                return g_totsav;
            }
            set
            {
                g_totsav = value;
            }

        }
        public double Land
        {
            get
            {
                return g_land;
            }
            set
            {
                g_land = value;
            }

        }
        public double LFSR
        {
            get
            {
                return g_LFSR;
            }
            set
            {
                g_LFSR = value;
            }

        }

        public double LFS
        {
            get
            {
                return g_LFS;
            }
            set
            {
                g_LFS = value;
            }

        }
        public double Interest
        {
            get
            {
                return g_int;
            }
            set
            {
                g_int = value;
            }

        }
        public double Cash
        {
            get
            {
                return g_cash;
            }
            set
            {
                g_cash = value;
            }

        }
        public double Short
        {
            get
            {
                return g_short;
            }
            set
            {
                g_short = value;
            }

        }

        public string Bank
        {
            get
            {
                return g_bank;
            }
            set
            {
                g_bank = value;
            }
        }

        public double Bankchrg
        {
            get
            {
                return g_bankchrg;
            }
            set
            {
                g_bankchrg = value;
            }

        }
        public string Busitype
        {
            get
            {
                return g_busitype;
            }
            set
            {
                g_busitype = value;
            }
        }
        public string Busitypeother
        {
            get
            {
                return g_busitypeother;
            }
            set
            {
                g_busitypeother = value;
            }
        }
        public int? Wdreasons
        {
            get
            {
                return g_wdreasons;
            }
            set
            {

                g_wdreasons = value;
            }
        }
        public Int64 GroupId
        {
            get
            {
                return g_GroupId;
            }
            set
            {
                // z_bool.m_Id = true;
                g_GroupId = value;
            }
        }
        public int OperationType
        {
            get
            {
                return m_OperationType;
            }
            set
            {
             //   z_bool.m_OperationType = true;
                m_OperationType = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return g_CreatedDate;
            }
            set
            {
               // z_bool.m_CreatedDate = true;
                g_CreatedDate = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return g_Isdeleted;
            }
            set
            {
                //z_bool.m_Isdeleted = true;
                g_Isdeleted = value;
            }
        }

        public Int64 Updatedby
        {
            get
            {
                return g_Updatedby;
            }
            set
            {
                //z_bool.m_Updatedby = true;
                g_Updatedby = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return g_Createdby;
            }
            set
            {
                //z_bool.m_Updatedby = true;
                g_Createdby = value;
            }
        }

        public DateTime UpdatedDate
        {
            get
            {
                return g_UpdatedDate;
            }
            set
            {
                //z_bool.m_UpdatedDate = true;
                g_UpdatedDate = value;
            }
        }
        public DataSet SearchGroup()
        {
            SqlParameter[] sqlParam = new SqlParameter[7];

            sqlParam[0] = new SqlParameter("@GroupName", SqlDbType.VarChar, 50);
            sqlParam[0].Value = g_Name;
            sqlParam[1] = new SqlParameter("@fk_CountryId", SqlDbType.Int);
            sqlParam[1].Value = (object)g_fk_CountryId ?? DBNull.Value;

            sqlParam[2] = new SqlParameter("@fk_StateId", SqlDbType.Int);
            sqlParam[2].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[3] = new SqlParameter("@fk_DistrictId", SqlDbType.Int);
            sqlParam[3].Value = (object)g_fk_DistrictId ?? DBNull.Value;

            sqlParam[4] = new SqlParameter("@fk_HubId", SqlDbType.Int);
            sqlParam[4].Value = (object)g_fk_HubId ?? DBNull.Value;

            sqlParam[5] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[5].Value = (object)g_fk_SlumId ?? DBNull.Value;

            sqlParam[6] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[6].Value = (object)g_GroupId ?? DBNull.Value;



            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GroupSearch]", sqlParam);

        }
        public DataSet LoadGroupById()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[0].Value = g_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GroupInfoSelect]", sqlParam);
        }
        public DataSet GetGroupDetails()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[0].Value = g_GroupId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GroupInfoSelect]", sqlParam);
        }
        //public DataSet InsertMemberDetailsToGrid()
        //{
        //    Group objgp = new Group();
        //    objgp.GroupId = GlobalValues.GroupId;
        //    DataSet dsgroupDetailsGrid = objgp.GetGroupDetails();
        //    if (dsgroupDetailsGrid.Tables[0].Rows.Count > 0)
        //    {
        //        dggroupdetails.DataSource = dsgroupDetailsGrid.Tables[0];
        //    }
        //    else
        //    {
        //        dggroupdetails.DataSource = null;
        //    }
        //}
        public int SaveGroup()
        {
            SqlParameter[] sqlParam = new SqlParameter[27];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_GroupId;

            sqlParam[iCounter] = new SqlParameter("@GroupName", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_Name.Length > 0 ? g_Name : null;

            sqlParam[iCounter] = new SqlParameter("@fk_HubId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_HubId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_CountryId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_CountryId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_StateId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_DistrictId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fkSlumId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_SlumId ?? DBNull.Value;


            sqlParam[iCounter] = new SqlParameter("Estyear", SqlDbType.Int, 50);
            sqlParam[iCounter++].Value = g_estyear;

            sqlParam[iCounter] = new SqlParameter("Male", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_male;

            sqlParam[iCounter] = new SqlParameter("Female", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_female;

            sqlParam[iCounter] = new SqlParameter("totsav", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_totsav;

            sqlParam[iCounter] = new SqlParameter("Land", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_land;

            sqlParam[iCounter] = new SqlParameter("LFS", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_LFS;

            sqlParam[iCounter] = new SqlParameter("LFSR", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_LFSR;

            sqlParam[iCounter] = new SqlParameter("Interest", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_int;

            sqlParam[iCounter] = new SqlParameter("Cash", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_cash;

            sqlParam[iCounter] = new SqlParameter("Short", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_short;

            sqlParam[iCounter] = new SqlParameter("Bank", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_bank;

            sqlParam[iCounter] = new SqlParameter("Bankchrg", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_bankchrg;

            sqlParam[iCounter] = new SqlParameter("Busitype", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_busitype;

            sqlParam[iCounter] = new SqlParameter("Wdreasons", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_wdreasons;

            sqlParam[iCounter] = new SqlParameter("Busitypeother", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_busitypeother;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = g_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = g_CreatedDate;



            sqlParam[iCounter] = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = g_UpdatedDate;

            sqlParam[iCounter] = new SqlParameter("@Isdeleted", SqlDbType.Bit);
            sqlParam[iCounter++].Value = g_Isdeleted;


            //string spName;                  
            //if (g_GroupId == 0)
            //    spName = "GroupInfoInsert";
            //else
            //    spName = "usp_GroupinfoUpdate";


            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "GroupInfoInsert", sqlParam));




        }

        public int UpdateGroup()
        {
            SqlParameter[] sqlParam = new SqlParameter[22];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_GroupId;

            sqlParam[iCounter] = new SqlParameter("@GroupName", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_Name.Length > 0 ? g_Name : null;

            sqlParam[iCounter] = new SqlParameter("@fk_HubId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_HubId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_CountryId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_CountryId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_StateId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_DistrictId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fkSlumId", SqlDbType.NVarChar, 50);
            sqlParam[iCounter++].Value = (object)g_fk_SlumId ?? DBNull.Value;


            sqlParam[iCounter] = new SqlParameter("Estyear", SqlDbType.Int, 50);
            sqlParam[iCounter++].Value = g_estyear;

            sqlParam[iCounter] = new SqlParameter("Male", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_male;

            sqlParam[iCounter] = new SqlParameter("Female", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_female;

            sqlParam[iCounter] = new SqlParameter("totsav", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_totsav;

            sqlParam[iCounter] = new SqlParameter("Land", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_land;

            sqlParam[iCounter] = new SqlParameter("LFS", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_LFS;

            sqlParam[iCounter] = new SqlParameter("LFSR", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_LFSR;

            sqlParam[iCounter] = new SqlParameter("Interest", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_int;

            sqlParam[iCounter] = new SqlParameter("Cash", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_cash;

            sqlParam[iCounter] = new SqlParameter("Short", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_short;

            sqlParam[iCounter] = new SqlParameter("Bank", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_bank;

            sqlParam[iCounter] = new SqlParameter("Bankchrg", SqlDbType.Float);
            sqlParam[iCounter++].Value = g_bankchrg;

            sqlParam[iCounter] = new SqlParameter("Busitype", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_busitype;

            sqlParam[iCounter] = new SqlParameter("Wdreasons", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_wdreasons;

            sqlParam[iCounter] = new SqlParameter("Busitypeother", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_busitypeother;



            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_GroupinfoUpdate", sqlParam));




        }

        public DataSet GetGrpcombo()
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@fk_slumId", SqlDbType.Int);
                param[0].Value = fk_SlumId;


                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "GetGroupvalues", param);
            }
            catch (Exception ex)
            {
                return null;


            }

        }
        public DataSet DeleteGroup(Int64 UserId)
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_GroupId;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;




            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DeleteGroupdata", sqlParam);

        }
        public DataSet FetchGroup()
        {
            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@GroupName", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = g_Name;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_GroupId;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[iCounter++].Value = g_fk_SlumId;

            //sqlParam[iCounter] = new SqlParameter("@OperationType", SqlDbType.BigInt);
            //sqlParam[iCounter++].Value = m_OperationType;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllGroupSelect", sqlParam);
        }

        public DataSet Displaydata()
        {
            SqlParameter[] sqlParam = new SqlParameter[7];

            sqlParam[0] = new SqlParameter("@GroupName", SqlDbType.VarChar, 50);
            sqlParam[0].Value = g_Name;
            sqlParam[1] = new SqlParameter("@fk_CountryId", SqlDbType.Int);
            sqlParam[1].Value = (object)g_fk_CountryId ?? DBNull.Value;

            sqlParam[2] = new SqlParameter("@fk_StateId", SqlDbType.Int);
            sqlParam[2].Value = (object)g_fk_StateId ?? DBNull.Value;

            sqlParam[3] = new SqlParameter("@fk_DistrictId", SqlDbType.Int);
            sqlParam[3].Value = (object)g_fk_DistrictId ?? DBNull.Value;

            sqlParam[4] = new SqlParameter("@fk_HubId", SqlDbType.Int);
            sqlParam[4].Value = (object)g_fk_HubId ?? DBNull.Value;

            sqlParam[5] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[5].Value = (object)g_fk_SlumId ?? DBNull.Value;

            sqlParam[6] = new SqlParameter("@GroupId", SqlDbType.Int);
            sqlParam[6].Value = (object)g_GroupId ?? DBNull.Value;



            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "[usp_GroupSelect]", sqlParam);
        }
   
    }
}
   
        



