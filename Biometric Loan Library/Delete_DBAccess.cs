﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace SPARC
{
    static class DBAccess
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public static int CreateAgent(Agent objAgent)
        {
            SqlConnection objConn = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand objCmd = new SqlCommand(DBQueries.CreateAgent, objConn);
                objCmd.Parameters.AddWithValue("@AName", objAgent.Name);
                objCmd.Parameters.AddWithValue("@AContactNumber", objAgent.ContactNumber);

                objConn.Open();
                return Convert.ToInt32(objCmd.ExecuteScalar());
            }
            finally
            {
                objConn.Close();
                objConn = null;
                
            }
        }
        public static int SearchAgent(Agent objAgent)
        {
            SqlConnection objConn = new SqlConnection(ConnectionString);
            try
            {
                SqlCommand objCmd = new SqlCommand(DBQueries.CreateAgent, objConn);
                objCmd.Parameters.AddWithValue("@AName", objAgent.Name);
                objCmd.Parameters.AddWithValue("@AContactNumber", objAgent.ContactNumber);

                objConn.Open();
                return Convert.ToInt32(objCmd.ExecuteScalar());
            }
            finally
            {
                objConn.Close();
                objConn = null;

            }
        }
    }
}
