﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : DayBook.
    /// </summary>
    public class DayBook
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private DateTime m_UpdatedDate;
        private DateTime m_TransactionDate;
        private string m_Particulars;
        private string m_Remarks;
        private DateTime m_CreatedDate;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private int m_Id;
        private string m_Tran_type;
        private double m_Amount;
        private int m_fk_HeadOfAccountId;

        public DayBook()
        {
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public DateTime TransactionDate
        {
            get
            {
                return m_TransactionDate;
            }
            set
            {
                m_TransactionDate = value;
            }
        }
        public string Particulars
        {
            get
            {
                return m_Particulars;
            }
            set
            {
                m_Particulars = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                m_Remarks = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        public string Tran_type
        {
            get
            {
                return m_Tran_type;
            }
            set
            {
                m_Tran_type = value;
            }
        }
        public double Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                m_Amount = value;
            }
        }
        public int fk_HeadOfAccountId
        {
            get
            {
                return m_fk_HeadOfAccountId;
            }
            set
            {
                m_fk_HeadOfAccountId = value;
            }
        }

        public int GenerateDayBook()
        {
            SqlParameter[] sqlParam = new SqlParameter[5];

            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@dtTransactionDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_TransactionDate;

            sqlParam[iCounter] = new SqlParameter("@Particulars", SqlDbType.NVarChar, 750);
            sqlParam[iCounter++].Value = m_Particulars;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_DayBookInsert", sqlParam));

        }
    }
}