﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
namespace SPARC
{
    public class Funders
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private Int64 m_Id;
        private string m_FunderName;
        private string m_Address1;
        private string m_Address2;
        private Int32 m_Country;
        private string m_Telephone;
        private string m_City;
        private string m_Zip;
        private Int32 m_State;
        private string m_Fax;
        private string m_Email;
        private string m_Contact_Person;
        private string m_ContactNumber;
        private bool m_Isdeleted;
        private DateTime m_CreatedDate;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private DateTime m_UpdatedDate;
        private Int32 m_Hub;
       

        public Funders()
        {
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }

        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public string FundersName
        {
            get
            {
                return m_FunderName;
            }
            set
            {
                m_FunderName = value;
            }
        }
        public string Address1
        {
            get
            {
                return m_Address1;
            }
            set
            {
                m_Address1 = value;
            }
        }
        public string Address2
        {
            get
            {
                return m_Address2;
            }
            set
            {
                m_Address2 = value;
            }
        }
        public Int32 Country
        {
            get
            {
                return m_Country;
            }
            set
            {
                m_Country = value;
            }
        }
        public string Telephone
        {
            get
            {
                return m_Telephone;
            }
            set
            {
                m_Telephone = value;
            }
        }
        public string City
        {
            get
            {
                return m_City;
            }
            set
            {
                m_City = value;
            }
        }
        public string Zip
        {
            get
            {
                return m_Zip;
            }
            set
            {
                m_Zip = value;
            }
        }

        public Int32 State
        {
            get
            {
                return m_State;
            }
            set
            {
                m_State = value;
            }
        }

        public string Fax
        {
            get
            {
                return m_Fax;
            }
            set
            {
                m_Fax = value;
            }
        }

        public string Email
        {
            get
            {
                return m_Email;
            }
            set
            {
                m_Email = value;
            }
        }

        public string ContactPerson
        {
            get
            {
                return m_Contact_Person;
            }
            set
            {
                m_Contact_Person = value;
            }
        }
        public string ContactNumber
        {
            get
            {
                return m_ContactNumber;
            }
            set
            {
                m_ContactNumber = value;
            }
        }


        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public Int64 FundersID
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        //edited by Archana
        public Int32 Hub
        {
            get
            {
                return m_Hub;
            }
            set
            {
                m_Hub = value;
            }
        }
        // edited by Archana
        public DataSet SaveFunders()
        {

            SqlParameter[] sqlParam = new SqlParameter[15];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@FunderName", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = m_FunderName;

            sqlParam[iCounter] = new SqlParameter("@Address1", SqlDbType.VarChar, 100);
            sqlParam[iCounter++].Value = m_Address1;

            sqlParam[iCounter] = new SqlParameter("@Address2", SqlDbType.VarChar, 100);
            sqlParam[iCounter++].Value = m_Address2;

            sqlParam[iCounter] = new SqlParameter("@City", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_City;

            sqlParam[iCounter] = new SqlParameter("@State", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_State;

            sqlParam[iCounter] = new SqlParameter("@Country", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Country;

            sqlParam[iCounter] = new SqlParameter("@Zip", SqlDbType.VarChar, 10);
            sqlParam[iCounter++].Value = m_Zip;

            sqlParam[iCounter] = new SqlParameter("@Telephone", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Telephone;

            sqlParam[iCounter] = new SqlParameter("@Fax", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Fax;

            sqlParam[iCounter] = new SqlParameter("@EmailAddress", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Email;

            sqlParam[iCounter] = new SqlParameter("@ContactPerson", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Contact_Person;

            sqlParam[iCounter] = new SqlParameter("@ContactNumber", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_ContactNumber;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            //edited by Archana--start

            sqlParam[iCounter] = new SqlParameter("@Hub", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Hub;


            //edited by Archana--end
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundersInsert", sqlParam);

        }

        public DataSet UpdateFunders()
        {

            SqlParameter[] sqlParam = new SqlParameter[15];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@FunderName", SqlDbType.NVarChar, 100);
            sqlParam[iCounter++].Value = m_FunderName;

            sqlParam[iCounter] = new SqlParameter("@Address1", SqlDbType.VarChar, 100);
            sqlParam[iCounter++].Value = m_Address1;

            sqlParam[iCounter] = new SqlParameter("@Address2", SqlDbType.VarChar, 100);
            sqlParam[iCounter++].Value = m_Address2;

            sqlParam[iCounter] = new SqlParameter("@City", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_City;

            sqlParam[iCounter] = new SqlParameter("@State", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_State;

            sqlParam[iCounter] = new SqlParameter("@Country", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Country;

            sqlParam[iCounter] = new SqlParameter("@Zip", SqlDbType.VarChar, 10);
            sqlParam[iCounter++].Value = m_Zip;

            sqlParam[iCounter] = new SqlParameter("@Telephone", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Telephone;

            sqlParam[iCounter] = new SqlParameter("@Fax", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Fax;

            sqlParam[iCounter] = new SqlParameter("@EmailAddress", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Email;

            sqlParam[iCounter] = new SqlParameter("@ContactPerson", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Contact_Person;

            sqlParam[iCounter] = new SqlParameter("@ContactNumber", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_ContactNumber;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;
            //edited by Archana--start
            sqlParam[iCounter] = new SqlParameter("@Hub", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Hub;

            //edited by Archana--end

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundersUpdate", sqlParam);

        }

        public DataSet LoadFunders()
        {
            SqlParameter[] sqlParam = new SqlParameter[1];
            //int iCounter = 0;
            sqlParam[0] = new SqlParameter("@FunderName", SqlDbType.NVarChar, 100);
            sqlParam[0].Value = (object)m_FunderName ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundersLoad", sqlParam);
        }

    }
}
