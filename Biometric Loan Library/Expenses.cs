﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SPARC
{
    public class Expenses
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        
        private Int64 m_ExpenseId;
        private string m_Description;
        private string m_Remarks;
        private DateTime m_ExpenseDate;
        private double m_Amount;
        private Int64 m_fk_Category;
        private Int64 m_fk_ModeOfPayment;
        private bool m_Isdeleted;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private DateTime m_UpdatedDate;
        private DateTime m_CreatedDate;

        public Expenses()
        {
            
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                m_Remarks = value;
            }
        }
        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public double Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                m_Amount = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public DateTime ExpenseDate
        {
            get
            {
                return m_ExpenseDate;
            }
            set
            {
                m_ExpenseDate = value;
            }
        }
        public Int64 ExpenseId
        {
            get
            {
                return m_ExpenseId;
            }
            set
            {
                m_ExpenseId = value;
            }
        }
        public Int64 Category
        {
            get
            {
                return m_fk_Category;
            }
            set
            {
                m_fk_Category = value;
            }
        }
        public Int64 ModeOfPayment
        {
            get
            {
                return m_fk_ModeOfPayment;
            }
            set
            {
                m_fk_ModeOfPayment = value;
            }
        }

        public Int64 SaveExpenses(Int64 iAction)
        {
            SqlParameter[] sqlParam = new SqlParameter[11];

            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@ExpenseId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_ExpenseId;

            sqlParam[iCounter] = new SqlParameter("@ExpenseDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_ExpenseDate;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@fk_ModeOfPayment", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_ModeOfPayment;

            sqlParam[iCounter] = new SqlParameter("@fk_Category", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_Category;

            sqlParam[iCounter] = new SqlParameter("@Description", SqlDbType.NVarChar, 500);
            sqlParam[iCounter++].Value = m_Description;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 1500);
            sqlParam[iCounter++].Value = m_Remarks.Length > 0 ? m_Remarks: DBNull.Value.ToString();

            sqlParam[iCounter] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_CreatedDate;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;
            
            sqlParam[iCounter] = new SqlParameter("@UpdatedDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_UpdatedDate;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;
            
            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_ExpensesInsert", sqlParam));

        }        
        public DataSet LoadExpenses(DateTime FromDate, DateTime ToDate)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];

            sqlParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            sqlParam[0].Value = FromDate;

            sqlParam[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            sqlParam[1].Value = ToDate;

            sqlParam[2] = new SqlParameter("@Category", SqlDbType.BigInt);
            sqlParam[2].Value = m_fk_Category;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_ExpensesSelect", sqlParam);
        }

        public DataSet DeleteExpenses(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@ExpenseId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_ExpenseId;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_ExpensesDelete", sqlParam);
        }
    }
}
