﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Collections;

namespace SPARC
{
   public  class FundAlloc
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        private Int64 FA_Id;
        private Int64 FA_fk_FunderTransactionId;
        private Int64 FA_fk_CenterId;
        private double FA_Amount;
        private Int64 FA_Currency;
        private bool FA_Isdeleted;
        private DateTime FA_CreatedDate;
        private Int64 FA_Createdby;
        private Int64 FA_Updatedby;
        private DateTime FA_UpdatedDate;
        private double FA_amountForValid;
        private double FA_TransactionAmount;
        private Int64 FA_TransactionCurrency;
        private DateTime FA_TransactionDate;
        private string FA_TransactionNote;
        private Int64 FA_TransCreatedBy;
        private Int64 FA_TransUpdatedBy;
        private Int64 FA_TransactionCenter;
        private double FA_TOTRemainingamount;
        private string FA_Project;

        private Int64 FA_cntr;
        
        public FundAlloc()
        {
        }
        public double TransactionAmounts
        {
            get
            {
                return FA_TransactionAmount;
            }
            set
            {
                FA_TransactionAmount = value;
            }
        }
        public Int64 FundCurrency
        {
            get
            {
                return FA_Currency;
            }
            set
            {
                FA_Currency = value;
            }
            
        }
        public string transactionproject
        {
            get
            {
                return FA_Project;
            }
            set
            {
                FA_Project = value;
            }
        }


        public Int64 TransactionCenter
        {
            get
            {
                return FA_TransactionCenter;
            }
            set
            {
                FA_TransactionCenter = value;
            }
        }
        public Int64 TransactionCreated
        {
            get
            {
                return FA_TransCreatedBy;
            }
            set
            {
                FA_TransCreatedBy = value;
                 
                      
            }
        }
        public Int64 TransactionUpdated
        {
            get
            {
                return FA_TransUpdatedBy;
            }
            set
            {
                FA_TransUpdatedBy = value;
            }
        }
        public double Amountvalue
        {
            get
            {
                return FA_amountForValid;
            }
            set
            {
                FA_amountForValid = value;
            }

        }
        public bool Isdeleted
        {
            get
            {
                return FA_Isdeleted;
            }
            set
            {
                FA_Isdeleted = value;
            }
        }

        public Int64 FundUpdatedBy
        {
            get
            {
                return FA_Updatedby;
            }
            set
            {
                FA_Updatedby = value;
            }
        }
        public Int64 FundCreatedBy
        {
            get
            {
                return FA_Createdby;
            }
            set
            {
                FA_Createdby = value;
            }
        }

        public Double FundAmount
        {
            get
            {
                return FA_Amount;
            }
            set
            {
                FA_Amount = value;
            }
        }
        public Int64 Fund_CenteredID
        {
            get
            {
                return FA_fk_CenterId;
            }
            set
            {
                FA_fk_CenterId = value;
            }
        }
        public Int64 FundID
        {
            get
            {
                return FA_Id;
            }
            set
            {
                FA_Id = value;
            }

        }
        public Int64 FunderTransactionID
        {
            get
            {
                return FA_fk_FunderTransactionId;
            }
            set
            {
                FA_fk_FunderTransactionId = value;
            }
        }

        public DateTime UpdatedDate
        {
            get
            {
                return FA_UpdatedDate;
            }
            set
            {
                FA_UpdatedDate = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return FA_CreatedDate;
            }
            set
            {
                FA_CreatedDate = value;
            }
        }
       public string TransactionNotes
       {
           get
           {
               return FA_TransactionNote;
           }
           set
           {
               FA_TransactionNote=value;
           }
       }
       public DateTime TransactionDate
       {
           get
           {
               return FA_TransactionDate;
           }
           set
           {
               FA_TransactionDate = value;
           }
       }
       public Int64 TransactionCurrency
       {
           get
           {
               return FA_TransactionCurrency;
           }
           set
           {
               FA_TransactionCurrency = value;
           }
       }
       public double TransactionAmount
       {
           get
           {
               return FA_TransactionAmount;
           }
           set
           {
               FA_TransactionAmount = value;
           }
       }
       public double Remainingamount
       {
           get
           {
               return FA_TOTRemainingamount;
           }
           set
           {
               FA_TOTRemainingamount = value;
           }
       }
       

       // added by pawan--start
        public DataSet LoadFundersForAlloc()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_Allocation_Funder");
        }

        public DataSet LoadFundersForAlloc2()
        {
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_Allocation_Funderss");
        }

       //added by pawan--end
        public DataSet LoadFundersDetails()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];  
            //int iCounter = 0;
            sqlParam[0] = new SqlParameter("@fk_FunderId", SqlDbType.NVarChar, 100);
            sqlParam[0].Value = (object)FA_Id ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllocLoadfunderDetails", sqlParam);
        }
       // public DataSet LoadCenters()
       // {
       //     return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FillCenterCombo");
       // }
       // public DataSet LoadCurrency()
       // {
       //     return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_Currency");
       //}
        //public DataSet AmountValidation()
        //{
        //    //SqlParameter[] sqlparam = new SqlParameter[2];
        //    //sqlparam[0] = new SqlParameter("@FA_AmountForValidation", SqlDbType.Money);
        //    //sqlparam[0].Value = FA_amountForValid;
        //    //sqlparam[1] = new SqlParameter("@ID", SqlDbType.BigInt);
        //    //sqlparam[1].Value = (object)FA_Id ?? DBNull.Value;
        //    //return Convert.ToInt32(SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundAllocationAmountValid",sqlparam));

        //}
        public Int32 AmountValidation()
        {
            SqlParameter[] sqlparam = new SqlParameter[2];
            sqlparam[0] = new SqlParameter("@FA_AmountForValidation", SqlDbType.Money);
            sqlparam[0].Value = FA_amountForValid;
            sqlparam[1] = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlparam[1].Value = (object)FA_Id ?? DBNull.Value;


            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_FundAllocationAmountValidCheck", sqlparam));
        }
       //edited by pawan--start
        public Int32 TransactionAmountINSERTION()
        {


            SqlParameter[] sqlparam = new SqlParameter[10];

            sqlparam[0] = new SqlParameter("@FA_TransactionID", SqlDbType.BigInt);
            sqlparam[0].Value = FA_fk_FunderTransactionId;

            sqlparam[1] = new SqlParameter("@FA_TransactionCenter", SqlDbType.BigInt);
            sqlparam[1].Value = FA_TransactionCenter;

            sqlparam[2] = new SqlParameter("@FA_TransactionAmount", SqlDbType.Money);
            sqlparam[2].Value = FA_TransactionAmount;

            sqlparam[3] = new SqlParameter("@FA_TransactionCurrency", SqlDbType.BigInt);
            sqlparam[3].Value = (object)FA_TransactionCurrency ?? DBNull.Value;

            sqlparam[4] = new SqlParameter("@FA_TransactionDate", SqlDbType.DateTime);
            sqlparam[4].Value = FA_TransactionDate;

            sqlparam[5] = new SqlParameter("@FA_TransactionNote", SqlDbType.VarChar);
            sqlparam[5].Value = FA_TransactionNote;

            sqlparam[6] = new SqlParameter("@FA_TOTTransactionAmount", SqlDbType.Money);
            sqlparam[6].Value = FA_TOTRemainingamount; 
                                  
            sqlparam[7] = new SqlParameter("@FA_TransCreatedBy", SqlDbType.BigInt);
            sqlparam[7].Value = FA_TransCreatedBy;

            sqlparam[8] = new SqlParameter("@FA_TransUpdatedBy", SqlDbType.BigInt);
            sqlparam[8].Value = FA_TransUpdatedBy;


            sqlparam[9] = new SqlParameter("@FA_Project", SqlDbType.VarChar);
            sqlparam[9].Value = FA_Project;

      

            return Convert.ToInt32(SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "usp_AllocationTransInsert", sqlparam));
        }

       //edited by pawan--end
        
        public DataSet LoadUnallocatedAmount()
        {
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@FA_id", SqlDbType.BigInt);
            sqlparam[0].Value = FA_Id;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UnAllocatedAmount",sqlparam);
        }

        //public DataSet LoadFundAllocationForGrid()
        //{
        //    return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllocationTransInsert");
        //}

       //Added by pawan---start
        public DataSet LoadFundDisplayinGrid(int funderId)
        {
            SqlParameter[] sqlparam = new SqlParameter[1];
            sqlparam[0] = new SqlParameter("@funderId", SqlDbType.BigInt);
            sqlparam[0].Value = funderId;
            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_FundsAllocatedDisplay", sqlparam);
        }
       //added by pawan--end
    }
}
