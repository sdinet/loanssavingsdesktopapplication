﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : SavingAccountTransactions.
    /// </summary>
    public class SavingAccountTransactions
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private string m_Remarks;
        private int m_fk_AgentId;
        private Int64 m_Createdby;
        private DateTime m_UpdatedDate;
        private Int64 m_fk_MemberId;
        private string m_Tran_type;
        private Int64 m_fk_Accountid;
        private DateTime m_CreatedDate;
        private Int64 m_Updatedby;
        private double m_Amount;
        private DateTime m_TransactionDate;
        private int m_Id;
        private string m_PassbookNo;
        private int? m_fk_SlumId;
        private Int64 m_SavingType;

        private Int64 m_LoanType;        
        
        private SavingAccountTransactions_Criteria z_WhereClause;

        public struct IsDirty_SavingAccountTransactions
        {
            public bool m_Remarks;
            public bool m_fk_AgentId;
            public bool m_Createdby;
            public bool m_UpdatedDate;
            public bool m_fk_MemberId;
            public bool m_Tran_type;
            public bool m_fk_Accountid;
            public bool m_CreatedDate;
            public bool m_Updatedby;
            public bool m_Amount;
            public bool m_TransactionDate;
            public bool m_Id;
            public bool m_PassbookNo;
            public bool m_fk_SlumId;
            public bool m_SavingType;
            public bool m_LoanType;
        }
        public IsDirty_SavingAccountTransactions z_bool;

        public SavingAccountTransactions()
        {
            z_WhereClause = new SavingAccountTransactions_Criteria();
        }

        public Int64 LoanType
        {
            get { return m_LoanType; }
            set { m_LoanType = value; }
        }

        public Int64 SavingType
        {
            get { return m_SavingType; }
            set { m_SavingType = value; }
        }

        public int? Fk_SlumId
        {
            get 
            {
                    return m_fk_SlumId; 
            }
            set
            {
                     z_bool.m_fk_SlumId = true;
                     m_fk_SlumId = value; 
            }
        }

        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                z_bool.m_Remarks = true;
                m_Remarks = value;
            }
        }
        public int fk_AgentId
        {
            get
            {
                return m_fk_AgentId;
            }
            set
            {
                z_bool.m_fk_AgentId = true;
                m_fk_AgentId = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
            }
        }
        public Int64 fk_MemberId
        {
            get
            {
                return m_fk_MemberId;
            }
            set
            {
                z_bool.m_fk_MemberId = true;
                m_fk_MemberId = value;
            }
        }
        public string Tran_type
        {
            get
            {
                return m_Tran_type;
            }
            set
            {
                z_bool.m_Tran_type = true;
                m_Tran_type = value;
            }
        }
        public Int64 fk_Accountid
        {
            get
            {
                return m_fk_Accountid;
            }
            set
            {
                z_bool.m_fk_Accountid = true;
                m_fk_Accountid = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
            }
        }
        public double Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                z_bool.m_Amount = true;
                m_Amount = value;
            }
        }
        public DateTime TransactionDate
        {
            get
            {
                return m_TransactionDate;
            }
            set
            {
                z_bool.m_TransactionDate = true;
                m_TransactionDate = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
            }
        }

        public string PassbookNo
        {
            get 
            {
                return m_PassbookNo; 
            }
            set
            {
                z_bool.m_PassbookNo = true;
                m_PassbookNo = value; 
            }
        }
        public SavingAccountTransactions_Criteria Where
        {
            get
            {
                return z_WhereClause;
            }
            set
            {
                z_WhereClause = value;
            }
        }

        public string Insert()
        {
            string z_sep = "";
            string SQL = "INSERT INTO SavingAccountTransactions ( ";
            if (z_bool.m_Remarks)
            {
                SQL += z_sep + "Remarks";
                z_sep = " , ";
            }
            if (z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "fk_AgentId";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate";
                z_sep = " , ";
            }
            if (z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "fk_MemberId";
                z_sep = " , ";
            }
            if (z_bool.m_Tran_type)
            {
                SQL += z_sep + "Tran_type";
                z_sep = " , ";
            }
            if (z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "fk_Accountid";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby";
                z_sep = " , ";
            }
            if (z_bool.m_Amount)
            {
                SQL += z_sep + "Amount";
                z_sep = " , ";
            }
            if (z_bool.m_TransactionDate)
            {
                SQL += z_sep + "TransactionDate";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id";
                z_sep = " , ";
            }
            SQL += ") VALUES (";
            z_sep = "";
            if (z_bool.m_Remarks)
            {
                SQL += z_sep + "'" + m_Remarks + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "'" + m_fk_AgentId + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "'" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "'" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "'" + m_fk_MemberId + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Tran_type)
            {
                SQL += z_sep + "'" + m_Tran_type + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "'" + m_fk_Accountid + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "'" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "'" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Amount)
            {
                SQL += z_sep + "'" + m_Amount + "'";
                z_sep = " , ";
            }
            if (z_bool.m_TransactionDate)
            {
                SQL += z_sep + "'" + m_TransactionDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "'" + m_Id + "'";
                z_sep = " , ";
            }
            SQL += ")";
            return SQL;
        }
        public string Update()
        {
            string z_sep = "";
            string SQL = "UPDATE SavingAccountTransactions SET ";
            if (z_bool.m_Remarks)
            {
                SQL += z_sep + "Remarks='" + m_Remarks + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "fk_AgentId='" + m_fk_AgentId + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "fk_MemberId='" + m_fk_MemberId + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Tran_type)
            {
                SQL += z_sep + "Tran_type='" + m_Tran_type + "'";
                z_sep = " , ";
            }
            if (z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "fk_Accountid='" + m_fk_Accountid + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Amount)
            {
                SQL += z_sep + "Amount='" + m_Amount + "'";
                z_sep = " , ";
            }
            if (z_bool.m_TransactionDate)
            {
                SQL += z_sep + "TransactionDate='" + m_TransactionDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + m_Id + "'";
                z_sep = " , ";
            }
            z_sep = " WHERE ";
            if (z_WhereClause.z_bool.m_Remarks)
            {
                SQL += z_sep + "Remarks='" + z_WhereClause.Remarks + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "fk_AgentId='" + z_WhereClause.fk_AgentId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "fk_MemberId='" + z_WhereClause.fk_MemberId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Tran_type)
            {
                SQL += z_sep + "Tran_type='" + z_WhereClause.Tran_type + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "fk_Accountid='" + z_WhereClause.fk_Accountid + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Amount)
            {
                SQL += z_sep + "Amount='" + z_WhereClause.Amount + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_TransactionDate)
            {
                SQL += z_sep + "TransactionDate='" + z_WhereClause.TransactionDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string Delete()
        {
            string z_sep = " WHERE ";
            string SQL = "DELETE FROM SavingAccountTransactions";
            if (z_WhereClause.z_bool.m_Remarks)
            {
                SQL += z_sep + "Remarks='" + z_WhereClause.Remarks + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "fk_AgentId='" + z_WhereClause.fk_AgentId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "fk_MemberId='" + z_WhereClause.fk_MemberId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Tran_type)
            {
                SQL += z_sep + "Tran_type='" + z_WhereClause.Tran_type + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "fk_Accountid='" + z_WhereClause.fk_Accountid + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Amount)
            {
                SQL += z_sep + "Amount='" + z_WhereClause.Amount + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_TransactionDate)
            {
                SQL += z_sep + "TransactionDate='" + z_WhereClause.TransactionDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string SearchSQL()
        {
            string z_sep = " WHERE ";
            string SQL = "SELECT * FROM SavingAccountTransactions";
            if (z_WhereClause.z_bool.m_Remarks)
            {
                SQL += z_sep + "Remarks='" + z_WhereClause.Remarks + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_AgentId)
            {
                SQL += z_sep + "fk_AgentId='" + z_WhereClause.fk_AgentId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_MemberId)
            {
                SQL += z_sep + "fk_MemberId='" + z_WhereClause.fk_MemberId + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Tran_type)
            {
                SQL += z_sep + "Tran_type='" + z_WhereClause.Tran_type + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_fk_Accountid)
            {
                SQL += z_sep + "fk_Accountid='" + z_WhereClause.fk_Accountid + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Amount)
            {
                SQL += z_sep + "Amount='" + z_WhereClause.Amount + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_TransactionDate)
            {
                SQL += z_sep + "TransactionDate='" + z_WhereClause.TransactionDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }

        public DataSet GetSavingsDepositTransactions()
        {
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[4];

                sqlParam[0] = new SqlParameter("@MemberPkId", SqlDbType.BigInt);
                sqlParam[0].Value = m_fk_MemberId;

                sqlParam[1] = new SqlParameter("@TransType", SqlDbType.NVarChar, 3);
                sqlParam[1].Value = m_Tran_type;

                sqlParam[2] = new SqlParameter("@SavingType", SqlDbType.BigInt);
                sqlParam[2].Value = m_SavingType;

                sqlParam[3] = new SqlParameter("@LoanType", SqlDbType.BigInt);
                sqlParam[3].Value = m_LoanType;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountTransactionsSelect", sqlParam);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Int32 UpdateSavingsDepositTransactions()
        {
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[9];
                int intCounter = 0;

                sqlParam[intCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_Id;

                sqlParam[intCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_MemberId;

                sqlParam[intCounter] = new SqlParameter("@Tran_type", SqlDbType.NVarChar, 3);
                sqlParam[intCounter++].Value = m_Tran_type;

                sqlParam[intCounter] = new SqlParameter("@fk_Accountid", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_Accountid;

                sqlParam[intCounter] = new SqlParameter("@Amount", SqlDbType.Float);
                sqlParam[intCounter++].Value = m_Amount;

                sqlParam[intCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_AgentId;

                sqlParam[intCounter] = new SqlParameter("@TransactionDate", SqlDbType.DateTime);
                sqlParam[intCounter++].Value = m_TransactionDate;

                sqlParam[intCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 500);
                sqlParam[intCounter++].Value = m_Remarks;

                sqlParam[intCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_Updatedby;

                return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountTransactionsUpdate", sqlParam));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet DeleteSavingAccTrans(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            sqlParam[iCounter] = new SqlParameter("fk_Accountid", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_Accountid;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountTransactionsDelete", sqlParam);
        }
       // private static string ConnectionString1 =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        public Int32 SaveSavingsDepositTransactions()
        {

            try
            {
                SqlConnection con = new SqlConnection();
        SqlDataAdapter da;
        SqlDataReader rdr = null;
       // SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        con.ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        //con.ConnectionString = "Data Source=AIT3-PC\\SQLEXPRESS;Initial Catalog=Loan;Integrated Security=True";
                con.Open();
           // int rno = Int32.Parse(txtRoom.Text);
                //SqlCommand cmd = new SqlCommand("usp_UpdateSavingsAccCurrentBalance", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //rdr = cmd.ExecuteReader();

                SqlCommand cm = new SqlCommand("SELECT CurrentBalance from SavingAccount WHERE fk_MemberId= @fk_MemberId AND fk_Savingtype = @fk_Savingtype", con);
            cm.Parameters.Add("@fk_MemberId", m_fk_MemberId);
            cm.Parameters.Add("@fk_Savingtype", m_SavingType);
            object amt = cm.ExecuteScalar();
            double k = Convert.ToDouble(amt);
            double ammt;
            if (m_Tran_type == "CRE")
            {
                ammt = m_Amount + k;

            }
            else
            {
                ammt = k-m_Amount;
            }
                float t = (float)ammt;
                SqlParameter[] sqlParam = new SqlParameter[10];
                int intCounter = 0;
               //float query= '';
                sqlParam[intCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_MemberId;

                sqlParam[intCounter] = new SqlParameter("@Tran_type", SqlDbType.NVarChar, 3);
                sqlParam[intCounter++].Value = m_Tran_type;

                sqlParam[intCounter] = new SqlParameter("@fk_Accountid", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_Accountid;

                sqlParam[intCounter] = new SqlParameter("@Amount", SqlDbType.Float);
                sqlParam[intCounter++].Value = m_Amount;

                sqlParam[intCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_fk_AgentId;

                sqlParam[intCounter] = new SqlParameter("@TransactionDate", SqlDbType.DateTime);
                sqlParam[intCounter++].Value = m_TransactionDate;

                sqlParam[intCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 500);
                sqlParam[intCounter++].Value = m_Remarks;

                sqlParam[intCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_Createdby;

                sqlParam[intCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = m_Updatedby;
                sqlParam[intCounter] = new SqlParameter("@CurrentBal", SqlDbType.BigInt);
                sqlParam[intCounter++].Value = t;
                

                return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountTransactionsInsert", sqlParam));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet GetAccountNumber()
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@PassbookNumber", SqlDbType.VarChar, 20);
                param[0].Value = m_PassbookNo;

                param[1] = new SqlParameter("@fk_Slumid", SqlDbType.BigInt);
                param[1].Value = m_fk_SlumId;

                param[2] = new SqlParameter("@Savingtype", SqlDbType.BigInt);
                param[2].Value = m_SavingType;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetAccountNoFromPassbokNo", param);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    /// <summary>
    /// Generated Class for Table : SavingAccountTransactions_Criteria.
    /// </summary>
    public class SavingAccountTransactions_Criteria
    {
        private string m_Remarks;
        private int m_fk_AgentId;
        private int m_Createdby;
        private DateTime m_UpdatedDate;
        private int m_fk_MemberId;
        private string m_Tran_type;
        private int m_fk_Accountid;
        private DateTime m_CreatedDate;
        private int m_Updatedby;
        private double m_Amount;
        private DateTime m_TransactionDate;
        private int m_Id;
        private string z_MyWhere;
        private string _zWhereClause;
        private string z_sep = " ";

        public struct IsDirty_SavingAccountTransactions_Criteria
        {
            public bool m_Remarks;
            public bool m_fk_AgentId;
            public bool m_Createdby;
            public bool m_UpdatedDate;
            public bool m_fk_MemberId;
            public bool m_Tran_type;
            public bool m_fk_Accountid;
            public bool m_CreatedDate;
            public bool m_Updatedby;
            public bool m_Amount;
            public bool m_TransactionDate;
            public bool m_Id;
            public bool MyWhere;

        }
        public IsDirty_SavingAccountTransactions_Criteria z_bool;

        public SavingAccountTransactions_Criteria()
        {
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                z_bool.m_Remarks = true;
                m_Remarks = value;
                if (z_bool.m_Remarks)
                {
                    _zWhereClause += z_sep + "Remarks='" + Remarks + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int fk_AgentId
        {
            get
            {
                return m_fk_AgentId;
            }
            set
            {
                z_bool.m_fk_AgentId = true;
                m_fk_AgentId = value;
                if (z_bool.m_fk_AgentId)
                {
                    _zWhereClause += z_sep + "fk_AgentId='" + fk_AgentId + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
                if (z_bool.m_Createdby)
                {
                    _zWhereClause += z_sep + "Createdby='" + Createdby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
                if (z_bool.m_UpdatedDate)
                {
                    _zWhereClause += z_sep + "UpdatedDate='" + UpdatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int fk_MemberId
        {
            get
            {
                return m_fk_MemberId;
            }
            set
            {
                z_bool.m_fk_MemberId = true;
                m_fk_MemberId = value;
                if (z_bool.m_fk_MemberId)
                {
                    _zWhereClause += z_sep + "fk_MemberId='" + fk_MemberId + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string Tran_type
        {
            get
            {
                return m_Tran_type;
            }
            set
            {
                z_bool.m_Tran_type = true;
                m_Tran_type = value;
                if (z_bool.m_Tran_type)
                {
                    _zWhereClause += z_sep + "Tran_type='" + Tran_type + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int fk_Accountid
        {
            get
            {
                return m_fk_Accountid;
            }
            set
            {
                z_bool.m_fk_Accountid = true;
                m_fk_Accountid = value;
                if (z_bool.m_fk_Accountid)
                {
                    _zWhereClause += z_sep + "fk_Accountid='" + fk_Accountid + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
                if (z_bool.m_CreatedDate)
                {
                    _zWhereClause += z_sep + "CreatedDate='" + CreatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
                if (z_bool.m_Updatedby)
                {
                    _zWhereClause += z_sep + "Updatedby='" + Updatedby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public double Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                z_bool.m_Amount = true;
                m_Amount = value;
                if (z_bool.m_Amount)
                {
                    _zWhereClause += z_sep + "Amount='" + Amount + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime TransactionDate
        {
            get
            {
                return m_TransactionDate;
            }
            set
            {
                z_bool.m_TransactionDate = true;
                m_TransactionDate = value;
                if (z_bool.m_TransactionDate)
                {
                    _zWhereClause += z_sep + "TransactionDate='" + TransactionDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
                if (z_bool.m_Id)
                {
                    _zWhereClause += z_sep + "Id='" + Id + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string MyWhere
        {
            get
            {
                return z_MyWhere;
            }
            set
            {
                z_bool.MyWhere = true;
                z_MyWhere = value;
                if (z_bool.MyWhere)
                {
                    _zWhereClause += z_sep + z_MyWhere;
                    z_sep = " AND ";
                }
            }
        }
        public string WhereClause()
        {
            return _zWhereClause;
        }
    }
}