﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : CodeValue.
    /// </summary>
    public class CodeValue
    {

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private bool m_Isdeleted;
        private DateTime m_UpdatedDate;
        private string m_Name;
        private string m_Abbreviation;
        private DateTime m_CreatedDate;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private int m_Id;
        private int m_fk_CodeTypeId;
        private int m_DisplayOrder;
        private int? m_fk_ParentId;

        public CodeValue()
        {
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                m_Name = value;
            }
        }
        public string Abbreviation
        {
            get
            {
                return m_Abbreviation;
            }
            set
            {
                m_Abbreviation = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        public int fk_CodeTypeId
        {
            get
            {
                return m_fk_CodeTypeId;
            }
            set
            {
                m_fk_CodeTypeId = value;
            }
        }
        public int DisplayOrder
        {
            get
            {
                return m_DisplayOrder;
            }
            set
            {
                m_DisplayOrder = value;
            }
        }
        public int? fk_ParentId
        {
            get
            {
                return m_fk_ParentId;
            }
            set
            {
                m_fk_ParentId = value;
            }
        }

        public DataSet FetchCodeValue()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@CodeTypeId", SqlDbType.BigInt);
            sqlParam[0].Value = (object)m_fk_CodeTypeId ?? DBNull.Value;

            sqlParam[1] = new SqlParameter("@fk_ParentId", SqlDbType.BigInt);
            sqlParam[1].Value = (object)m_fk_ParentId ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_CodeValueSelect", sqlParam);
        }

        public DataSet FetchAbbreviation()
        {
            SqlParameter[] sqlParam = new SqlParameter[3];

            sqlParam[0] = new SqlParameter("@fk_CodeTypeId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_CodeTypeId;

            sqlParam[1] = new SqlParameter("@Abbreviation", SqlDbType.VarChar,5);
            sqlParam[1].Value = m_Abbreviation;

            sqlParam[2] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[2].Value = m_Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AbbrevationCheck", sqlParam);
        }

        public DataSet SaveCodeValue()
        {

            SqlParameter[] sqlParam = new SqlParameter[7];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Name;

            sqlParam[iCounter] = new SqlParameter("@fk_CodeTypeId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_CodeTypeId;

            sqlParam[iCounter] = new SqlParameter("@fk_ParentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_ParentId;

            sqlParam[iCounter] = new SqlParameter("@DisplayOrder", SqlDbType.Int);
            sqlParam[iCounter++].Value = m_DisplayOrder;

            sqlParam[iCounter] = new SqlParameter("@Abbreviation", SqlDbType.VarChar,5);
            sqlParam[iCounter++].Value = m_Abbreviation;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_CodeValueInsert", sqlParam);
        }

        public DataSet UpdateCodeValue()
        {

            SqlParameter[] sqlParam = new SqlParameter[7];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_Name;

            sqlParam[iCounter] = new SqlParameter("@fk_CodeTypeId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_CodeTypeId;

            sqlParam[iCounter] = new SqlParameter("@fk_ParentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_ParentId;

            sqlParam[iCounter] = new SqlParameter("@DisplayOrder", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_DisplayOrder;

            sqlParam[iCounter] = new SqlParameter("@Abbreviation", SqlDbType.VarChar,5);
            sqlParam[iCounter++].Value = m_Abbreviation;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_CodeValueUpdate", sqlParam);
        }

        public DataSet DeleteCodeValue(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_CodeValueDelete", sqlParam);
        }
    }
}