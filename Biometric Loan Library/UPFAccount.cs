using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration; 

namespace SPARC
{
	/// <summary>
	/// Generated Class for Table : UPFAccount.
	/// </summary>
	public class UPFAccount
	{
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

		private double m_InterestPercent;
		private DateTime m_UpdatedDate;
		private string m_Status;
		private string m_AccountNumber;
		private DateTime m_CreatedDate;
		private Int64 m_Createdby;
		private Int64 m_Updatedby;
		private string m_PayOutFrequency;
		private Int64 m_fk_MemberId;
		private int m_Id;
		private bool m_Isdeleted;
		private DateTime m_PayOutEndDate;
		private DateTime m_Opendate;
		private string m_Remarks;
        private string m_Tran_type;
        private decimal m_Amount;
        private int m_fk_AgentId;
        private string m_PayMode;
        private Int64 m_AccId;

		public UPFAccount()
		{
			//z_WhereClause = new UPFAccount_Criteria();
		}
		public double InterestPercent
		{
			get
			{
				return m_InterestPercent;
			}
			set
			{
				m_InterestPercent = value;
			}
		}
		public DateTime UpdatedDate
		{
			get
			{
				return m_UpdatedDate;
			}
			set
			{
				m_UpdatedDate = value;
			}
		}
		public string Status
		{
			get
			{
				return m_Status;
			}
			set
			{
				m_Status = value;
			}
		}
		public string AccountNumber
		{
			get
			{
				return m_AccountNumber;
			}
			set
			{
				m_AccountNumber = value;
			}
		}
		public DateTime CreatedDate
		{
			get
			{
				return m_CreatedDate;
			}
			set
			{
				m_CreatedDate = value;
			}
		}
		public Int64 Createdby
		{
			get
			{
				return m_Createdby;
			}
			set
			{
				m_Createdby = value;
			}
		}
		public Int64 Updatedby
		{
			get
			{
				return m_Updatedby;
			}
			set
			{
				m_Updatedby = value;
			}
		}
		public string PayOutFrequency
		{
			get
			{
				return m_PayOutFrequency;
			}
			set
			{
				m_PayOutFrequency = value;
			}
		}
		public Int64 fk_MemberId
		{
			get
			{
				return m_fk_MemberId;
			}
			set
			{
				m_fk_MemberId = value;
			}
		}
		public int Id
		{
			get
			{
				return m_Id;
			}
			set
			{
				m_Id = value;
			}
		}

        public Int64 AccId
		{
			get
			{
				return m_AccId;
			}
			set
			{
				m_AccId = value;
			}
		}

		public bool Isdeleted
		{
			get
			{
				return m_Isdeleted;
			}
			set
			{
				m_Isdeleted = value;
			}
		}
		public DateTime PayOutEndDate
		{
			get
			{
				return m_PayOutEndDate;
			}
			set
			{
				m_PayOutEndDate = value;
			}
		}
		public DateTime Opendate
		{
			get
			{
				return m_Opendate;
			}
			set
			{
				m_Opendate = value;
			}
		}
		public string Remarks
		{
			get
			{
				return m_Remarks;
			}
			set
			{
				m_Remarks = value;
			}
		}
        public string Tran_type
        {
            get
            {
                return m_Tran_type;
            }
            set
            {
                m_Tran_type = value;
            }
        }
        public string Pay_Mode
        {
            get
            {
                return m_PayMode;
            }
            set
            {
                m_PayMode = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return m_Amount;
            }
            set
            {
                m_Amount = value;
            }
        }

        public int fk_AgentId
        {
            get
            {
                return m_fk_AgentId;
            }
            set
            {
                m_fk_AgentId = value;
            }
        }

        public DataSet LoadAllUPFAccs()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_MemberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllUPFAccountSelect", sqlParam);
        }

        public DataSet LoadAllUPFAccsTrans()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_MemberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllUPFAccTransSelect", sqlParam);
        }

        public DataSet ReOpenUPFAccount(Int64 UserId,Int32 tranid)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            sqlParam[iCounter] = new SqlParameter("@tranid", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = tranid;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UPFAccountReOpen", sqlParam);
        }

        public DataSet DeleteOpenUPFAccount(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[3];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@AccId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_AccId;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UPFAccountTransactionsDelete", sqlParam);
        }

        public Int32 SaveUPFAccount()
        {

            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;
            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;


            sqlParam[iCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_MemberId;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Status;

            //sqlParam[iCounter] = new SqlParameter("@Tran_type", SqlDbType.Char, 3);
            //sqlParam[iCounter++].Value = m_Tran_type;

            //sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            //sqlParam[iCounter++].Value = m_Amount;

            //sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            //sqlParam[iCounter++].Value = m_fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@InterestPercent", SqlDbType.Float, 0);
            sqlParam[iCounter++].Value = m_InterestPercent;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;
            
          // return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UPFAccountTransactionsInsert", sqlParam);
            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "InsertUPFAccount", sqlParam));

        }

        public DataSet SaveUPFAccounttrans(Int32 UPFAccId)
        {

            SqlParameter[] sqlParam = new SqlParameter[13];
            int iCounter = 0;
            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;


            sqlParam[iCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_MemberId;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Status;

            sqlParam[iCounter] = new SqlParameter("@Tran_type", SqlDbType.Char, 3);
            sqlParam[iCounter++].Value = m_Tran_type;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@InterestPercent", SqlDbType.Float, 0);
            sqlParam[iCounter++].Value = m_InterestPercent;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@fk_AccountId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UPFAccId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "InsertUPFAccounttrans", sqlParam);
           // return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "InsertUPFAccount", sqlParam));

        }
        
        public DataSet UpdateUPFAccount(int iAction)
        
        {

            SqlParameter[] sqlParam = new SqlParameter[11];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Status;

            sqlParam[iCounter] = new SqlParameter("@Amount", SqlDbType.Float);
            sqlParam[iCounter++].Value = m_Amount;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_AgentId;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@iAction", SqlDbType.Int);
            sqlParam[iCounter++].Value = iAction;

            sqlParam[iCounter] = new SqlParameter("@PayMode", SqlDbType.VarChar,10);
            sqlParam[iCounter++].Value = Pay_Mode;

            sqlParam[iCounter] = new SqlParameter("@AccId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_AccId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "InsertUPFPayouttrans", sqlParam);
            //return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "InsertUPFPayouttrans", sqlParam));
        }
        //added by Archana--start
        public DataSet UpdateUPFAccountnew()
        {

            SqlParameter[] sqlParam = new SqlParameter[6];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Status;

            sqlParam[iCounter] = new SqlParameter("@OpenDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.VarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks;

            sqlParam[iCounter] = new SqlParameter("@PayOutEndDate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_PayOutEndDate;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;


            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "UpdateUpfAccount", sqlParam);
            //  return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, "usp_UPFAccountTransactionsUpdate", sqlParam));
        }
        //added by Archana--end
        public DataSet LoadAllUPFTrans()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_MemberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllUPFAccountSelectLoad", sqlParam);
        }

        public DataSet LoadInterestPayOut()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_MemberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoadInterestPayout", sqlParam);
        }

    }
}