﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;

namespace SPARC
{
    public class Users
    {
      
           //  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private Int64 m_Id;
        private string m_FullName;
        private string m_ContactNo;
        private string m_UserName;
        private string m_Password;
        private string m_Role;
        private bool m_Isdeleted;
        private DateTime m_CreatedDate;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private DateTime m_UpdatedDate;
        private bool m_IsAgent;
        private Nullable<Int64> m_CenterId;
        private string ConnectionString;

        public Users()
        {
             ConnectionString = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
       
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public bool IsAgent
        {
            get
            {
                return m_IsAgent;
            }
            set
            {
                m_IsAgent = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public string FullName
        {
            get
            {
                return m_FullName;
            }
            set
            {
                m_FullName = value;
            }
        }
        public string ContactNo
        {
            get
            {
                return m_ContactNo;
            }
            set
            {
                m_ContactNo = value;
            }
        }
        public string UserName
        {
            get
            {
                return m_UserName;
            }
            set
            {
                m_UserName = value;
            }
        }
        public string Password
        {
            get
            {
                return m_Password;
            }
            set
            {
                m_Password = value;
            }
        }
        public string Role
        {
            get
            {
                return m_Role;
            }
            set
            {
                m_Role = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public Int64 UserID
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }

        public Nullable<Int64> CenterID
        {
            get
            {
                return m_CenterId;
            }
            set
            {
                m_CenterId = value;
            }    
        }

        public DataSet GetUserDetails()
        {
            try
            {
              
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 25);
                param[0].Value = UserName;

                param[1] = new SqlParameter("@Password", SqlDbType.VarChar, 25);
                param[1].Value = Password;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersLogin", param);
            }
            catch (Exception ex)
            {
                throw ex;


            }
        }

        public DataSet LoadUsers()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
            sqlParam[0].Value = (object)m_FullName ?? DBNull.Value;

            sqlParam[1] = new SqlParameter("@Role", SqlDbType.VarChar, 20);
            sqlParam[1].Value = (object)m_Role ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersSelect", sqlParam);
        }
        
        public DataSet SaveUsers()
        {

            SqlParameter[] sqlParam = new SqlParameter[8];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@FullName", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_FullName;

            sqlParam[iCounter] = new SqlParameter("@ContactNumber", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_ContactNo;

            sqlParam[iCounter] = new SqlParameter("@Username", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_UserName;

            sqlParam[iCounter] = new SqlParameter("@Password", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Password;

            sqlParam[iCounter] = new SqlParameter("@Role", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Role;

            sqlParam[iCounter] = new SqlParameter("@Agent", SqlDbType.Bit);
            sqlParam[iCounter++].Value = m_IsAgent;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@CenterId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_CenterId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersInsert", sqlParam);
        }

        public DataSet UpdateUsers()
        {

            SqlParameter[] sqlParam = new SqlParameter[9];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@FullName", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_FullName;

            sqlParam[iCounter] = new SqlParameter("@ContactNumber", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_ContactNo;

            sqlParam[iCounter] = new SqlParameter("@Username", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_UserName;

            sqlParam[iCounter] = new SqlParameter("@Password", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Password;

            sqlParam[iCounter] = new SqlParameter("@Role", SqlDbType.VarChar, 20);
            sqlParam[iCounter++].Value = m_Role;

            sqlParam[iCounter] = new SqlParameter("@Agent", SqlDbType.Bit);
            sqlParam[iCounter++].Value = m_IsAgent;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@CenterId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_CenterId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersUpdate", sqlParam);
        }

        public DataSet DeleteUsers(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersDelete", sqlParam);
        }

        public DataSet GetUserRoles(int CodeType, int Id)
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@CodeType", SqlDbType.Int);
            sqlParam[iCounter++].Value = CodeType;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[iCounter++].Value = Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UserRoleHierarchy", sqlParam);
        }

        public DataSet FetchUsers()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Username", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_UserName;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.VarChar, 25);
            sqlParam[iCounter++].Value = m_Id;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AllUsersSelect", sqlParam);
        }
    }

    public class Agent
    {
        public int? AgentID { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string centerId { get; set; }

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public int CreateAgent(Agent objAgent)
        {
            return DBAccess.CreateAgent(objAgent);
        }

        public DataSet GetAgents()
        {
            try
            {
                //SqlParameter[] param = new SqlParameter[1];
                //param[0] = new SqlParameter("@Id", SqlDbType.BigInt);
                //param[0].Value = (object)AgentID ?? DBNull.Value;

                //return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UserAgentsSelect", param);  CenterID

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Id", SqlDbType.BigInt);
                param[0].Value = (object)AgentID ?? DBNull.Value;

               
                param[1] = new SqlParameter("@centerId", SqlDbType.BigInt);
                param[1].Value = (object)centerId ?? DBNull.Value;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UserAgentsSelect", param);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
    }
}