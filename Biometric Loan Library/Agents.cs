﻿using System;
using System.Data;
using System.Data.OleDb;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : Agents.
    /// </summary>
    public class Agents
    {
        private bool m_Isdeleted;
        private DateTime m_UpdatedDate;
        private string m_AgentName;
        private DateTime m_CreatedDate;
        private int m_Createdby;
        private int m_Updatedby;
        private int m_Id;
        private string m_Contactnumber;
        private Agents_Criteria z_WhereClause;

        public struct IsDirty_Agents
        {
            public bool m_Isdeleted;
            public bool m_UpdatedDate;
            public bool m_AgentName;
            public bool m_CreatedDate;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Contactnumber;
        }
        public IsDirty_Agents z_bool;

        public Agents()
        {
            z_WhereClause = new Agents_Criteria();
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                z_bool.m_Isdeleted = true;
                m_Isdeleted = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
            }
        }
        public string AgentName
        {
            get
            {
                return m_AgentName;
            }
            set
            {
                z_bool.m_AgentName = true;
                m_AgentName = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
            }
        }
        public string Contactnumber
        {
            get
            {
                return m_Contactnumber;
            }
            set
            {
                z_bool.m_Contactnumber = true;
                m_Contactnumber = value;
            }
        }
        public Agents_Criteria Where
        {
            get
            {
                return z_WhereClause;
            }
            set
            {
                z_WhereClause = value;
            }
        }

        public string Insert()
        {
            string z_sep = "";
            string SQL = "INSERT INTO Agents ( ";
            if (z_bool.m_Isdeleted)
            {
                SQL += z_sep + "Isdeleted";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate";
                z_sep = " , ";
            }
            if (z_bool.m_AgentName)
            {
                SQL += z_sep + "AgentName";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id";
                z_sep = " , ";
            }
            if (z_bool.m_Contactnumber)
            {
                SQL += z_sep + "Contactnumber";
                z_sep = " , ";
            }
            SQL += ") VALUES (";
            z_sep = "";
            if (z_bool.m_Isdeleted)
            {
                SQL += z_sep + "'" + m_Isdeleted + "'";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "'" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_AgentName)
            {
                SQL += z_sep + "'" + m_AgentName + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "'" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "'" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "'" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "'" + m_Id + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Contactnumber)
            {
                SQL += z_sep + "'" + m_Contactnumber + "'";
                z_sep = " , ";
            }
            SQL += ")";
            return SQL;
        }
        public string Update()
        {
            string z_sep = "";
            string SQL = "UPDATE Agents SET ";
            if (z_bool.m_Isdeleted)
            {
                SQL += z_sep + "Isdeleted='" + m_Isdeleted + "'";
                z_sep = " , ";
            }
            if (z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + m_UpdatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_AgentName)
            {
                SQL += z_sep + "AgentName='" + m_AgentName + "'";
                z_sep = " , ";
            }
            if (z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + m_CreatedDate + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + m_Createdby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + m_Updatedby + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + m_Id + "'";
                z_sep = " , ";
            }
            if (z_bool.m_Contactnumber)
            {
                SQL += z_sep + "Contactnumber='" + m_Contactnumber + "'";
                z_sep = " , ";
            }
            z_sep = " WHERE ";
            if (z_WhereClause.z_bool.m_Isdeleted)
            {
                SQL += z_sep + "Isdeleted='" + z_WhereClause.Isdeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AgentName)
            {
                SQL += z_sep + "AgentName='" + z_WhereClause.AgentName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Contactnumber)
            {
                SQL += z_sep + "Contactnumber='" + z_WhereClause.Contactnumber + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string Delete()
        {
            string z_sep = " WHERE ";
            string SQL = "DELETE FROM Agents";
            if (z_WhereClause.z_bool.m_Isdeleted)
            {
                SQL += z_sep + "Isdeleted='" + z_WhereClause.Isdeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AgentName)
            {
                SQL += z_sep + "AgentName='" + z_WhereClause.AgentName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Contactnumber)
            {
                SQL += z_sep + "Contactnumber='" + z_WhereClause.Contactnumber + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
        public string SearchSQL()
        {
            string z_sep = " WHERE ";
            string SQL = "SELECT * FROM Agents";
            if (z_WhereClause.z_bool.m_Isdeleted)
            {
                SQL += z_sep + "Isdeleted='" + z_WhereClause.Isdeleted + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_UpdatedDate)
            {
                SQL += z_sep + "UpdatedDate='" + z_WhereClause.UpdatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_AgentName)
            {
                SQL += z_sep + "AgentName='" + z_WhereClause.AgentName + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_CreatedDate)
            {
                SQL += z_sep + "CreatedDate='" + z_WhereClause.CreatedDate + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Createdby)
            {
                SQL += z_sep + "Createdby='" + z_WhereClause.Createdby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Updatedby)
            {
                SQL += z_sep + "Updatedby='" + z_WhereClause.Updatedby + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Id)
            {
                SQL += z_sep + "Id='" + z_WhereClause.Id + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.m_Contactnumber)
            {
                SQL += z_sep + "Contactnumber='" + z_WhereClause.Contactnumber + "'";
                z_sep = " AND ";
            }
            if (z_WhereClause.z_bool.MyWhere)
            {
                SQL += z_sep + z_WhereClause.MyWhere;
                z_sep = " AND ";
            }
            return SQL;
        }
    }

    /// <summary>
    /// Generated Class for Table : Agents_Criteria.
    /// </summary>
    public class Agents_Criteria
    {
        private bool m_Isdeleted;
        private DateTime m_UpdatedDate;
        private string m_AgentName;
        private DateTime m_CreatedDate;
        private int m_Createdby;
        private int m_Updatedby;
        private int m_Id;
        private string m_Contactnumber;
        private string z_MyWhere;
        private string _zWhereClause;
        private string z_sep = " ";

        public struct IsDirty_Agents_Criteria
        {
            public bool m_Isdeleted;
            public bool m_UpdatedDate;
            public bool m_AgentName;
            public bool m_CreatedDate;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Contactnumber;
            public bool MyWhere;

        }
        public IsDirty_Agents_Criteria z_bool;

        public Agents_Criteria()
        {
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                z_bool.m_Isdeleted = true;
                m_Isdeleted = value;
                if (z_bool.m_Isdeleted)
                {
                    _zWhereClause += z_sep + "Isdeleted='" + Isdeleted + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
                if (z_bool.m_UpdatedDate)
                {
                    _zWhereClause += z_sep + "UpdatedDate='" + UpdatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string AgentName
        {
            get
            {
                return m_AgentName;
            }
            set
            {
                z_bool.m_AgentName = true;
                m_AgentName = value;
                if (z_bool.m_AgentName)
                {
                    _zWhereClause += z_sep + "AgentName='" + AgentName + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
                if (z_bool.m_CreatedDate)
                {
                    _zWhereClause += z_sep + "CreatedDate='" + CreatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
                if (z_bool.m_Createdby)
                {
                    _zWhereClause += z_sep + "Createdby='" + Createdby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
                if (z_bool.m_Updatedby)
                {
                    _zWhereClause += z_sep + "Updatedby='" + Updatedby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
                if (z_bool.m_Id)
                {
                    _zWhereClause += z_sep + "Id='" + Id + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string Contactnumber
        {
            get
            {
                return m_Contactnumber;
            }
            set
            {
                z_bool.m_Contactnumber = true;
                m_Contactnumber = value;
                if (z_bool.m_Contactnumber)
                {
                    _zWhereClause += z_sep + "Contactnumber='" + Contactnumber + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string MyWhere
        {
            get
            {
                return z_MyWhere;
            }
            set
            {
                z_bool.MyWhere = true;
                z_MyWhere = value;
                if (z_bool.MyWhere)
                {
                    _zWhereClause += z_sep + z_MyWhere;
                    z_sep = " AND ";
                }
            }
        }
        public string WhereClause()
        {
            return _zWhereClause;
        }
    }
}