﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : SavingAccount.
    /// </summary>
    public class SavingAccount
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;
        
        private Int64 m_Createdby;
        private string m_Status;
        private Int64 m_fk_MemberId;
        private Int64 m_Updatedby;
        private string m_Remarks;
        private DateTime m_CreatedDate;
        private double m_YearOpeningBalance;
        private bool m_Isdeleted;
        private double m_CurrentBalance;
        private string m_OpenedByName;
        private string m_AccountNumber;
        private DateTime m_UpdatedDate;
        private double m_interest;
        private DateTime m_Opendate;
        private int m_OpenedBy;
        private Int64 m_Id;
        private Int64 m_SavingType;        
        private string m_PassbookNo;       

        public SavingAccount()
        {
            
        }

        public string PassbookNo
        {
            get { return m_PassbookNo; }
            set { m_PassbookNo = value; }
        }

        public Int64 SavingType
        {
            get { return m_SavingType; }
            set { m_SavingType = value; }
        }

        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public string Status
        {
            get
            {
                return m_Status;
            }
            set
            {
                m_Status = value;
            }
        }
        public Int64 fk_MemberId
        {
            get
            {
                return m_fk_MemberId;
            }
            set
            {
                m_fk_MemberId = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                m_Remarks = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public double YearOpeningBalance
        {
            get
            {
                return m_YearOpeningBalance;
            }
            set
            {
                m_YearOpeningBalance = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public double CurrentBalance
        {
            get
            {
                return m_CurrentBalance;
            }
            set
            {
                m_CurrentBalance = value;
            }
        }
        public string OpenedByName
        {
            get
            {
                return m_OpenedByName;
            }
            set
            {
                m_OpenedByName = value;
            }
        }
        public string AccountNumber
        {
            get
            {
                return m_AccountNumber;
            }
            set
            {
                m_AccountNumber = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public double interest
        {
            get
            {
                return m_interest;
            }
            set
            {
                m_interest = value;
            }
        }
        public DateTime Opendate
        {
            get
            {
                return m_Opendate;
            }
            set
            {
                m_Opendate = value;
            }
        }
        public int OpenedBy
        {
            get
            {
                return m_OpenedBy;
            }
            set
            {
                m_OpenedBy = value;
            }
        }
        public Int64 Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        
        public int SaveSBAcc()
        {
            SqlParameter[] sqlParam;
            int iCounter = 0;

            if (m_Id == 0)
                sqlParam = new SqlParameter[9];
            else
            {
                sqlParam = new SqlParameter[10];

                sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_Id;
            }

            sqlParam[iCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_fk_MemberId;

            sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_Status;

            sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 0);
            sqlParam[iCounter++].Value = m_Remarks.Length > 0 ? m_Remarks: DBNull.Value.ToString();

            sqlParam[iCounter] = new SqlParameter("@Opendate", SqlDbType.DateTime);
            sqlParam[iCounter++].Value = m_Opendate;

            sqlParam[iCounter] = new SqlParameter("@OpenedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_OpenedBy;

            sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            sqlParam[iCounter] = new SqlParameter("@SavingType", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_SavingType;

            sqlParam[iCounter] = new SqlParameter("@PassbookNo", SqlDbType.NVarChar, 20);
            sqlParam[iCounter++].Value = m_PassbookNo;

            string spName;
            if (m_Id == 0)
                spName = "usp_SavingAccountInsert";
            else
                spName = "usp_SavingAccountUpdate";

            return Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, spName, sqlParam));

        }        
        public DataSet LoadSBAccById()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[0].Value = m_Id;

            sqlParam[1] = new SqlParameter("@SavingType", SqlDbType.BigInt);
            sqlParam[1].Value = m_SavingType;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountSelect", sqlParam);
        }

        public DataSet DeleteSavingAccount(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SavingAccountDelete", sqlParam);
        }
    }
}