﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : LoanAccount.
    /// </summary>
    public class LoanAccount
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private double m_YearOpeningInterestBalance;
        private Int64 m_Createdby;
        private DateTime m_LoanSanctionedDate;
        private DateTime m_LoanAppliedDate;
        private DateTime m_EMIPlannedEnd;
        private int m_Tenure;
        private double m_InterestPercent;
        private Int64 m_fk_MemberId;
        private DateTime m_EMIPlannedStart;
        private string m_Status;
        private double m_YearOpeningPrincipleBalance;
        private string m_Remarks;
        private Int64 m_LoanType;
        private DateTime m_CreatedDate;
        private bool m_Isdeleted;
        private string m_EMISchedule;
        private Int64 m_Updatedby;
        private Int64? m_ApprovedBy;
        private double m_LoanAmount;
        private string m_AccountNumber;
        private DateTime m_UpdatedDate;
        private DateTime m_Opendate;
        private double m_BalancePrinciple;
        private Int64 m_Id;
        private string m_ApprovedByName;
        private double m_EMIPrincipal;
        private double m_TotalCharges;
        private bool m_IsUpfLoan;

        private string m_RelationName;
        private string m_Address;
        private string m_AreaFederation;
        private string m_Phone;
        private Int64? m_WaterSource;
        private Int64? m_ToiletFacility;
        private string m_HouseSize;
        private Int64? m_HouseRoof;
        private Int64? m_Walls;
        private Int64? m_HouseFloor;
        private string m_HousePatta;
        private Int64? m_ConstructionType;
        private Int64? m_ConstructionDone;
        private string m_HouseCost;
        private string m_ConstructionTime;
        private string m_DepositGiven;
        
        private string m_LoanPassbook;
        private Int64 m_SavingType;       

        public LoanAccount()
        {

        }

        public Int64 SavingType
        {
            get { return m_SavingType; }
            set { m_SavingType = value; }
        }

        public string LoanPassbook
        {
            get { return m_LoanPassbook; }
            set { m_LoanPassbook = value; }
        }
      
        public string RelationName
        {
            get { return m_RelationName; }
            set { m_RelationName = value; }
        }

        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }

        public string AreaFederation
        {
            get { return m_AreaFederation; }
            set { m_AreaFederation = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public Int64? WaterSource
        {
            get { return m_WaterSource; }
            set { m_WaterSource = value; }
        }

        public Int64? ToiletFacility
        {
            get { return m_ToiletFacility; }
            set { m_ToiletFacility = value; }
        }

        public string HouseSize
        {
            get { return m_HouseSize; }
            set { m_HouseSize = value; }
        }

        public Int64? HouseRoof
        {
            get { return m_HouseRoof; }
            set { m_HouseRoof = value; }
        }

        public Int64? Walls
        {
            get { return m_Walls; }
            set { m_Walls = value; }
        }

        public Int64? HouseFloor
        {
            get { return m_HouseFloor; }
            set { m_HouseFloor = value; }
        }

        public string HousePatta
        {
            get { return m_HousePatta; }
            set { m_HousePatta = value; }
        }

        public Int64? ConstructionType
        {
            get { return m_ConstructionType; }
            set { m_ConstructionType = value; }
        }

        public Int64? ConstructionDone
        {
            get { return m_ConstructionDone; }
            set { m_ConstructionDone = value; }
        }

        public string HouseCost
        {
            get { return m_HouseCost; }
            set { m_HouseCost = value; }
        }

        public string ConstructionTime
        {
            get { return m_ConstructionTime; }
            set { m_ConstructionTime = value; }
        }

        public string DepositGiven
        {
            get { return m_DepositGiven; }
            set { m_DepositGiven = value; }
        }

        public double YearOpeningInterestBalance
        {
            get
            {
                return m_YearOpeningInterestBalance;
            }
            set
            {
                m_YearOpeningInterestBalance = value;
            }
        }
        public Int64 Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                m_Createdby = value;
            }
        }
        public DateTime LoanSanctionedDate
        {
            get
            {
                return m_LoanSanctionedDate;
            }
            set
            {
                m_LoanSanctionedDate = value;
            }
        }
        public DateTime LoanAppliedDate
        {
            get
            {
                return m_LoanAppliedDate;
            }
            set
            {
                m_LoanAppliedDate = value;
            }
        }
        public double EMIPrincipal
        {
            get
            {
                return m_EMIPrincipal;
            }
            set
            {
                m_EMIPrincipal = value;
            }
        }
       
        public double TotalCharges
        {
            get
            {
                return m_TotalCharges;
            }
            set
            {
                m_TotalCharges = value;
            }
        }
        public DateTime EMIPlannedEnd
        {
            get
            {
                return m_EMIPlannedEnd;
            }
            set
            {
                m_EMIPlannedEnd = value;
            }
        }
        public int Tenure
        {
            get
            {
                return m_Tenure;
            }
            set
            {
                m_Tenure = value;
            }
        }
        public double InterestPercent
        {
            get
            {
                return m_InterestPercent;
            }
            set
            {
                m_InterestPercent = value;
            }
        }
        public Int64 fk_MemberId
        {
            get
            {
                return m_fk_MemberId;
            }
            set
            {
                m_fk_MemberId = value;
            }
        }
        public DateTime EMIPlannedStart
        {
            get
            {
                return m_EMIPlannedStart;
            }
            set
            {
                m_EMIPlannedStart = value;
            }
        }
        public string Status
        {
            get
            {
                return m_Status;
            }
            set
            {
                m_Status = value;
            }
        }
        public double YearOpeningPrincipleBalance
        {
            get
            {
                return m_YearOpeningPrincipleBalance;
            }
            set
            {
                m_YearOpeningPrincipleBalance = value;
            }
        }
        public string Remarks
        {
            get
            {
                return m_Remarks;
            }
            set
            {
                m_Remarks = value;
            }
        }
        public Int64 LoanType
        {
            get
            {
                return m_LoanType;
            }
            set
            {
                m_LoanType = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                m_CreatedDate = value;
            }
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                m_Isdeleted = value;
            }
        }
        public string EMISchedule
        {
            get
            {
                return m_EMISchedule;
            }
            set
            {
                m_EMISchedule = value;
            }
        }
        public Int64 Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                m_Updatedby = value;
            }
        }
        public Int64? ApprovedBy
        {
            get
            {
                return m_ApprovedBy;
            }
            set
            {
                m_ApprovedBy = value;
            }
        }
        public double LoanAmount
        {
            get
            {
                return m_LoanAmount;
            }
            set
            {
                m_LoanAmount = value;
            }
        }
        public string AccountNumber
        {
            get
            {
                return m_AccountNumber;
            }
            set
            {
                m_AccountNumber = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                m_UpdatedDate = value;
            }
        }
        public DateTime Opendate
        {
            get
            {
                return m_Opendate;
            }
            set
            {
                m_Opendate = value;
            }
        }
        public double BalancePrinciple
        {
            get
            {
                return m_BalancePrinciple;
            }
            set
            {
                m_BalancePrinciple = value;
            }
        }
        public Int64 Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }
        public string ApprovedByName
        {
            get
            {
                return m_ApprovedByName;
            }
            set
            {
                m_ApprovedByName = value;
            }
        }

        public bool IsUpfLoan
        {
            get
            {
                return m_IsUpfLoan;
            }
            set
            {
                m_IsUpfLoan = value;
            }
        }
        public DataSet SaveLoanAcc()
        {
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[37];

                int iCounter = 0;

                sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_Id;

                sqlParam[iCounter] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_fk_MemberId;

                sqlParam[iCounter] = new SqlParameter("@AccountNumber", SqlDbType.NVarChar, 50);
                sqlParam[iCounter++].Value = m_AccountNumber;

                sqlParam[iCounter] = new SqlParameter("@LoanType", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_LoanType;

                sqlParam[iCounter] = new SqlParameter("@Status", SqlDbType.NVarChar, 25);
                sqlParam[iCounter++].Value = m_Status;

                sqlParam[iCounter] = new SqlParameter("@LoanAmount", SqlDbType.Float);
                sqlParam[iCounter++].Value = m_LoanAmount;

                sqlParam[iCounter] = new SqlParameter("@EMISchedule", SqlDbType.Char, 1);
                sqlParam[iCounter++].Value = m_EMISchedule;

                sqlParam[iCounter] = new SqlParameter("@InterestPercent", SqlDbType.Float);
                sqlParam[iCounter++].Value = m_InterestPercent;

                sqlParam[iCounter] = new SqlParameter("@Tenure", SqlDbType.Int);
                sqlParam[iCounter++].Value = m_Tenure;

                sqlParam[iCounter] = new SqlParameter("@LoanAppliedDate", SqlDbType.DateTime);
                sqlParam[iCounter++].Value = m_LoanAppliedDate;

                sqlParam[iCounter] = new SqlParameter("@LoanSanctionedDate", SqlDbType.DateTime);
                sqlParam[iCounter++].Value = m_LoanSanctionedDate;

                sqlParam[iCounter] = new SqlParameter("@EMIPlannedStart", SqlDbType.DateTime);
                sqlParam[iCounter++].Value = m_EMIPlannedStart;

                sqlParam[iCounter] = new SqlParameter("@EMIPlannedEnd", SqlDbType.DateTime);
                sqlParam[iCounter++].Value = m_EMIPlannedEnd;

                sqlParam[iCounter] = new SqlParameter("@EMIPrincipal", SqlDbType.Float);
                sqlParam[iCounter++].Value = m_EMIPrincipal;

                sqlParam[iCounter] = new SqlParameter("@TotalCharges", SqlDbType.Float);
                sqlParam[iCounter++].Value = m_TotalCharges;

                sqlParam[iCounter] = new SqlParameter("@ApprovedBy", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_ApprovedBy == 0 ? -1 : m_ApprovedBy;

                sqlParam[iCounter] = new SqlParameter("@Remarks", SqlDbType.NVarChar, 0);
                sqlParam[iCounter++].Value = m_Remarks.Length > 0 ? m_Remarks : DBNull.Value.ToString();

                sqlParam[iCounter] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_Createdby;

                sqlParam[iCounter] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_Updatedby;

                sqlParam[iCounter] = new SqlParameter("@IsUpfLoan", SqlDbType.Bit);
                sqlParam[iCounter++].Value = m_IsUpfLoan;


                sqlParam[iCounter] = new SqlParameter("@RelationName", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_RelationName;

                sqlParam[iCounter] = new SqlParameter("@Address", SqlDbType.NVarChar,100);
                sqlParam[iCounter++].Value = m_Address;

                sqlParam[iCounter] = new SqlParameter("@AreaFederation", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_AreaFederation;

                sqlParam[iCounter] = new SqlParameter("@Phone", SqlDbType.NVarChar,20);
                sqlParam[iCounter++].Value = m_Phone;

                sqlParam[iCounter] = new SqlParameter("@WaterSource", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_WaterSource;

                sqlParam[iCounter] = new SqlParameter("@ToiletFacility", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_ToiletFacility;

                sqlParam[iCounter] = new SqlParameter("@HouseSize", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_HouseSize;

                sqlParam[iCounter] = new SqlParameter("@HouseRoof", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_HouseRoof;

                sqlParam[iCounter] = new SqlParameter("@Walls", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_Walls;

                sqlParam[iCounter] = new SqlParameter("@HouseFloor", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_HouseFloor;

                sqlParam[iCounter] = new SqlParameter("@HousePatta", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_HousePatta;

                sqlParam[iCounter] = new SqlParameter("@ConstructionType", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_ConstructionType;

                sqlParam[iCounter] = new SqlParameter("@ConstructionDone", SqlDbType.BigInt);
                sqlParam[iCounter++].Value = m_ConstructionDone;

                sqlParam[iCounter] = new SqlParameter("@HouseCost", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_HouseCost;

                sqlParam[iCounter] = new SqlParameter("@ConstructionTime", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_ConstructionTime;

                sqlParam[iCounter] = new SqlParameter("@DepositGiven", SqlDbType.NVarChar,50);
                sqlParam[iCounter++].Value = m_DepositGiven;

                sqlParam[iCounter] = new SqlParameter("@LoanPassbookNo", SqlDbType.NVarChar, 20);
                sqlParam[iCounter++].Value = m_LoanPassbook;

                string spName;
                if (m_Id == 0)
                    spName = "usp_LoanAccountInsert";
                else
                    spName = "usp_LoanAccountUpdate";

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, spName, sqlParam);
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                return null;
            }
        }
        public DataSet LoadLoanAccById()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[0].Value = m_Id;

            sqlParam[1] = new SqlParameter("@LoanType", SqlDbType.BigInt);
            sqlParam[1].Value = m_LoanType;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoanAccountSelect", sqlParam);
        }
        public DataSet DeleteLoanAccount(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_LoanAccountDelete", sqlParam);
        }

        public DataSet GetSavingsCurrBalance()
        {

            SqlParameter[] sqlParam = new SqlParameter[2];

            sqlParam[0] = new SqlParameter("@fk_MemberId", SqlDbType.BigInt);
            sqlParam[0].Value = m_fk_MemberId;

            sqlParam[1] = new SqlParameter("@m_SavingType", SqlDbType.BigInt);
            sqlParam[1].Value = m_SavingType;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SavingsAccCurrentBalSelect", sqlParam);
        }
    }
}