﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPARC;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SPARC
{
    //public class Users
    //{
    //    public int UserID;
    //    public string UserName;
    //    public string Password;

    //    private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

    //    public DataSet GetUserDetails()
    //    {
    //        try
    //        {
    //            SqlParameter[] param = new SqlParameter[2];
    //            param[0] = new SqlParameter("@UserName", SqlDbType.VarChar,25);
    //            param[0].Value = UserName;

    //            param[1] = new SqlParameter("@Password", SqlDbType.VarChar, 25);
    //            param[1].Value = Password;

    //            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_UsersSelect", param);
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }
    //}
    public class MemberUser
    {
        public int MemberID {get;set;}
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string PassBookNumber { get; set; }

        public int CreateMember()
        {
            return 1;
        }

        public int UpdateMember()
        {
            return 2;
        }
    }

    //public class Agent
    //{
    //    public int? AgentID { get; set; }
    //    public string Name { get; set; }
    //    public string ContactNumber { get; set; }

    //    private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

    //    public int CreateAgent(Agent objAgent)
    //    {
    //        return DBAccess.CreateAgent(objAgent);
    //    }

    //    public DataSet GetAgents()
    //    {
    //        try
    //        {
    //            SqlParameter[] param = new SqlParameter[1];
    //            param[0] = new SqlParameter("@Id", SqlDbType.BigInt);
    //            param[0].Value = (object)AgentID ?? DBNull.Value;

    //            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_AgentsSelect", param);
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }
    //}
    public enum LocationCode
    {
        Hub = 1,
        Country=2,
        State=3,
        District=4,
        Taluk=4,
        Slum=5,
        Gender=6,
        //commented on 13.Dec.2014 by sushmitha Member = 12
        // FundersType=
        FundersType = 11,
        CurrencyType = 12,
        UserRole = 15,
        Member = 14,
        WaterSource = 16,
        ToiletFacility = 17,
        HouseRoof = 18,
        Walls = 19,
        HouseFloor = 20,
        ConstructionType = 21,
        ConstructionDone = 22,        
        DocumentType = 23,
        WithdrawalReasons = 24
    }

    public enum OtherCodeTypes
    { 
        LoanandSavingType = 7,
        EMISchedule = 8,
        ModeOfPayment = 10,
        Category = 9
    }

    public class CodeValues
    {
        public int ID { get; set; }
        public int CodeTypeID { get; set; }
        public int? ParentID { get; set; }
        public Int64 fk_memberId { get; set; }

        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        public DataSet GetCodeValues(bool bAllValues)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@CodeTypeID", SqlDbType.Int);
                param[0].Value = CodeTypeID;
                param[1] = new SqlParameter("@ParentID", SqlDbType.Int);
                param[1].Value = ParentID==-1?null:ParentID;
                param[2] = new SqlParameter("@AllValues", SqlDbType.Bit);
                param[2].Value = bAllValues==true?1:0;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetCodeValues", param);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetSavingsName()
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@fk_memberId", SqlDbType.BigInt);
            param[0].Value = fk_memberId;
           
           

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetSavingtype", param);
        }

        public DataSet GetSavingsNamenew()
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@fk_memberId", SqlDbType.BigInt);
            param[0].Value = fk_memberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetSavingtypenew", param);
        }
        public DataSet GetLoanName()
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@fk_memberId", SqlDbType.BigInt);
            param[0].Value = fk_memberId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetLoantype", param);
        }

        public DataSet GetCodeValuesForAllocation(bool bAllValues)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@CodeTypeID", SqlDbType.Int);
                param[0].Value = CodeTypeID;
                param[1] = new SqlParameter("@ParentID", SqlDbType.Int);
                param[1].Value = ParentID == -1 ? null : ParentID;
                param[2] = new SqlParameter("@AllValues", SqlDbType.Bit);
                param[2].Value = bAllValues == true ? 1 : 0;

                return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_GetCodeValuesForAllocation", param);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
