﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Configuration;

namespace SPARC
{
    /// <summary>
    /// Generated Class for Table : CodeType.
    /// </summary>
    public class CodeType
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private bool m_Isdeleted;
        private DateTime m_UpdatedDate;
        private DateTime m_CreatedDate;
        private int m_Createdby;
        private int m_Updatedby;
        private int? m_Id;
        private string m_Code;
        private CodeType_Criteria z_WhereClause;

        public struct IsDirty_CodeType
        {
            public bool m_Isdeleted;
            public bool m_UpdatedDate;
            public bool m_CreatedDate;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Code;
        }
        public IsDirty_CodeType z_bool;

        public CodeType()
        {
            z_WhereClause = new CodeType_Criteria();
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                z_bool.m_Isdeleted = true;
                m_Isdeleted = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
            }
        }
        public int? Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
            }
        }
        public string Code
        {
            get
            {
                return m_Code;
            }
            set
            {
                z_bool.m_Code = true;
                m_Code = value;
            }
        }
        public CodeType_Criteria Where
        {
            get
            {
                return z_WhereClause;
            }
            set
            {
                z_WhereClause = value;
            }
        }

        public DataSet FetchCodeType()
        {

            SqlParameter[] sqlParam = new SqlParameter[1];

            sqlParam[0] = new SqlParameter("@Id", SqlDbType.Int);
            sqlParam[0].Value = (object)m_Id ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_CodeTypeSelect", sqlParam);
        }
    }

    /// <summary>
    /// Generated Class for Table : CodeType_Criteria.
    /// </summary>
    public class CodeType_Criteria
    {
        private bool m_Isdeleted;
        private DateTime m_UpdatedDate;
        private DateTime m_CreatedDate;
        private int m_Createdby;
        private int m_Updatedby;
        private int m_Id;
        private string m_Code;
        private string z_MyWhere;
        private string _zWhereClause;
        private string z_sep = " ";

        public struct IsDirty_CodeType_Criteria
        {
            public bool m_Isdeleted;
            public bool m_UpdatedDate;
            public bool m_CreatedDate;
            public bool m_Createdby;
            public bool m_Updatedby;
            public bool m_Id;
            public bool m_Code;
            public bool MyWhere;

        }
        public IsDirty_CodeType_Criteria z_bool;

        public CodeType_Criteria()
        {
        }
        public bool Isdeleted
        {
            get
            {
                return m_Isdeleted;
            }
            set
            {
                z_bool.m_Isdeleted = true;
                m_Isdeleted = value;
                if (z_bool.m_Isdeleted)
                {
                    _zWhereClause += z_sep + "Isdeleted='" + Isdeleted + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return m_UpdatedDate;
            }
            set
            {
                z_bool.m_UpdatedDate = true;
                m_UpdatedDate = value;
                if (z_bool.m_UpdatedDate)
                {
                    _zWhereClause += z_sep + "UpdatedDate='" + UpdatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public DateTime CreatedDate
        {
            get
            {
                return m_CreatedDate;
            }
            set
            {
                z_bool.m_CreatedDate = true;
                m_CreatedDate = value;
                if (z_bool.m_CreatedDate)
                {
                    _zWhereClause += z_sep + "CreatedDate='" + CreatedDate + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Createdby
        {
            get
            {
                return m_Createdby;
            }
            set
            {
                z_bool.m_Createdby = true;
                m_Createdby = value;
                if (z_bool.m_Createdby)
                {
                    _zWhereClause += z_sep + "Createdby='" + Createdby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Updatedby
        {
            get
            {
                return m_Updatedby;
            }
            set
            {
                z_bool.m_Updatedby = true;
                m_Updatedby = value;
                if (z_bool.m_Updatedby)
                {
                    _zWhereClause += z_sep + "Updatedby='" + Updatedby + "'";
                    z_sep = " AND ";
                }
            }
        }
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                z_bool.m_Id = true;
                m_Id = value;
                if (z_bool.m_Id)
                {
                    _zWhereClause += z_sep + "Id='" + Id + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string Code
        {
            get
            {
                return m_Code;
            }
            set
            {
                z_bool.m_Code = true;
                m_Code = value;
                if (z_bool.m_Code)
                {
                    _zWhereClause += z_sep + "Code='" + Code + "'";
                    z_sep = " AND ";
                }
            }
        }
        public string MyWhere
        {
            get
            {
                return z_MyWhere;
            }
            set
            {
                z_bool.MyWhere = true;
                z_MyWhere = value;
                if (z_bool.MyWhere)
                {
                    _zWhereClause += z_sep + z_MyWhere;
                    z_sep = " AND ";
                }
            }
        }
        public string WhereClause()
        {
            return _zWhereClause;
        }
    }
}