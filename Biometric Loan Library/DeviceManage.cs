﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace SPARC
{
    public class DeviceManage
    {
        private static string ConnectionString =  ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).AppSettings.Settings["ConnString"].Value;

        private Int64 m_Id;
        private string m_DeviceId;
        private Int64 m_UserId;
        private int? m_fk_SlumId;
        private string m_MacAddress;
        private Int64 m_Createdby;
        private Int64 m_Updatedby;
        private DateTime m_CreatedDate;
        private DateTime m_UpdatedDate;        

        public Int64 Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public string DeviceId
        {
            get { return m_DeviceId; }
            set { m_DeviceId = value; }
        }

        public Int64 UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }

        public int? Fk_SlumId
        {
            get { return m_fk_SlumId; }
            set { m_fk_SlumId = value; }
        }

        public string MacAddress
        {
            get { return m_MacAddress; }
            set { m_MacAddress = value; }
        }

        public Int64 Createdby
        {
            get { return m_Createdby; }
            set { m_Createdby = value; }
        }

        public Int64 Updatedby
        {
            get { return m_Updatedby; }
            set { m_Updatedby = value; }
        }

        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }

        public DateTime UpdatedDate
        {
            get { return m_UpdatedDate; }
            set { m_UpdatedDate = value; }
        }

        public DataSet SaveDevice()
        {
            SqlParameter[] sqlParam = new SqlParameter[5];
            int iCounter = 0;

            //sqlParam[iCounter] = new SqlParameter("@DeviceId", SqlDbType.VarChar, 50);
            //sqlParam[iCounter++].Value = m_DeviceId;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_SlumId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_UserId;

            sqlParam[iCounter] = new SqlParameter("@MacAddress", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_MacAddress;

            sqlParam[iCounter] = new SqlParameter("@Createdby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Createdby;

            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DeviceInfoInsert", sqlParam);
        }

        public DataSet UpdateDevice()
        {
            SqlParameter[] sqlParam = new SqlParameter[5];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            //sqlParam[iCounter] = new SqlParameter("@DeviceId", SqlDbType.VarChar, 50);
            //sqlParam[iCounter++].Value = m_DeviceId;

            sqlParam[iCounter] = new SqlParameter("@fk_SlumId", SqlDbType.Int);
            sqlParam[iCounter++].Value = (object)m_fk_SlumId ?? DBNull.Value;

            sqlParam[iCounter] = new SqlParameter("@fk_AgentId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_UserId;

            sqlParam[iCounter] = new SqlParameter("@MacAddress", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_MacAddress;
            
            sqlParam[iCounter] = new SqlParameter("@Updatedby", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Updatedby;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DeviceInfoUpdate", sqlParam);
        }

        public DataSet LoadDevices()
        {

            //SqlParameter[] sqlParam = new SqlParameter[2];

            //sqlParam[0] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
            //sqlParam[0].Value = (object)m_FullName ?? DBNull.Value;

            //sqlParam[1] = new SqlParameter("@Role", SqlDbType.VarChar, 20);
            //sqlParam[1].Value = (object)m_Role ?? DBNull.Value;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DevicesSelect");
        }

        public DataSet DeleteDevices(Int64 UserId)
        {

            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;

            sqlParam[iCounter] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = UserId;

            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_DeviceDelete", sqlParam);
        }

        public DataSet FetchMac()
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            int iCounter = 0;

            sqlParam[iCounter] = new SqlParameter("@MacAddress", SqlDbType.VarChar, 50);
            sqlParam[iCounter++].Value = m_MacAddress;

            sqlParam[iCounter] = new SqlParameter("@Id", SqlDbType.BigInt);
            sqlParam[iCounter++].Value = m_Id;


            return SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, "usp_SelectAllMacAddress", sqlParam);
        }

    }
}
